import { Options } from 'highcharts';

export const lineChart: Options = {
  chart: {
    type: 'line',
    backgroundColor: 'transparent',
    zoomType: 'x',
  },
  credits: {
    enabled: false,
  },
  title: {
    text: '',
  },
  yAxis: {
    gridLineWidth: 0,
    startOnTick: true,
    endOnTick: true,
    lineWidth: 1,
    tickLength: 6,
    tickAmount: 4,
    tickWidth: 2,
    title: {
      text:''
    },
    labels: {
      format: '{value}%',
      style: {
        color: '#031b4e',
        fontWeight: 'normal',
        fontSize: '13px',
        fontFamily:'Poppins',
      },
    },
  },
  legend: {
    enabled: false,
  },
  exporting: {
    buttons: {
        contextButton: {
            enabled: false
        },
    }
},
  xAxis: {
    type: 'datetime',
    lineWidth: 1,
    tickAmount: 8,
    tickWidth: 2,
    tickLength: 6,
    startOnTick: true,
    endOnTick: true,
    dateTimeLabelFormats: {
      day: '%b-%y',
      week: '%b-%y',
      month: '%b-%y',
      year: '%Y',
    },
    labels: {
      style: {
        color: '#031b4e',
        fontWeight: 'normal',
        fontSize: '13px',
        fontFamily:'Poppins',
      },
    },
    
  },
  tooltip: {
    xDateFormat: '%Y-%m-%d',
    shared: true,
    // formatter: function() {
    //     return 'Date:<b>' + this.x + '</b> <br> Value: <b>' + this.y + '</b>' ;
    // }
  },

  plotOptions: {},
  series: [] as any,
};
