import * as Highcharts from 'highcharts';
import { Options } from 'highcharts';
// function graphs(graphid, data1, data2, cate, pw, icon, y1) {
//     const color_grad1 = {
//         linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
//         stops: [
//             [0, '#4AC29A'],
//             [1, '#FDBB2D']
//         ]
//     };
export const SentimentBarChart: Options = {
  chart: {
    backgroundColor: 'transparent',
    height: 300,
    width: 450,
    spacingLeft: 50,
  },
  title: {
    text: null,
  },
  subtitle: {
    text: null,
  },
  xAxis: {
    categories: [
      'Nifty',
      'Bank Nifty',
      'Nifty Mid Cap 100',
      'Nifty Small Cap 100',
    ],
    crosshair: true,
    lineWidth: 1,
    labels: {
      style: {
        color: '#031B4E',
        fontSize: '13px ',
        fontWeight: 'bold',
        fontFamily:'Poppins',
      },
    },
  },
  yAxis: {
    min: 0,
    max: 100,
    gridLineWidth: 0,
    title: {
      text: null,
    },
    labels: {
      enabled: true,
      style: {
        color: 'transparent',
      },
    },
    plotLines: [
      {
        color: 'transparent',
        value: 0,
        label: {
          text: 'BEARISH',
          y: -2 /*moves label down*/,
          x: -66,
          style: {
            color: '#002060',
            fontSize: '11px  ',
            fontWeight: 'bold',
            fontFamily:'Poppins',
          },
        },
      },
      {
        color: 'transparent',
        value: 50,
        label: {
          text: 'CONSOLIDATION',
          align: 'left',
          y: 5 /*moves label down*/,
          x: -77,
          style: {
            color: '#002060',
            fontSize: '11px  ',
            fontWeight: 'bold',
            fontFamily:'Poppins',
          },
        },
      },
      {
        color: 'transparent',
        value: 100,
        label: {
          text: 'BULLISH',
          align: 'left',
          y: 10 /*moves label down*/,
          x: -66,
          style: {
            color: '#002060',
            fontSize: '11px ',
            fontWeight: 'bold',
            fontFamily:'Poppins',
          },
        },
      },
    ],
  },
  tooltip: {
    enabled: false,
  },
  legend: {
    enabled: false,
  },
  credits: {
    enabled: false,
  },
  exporting: {
    buttons: {
      contextButton: {
        enabled: false,
      },
    },
  },
  plotOptions: {
    series: {
      states: {
        hover: {
          enabled: false,
        },
      },
    },
  },
  responsive: {
    rules: [
      {
        condition: {
          maxWidth: 50,
        },
        chartOptions: {
          legend: {
            align: 'center',
            verticalAlign: 'bottom',
            layout: 'horizontal',
          },
          yAxis: {
            labels: {
              align: 'left',
              x: 0,
              y: -5,
            },
            title: {
              text: null,
            },
          },
          subtitle: {
            text: null,
          },
          credits: {
            enabled: false,
          },
        },
      },
    ],
  },
  series: [
    {
      type: 'bullet',
      pointPadding: 0.25,
      targetOptions: {
        width: '130%',
        color: 'black',
        height: 3,
      },
      color: {
        linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
        // stops: [
        //   [0, '#136F63'],
        //   [1, '#F52A4C'],
        // ],
        stops:
        [
          [0,'#136F63'],
          [0.5,'#408B82'],
          [1,'#F52A4C']
        ]
      },
    },
  ],
  // series: [
  //   {
  //     type: 'column',
  //     name: null,
  //     data: [100, 100, 100, 100],
  //     showInLegend: false,
  //     color: {
  //       linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
  //       stops: [
  //         [0, '#4AC29A'],
  //         [1, '#FDBB2D'],
  //       ],
  //     },
  //     states: {
  //       inactive: {
  //         opacity: 1,
  //       },
  //     },
  //   },
  //   {
  //     type: 'scatter',
  //     name: '',
  //     data: [90, 95, 80, 85],
  //     states: {
  //       inactive: {
  //         opacity: 1,
  //       },
  //     },
  //     dataLabels: {
  //       enabled: true,
  //       useHTML: true,
  //       format:
  //         '<i class="fa fa-arrows-h" style="font-size:40px;color:black;margin-left:-20px"></i>',
  //     },
  //     marker: {
  //       enabled: false,
  //       radius: 0,
  //     },
  //   },
  // ],
};
//graphs('india_1y_price', [100, 100, 100, 100], [90, 95, 80, 85], ['Nifty','Bank Nifty','Nifty Mid Cap 100','Nifty Small Cap 100'], 70, '<i class="fa fa-arrows-h arrow1 " style="font-size:30px;color:black;" ></i>', 22)
