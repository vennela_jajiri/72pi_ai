import { Options } from 'highcharts';
import * as Highcharts from 'highcharts';

declare var require: any;
const More = require('highcharts/highcharts-more');
More(Highcharts);

const Exporting = require('highcharts/modules/exporting');
Exporting(Highcharts);

const ExportData = require('highcharts/modules/export-data');
ExportData(Highcharts);

const Accessibility = require('highcharts/modules/accessibility');
Accessibility(Highcharts);

export const bubbleChart: Options = {
  chart: {
    type: 'packedbubble',
  },
  title: {
    text: '',
  },
  tooltip: {
    pointFormat: '<b>{point.name}:</b> {point.value}',
  },
  credits: {
    enabled: false,
  },
  exporting: {
    buttons: {
        contextButton: {
            enabled: false
        },
    }
},
  legend: {
    enabled: false,
  },
  plotOptions: {
    packedbubble: {
      minSize: '30%',
      maxSize: '120%',
      zMin: 0,
      zMax: 1000,
      layoutAlgorithm: {
        splitSeries: false,
        gravitationalConstant: 0.02,
      },
      dataLabels: {
        enabled: true,
        format: '{point.name}',
        filter: {
          property: 'y',
          operator: '>',
          value: 6,
        },
        style: {
          color: 'white',
          textOutline: 0,
          fontWeight: 600,
          fontSize: '12px',
          fontFamily: 'Poppins',
        },
      },
    } as any,
  },
  series: [
    {
      data: [
        {
          color: '#49A0B1',
          name: 'Company1',
          value: 100,
        },
        {
          color: '#47B047',
          name: 'Company2',
          value: 300,
        },
        {
          color: '#186869',
          name: 'Company3',
          value: 200,
        },
        {
          color: '#E91F5E',
          name: 'Company4',
          value: 350,
        },
        {
          color: '#F46D89',
          name: 'Company5',
          value: 500,
        },
        {
            color: '#9470E9',
            name: 'Company6',
            value: 250,
          },
      ],
    },
  ] as any,
};
