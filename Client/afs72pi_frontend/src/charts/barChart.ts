import { Options } from 'highcharts';

export const barChart: Options = {
  chart: {
    type: 'column',
    width: 450,
    height : 300,
  },
  title: {
    text: null,
  },
  subtitle: {
    text: null,
  },
  legend: {
    enabled: true,
  },
  exporting: {
    buttons: {
        contextButton: {
            enabled: false
        },
    }
},
  xAxis: {
    type: 'datetime',
    lineWidth: 1,
    dateTimeLabelFormats: {
      day: '%b-%y',
      week: '%b-%y',
      month: '%b-%y',
      year: '%Y',
    },
    categories: [
      '11-Jan',
      '12-Jan',
      '13-Jan',
      '14-Jan',
      '15-Jan',
      '16-Jan',
      '17-Jan',
      '18-Jan',
      '19-Jan',
      '20-Jan'
    ],
    title: {
      text: null,
    },
  },
  credits: {
    enabled: false,
  },
 
  yAxis: {
    min:0,
    gridLineWidth: 0,
    startOnTick: false,
    endOnTick: false,
    lineWidth: 1,
    tickLength: 6,
    // tickAmount: 5,
    tickInterval:100,
    tickWidth: 2,
    title: {
      text: null,
    },
    
    labels: {
        formatter: function() {
           return (this.value);
        }
    }as any,
  },
  tooltip: {
    xDateFormat: '%Y-%m-%d',
  }as any,
  plotOptions: {
    column: {
      stacking: 'normal',
      dataLabels: {
        enabled: true,
        rotation: -90,
        color: 'white',
        style: {
           textOutline: 0,
           fontWeight: 'normal',
           fontSize: '12px',
           fontFamily: 'Poppins',
       }
    }as any
    },
  },
  series: []
    
};
