import { Options } from 'highcharts';



export const pieChart: Options = {
    chart: {
        type: 'pie',
        backgroundColor: 'transparent',
        
    },
    credits: {
        enabled: false
    },
    exporting: {
        buttons: {
            contextButton: {
                enabled: false
            },
        }
    },
    title: {
        style: {
            color: '#031B4E',
            fontSize: '12px',
            fontWeight: 'bold',
            fontFamily:'Poppins',
        },
        text: null,
        x:-80, 
        y:20,
        verticalAlign: 'bottom',
            floating: true,
    }as any,
    legend: {
        margin: 10,
        padding: 20,
        borderWidth: 0,
        width: 300,
        itemWidth: 100,
        itemStyle: {
            color: '#031B4E',
            fontSize: '10px',
            fontFamily:'Poppins',
        },
    },
    tooltip: {
        formatter: function () {
            return 'Sector : <b>' + this.point.name + '</b><br>' + 'Investment Amount(%) : ' +
                '<b>' + (this.y/100).toFixed(2) + '%</b>' ;
        }
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            innerSize: "60%",
            size: '220',
            depth: "45%",
            showInLegend: true,
            dataLabels: {
                enabled: false,
                formatter: function () {
                    return '<b>' + this.point.name + '</b><br>';
                },
            }
        },
    }as any,
    series: [],
}
