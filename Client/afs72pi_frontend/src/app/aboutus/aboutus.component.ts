import { Component, OnInit,HostListener } from '@angular/core';
import { ViewEncapsulation } from '@angular/core';
import { UserPortfolioService } from '../service/userPortfolios.service';
import { AppComponent } from '../app.component';
import { Title, Meta } from '@angular/platform-browser';

'use strict';
export var value : number = 0;
@Component({
  selector: 'app-aboutus',
  templateUrl: './aboutus.component.html',
  styleUrls: ['./aboutus.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AboutusComponent implements OnInit {
  constructor(private titleService: Title, private metaService: Meta, public userService: UserPortfolioService,public myApp: AppComponent) { }
  customCollapsedHeight: string = '20%';
  customExpandedHeight: string = '20%';
  public linkedinUrl: string = 'https://www.linkedin.com/company/72pi/';
  public gmailUrl:string = "https://www.gmail.com/72pisignals/";
  ngOnInit(): void {
    this.titleService.setTitle('About Us | 72PI');
    this.metaService.addTags([
      {name: 'keywords', content: '72PI About us '},
      {name: 'description', content: 'We bring a GLOBAL perspective to all our investment solutions for our clients around the world.'},
    ]);
    window.scroll({
      top: 0,
      behavior: 'smooth'
    });
    this.myApp.imgname = this.userService.getBodyFunction();
  
  }

  @HostListener('window:scroll', ['$event'])
  onWindowScroll(e) {
    this.myApp.imgname = this.userService.getBodyFunction();
  }
  call(num){
    value=num;
  }
}
