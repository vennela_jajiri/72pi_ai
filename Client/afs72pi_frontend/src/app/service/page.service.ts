import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

function _window() : any {
  return window;
}

@Injectable({
  providedIn: 'root'
})
export class PageService {
  constructor(private http: HttpClient) { }

  // ROOTURL: string = 'http://15.207.58.49:8080/'; 
  ROOTURL: string = 'http://192.168.1.10:2829/';
  // ROOTURL: string = 'http://183.82.104.133:2829/';


  get nativeWindow() : any {
    return _window();
  }
  country: string;
  getBlogs(): Observable<any[]> { return this.http.get<any[]>(this.ROOTURL.concat('blogs/')); }
  readMoreFunction(blogTitle) {
    let selectedBlog = { 'title': blogTitle }
    return this.http.post(this.ROOTURL.concat('blogs/'), selectedBlog);
  }
  getValuationMultiplesData( country, portfolioName, factsetTicker): Observable<object> {
    return this.http.post(this.ROOTURL.concat('pages/' + country + "/Analyzer/valuationMultiples/"), {
      'country': country, 
      'portfolio_name': portfolioName, 'factset_ticker': factsetTicker
    });
  }
  getStockInfo(country, stockname): Observable<object> { 
    return this.http.post(this.ROOTURL.concat('pages/' + country + "/StockInfo/"), 
    { 'country': country, 'stockname': stockname }); 
  }

  getFactorAnalysisViews(country, portfolioName): Observable<any[]> {
    return this.http.post<any[]>(this.ROOTURL.concat('pages/' + country + "/Analyzer/factorAnalysis/"),
      { 'country': country, 'portfolio_name': portfolioName });
  }

  getUserPortfolios(country): Observable<any[]> {
    return this.http.post<any[]>(this.ROOTURL.concat('pages/PortfoliosList/'),
      { 'country': country});
  }
  getMovingAverageData(username, country, portfolioName, factsetTicker): Observable<object> {
    return this.http.post(this.ROOTURL.concat('pages/' + country + "/Analyzer/volume-volatility/"), 
    {
      'country': country, 'username': username,
      'portfolio_name': portfolioName, 'factset_ticker': factsetTicker
    });
  }
  getFactorAnalystTargetPriceViews(country, portfolioName): Observable<any[]> { 
    return this.http.post<any[]>(this.ROOTURL.concat('pages/' + country + "/Analyzer/analystTargetPrice/"), 
    { 'country': country, 'portfolio_name': portfolioName });}

  getmarketViews(country): Observable<any[]> {
    return this.http.get<any[]>(this.ROOTURL.concat('pages/' + country + '/ourmarketview/'));}

  getData(country): Observable<object> {
    return this.http.post(this.ROOTURL.concat('pages/' + country + "/market/"), { 'country': country });
  }

  getETFData(country, selectedETFList, defaultXaxis, defaultYaxis): Observable<object> {
    return this.http.post(this.ROOTURL.concat('pages/' + country + "/etfmonitor/"), 
    { 'country': country, 'etfList': selectedETFList, 'xaxis': defaultXaxis, 'yaxis': defaultYaxis }
    );
  }

  getCreatePortfolioSelectingStocks(country, sector): Observable<object> {
    return this.http.post(this.ROOTURL.concat('pages/' + country + "/create-portfolio-selecting-stocks/"), 
    { 'country': country, 'sector': sector });
  }

  getModelPortfoliosData(country): Observable<object> {
    return this.http.post(this.ROOTURL.concat('pages/' + country + "/modelportfolios/"), { 'country': country });
  }
  getModelPortfoliosReturns(country): Observable<object> {
    return this.http.post(this.ROOTURL.concat('pages/' + country + "/modelportfoliosreturns/"), { 'country': country });
  }
  
  getModelPortfoliosVisualData(country, modelPortfolioName, period, onchangevalue): Observable<object> {
    return this.http.post(this.ROOTURL.concat('pages/' + country + "/viewmodelportfolios/"), {
      'country': country, 'modelPortfolioName': modelPortfolioName, 
      'period': period, 'onChangeValue': onchangevalue
    });
  }
  getPortfolioData(country): Observable<object> {
    return this.http.get(this.ROOTURL.concat('core/' + country + '/portfolio/actions/'));
  }
  deleteSignleStockFromPortfolio(stockID, country) {
    return this.http.delete(this.ROOTURL.concat('core/' + country + '/portfolio/actions/' + stockID + '/')).subscribe();
  }
  updateSignleStockFromPortfolio(stockOBJ, country) {
    return this.http.put(this.ROOTURL.concat('core/' + country + '/portfolio/actions/' + stockOBJ.id + '/'), stockOBJ).subscribe();
  }
  deleteMultipleStockFromPortfolio(stockOBJS, country) {
    return this.http.post(this.ROOTURL.concat('core/' + country + '/portfolio/actions/1/delete_multiple/'), stockOBJS).subscribe();
  }
  saveNewStockToPortfolio(stockOBJ, country) {
    return this.http.post(this.ROOTURL.concat('core/' + country + '/portfolio/actions/1/add_new/'), stockOBJ).subscribe();
  }

  getStocksListSearch(country) {
    return this.http.post(this.ROOTURL.concat('pages/' + country + "/StockListSearch/"), { 'country': country });
  }

  getOurPortfolioData(country) {
    return this.http.get(this.ROOTURL.concat('pages/' + country + '/ourportfolio/'));
  }

  getRiskOverviewViews(country, portfolio_name, period): Observable<object> {
    return this.http.post(this.ROOTURL.concat('pages/' + country + '/risk-overview/'), 
    { 'country': country,  'portfolio_name': portfolio_name, 'period': period });
  }

  getStockCharts(country, list_of_stocks, etf_benchmarks_list, period): Observable<object> {
    return this.http.post(this.ROOTURL.concat('pages/' + country + "/StockChart/"),
    { 'country': country, 'list_of_stocks': list_of_stocks, 
    'etf_benchmarks_list': etf_benchmarks_list, 'period': period });
  }


  getScreenerData(country, filterlist): Observable<object> {
    return this.http.post(this.ROOTURL.concat('pages/' + country + "/screener"), {
      'country': country,
      'filterlist': filterlist,
    });
  }
  getPortfolioReturnsViews(country, portfolio_name, period, onchangevalue,custom_date): Observable<any[]> { 
    return this.http.post<any[]>(this.ROOTURL.concat('pages/' + country + '/portfolioReturns/'),
    { 'country': country, 'portfolio_name': portfolio_name,
      'period': period, 'onChangeValue': onchangevalue ,'custom_date':custom_date}) 
  }

  getTreeMapData(frequency, scale, size, parent, child, layer,country,portfolio_name): Observable<any[]> {
    return this.http.post<any[]>(this.ROOTURL.concat('pages/' + country + '/treemap/'), {
      'frequency': frequency, 'scale': scale, 'size': size, 
      'parent': parent, 'child': child, 'layer': layer,'country': country,'portfolio_name':portfolio_name
    })
  }

  getScrollData(): Observable<object> {
    return this.http.get(this.ROOTURL.concat('pages' + "/dailymarket/"));
  }
  
  getTechnicalIndicatorsViews(country,ticker):Observable<any[]> {
  return this.http.post<any[]>(this.ROOTURL.concat('pages/' + country+ '/technicalIndicators/'),
  {'country':country,'factset_ticker':ticker})
  }

  geOurPerformanceViews(country):Observable<any[]> {
    return this.http.post<any[]>(this.ROOTURL.concat('pages/' +country + '/ourperformance/'),
    {'country':country})
  }

  getDashboardSummaryViews(country,portfolio_name):Observable<any[]> {
    return this.http.post<any[]>(this.ROOTURL.concat('pages/' + country + '/dashboardSummary/'),
    {'country':country,'portfolio_name':portfolio_name})
  }

  getWizardDefaultIndexStocks(country, index){
    return this.http.get(this.ROOTURL.concat('pages/'+country+'/wizard/'+index+'/step/'));
  }
  getWizardChangedIndexStocks(country, index){
    return this.http.get(this.ROOTURL.concat('pages/'+country+'/wizard/'+index+'/step/'));
  }
  getFundamentalWizardStocks(country, index, fundamentalFilters){
    return this.http.post(this.ROOTURL.concat('pages/'+country+'/wizard/'+index+'/step/1/fundamental/'),fundamentalFilters);
  }
  getTechnicalWizardStocks(country, index, technicalFilters){
    return this.http.post(this.ROOTURL.concat('pages/'+country+'/wizard/'+index+'/step/2/technical/'), technicalFilters);
  }
  getQuantOverlayWizardStocks(country, index,oldfilters , quantFilters){
    return this.http.post(this.ROOTURL.concat('pages/'+country+'/wizard/'+index+'/step/3/quant_overlay/'),
    {'oldfilters':oldfilters, 'quantfilters':quantFilters});
  }
  getStocksSummary(country, index,oldfilters , quantFilters){
    return this.http.post(this.ROOTURL.concat('pages/'+country+'/wizard/'+index+'/step/4/summary/'),
    {'oldfilters':oldfilters, 'quantfilters':quantFilters});
  }

  saveWizardPortfolio(country, index, portoflioName, invst_amnt, oldfilters , quantFilters, addToExisting){
    return this.http.post(this.ROOTURL.concat('pages/'+country+'/wizard/'+index+'/step/5/portfolio_saving/'),{'portfolio_name':portoflioName,
    'invst_amnt':invst_amnt,'oldfilters':oldfilters, 'quantfilters':quantFilters,'addToExisting':addToExisting});
  }
  getUserPortfoliosList(country): Observable<any[]> {
    return this.http.post<any[]>(this.ROOTURL.concat('pages/PortfoliosList/'),
      { 'country': country });
  }
  
  getFuturesAnsOptionsData(country){
    return this.http.get(this.ROOTURL.concat('pages/'+country+'/futures-options/'),);
  }
  getPriceData(country,values){
    return this.http.post(this.ROOTURL.concat('accounts/'+country+'/savepricemovements/'),
    {'country':country,'values':values});
  }
  
  getTechData(country,values){
    return this.http.post(this.ROOTURL.concat('accounts/'+country+'/savetechnicaldata/'),
    {'country':country,'values':values});
  }

  getPersonalisationData(country,etfList,Riskvalue){
    return this.http.post(this.ROOTURL.concat('pages/'+country+'/personalisation/'),
    {'country':country,'etf_list':etfList,'riskValue':Riskvalue});
  }

  saveCustomerPortfolio(stocks, country){
    return this.http.post(this.ROOTURL.concat('accounts/'+country+'/customer/portfolio/saving/'),stocks);
  }
  getViewAlert(AlertData,country,state){
    return this.http.post(this.ROOTURL.concat('accounts/'+country+'/viewAlert/notifications/'),{'AlertData':AlertData,'state':state});
  }
  getMyAlerts(country){
    return this.http.post(this.ROOTURL.concat('accounts/'+country+'/myAlerts/'),{'country':country});
  }
  editAlertData(element,country){
    return this.http.post(this.ROOTURL.concat('accounts/'+country+'/myAlerts/edit/'),element);
  }
  deleteAlertData(element,country){
    return this.http.post(this.ROOTURL.concat('accounts/'+country+'/myAlerts/delete/'),element);
  }
  getPricingDetails(country): Observable<any[]> {
    return this.http.get<any[]>(this.ROOTURL.concat('accounts/'+country+'/pricing/'));}

  createFreeSubscription(country): Observable<any[]> {
    return this.http.get<any[]>(this.ROOTURL.concat('accounts/'+country+'/freesubscription/'))}
  
  createPremiumSubscription(country,currency): Observable<any[]> {
    return this.http.get<any[]>(this.ROOTURL.concat('accounts/'+country+'/'+currency+'/premium/subscription/'))}
  
  activatePremiumSubscription(country,currency,userDetails){
    return this.http.post(this.ROOTURL.concat('accounts/'+country+'/'+currency+'/premium/subscription/'),userDetails);
  }
  
  activateFreeSubscription(country,userDetails){
    return this.http.post(this.ROOTURL.concat('accounts/'+country+'/freesubscription/'),userDetails);
  }

  getSubscriptionStatus(country):Observable<any[]> {
    return this.http.get<any[]>(this.ROOTURL.concat('accounts/'+country+'/subscription/status/'))
  }
  verifyPayment(response){
    return this.http.post(this.ROOTURL.concat('accounts/payment/signature/validation/'),response);
  }
}