import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';


@Injectable({
  providedIn: 'root'
})
export class UserPortfolioService {
  private apiData = new BehaviorSubject<any>(null);
  public apiData$ = this.apiData.asObservable();
  private imgname;
  private portfolioName = new BehaviorSubject<any>(null);
  
  public portfolioName$ = this.portfolioName.asObservable();

  userportfolios: any = [];
  constructor(private http: HttpClient) { this.imgname = 1; }
    
  // ROOTURL: string = 'http://15.207.58.49:8080/';
  ROOTURL: string = 'http://192.168.1.10:2829/';
  // ROOTURL: string = 'http://183.82.104.133:2829/';


  country: string;
  current_portfolio: string;
  getUserPortfolios(country){
    return this.http.post(this.ROOTURL.concat('pages/PortfoliosList/'),{ 'country': country });
  }

  getPortfoliosList(country_name) {
    this.getUserPortfolios(country_name).subscribe(portfoliosData => {
      this.userportfolios = portfoliosData['user_portfolios']
      this.setData(this.userportfolios)
      this.setPortfolioName(this.userportfolios[0])
    });
  }
  setData(data) {
    this.apiData.next(data)
  }
  setPortfolioName(portfolioName) {
    this.portfolioName.next(portfolioName)
  }
  onCountryChangeReset()
    {
      this.apiData = new BehaviorSubject<any>(null);
      this.apiData$ = this.apiData.asObservable();

      this.portfolioName = new BehaviorSubject<any>(null);
      this.portfolioName$ = this.portfolioName.asObservable();
    
    }
  getBodyFunction() {
    let element = document.querySelector('.navbar-inverse');
    if (window.pageYOffset === 0) {
      element.classList.add('navbar');
      this.imgname = 1;
      return this.imgname;

    } else {
      element.classList.remove('navbar');
      this.imgname = 0;
      return this.imgname;
    }
  }
}

