import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { JwtHelperService } from '@auth0/angular-jwt';
import { map } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class AuthService {

  // ROOTURL: string = 'http://15.207.58.49:8080/';
  ROOTURL: string = 'http://192.168.1.10:2829/';
  // ROOTURL: string = 'http://183.82.104.133:2829/';
  constructor(private http: HttpClient,
              public jwtHelper: JwtHelperService) { }

  private loginStatus = new BehaviorSubject<boolean>(!this.isAuthenticated());
  login(username: string, password: string){
    return this.http.post<any>(this.ROOTURL.concat('accounts/auth/access-token/login/'),
    { username, password }).pipe(
      map(object =>{
        if (object ){
          this.loginStatus.next(true);
          this.storeToken(object)
        }
        return object;
      })
    )
  }
  logout(){
    this.loginStatus.next(false);
    return this.http.post<any>(this.ROOTURL.concat('accounts/auth/logout/'),
    {"token": this.getRefreshToken()}).pipe(
      map(object =>{
        if (object ){
          this.removeStoredToken();
        }
        return object;
      })
    )
  }
  captchaVerification(captcha_response:string){
    return this.http.post<any>(this.ROOTURL.concat('accounts/auth/captcha/validation/'),
    {captcha_response}).pipe(map(object =>{
      if (object)
      return object;
    }))
  }
  forgotPassword(email:String){
    return this.http.post<any>(this.ROOTURL.concat('accounts/auth/password/reset/'),
    { email}).pipe(
      map(user =>{return user;
      })
    )
  }

  signup(username: string, email: string, password1: string,password2: string){
    return this.http.post<any>(this.ROOTURL.concat('accounts/auth/signup/'),
    { username, email, password1, password2}).pipe(
      map(user =>{return user;
      })
    )
  }

  subscriptionVerification(): Observable<any[]> { 
    return this.http.get<any[]>(this.ROOTURL.concat('accounts/auth/user/subscription/verification/')); 
  }

  paaswordReset(uid:String, token: String,new_password1:String, new_password2:String){
    return this.http.post<any>(this.ROOTURL.concat('accounts/auth/password/reset/confirm/'),
    {new_password1,new_password2,uid,token}).pipe(
      map(object =>{return object;})
    )
  }
  verifyEmailAddress(key:string){
    return this.http.post<any>(this.ROOTURL.concat('accounts/auth/signup/verify-email/'),
    {key}).pipe(
      map(object =>{return object;})
    )
  }
  newsLetterSubscription(eMail: string,country){
    return this.http.post<any>(this.ROOTURL.concat('accounts/newsletter/subscription/'),{'eMail':eMail,'country':country});
  }

  contactUs(data){
    return this.http.post<any>(this.ROOTURL.concat('accounts/contactUs/'),{data});
  }

  saveFeedback(rating,feedback,country){
    return this.http.post<any>(this.ROOTURL.concat('accounts/feedback/'),{'rating':rating,'feedback':feedback,'country':country});
  }
  private storeToken(object){
    localStorage.setItem('token', object.token);
  }
  private storeRfreshToken(object){
    localStorage.removeItem('token');
    localStorage.setItem('token', object.token);
  }
  public RefreshToken() {
    let token = this.getRefreshToken();
    return this.http.post<any>(this.ROOTURL.concat('accounts/auth/refresh-token/'),
    {token}).pipe(
      map(object =>{
        if (object ){this.storeRfreshToken(object); }
        return object;
      })
    )
  }
  public getRefreshToken() {
    return localStorage.getItem('token');
  }
  private removeStoredToken(){
    localStorage.removeItem('token');
  }
  public isAuthenticated(): boolean{
    const token = localStorage.getItem('token');
    return this.jwtHelper.isTokenExpired(token);
  }
  get isLoggedIn() 
  {
    
    return this.loginStatus.asObservable();

  }

  public loginWithGoogle(userOBJ){
    return this.http.post<any>(this.ROOTURL.concat('accounts/auth/google/'),{'token':userOBJ.authToken}).pipe(
      map(object =>{
        if (object ){this.storeToken(object)}
        return object;
      })
    )
  }
}
