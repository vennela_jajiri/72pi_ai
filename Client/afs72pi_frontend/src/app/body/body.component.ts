import { Component,  OnInit, ViewChildren, QueryList, ElementRef, HostListener } from '@angular/core';
import { PageService } from '../service/page.service';
import { UserPortfolioService } from '../service/userPortfolios.service';
import { NavbarComponent } from '../navbar/navbar.component';
import {FormBuilder, FormGroup, Validators,FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import Typewriter from "t-writer.js";
import { AuthService } from '../service/auth.service';
import { timer } from 'rxjs';
import { Observable } from '../../../node_modules/rxjs';
import { Title, Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.scss'],
})
export class BodyComponent implements OnInit {
  public country: string;
  stocknames: string;
  placeholder: string;
  selectedStock;
  step3text: string;
  submitted = false;
  response : string;
  status ;
  LoginStatus$: Observable<boolean>;
  particlesOptions;
  myOptions; India_model_portfolio_itd_return; India_model_portfolio_itd_date; US_model_portfolio_itd_return; US_model_portfolio_itd_date;
  newslettersubscriptionForm: FormGroup;

  constructor(private titleService: Title, private metaService: Meta,private pageService: PageService,
              private myApp: NavbarComponent,
              private userService: UserPortfolioService,
              private route: ActivatedRoute,
              private router: Router,
              private formBuilder:FormBuilder,
              private authService: AuthService,) {

    this.myOptions = {
      'show-delay': 500,
      'autoPlacement': true,
      'theme': 'light',
      'tooltip-class': 'tooltip-div',
      'max-width': '300',
      'width': '300',
      'offset': '2',
    }

  }

  Images = ['/assets/images/manhatten.png', '/assets/images/singapore.png',
    '/assets/images/sealink.png', '/assets/images/singapore.png'];
  @ViewChildren("tw") tw: QueryList<ElementRef>;

  ngAfterViewInit() {
    this.tw.toArray().forEach(el => {
      const target = el.nativeElement;
      const writer = new Typewriter(target, {
        loop: true,
        typeColor: "white",
        typeSpeed: 150,
        deleteSpeed: 0,
        cursorColor: "white",
        blinkSpeed: 600,
      });
      writer.type("Made Transparent & Simple")
        .rest(1800)
        .clear()
        .type("For Access to Global Markets")
        .rest(1800)
        .clear()
        .type("To Build your Financial Future")
        .rest(1800)
        .clear()
        .start();
    });
  }
  getModelPortfolioITDReturn(countryname) {
    this.pageService.getModelPortfoliosReturns(countryname).subscribe(modelPortfolioTable => {
      this[countryname + "_model_portfolio_itd_return"] = modelPortfolioTable['itd_return']
      this[countryname + "_model_portfolio_itd_date"] = modelPortfolioTable['itd_date']
    });
  }
  get f(){return this.newslettersubscriptionForm.controls;}
  onSubmit(){
    this.submitted = true;
    if(this.newslettersubscriptionForm.invalid){
      this.response='Enter Valid Email'
      this.status = 'error'

    }
    else{
      const eMail = this.f.newsletteremail.value;
      this.authService.newsLetterSubscription(eMail,this.country).subscribe(data =>{
        this.response = data['response']
        this.status = data['status']
       });
    }
    timer(3000).toPromise().then(res => {
      this.response=''
    })
}

  ngOnInit(): void {
    this.titleService.setTitle('72PI Portfolio Intelligence');
    this.metaService.addTags([
      {name: 'keywords', content: '72PI, 72pi, Portfolio Intelligence,Global Markets'},
      {name: 'description', content: 'Provides Investors Access to Global Markets'},
    ]);

    window.scroll(0,0);
    this.country = this.myApp.selectedCountry;
    this.getModelPortfolioITDReturn("India")
    this.getModelPortfolioITDReturn("US");
    this.LoginStatus$ = this.authService.isLoggedIn;
    this.pageService.getStocksListSearch(this.country).subscribe(subscribedData => {
      this.stocknames = subscribedData['companies'];
      this.placeholder = subscribedData['placeholder']
    });
    
    this.newslettersubscriptionForm = this.formBuilder.group({
      newsletteremail: new FormControl('',[Validators.required,Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]),
    });
    this.myApp.imgname = this.userService.getBodyFunction();


    this.particlesOptions = {
      autoPlay: true,
    background: {
      color: {
        value: "transparent"
      },
      image: "",
      position: "50% 50%",
      repeat: "no-repeat",
      size: "cover",
      opacity: 1
    },
    backgroundMask: {
      composite: "destination-out",
      cover: {
        color: {
          value: "#fff"
        },
        opacity: 1
      },
      enable: false
    },
      fpsLimit: 60,
      interactivity: {
          detectsOn: "canvas",
          events: {
              onClick: {
                  enable: true,
                  mode: "push"
              },
              onHover: {
                  enable: true,
                  mode: "slow"
              },
              resize: true
          },
          resize: true,
          modes: {
              text: {
                  distance: 400,
                  duration: 2,
                  opacity: 0.8,
                  size: 600,
                  speed: 3
              },
              push: {
                  quantity: 4
              },
              repulse: {
                  distance: 200,
                  duration: 0.4
              }
          }
      },
      particles: {
          color: {
              value: "#ffffff",
          },
          links: {
              color: "#FFFFFF",
              distance: 100,
              enable: true,
              opacity: 0.4,
              width: 0.5
          },
          collisions: {
              enable: true
          },
          move: {
              direction: "none",
              enable: true,
              outMode: "bounce",
              random: false,
              speed: 2,
              straight: false
          },
          number: {
              density: {
                  enable: true,
                  value_area: 900
              },
              value: 140
          },
          opacity: {
              value: 0.4
          },
          shape: {
              type: "circle",
          },
          size: {
              random: true,
              value: 2
          },
      },
      detectRetina: true
  };


  }
  @HostListener('window:scroll', ['$event'])
  onWindowScroll(e) {
    this.myApp.imgname = this.userService.getBodyFunction();
  }
  
  SlideOptions = {
    items: 1, dots: true, nav: false, loop: true, autoplay: true, autoplayTimeout: 6000, autoplayHoverPause: true,
  };
  SlideOptions2 = {
    items: 1, dots: true, nav: true,
    navText: ['<img src="/assets/images/testimonial_left.png" alt="left arrow">', '<img src="/assets/images/testimonial_right.png" alt="right arrow">'],
     loop: true, autoplay: true, autoplayTimeout: 6000, autoplayHoverPause: true,
  };
  CarouselOptions = { items: 3, dots: true, nav: false };

  SlideOptions1 = {
    items: 1, dots: true, nav: true, loop: true, autoplay: true, autoplayTimeout: 3000, autoplayHoverPause: true,
    navText: ['<img src="/assets/images/testimonial_left.png" alt="left arrow">', '<img src="/assets/images/testimonial_right.png" alt="right arrow">'],
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 1
      },
      740: {
        items: 2
      },
      940: {
        items: 2
      },
      1000: {
        items: 2
      },
      1200: {
        items: 2
      },
      1600: {
        items: 3
      },
      1960: {
        items: 3
      }
    },
  };
  SlideOptions3 = {
    items: 1, dots: false, nav: false, loop: true, autoplay: true, autoplayTimeout: 20000, autoplayHoverPause: false,
  };


 

}

