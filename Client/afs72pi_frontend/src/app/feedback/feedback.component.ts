import { Component, OnInit } from '@angular/core';
import { timer } from 'rxjs';
import { MatDialogRef } from '@angular/material/dialog';
import { CookieService } from 'ngx-cookie-service';
import { AuthService } from '../service/auth.service';


@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.scss']
})
export class FeedbackComponent implements OnInit {
  element;
  country;
  constructor(public dialogRef:MatDialogRef<FeedbackComponent>,private authService: AuthService,private cookieService:CookieService) { }
  ngOnInit(): void {
    this.element = document.querySelector('.navbar-inverse');
    this.element.classList.remove('navbar');

  }
  stars: number[] = [1, 2, 3, 4, 5];
  selectedValue: number = 0;
  click: boolean = true;
  feedback = 0;
  feedback_text: any = ''
  countStar(star) {
    this.selectedValue = star;
    if (star > 0) {
      this.click = false
    }
  }
  addStar(star) {
    let ab = "";
    for (let i = 0; i < star; i++) {
      ab = "starId" + i;
      document.getElementById(ab).classList.add("selected");
    }
  }
  removeStar(star) {
    let ab = "";
    for (let i = star - 1; i >= this.selectedValue; i--) {
      ab = "starId" + i;
      document.getElementById(ab).classList.remove("selected");
    }
  }
  submit() {
    this.feedback = 1;
    let country = this.cookieService.get('country')
    this.authService.saveFeedback(this.selectedValue,this.feedback_text,country).subscribe(response=>{
    });
    timer(1600).toPromise().then(res => {
      this.dialogRef.close();
    })
  }
}
