import { Component, OnInit } from '@angular/core';
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'app-page402',
  templateUrl: './page402.component.html',
  styleUrls: ['./page402.component.scss'],
})
export class Page402Component implements OnInit {

  constructor(private titleService:Title) {
    this.titleService.setTitle("Payment Required, Access is denied");
  }

  ngOnInit() {
    window.scroll(0,0);
    let element = document.querySelector('.navbar-inverse');
    element.classList.remove('navbar');
  }

}
