import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-email-verification-status',
  templateUrl: './email-verification-status.component.html',
  styleUrls: ['./email-verification-status.component.scss'],
})
export class EmailVerificationStatusComponent implements OnInit {

  constructor() { }
  error;
  ngOnInit() {
    window.scroll(0,0);
    let element = document.querySelector('.navbar-inverse');
    element.classList.remove('navbar');
  }
}
