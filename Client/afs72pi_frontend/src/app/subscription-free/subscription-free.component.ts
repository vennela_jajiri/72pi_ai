import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { PageService } from '../service/page.service';
import { AppComponent } from '../app.component';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-subscription-free',
  templateUrl: './subscription-free.component.html',
  styleUrls: ['./subscription-free.component.scss'],
})
export class SubscriptionFreeComponent implements OnInit {
  firstName : string;
  lastName : string;
  email : string;
  mobileNumber : number;
  free_eligible : boolean;
  country : string;
  error : any;
  constructor(private cookie : CookieService,
              private pageService : PageService,
              private myApp : AppComponent,
              private route: ActivatedRoute,
              private router : Router) { }
  
  ngOnInit() {
    this.country = this.route.snapshot.params.country;
    this.myApp.setDefaultValue(this.country);
    this.pageService.createFreeSubscription(this.country).subscribe(
      response=>{this.firstName = response['first_name'];
      this.lastName = response['last_name'];
      this.email = response['email'];
      this.mobileNumber = response['contact'];
      this.free_eligible = response['free_eligible'];}
    )
  }
  activateSubscription(){
    let data = {'first_name' : this.firstName, 'last_name': this.lastName,
               'email' : this.email, 'contact_no' : this.mobileNumber};
    this.pageService.activateFreeSubscription(this.country,data).subscribe(
      success => {this.router.navigate([this.country+'/customer/free/subscription/status'])},
      error => {this.error=error}
    );
  }
}
