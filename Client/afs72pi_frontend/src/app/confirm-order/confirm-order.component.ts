import { Component, OnInit ,HostListener, Input} from '@angular/core';
import { AppComponent } from '../app.component';
import { UserPortfolioService } from '../service/userPortfolios.service';
import { ActivatedRoute, Router } from '@angular/router';
import { PageService } from '../service/page.service';
import { Location } from '@angular/common'

@Component({
  selector: 'app-confirm-order',
  templateUrl: './confirm-order.component.html',
  styleUrls: ['./confirm-order.component.scss'],
})
export class ConfirmOrderComponent implements OnInit {
  public subscriberName : string;
  public subscriptionOrder : string;
  public subscriberEmail : string;
  public subscriberContact : string;
  public subscriberPlan: string;
  public subscriptionPrice : number;
  public subscriptionStartDate : string;
  public subscriptionEndDate : string;
  public subscriptionCurrency : string;
  public subscriptionOrderID: string;
  public orderDetails : any;
  public country : string;
  SubscriptionPaymentForm;
  constructor(public myApp: AppComponent, 
              private userService: UserPortfolioService,
              private pageService : PageService,
              private activeRoute: ActivatedRoute,
              private router : Router,
              private location : Location) { }
  ngOnInit() {
    this.country = this.activeRoute.snapshot.params.country;
    this.myApp.setDefaultValue(this.country);
    this.myApp.imgname = this.userService.getBodyFunction();
    this.orderDetails = JSON.parse(localStorage.getItem('orderdetails'));
    if(this.orderDetails){
      this.subscriptionOrder = this.orderDetails['product_id'];
      this.subscriberName = this.orderDetails['name'];
      this.subscriberEmail = this.orderDetails['email'];
      this.subscriberContact = this.orderDetails['phone'];
      this.subscriberPlan = this.orderDetails['payfor'];
      this.subscriptionPrice = this.orderDetails['price'];
      this.subscriptionStartDate = this.orderDetails['start'];
      this.subscriptionEndDate = this.orderDetails['end'];
      this.subscriptionCurrency = this.orderDetails['currency'];
      this.subscriptionOrderID = this.orderDetails['order_id'];
    }else{
      this.router.navigate([this.country+'/pricing-details']);
    }
  }

  @HostListener('window:scroll', ['$event'])
  onWindowScroll(e) {
    this.myApp.imgname = this.userService.getBodyFunction();
  }
 
  //MakePayment function on click of make payment in confirm order page
  makePayment(){
    //Setting all the options to the payment
    const options:any  = {
      key: "rzp_live_lbU8W1Qyi9zpKo",
      amount: this.subscriptionPrice, 
      currency: this.subscriptionCurrency,
      name: this.subscriberName,
      description: this.subscriptionOrder,
      image: "https://www.72pi.ai/assets/images/logo_color.png",
      order_id: this.subscriptionOrderID,
      modal: {
        escape: false,
      }, 
      prefill: {
          "name": this.subscriberName,
          "email": this.subscriberEmail,
          "contact": this.subscriberContact
      },
      theme: {
          "color": "#3399cc"
      }
  };//end of the options
  options.handler = ((response, error) => {
    options.response = response;
    // call your backend api to verify payment signature & capture transaction
    this.pageService.verifyPayment(response).subscribe(response=>{
      localStorage.setItem('subscription_details', JSON.stringify(response));
      this.router.navigate([this.country+'/customer/subscription/status'], { queryParams:{payment_verification: response['payment_verification'], 
        subscription : response['subscription'], status : response['status'],subscription_type:response['subscription_type']
      }});
    });
  });
  options.modal.ondismiss = (() => {
    // handle the case when user closes the form while transaction is in progress
    alert('Transaction cancelled.');
  });
  var rzp = new this.pageService.nativeWindow.Razorpay(options);
  rzp.open();
  }
  goBack(): void {  //GoBack Functionality
    this.location.back();
  }
 
}