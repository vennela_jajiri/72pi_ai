import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../service/auth.service';

@Component({
  selector: 'app-email-verification',
  templateUrl: './email-verification.component.html',
  styleUrls: ['./email-verification.component.scss'],
})
export class EmailVerificationComponent implements OnInit {
  public key : string;
  public error : any;
  constructor(private route : ActivatedRoute,
              private authService : AuthService,
              private router : Router) { }

  ngOnInit() {
    window.scroll(0,0);
    let element = document.querySelector('.navbar-inverse');
    element.classList.remove('navbar');
    this.key = this.route.snapshot.params.key;
  }

  verifyMyEmail(){
    this.authService.verifyEmailAddress(this.key).pipe().subscribe(
    success => {this.router.navigate(['new_user/registration/verify-email/verification_status/']);},
    error => {this.error=error;}
    );
  }

}
