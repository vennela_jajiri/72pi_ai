import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../service/auth.service';
import { first } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { SocialAuthService, SocialUser } from "angularx-social-login";
import { FacebookLoginProvider, GoogleLoginProvider } from "angularx-social-login";
import { AppComponent } from '../app.component';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  error: any;
  sitekey: string;
  submitted = false;
  loginForm: FormGroup;
  hide = true;
  nextURL : string
  user : SocialUser;
  constructor( private authService: AuthService,
               private router : Router,
               private formBuilder:FormBuilder,
               private socialAuthService:SocialAuthService,
               public myApp: AppComponent,
               private route: ActivatedRoute,
              ) {this.sitekey='6LeEpTUaAAAAACLREEbFiJ_EQm8oW2zhABWAAlNi'}
  
  get f(){return this.loginForm.controls;}
  ngOnInit(): void {
    window.scroll(0,0);
    let element = document.querySelector('.navbar-inverse');
    element.classList.remove('navbar');
    this.myApp.imgname = 0;
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
    this.socialAuthService.authState.subscribe(user => {
      this.user = user;
      if(user){
        this.authService.loginWithGoogle(user).subscribe();
      }
    });
    // get return url from route parameters or default to '/'
    this.nextURL = this.route.snapshot.queryParams['next'] || '/';
  }
  onSubmit(){
    this.submitted = true;
    if(this.loginForm.invalid){
    return;
  }
  const userName = this.f.username.value;
  const passWord = this.f.password.value;
  this.authService.login(userName, passWord).pipe(first()).subscribe(
  success => { 
    this.authService.subscriptionVerification().subscribe(response =>{
      this.router.navigate([this.nextURL]);
    });},
  error => {this.error = error},
  );
}
  signInWithGoogle(): void {
    this.socialAuthService.signIn(GoogleLoginProvider.PROVIDER_ID);
  }

  signInWithFB(): void {
    this.socialAuthService.signIn(FacebookLoginProvider.PROVIDER_ID);
  }
}
