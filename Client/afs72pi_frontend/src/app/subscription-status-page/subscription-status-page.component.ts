import { Component, OnInit } from '@angular/core';
import { PageService  } from '../service/page.service';
import { AppComponent } from '../app.component';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-subscription-status-page',
  templateUrl: './subscription-status-page.component.html',
  styleUrls: ['./subscription-status-page.component.scss'],
})
export class SubscriptionStatusPageComponent implements OnInit {

  response = {};
  country : string;
  constructor(private pageService : PageService,
              private myApp : AppComponent,
              private route: ActivatedRoute,) { }

  ngOnInit() {
    this.country = this.route.snapshot.params.country;
    this.myApp.setDefaultValue(this.country);
    this.pageService.getSubscriptionStatus(this.country).subscribe(respose=>{
      this.response= respose;
    });
  }
}
