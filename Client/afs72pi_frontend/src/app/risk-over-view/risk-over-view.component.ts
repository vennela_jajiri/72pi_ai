import { Component, OnInit,Injectable } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { PageService } from '../service/page.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AppComponent } from '../app.component';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { UserPortfolioService } from '../service/userPortfolios.service';
import { Sort } from '@angular/material/sort';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { NavbarComponent } from '../navbar/navbar.component';
import { Title, Meta } from '@angular/platform-browser';
@Component({
  selector: 'app-risk-over-view',
  templateUrl: './risk-over-view.component.html',
  styleUrls: ['./risk-over-view.component.scss'],
})
export class RiskOverViewComponent implements OnInit {
  constructor(private titleService: Title, private metaService: Meta,  private navbar:NavbarComponent,private pageService: PageService, private route: ActivatedRoute, private router: Router, 
    public myApp: AppComponent, private userService: UserPortfolioService,private http: HttpClient) { }
  dataSource: MatTableDataSource<any> = new MatTableDataSource();
  dataSource2: MatTableDataSource<any> = new MatTableDataSource();
  riskoverviewData: any[];
  stockwiseData: any[];
  minValue1: number;
  maxValue1: number;
  minValue2: number;
  maxValue2: number;
  min_beta_list: any[];
  min_vol_list: any[];
  min_var_list: any[];
  max_beta_list: any[];
  max_vol_list: any[];
  max_var_list: any[];
  selectedPortfolio;
  country: string; username: string;
  portfolio_list;
  selectedIndex = 0;
  element;
  symbols = { 'India': 'en-IN', 'US': 'en-US' }
  currencysymbols = { 'India': '₹', 'US': '$' }
  symbol: string;
  currencysymbol: string;
  loading : boolean = true;
  divloading : boolean = false;
  riskoverviewDataSource;
  emptyStocks : boolean = false;errorMsg: string;
  contentFlag = true; errorFlag = false;

  private getServerErrorMessage(error: HttpErrorResponse): string {
    switch (error.status) {
        case 401: {
            return '401';
        }
        case 402: {
            return '402';
        }
        case 403: {
            return '403';
        }
        case 404: {
          return '404';
        }
      
        case 405: {
          return '405';
        }
        case 406: {
          return '406';
        }
        case 412: {
          return '412';
        }
        case 500: {
          return '500';
        }
        case 501: {
          return '501';
        }
        case 502: {
          return '502';
        }
        default: {
            return '500';
        }

    }
  }
  ngOnInit() {
    // window.scroll(0,0);
    let element = document.querySelector('.navbar-inverse');
    element.classList.remove('navbar');
    this.myApp.imgname = 0;
    this.country = this.route.snapshot.params.country;
    this.symbol = this.symbols[this.country]
    this.currencysymbol = this.currencysymbols[this.country]
    this.myApp.setDefaultValue(this.country)
    this.userService.apiData$.subscribe(async portfoliodata => {
      if (portfoliodata == null) {
        await this.userService.getPortfoliosList(this.country)
        await this.userService.apiData$.subscribe(async portfoliodata1 => this.portfolio_list = portfoliodata1)
        await this.userService.portfolioName$.subscribe(async portfolioname => { this.selectedPortfolio = portfolioname })
        if (portfoliodata != null)
          this.getRiskOverviewData(this.country, this.selectedPortfolio.portfolio_name, "1Y")
      }
      else {
        this.portfolio_list = portfoliodata;
        if(portfoliodata.length!=0){
        await this.userService.portfolioName$.subscribe(async portfolioname => { this.selectedPortfolio = portfolioname })
        this.getRiskOverviewData(this.country, this.selectedPortfolio.portfolio_name, "1Y")
        }
        else{
          this.emptyStocks = true;
          this.loading = false;
        }
      }
    });
    

    this.titleService.setTitle('Risk overview');
    this.metaService.addTags([
      {name: 'keywords', content: '72pivar , 72pivfx, var,vfx'},
      {name: 'description', content: 'It helps in understanding the portfolio using risk metrics like volatility, beta, VaR and our in-house risk metric, VFX'},
    ]);


  }
  onChange(event: MatTabChangeEvent) {
    const tab = event.tab.textLabel;
    this.getRiskOverviewData(this.country,  this.selectedPortfolio.portfolio_name, tab);
  }


  onPortfolioChange() {
    this.divloading = true
    this.userService.setPortfolioName(this.selectedPortfolio)
    this.getRiskOverviewData(this.country, this.selectedPortfolio.portfolio_name, "1Y")
    this.selectedIndex = 0;

  }
  getRiskOverviewData(country,  portfolio_name, period) {
    this.pageService.getRiskOverviewViews(country, portfolio_name, period).subscribe(targetPriceInfo => {
      this.titleService.setTitle('Risk overview');
      this.contentFlag = true;
      this.errorFlag = false;
      if(targetPriceInfo['status']==1){
        this.riskoverviewDataSource=targetPriceInfo
        this.loading = true;
        this.riskoverviewData = targetPriceInfo['risk_monitor_table'];
        this.stockwiseData = targetPriceInfo['stockwise_table'];
        this.dataSource = new MatTableDataSource<Element>(this.riskoverviewData);
        this.dataSource2 = new MatTableDataSource<Element>(this.stockwiseData);
        this.minValue1 = targetPriceInfo['min_value1'];
        this.maxValue1 = targetPriceInfo['max_value1'];
        this.maxValue2 = targetPriceInfo['max_value2'];
        this.minValue2 = targetPriceInfo['min_value2'];
        this.min_beta_list = targetPriceInfo['min_beta_list'];
        this.min_var_list = targetPriceInfo['min_var_list'];
        this.min_vol_list = targetPriceInfo['min_vol_list'];
        this.max_beta_list = targetPriceInfo['max_beta_list'];
        this.max_vol_list = targetPriceInfo['max_vol_list'];
        this.max_var_list = targetPriceInfo['max_var_list'];
        this.loading = false;
        this.divloading = false;
      }
      else{
          this.emptyStocks = true;
          this.loading = false;
      }
    }, error => {
      if (error.error instanceof ErrorEvent) {
        this.errorMsg = `Error: ${error.error.message}`;
      }
      else {
        this.contentFlag = false;
        this.errorFlag = true;
        this.loading = false;
        this.errorMsg = this.getServerErrorMessage(error);
      }
    });
  }
 
  sortTableData(sort: Sort,tableName){
    if (sort.active && sort.direction !== '')
        {
          this.riskoverviewDataSource[tableName] = this[tableName].sort((a, b) => {     
          const isAsc = (sort.direction === 'asc');      
          return this._compare(a[sort.active], b[sort.active], isAsc);
          });    
        }
        this[tableName] = Object.assign([], this.riskoverviewDataSource[tableName]);   
  }
  private _compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a > b ? 1 : -1) * (isAsc ? 1 : -1);
  }
  getStockInfopage(selectedStock, selectedSymbol) {
    let pagename = "/StockInfoPage/";
    this.router.navigate([]).then(result => { window.open(this.country + pagename + "?stockname=" + (selectedStock + " (" + selectedSymbol + ")").split("&").join("`"), '_blank','noopener') });
  }
}
