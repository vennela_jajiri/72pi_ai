import { lineChart } from './../../charts/lineChart';
import { Chart } from 'angular-highcharts';
import { Component, OnInit, Injectable } from '@angular/core';
import { PageService } from '../service/page.service';
import { ActivatedRoute,Router } from '@angular/router';
import { MarketComponent } from '../market/market.component';
import * as Highcharts from 'highcharts'
import { AppComponent } from '../app.component';
import { UserPortfolioService } from '../service/userPortfolios.service';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { NavbarComponent } from '../navbar/navbar.component';
import { Title, Meta } from '@angular/platform-browser';


@Component({
  selector: 'app-valuation-multiples',
  templateUrl: './valuation-multiples.component.html',
  styleUrls: ['./valuation-multiples.component.scss'],
})
@Injectable({
  providedIn: 'root'
})
export class ValuationMultiplesComponent implements OnInit {

  userPortfolios: any; portfolioStocks: any; portfolioTickers: any;
  portfolioName: string = ""; companyName: string = ""; factsetTicker: string = "";
  public charts: any = [];
  public chartNames: any = ['Price-to-Earnings (PE)', 'Price-to-Book (PB)', 'Debt-to-Equity (DE)', 'Return-on-Equity (ROE)']
  Chart: any;
  country: string;
  selectedPortfolio;
  selectedStock;
  loading : boolean = true
  errorMsg: string;
  contentFlag = true; errorFlag = false;
  emptyStocks : boolean = false;
  plotCharts(dynamicColumn, plotLinesData, data, textTitle, multiplyFactor, symbol, roundDigits, axisRoundDigits) {
    this[dynamicColumn] = new Chart(lineChart);
    this.charts.push(this[dynamicColumn])
    this[dynamicColumn].options = { series: [data[dynamicColumn]] };
    this[dynamicColumn].options.xAxis = {
      type: 'datetime',
      tickWidth: 1,
      tickLength: 6,
      startOnTick: true,
      endOnTick: true,
      lineWidth: 1,
      dateTimeLabelFormats: {
        day: '%e-%b-%y',
        week: '%e-%b-%y',
        month: '%b-%y',
        year: '%Y',
      },
    },
      this[dynamicColumn].options.yAxis = {
        gridLineWidth: 0,
        lineWidth: 1,
        tickLength: 6,
        tickWidth: 1,
        title: {
          text: textTitle,
          style: {
            color: 'black',
            fontWeight: '600',
            fontFamily: 'Poppins',
          },

        },
        plotLines:
          plotLinesData,
        labels: {
          formatter: function () {
            return ((this.value * multiplyFactor)).toFixed(axisRoundDigits) + symbol;
          },
          style: {
            color: '#031b4e',
            fontSize: '14px',
            fontWeight: 'normal',
            fontFamily: 'Poppins',
          },
        },
      },

      this[dynamicColumn].options.plotOptions = lineChart.plotOptions;
    this[dynamicColumn].options.legend = lineChart.legend;
    this[dynamicColumn].options.chart = {
      type: 'line',
      backgroundColor: 'transparent',
      zoomType: 'x',
      height: 280,
    };
    this[dynamicColumn].options.exporting = lineChart.exporting;
    this[dynamicColumn].options.credits = lineChart.credits;

    this[dynamicColumn].options.tooltip = {
      formatter: function () {
        var s = ["Date : " + '<b>' + Highcharts.dateFormat('%d-%b-%Y', this.x)];
        for (let i = 0; i < this.points.length; i++) {
          s.push(this.points[i].series.name + " : " + '<b>' + (this.points[i].point.y * multiplyFactor).toFixed(roundDigits) + symbol + '</b>');
        }
        return s.join('<br>');
      },
      shared: true,

      style: {
        color: '#031b4e',
        fontSize: '14px',
        fontWeight: 'normal',
        fontFamily: 'Poppins',
      },
    }
    this[dynamicColumn].options.title = lineChart.title;

  }
  constructor(private titleService: Title, private metaService: Meta, private navbar:NavbarComponent,private pageService: PageService, private route: ActivatedRoute, private objForDateConvert: MarketComponent,private http: HttpClient,
    private myApp: AppComponent, private userService: UserPortfolioService,private router: Router,) { }
  callForPlotCharts(inputdata) {
    let noExtraColumn = -1;
    this.objForDateConvert.convertDateToUTC(inputdata, 'pe_line', noExtraColumn)
    this.objForDateConvert.convertDateToUTC(inputdata, 'pb_line', noExtraColumn)
    this.objForDateConvert.convertDateToUTC(inputdata, 'debt_equity_line', noExtraColumn)
    this.objForDateConvert.convertDateToUTC(inputdata, 'roe_line', noExtraColumn)
    this.plotCharts('pe_line', inputdata['pe_avg'], inputdata, this.chartNames[0], 1, "", 2, 0)
    this.plotCharts('pb_line', inputdata['pb_avg'], inputdata, this.chartNames[1], 1, "", 2, 0)
    this.plotCharts('debt_equity_line', inputdata['de_avg'], inputdata, this.chartNames[2], 1, "", 2, 1)
    this.plotCharts('roe_line', inputdata['roe_avg'], inputdata, this.chartNames[3], 100, "%", 2, 0)
  }
  private getServerErrorMessage(error: HttpErrorResponse): string {
    switch (error.status) {
        case 401: {
            return '401';
        }
        case 402: {
            return '402';
        }
        case 403: {
            return '403';
        }
        case 404: {
          return '404';
        }
      
        case 405: {
          return '405';
        }
        case 406: {
          return '406';
        }
        case 412: {
          return '412';
        }
        case 500: {
          return '500';
        }
        case 501: {
          return '501';
        }
        case 502: {
          return '502';
        }
        default: {
            return '500';
        }

    }
  }

  ngOnInit(): void {
    window.scroll(0, 0);
    let element = document.querySelector('.navbar-inverse');
    element.classList.remove('navbar');
    this.myApp.imgname = 0;

    this.country = this.route.snapshot.params.country;
    this.myApp.setDefaultValue(this.country)
    this.userService.apiData$.subscribe(portfoliodata => { this.userPortfolios = portfoliodata; })
    this.userService.portfolioName$.subscribe(portfolioname => { this.selectedPortfolio = portfolioname })

    this.userService.apiData$.subscribe(async portfoliodata => {
      if (portfoliodata == null) {
        await this.userService.getPortfoliosList(this.country)
        await this.userService.apiData$.subscribe(async portfoliodata1 => this.userPortfolios = portfoliodata1)
        await this.userService.portfolioName$.subscribe(async portfolioname => { this.selectedPortfolio = portfolioname })
        if (portfoliodata != null) {
          this.loading = true
          this.onPortfolioChange('portfolioName')
        }
      }
      else {
        this.loading = true
        this.userPortfolios = portfoliodata;
        if(portfoliodata.length!=0){
          await this.userService.portfolioName$.subscribe(async portfolioname => { this.selectedPortfolio = portfolioname })
          this.onPortfolioChange('portfolioName')                   
        }
          else{
            this.emptyStocks = true;
            this.loading = false;
        }

        
      }
    });

    this.titleService.setTitle('Valuation multiples');
    this.metaService.addTags([
      {name: 'keywords', content: '72piportfoliostockvaluation ,72piportfoliostockpe ,72piportfoliostockpb ,72piportfoliostockroe ,72piportfoliostockleverage,portfoliostockvaluation ,portfoliostockpe ,portfoliostockpb ,portfoliostockroe ,portfoliostockleverage'},
      {name: 'description', content: 'Historical graphs Valution multiples like P/E, P/B, D/E and RoE of the portfolio stocks are arranged in a single page for the subscriber to review his portfolio stock by stock'},
    ]);
  }
  onPortfolioChange(variableName) {
    if (variableName == 'portfolioName') {
      this[variableName] = this.selectedPortfolio.portfolio_name;
      this.userService.setPortfolioName(this.selectedPortfolio)
      this.factsetTicker = ""
    }
    else {
      this[variableName] = this.selectedStock.fs_ticker;
    }
    this.pageService.getValuationMultiplesData(this.country, this.portfolioName, this.factsetTicker).subscribe(subscribedData => {
      this.titleService.setTitle('Valuation multiples');
      this.contentFlag = true;
      this.errorFlag = false;
      this.loading = false;
      if(subscribedData['status']==1){
      this.portfolioStocks = subscribedData['portfolio_stocks']
      if (this.factsetTicker == "")
        this.selectedStock = this.portfolioStocks[0]
      this.charts = []
      this.callForPlotCharts(subscribedData)
      }
      else{
        this.emptyStocks = true;
        this.loading = false;
    }
    }, error => {
      if (error.error instanceof ErrorEvent) {
        this.errorMsg = `Error: ${error.error.message}`;
      }
      else {
        this.contentFlag = false;
        this.errorFlag = true;
        this.loading = false;
        this.errorMsg = this.getServerErrorMessage(error);
      }
    });
  }

}
