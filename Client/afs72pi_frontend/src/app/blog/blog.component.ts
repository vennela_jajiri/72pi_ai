import { Component, OnInit,HostListener } from '@angular/core';
import { PageService } from '../service/page.service';
import { Router,ActivatedRoute} from '@angular/router';
import { AppComponent } from '../app.component';
import { Title, Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.scss']
})
export class BlogComponent implements OnInit {
  blogposts : any = []
  blogs : any = []
  title: any;
  error:any;
  site : string;
  homeSlider={items: 1, dots: true, nav: true}
  p:number =1;
  constructor(private titleService: Title, private metaService: Meta,private pageService: PageService,private router : Router,private activeRoute: ActivatedRoute,private myApp:AppComponent) { }

  ngOnInit(): void {
    this.titleService.setTitle('Blogs');
    let selectedBlogTitle = this.activeRoute.snapshot.params.blogTitle;
    this.pageService.getBlogs().subscribe(response=>{this.blogs=response['blogs'];
                                                     this.site= response['site'];
                                                     this.blogposts=response['blogposts']});  
    window.scroll(0,0);
    let element = document.querySelector('.navbar-inverse');
    element.classList.remove('navbar');
    this.myApp.imgname = 0;


  }
  Search(){
    if(this.title==""){
      this.ngOnInit();
    }else{
    this.blogposts = this.blogposts.filter(res=>{
      return res.title.toLocaleLowerCase().match(this.title.toLocaleLowerCase());
    });
  }
  }
  blogReadMore(blog){
    let redirectURL = this.router.url.concat("/"+blog.title);
    this.router.navigateByUrl(redirectURL)
      .then(() => {
        this.router.navigated = false;
        this.router.navigate([redirectURL]);
      });
  }
  
}
