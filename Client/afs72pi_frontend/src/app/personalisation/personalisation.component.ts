import { ActivatedRoute } from '@angular/router';
import { PageService } from './../service/page.service';
import { Chart } from 'angular-highcharts';
import { pieChart } from './../../charts/pieChart';
import { Component, OnInit } from '@angular/core';
import { AppComponent } from '../app.component';


@Component({
  selector: 'app-personalisation',
  templateUrl: './personalisation.component.html',
  styleUrls: ['./personalisation.component.scss'],
})
export class PersonalisationComponent implements OnInit {
  Tenure1;
  Tenure2;
  InterestRate;
  isLinear: boolean = true;
  Riskprofile='Risk Averse';
  InvestorApproach;
  FutureIncomeSources;
  ModeofInvestment;
  etf_ticker;
  outputChartData;
  riskPortfolio;
  AssetPortfolioValue;
  tableHighlight;
  title;
  Highlight : boolean = false;
  showMethods : boolean = false;
  buttonName = "View All Methods";
  Riskvalue=6;allMethodsWeights;allMethodsStats;
  selectPortifolio = [
    '5 Asset Portfolio',
    '10 Asset Portfolio',
    'Equity & ETF Combination',
    'Strategies',
    'Core Satellite',
  ];
  selectEtf = [
    { etf_name: '5 Asset Portfolio', etf_ticker: '5 Asset Portfolio' },
    { etf_name: '10 Asset Portfolio', etf_ticker: '10 Asset Portfolio' },
    {
      etf_name: 'Equity & ETF Combination',
      etf_ticker: 'Equity & ETF Combination',
    },
    { etf_name: 'Strategies', etf_ticker: 'Strategies' },
    { etf_name: 'Core Satellite', etf_ticker: 'Core Satellite' },
  ];
  
  AssetPortfolio = [
    {name:'iShares MSCI EAFE ETF (EFA)',value:'EFA'},
    {name:'iShares MSCI India ETF (INDA)',value:'INDA'},
    {name:'SPDR Gold Shares (GLD)',value:'GLD'},
    {name:'iShares Russell 2000 ETF (IWM)',value:'IWM'},
    {name:'SPDR S&P 500 ETF Trust (SPY)',value:'SPY'},
    {name:'iShares iBoxx $ High Yield Corporate Bond ETF (HYG)',value:'HYG'},
    {name:'iShares Silver Trust (SLV)',value:'SLV'},
    {name:'iShares TIPS Bond ETF (TIP)',value:'TIP'},
    {name:'iShares 20+ Year Treasury Bond ETF (TLT)',value:'TLT'},
    {name:'iShares MSCI Emerging Markets ETF (EEM)',value:'EEM'},
    {name:'iShares 1-3 Year Treasury Bond ETF (SHY)',value:'SHY'},
    {name:'iShares Core U.S. Aggregate Bond ETF (AGG)',value:'AGG'},
    {name:'iShares J.P. Morgan USD Emerging Markets Bond ETF (EMB)',value:'EMB'},
    {name:'SPDR Bloomberg Barclays 1-3 Month T-Bill ETF (BIL)',value:'BIL'},
    {name:'iShares iBoxx $ Investment Grade Corporate Bond ETF (LQD)',value:'LQD'},
    {name:'iShares 7-10 Year Treasury Bond ETF (IEF)',value:'IEF'},
  ];
  displayedColumnsriskPortfolio: string[] = ['AssetClass', 'Allocation'];
  country: string;
  personalisedWeights;statsData;
  riskTypeColumns = ['ETF','Risk Averse','Conservative','Moderate','Aggressive','Very Aggressive']
  statsColumns = ['Metric','Risk Averse','Conservative','Moderate','Aggressive','Very Aggressive']
  
  allMethodsWeightsColumns = ['ETF','Target Risk (16%)','Max Quadratic Utility','Maximum Sharpe','Minimum Volatility']
  allStatsColumns = ['Metric','Target Risk (16%)','Max Quadratic Utility','Maximum Sharpe','Minimum Volatility']

  constructor(private myApp: AppComponent,private pageservice:PageService,private route:ActivatedRoute) {}
  ngOnInit() {
    window.scroll(0, 0);
    let element = document.querySelector('.navbar-inverse');
    element.classList.remove('navbar');
    this.myApp.imgname = 0;
    this.country = this.route.snapshot.params.country;
    this.myApp.setDefaultValue(this.country);
    this.etf_ticker = '5 Asset Portfolio'
    this.AssetPortfolioValue = ['INDA','GLD','SPY','TIP','SHY']
  }
  changeRiskValue(val){
    let riskValues = {'Risk Averse':6,'Conservative':9,'Moderate':11,'Aggressive':14,'Very Aggressive':16}
    this.Riskvalue = riskValues[val]
  }
  viewAllData(){
    if(this.showMethods == true)
      this.buttonName = "View All Methods"
    else
      this.buttonName = "Hide All Methods"
    this.showMethods = !(this.showMethods);
   
  }
  step1Next() {
    console.log('');
  }
  plotPieChartData(weightsData)
    {
      this.riskPortfolio = new Chart(pieChart);
      this.riskPortfolio.options.title = pieChart.title;
      this.riskPortfolio.options.chart = pieChart.chart;
      this.riskPortfolio.options.credits = pieChart.credits;
      this.riskPortfolio.options.exporting = pieChart.exporting;
      this.riskPortfolio.options.legend = pieChart.legend;
      this.riskPortfolio.options.plotOptions = pieChart.plotOptions;
      this.riskPortfolio.options.series = weightsData;
      this.riskPortfolio.options.tooltip = {
        formatter: function () {
            return 'Sector : <b>' + this.point.name + '</b><br>' + 'Weight : ' +
                '<b>' + (this.y).toFixed(2) + '%</b>' ;
        }
    }
    }
  step3Next() {
    this.title=this.Riskprofile
    this.pageservice.getPersonalisationData(this.country,this.AssetPortfolioValue,this.Riskvalue).subscribe(subscribedData => {
      this.personalisedWeights = subscribedData['optimal_weights'];
      this.outputChartData =  subscribedData['output_chart_data'];
      this.statsData = subscribedData['stats']
      this.allMethodsWeights = subscribedData['all_methods_weights']
      this.allMethodsStats= subscribedData['all_methods_stats']
      this.plotPieChartData(this.outputChartData[this.Riskprofile]) 
    })
    this.buttonName = "View All Methods"
    this.showMethods = false
   
  }
  riskValueChange(riskValue)
  {
    if (riskValue>=0 && riskValue<=6)
      this.Riskprofile = 'Risk Averse'
    else if (riskValue>=7 && riskValue<=9)
      this.Riskprofile = 'Conservative'
    else if (riskValue>=10 && riskValue<=11)
      this.Riskprofile = 'Moderate'
    else if (riskValue>=12 && riskValue<=14)
      this.Riskprofile = 'Aggressive'
      else if (riskValue>=15)
      this.Riskprofile = 'Very Aggressive'

  }
  changeRiskMode(columnName)
  {
   
    if (columnName!='ETF')
      this.title = columnName
      this.plotPieChartData(this.outputChartData[columnName])  
  }
  etfChange(){
    this.AssetPortfolioValue = []
  }
}
