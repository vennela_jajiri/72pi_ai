import { Component, OnInit,Injectable } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { PageService } from '../service/page.service';
import * as Highcharts from 'highcharts';
import { ActivatedRoute,Router } from '@angular/router';
import { AppComponent } from '../app.component';
import { StockChart } from "angular-highcharts";
import { UserPortfolioService } from '../service/userPortfolios.service';
import { Sort } from '@angular/material/sort';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { NavbarComponent } from '../navbar/navbar.component';
import { Title, Meta } from '@angular/platform-browser';
@Component({
    selector: 'app-factor-analysis',
    templateUrl: './factor-analysis.component.html',
    styleUrls: ['./factor-analysis.component.scss'],
})
@Injectable({
    providedIn: 'root'
  })
export class FactorAnalysisComponent implements OnInit {
    factorInfoData: any[];
    factors: any[];
    ouptut: any[];
    data: any[];
    country: string;
    portfolio_list: any[];
    portfolio_name;
    selectedPortfolio;
    dataSource: MatTableDataSource<any> = new MatTableDataSource();
    username: string;
    stock: StockChart;
    symbols = {'India':'en-IN','US':'en-US'}
    currencysymbols = {'India':'₹','US':'$'}
    symbol:string;
    currencysymbol:string;
    graphFormat={'India':'hi-IN','US':'en-US'}
    loading : boolean = true;
    divloading: boolean;
  errorMsg: string;
  contentFlag = true; errorFlag = false;
  emptyStocks:boolean = false;
    constructor(private titleService: Title, private metaService: Meta,  
        private navbar:NavbarComponent,private pageService: PageService, private route: ActivatedRoute,private router: Router, public myApp: AppComponent,private userService:UserPortfolioService,private http: HttpClient) { }
    getFactorsData(country, portfolio_name) {
        this.pageService.getFactorAnalysisViews(country, portfolio_name).subscribe(factorInfo => {
            this.titleService.setTitle('Factor analysis');
            this.contentFlag = true;
            this.errorFlag = false;
            if(factorInfo['status']==1){
            this.factorInfoData = factorInfo['factorInfo'];
            this.factors = factorInfo['factors'];
            this.ouptut = factorInfo['output'];
            this.dataSource = new MatTableDataSource<Element>(this.factorInfoData);
            this.applyFilter(this.factors[0])
            this.loading = false;
            this.divloading = false;
            }
            else{
                this.emptyStocks = true;
                this.loading = false;
            }
        }, error => {
            if (error.error instanceof ErrorEvent) {
              this.errorMsg = `Error: ${error.error.message}`;
            }
            else {
              this.contentFlag = false;
              this.errorFlag = true;
              this.loading = false;
              this.errorMsg = this.getServerErrorMessage(error);
            }
          });
    }
    getPortfolioNames(country) {
        this.pageService.getUserPortfolios(country).subscribe(portfoliosData => {
            this.portfolio_list = portfoliosData['user_portfolios']
            this.portfolio_name = this.portfolio_list[0]
        });
    }
    private getServerErrorMessage(error: HttpErrorResponse): string {
        switch (error.status) {
            case 401: {
                return '401';
            }
            case 402: {
                return '402';
            }
            case 403: {
                return '403';
            }
            case 404: {
              return '404';
            }
          
            case 405: {
              return '405';
            }
            case 406: {
              return '406';
            }
            case 412: {
              return '412';
            }
            case 500: {
              return '500';
            }
            case 501: {
              return '501';
            }
            case 502: {
              return '502';
            }
            default: {
                return '500';
            }
    
        }
      }
    onChange() {
        this.divloading = true;
        this.userService.setPortfolioName(this.selectedPortfolio)
        this.getFactorsData(this.country, this.selectedPortfolio.portfolio_name)
    }
    ngOnInit() {
        window.scroll(0,0);
        let element = document.querySelector('.navbar-inverse');
        element.classList.remove('navbar');
        this.myApp.imgname = 0;
        this.country = this.route.snapshot.params.country;
        this.myApp.setDefaultValue(this.country)
        this.symbol = this.symbols[this.country]
        this.currencysymbol = this.currencysymbols[this.country]
        this.userService.apiData$.subscribe(async portfoliodata =>{
        if (portfoliodata==null)
        { 
        await this.userService.getPortfoliosList(this.country);  
        await this.userService.apiData$.subscribe(async portfoliodata1 => this.portfolio_list = portfoliodata1);     
        await this.userService.portfolioName$.subscribe(async portfolioname=>{this.selectedPortfolio=portfolioname})
        if (portfoliodata!=null)
            this.getFactorsData(this.country,this.selectedPortfolio.portfolio_name)
        } 
        else
        {
        this.portfolio_list = portfoliodata;
        if(portfoliodata.length!=0){
        await this.userService.portfolioName$.subscribe(async portfolioname=>{this.selectedPortfolio=portfolioname})
        this.getFactorsData(this.country,  this.selectedPortfolio.portfolio_name)
        }
        else{
            this.emptyStocks = true;
            this.loading = false;
        }
        } 
        });
        
    this.titleService.setTitle('Factor analysis');
    this.metaService.addTags([
      {name: 'keywords', content: '72pifactoranlaysis ,72piriskmonitoring ,72piquantitativeriskmonitoring ,72piportfoliostockbeta ,72piportfoliostockpe ,72piportfoliostockpb ,72piportfoliostockRoE ,72piportfoliostockvaluation,factoranlaysis ,riskmonitoring ,quantitativeriskmonitoring ,portfoliostockbeta ,portfoliostockpe ,portfoliostockpb ,portfoliostockRoE ,portfoliostockvaluation'},
      {name: 'description', content: ': It helps the subscriber in analysing the portfolio stocks by different factors like size, valuation, growth, profitability, leverage, risk and piotroski score. Also, It helps in reviewing the portfolio by beta, volatility, PE,PB, Dividend yield, Dividend payout, Sales growth, EPS growth, RoE, RoCE, Net debt/EBITDA, Debt/Equity, Piotroski score'},
    ]);

    }
    sortData(sort: Sort){
        if (sort.active && sort.direction !== '')
            {
              this.dataSource['data'] = this.factorInfoData.sort((a, b) => {     
              const isAsc = (sort.direction === 'asc');      
              return this._compare(a[sort.active], b[sort.active], isAsc);
              });    
            }
        this.factorInfoData = Object.assign([], this.dataSource['data']);     
        this.dataSource = new MatTableDataSource(this.factorInfoData); 
      }
      private _compare(a: number | string, b: number | string, isAsc: boolean) {
        return (a > b ? 1 : -1) * (isAsc ? 1 : -1);
      }
    
    applyFilter(firstDropVal) {
        for (let i = 0; i < this.ouptut.length; i++) {
            if (this.ouptut[i]['Factor'] == firstDropVal) {
                this.data = this.ouptut[i]['data']
                break;
            }
        }
        this.plotCharts('InvestmentAmount', 'Investment Amount', this.data[0],this.graphFormat[this.country]);
        this.plotCharts('Return', 'Return('+this.currencysymbol+')', this.data[1],this.graphFormat[this.country]);
        this.plotCharts('NoOfStocks', '# of Stocks', this.data[2],this.graphFormat[this.country]);
    }
    plotCharts(id, graphTitle, data,format) {
        Highcharts.chart(id, {
            chart: {
                type: 'column',
                backgroundColor: "transparent",
            },
            title: {
                text: graphTitle,
                verticalAlign: 'bottom',
                style: {
                    color: '#031b4e',
                    fontSize: '16px ',
                    fontWeight: 'bold'
                },
            },
            credits: {
                enabled: false
            },
            exporting: {
                buttons: {
                    contextButton: {
                        enabled: false
                    },
                }
            },
            xAxis: {
                categories: ['Low', 'Inline', 'High'],
                title: {
                    text: null
                },
                labels: {
                    style: {
                        color: '#031b4e',
                        fontSize: '14px ',
                        fontWeight: 'normal'
                    },
                },
            },
            yAxis: {
                gridLineWidth: 0,
                lineWidth: 1,
                endOnTick: true,
                title: {
                    text: null
                },
                labels: {
                    formatter: function () {
                        return this.axis.defaultLabelFormatter.call(this);
                    },
                    style: {
                        color: '#031b4e',
                        fontSize: '14px ',
                        fontWeight: 'normal'
                    },
                },
            },
            tooltip: {
                formatter: function () {
                    return this.x + ' : <b>' + (this.point.y).toLocaleString(format, { maximumFractionDigits: 0 }) + '</b>'
                },
                style: {
                    textShadow: false,
                    color: '#031b4e',
                    fontSize: '14px ',
                    fontWeight: 'normal'
                }
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0,
                    colors: ['#FDCA40','#00A878', '#00A8E8' ],
                    colorByPoint: true,
                    dataLabels: {
                        enabled: true,
                        allowOverlap: true,
                        formatter: function () {
                            return (this.y).toLocaleString(format, { maximumFractionDigits: 0 });
                        },
                        style: {
                            textShadow: false,
                            color: 'black',
                            fontSize: '14px ',
                            fontWeight: 'bold'
                        }
                    }
                }
            },
            series: [{
                type: undefined,
                name: id,
                data: data
            },],
            legend: {
                enabled: false
            }
        });
    }
    getStockInfopage(selectedStock,selectedSymbol){
        let pagename = "/StockInfoPage/";
        this.router.navigate([]).then(result => {  window.open( this.country + pagename + "?stockname=" + (selectedStock+" ("+selectedSymbol+")").split("&").join("`") , '_blank','noopener') });
      }
}
