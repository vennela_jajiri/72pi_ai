import { BrowserModule } from '@angular/platform-browser';
import { NgModule ,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { AppComponent,CookiesComponent, } from './app.component';
import { LogoutDialogComponent,SuccessDialogComponent } from './navbar/navbar.component';
import { NavbarComponent } from './navbar/navbar.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material/material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FooterComponent } from './footer/footer.component';
import { BodyComponent } from './body/body.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { MatSelectCountryModule } from '@angular-material-extensions/select-country';
import { HttpClientModule,HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { BlogComponent } from './blog/blog.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { PasswordResetComponent } from './password-reset/password-reset.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import { ScrollTopComponent } from './scroll-top/scroll-top.component';
import { MarketComponent } from './market/market.component';
import { ChartModule } from 'angular-highcharts';
import { AboutusTeamComponent } from './aboutus-team/aboutus-team.component';
import { AboutusVisionMissionComponent } from './aboutus-vision-mission/aboutus-vision-mission.component';
import { InvestmentPhilosophyComponent } from './investment-philosophy/investment-philosophy.component';
import { FloatingIconsComponent } from './floating-icons/floating-icons.component';
import { FreeTrailComponent } from './free-trail/free-trail.component';
import { FeedbackComponent } from './feedback/feedback.component';
import { ETFmonitorComponent } from './etfmonitor/etfmonitor.component';
import { StrategiesComponent } from './strategies/strategies.component';
import { NgxSliderModule } from '@angular-slider/ngx-slider';
import { PortfolioDetailsComponent } from './portfolio-details/portfolio-details.component';
import { ModelPortfoliosComponent } from './model-portfolios/model-portfolios.component';
import { TableModule } from 'primeng/table';
import { ToastModule } from 'primeng/toast';
import { CalendarModule } from 'primeng/calendar';
import { SliderModule } from 'primeng/slider';
import { MultiSelectModule } from 'primeng/multiselect';
import { ContextMenuModule } from 'primeng/contextmenu';
import { DialogModule } from 'primeng/dialog';
import { ButtonModule } from 'primeng/button';
import { DropdownModule } from 'primeng/dropdown';
import { ProgressBarModule } from 'primeng/progressbar';
import { InputTextModule } from 'primeng/inputtext';
import { FileUploadModule } from 'primeng/fileupload';
import { ToolbarModule } from 'primeng/toolbar';
import { RatingModule } from 'primeng/rating';
import { RadioButtonModule } from 'primeng/radiobutton';
import { InputNumberModule } from 'primeng/inputnumber';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ConfirmationService } from 'primeng/api';
import { MessageService } from 'primeng/api';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { ModelPortfolioParentComponent } from './model-portfolio-parent/model-portfolio-parent.component';
import { OurMarketViewComponent } from './our-market-view/our-market-view.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { TermsConditionsComponent } from './terms-conditions/terms-conditions.component';
import { FaqComponent } from './faq/faq.component';
import { CreatePortfolioStocksSelectionComponent } from './create-portfolio-stocks-selection/create-portfolio-stocks-selection.component';
import { PersonalisationComponent } from './personalisation/personalisation.component';
import { FactorAnalysisComponent } from './factor-analysis/factor-analysis.component';
import { AnalystTargetPriceComponent } from './analyst-target-price/analyst-target-price.component';
import { ValuationMultiplesComponent } from './valuation-multiples/valuation-multiples.component';
import { SparkLineComponent } from './spark-line/spark-line.component';
import { SparkLine2Component } from './spark-line2/spark-line2.component';
import { PageService } from './service/page.service';
import { OrderModule } from 'ngx-order-pipe';
import { HighchartsChartModule } from 'highcharts-angular';
import { BlogsReadmoreComponent } from './blogs-readmore/blogs-readmore.component';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { StockInfoComponent } from './stock-info/stock-info.component';
import { FuturesOptionsComponent } from './futures-options/futures-options.component';
import { OurPortfolioComponent } from './our-portfolio/our-portfolio.component';
import { RiskOverViewComponent } from './risk-over-view/risk-over-view.component';
import { StocksChartsComponent } from './stocks-charts/stocks-charts.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { ScreenerComponent } from './screener/screener.component';
import { FusionChartsModule } from 'angular-fusioncharts';
import * as FusionCharts from 'fusioncharts';
import * as Charts from 'fusioncharts/fusioncharts.charts';
import { PortfolioReturnsComponent } from './portfolio-returns/portfolio-returns.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { OwlModule } from 'ngx-owl-carousel'; 
import { DailyMarketDataScrollComponent } from './daily-market-data-scroll/daily-market-data-scroll.component';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { DashboardSummaryComponent } from './dashboard-summary/dashboard-summary.component';
import { OurPerformanceComponent } from  './our-performance/our-performance.component';
import { OurPortfolioPerformanceComponent } from  './our-portfolio-performance/our-portfolio-performance.component';
import { WizardComponent,WizardDialogComponent,WizardDialogTechnicalComponent,WizardSavePortfolio,WizardDialogQuantComponent } from './wizard/wizard.component'
import { OverlayPanelModule } from 'primeng/overlaypanel';
import { registerLocaleData } from '@angular/common';
import { TooltipModule } from 'ng2-tooltip-directive';
import localeIn from '@angular/common/locales/en-IN';
import { DisclaimerComponent } from './disclaimer/disclaimer.component';
import { NgxCookieConsentModule} from '@lacosanostra/ngx-cookie-consent';
import { SocialLoginModule, SocialAuthServiceConfig } from 'angularx-social-login';
import { GoogleLoginProvider,FacebookLoginProvider } from 'angularx-social-login';
import { JwtModule } from "@auth0/angular-jwt";
import { AuthGuardService } from './service/auth-guard.service';
import { AuthService } from './service/auth.service';
import { CookieService } from 'ngx-cookie-service'
import { TokenInterceptor } from './interceptors/token.interceptor';
import { NgxCaptchaModule } from 'ngx-captcha';
import bulletChart from 'highcharts/modules/bullet';
import * as Highcharts from 'highcharts';
bulletChart(Highcharts);
import { ConfirmOrderComponent } from './confirm-order/confirm-order.component';
import { SubscriptionInformationComponent } from './subscription-information/subscription-information.component';
import {StockProfilerComponent} from './stock-profiler/stock-profiler.component';
import { PricingDetailsComponent,SubscriptionDialogComponent } from './pricing-details/pricing-details.component';
import { SubscriptionExpiredComponent } from './subscription-expired/subscription-expired.component';
import { SubscriptionUpgradeComponent } from './subscription-upgrade/subscription-upgrade.component';
import { SubscriptionFreeComponent } from './subscription-free/subscription-free.component';
import {SubscriptionStatusPageComponent} from './subscription-status-page/subscription-status-page.component';
import {OrderSuccessFailureComponent} from './order-success-failure/order-success-failure.component';
import { PasswordResetPageComponent } from './password-reset-page/password-reset-page.component';
import { Page401Component } from './page401/page401.component';
import { Page402Component } from './page402/page402.component';
import { Page403Component } from './page403/page403.component';
import { Page404Component } from './page404/page404.component';
import { Page405Component } from './page405/page405.component';
import { Page406Component } from './page406/page406.component';
import { Page412Component } from './page412/page412.component';
import { Page500Component } from './page500/page500.component';
import { Page501Component } from './page501/page501.component';
import { Page502Component } from './page502/page502.component';
import { EmailVerificationStatusComponent } from './email-verification-status/email-verification-status.component';
import { EmailVerificationComponent } from './email-verification/email-verification.component';
import { MyAlertComponent } from './my-alert/my-alert.component'
import { NgParticlesModule } from 'ng-particles';
import { MySubscriptionComponent } from './my-subscription/my-subscription.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { SitemapComponent } from './sitemap/sitemap.component';


export function tokenGetter(){
  return localStorage.getItem('token');
}

registerLocaleData(localeIn);
FusionChartsModule.fcRoot(FusionCharts, Charts)
@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FooterComponent,
    BodyComponent,
    LoginComponent,
    SignupComponent,
    BlogComponent,
    PasswordResetComponent,
    AboutusComponent,
    ScrollTopComponent,
    MarketComponent,
    AboutusTeamComponent,
    AboutusVisionMissionComponent,
    InvestmentPhilosophyComponent,
    FloatingIconsComponent,
    FreeTrailComponent,
    FeedbackComponent,
    ETFmonitorComponent,
    StrategiesComponent,
    PortfolioDetailsComponent,
    ModelPortfoliosComponent,
    ModelPortfolioParentComponent,
    OurMarketViewComponent,
    ContactUsComponent,
    PrivacyPolicyComponent,
    TermsConditionsComponent,
    FaqComponent,
    CreatePortfolioStocksSelectionComponent,
    PersonalisationComponent,
    FactorAnalysisComponent,
    ValuationMultiplesComponent,
    AnalystTargetPriceComponent,
    SparkLine2Component,
    SparkLineComponent,
    BlogsReadmoreComponent,
    StockInfoComponent,
    FuturesOptionsComponent,
    OurPortfolioComponent,
    RiskOverViewComponent,
    StocksChartsComponent,
    ScreenerComponent,
    PortfolioReturnsComponent,
    DailyMarketDataScrollComponent,
    DashboardSummaryComponent,
    OurPerformanceComponent,
    WizardComponent,
    WizardDialogComponent,
    WizardDialogTechnicalComponent,
    WizardSavePortfolio,
    WizardDialogQuantComponent,
    OurPortfolioPerformanceComponent,
    DisclaimerComponent,
    CookiesComponent,LogoutDialogComponent,SuccessDialogComponent,
    PricingDetailsComponent,
    SubscriptionInformationComponent,
    ConfirmOrderComponent,
    SubscriptionDialogComponent,
    SubscriptionExpiredComponent,
    SubscriptionUpgradeComponent,
    SubscriptionFreeComponent,
    SubscriptionStatusPageComponent,
    OrderSuccessFailureComponent,
    PasswordResetPageComponent,
    StockProfilerComponent,
    MyAlertComponent,
    Page401Component,
    Page402Component,
    Page403Component,
    Page404Component,
    Page405Component,
    Page406Component,
    Page412Component,
    Page500Component,
    Page501Component,
    Page502Component,
    EmailVerificationStatusComponent,
    EmailVerificationComponent,
    MySubscriptionComponent,
    ChangePasswordComponent,
    SitemapComponent
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    FlexLayoutModule,
    MatSelectCountryModule.forRoot('de'),
    HttpClientModule,
    AppRoutingModule,
    NgxPaginationModule,
    ChartModule,
    FormsModule,
    ReactiveFormsModule,
    NgxSliderModule,
    TableModule,
    CalendarModule,
		SliderModule,
		DialogModule,
		MultiSelectModule,
		ContextMenuModule,
		DropdownModule,
		ButtonModule,
		ToastModule,
    InputTextModule,
    ProgressBarModule,
    HttpClientModule,
    FileUploadModule,
    ToolbarModule,
    RatingModule,
    RadioButtonModule,
    InputNumberModule,
    ConfirmDialogModule,
    InputTextareaModule,
    OrderModule,
    HighchartsChartModule,
    NgxMatSelectSearchModule,
    CarouselModule,
    FusionChartsModule,
    NgMultiSelectDropDownModule,
    NgSelectModule,
    OwlModule,
    MatDatepickerModule,
    MatNativeDateModule,
    OverlayPanelModule,
    TooltipModule,
    NgxCaptchaModule,
    NgxCookieConsentModule,
    SocialLoginModule,
    NgParticlesModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        allowedDomains: [],
        disallowedRoutes: [],
      },
    }),
  ],
  providers: [PageService,
    AuthGuardService,
    AuthService,
    MessageService,
    ConfirmationService,
    CookieService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
    {
      provide: 'SocialAuthServiceConfig',
      useValue: {
        autoLogin: false,
        providers: [
          {
            id: GoogleLoginProvider.PROVIDER_ID,
            provider: new GoogleLoginProvider('193056749460-mqi37qvdkriook8goahl3pli3bv21ts6.apps.googleusercontent.com')
          },
          {
            id: FacebookLoginProvider.PROVIDER_ID,
            provider: new FacebookLoginProvider('clientId')
          }
        ]
      } as SocialAuthServiceConfig,
    }],
  bootstrap: [AppComponent]
})
export class AppModule { 
  
}
