import { Component, OnInit,Injectable } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute,Router } from '@angular/router';
import { AppComponent } from '../app.component';
import { PageService } from '../service/page.service';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { NavbarComponent } from '../navbar/navbar.component';
import { Location } from '@angular/common';
import { Sort } from '@angular/material/sort';
import { Title, Meta } from '@angular/platform-browser';
@Component({
  selector: 'app-futures-options',
  templateUrl: './futures-options.component.html',
  styleUrls: ['./futures-options.component.scss'],
})
export class FuturesOptionsComponent implements OnInit {
  country : string;
  labels: any = [];
  startdates: any = [];
  enddates: any = [];
  stockOptions: any = [];
  indexOptions: any = [];
  newStockOptionSource = []
  currentTab =0;
  stockOptionsColumns: string[] = ['Security_Code', 'sector', 'begin_price', 'High_Probability_Range', 'Remark_1', 'signal', 'end_price', 'Signal_Efficiency'];
  indexOptionsColumns: string[] = ['index', 'begin_price', 'high_probability_range', 'remark_1', 'end_price', 'signal_efficiency'];
  symbols = {'India':'en-IN','US':'en-US'}
  currencysymbols = {'India':'₹','US':'$'}
  currencysymbol:string;
  symbol:string;
  errorMsg: string;
  loading: boolean = true;
  contentFlag = true; errorFlag = false;

  constructor(private titleService: Title, private metaService: Meta,  private  pageService: PageService,private route:ActivatedRoute,public myApp:AppComponent,public router:Router,private http: HttpClient,private location: Location,
    private navbar:NavbarComponent) { }
  indexdatasource = new MatTableDataSource();
  stockdatasource = new MatTableDataSource();
  private getServerErrorMessage(error: HttpErrorResponse): string {
    switch (error.status) {
        case 401: {
            return '401';
        }
        case 402: {
            return '402';
        }
        case 403: {
            return '403';
        }
        case 404: {
          return '404';
        }
        case 405: {
          return '405';
        }
        case 406: {
          return '406';
        }
        case 412: {
          return '412';
        }
        case 500: {
          return '500';
        }
        case 501: {
          return '501';
        }
        case 502: {
          return '502';
        }
        default: {
            return '500';
        }
    }
  }
  ngOnInit() {
    this.titleService.setTitle('Futures & Options');
    this.metaService.addTags([
      {name: 'keywords', content: '72piF&Osignal ,72piFnOsignal ,72piFnOstockrange ,72piF&Ostockrange ,72piF&Ostock1monthrange ,72piF&Ostockneartermrange ,72piF&Ostock1Mrange ,72piFnOstock1monthrange ,72piFnOstockneartermrange ,72piFnOstock1Mrange ,72piFnOhighprobabilityrange ,72piF&Ohighprobabilityrange,F&Osignal ,FnOsignal ,FnOstockrange ,F&Ostockrange ,F&Ostock1monthrange ,F&Ostockneartermrange ,F&Ostock1Mrange ,FnOstock1monthrange ,FnOstockneartermrange ,FnOstock1Mrange ,FnOhighprobabilityrange ,F&Ohighprobabilityrange'},
      {name: 'description', content: "It uses an in-house proprietary model and provides a 'high probability price range's for F&O stocks till the current monthly expiry."},
    ]);

    window.scroll(0,0);
    let element = document.querySelector('.navbar-inverse');
    element.classList.remove('navbar');
    this.myApp.imgname = 0;
    //This is get the data from database on pageload
    this.country = this.route.snapshot.params.country;
    this.symbol = this.symbols[this.country]
    this.currencysymbol = this.currencysymbols[this.country]
    this.myApp.setDefaultValue(this.country)
    this.pageService.getFuturesAnsOptionsData(this.country)
   .subscribe(response=>{
    this.titleService.setTitle('Futures & Options');
    this.contentFlag = true;
    this.errorFlag = false;
      this.stockOptions = response['stockoptions'];
      for(let i=0; i <this.stockOptions.length; i++){
        this.stockOptions[i]['stockdatasource']=new MatTableDataSource(this.stockOptions[i]['stockdatasource']);
        this.stockOptions[i]['indexdatasource']=new MatTableDataSource(this.stockOptions[i]['indexdatasource']);
        this.loading = false;
      }}, error => {
        if (error.error instanceof ErrorEvent) {
          this.errorMsg = `Error: ${error.error.message}`;
        }
        else {
          this.contentFlag = false;
          this.errorFlag = true;
          this.errorMsg = this.getServerErrorMessage(error);
        }
      }
    );
  }
  sortData(sort: Sort){
    if (sort.active && sort.direction !== '')
        {
          this.stockOptions[this.currentTab]['stockdatasource']['data'] = this.stockOptions[this.currentTab]['stockdatasource']['data'].sort((a, b) => {     
          const isAsc = (sort.direction === 'asc');      
          return this._compare(a[sort.active], b[sort.active], isAsc);
          });    
        }
  }
  private _compare(a: number | string, b: number | string, isAsc: boolean) {
    if (typeof a == "string" && a.includes(','))
      a = a.replace(/,/g,"")
    if (typeof b == "string" && b.includes(','))
      b = b.replace(/,/g,"")
    return (a> b ? 1 : -1) * (isAsc ? 1 : -1);
  }
  onTabChange(tab)
    {
      this.currentTab = tab.index;
    }
  getStockInfopage(selectedStock,selectedSymbol){
    let pagename = "/StockInfoPage/";
    this.router.navigate([]).then(result => {  window.open( this.country + pagename + "?stockname=" + (selectedStock+" ("+selectedSymbol+")").split("&").join("`") , '_blank','noopener') });
  }
}