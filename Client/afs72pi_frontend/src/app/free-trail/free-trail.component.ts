// import { Component, OnInit } from '@angular/core';

// @Component({
//   selector: 'app-free-trail',
//   templateUrl: './free-trail.component.html',
//   styleUrls: ['./free-trail.component.scss']
// })
// export class FreeTrailComponent implements OnInit {

//   constructor() { }
//   showMyContainer: boolean = true;
//   ngOnInit(): void {
//   }

// }
import { Component, Inject, OnInit, HostListener } from '@angular/core';
import { Observable } from '../../../node_modules/rxjs';
import { Country } from '@angular-material-extensions/select-country';
import { ActivatedRoute, Router } from '@angular/router';
import { PageService } from '../service/page.service';
import { UserPortfolioService } from '../service/userPortfolios.service';
import { AuthService } from '../service/auth.service';
import { AppComponent } from '../app.component';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

interface MatSelectCountryModule {
  name: string;
  value: string;
  alpha2Code: string;
  alpha3Code: string;
  numericCode: string;
}
@Component({
  selector: 'app-free-trail',
  templateUrl: './free-trail.component.html',
  styleUrls: ['./free-trail.component.scss']
})
export class FreeTrailComponent implements OnInit {
  public imgname: number;
  public portfolioName: string;
  public modelPortfolioName: string;
  public selectedCountry: string = 'India';
  public stocknames; 
  public placeholder; 
  public selectedStock;
  showMyContainer: boolean = false;
  constructor(public myApp: AppComponent, private router: Router, private route: ActivatedRoute,
    public dialog: MatDialog,
    private pageService: PageService, private userService: UserPortfolioService, private authService: AuthService,) { }
  LoginStatus$: Observable<boolean>;
  ngOnInit(): void {
    this.LoginStatus$ = this.authService.isLoggedIn;
    this.getStockList(this.selectedCountry);
    this.myApp.imgname = this.userService.getBodyFunction();
    this.imgname = this.myApp.imgname;
  }
  @HostListener('window:scroll', ['$event'])
  onWindowScroll(e) {
    if (window.pageYOffset === 0) {
      this.imgname = 1;
    } else {
      this.imgname = 0;
    }
  }
  getStockList(country_name) {
    this.pageService.getStocksListSearch(country_name).subscribe(subscribedData => {
      this.stocknames = subscribedData['companies'];
      this.placeholder = subscribedData['placeholder']
    });
  }
  getStockInfopageGlobal(selectedStock) {
    this.showMyContainer=false;
    let pagename = "/StockInfoPage/";
    this.router.navigate([]).then(result => { window.open(this.selectedCountry + pagename + "?stockname=" + selectedStock.fs_name, '_blank'); });
  }
  predefinedCountries: MatSelectCountryModule[] = [
    {
      name: 'INDIA',
      value: 'India',
      alpha2Code: 'IN',
      alpha3Code: '',
      numericCode: '356'
    },
    {
      name: 'US',
      value: 'US',
      alpha2Code: 'US',
      alpha3Code: '',
      numericCode: '840'
    },
  ];

  defaultValue: MatSelectCountryModule = {
    name: 'INDIA',
    value: 'India',
    alpha2Code: 'IN',
    alpha3Code: '',
    numericCode: '356'
  };
  setDefaultValue(country) {
    let countryNames = { 'India': ['INDIA', 'IN', '356'], 'US': ['US', 'US', '840'], }
    this.defaultValue = {
      name: countryNames[country][0],
      value: country,
      alpha2Code: countryNames[country][1],
      alpha3Code: '',
      numericCode: countryNames[country][2]
    };
    this.getStockList(country)
    this.selectedCountry = country;
  }
  onCountrySelected($event: Country) {
    this.selectedCountry = $event['value'];
    this.getStockList(this.selectedCountry)
    this.userService.getPortfoliosList(this.selectedCountry)
    this.onRefresh();
  }
  toTeam() {
    window.scroll({
      top: 3070,
      behavior: 'smooth'
    });
  }
  toAbout() {
    window.scroll({
      top: 0,
      behavior: 'smooth'
    });
  }

  onRefresh() {
    this.router.routeReuseStrategy.shouldReuseRoute = function () { return false; };
    let currentUrl = this.router.url.split("?")[0] + '?';
    let urlparts = currentUrl.split("/");
    if (urlparts[1] != '?') {
      urlparts[1] = this.selectedCountry;
      currentUrl = urlparts.join("/")
    }
    this.router.navigateByUrl(currentUrl)
      .then(() => {
        this.router.navigated = false;
        this.router.navigate([this.router.url]);
      });
  }
}

