import { Component, OnInit} from '@angular/core';
import { PageService } from '../service/page.service';
import { ConfirmationService } from 'primeng/api';
import { MessageService } from 'primeng/api';
import { AppComponent } from '../app.component';
import { ActivatedRoute, Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { SuccessDialogComponent } from '../navbar/navbar.component';
import { MatDialog } from '@angular/material/dialog';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { NavbarComponent } from '../navbar/navbar.component';
import { Title, Meta } from '@angular/platform-browser';
@Component({
  selector: 'app-screener',
  templateUrl: './screener.component.html',
  styleUrls: ['./screener.component.scss'],
})
@Injectable({
  providedIn: 'root'
})
export class ScreenerComponent implements OnInit {
  quantFlag=1;fundFlag=0;techFlag=0;riskFlag=0;
  filtersDailog: boolean;
  filters;
  submitted;
  country: string;
  screener_data_main;
  quantitative_list; quantitative_keys; quantitative_names;
  fundamental_list; fundamental_keys; fundamental_names
  risk_list; risk_keys; risk_names;
  technical_list; technical_keys; technical_names;
  screener_table_data;
  selectedfilterlist = [];
  cols;
  selectedVals;
  def_amnt = 100000; symbol = "₹";
  symbols = { 'India': 'en-IN', 'US': 'en-US' }
  currency_symbol: string;
  selectedstocks: any = [];
  NewPortfolioDialog: boolean;
  AddExisitingDialog: boolean;
  newportfolioname: string;
  selectedPortfolio;
  userportfolios: any;
  filteredValuesLength: number;
  loading: boolean = true;
  divloading: boolean;
  message;
  errorMsg: string;
  status;
  contentFlag = true; errorFlag = false;

  constructor(private titleService: Title, private metaService: Meta, private navbar:NavbarComponent,private pageService: PageService,
    public dialog: MatDialog,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    public myApp: AppComponent,
    private activeRoute: ActivatedRoute, private router: Router,private http: HttpClient) { }
  createPortfolio() {
    this.NewPortfolioDialog = true;
  }

  resetFilters() {
    this.selectedfilterlist = [];
    this.getfiltersdata(1);
  }

  private getServerErrorMessage(error: HttpErrorResponse): string {
    switch (error.status) {
        case 401: {
            return '401';
        }
        case 402: {
            return '402';
        }
        case 403: {
            return '403';
        }
        case 404: {
          return '404';
        }
      
        case 405: {
          return '405';
        }
        case 406: {
          return '406';
        }
        case 412: {
          return '412';
        }
        case 500: {
          return '500';
        }
        case 501: {
          return '501';
        }
        case 502: {
          return '502';
        }
        default: {
            return '500';
        }

    }
  }
  saveCreatePortfolio() {
    let data = { 'stocks': this.selectedstocks, 'portfolioName': this.newportfolioname, 'addToExisting': false }
    this.pageService.saveCustomerPortfolio(data, this.country).subscribe(response => {
      this.titleService.setTitle('Portfolio Builder');
      this.message = response['message'];
      this.status = response['status'];
      const timeout = 1600;
      const dialogRef = this.dialog.open(SuccessDialogComponent, {
        width: '500px',
        data: { message: this.message, status: this.status },
      });
      dialogRef.afterOpened().subscribe((_) => {
        setTimeout(() => {
          dialogRef.close();
        }, timeout);
      });
    }, error => {
      if (error.error instanceof ErrorEvent) {
        this.errorMsg = `Error: ${error.error.message}`;
      }
      else {
        alert(error.error.message)
      }
    });
    this.messageService.add({ severity: 'success', summary: 'Successfull', detail: 'Portfolio Created Successfully!!', life: 1500 });
    this.NewPortfolioDialog = false;
 
  }
  addToExistPortfolio() {
    this.AddExisitingDialog = true;
  }
  saveAddToExistPort() {
    let data = { 'stocks': this.selectedstocks, 'portfolioName': this.selectedPortfolio.portfolio_name, 'addToExisting': true }
    this.pageService.saveCustomerPortfolio(data, this.country).subscribe(response => {
      this.titleService.setTitle('Portfolio Builder');
      this.message = response['message'];
      this.status = response['status'];
      const timeout = 1600;
      const dialogRef = this.dialog.open(SuccessDialogComponent, {
        width: '500px',
        data: { message: this.message, status: this.status },
      });
      dialogRef.afterOpened().subscribe((_) => {
        setTimeout(() => {
          dialogRef.close();
        }, timeout);
      });
    }, error => {
      if (error.error instanceof ErrorEvent) {
        this.errorMsg = `Error: ${error.error.message}`;
      }
      else {
        alert(error.error.message)
      }
    });
    this.messageService.add({ severity: 'success', summary: 'Successfull', detail: 'Stocks Added Successfully!!', life: 1500 });
    this.AddExisitingDialog = false;
  }
  ngOnInit(): void {
    this.titleService.setTitle('Portfolio Builder');
    this.metaService.addTags([
      {name: 'keywords', content: '72piportfoliocreator ,72pistockscreener ,72piportfoliogenerator ,72piportfolioconstruction,portfoliocreator ,stockscreener ,portfoliogenerator ,portfolioconstruction'},
      {name: 'description', content: 'To create portfolio of stocks by selecting stocks manually, using filters on technical/fundamental factors or by excel '},
    ]);
    window.scroll(0, 0);
    let element = document.querySelector('.navbar-inverse');
    element.classList.remove('navbar');
    this.myApp.imgname = 0;
    this.country = this.activeRoute.snapshot.params.country
    this.myApp.setDefaultValue(this.country)
    this.currency_symbol = this.symbols[this.country]
    this.pageService.getScreenerData(this.country, []).subscribe(subscribedData => {
      this.titleService.setTitle('Portfolio Builder');
      this.contentFlag = true;
      this.errorFlag = false;
      this.selectedVals = subscribedData['selectedVals']
      this.userportfolios = subscribedData['user_portfolios']
      this.screener_data_main = subscribedData;
      let iterateTabs = ['quantitative', 'technical', 'risk', 'fundamental']
      for (var i = 0; i < iterateTabs.length; i++) {
        this[iterateTabs[i] + '_list'] = this.screener_data_main[iterateTabs[i] + "_list"];
        this[iterateTabs[i] + '_keys'] = this.screener_data_main[iterateTabs[i] + "_keys"];
        this[iterateTabs[i] + '_names'] = this.screener_data_main[iterateTabs[i] + "_names"];
      }
      this.screener_table_data = subscribedData['screener_data']
      this.filteredValuesLength = this.screener_table_data.length;
      this.cols = subscribedData['colnames_dict']
      if (this.country == 'US') {
        this.def_amnt = 5000;
        this.symbol = "$"
      }
      this.loading = false;
    },error => {
      if (error.error instanceof ErrorEvent) {
        this.errorMsg = `Error: ${error.error.message}`;
      }
      else {
        this.contentFlag = false;
        this.errorFlag = true;
        this.loading = false;
        this.errorMsg = this.getServerErrorMessage(error);
      }
    }
    );
  }

  onTabChange(tab)
  {
    let tabs = {0:'quantFlag',1:'fundFlag',2:'techFlag',3:'riskFlag'}
    for (let value of Object.keys(tabs))
      {
        if (value==tab.index)
          this[tabs[value]]=1
        else
          this[tabs[value]] = 0
      }
      
  }

  onFilter(event, dt) { this.filteredValuesLength = event.filteredValue.length; }


  modifyQuantity(event): void {
    for (let i = 0; i < this.screener_table_data.length; i++) {
      this.screener_table_data[i].Quantity = (event.value / this.screener_table_data[i]['Price']).toFixed(0);
    }
    this.screener_table_data = [...this.screener_table_data];
  }
  onFilterChange(event, factor) {
    let condition_val = event.target.value;
    let filterExist = 0;
    // event.target.style.backgroundColor = '#FDCA40';

    for (let i = 0; i < this.selectedfilterlist.length; i++) {
      if (this.selectedfilterlist[i]['factor'] == factor) {
        if (condition_val != 'Any') {
          this.selectedfilterlist[i].condition = condition_val;
          filterExist = 1;
          // event.target.style.backgroundColor = '#FDCA40';
          break;
        }
        else {
          this.selectedfilterlist.splice(i, 1);
          // event.target.style.backgroundColor = 'transparent';
          break;
        }
      }
    }
    if (filterExist == 0 && condition_val != 'Any') {
      this.selectedfilterlist.push({ 'factor': factor, 'condition': condition_val })

    }
  }
  openFiltersDailog() {
    this.submitted = false;
    this.filtersDailog = true;
  }
  getfiltersdata(resetFilters = 0) {
    this.divloading = true;
    this.pageService.getScreenerData(this.country, this.selectedfilterlist).subscribe(subscribedData => {
      this.screener_table_data = subscribedData['screener_data']
      this.filteredValuesLength = this.screener_table_data.length;
      if (resetFilters == 1)
        this.selectedVals = subscribedData['selectedVals']
      this.divloading = false
    });
  }
  hideDialog() {
    this.NewPortfolioDialog = false;
    this.AddExisitingDialog = false;
    this.submitted = false;
  }
  getStockInfopage(selectedStock, selectedSymbol) {
    let pagename = "/StockInfoPage/";
    this.router.navigate([]).then(result => { window.open(this.country + pagename + "?stockname=" + (selectedStock + " (" + selectedSymbol + ")").split("&").join("`"), '_blank','noopener') });
  }

}
