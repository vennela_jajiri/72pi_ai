import { Component, ViewChild, OnInit } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { AppComponent } from '../app.component';
import { PageService } from '../service/page.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Title, Meta } from '@angular/platform-browser';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-our-portfolio',
  templateUrl: './our-portfolio.component.html',
  styleUrls: ['./our-portfolio.component.scss'],
})
export class OurPortfolioComponent implements OnInit {
  country: String;
  activeData: any = [];
  additionData: any = []
  exitData: any = [];
  tradeSignals: any = [];
  tradeIdeas: any = [];
  showMore = false;
  loading: boolean = true;
  symbols = { 'India': 'en-IN', 'US': 'en-US' }
  currencysymbols = { 'India': '₹', 'US': '$' }
  symbol: string;
  isCollapsed;
  currencysymbol: string;
  contentFlag = true; errorFlag = false;
  errorMsg: string;

  constructor(private titleService: Title, private metaService: Meta, private pageService: PageService, private route: ActivatedRoute, public myApp: AppComponent, private router: Router) { }
  displayColumnsnames = {
    'ADDITIONS': ['Company', 'Sector', 'Date of Purchase', 'Purchase Price', 'Current Price'],
    'EXITS': ['Company', 'Sector', 'Date of Sell', 'Purchase Price', 'Sell Price', 'Current Price']
  }
  accessColumnsNames = {
    'ADDITIONS': ['stock_name', 'sector_name', 'date', 'buy_price', 'price'],
    'EXITS': ['stock_name', 'sector_name', 'date', 'buy_price', 'sell_price', 'price']
  }
  displayColumns = ['Company', 'Sector', 'Date of Purchase', 'Purchase Price', 'Current Price'];
  accessColumns = ['stock_name', 'sector_name', 'date', 'buy_price', 'price'];
  dataSource = new MatTableDataSource();
  @ViewChild(MatSort) sort: MatSort;
  
  tabChanged($event) {
    if ($event.tab.textLabel == 'ADDITIONS') {
      this.dataSource = new MatTableDataSource(this.additionData);
      this.displayColumns =  this.displayColumnsnames['ADDITIONS']
      this.accessColumns = this.accessColumnsNames['ADDITIONS']
      this.dataSource.sort = this.sort;
    }
    else if ($event.tab.textLabel == 'EXITS') {
      this.displayColumns =  this.displayColumnsnames['EXITS']
      this.accessColumns = this.accessColumnsNames['EXITS']
      this.dataSource = new MatTableDataSource(this.exitData);
      this.dataSource.sort = this.sort;
    }
    else {
      this.displayColumns =  this.displayColumnsnames['ADDITIONS']
      this.accessColumns = this.accessColumnsNames['ADDITIONS']
      this.dataSource = new MatTableDataSource(this.activeData);
      this.dataSource.sort = this.sort;
    }
  }
  private getServerErrorMessage(error: HttpErrorResponse): string {
    switch (error.status) {
      case 401: {
        return '401';
      }
      case 402: {
        return '402';
      }
      case 403: {
        return '403';
      }
      case 404: {
        return '404';
      }

      case 405: {
        return '405';
      }
      case 406: {
        return '406';
      }
      case 412: {
        return '412';
      }
      case 500: {
        return '500';
      }
      case 501: {
        return '501';
      }
      case 502: {
        return '502';
      }
      default: {
        return '500';
      }

    }
  }
  ngAfterViewInit() {
    this.dataSource = new MatTableDataSource(this.activeData);
    this.dataSource.sort = this.sort;
  }
  getStockInfopage(selectedStock, selectedSymbol) {
    let pagename = "/StockInfoPage/";
    this.router.navigate([]).then(result => { window.open(this.country + pagename + "?stockname=" + (selectedStock + " (" + selectedSymbol + ")").split("&").join("`"), '_blank','noopener') });
  }
  ngOnInit() {
    this.titleService.setTitle('72PI Portfolio');
    this.metaService.addTags([
      { name: 'keywords', content: '72PIPortfolio ,72PIPortfoliostocks ,72PIstocks ,72PIselectedstocks ,72PIPortfolioshortnotes ,72PIshortnotes ,72PIsituationalanalysis ,Portfolio ,Portfoliostocks ,stocks ,selectedstocks ,Portfolioshortnotes ,shortnotes ,situationalanalysis ' },
      { name: 'description', content: 'A portfolio of stocks selected using fundamental, quantitative, technical and situational analysis from the NSE 500, along with a short note of our view on the selected stocks.' },
    ]);

    window.scroll(0, 0);
    let element = document.querySelector('.navbar-inverse');
    element.classList.remove('navbar');
    this.myApp.imgname = 0;

    //This is get the data from database on pageload
    this.country = this.route.snapshot.params.country;
    this.myApp.setDefaultValue(this.country)
    this.symbol = this.symbols[this.route.snapshot.params.country]
    this.currencysymbol = this.currencysymbols[this.route.snapshot.params.country]
    this.pageService.getOurPortfolioData(this.country).subscribe(response => {
    this.titleService.setTitle('72PI Portfolio');

      this.contentFlag = true;
      this.errorFlag = false;
      this.tradeSignals = response['tradesignals'];
      this.tradeIdeas = response['tradeideas'],
        this.additionData = response['additionsIdeas'],
        this.activeData = response['activeIdeas'],
        this.exitData = response['exitIdeas'],
        this.dataSource = new MatTableDataSource(this.activeData)
      this.dataSource.sort = this.sort;
      this.loading = false
    }, error => {
      if (error.error instanceof ErrorEvent) {
        this.errorMsg = `Error: ${error.error.message}`;
      }
      else {
        this.contentFlag = false;
        this.errorFlag = true;
        this.errorMsg = this.getServerErrorMessage(error);
      }
    });
  }

  getSignalIdeas(idea, signal) {
    if (idea.company_name === signal.company_name)
      return true;
    else
      return false;
  }


}
