import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { AppComponent } from '../app.component';
import { PageService } from '../service/page.service';
export let browserRefresh = false;
import { Title, Meta } from '@angular/platform-browser';


@Component({
  selector: 'app-aboutus-team',
  templateUrl: './aboutus-team.component.html',
  styleUrls: ['./aboutus-team.component.scss']
})
export class AboutusTeamComponent  {
  constructor(private titleService: Title, private metaService: Meta,private router: Router,public myApp: AppComponent,public page:PageService,private activated:ActivatedRoute) {
  }
  element;
  memberName :string;
  indexVal;
  public names:any[];
  ngOnInit(): void {
    this.titleService.setTitle('About Us - Team | 72PI');

    this.memberName = this.activated.snapshot.params.memberName;
    window.scrollTo(0, 0);
    window.scroll(0,0);
    let element = document.querySelector('.navbar-inverse');
    element.classList.remove('navbar');
    this.myApp.imgname = 0;
     this.names =['suresh','madhu']
    this.indexVal = this.names.indexOf(this.memberName)
  }
  counter = 0;
  slideItems = [
    {
      src: 'assets/images/suresh.jpg',
      title: ` 
            <h5 class="tx-36-f  tx-lg-36-f tx-md-34-f tx-sm-32-f tx-xs-30-f tx-bold lh-2">Suresh Raju</h5>
            <p  class="tx-24-f  tx-md-22-f  tx-sm-20-f tx-xs-18-f  tx-semibold pd-t-10-f">Founder, CEO</p>
            <div class="pd-t-10-f ">
                <a href="mailto:suresh@72pi.ai" target="_blank" class="tx-white" >
                    <img  src="/assets/images/email.png" alt="">
                </a>
                <a href="https://www.linkedin.com/in/suresh-raju-4966282/" target="_blank" class="pd-l-15-f">
                    <img  src="/assets/images/linkedin.png" alt="">
                </a>
            </div>`,
      about: `
              <p class="tx-16-f tx-md-14-f  lh-8 mg-t-20-f">
                Suresh Raju founded 72PI to provide insightful equity research ideas and bring
                institutional style portfolio analytics to the individual investor. Suresh and his team
                currently operate a SEBI registered Alternative Investment Fund, Category III, that
                invests in equities and derivatives market. Suresh has over 20 years of extensive
                experience in Investment Management, Capital Markets, Financial Structuring, Valuation,
                Corporate Finance and Due Diligence.
              </p>
              <p class="tx-16-f  tx-md-14-f lh-8 mg-t-20-f">
                Prior to launching 72PI, he founded and operated a company in the financial services space
                to provide equity research, predictive modeling, and analytics support to asset management
                clients, primarily fund managers and hedge funds, in Boston and New York City. He and his
                team have researched and delivered over 1500+ company writeups across sectors and
                international markets to clients in USA. Suresh spent 4 years in the private equity market
                as a General Partner at TVS Capital Funds, evaluating over 600+ companies across consumer
                consumption sectors in India. In his earlier days, Suresh spent time in the Investment
                Banking practice at Deutsche Bank (Boston, USA) and led over 25 transactions from IPOs,
                private placements, debt financings, M&A transactions, LBOs across technology sector.
                Suresh started his career at ABAQUS Inc. (USA) a software company that focused on finite
                element technologies.
              </p>
              <p class="tx-16-f tx-md-14-f lh-8 mg-t-20-f">
                  Suresh received his bachelor's from IIT Madras in 1993 and master's in engineering from
                  Ohio State University in 1995. He received his MBA from University of Chicago Booth School
                  of Business in 2001. Suresh is currently a member of the Indian Angel Network and a
                  Charter Member of TiE, Hyderabad.
              </p>`},
    {
      src: 'assets/images/madhu.jpg',
      title: `
              <h5 class="tx-36-f tx-lg-36-f tx-md-34-f tx-sm-32-f tx-xs-30-f tx-bold lh-2">Madhusudan Subramanian</h5>
              <p class="tx-24-f  tx-md-22-f  tx-sm-20-f tx-xs-18-f tx-semibold pd-t-10-f">Co-Founder, CIO</p>
              <div class="pd-t-10-f">
                  <a href="mailto:madhu@72pi.ai" target="_blank" class="tx-white">
                      <img  src="/assets/images/email.png" alt="">
                  </a>
                  <a href="https://www.linkedin.com/in/madhusudan-subramanian-cfa-caia-89662b4/" target="_blank" class="pd-l-15-f">
                      <img  src="/assets/images/linkedin.png" alt="">
                  </a>
              </div>`,
      about: `
                  <p class="tx-16-f tx-md-14-f lh-8 mg-t-20-f">
                      Madhusudan Subramanian is 72PI's Co-Founder and CIO and is helping identify new
                      analytics features that an individual investor will find useful in managing wealth. He
                      is a strategic and results-driven investment management professional with over 20 years
                      of experience in global investing, quant equities research, risk analytics and niche
                      fin-tech projects.
                    </p>
                    <p class="tx-16-f tx-md-14-f lh-8 mg-t-20-f">
                      He has held senior investment research, product strategy and portfolio analytics roles
                      developing and launching innovative ETFs and actively managed funds at Morgan Stanley,
                      MSCI and Principal Mutual Fund. His responsibilities spanned across core Equity, Fixed
                      Income and Hybrid Fund strategies. Steered key fund strategy and product level decisions
                      and enabled Principal PNB Asset Management to grow over INR 3500 Crores AUM over 2 years
                      through select top-ranked Equity and Hybrid funds. He was Head of Quantitative Equity
                      Research team at MSCI, Morgan Stanley Capital International, an investment research firm
                      that provides stock indexes, portfolio risk and performance analytics, and governance
                      tools to institutional investors and hedge funds. He was responsible for co-leading New
                      Product development.
                    </p>
                    <p class="tx-16-f tx-md-14-f lh-8 mg-t-20-f">
                      Madhusudan graduated with a bachelor's degree in Mechanical Engineering from the
                      National Institute of Technology, Trichy with distinction and received an MBA from the
                      University of Chicago Booth School of Business. He also holds the CFA and CAIA charter
                      designations.
                    </p> `
    }
  ];
  changeURL(finalURL) {
    this.router.navigateByUrl(finalURL)
    .then(() => {
      this.router.navigated = false;
      this.router.navigate([this.router.url]);
    });
  }
  showNextImage() {
    let urlparts = this.router.url.split("/");
    this.indexVal = (this.indexVal + 1) % (this.slideItems.length) ;
    urlparts[urlparts.length-1]  = this.names[this.indexVal]
    let finalURL = urlparts.join("/")
    this.changeURL(finalURL)
  }
  showPreviousImage() {
    let urlparts = this.router.url.split("/");
    this.indexVal = (this.indexVal + 1) % (this.slideItems.length) ;
    urlparts[urlparts.length-1]  = this.names[this.indexVal]
    let finalURL = urlparts.join("/")
    this.changeURL(finalURL)
  }
  team() {
    
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      window.scroll({
        top: 3070,
        behavior: 'smooth'
      });
    });
  }
}