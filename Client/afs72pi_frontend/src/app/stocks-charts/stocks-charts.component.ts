import { PageService } from '../service/page.service';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { Component, OnInit } from '@angular/core';
import { AppComponent } from '../app.component';
import { ActivatedRoute,Router } from '@angular/router';
import { Chart } from 'angular-highcharts';
import { lineChart } from './../../charts/lineChart';
import * as Highcharts from 'highcharts';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { NavbarComponent } from '../navbar/navbar.component';
import { Title, Meta } from '@angular/platform-browser';
@Component({
  selector: 'app-stocks-charts',
  templateUrl: './stocks-charts.component.html',
  styleUrls: ['./stocks-charts.component.scss'],
})
export class StocksChartsComponent implements OnInit {
  contentFlag = true; errorFlag = false;
  stocksList = [];
  list_of_stocks_selected: any=[];
  stockdropdownSettings: IDropdownSettings = {};
  etfList = [];
  etf_benchmarks_list_selected = [];
  ETFdropdownSettings: IDropdownSettings = {};
  country: string;
  period: string = '12M';
  outputViewData: any;
  defaultStock: any;
  total_stocks: any;
  etf_list: any;
  errorMsg: string;
  outputData: any;
  volatility_output;
  rsi_output;
  cumulative_output;
  stocksSelected = [];
  loading : boolean = true;
  divloading : boolean;
  plotLinesData = [
    {
      width: 1,
      value: 70,
      dashStyle: 'dash',
      label: {
        text: 'overbought=70',
        align: 'left',
        x: 10,
      },
    },
    {
      width: 1,
      value: 30,
      dashStyle: 'dash',
      label: {
        text: 'oversold=30',
        align: 'left',
        x: 10,
      },
    },
  ];
  constructor(private titleService: Title, private metaService: Meta,private navbar:NavbarComponent,
    private pageService: PageService,
    private route: ActivatedRoute,
    private myApp: AppComponent,private http: HttpClient,private router: Router
  ) { }
  plotCharts(
    dynamicColumn,
    plotLinesData,
    chartHeight,
    multiplyFactor,
    dividendFactor,
    symbol,
    tooltipSymbol,
    roundDigits,
    shared,
    legends,
    chartTitle = null,
    formatCurrency = '',
    symbol_param = ''
  ) {
    this[dynamicColumn] = new Chart(lineChart);
    this.outputData = this.outputViewData[dynamicColumn];
    (this[dynamicColumn].options.title = {
      text: chartTitle,
      style: {
        color: '#031b4e',
        fontWeight: 'bold',
        fontSize: '14px',
      },
    }),
      (this[dynamicColumn].options = { series: this.outputData });
    this[dynamicColumn].options.xAxis = {
      type: 'datetime',
      tickWidth: 1,
      tickLength: 6,
      startOnTick: false,
      endOnTick: false,
      dateTimeLabelFormats: {
        day: '%e-%b-%y',
        week: '%e-%b-%y',
        month: '%b-%y',
        year: '%Y',
      },
    };
    this[dynamicColumn].options.yAxis = {
      gridLineWidth: 0,
      lineWidth: 1,
      tickLength: 6,
      tickWidth: 1,
      labels: {
        formatter: function () {
          if (['volume_list', 'volatility_output'].includes(dynamicColumn))
            return (this.value / dividendFactor).toFixed(0) + symbol;
          else if (['rsi_output'].includes(dynamicColumn)) return this.value;
          else
            return (
              ((this.value * multiplyFactor) / dividendFactor).toFixed(0) +
              symbol
            );
        },
      },
      title: {
        style: {
          color: '#031b4e',
          font: '18px ',
          fontWeight: 'bold',
          fontFamily:'Poppins'
      },
      },
      plotLines: plotLinesData,
    };
    this[dynamicColumn].options.plotOptions = {
      series: {
        animation: false,
        marker: {
          enabled: false,
          symbol: "circle"
        }
      },
    };
    this[dynamicColumn].options.legend = {
      enabled: legends,
    };
    this[dynamicColumn].options.title = lineChart.title;
    this[dynamicColumn].options.credits = lineChart.credits;
    this[dynamicColumn].options.exporting = lineChart.exporting
    this[dynamicColumn].options.tooltip = {
      formatter: function () {
        var s = ['Date : ' + '<b>' + Highcharts.dateFormat('%d-%b-%Y', this.x)];
        for (let i = 0; i < this.points.length; i++) {
          if (['volume_list', 'volatility_output'].includes(dynamicColumn)) {
            s.push(
              this.points[i].series.name +
              ' : ' +
              '<b>' +
              (this.points[i].point.y / dividendFactor).toFixed(roundDigits) +
              tooltipSymbol +
              '</b>'
            );
          } else if (['rsi_output'].includes(dynamicColumn)) {
            s.push(
              this.points[i].series.name +
              ' : ' +
              '<b>' +
              this.points[i].point.y.toFixed(roundDigits) +
              '</b>'
            );
          } else {
            s.push(
              this.points[i].series.name +
              ' : ' +
              '<b>' +
              (this.points[i].point.y * multiplyFactor).toFixed(roundDigits) +
              tooltipSymbol +
              '</b>'
            );
          }
        }
        return s.join('<br>');
      },
      shared: true,
      style: {
        color: '#031b4e',
        fontSize: '14px',
        fontWeight: 'normal',
      },
    };
    this[dynamicColumn].options.chart = {
      type: 'spline',
      backgroundColor: 'transparent',
      zoomType: 'x',
      height: chartHeight,
    };
  }
  convertDateToUTC(inputdata, dynamicColumn, extraIndex) {
    var data;
    if (extraIndex != -1) data = inputdata[dynamicColumn][extraIndex];
    else data = inputdata[dynamicColumn];
    for (let i = 0; i < data['data'].length; i++) {
      let date = new Date(data['data'][i][0]);
      var now_utc = Date.UTC(
        date.getUTCFullYear(),
        date.getUTCMonth(),
        date.getUTCDate(),
        date.getUTCHours(),
        date.getUTCMinutes(),
        date.getUTCSeconds()
      );
      data['data'][i][0] = now_utc;
    }
    return inputdata;
  }
  private getServerErrorMessage(error: HttpErrorResponse): string {
    switch (error.status) {
        case 401: {
            return '401';
        }
        case 402: {
            return '402';
        }
        case 403: {
            return '403';
        }
        case 404: {
          return '404';
        }
        case 405: {
          return '405';
        }
        case 406: {
          return '406';
        }
        case 412: {
          return '412';
        }
        case 500: {
          return '500';
        }
        case 501: {
          return '501';
        }
        case 502: {
          return '502';
        }
        default: {
            return '500';
        }
    }
  }
  ngOnInit() {
    
    this.titleService.setTitle('Stocks Charts');
    this.metaService.addTags([
      {name: 'keywords', content: 'stockcomparision ,etfcomparision '},
      {name: 'description', content: 'Specific page to compare performance of different stocks against different etfs'},
    ]);
    window.scroll(0, 0);
    let element = document.querySelector('.navbar-inverse');
    element.classList.remove('navbar');
    this.myApp.imgname = 0;
    this.country = this.route.snapshot.params.country;
    this.myApp.setDefaultValue(this.country);
    this.getStockChartPageData(this.country, this.period);
    this.stockdropdownSettings = {
      singleSelection: false,
      idField: 'security_code',
      textField: 'fs_name',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      allowSearchFilter: true,
      enableCheckAll: false,
    };
    this.ETFdropdownSettings = {
      singleSelection: false,
      idField: 'security_code',
      textField: 'fs_name',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      allowSearchFilter: true,
      enableCheckAll: false,
    };
    if(this.country == 'India'){
    this.list_of_stocks_selected = [
      {
        fs_name: 'Reliance Industries Ltd (RELIANCE)',
        security_code: 'RELIANCE',
      },
    ];
  }
  else{
    this.list_of_stocks_selected = [
      {
        fs_name: 'Apple Inc (AAPL)',
        security_code: 'AAPL',
      },
    ];
  }
  }
  totalSelectedStocks() {
    this.divloading = true;
    this.stocksSelected = [];
    for (var i = 0; i < this.list_of_stocks_selected.length; i++) {
      this.stocksSelected.push(this.list_of_stocks_selected[i].security_code);
    }
    for (var i = 0; i < this.etf_benchmarks_list_selected.length; i++) {
      this.stocksSelected.push(
        this.etf_benchmarks_list_selected[i].security_code
      );
    }
    this.getStockChartPageData(this.country, this.period);
  }
  onSelectAll(all) {
    this.list_of_stocks_selected = all;
  }
  Reset() {
    this.divloading = true;

    this.stocksSelected = []
    this.list_of_stocks_selected = []
    if(this.country == 'India'){
      this.stocksSelected.push('RELIANCE')
      this.list_of_stocks_selected=['Reliance Industries Ltd (RELIANCE)']
    } 
    else{
      this.stocksSelected.push('AAPL')
      this.list_of_stocks_selected=['Apple Inc (AAPL)']
    }
    this.etf_benchmarks_list_selected = [];
    this.getStockChartPageData(this.country, this.period);
  }
  getStockChartPageData(country, period) {
    if (this.list_of_stocks_selected.length==0){
      this.stocksSelected.push('AAPL')
    this.list_of_stocks_selected=['Apple Inc (AAPL)']
    if (country=='India'){
      this.stocksSelected.push('RELIANCE')
      this.list_of_stocks_selected=['Reliance Industries Ltd (RELIANCE)']
    }}
    this.pageService.getStockCharts(country,this.stocksSelected,[],period).subscribe((subscribedData) => {
      this.titleService.setTitle('Stocks Charts');
      this.contentFlag = true;
      this.errorFlag = false;
      this.outputViewData = subscribedData;
      this.total_stocks = this.outputViewData['total_stocks'];
      this.stocksList = this.total_stocks;
    
      this.etf_list = this.outputViewData['etf_list'];
      this.etfList = this.etf_list;
      for (let i = 0;i < this.outputViewData['cumulative_output'].length;i++) {
        this.convertDateToUTC(this.outputViewData, 'cumulative_output', i);
      }
      for (let i = 0; i < this.outputViewData['rsi_output'].length; i++) {
        this.convertDateToUTC(this.outputViewData, 'rsi_output', i);
      }
      for (let i = 0;i < this.outputViewData['volatility_output'].length;i++) {
        this.convertDateToUTC(this.outputViewData, 'volatility_output', i);
      }
      this.plotCharts('cumulative_output',[],'',100,1,'%','%',0,true,true);
      this.plotCharts('rsi_output',this.plotLinesData,'',100,1,'%','%',0,true,true);
      this.plotCharts('volatility_output',[],'',100,1,'%','%',0,true,true);
      this.loading = false
      this.divloading = false;

    }, error => {
      if (error.error instanceof ErrorEvent) {
        this.errorMsg = `Error: ${error.error.message}`;
      }
      else {
        this.contentFlag = false;
        this.errorFlag = true;
        this.loading = false;
        this.errorMsg = this.getServerErrorMessage(error);
      }
    });
  }
  tabClick(tab) {
    this.divloading = true;
    if (tab.index == 0) {
      this.period = 'WTD';
    } else if (tab.index == 1) {
      this.period = 'MTD';
    } else if (tab.index == 2) {
      this.period = 'YTD';
    } else if (tab.index == 3) {
      this.period = '12M';
    } else if (tab.index == 4) {
      this.period = '2020';
    } else if (tab.index == 5) {
      this.period = '2019';
    }
    this.getStockChartPageData(this.country, this.period);
  }
}