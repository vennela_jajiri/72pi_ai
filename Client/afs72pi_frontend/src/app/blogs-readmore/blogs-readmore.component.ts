import { Component, OnInit } from '@angular/core';
import { PageService } from '../service/page.service';
import { ActivatedRoute } from '@angular/router';
import { AppComponent } from '../app.component';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-blogs-readmore',
  templateUrl: './blogs-readmore.component.html',
  styleUrls: ['./blogs-readmore.component.scss'],
})
export class BlogsReadmoreComponent implements OnInit {
  blogData: any[];
  site: string;
  selectedBlogTitle;
  image_title;description;
  contentFlag = true; errorFlag = false;
  errorMsg: string;

  constructor(private pageService: PageService,
    private activeRoute: ActivatedRoute, private myApp: AppComponent, private http: HttpClient) { }
    private getServerErrorMessage(error: HttpErrorResponse): string {
      switch (error.status) {
        case 401: {
          return '401';
        }
        case 402: {
          return '402';
        }
        case 403: {
          return '403';
        }
        case 404: {
          return '404';
        }
  
        case 405: {
          return '405';
        }
        case 406: {
          return '406';
        }
        case 412: {
          return '412';
        }
        case 500: {
          return '500';
        }
        case 501: {
          return '501';
        }
        case 502: {
          return '502';
        }
        default: {
          return '500';
        }
  
      }
    }
  ngOnInit(): void {
    this.selectedBlogTitle = this.activeRoute.snapshot.params.blogTitle;
    this.pageService.readMoreFunction(this.selectedBlogTitle).subscribe(response => {
      this.contentFlag = true;
      this.errorFlag = false;
      this.blogData = response['blog'][0];
      this.image_title = this.blogData['image_title'];
      this.description = this.blogData['description'];
      this.site = response['site'];
    }, error => {
      if (error.error instanceof ErrorEvent) {
        this.errorMsg = `Error: ${error.error.message}`;
      }
      else {
        this.contentFlag = false;
        this.errorFlag = true;
        this.errorMsg = this.getServerErrorMessage(error);
      }
    });

    window.scroll(0, 0);
    let element = document.querySelector('.navbar-inverse');
    element.classList.remove('navbar');
    this.myApp.imgname = 0;
  }
}
