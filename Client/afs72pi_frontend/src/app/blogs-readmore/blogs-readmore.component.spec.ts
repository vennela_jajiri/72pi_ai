import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BlogsReadmoreComponent } from './blogs-readmore.component';

describe('BlogsReadmoreComponent', () => {
  let component: BlogsReadmoreComponent;
  let fixture: ComponentFixture<BlogsReadmoreComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BlogsReadmoreComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BlogsReadmoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
