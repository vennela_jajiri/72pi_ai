import { Component } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { NavbarComponent } from '../navbar/navbar.component';


@Component({
  selector: 'app-sitemap',
  templateUrl: './sitemap.component.html',
  styleUrls: ['./sitemap.component.scss'],
})
export class SitemapComponent {

  public country: string;
  constructor(private router: Router, 
              public navComp : NavbarComponent) { }

  ngOnInit() {
    window.scroll(0, -3020);
    let element = document.querySelector('.navbar-inverse');
    element.classList.remove('navbar');
    this.country = this.navComp.selectedCountry;
  }

  toTeam() {
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      window.scroll({
        top: 3020,
        behavior: 'smooth'
      });
    });
  }
}
