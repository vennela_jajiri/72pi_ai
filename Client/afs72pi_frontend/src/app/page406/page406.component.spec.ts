import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Page406Component } from './page406.component';

describe('Page406Component', () => {
  let component: Page406Component;
  let fixture: ComponentFixture<Page406Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Page406Component ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Page406Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
