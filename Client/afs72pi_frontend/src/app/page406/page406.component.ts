import { Component, OnInit } from '@angular/core';
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'app-page406',
  templateUrl: './page406.component.html',
  styleUrls: ['./page406.component.scss'],
})
export class Page406Component implements OnInit {

  constructor(private titleService:Title) {
    this.titleService.setTitle("We are sorry, Browser not supporting!");
  }
  ngOnInit() {
    window.scroll(0,0);
    let element = document.querySelector('.navbar-inverse');
    element.classList.remove('navbar');
  }

}
