import { Component, OnInit, Input, Injectable } from '@angular/core';
import { PageService } from '../service/page.service';
import { AppComponent } from '../app.component';
import { ActivatedRoute, Router } from '@angular/router';
import { catchError } from 'rxjs/operators';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { _throw as throwError } from 'rxjs/observable/throw';
import { NavbarComponent } from '../navbar/navbar.component';
import { Title, Meta } from '@angular/platform-browser';
import * as Highcharts from 'highcharts/highstock';
declare var require: any

const HighchartsMore = require('highcharts/highcharts-more.src');
HighchartsMore(Highcharts);

const HC_solid_gauge = require('highcharts/modules/solid-gauge.src');
HC_solid_gauge(Highcharts);

@Component({
  selector: 'app-stock-info',
  templateUrl: './stock-info.component.html',
  styleUrls: ['./stock-info.component.scss'],
})
export class StockInfoComponent implements OnInit {
  company_name;
  contentFlag = true; errorFlag = false;
  security_code;
  show = true;
  loading = true;
  showReadMore = true;
  stringToShow;
  text;
  flag = 0;
  length = 1100;
  isCollapsed = true;
  img_url;
  Highcharts = Highcharts;
  public charts: any = [];
  outputViewData: any;
  Chart: any;
  country: string;
  companyName: string = '';
  companyDescription: any;
  keyInformation: any;
  factorDataSource: any;
  stock_ranking_data: any;
  NiftyCheck: any;
  NiftyMipcapCheck: any;
  NiftySmallcapCheck: any;
  priceCheck: any;
  MA9Check: any;
  Price;MA9;
  MA20Check: any;
  MA50Check: any;
  outputData: any;
  RSIDiv: boolean = true;
  VolatilityDiv: boolean = false;
  stockname;
  stockName;
  moving_average_main_data;
  displayedColumns: string[] = ['Factor', 'Value'];
  moving_average_clicks = ['MA200', 'MA50', 'Price'];
  updateFlag = false;
  chart;
  chartConstructor = 'chart';
  chartCallback;
  movingAverageOptions;
  stockPriceOptions;
  volumeOptions;
  rsiOptions;
  volatilityOptions;
  stockIndexMainData;
  stockIndexOptions;
  stockIndexClicks = [];
  stockIndexFlag = false;
  chartOptions;
  value = 0;
  plotLinesData = [
    {
      width: 1,
      value: 70,
      dashStyle: 'dash',
      label: {
        text: 'overbought=70',
        align: 'left',
        x: 10,
      },
    },
    {
      width: 1,
      value: 30,
      dashStyle: 'dash',
      label: {
        text: 'oversold=30',
        align: 'left',
        x: 10,
      },
    },
  ];
  headings: string[] = [
    'Size',
    'Valuation',
    'Growth',
    'Profitability',
    'Leverage',
    'Risk',
    'Piotroski Score',
  ];
  benchmark_headers = {
    India: [
      'Nifty 50 Index',
      'Nifty Midcap 100 Index',
      'Nifty Smallcap 100 Index',
    ],
    US: ['S&P 500', 'Dow Jones Industrial Average', 'Russell 2000'],
  };
  factorColumns: string[] = ['Factor', 'Value'];
  currency_symbol: string;
  quality_score;
  growth_score;
  technical_score;
  indv_quality_scores: any[];
  indv_growth_scores: any[];
  indv_technical_scores: any[];
  week_52_prices: any;
  StockInfoPage;
  errorMsg: string;
  constructor(
    private titleService: Title, private metaService: Meta, private navbar: NavbarComponent,
    private pageService: PageService,
    private route: ActivatedRoute,
    private myApp: AppComponent,
    private router: Router,
    private http: HttpClient
  ) {
    const self = this;
    this.chartCallback = (chart) => {
      self.chart = chart;
    };
  }
  addDisableSeries(value) {
    if (
      this.moving_average_clicks.includes(value) &&
      this.moving_average_clicks.indexOf(value) !== -1
    ) {
      this.moving_average_clicks.splice(
        this.moving_average_clicks.indexOf(value),
        1
      );
    }
    else {
      this.moving_average_clicks.push(value);
    }
    let source_data = JSON.parse(JSON.stringify(this.moving_average_main_data));
    let updatedChartData = [];
    this.updateFlag = false;
    for (let i = 0; i < source_data.length; i++) {
      let flag = 0;
      for (let j = 0; j < this.moving_average_clicks.length; j++) {
        if (source_data[i].name == this.moving_average_clicks[j]) {
          flag = 1;
          updatedChartData.push(JSON.parse(JSON.stringify(source_data[i])));
        }
      }
    }
    const self = this,
      chart = this.chart;
    self.movingAverageOptions.series = JSON.parse(
      JSON.stringify(updatedChartData)
    );
    self.updateFlag = true;
  }

  addDisableIndexSeries(tabIndex) {
    if (
      this.stockIndexClicks.includes(tabIndex) &&
      this.stockIndexClicks.indexOf(tabIndex) !== -1
    ) {
      this.stockIndexClicks.splice(this.stockIndexClicks.indexOf(tabIndex), 1);
    }
    else {
      this.stockIndexClicks.push(tabIndex);
    }
    let source_data = JSON.parse(JSON.stringify(this.stockIndexMainData));
    let updatedChartData = [];
    this.stockIndexFlag = false;
    const self = this,
      chart = this.chart;
    if (tabIndex != 0) {
      updatedChartData.push(JSON.parse(JSON.stringify(source_data[0])));
      updatedChartData.push(JSON.parse(JSON.stringify(source_data[tabIndex])));
      self.stockIndexOptions.series = JSON.parse(
        JSON.stringify(updatedChartData)
      );
      self.stockIndexFlag = true;
    } else {
      self.stockIndexOptions.series = JSON.parse(JSON.stringify(source_data));
      self.stockIndexFlag = true;
    }
  }

  convertDateToUTC(inputdata, dynamicColumn, extraIndex) {

    var data;
    if (extraIndex != -1) data = inputdata[dynamicColumn][extraIndex];
    else data = inputdata[dynamicColumn];
    for (let i = 0; i < data['data'].length; i++) {
      let date = new Date(data['data'][i][0]);
      var now_utc = Date.UTC(
        date.getUTCFullYear(),
        date.getUTCMonth(),
        date.getUTCDate(),
        date.getUTCHours(),
        date.getUTCMinutes(),
        date.getUTCSeconds()
      );
      data['data'][i][0] = now_utc;
    }
    return inputdata;
  }

  getIndividualScores(inputdata, columns_list) {
    let output_array = [];
    for (var i = 0; i < columns_list.length; i++)
      output_array.push(inputdata[columns_list[i]] * 20);
    return output_array;
  }
  private getServerErrorMessage(error: HttpErrorResponse): string {
    switch (error.status) {
      case 401: {
        return '401';
      }
      case 402: {
        return '402';
      }
      case 403: {
        return '403';
      }
      case 404: {
        return '404';
      }

      case 405: {
        return '405';
      }
      case 406: {
        return '406';
      }
      case 412: {
        return '412';
      }
      case 500: {
        return '500';
      }
      case 501: {
        return '501';
      }
      case 502: {
        return '502';
      }
      default: {
        return '500';
      }

    }
  }
  getStockInfoData(country, stockname) {
    let symbols = { India: ['/gm', 'en-IN', '₹'], US: ['/oz', 'en-US', '$'] };
    this.currency_symbol = symbols[country][2];
    this.pageService
      .getStockInfo(country, stockname)
      .subscribe((subscribedData) => {

        this.outputViewData = subscribedData;
        this.company_name = this.outputViewData['stockName'];
        this.security_code = this.outputViewData['security_code'];
        this.img_url = this.outputViewData['img_url'];
        this.stock_ranking_data = this.outputViewData['stock_ranking'];
        this.quality_score =
          this.stock_ranking_data['quality_score'].toFixed(1) * 20;
        this.growth_score =
          this.stock_ranking_data['growth_score'].toFixed(1) * 20;
        this.technical_score =
          this.stock_ranking_data['technical_score'].toFixed(1) * 20;
        this.indv_quality_scores = this.getIndividualScores(
          this.stock_ranking_data,
          [
            'q_sales',
            'q_piotroski_score',
            'q_roe',
            'q_debt_equity',
            'q_fcf_margin',
            'q_beta',
          ]
        );
        this.indv_growth_scores = this.getIndividualScores(
          this.stock_ranking_data,
          ['g_eps_growth', 'g_sales_growth', 'g_sales_qoq']
        );
        this.indv_technical_scores = this.getIndividualScores(
          this.stock_ranking_data,
          [
            't_price_vs_ema20',
            't_price_vs_ema50',
            't_price_vs_ema200',
            't_rsi',
            't_ema50_vs_ema200',
            't_current_price_vs_52_week_high',
          ]
        );

        this.week_52_prices = this.stock_ranking_data['week_52'];
        this.companyDescription = this.outputViewData['company_description'];
        this.stringToShow = this.companyDescription;
        if (this.companyDescription.length > this.length) {
          this.stringToShow =
            this.companyDescription.slice(0, this.length - 1) + '...';
        } else {
          this.showReadMore = false;
        }
        if (this.companyDescription.length === 0) {
          this.show = false;
        }
        this.keyInformation = this.outputViewData['key_factors'][0];
        this.flag = 1;
        this.factorDataSource = [
          this.outputViewData['size_factors'],
          this.outputViewData['valuation_factors'],
          this.outputViewData['growth_factors'],
          this.outputViewData['profitability_factors'],
          this.outputViewData['leverage_factors'],
          this.outputViewData['risk_factors'],
          this.outputViewData['score_factors'],
        ];
        
        var noExtraColumn = -1;
        this.convertDateToUTC(
          this.outputViewData,
          'stock_prices',
          noExtraColumn
        );
        this.convertDateToUTC(this.outputViewData, 'volatility', noExtraColumn);
        this.convertDateToUTC(this.outputViewData, 'rsi', noExtraColumn);
        for (let i = 0; i < this.outputViewData['stock_index'].length; i++) {
          this.convertDateToUTC(this.outputViewData, 'stock_index', i);
        }
        for (
          let i = 0;
          i < this.outputViewData['output_pricevsDMA'].length;
          i++
        ) {
          this.convertDateToUTC(this.outputViewData, 'output_pricevsDMA', i);
        }

        this.moving_average_main_data = JSON.parse(
          JSON.stringify(this.outputViewData['output_pricevsDMA'])
        );

        for (let i = 0; i < this.outputViewData['volume_list'].length; i++) {
          this.convertDateToUTC(this.outputViewData, 'volume_list', i);
        }

        this.stockIndexMainData = JSON.parse(
          JSON.stringify(this.outputViewData['stock_index'])
        );
        this.stockIndexOptions = this.technical_factors_graph(
          'Stock vs Index',
          this.outputViewData['stock_index'],
          300,
          null,
          true,
          1,
          true,
          [],
          null,
          100,
          true,
          []
        );
        this.movingAverageOptions = this.technical_factors_graph(
          'Price vs Moving Average',
          this.outputViewData['output_pricevsDMA'],
          300,
          null,
          true,
          0,
          true,
          [],
          null,
          1,
          false,
          []
        );
        this.stockPriceOptions = this.technical_factors_graph(
          'Stock Prices',
          [this.outputViewData['stock_prices']],
          300,
          null,
          true,
          0,
          true,
          [],
          null,
          1,
          false,
          []
        );
        this.volumeOptions = this.technical_factors_graph(
          'Volume',
          this.outputViewData['volume_list'],
          300,
          null,
          true,
          0,
          true,
          [],
          null,
          1,
          false,
          []
        );
        this.rsiOptions = this.technical_factors_graph(
          'RSI',
          [this.outputViewData['rsi']],
          300,
          null,
          true,
          0,
          true,
          [],
          null,
          1,
          false,
          this.plotLinesData
        );
        this.volatilityOptions = this.technical_factors_graph(
          'Volatility',
          [this.outputViewData['volatility']],
          300,
          null,
          true,
          1,
          true,
          [],
          null,
          1,
          false,
          []
        );
        this.chartOptions = this.generateStockRankingChartOptions(
          this.stock_ranking_data['stock_rank']
        );
        let updatedChartData = [];
        const self = this,
          chart = this.chart;
        updatedChartData.push(this.movingAverageOptions.series[0], this.movingAverageOptions.series[4], this.movingAverageOptions.series[6]);
        self.movingAverageOptions.series = JSON.parse(
          JSON.stringify(updatedChartData)
        );
        this.loading = false;

      }, error => {
        if (error.error instanceof ErrorEvent) {
          this.errorMsg = `Error: ${error.error.message}`;
        }
        else {
          this.contentFlag = false;
          this.errorFlag = true;
          this.loading = false;
          this.errorMsg = this.getServerErrorMessage(error);
        }
      });

  }
  technical_factors_graph(
    chartID,
    output,
    chartHeight,
    chartTitle,
    legendDisplay,
    reformat,
    axisDisplay,
    plotline,
    tickpositionslist,
    multiplyFactor,
    legend,
    plotLinesData,
  ) {
    var chartOptions = {
      chart: {
        height: chartHeight,
        backgroundColor: 'transparent',
        zoomType: 'xy',
      },
      title: {
        text: chartTitle,
        margin: 0,
        y: -35,
        x: 0,
        align: 'left',

        verticalAlign: 'middle',
        style: {
          color: '#031b4e',
          fontSize: '14px ',
          fontWeight: 'bold',
        },
      },
      credits: {
        enabled: false,
      },
      legend: {
        enabled: legend
      },
      exporting: {
        buttons: {
          contextButton: {
            enabled: false,
          },
        },
      },
      plotOptions: {
        series: {
          marker: {
            enabled: false
          }
        }
      },
      xAxis: [
        {
          type: 'datetime',
          dateTimeLabelFormats: {
            day: '%e-%b-%y',
            week: '%e-%b-%y',
            month: '%b-%y',
            year: '%Y',
          },
          labels: {
            enabled: axisDisplay,
          },
          max: output[0]['data'][output[0]['data'].length - 1][0],
          min: output[0]['data'][0][0],
          tickLength: 0,
          tickWidth: 0,
          crosshair: false,
        },
      ],
      yAxis: [
        {
          title: {
            text: null,
          },
          tickPositions: tickpositionslist,
          gridLineWidth: 0,
          lineWidth: 1,
          tickLength: 6,
          tickWidth: 1,
          opposite: false,
          labels: {
            formatter: function () {
              function test(labelValue) {
                // Nine Zeroes for Billions
                return Math.abs(Number(labelValue)) >= 1.0e9
                  ? Math.ceil(Math.abs(Number(labelValue)) / 1.0e9) + 'B'
                  : // Six Zeroes for Millions
                  Math.abs(Number(labelValue)) >= 1.0e6
                    ? Math.ceil(Math.abs(Number(labelValue)) / 1.0e6) + 'M'
                    : // Three Zeroes for Thousands
                    Math.abs(Number(labelValue)) >= 1.0e3
                      ? Math.ceil(Math.abs(Number(labelValue)) / 1.0e3) + 'K'
                      : Math.abs(Number(labelValue));
              }

              if (reformat != 1) {
                if (chartID == 'Volume') return test(this.value);
                else
                  return this.value.toLocaleString('hi-IN', {
                    maximumFractionDigits: 0,
                  });
              }
              else {
                return (
                  (this.value * multiplyFactor).toLocaleString('hi-IN', {
                    maximumFractionDigits: 0,
                  }) + '%'
                );
              }
            },

            style: {
              color: '#031b4e',
              fontWeight: 'normal',
              width: '40px',
              'min-width': '40px',
            },
            useHTML: true,
          },
          plotLines: plotLinesData,
        },
      ],
      tooltip: {
        formatter: function () {
          var s = [];
          var count = 0;
          for (var i = 0; i < this.points.length; i++) {
            // $.each(this.points, function (i, point) {

            if (count == 0)
              s.push(
                'Date: <b>' +
                Highcharts.dateFormat('%d-%b-%Y', this.x) +
                '</b><br>'
              );
            count = 1;
            if (reformat != 1) {
              s.push(
                this.points[i].series.name +
                ' : <b>' +
                (this.points[i].y * multiplyFactor).toLocaleString('hi-IN', {
                  maximumFractionDigits: 0,
                }) +
                '</b><br>'
              );
            } else {
              s.push(
                this.points[i].series.name +
                ' : <b>' +
                (this.points[i].y * multiplyFactor).toLocaleString('hi-IN', {
                  maximumFractionDigits: 0,
                }) +
                '%</b><br>'
              );
            }
          }
          return s.join('<br>');
        },
        shared: true,
      },
      series: output,
    };
    return chartOptions;
  }
  toggle() {
    this.isCollapsed = !this.isCollapsed;
    if (this.isCollapsed) {
      this.stringToShow =
        this.companyDescription.slice(0, this.length - 1) + '...';
    } else {
      this.stringToShow = this.companyDescription;
    }
  }

  generateStockRankingChartOptions(rankingData) {
    this.chartOptions = {
      chart: {
        type: 'gauge',
         backgroundColor: 'transparent',
          height: 50,
          width: 120,
          margin: 0,
          padding: 0,
          spacing: [0, 0, 0, 0],
      },
     title: {
          text: null,
        },
        credits: {
          enabled: false,
        },
        exporting: {
          buttons: {
            contextButton: {
              enabled: false,
            },
          },
        },
      tooltip:{
        enabled:false
      },
      pane: {
          size: '200%',
          center: ['50%', '100%'],
          startAngle: -90,
          endAngle: 90,
          background: {
            innerRadius: '60%',
            outerRadius: '100%',
            shape: 'arc',
          },
        },
      yAxis: {
        min: 0,
        max: 100,
        minorTickInterval: 'auto',
        minorTickWidth: 0,
        minorTickLength: 0,
        minorTickPosition: 'inside',
        minorTickColor: '#666',
        tickPixelInterval: 30,
        tickWidth: 0,
        tickPosition: 'inside',
        tickLength: 0,
        tickColor: '#666',
        title: {
          text: null
        },
           labels: {
            enabled: false,
          },
        plotBands: [{
          innerRadius: "60%",
          from: 0,
          to: 100,
          color: {
            linearGradient:  { x1: 0, x2: 1, y1: 1, y2: 1 },
            stops: [
              [0, '#ECF626'], //yellow
              [1, '#389C26'] //green
            ]
        }
      },
    ]  
      },
      plotOptions: {
        gauge: {
          dial: {
            backgroundColor: 'white',
            baseWidth: 5,
            radius: '100%',
          },
          dataLabels: {
            enabled: false,
          },
          pivot: {
            radius: 15,
            borderWidth: 1,
            borderColor: 'white',
            backgroundColor: {
                linearGradient:  { x1: 0, x2: 1, y1: 1, y2: 1 },
                stops: [
                  [0, '#ECF626'], //yellow
                  [1, '#389C26'] //green
                ]
            }
        }
        }
      },
      series: [{
        name: 'Rank',
        data: [rankingData * 20],
      }]
      };
    this.value = 1;

    return this.chartOptions;
  }

  ngOnInit() {
    window.scroll(0, 0);
    let element = document.querySelector('.navbar-inverse');
    element.classList.remove('navbar');
    this.myApp.imgname = 0;
    this.country = this.route.snapshot.params.country;
    this.myApp.setDefaultValue(this.country);
    this.route.queryParams.subscribe((params) => {
      this.stockName = params['stockname'];
      this.stockname = this.stockName.split('`').join('&');
    });


    this.titleService.setTitle(this.stockName);

    this.getStockInfoData(this.country, this.stockName);

  }


  RSIVolatility(divName, otherDivName) {
    this[divName] = true;
    this[otherDivName] = false;
  }

}
