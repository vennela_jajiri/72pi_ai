import { Component, OnInit } from '@angular/core';
import { PageService } from '../service/page.service';
import { MessageService } from 'primeng/api';
import { AppComponent } from '../app.component';
import { ActivatedRoute , Router} from '@angular/router';
import { SuccessDialogComponent } from '../navbar/navbar.component';
import { MatDialog } from '@angular/material/dialog';
import { catchError } from 'rxjs/operators';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { _throw as throwError } from 'rxjs/observable/throw';
import { NavbarComponent } from '../navbar/navbar.component';
import { Title, Meta } from '@angular/platform-browser';
export interface StocksData {
  id?: string;
  company_name?: string;
  sector_name?: string;
  market_cap_category?: string;
  price?: number;
  quantity?: number;
  current_mkt_val?: number;
  portfolioname?: string;
}
@Component({
  selector: 'app-create-portfolio-stocks-selection',
  templateUrl: './create-portfolio-stocks-selection.component.html',
  styleUrls: ['./create-portfolio-stocks-selection.component.scss'],
})
export class CreatePortfolioStocksSelectionComponent implements OnInit {
  country: string;
  sector: string;
  public outputViewData: any = [];
  public stocksData_main: any = [];
  public stocksData: any = [];
  selectedSector;
  selectedMcapFilter;
  selectedPortfolio;
  scrollableCols: any[];
  sectorlist: any[];
  portfolioNames_dict: any[];
  frozenCols: any[];
  mcap_filter: any[];
  selectedstocks: StocksData[];
  NewPortfolioDialog: boolean;
  AddExisitingDialog: boolean;
  currency: string;
  def_amnt: number;
  filteredValuesLength:number;
  currency_mode: string;
  value;
  symbols = { 'India': 'en-IN', 'US': 'en-US' }
  mcap= { 'India': 'MCap (in Cr)', 'US': 'MCap (in M)' }
  symbol: string;
  newportfolioname: string;
  loading: boolean = true;
  divloading : boolean;
  errorMsg: string;
  message;
  status;mcapname;
  filter={'India':[{ 'name': 'Market Cap > 500Cr', 'id': 'default' }, { 'name': 'All', 'id': 'all' }],'US':[{ 'name': 'Market Cap > 500M', 'id': 'default' }, { 'name': 'All', 'id': 'all' }]}
  contentFlag = true; errorFlag = false;
   
    constructor( private titleService: Title, private metaService: Meta,  private navbar:NavbarComponent,public dialog: MatDialog,private pageservice: PageService, private messageService: MessageService,private http: HttpClient,
    public myApp: AppComponent, private route: ActivatedRoute, private router: Router) { }
  getStocksData(country, sector) {
    this.pageservice.getCreatePortfolioSelectingStocks(country, sector).subscribe(subscribedData => {
      this.contentFlag = true;
      this.errorFlag = false;
      this.outputViewData = subscribedData;
      this.stocksData = this.outputViewData['stockData'];
      this.stocksData_main = this.outputViewData['stockData'];
      this.filteredValuesLength = this.stocksData.length;
      this.sectorlist = this.outputViewData['sectorlist_dict']
      this.portfolioNames_dict = this.outputViewData['portfolioNames_dict']
      this.mcap_filter = this.filter[country]
      this.selectedMcapFilter = this.mcap_filter.find(mcap_filter => mcap_filter.id === 'default');
      this.currency = this.outputViewData['currency']
      this.def_amnt = this.outputViewData['def_amnt']
      this.currency_mode = this.outputViewData['currency_mode']
      this.loading = false
      this.divloading = false;
      this.mcapname=this.mcap[country]
    }, error => {
      if (error.error instanceof ErrorEvent) {
        this.errorMsg = `Error: ${error.error.message}`;
      }
      else {
        this.contentFlag = false;
        this.errorFlag = true;
        this.loading = false;
        this.errorMsg = this.getServerErrorMessage(error);
      }
    });
  }
  ngOnInit(): void {
    this.titleService.setTitle('Portfolio Builder');
    this.metaService.addTags([
      {name: 'keywords', content: '72piportfoliocreator ,72pistockscreener ,72piportfoliogenerator ,72piportfolioconstruction,portfoliocreator ,stockscreener ,portfoliogenerator ,portfolioconstruction'},
      {name: 'description', content: 'To create portfolio of stocks by selecting stocks manually, using filters on technical/fundamental factors or by excel '},
    ]);
    window.scroll(0, 0);
    let element = document.querySelector('.navbar-inverse');
    element.classList.remove('navbar');
    this.myApp.imgname = 0;
    this.country = this.route.snapshot.params.country;
    this.symbol = this.symbols[this.country]
    this.myApp.setDefaultValue(this.country)
    this.getStocksData(this.country, 'All');
    this.scrollableCols = [
      { field: 'gics', header: 'Sector' },
      { field: 'market_cap_category', header: 'Market Cap Category' },
      { field: 'market_cap', header: 'Market Cap' },
      { field: 'price', header: 'Current Price' },
      { field: 'security_code', header: 'Security Code' },
      { field: 'IMGURL', header: 'Img URL' },
      { field: 'company', header: 'Company' },
      { field: 'min_price', header: 'Price@2019'}

    ];
  }
  filterBySector() {
    this.divloading = true;
    this.sector = this.selectedSector.name;
    this.getStocksData(this.country, this.sector)
  }

  createPortfolio() {
    this.NewPortfolioDialog = true;
  }
  addToExistPortfolio() {
    this.AddExisitingDialog = true;
  }
  hideDialog() {
    this.NewPortfolioDialog = false;
    this.AddExisitingDialog = false;
  }
  modifyQuantity(event): void {
    this.value = event.value
    for (let i = 0; i < this.stocksData.length; i++) {
      this.stocksData[i].Quantity = (this.value / this.stocksData[i].Price).toFixed(0);
    }
    this.stocksData = [...this.stocksData];
  }
  private getServerErrorMessage(error: HttpErrorResponse): string {
    switch (error.status) {
        case 401: {
            return '401';
        }
        case 402: {
            return '402';
        }
        case 403: {
            return '403';
        }
        case 404: {
          return '404';
        }
      
        case 405: {
          return '405';
        }
        case 406: {
          return '406';
        }
        case 412: {
          return '412';
        }
        case 500: {
          return '500';
        }
        case 501: {
          return '501';
        }
        case 502: {
          return '502';
        }
        default: {
            return '500';
        }

    }
  }
  saveCreatePortfolio(){
    let data = {'stocks':this.selectedstocks, 'portfolioName':this.newportfolioname, 'addToExisting':false}
    this.pageservice.saveCustomerPortfolio(data, this.country).subscribe(response=>{
      this.titleService.setTitle('Portfolio Builder');
      this.message = response['message'];
      this.status = response['status'];
      const timeout = 1600;
      const dialogRef = this.dialog.open(SuccessDialogComponent, {
        width: '500px',
        data: { message: this.message, status: this.status },
      });
      dialogRef.afterOpened().subscribe((_) => {
        setTimeout(() => {
          dialogRef.close();
        }, timeout);
      });
    }, error => {
      if (error.error instanceof ErrorEvent) {
        this.errorMsg = `Error: ${error.error.message}`;
      }
      else {
        alert(error.error.message)
      }
    });
   
    this.NewPortfolioDialog = false;
  }
  
  saveAddToExistPort() {
    let data = {'stocks':this.selectedstocks, 'portfolioName':this.selectedPortfolio['name'],'addToExisting':true}
    this.pageservice.saveCustomerPortfolio(data, this.country).subscribe(response=>{
      this.message = response['message'];
      this.status = response['status'];
      const timeout = 1600;
      const dialogRef = this.dialog.open(SuccessDialogComponent, {
        width: '500px',
        data: { message: this.message, status: this.status },
      });
      dialogRef.afterOpened().subscribe((_) => {
        setTimeout(() => {
          dialogRef.close();
        }, timeout);
      });
    }, error => {
      if (error.error instanceof ErrorEvent) {
        this.errorMsg = `Error: ${error.error.message}`;
      }
      else {
        alert(error.error.message)
      }
    });
    this.AddExisitingDialog = false;      
  }
  getStockInfopage(selectedStock,selectedSymbol){
    let pagename = "/StockInfoPage/";
    this.router.navigate([]).then(result => {  window.open( this.country + pagename + "?stockname=" + (selectedStock+" ("+selectedSymbol+")").split("&").join("`") , '_blank','noopener') });
  }
 
}