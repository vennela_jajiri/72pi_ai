import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CreatePortfolioStocksSelectionComponent } from './create-portfolio-stocks-selection.component';

describe('CreatePortfolioStocksSelectionComponent', () => {
  let component: CreatePortfolioStocksSelectionComponent;
  let fixture: ComponentFixture<CreatePortfolioStocksSelectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatePortfolioStocksSelectionComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CreatePortfolioStocksSelectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
