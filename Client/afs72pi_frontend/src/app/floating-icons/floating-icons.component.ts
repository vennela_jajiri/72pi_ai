import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-floating-icons',
  templateUrl: './floating-icons.component.html',
  styleUrls: ['./floating-icons.component.scss']
})
export class FloatingIconsComponent implements OnInit {
  constructor() { }
  public linkedinUrl: string = 'https://www.linkedin.com/company/72pi/';
  public facebookUrl:string = "https://www.facebook.com/72pisignals/";
  public twitterUrl:string = 'https://twitter.com/72pisignals';
  public youtubeUrl:string = 'https://www.youtube.com/channel/UCGKP2GoOr2RTMpe9cPwr7WA';
  public instagramUrl:string = 'https://www.instagram.com/72pisignals/';

  ngOnInit(): void {
  }

}
