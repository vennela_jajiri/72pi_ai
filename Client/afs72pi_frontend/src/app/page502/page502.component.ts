import { Component, OnInit } from '@angular/core';
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'app-page502',
  templateUrl: './page502.component.html',
  styleUrls: ['./page502.component.scss'],
})
export class Page502Component implements OnInit {

  constructor(private titleService:Title) {
    this.titleService.setTitle("Sorry, server error!");
  }

  ngOnInit() {
    window.scroll(0,0);
    let element = document.querySelector('.navbar-inverse');
    element.classList.remove('navbar');
  }

}
