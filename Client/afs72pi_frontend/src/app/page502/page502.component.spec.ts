import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Page502Component } from './page502.component';

describe('Page502Component', () => {
  let component: Page502Component;
  let fixture: ComponentFixture<Page502Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Page502Component ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Page502Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
