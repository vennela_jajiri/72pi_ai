import { Options } from 'highcharts';

export const lineChart: Options = {
  chart: {
    type: 'line',
  },
  credits: {
    enabled: false,
  },
  title: {
    text: null,
  },
  legend: {
    enabled: true,
  },
  xAxis: {
    type: 'datetime',
    dateTimeLabelFormats: { // don't display the dummy year
        day: '%e-%b-%y',
        week: '%e-%b-%y',
        month: '%b-%y',
        year: '%Y'
    },
    labels: {
        style: {
            color: '#031b4e',
            fontWeight: 'normal'
        },
    },
    tickLength: 6,
    tickWidth: 1,
    startOnTick: false,
    endOnTick: false,
},
yAxis: {
    gridLineWidth: 0,
    lineWidth: 1,
    tickLength: 6,
    tickWidth: 1,
    title: {
        text: null,
    },
    plotLines: [{
        color: '#F2F2F2',
        width: 2,
        value: 0
    }]
},

  // plotOptions: {
  //   series: {
  //     borderRadius: 5,
  //   } as any,
  // },
  series:[]

  
  
};