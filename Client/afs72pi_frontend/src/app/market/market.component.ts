
import { lineChart } from 'src/charts/lineChart';
import { SentimentBarChart } from 'src/charts/sentimentBarChart';
import { Chart } from 'angular-highcharts';
import { Component, OnInit } from '@angular/core';
import { Injectable } from '@angular/core';
import 'rxjs';
import { PageService } from '../service/page.service';
import * as Highcharts from 'highcharts'
import { AppComponent } from '../app.component';
import { ActivatedRoute, Router } from '@angular/router';
import { bubbleChart } from 'src/charts/bubbleChart';
import { barChart } from 'src/charts/barChart';
import { heatMap } from 'src/charts/heatMap';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { NavbarComponent } from '../navbar/navbar.component';
import { Title, Meta } from '@angular/platform-browser';
@Component({
  selector: 'app-market',
  templateUrl: './market.component.html',
  styleUrls: ['./market.component.scss']
},
)
@Injectable({
  providedIn: 'root'
})
export class MarketComponent implements OnInit {
  element;
  SlideOptions = {
    dots: true, nav: true, loop: true, autoplay: false, autoplayTimeout: 5000, autoplayHoverPause: true,
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 1
      },
      740: {
        items: 1
      },
      940: {
        items: 1
      }
    },
  };
  CarouselOptions = { items: 3, dots: true, nav: true, autoplay: true, autoplayTimeout: 5000, };
  public outputViewData: any = [];
  public charts: any = [];
  Chart: any;
  sentiment_lines: any;
  nifty_indices_lines: any; oil_lines: any; gold_lines: any; fxrate_lines: any; interest_rate_lines: any; indices_lines: any;
  beer: any; pe: any; vix: any; nifty_ma: any; p_e: any;
  indices_data: any; macro_data; market_index_performance: any; market_sector_performance: any; sector_pe_pb_performance: any;
  country: string; header_values = {};
  Headers: any;
  Content: any;
  displayColumns: any;
  displayColumns2; displayPerformanceColumns: any; displaySectorPEPBColumns: any;
  sectorGroupColumns: any;
  barChart: any;
  barChart1: any; barchart1Data: any; adv_dec_dates: any;
  barChart2: any;
  advances: any;
  declines: any;
  loading = true;
  market_byte_table1 = ['country', 'ticker', 'dailypercentchange', 'monthly_price_change_percent', 'yearly_price_change_percent']
  market_byte_table1_display = ['Country', 'Index', 'DTD %', 'MTD %', 'YTD %']
  market_byte; gainers; losers; index_gain_losers;
  gainers_cols = ['Company', 'Price', 'Gain']
  gainers_columns = ['Company', 'Price', '% Gain']
  losers_columns = ['Company', 'Price', '% Loss']
  sector_columns = ['Sector Index', '% Change']
  correlation_categories; correlation_output;
  heatMapCorrelation; bubble_chart; bubble_chart_output;
  symbols = { 'India': 'en-IN', 'US': 'en-US' }
  symbol: string;
  valuationFlag = 0;
  correlationFlag = 0;
  errorMsg: string;
  contentFlag = true; errorFlag = false;

  asofDate;
  headers: any = {
    'India': ['Bond Equity Earnings Yield Ratio', 'Trailing 12M PE of NIFTY 50', 'India VIX',
      '% of NIFTY 500 Stocks above 200MA'],
    'US': ['CBOE Volatility Index', 'Schiller PE Ratio', 'Trailing 12M PE of S&P 500']
  }
  content: any = {
    'India': ['Bond Equity Earnings Yield Ratio (BEER) is calculated using trailing 12M earnings of NIFTY 50 for the last 15 years.Market has been very volatile this year, it was trading close to its single standard deviation (+1σ) at 1.9 on January 20th 2020 and dipped to its second standard deviation on the downside (-2σ) at 1.1. Historically under normal circumstances, it trades within a single standard deviation from mean. We remain cautious at these levels, as it has recovered very fast from March 2020 lows.',
      'The above chart shows trailing 12M PE of NIFTY 50 for the last 15 years. In general, Nifty has rallied after trading close to 15x and has corrected after trading closing to 30x. Generally whenever trailing 12M PE multiple is above 30x, market levels are higher on anticipation of better forward earnings. We ”remain cautious” at these levels.',
      'The above chart shows India VIX for the last 12 years. India VIX is the annualized volatility number that indicates the expected market volatility over the next 30 calendar days. It helps in understanding the market sentiment. It trades in the band 10-15 under a low volatile market, 15-25 under a volatile market, 25-40 under a highly volatile market and above 40 in extreme volatile markets.',
      'The above chart shows % of NIFTY500 stocks trading above their 200 moving average in the last 3 years. The mean is around 45% and it hovers between its first standard deviations 26% and 63%. At 63%, market looks expensive with limited upside and at 26%, market looks discounted with limited downside. Above 63%, market is highly expensive and below 26%, market is deeply discounted.'],

    'US': ['The above chart shows CBOE Volatility index for the last 15 years. CBOE Volatility Index is the annualized volatility number that indicates the expected market volatility over the next 30 calendar days. It helps in understanding the market sentiment. It trades in the band 10-15 under a low volatile market, 15-25 under a volatile market, 25-40 under a highly volatile market and above 40 in extreme volatile markets.',
      'The above chart shows Shiller PE Ratio of S&P 500 for the last 10 years. It is calculated as the price of the S&P 500 index divided by the average inflation-adjusted earnings of the previous 10 years. In the last 5 years, S&P 500 has rallied after trading close to 24x and has corrected after trading close to 34x. We ”remain cautious”, when Shiller PE ratio is close to 34x',
      'The above chart shows trailing 12M PE of S&P 500 for the last 10 years. In the last 3 years, S&P 500 has rallied after trading close to 20x and has corrected after trading above 25x. Generally whenever trailing 12M PE multiple is above 25x, market levels are higher on anticipation of better forward earnings. We ”remain cautious” at these levels.']
  }



  constructor(private titleService: Title, private metaService: Meta, private navbar: NavbarComponent, private pageService: PageService, public myApp: AppComponent, private route: ActivatedRoute, private http: HttpClient, private router: Router) {

  }
  private getServerErrorMessage(error: HttpErrorResponse): string {
    switch (error.status) {
      case 401: {
        return '401';
      }
      case 402: {
        return '402';
      }
      case 403: {
        return '403';
      }
      case 404: {
        return '404';
      }

      case 405: {
        return '405';
      }
      case 406: {
        return '406';
      }
      case 412: {
        return '412';
      }
      case 500: {
        return '500';
      }
      case 501: {
        return '501';
      }
      case 502: {
        return '502';
      }
      default: {
        return '500';
      }

    }
  }
  plotCharts(dynamicColumn, plotLinesData, legend, chartHeight, multiplyFactor, dividendFactor, symbol, tooltipSymbol, roundDigits, chartTitle = null, formatCurrency = "", symbol_param = "") {
    this[dynamicColumn] = new Chart(lineChart)
    if (!(['oil_lines', 'gold_lines', 'interest_rate_lines', 'fxrate_lines', 'indices_lines', 'sentiment_lines'].includes(dynamicColumn)))
      this.charts.push(this[dynamicColumn])
    let outputData = [this.outputViewData[dynamicColumn]]
    if (dynamicColumn == 'indices_lines')
      outputData = this.outputViewData[dynamicColumn];
    this[dynamicColumn].options.title = {
      text: chartTitle,
      style: {
        color: '#031b4e',
        fontWeight: 'bold',
        fontSize: '14px'
      },
    },
      this[dynamicColumn].options = { series: outputData }
    this[dynamicColumn].options.exporting = lineChart.exporting
    this[dynamicColumn].options.xAxis = {
      type: 'datetime',
      tickWidth: 1,
      tickLength: 6,
      startOnTick: false,
      endOnTick: false,
      dateTimeLabelFormats: {
        day: '%b-%y',
        week: '%b-%y',
        month: '%b-%y',
        year: '%Y',
      },
      labels: {
        style: {
          color: '#031b4e',
          fontWeight: 'normal',
          fontSize: '14px'
        },
      },
    }
    this[dynamicColumn].options.yAxis = {
      gridLineWidth: 0,
      lineWidth: 1,
      tickLength: 6,
      tickWidth: 1,
      labels: {
        formatter: function () {
          if ((['oil_lines', 'gold_lines', 'interest_rate_lines', 'fxrate_lines', 'indices_lines'].includes(dynamicColumn)))
            return ((this.value * multiplyFactor) / dividendFactor).toFixed(0) + symbol;
          else if((['beer','nifty500_stocks_above_200ma']).includes(dynamicColumn)){
            return ((this.value ).toFixed(1) + "%");
          }
          else
            return ((this.value * multiplyFactor) / dividendFactor).toFixed(1) + symbol;
        },
        style: {
          color: '#031b4e',
          fontSize: '14px',
          fontWeight: 'normal'
        },
      },
      title: {
        text: null,
        style: {
          color: '#031b4e',
          fontSize: '14px',
          fontWeight: 'bold'
        },
      },

      plotLines:
        plotLinesData


    }
    this[dynamicColumn].options.legend = {
      enabled: legend,
      itemStyle: {
        fontSize: '12px'
      },
    };
    this[dynamicColumn].options.plotOptions = lineChart.plotOptions;
    this[dynamicColumn].options.chart = {
      type: 'line',
      backgroundColor: "transparent",
      // zoomType: 'x',
      height: chartHeight,
    };
    this[dynamicColumn].options.title = lineChart.title;
    this[dynamicColumn].options.credits = lineChart.credits;
    this[dynamicColumn].options.tooltip = {
      formatter: function () {
        var s = ["Date : " + '<b>' + Highcharts.dateFormat('%d-%b-%Y', this.x)];
        for (let i = 0; i < this.points.length; i++) {
          if (dynamicColumn == 'gold_lines') {
            s.push(this.points[i].series.name + ' : <b>' + symbol_param + parseFloat(this.points[i].y).toLocaleString(formatCurrency) + tooltipSymbol + '</b><br>');
          }
          else if(dynamicColumn == 'oil_lines'){
            s.push(this.points[i].series.name + ' : <b>$' + symbol_param + parseFloat(this.points[i].y).toLocaleString(formatCurrency) + '/barrel</b><br>');
          }
          else if((['beer','nifty500_stocks_above_200ma']).includes(dynamicColumn)){
            s.push(this.points[i].series.name + ' : <b>' + symbol_param + (parseFloat(this.points[i].y)).toFixed(1)+'%</b><br>');
          }
          else {
            s.push(this.points[i].series.name + " : " + '<b>' + (this.points[i].point.y * multiplyFactor).toFixed(roundDigits) + tooltipSymbol + '</b>');
          }
        }
        return s.join('<br>');
      },
      shared: true,

      style: {
        color: '#031b4e',
        fontSize: '14px',
        fontWeight: 'normal'
      },
    }
  }
  convertDateToUTC(inputdata, dynamicColumn, extraIndex) {
    var data;
    if (extraIndex != -1)
      data = inputdata[dynamicColumn][extraIndex];
    else
      data = inputdata[dynamicColumn];
    for (let i = 0; i < data['data'].length; i++) {
      let date = new Date(data['data'][i][0]);
      var now_utc = Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(),
        date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds());
      data['data'][i][0] = now_utc;
    }
    return inputdata
  }
  onChange(event: MatTabChangeEvent) {
    this.correlationFlag = 0;
    this.valuationFlag = 0;
    if (event.tab.textLabel == 'Valuation') {
      this.valuationFlag = 1;
      this.correlationFlag = 0;

    } else if (event.tab.textLabel == 'Risk') {
      this.correlationFlag = 1;
      this.valuationFlag = 0;
    }
    else {
      this.correlationFlag = 0;
      this.valuationFlag = 0;
    }
  }
  getMarketPageData(country) {
    let symbols = { 'India': ['/gm', 'hi-IN', '₹', ' (in Cr)'], 'US': ['/oz', 'en-US', '$', ' (in M) '] }
    this.pageService.getData(country).subscribe(subscribedData => {
      this.titleService.setTitle('Market Monitor');
      this.contentFlag = true;
      this.errorFlag = false;
      this.outputViewData = subscribedData;
      this.macro_data = this.outputViewData['macro_data'];
      this.indices_data = this.outputViewData['indices_data'];
      this.market_index_performance = this.outputViewData['market_performance1']
      this.market_sector_performance = this.outputViewData['market_performance2']
      this.sector_pe_pb_performance = this.outputViewData['sector_pe_pb']
      this.sectorGroupColumns = this.outputViewData['sector_group_dates']
      var noExtraColumn = -1
      this.outputViewData['sentiment_lines'] = this.outputViewData['nifty_indices_lines']
      this.convertDateToUTC(this.outputViewData, 'gold_lines', noExtraColumn)
      this.convertDateToUTC(this.outputViewData, 'interest_rate_lines', noExtraColumn)
      this.convertDateToUTC(this.outputViewData, 'fxrate_lines', noExtraColumn)
      this.convertDateToUTC(this.outputViewData, 'oil_lines', noExtraColumn)
      for (let i = 0; i < this.outputViewData['indices_lines'].length; i++) {
        this.convertDateToUTC(this.outputViewData, 'indices_lines', i)
      }
      for (let i = 0; i < this.outputViewData['charts'].length; i++) {
        this.convertDateToUTC(this.outputViewData, this.outputViewData['charts'][i], noExtraColumn)
        this.plotCharts(this.outputViewData['charts'][i], this.outputViewData[this.outputViewData['charts'][i] + '_plotLines'], false, 250, 1, 1, "", "", 1);
      }
      this.plotCharts('indices_lines', [], true, 300, 100, 1, "%", "%", "1");
      this.plotCharts('gold_lines', [], false, 220, 1, 1000, "K", symbols[country][0], 0, "", symbols[country][1], symbols[country][2]);
      this.plotCharts('interest_rate_lines', [], false, 220, 100, 1, "%", "%", 1, "");
      this.plotCharts('fxrate_lines', [], false, 220, 1, 1, "", "", 2, '');
      this.plotCharts('oil_lines', [], false, 220, 1, 1, "", "", 2, "",symbols[country][1]);
      this.plotCharts('sentiment_lines', [], false, 300, 1, 1, "", "", 0);
      this.displayColumns = this.outputViewData['displayColumns'];
      this.displayColumns2 = this.outputViewData['displayColumns2'];
      this.displayPerformanceColumns = this.outputViewData['displayPerformanceColumns']
      this.displaySectorPEPBColumns = this.outputViewData['sector_pe_pb_columns']
      this.sectorGroupColumns = this.outputViewData['sector_group_dates']
      this.advances = this.outputViewData['advances']
      this.barchart1Data = this.outputViewData['last_10_adv_dec']
      this.advances = this.outputViewData['advances']
      this.declines = this.outputViewData['declines']
      this.adv_dec_dates = this.outputViewData['adv_dec_dates']
      this.market_byte = this.outputViewData['market_byte']
      this.gainers = this.outputViewData['gainers']
      this.losers = this.outputViewData['losers']
      this.index_gain_losers = this.outputViewData['index_gain_losers']
      this.correlation_categories = this.outputViewData['correlation_categories']
      this.correlation_output = this.outputViewData['correlation_output']
      this.drawHeapMap(this.correlation_output)
      this.bubble_chart_output = this.outputViewData['screening_bubble_data']
      this.drawBubbleChart(this.bubble_chart_output, symbols[country][3], symbols[country][1])
      this.drawBarChart(this.barchart1Data, this.advances, this.declines, this.adv_dec_dates);
      this.loading = false;
      this.asofDate = this.outputViewData['asofDate']

    }, error => {
      if (error.error instanceof ErrorEvent) {
        this.errorMsg = `Error: ${error.error.message}`;
      }
      else {
        this.contentFlag = false;
        this.errorFlag = true;
        this.loading = false;
        this.errorMsg = this.getServerErrorMessage(error);
      }
    });
  }

  drawHeapMap(outputData) {
    var that=this;
    this.heatMapCorrelation = new Chart(heatMap);
    this.heatMapCorrelation.options.xAxis = {
      opposite: true,
      reversed: false,
      lineWidth: 0,
      categories: this.correlation_categories,
    },
      this.heatMapCorrelation.options.yAxis = {
        gridLineWidth: 0,
        categories: JSON.parse(JSON.stringify(this.correlation_categories)).reverse(),
        title: null,
      },
      this.heatMapCorrelation.options.tooltip = {
        formatter: function () {
            return that.correlation_categories[this.point.x] + "<br>" +  JSON.parse(JSON.stringify(that.correlation_categories)).reverse()[this.point.y]        
        }
        
      }
      this.heatMapCorrelation.options.series = [{
        name: 'Correlation',
        data: outputData['data'],
        dataLabels: {
          enabled: true,
          color: 'black',
          style: {
            textOutline: 0,
            fontWeight: 'normal',
            fontSize: '12px',
          },
        },
      },
      ]
  }
  drawBubbleChart(bubbleOutput, currencySymbol, formatCurrency) {
    this.bubble_chart = new Chart(bubbleChart);
    this.bubble_chart.options.series = [{ data: bubbleOutput }]
    this.bubble_chart.options.tooltip = {
      formatter: function () {
        return "Company : " + this.point.Company + "<br>Symbol : " + this.point.Security_Code + "<br>Market Cap " + currencySymbol + ' : ' + parseFloat(this.point.Market_Cap).toLocaleString(formatCurrency, { maximumFractionDigits: 0 })
          + "<br>Sales " + currencySymbol + ' : ' + parseFloat(this.point.value).toLocaleString(formatCurrency, { maximumFractionDigits: 0 }) + "<br>Beta : " + parseFloat(this.point.Beta_1Y).toFixed(1) + "<br>PE : " + parseFloat(this.point.PE).toFixed(1) +
          "<br>Debt/Equity : " + parseFloat(this.point.Debt_Equity).toFixed(1) + "<br>Sales Growth : " + parseFloat(this.point.Sales_Growth).toFixed(1) + '%';
      }
    };
  }


  drawBarChart(barchart1data, advance, decline, adv_dec_dates) {

    this.barChart1 = new Chart(barChart);
    this.barChart1.options = { series: barchart1data };
    this.barChart1.options.title = barChart.title;
    this.barChart1.options.chart = barChart.chart;
    this.barChart1.options.credits = barChart.credits;
    this.barChart1.options.legend = barChart.legend;

    this.barChart1.options.tooltip = {
      formatter: function () {
        return this.series.name + " : " + '<b>' + (this.point.y * 100).toFixed(0) + "%" + '</b>';
      }
    };
    this.barChart1.options.plotOptions = {
      column: {
        stacking: 'normal',
        dataLabels: {
          formatter: function () {
            return (this.point.y * 100).toFixed(0) + "%" + '</b>';
          },
          enabled: true,
          rotation: -90,
          color: 'white',
          style: {
            textOutline: 0,
            fontWeight: 'normal',
            fontSize: '12px',
          }
        } as any
      },
    };
    this.barChart1.options.yAxis = {
      min: 0,
      max: 1,
      gridLineWidth: 0,
      opposite: true,
      title: {
        text: null
      },
      labels: {
        //   formatter: function() {
        //     return this.value * 100 +"%";
        //  }
        enabled: false
      }
    };
    this.barChart1.options.xAxis = {
      lineWidth: 1,
      labels: {
        formatter: function () {
          var m = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
          var dArr = (this.value).split("-");
          return dArr[2] + "-" + m[parseInt(dArr[1], 10) - 1]

        }
      },
      categories: adv_dec_dates,
      title: {
        text: null,
      },
    },
      // this.barChart1.options.chart = {
      //   type: 'column',
      //   // width: 400,
      //   // height: 320,

      // };
      this.barChart1.options.exporting = barChart.exporting;
    this.barChart2 = new Chart(barChart);
    this.barChart2.options = {
      series: [
        {
          color: '#F52A4C',
          name: 'Declines',
          data: decline,
        },
        {
          color: '#136F63',
          name: 'Advances',
          data: advance,
        },

      ],
    };
    this.barChart2.options.title = barChart.title;
    this.barChart2.options.chart = {
      type: 'bar',
      height: 70,
      width: 430,
    };
    this.barChart2.options.credits = barChart.credits;
    this.barChart2.options.exporting = barChart.exporting;

    this.barChart2.options.legend = {
      enabled: false,
    };
    this.barChart2.options.tooltip = barChart.tooltip;
    this.barChart2.options.plotOptions = {
      bar: {
        dataLabels: {
          enabled: true,
          color: 'white',
          style: {
            textOutline: 0,
            fontWeight: 'normal',
            fontSize: '12px',
          },
        },
      },
      series: {
        stacking: 'normal',
      },
    };
    this.barChart2.options.yAxis = {
      gridLineWidth: 0,
      // reversed: true,
      lineWidth: 0,
      labels: {
        enabled: false,
      },
      title: {
        text: null,
      },
    };
    this.barChart2.options.xAxis = {
      lineWidth: 0,
      tickLength: 0,
      tickWidth: 0,
      labels: {
        enabled: false,
      },
      title: {
        text: null,
      },
    };
  }

  ngOnInit(): void {
    this.titleService.setTitle('Market Monitor');
    this.metaService.addTags([
      { name: 'keywords', content: 'marketoutlook ,marketview ,marketsentiment ,sectorpe ,sectorpb ,indicesperformance ,indiavix , beer ,beernifty ,cobevolatility ,90Dcorrelation ,shillerpe ,macroeconomicdata ,marketindicators ' },
      { name: 'description', content: 'This is a snapshot of current equity market conditions using indices and macro indicators.' },
    ]);
    window.scroll(0, 0);
    let element = document.querySelector('.navbar-inverse');
    element.classList.remove('navbar');
    this.myApp.imgname = 0;
    this.country = this.route.snapshot.params.country;
    this.header_values['country'] = this.country.toUpperCase();
    let benchmarks = { 'India': 'Nifty', 'US': 'S&P' }
    this.header_values['index_name'] = benchmarks[this.country]
    this.myApp.setDefaultValue(this.country)
    this.headers = this.headers[this.country];
    this.Content = this.content[this.country];
    this.getMarketPageData(this.country);
    this.symbol = this.symbols[this.country]
    let categories_data = {
      'India': [['Nifty', 'Bank Nifty', 'Nifty Mid Cap 100', 'Nifty Small Cap 100'], [{ y: 100, target: 90 }, { y: 100, target: 95 }, { y: 100, target: 80 }, { y: 100, target: 85 }]]
      , 'US': [['S&P 500', 'Dow Jones Industrial Average', 'Russell 2000'], [{ y: 100, target: 90 }, { y: 100, target: 95 }, { y: 100, target: 80 }]]
    }
    this.barChart = new Chart(SentimentBarChart);
    this.barChart.options.series = SentimentBarChart.series
    this.barChart.options.title = SentimentBarChart.title;
    this.barChart.options.chart = SentimentBarChart.chart;
    this.barChart.options.credits = SentimentBarChart.credits;
    this.barChart.options.legend = SentimentBarChart.legend;
    this.barChart.options.tooltip = SentimentBarChart.tooltip;
    this.barChart.options.plotOptions = SentimentBarChart.plotOptions
    this.barChart.options.yAxis = SentimentBarChart.yAxis
    this.barChart.options.xAxis = SentimentBarChart.xAxis
    this.barChart.options.xAxis.categories = categories_data[this.country][0]
    this.barChart.options.series[0].data = categories_data[this.country][1]
    this.barChart.options.exporting = SentimentBarChart.exporting
  }
}
