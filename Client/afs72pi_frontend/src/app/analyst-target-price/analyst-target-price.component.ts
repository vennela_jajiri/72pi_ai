import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { AppComponent } from '../app.component';
import { PageService } from '../service/page.service';
import { UserPortfolioService } from '../service/userPortfolios.service';
import { Sort } from '@angular/material/sort';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { NavbarComponent } from '../navbar/navbar.component';
import { Title, Meta } from '@angular/platform-browser';
@Component({
  selector: 'app-analyst-target-price',
  templateUrl: './analyst-target-price.component.html',
  styleUrls: ['./analyst-target-price.component.scss'],
})
export class AnalystTargetPriceComponent implements OnInit {
  targetPriceData: any[];
  maxValue: number;
  maxValue2: number;
  minValue2: number;
  country: string;
  portfolio_list: any[];
  selectedPortfolio;
  portfolioname: string;

  username: string;
  symbols = { 'India': 'en-IN', 'US': 'en-US' }
  currencysymbols = { 'India': '₹', 'US': '$' }
  symbol: string;
  currencysymbol: string;
  element;
  loading: boolean = true;
  divloading: boolean;
  analystTargetPriceDataSource;
  errorMsg: string;
  emptyStocks: boolean = false;
  contentFlag = true; errorFlag = false;
  dataSource: MatTableDataSource<any> = new MatTableDataSource();
  constructor(private titleService: Title, private metaService: Meta, private pageService: PageService, private myApp: AppComponent, private route: ActivatedRoute, private router: Router, private userService: UserPortfolioService, private http: HttpClient, private navbar: NavbarComponent) { }

  getAnalystTargetPriceData(country, selectedPortfolio) {
    this.pageService.getFactorAnalystTargetPriceViews(country, selectedPortfolio).subscribe(targetPriceInfo => {
      this.titleService.setTitle('Analyst target price');
      this.contentFlag = true;
      this.errorFlag = false;
      if (targetPriceInfo['status'] == 1) {
        this.analystTargetPriceDataSource = targetPriceInfo
        this.targetPriceData = targetPriceInfo['output'];
        this.maxValue = targetPriceInfo['max_value'];
        this.maxValue2 = targetPriceInfo['max_value2'];
        this.minValue2 = targetPriceInfo['min_value2'];
        this.dataSource = new MatTableDataSource<Element>(this.targetPriceData);
        this.loading = false
        this.divloading = false;
      }
      else {
        this.emptyStocks = true;
        this.loading = false;
      }
    }, error => {
      if (error.error instanceof ErrorEvent) {
        this.errorMsg = `Error: ${error.error.message}`;
      }
      else {
        this.contentFlag = false;
        this.errorFlag = true;
        this.loading = false;
        this.errorMsg = this.getServerErrorMessage(error);
      }
    });
  }
  private getServerErrorMessage(error: HttpErrorResponse): string {
    switch (error.status) {
      case 401: {
        return '401';
      }
      case 402: {
        return '402';
      }
      case 403: {
        return '403';
      }
      case 404: {
        return '404';
      }

      case 405: {
        return '405';
      }
      case 406: {
        return '406';
      }
      case 412: {
        return '412';
      }
      case 500: {
        return '500';
      }
      case 501: {
        return '501';
      }
      case 502: {
        return '502';
      }
      default: {
        return '500';
      }

    }
  }
  onChange() {
    this.divloading = true;
    this.userService.setPortfolioName(this.selectedPortfolio)
    this.getAnalystTargetPriceData(this.country, this.selectedPortfolio.portfolio_name)
  }
  ngOnInit() {
    window.scroll(0, 0);
    let element = document.querySelector('.navbar-inverse');
    element.classList.remove('navbar');
    this.myApp.imgname = 0;
    this.country = this.route.snapshot.params.country;
    let dataExist = 1;
    this.symbol = this.symbols[this.country]
    this.currencysymbol = this.currencysymbols[this.country]
    this.myApp.setDefaultValue(this.country)
    this.userService.apiData$.subscribe(async portfoliodata => {
      if (portfoliodata == null) {
        await this.userService.getPortfoliosList(this.country)
        await this.userService.apiData$.subscribe(async portfoliodata1 => this.portfolio_list = portfoliodata1)
        await this.userService.portfolioName$.subscribe(async portfolioname => { this.selectedPortfolio = portfolioname })
        if (portfoliodata != null)
          this.getAnalystTargetPriceData(this.country, this.selectedPortfolio.portfolio_name);
      }
      else {
        this.portfolio_list = portfoliodata;
        if (portfoliodata.length != 0) {
          await this.userService.portfolioName$.subscribe(async portfolioname => { this.selectedPortfolio = portfolioname })
          this.getAnalystTargetPriceData(this.country, this.selectedPortfolio.portfolio_name);
        }
        else {
          this.emptyStocks = true;
          this.loading = false;
        }
      }
    });
    
    this.metaService.addTags([
      { name: 'keywords', content: '72pianalysttargetprice ,72piuppotential ,72pidownpotential ,72pistockdownpotential ,72pistockuppotential,analysttargetprice ,uppotential ,downpotential ,stockdownpotential ,stockuppotential' },
      { name: 'description', content: 'It helps the subscriber in evaluating the stocks in the portfolio against average up/down target prices by analyst.' },
    ]);

  }
  sortTableData(sort: Sort, tableName) {
    if (sort.active && sort.direction !== '') {
      this.analystTargetPriceDataSource[tableName] = this[tableName].sort((a, b) => {
        const isAsc = (sort.direction === 'asc');
        return this._compare(a[sort.active], b[sort.active], isAsc);
      });
    }
    this[tableName] = Object.assign([], this.analystTargetPriceDataSource[tableName]);
  }
  private _compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a > b ? 1 : -1) * (isAsc ? 1 : -1);
  }
  getStockInfopage(selectedStock, selectedSymbol) {
    let pagename = "/StockInfoPage/";
    this.router.navigate([]).then(result => { window.open(this.country + pagename + "?stockname=" + (selectedStock + " (" + selectedSymbol + ")").split("&").join("`"), '_blank', 'noopener') });
  }
}

