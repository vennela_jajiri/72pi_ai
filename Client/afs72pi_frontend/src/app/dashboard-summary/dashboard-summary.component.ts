import { Component, OnInit,Injectable} from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute,Router } from '@angular/router';
import { AppComponent } from '../app.component';
import { PageService } from '../service/page.service';
import * as Highcharts from "highcharts/highstock";
import { UserPortfolioService } from '../service/userPortfolios.service';
import { Sort } from '@angular/material/sort';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { NavbarComponent } from '../navbar/navbar.component';
import { Title, Meta } from '@angular/platform-browser';
@Component({
    selector: 'app-dashboard-summary',
    templateUrl: './dashboard-summary.component.html',
    styleUrls: ['./dashboard-summary.component.scss'],
})
export class DashboardSummaryComponent implements OnInit {
    country: string; username: string;
    displayPerfomanceMetricsTableColumns1;
    displayPerfomanceMetricsTableColumns2;
    displayStyleMeasuresTableColumns;
    performanceMetrics1: MatTableDataSource<any> = new MatTableDataSource();
    performanceMetrics2: MatTableDataSource<any> = new MatTableDataSource();
    portfolioHoldings: MatTableDataSource<any> = new MatTableDataSource();
    stockBystock: MatTableDataSource<any> = new MatTableDataSource();
    styleMeasures: MatTableDataSource<any> = new MatTableDataSource();
    cumulative_returns;
    investmentAmtSector;
    investmentAmtMcapr;
    flag = 0;
    Highcharts = Highcharts;
    userPortfolios; 
    portfolioName: string;
    selectedPortfolio ;
    stockscount;
    sectorcount;
    returns_summary;
    risk_summary;
    investment_summary;
    cumulative_return;
    stockwise_performers;
    portfolio_cumulative_return ;
    nifty_cumulative_return ;
    best_stock;
    best_stock_return ;
    best_stock_investment ;
    worst_stock;
    worst_stock_return ;
    worst_stock_investment ;
    best_sector;
    best_sector_return ;
    best_sector_investment;
    worst_sector;
    worst_sector_return;
    worst_sector_investment ;
    portfolio_volatility ;
    nifty_volatility ;
    highest_investment_sector_amount;highest_beta_val;
    top_3_stocks ;beta;
    loading : boolean = true;
    divloading : boolean;
  errorMsg: string;
  dashboardSummaryFullData;
    symbols = {'India':'en-IN','US':'en-US'}
    currencysymbols = {'India':'₹','US':'$'}
    symbol:string;
    currencysymbol:string;
    emptyStocks: boolean = false;
    layer: string;
    frequency: string;
    dailyScale = [-3, -2, -1, 0, 1, 2, 3];
    size: string;
    parent = { "0": "GICS_Sector", "1": "GICS_Sector", "2": "GICS_Industry_Group", "3": "GICS_Industry", "4": "GICS_Sector", "5": "GICS_Sector", "6": "GICS_Sector", "7": "GICS_Sector", "8": "GICS_Industry_Group", "9": "GICS_Industry_Group", "10": "GICS_Industry" };
    child = { "0": "Gics_SubIndustry_Name", "1": "GICS_Industry_Group", "2": "GICS_Industry", "3": "Gics_SubIndustry_Name", "4": "Gics_SubIndustry_Name", "5": "GICS_Industry_Group", "6": "GICS_Industry", "7": "Gics_SubIndustry_Name", "8": "GICS_Industry", "9": "Gics_SubIndustry_Name", "10": "Gics_SubIndustry_Name" };
    FUSIONdataSource: Object = {};
    high_inv_percnet;high_inv_amnt;hihest_beta_stock;
    contentFlag = true; errorFlag = false;

    constructor(private titleService: Title, private metaService: Meta,  private navbar:NavbarComponent,private DashboardSummaryService: PageService, private route: ActivatedRoute,private http: HttpClient,
    private router: Router,  public myApp: AppComponent, private userService: UserPortfolioService,) { }
    
    DrawTreeMap() {
        this.layer = (document.getElementById("layer") as HTMLSelectElement).value;
        this.size = (document.getElementById("size") as HTMLSelectElement).value;
        this.frequency = (document.getElementById("frequency") as HTMLSelectElement).value;
        this.DashboardSummaryService.getTreeMapData(this.frequency, this.dailyScale, this.size, this.parent[this.layer], this.child[this.layer], this.layer, this.country,this.selectedPortfolio.portfolio_name).subscribe(FusionChartData => {
            this.FUSIONdataSource = {
            "chart": {
                "hideTitle": "1",
                "plotToolText": "<div><b>$label</b><br/></div>",
                "horizontalPadding": "0",
                "verticalPadding": "0",
                "plotborderthickness": ".5",
                "plotbordercolor": "ffffff",
                "chartBottomMargin": "0",
                "chartLeftMargin": "0",
                "chartRightMargin": "0",
                "labelGlow": "0",
                "labelDisplay": "auto",
                "showLegend": "1",
                "algorithm": "squarified",
                "theme": "fusion",
                "subcaption": null,
                "legendCaption": null,
                "showChildLabels": "1",
                "labelFontColor": "FFFFFF",
             
                "exportEnabled": 1,
                "exportFileName": this.selectedPortfolio.portfolio_name,
                // "baseFontSize": "14",
            },
            "data": [{
                "label": this.selectedPortfolio.portfolio_name,
                "fillcolor": "000000",
                "value": FusionChartData['mcap'],
                "data": FusionChartData['gics_list']
            }]
            }
        });
        };
    
    onPortfolioChange() {
        this.divloading = true;
        this.userService.setPortfolioName(this.selectedPortfolio)
        this.getDashboardSummaryData(this.country, this.selectedPortfolio.portfolio_name);
    }
    isBig =window.innerWidth>1279;
    legendBig = {
    margin: 0,
    padding: 0,
    borderWidth: 0,
    width: 400,
    itemWidth: 200,
    itemStyle: {
        color: '#031B4E',
        fontSize: '12px',
        fontWeight: 'bold',
        textOverflow: null 

    },
    };
    legendSmall = {
    margin: 0,
    padding: 0,
    itemStyle: {
        color: '#031B4E',
        fontSize: '12px',
        fontWeight: 'bold',
        textOverflow: null 

    },
    }
    private getServerErrorMessage(error: HttpErrorResponse): string {
        switch (error.status) {
            case 401: {
                return '401';
            }
            case 402: {
                return '402';
            }
            case 403: {
                return '403';
            }
            case 404: {
              return '404';
            }
          
            case 405: {
              return '405';
            }
            case 406: {
              return '406';
            }
            case 412: {
              return '412';
            }
            case 500: {
              return '500';
            }
            case 501: {
              return '501';
            }
            case 502: {
              return '502';
            }
            default: {
                return '500';
            }
    
        }
      }
    ngOnInit() {
        window.scroll(0, 0);
        let element = document.querySelector('.navbar-inverse');
        element.classList.remove('navbar');
        this.myApp.imgname = 0;
        this.country = this.route.snapshot.params.country;
        this.myApp.setDefaultValue(this.country)
        this.userService.apiData$.subscribe(async portfoliodata => {
            if (portfoliodata == null) {
                await this.userService.getPortfoliosList(this.country)
                await this.userService.apiData$.subscribe(async portfoliodata1 => this.userPortfolios = portfoliodata1)
                await this.userService.portfolioName$.subscribe(async portfolioname => { this.selectedPortfolio = portfolioname })
                if (portfoliodata != null){
                    this.getDashboardSummaryData(this.country, this.selectedPortfolio.portfolio_name);
                    this.DrawTreeMap();}
                
            }
            else {
                this.userPortfolios = portfoliodata;
                    if(portfoliodata.length!=0){
                        await this.userService.portfolioName$.subscribe(async portfolioname => { this.selectedPortfolio = portfolioname })
                        this.getDashboardSummaryData(this.country, this.selectedPortfolio.portfolio_name);
                        this.DrawTreeMap();
                    
                    }
                    else{
                        this.emptyStocks = true;
                        this.loading = false;
                    }
               
            }
        });
        
this.titleService.setTitle('Dashboard Summary');
this.metaService.addTags([
  {name: 'keywords', content: '72pidashboard, 72piportfoliodashboard, 72piportfolioperformancemetrics  , 72piportfoliostylemeasures,dashboard,portfoliodashboard,portfolioperformancemetrics  ,portfoliostylemeasures'},
  {name: 'description', content: 'It is a dashboard of performance summary of the portfolio in the last 1 year'},
]);
    }
    getDashboardSummaryData(country, portfolio_name) {
        this.DashboardSummaryService.getDashboardSummaryViews(country, portfolio_name).subscribe(dashboardSummaryData => {
            this.contentFlag = true;
            this.errorFlag = false;
            this.titleService.setTitle('Dashboard Summary');

            if(dashboardSummaryData['status']==1){
            this.dashboardSummaryFullData = dashboardSummaryData;  
            this.portfolioName = portfolio_name;
            this.stockscount = dashboardSummaryData['stocks_count'];

            this.sectorcount = dashboardSummaryData['sector_count'];
            var graphsList = this.plotChart(dashboardSummaryData['cumulative_returns'], dashboardSummaryData['sector_wise'], dashboardSummaryData['mcap_wise']);
            this.cumulative_returns = graphsList[0];
            this.investmentAmtSector = graphsList[1];
            this.investmentAmtMcapr = graphsList[2];

            this.username = dashboardSummaryData['username']
            this.performanceMetrics1 = dashboardSummaryData['table_data1'];
            this.performanceMetrics2 = dashboardSummaryData['table_data2'];
            this.displayPerfomanceMetricsTableColumns1 = dashboardSummaryData['tableColumns1'];
            this.displayPerfomanceMetricsTableColumns2 = dashboardSummaryData['tableColumns2'];

            this.portfolioHoldings = dashboardSummaryData['stock_df'];
            this.stockBystock = dashboardSummaryData['risk_monitor'];
            this.styleMeasures = dashboardSummaryData['style_measures'];
            this.displayStyleMeasuresTableColumns = dashboardSummaryData['styleMeasuresColumns'];
            this.flag = 1;

            this.returns_summary = dashboardSummaryData['returns_summary']
            this.risk_summary = dashboardSummaryData['risk_summary']
            this.investment_summary = dashboardSummaryData['investment_summary']

            this.portfolio_cumulative_return = this.returns_summary[0][0];
            this.nifty_cumulative_return = this.returns_summary[0][1];

            this.best_stock = this.returns_summary[1][0];
            this.best_stock_investment = this.returns_summary[1][1];
            this.best_stock_return = this.returns_summary[1][2];

            this.worst_stock = this.returns_summary[2][0];
            this.worst_stock_investment = this.returns_summary[2][1];
            this.worst_stock_return = this.returns_summary[2][2];

            this.best_sector = this.returns_summary[3][0];
            this.best_sector_investment = this.returns_summary[3][1];
            this.best_sector_return = this.returns_summary[3][2];

            this.worst_sector = this.returns_summary[4][0];
            this.worst_sector_investment = this.returns_summary[4][1];
            this.worst_sector_return = this.returns_summary[4][2];

            this.portfolio_volatility = this.risk_summary[0][0];
            this.nifty_volatility = this.risk_summary[1][0];
         
            this.beta = this.risk_summary[2];
            this.hihest_beta_stock = this.risk_summary[3][0];
            this.highest_beta_val = this.risk_summary[3][1];

            this.high_inv_amnt = this.investment_summary[0][0]
            this.highest_investment_sector_amount = this.investment_summary[0][1];
            this.high_inv_percnet = this.investment_summary[0][2];
            this.top_3_stocks = this.investment_summary[1]; 
            this.loading = false;
            this.symbol = this.symbols[this.country];
            this.currencysymbol = this.currencysymbols[this.country];
            this.divloading = false;
            this.DrawTreeMap();

            }
            else{
                this.emptyStocks = true;
                this.loading = false;
            }
       
        }, error => {
            if (error.error instanceof ErrorEvent) {
              this.errorMsg = `Error: ${error.error.message}`;
            }
            else {
              this.contentFlag = false;
              this.errorFlag = true;
              this.loading = false;
              this.errorMsg = this.getServerErrorMessage(error);
            }
          })
    }
    sortData(sort: Sort,tableName){
        if (sort.active && sort.direction !== '')
            {
              this.dashboardSummaryFullData[tableName] = this[tableName].sort((a, b) => {     
              const isAsc = (sort.direction === 'asc');      
              return this._compare(a[sort.active], b[sort.active], isAsc);
              });    
            }
            this[tableName] = Object.assign([], this.dashboardSummaryFullData[tableName]);   
      }
      private _compare(a: number | string, b: number | string, isAsc: boolean) {
        return (a > b ? 1 : -1) * (isAsc ? 1 : -1);
      }
    plotChart(inputdata, sector_wise, mcap_wise) {
        for (let i = 0; i < inputdata.length; i++) {
            for (let j = 0; j < inputdata[i]['data'].length; j++) {
                let date = new Date(inputdata[i]['data'][j][0]);
                var now_utc = Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(),
                    date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds());
                inputdata[i]['data'][j][0] = now_utc;
            }
        }
        var cumulative = {
            chart: {
                type: 'line',
                backgroundColor: "transparent",
            },
            exporting: {
                enabled: false,
                fallbackToExportServer: false
            },
            title: {
                text: null,

            },
            credits: {
                enabled: false
            },
            xAxis: {
                type: 'datetime',
                dateTimeLabelFormats: { // don't display the dummy year
                    day: '%e-%b-%y',
                    week: '%e-%b-%y',
                    month: '%b-%y',
                    year: '%Y'

                },
                labels: {
                    style: {
                        color: '#031b4e',
                        font: '13px  ',
                        fontWeight: 'normal'
                    },
                },
                tickLength: 6,
                tickWidth: 1,
                min: inputdata[0]['data'][0][0],
                startOnTick: true,
                endOnTick: false,

            },
            yAxis: {
                gridLineWidth: 0,
                lineWidth: 1,
                tickLength: 6,
                tickWidth: 1,
                labels: {
                    formatter: function () {
                        return (this.value * 100).toFixed(0) + '%';
                    },
                    style: {
                        color: '#031b4e',
                        font: '13px  ',
                        fontWeight: 'normal'
                    },
                },
                title: {
                    text: "Return",
                    style: {
                        color: '#031b4e',
                        font: '13px  ',
                        fontWeight: 'normal'
                    },
                },
                plotLines: [{
                    color: '#F2F2F2',
                    width: 2,
                    value: 0
                }]
            },
            tooltip: {

                formatter: function () {
                    var s = [];
                    var count = 0
                    for (let i = 0; i < this.points.length; i++) {
                        if (count == 0)
                            s.push('Date: <b>' + Highcharts.dateFormat('%d-%b-%Y', this.x) + '</b><br>');
                        count = 1


                        s.push(this.points[i].series.name + ' : <b>' +
                            (this.points[i].y * 100).toFixed(1) + '%</b><br>');



                    }

                    return s.join('<br>');
                },
                shared: true,
                style: {
                    color: '#031b4e',
                    font: '13px  ',
                    fontWeight: 'normal'
                },
            },
            plotOptions: {
                series: {
                    marker: {
                        enabled: false,
                        symbol: "circle"
                    }
                }
            },
            legend:
            {
                itemStyle: {
                    color: '#031B4E',
                    fontFamily: ' ',
                    fontSize: '14px'

                },
            },
            series: inputdata,

        }
        var investmentAmtSector = {
            chart: {
                type: 'pie',
                backgroundColor: 'transparent',
                // height: (9 / 9 * 100) + '%',
                
            },
            credits: {
                enabled: false
            },
            exporting: {
                buttons: {
                    contextButton: {
                        enabled: false
                    },
                }
            },
            title: {
                style: {
                    color: '#031b4e',
                    fontSize: '12px',
                    fontWeight: 'bold',
                },
                text: null,
            },
            legend:  this.isBig? this.legendBig : this.legendSmall,
            tooltip: {
                formatter: function () {
                    return 'Sector : <b>' + this.point.name + '</b><br>' + 'Investment Amount(%) : ' +
                        '<b>' + (this.point.y*100).toFixed(1) + '%</b><br>' + 'Investment Amount : <b>' + (
                            this.point.Exposure).toLocaleString('hi-IN', {
                                maximumFractionDigits: 1
                            }) + '<br></b>';
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    innerSize: "60%",
                    size: '220',
                    depth: "65%",
                    showInLegend: true,
                    dataLabels: {
                        enabled: false,
                        formatter: function () {
                            return '<b>' + this.point.name + '</b><br>';
                        },
                    }
                },
            },
            series: [{
                name: 'By Sector',
                data: sector_wise,
            }],
        };
        var investmentAmtMcap = {
            chart: {
                type: 'pie',
                backgroundColor: 'transparent',
                // height: (9 / 9 * 100) + '%',

            },
            credits: {
                enabled: false
            },
            exporting: {
                buttons: {
                    contextButton: {
                        enabled: false
                    },

                }
            },
            title: {
                style: {
                    color: '#031b4e',
                    fontSize: '12px',
                    fontWeight: 'bold',
                },
                text: null,
            },

            legend: {
                // layout:"vertical",
                // verticalAlign:"middle",
                // align:'right',
                itemStyle: {
                    color: '#031B4E',
                    fontSize: '12px',
                    fontWeight: 'bold',
                    textOverflow: null 

                },
            },
            tooltip: {
                formatter: function () {

                    return 'Market Cap : <b>' + this.point.name + '</b><br>' + 'Investment Amount(%) : ' +
                        '<b>' + (this.point.y).toFixed(2) + '%</b><br>' + 'Investment Amount : <b>' + (
                            this.point.Exposure).toLocaleString('hi-In', {
                                maximumFractionDigits: 1
                            }) + '<br></b>';
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    innerSize: "60%",
                    depth: "65%",
                    size: '220',
                    showInLegend: true,

                    dataLabels: {
                        enabled: false,

                        formatter: function () {
                            return '<b>' + this.point.name + '</b><br>';
                        },
                    }
                },
            },
            series: [{
                name: 'Country',
                colorByPoint: true,
                data: mcap_wise,

            }],
        }
        return [cumulative, investmentAmtSector, investmentAmtMcap];
    }
    getStockInfopage(selectedStock,selectedSymbol){
        let pagename = "/StockInfoPage/";
        this.router.navigate([]).then(result => {  window.open( this.country + pagename + "?stockname=" + (selectedStock+" ("+selectedSymbol+")").split("&").join("`") , '_blank','noopener') });
      }

}
