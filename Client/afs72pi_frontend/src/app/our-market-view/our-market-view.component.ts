import { Component, OnInit,Injectable } from '@angular/core';
import { PageService } from '../service/page.service';
import { AppComponent } from '../app.component';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { NavbarComponent } from '../navbar/navbar.component';
import { Title, Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-our-market-view',
  templateUrl: './our-market-view.component.html',
  styleUrls: ['./our-market-view.component.scss'],
})
export class OurMarketViewComponent implements OnInit {
  marketViews : any = [];
  country : string;
  errorMsg: string;
  contentFlag = true; errorFlag = false;
  
  constructor(private titleService: Title, private metaService: Meta,
              private navbar:NavbarComponent,private pageService: PageService,
              public myApp: AppComponent, private router: Router,
              private activeRoute: ActivatedRoute,private http: HttpClient) {}
              private getServerErrorMessage(error: HttpErrorResponse): string {
                switch (error.status) {
                    case 401: {
                        return '401';
                    }
                    case 402: {
                        return '402';
                    }
                    case 403: {
                        return '403';
                    }
                    case 404: {
                      return '404';
                    }
                  
                    case 405: {
                      return '405';
                    }
                    case 406: {
                      return '406';
                    }
                    case 412: {
                      return '412';
                    }
                    case 500: {
                      return '500';
                    }
                    case 501: {
                      return '501';
                    }
                    case 502: {
                      return '502';
                    }
                    default: {
                        return '500';
                    }
            
                }
              }
  ngOnInit() {
    window.scroll(0,0);
    let element = document.querySelector('.navbar-inverse');
    element.classList.remove('navbar');
    this.myApp.imgname = 0;
    this.country = this.activeRoute.snapshot.params.country;
    this.myApp.setDefaultValue(this.country);
    this.pageService.getmarketViews(this.country).subscribe(marketViews=>{
      this.titleService.setTitle('Our Market View');
      this.contentFlag = true;
      this.errorFlag = false;
      this.marketViews=marketViews;
    },error => {
      if (error.error instanceof ErrorEvent) {
        this.errorMsg = `Error: ${error.error.message}`;
      }
      else {
        this.contentFlag = false;
        this.errorFlag = true;
        this.errorMsg = this.getServerErrorMessage(error);
      }
    });

    this.titleService.setTitle('Our Market View');
    this.metaService.addTags([
      {name: 'keywords', content: '72PIpoints , 72pimarketview , points , marketview'},
      {name: 'description', content: '72PI points is the market outlook which is updated twice a month'},
    ]);

  }
}