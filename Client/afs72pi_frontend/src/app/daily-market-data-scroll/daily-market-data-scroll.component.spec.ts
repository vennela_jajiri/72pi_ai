import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DailyMarketDataScrollComponent } from './daily-market-data-scroll.component';

describe('DailyMarketDataScrollComponent', () => {
  let component: DailyMarketDataScrollComponent;
  let fixture: ComponentFixture<DailyMarketDataScrollComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DailyMarketDataScrollComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DailyMarketDataScrollComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
