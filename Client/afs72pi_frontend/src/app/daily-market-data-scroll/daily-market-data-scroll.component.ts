import { Component, OnInit } from '@angular/core';
import { PageService } from '../service/page.service';

@Component({
  selector: 'app-daily-market-data-scroll',
  templateUrl: './daily-market-data-scroll.component.html',
  styleUrls: ['./daily-market-data-scroll.component.scss'],
})
export class DailyMarketDataScrollComponent implements OnInit {

  constructor(private pageService: PageService,) { }
  market_data;
  ngOnInit() {
    this.pageService.getScrollData().subscribe(subscribedData => {
      this.market_data = subscribedData['market_data'];
    });
  }
 
}
