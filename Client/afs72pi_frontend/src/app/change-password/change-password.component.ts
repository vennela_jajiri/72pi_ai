import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../service/auth.service';
import { Router } from '@angular/router';
import { AppComponent } from '../app.component';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss'],
})
export class ChangePasswordComponent implements OnInit {
  hide = true;
  error;
  changePasswordForm: FormGroup = new FormGroup({
    oldpassword: new FormControl(''),
    newpassword: new FormControl('',[Validators.required,Validators.min(3) ]),
    confirmpassword: new FormControl('',[Validators.required, Validators.min(3) ]),
  })

  constructor(private authService: AuthService,
    private router : Router,
    public myApp: AppComponent,) { }

  ngOnInit() {
    window.scroll(0,0);
    let element = document.querySelector('.navbar-inverse');
    element.classList.remove('navbar');
    this.myApp.imgname = 0;
    this.changePasswordForm = new FormGroup({
      oldpassword : new FormControl(''),
      newpassword: new FormControl(''),
      confirmpassword: new FormControl(''),
    });
  }

}
