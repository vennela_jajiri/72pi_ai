import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppComponent } from '../app.component';

@Component({
  selector: 'app-order-success-failure',
  templateUrl: './order-success-failure.component.html',
  styleUrls: ['./order-success-failure.component.scss'],
})
export class OrderSuccessFailureComponent implements OnInit {

  public subscriptionDetails : any;
  public paramsDetails : any;
  public success : boolean;
  public error :boolean;
  public country : string;
  constructor(private activeRoute: ActivatedRoute,
              private router : Router,
              private myApp : AppComponent) { }

  ngOnInit() {
    this.subscriptionDetails = JSON.parse(localStorage.getItem('subscription_details'));
    this.activeRoute.queryParams.subscribe(params => {
      this.paramsDetails = params;
    });
    if(this.paramsDetails){
      if(this.paramsDetails['payment_verification'] =='success' && this.paramsDetails['subscription'] == 'activated'){
      this.success = true;
      this.error = false;
      }else{
      this.success = false;
      this.error = true;
      }
    }else{
      this.country = this.activeRoute.snapshot.params.country;
      this.myApp.setDefaultValue(this.country);
      this.router.navigate([this.country+'/pricing-details']);
    }
  }
}
