import { Component, OnInit, Injectable } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { AppComponent } from '../app.component';
import { UserPortfolioService } from '../service/userPortfolios.service';
import { MatTabChangeEvent } from '@angular/material/tabs';
import * as Highcharts from 'highcharts'
import { lineChart } from 'src/charts/lineChart';
import { Chart } from 'angular-highcharts';
import More from 'highcharts/highcharts-more';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Sort } from '@angular/material/sort';
import { NavbarComponent } from '../navbar/navbar.component';
import { Title, Meta } from '@angular/platform-browser';
More(Highcharts);
import Tree from 'highcharts/modules/treemap';
Tree(Highcharts);
import Heatmap from 'highcharts/modules/heatmap';
Heatmap(Highcharts);
// Load the exporting module.
import Exporting from 'highcharts/modules/exporting';
// Initialize exporting module.
Exporting(Highcharts);
import { PageService } from '../service/page.service';

@Component({
  selector: 'app-portfolio-returns',
  templateUrl: './portfolio-returns.component.html',
  styleUrls: ['./portfolio-returns.component.scss'],
})
@Injectable({
  providedIn: 'root'
})

export class PortfolioReturnsComponent implements OnInit {
  country: string;
  FUSIONdataSource: Object = {};
  isloaded = true;
  userPortfolios: any[];
  portfolio_list: any[];
  parent = { "0": "GICS_Sector", "1": "GICS_Sector", "2": "GICS_Industry_Group", "3": "GICS_Industry", "4": "GICS_Sector", "5": "GICS_Sector", "6": "GICS_Sector", "7": "GICS_Sector", "8": "GICS_Industry_Group", "9": "GICS_Industry_Group", "10": "GICS_Industry" };
  child = { "0": "Gics_SubIndustry_Name", "1": "GICS_Industry_Group", "2": "GICS_Industry", "3": "Gics_SubIndustry_Name", "4": "Gics_SubIndustry_Name", "5": "GICS_Industry_Group", "6": "GICS_Industry", "7": "Gics_SubIndustry_Name", "8": "GICS_Industry", "9": "Gics_SubIndustry_Name", "10": "Gics_SubIndustry_Name" };
  PortfolioReturnsData: any;
  modelPortfoliosMonthlyData: any;
  portfolio_name: string;
  displayedColumns = ['CompanyName', 'Sector', 'MarketCapCategory', 'Quantity', 'Price', 'Market_Value', 'Exposure'];
  displayedColumnsGraphs = ['Portfolio_Benchmark', 'Annualized_Return', 'Annualized_Volatility', 'Sharpe', 'Maximum_DrawDown'];
  MonthlyPerfomanceTableColumns: any[];
  header_value: string;
  dataSource: MatTableDataSource<any> = new MatTableDataSource();
  metricsTable1: MatTableDataSource<any> = new MatTableDataSource();
  metricsTable2: MatTableDataSource<any> = new MatTableDataSource();
  performanceTable: MatTableDataSource<any> = new MatTableDataSource();
  ror_metricsTable: MatTableDataSource<any> = new MatTableDataSource();
  PerformanceTable: MatTableDataSource<any> = new MatTableDataSource();
  stock_investemnt_data; stock_return_data; stock_pnl_data;
  sector_investemnt_data; sector_return_data; sector_pnl_data;
  maxDate = new Date();
  minDate = new Date("2017-01-01")
  displayTableColumns1 = [];
  displayTableColumns2 = [];
  currencysymbols = { 'India': '₹', 'US': '$' }
  formater = { 'India': 'hi-IN', 'US': 'en-US' }
  IndexValue;
  currencysymbol: string;
  displayPerfomanceTableColumns = [];
  PerformanceTableColumns = [];
  monthlyChartData;
  benchmarks = [];
  size: string;
  layer: string;
  frequency: string;
  dailyScale = [-3, -2, -1, 0, 1, 2, 3];
  weeklyScale = [-6, -4, -2, 0, 2, 4, 6]
  monthlyScale = [-10, -6.7, -3.3, 0, 3.3, 6.7, 10]
  config; userName; currentPeriod; startDate;
  selectedPortfolio;
  loading: boolean = true;
  divloading: boolean;
  emptyStocks: boolean = false;
  errorMsg: string;
  cumulative_returns;
  contentFlag = true; errorFlag = false;

  constructor(
    private titleService: Title, private metaService: Meta,  private navbar:NavbarComponent,private pageService: PageService, private route: ActivatedRoute, private router: Router, public myApp: AppComponent, public userService: UserPortfolioService,private http: HttpClient) { }
  DrawTreeMap() {
    this.layer = (document.getElementById("layer") as HTMLSelectElement).value;
    this.size = (document.getElementById("size") as HTMLSelectElement).value;
    this.frequency = (document.getElementById("frequency") as HTMLSelectElement).value;
    this.pageService.getTreeMapData(this.frequency, this.dailyScale, this.size, this.parent[this.layer], this.child[this.layer], this.layer, this.country,this.selectedPortfolio.portfolio_name).subscribe(FusionChartData => {
      this.FUSIONdataSource = {
        "chart": {
          "hideTitle": "1",
          "plotToolText": "<div><b>$label</b><br/></div>",
          "horizontalPadding": "0",
          "verticalPadding": "0",
          "plotborderthickness": ".5",
          "plotbordercolor": "ffffff",
          "chartBottomMargin": "0",
          "chartLeftMargin": "0",
          "chartRightMargin": "0",
          "labelGlow": "0",
          "labelDisplay": "auto",
          "showLegend": "1",
          "algorithm": "squarified",
          "theme": "fusion",
          "subcaption": null,
          "legendCaption": null,
          "showChildLabels": "1",
          "labelFontColor": "FFFFFF",
          "exportEnabled": 1,
          "exportFileName": this.selectedPortfolio.portfolio_name,
        },
        "data": [{
          "label": this.selectedPortfolio.portfolio_name,
          "fillcolor": "000000",
          "value": FusionChartData['mcap'],
          "data": FusionChartData['gics_list']
        }]
      }
    });
  };
  private getServerErrorMessage(error: HttpErrorResponse): string {
    switch (error.status) {
        case 401: {
            return '401';
        }
        case 402: {
            return '402';
        }
        case 403: {
            return '403';
        }
        case 404: {
          return '404';
        }
      
        case 405: {
          return '405';
        }
        case 406: {
          return '406';
        }
        case 412: {
          return '412';
        }
        case 500: {
          return '500';
        }
        case 501: {
          return '501';
        }
        case 502: {
          return '502';
        }
        default: {
            return '500';
        }

    }
  }
  onPortfolioChange() {
    this.divloading = true;
    this.userService.setPortfolioName(this.selectedPortfolio);
    this.getDataForPortfolioReturns(this.country, this.selectedPortfolio.portfolio_name, 0, this.currentPeriod);
  };
  onChange(event: MatTabChangeEvent) {
    const tab = event.tab.textLabel;
    this.currentPeriod = event.tab.textLabel;
    this.getDataForPortfolioReturns(this.country, this.selectedPortfolio.portfolio_name, 1, tab);
  }
  onIndexChange(event)
  {
    this.IndexValue=event.target.value;
    this.monthlyPerformance('monthlyReturns',this.monthlyChartData,this.PortfolioReturnsData[event.target.value+'_monthly']['data'],event.target.value)
  }
  onDateChange(event)
  {
    var input_date = new Date(event);
    let custom_date = [input_date.getFullYear(), ("0" + (input_date.getMonth() + 1)).slice(-2),("0" + input_date.getDate()).slice(-2)].join("-");
    this.startDate = new Date(event);
    this.getDataForPortfolioReturns(this.country,this.selectedPortfolio.portfolio_name,1,'Custom',custom_date)
  }
  ngOnInit() {
    window.scroll(0, 0);
    let element = document.querySelector('.navbar-inverse');
    element.classList.remove('navbar');
    this.myApp.imgname = 0;
    this.country = this.route.snapshot.params.country;

    this.navbar.setDefaultValue(this.country);
    this.userService.apiData$.subscribe(async portfoliodata => {
      if (portfoliodata == null) {
        await this.userService.getPortfoliosList(this.country);
        await this.userService.apiData$.subscribe(async portfoliodata => this.portfolio_list = portfoliodata)
        await this.userService.portfolioName$.subscribe(async portfolioname => { this.selectedPortfolio = portfolioname })
        if (portfoliodata != null){
          this.getDataForPortfolioReturns(this.country, this.selectedPortfolio.portfolio_name, 0, this.currentPeriod);
          this.DrawTreeMap();}
      }
      else {
        this.portfolio_list = portfoliodata;
        if(portfoliodata.length!=0){
          await this.userService.portfolioName$.subscribe(async portfolioname => { this.selectedPortfolio = portfolioname })
          this.getDataForPortfolioReturns(this.country, this.selectedPortfolio.portfolio_name, 0, this.currentPeriod);
          this.DrawTreeMap();
        }
        else{
          this.emptyStocks = true;
          this.loading = false; 
        }
      }
    });
    this.titleService.setTitle('Portfolio Returns');
    this.metaService.addTags([
      {name: 'keywords', content: '72piportfoliosharpe ,72piportfoliodrawdown ,72pireturnbystock ,72piinvestmentbystock ,72piinvestmentamountbystock ,72pireturnby ,72pirelativeperformance ,72pioutperformbenchmark ,72piunderperformbenchmark ,72piportfolioallocation ,72piportfolioallocationbystock ,72piportfolioallocationbysector ,72piportfolioallocationbymarketcap ,72piportfolioallocationbymarketcapitalisation ,72pistockallocation ,72pisectorallocation ,72pimarketcapallocation ,72pimarketcapitalisationallocation,,portfoliosharpe ,portfoliodrawdown ,returnbystock ,investmentbystock ,investmentamountbystock ,returnby ,relativeperformance ,outperformbenchmark ,underperformbenchmark ,portfolioallocation ,portfolioallocationbystock ,portfolioallocationbysector ,portfolioallocationbymarketcap ,portfolioallocationbymarketcapitalisation ,stockallocation ,sectorallocation ,marketcapallocation ,marketcapitalisationallocation'},
      {name: 'description', content: 'Portfolio returns helps our subscribers in comparing their portfolio performance with benchmark indices. It helps the subscriber in understanding the relative performance of the portfolio with respect to indices and the portfolio allocation by stock, sector and market capitalisation.'},
    ]);


  }
  getDataForPortfolioReturns(country, portfolio_name, onchangevalue, period = "YTD", custom_date = "") {
    this.isloaded = true;
    this.pageService.getPortfolioReturnsViews(country, portfolio_name, period, onchangevalue, custom_date).subscribe(PortfolioReturnsDataMain => {
      this.PortfolioReturnsData = PortfolioReturnsDataMain;
      this.contentFlag = true;
      this.errorFlag = false;
      this.titleService.setTitle('Portfolio Returns');

      if (PortfolioReturnsDataMain['status']==1) {
      if (onchangevalue == 0) {
        this.ror_metricsTable= this.PortfolioReturnsData['ror_metrics'];
        this.performanceTable=this.PortfolioReturnsData['monthly_cumulative'];
        this.MonthlyPerfomanceTableColumns = this.PortfolioReturnsData['MonthlyperformancetableColumns']
        this.stock_investemnt_data = this.PortfolioReturnsData['stock_exposure_list']
        this.stock_return_data = this.PortfolioReturnsData['stock_return_list']
        this.stock_pnl_data = this.PortfolioReturnsData['stock_pnl_list']
        this.sector_investemnt_data = this.PortfolioReturnsData['sector_exposure_list']
        this.sector_return_data = this.PortfolioReturnsData['sector_return_list']
        this.sector_pnl_data = this.PortfolioReturnsData['sector_pnl_list']
        this.ReturnsByStockSector(this.stock_investemnt_data, this.stock_return_data, this.stock_pnl_data);
        this.performanceTable=this.PortfolioReturnsData['monthly_cumulative']
        this.displayPerfomanceTableColumns = this.PortfolioReturnsData['performancetableColumns']
        this.PerformanceTable = this.PortfolioReturnsData['performance_data']
        this.PerformanceTableColumns = this.PortfolioReturnsData['performancetableColumns'];
        this.benchmarks = this.PortfolioReturnsData['benchmarks'];
        for (let i=0;i<this.benchmarks.length;i++)
          this.convertDateToUTC(this.PortfolioReturnsData,this.benchmarks[i] + "_monthly",-1)
        this.convertDateToUTC(this.PortfolioReturnsData,'monthly_cumulative_chart_data',-1)
        this.monthlyChartData = this.PortfolioReturnsData['monthly_cumulative_chart_data']['data']
        this.monthlyPerformance('monthlyReturns', this.monthlyChartData, this.PortfolioReturnsData[this.benchmarks[0] + "_monthly"]['data'],this.benchmarks[0])

      }
      for (let i = 0; i < this.PortfolioReturnsData['cumulative_returns'].length; i++) {
        this.convertDateToUTC(this.PortfolioReturnsData, 'cumulative_returns', i)
      }
      this.header_value = "Portfolio Return - From " + period;
      this.plotCharts('cumulative_returns', [], '', 100, 1, "%", "%", "1");
      this.displayTableColumns1 = this.PortfolioReturnsData['tableColumns1']
      this.displayTableColumns2 = this.PortfolioReturnsData['tableColumns2']
      this.metricsTable1 = this.PortfolioReturnsData['table_data1'];
      this.metricsTable2 = this.PortfolioReturnsData['table_data2'];
      this.DrawTreeMap();

      this.isloaded = false;
      this.loading = false
      this.divloading = false
      this.currencysymbol=this.currencysymbols[this.route.snapshot.params.country]
    }
    else{
        this.emptyStocks = true;
        this.loading = false;
    }
    }, error => {
      if (error.error instanceof ErrorEvent) {
        this.errorMsg = `Error: ${error.error.message}`;
      }
      else {
        this.contentFlag = false;
        this.errorFlag = true;
        this.loading = false;
        this.errorMsg = this.getServerErrorMessage(error);
      }
    });
  }
   
  sortTableData(sort: Sort,tableName){
    if (sort.active && sort.direction !== '')
        {
          this.PortfolioReturnsData[tableName] = this[tableName].sort((a, b) => {     
          const isAsc = (sort.direction === 'asc');      
          return this._compare(a[sort.active], b[sort.active], isAsc);
          });    
        }
        this[tableName] = Object.assign([], this.PortfolioReturnsData[tableName]);   
  }
  private _compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a > b ? 1 : -1) * (isAsc ? 1 : -1);
  }
  convertDateToUTC(inputdata, dynamicColumn, extraIndex) {
    var data;
    if (extraIndex != -1)
      data = inputdata[dynamicColumn][extraIndex];
    else
      data = inputdata[dynamicColumn];
    for (let i = 0; i < data['data'].length; i++) {
      let date = new Date(data['data'][i][0]);
      var now_utc = Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(),
        date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds());
      data['data'][i][0] = now_utc;
    }
    return inputdata
  }
  monthlyPerformance(id, data, indexData,indexName) {
    Highcharts.chart(id, {
      title: {
        text: null
      },
      chart: {
        height: "370px"
      },
      credits: {
        enabled: false
      },
      exporting: {
        buttons: {
          contextButton: {
            enabled: false
          },
        }
      },
      xAxis: {
        type: 'datetime',
        dateTimeLabelFormats: { // don't display the dummy year
          day: '%e-%b-y',
          week: '%e-%b-%y',
          month: '%b-%y',
          year: '%Y'
        },
        labels: {
          style: {
            color: '#031B4E',
            font: '13px  ',
            fontWeight: 'normal'
          },
        },
        tickLength: 6,
        tickWidth: 1,
      },
      yAxis: {
        gridLineWidth: 0,
        lineWidth: 1,
        tickLength: 6,
        tickWidth: 1,
        title: {
          text: null
        },
        labels: {
          formatter: function () {
            return (this.value * 100).toFixed(0) + '%';
          }
        },
        plotLines: [{
          color: '#F2F2F2',
          width: 2,
          value: 0
        }]
      },
      legend: {
        enabled: true
      },
      plotOptions: {
        series: {
          marker: {
            enabled: false,
            symbol: "circle"
          }
        }
      },
      tooltip: {
        formatter: function () {
          var s = ["Date : " + '<b>' + Highcharts.dateFormat('%d-%b-%Y', this.x)];
          for (let i = 0; i < this.points.length; i++) {
            s.push(this.points[i].series.name + " : " + '<b>' + (this.points[i].point.y*100).toFixed(2) + '%</b>');
          }
          return s.join('<br>');
        },
        shared: true,
      },
      series: [{
        type: 'column',
        name: "Portfolio",
        data: data,
        color: '#002060'
      },
      { 
        type: 'spline',
        name: indexName,
        data: indexData,
        color: '#F79824'
      }
      ],
    });
  }
  changeBarData() {
    let filterBy = (document.getElementById("filterBy") as HTMLSelectElement).value.toLowerCase();
    this.ReturnsByStockSector(this[filterBy + "_investemnt_data"], this[filterBy + "_return_data"], this[filterBy + "_pnl_data"]);
  }
  sortData() {
    let orderBy = (document.getElementById("order") as HTMLSelectElement).value;
    let filterBy = (document.getElementById("filterBy") as HTMLSelectElement).value.toLowerCase();
    let dynamicColumn = { 'Actual Return(%)': 'return', 'Investment': 'investment', 'Actual Return': 'pnl' }[orderBy]
    this[filterBy + "_investemnt_data"] = this[filterBy + "_investemnt_data"].sort(function (a, b) { return (a[dynamicColumn] < b[dynamicColumn]) ? 1 : ((b[dynamicColumn] < a[dynamicColumn]) ? -1 : 0); });
    this[filterBy + "_pnl_data"] = this[filterBy + "_pnl_data"].sort(function (a, b) { return (a[dynamicColumn] < b[dynamicColumn]) ? 1 : ((b[dynamicColumn] < a[dynamicColumn]) ? -1 : 0); });
    this[filterBy + "_return_data"] = this[filterBy + "_return_data"].sort(function (a, b) { return (a[dynamicColumn] < b[dynamicColumn]) ? 1 : ((b[dynamicColumn] < a[dynamicColumn]) ? -1 : 0); });
    this.ReturnsByStockSector(this[filterBy + "_investemnt_data"], this[filterBy + "_return_data"], this[filterBy + "_pnl_data"]);
  }
  ReturnsByStockSector(investment_data, return_data, pnl_data) {
    var categories = ['1', '2']
    var H = Highcharts,
      flag = true,
      redrawWithNewHeight = function (config, width) {
        var number = 0
        H.each(config.series, function (p, i) {
          if (p.visible === true) {
            H.each(p.data, function (ob, j) {
              number += 1;
            });
          }
        });
        var height = width * number + config.plotTop + config.marginBottom;
        if (height !== config.chartHeight) {
          flag = true;
        }
        if (flag) {
          flag = false;
          config.setSize(config.chartWidth, height);
          config.hasUserSize = null;
        }
      };
    var temp = this.formater[this.route.snapshot.params.country];
    this.config = {
      chart: {
        renderTo: 'current_positions',
        inverted: true,
        spacingTop: 50,
        animation: false,
        backgroundColor: "transparent",
        events: {
          load: function () {
            redrawWithNewHeight(this, 10);
          }
        },
      
      },
      credits: {
        enabled: false
      },
      legend: {
        enabled: false
      },
      title: {
        text: null
      },
      exporting: {
        enabled: false,
        fallbackToExportServer: false
      },
      plotOptions: {
        series: {
          pointWidth: 15,
          events: {
            hide: function () {
              redrawWithNewHeight(this.chart, 10);
            },
            show: function () {
              redrawWithNewHeight(this.chart, 10);
            }
          }
        },
        column: {
          threshold: 0,
          dataLabels: {
            enabled: true,
            formatter: function () {
              if (this.series.name == "Actual Return(%)") {
                return (this.y * 100).toFixed(1) + '%';
              }
              else {
                return parseFloat(((this.y) / 1000).toFixed(1)).toLocaleString(temp) + 'K';
              }
            },
            style: {
              color: '#031B4E',
              font: '13px ',
              fontWeight: 'normal',
              textOutline: false
            },
          },
        },
      },
      tooltip: {
        formatter: function () {
          if (this.series.name == "Actual Return(%)") {
            return this.series.name + ":<b>" + ((this.y)).toFixed(1) + '%</b>';
          }
          else
            return this.series.name + ":<b>" + parseFloat(((this.y)).toFixed(0)).toLocaleString('hi-IN') + '</b>';
        },
        style:{
          fontFamily:'verdana'

        }
      },
      series: [
        {
          name: "Investment Amount(" + (this.currencysymbols[this.route.snapshot.params.country]) + ')',
          data: investment_data,
          marker: {
            enabled: false
          },
          type: "column",
          color: '#002060',
        

        },
        {
          name: 'Actual Return(%)',
          data: return_data,
          marker: {
            enabled: false
          },
          color: '#89BBFE',
          type: "column",
          yAxis: 1,
        },
        {
          name: "Actual Return(" + (this.currencysymbols[this.route.snapshot.params.country]) + ')',
          data: pnl_data,
          marker: {
            enabled: false
          },
          color: '#89BBFE',
          type: "column",
          yAxis: 2,
        }
      ],
      xAxis: [{

        type: 'category',
        lineWidth: 0,
        labels: {
          style: {
            color: '#031B4E',
            font: '13px  ',
            fontWeight: 'normal',
            textOutline: false,
          },
        }
      }],
      yAxis: [
        {
          offset: 0,
          height: 1,
          top: 10,
          width: "30%",
          title: {
            text: "Investment Amount(" + (this.currencysymbols[this.route.snapshot.params.country]) + ')',
            textAlign: 'right',
            style: {
              color: '#031B4E',
              font: '15px ',
              fontWeight: 'bold',
              textOutline: false,
              align: 'high',
              fontFamily:'verdana'

            },
          },
          labels:
          {
            enabled: false,
          },
        },
        {
          width: "33%",
          top: 10,
          offset: 0,
          left: "33%",
          height: 1,
          title: {
            text: "Actual Return(%)",
            textAlign: 'right',
            style: {
              color: '#031B4E',
              font: '15px ',
              fontWeight: 'bold',
              textOutline: false,
              align: 'high'
            },
          },
          labels:
          {
            enabled: false,
          }
        },
        {
          width: "33%",
          top: 10,
          offset: 0,
          left: "65%",
          height: 1,
          title: {
            text: "Actual Return(" + (this.currencysymbols[this.route.snapshot.params.country]) + ')',
            textAlign: 'right',
            style: {
              color: '#031B4E',
              font: '15px ',
              fontWeight: 'bold',
              textOutline: false,
              align: 'high',
              fontFamily:'verdana'

            },
          },
          labels:
          {
            enabled: false,
          }
        },
      ],
    };
    var chart = new Highcharts.Chart(this.config);
  }
  plotCharts(dynamicColumn, plotLinesData, chartHeight, multiplyFactor, dividendFactor, symbol, tooltipSymbol, roundDigits, chartTitle = null, formatCurrency = "", symbol_param = "") {
    this[dynamicColumn] = new Chart(lineChart)
    this[dynamicColumn].options.title = {
      text: chartTitle,
      style: {
        color: '#031b4e',
        fontWeight: 'bold',
        fontSize: '14px'
      },
    },

    this[dynamicColumn].options = { series: this.PortfolioReturnsData[dynamicColumn] }
    this[dynamicColumn].options.xAxis = {
      type: 'datetime',
      tickWidth: 1,
      tickLength: 6,
      startOnTick: false,
      endOnTick: false,
      dateTimeLabelFormats: { // don't display the dummy year
        day: '%e-%b',
        week: '%e-%b',
        month: '%b-%y',
        year: '%Y'
      },
      labels: {
        style: {
          color: '#031b4e',
          fontWeight: 'normal',
          fontSize: '14px'
        },
      },
    }
    this[dynamicColumn].options.yAxis = {
      gridLineWidth: 0,
      lineWidth: 1,

      labels: {
        formatter: function () {
          return ((this.value * multiplyFactor) / dividendFactor).toFixed(0) + symbol;
        },
        style: {
          color: '#031b4e',
          fontSize: '14px',
          fontWeight: 'normal'
        },
      },

      title: {
        text: null,
      },
      plotLines:
        plotLinesData
    }

    this[dynamicColumn].options.plotOptions = {
      line: {
        marker: {
          enabled: false,
          symbol: "circle"
        }
      }
    }
    this[dynamicColumn].options.exporting = lineChart.exporting
    this[dynamicColumn].options.chart = {
      type: 'line',
      backgroundColor: "transparent",
      zoomType: 'x',
      height: chartHeight
    }
    this[dynamicColumn].options.title = lineChart.title
    this[dynamicColumn].options.credits = lineChart.credits
    this[dynamicColumn].options.tooltip = {
      formatter: function () {
        var s = ["Date : " + '<b>' + Highcharts.dateFormat('%d-%b-%Y', this.x)];
        for (let i = 0; i < this.points.length; i++) {
          s.push(this.points[i].series.name + " : " + '<b>' + (this.points[i].point.y * multiplyFactor).toFixed(roundDigits) + tooltipSymbol + '</b>');
        }
        return s.join('<br>');
      },
      shared: true,
      style: {
        color: '#031b4e',
        fontSize: '14px',
        fontWeight: 'normal'
      },
    }
  }


  getStockInfopage(selectedStock,selectedSymbol){
    let pagename = "/StockInfoPage/";
    this.router.navigate([]).then(result => {  window.open( this.country + pagename + "?stockname=" + (selectedStock+" ("+selectedSymbol+")").split("&").join("`") , '_blank','noopener') });
  }
}
