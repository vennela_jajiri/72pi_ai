import { Component, OnInit,Injectable} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import { AppComponent } from '../app.component';
import { PageService } from '../service/page.service';
import * as Highcharts from "highcharts/highstock";
import { ViewEncapsulation } from '@angular/core';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { ActivatedRoute, Router } from '@angular/router';
import { Sort } from '@angular/material/sort';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { NavbarComponent } from '../navbar/navbar.component';
import { Title, Meta } from '@angular/platform-browser';
@Component({
  selector: 'app-our-performance',
  templateUrl: './our-performance.component.html',
  styleUrls: ['./our-performance.component.scss'],
  encapsulation: ViewEncapsulation.Emulated
})


export class OurPerformanceComponent implements OnInit {
  country:string;
  symbols = {'India':'en-IN','US':'en-US'}
  currencysymbols = {'India':'₹','US':'$'}
  symbol:string;
  currencysymbol:string;
  portfolio_holdings: MatTableDataSource<any> = new MatTableDataSource();
  transactions :  MatTableDataSource<any> = new MatTableDataSource();
  performanceHitIndiaTable: MatTableDataSource<any> = new MatTableDataSource();
  performanceHitUSTable :  MatTableDataSource<any> = new MatTableDataSource();
  performanceTable :  MatTableDataSource<any> = new MatTableDataSource();
  displayTableColumnsPerformanceTable;
  equity_valuation;
  equity_investment;
  profit_loss;
  profit_loss_day;
  portfolio_highlights_cash;
  portfolio_highlights_realized;
  portfolio_top_exposure_name;
  portfolio_top_exposure_value;
  portfolio_highlights_mps_name;
  portfolio_highlights_mps_value;
  portfolio_highlights_lps_name;
  portfolio_highlights_lps_value;
  maxGainerCompany;
  maxGainervalue;
  maxLoserCompany;
  maxLoservalue;
  output_cumulative;
  allocation;
  loading : boolean = true;
  equityCashflow;
  equityCashflowFlag=false;
  profitLoss;
  flag=0;
  ourPerformanceData;
  performanceDate;
  current_date;
  errorMsg: string;
  performanceHitDate;
  Highcharts = Highcharts;
  contentFlag = true; errorFlag = false;

    element;
    
    isBig =window.innerWidth>1279;
    legendBig = {
    margin: 0,
    padding: 0,
    borderWidth: 0,
    width: 400,
    itemWidth: 200,
    itemStyle: {
        color: '#031B4E',
        fontSize: '12px',
        fontWeight: 'bold',
        textOverflow: null 

    },
    };
    legendSmall = {
    margin: 0,
    padding: 0,
    itemStyle: {
        color: '#031B4E',
        fontSize: '12px',
        fontWeight: 'bold',
        textOverflow: null 

    },
    }

    constructor(private titleService: Title, private metaService: Meta,  private navbar:NavbarComponent,private  ourPerformanceService: PageService,private route:ActivatedRoute,public myApp:AppComponent,private router: Router,private http: HttpClient) { }

    private getServerErrorMessage(error: HttpErrorResponse): string {
        switch (error.status) {
            case 401: {
                return '401';
            }
            case 402: {
                return '402';
            }
            case 403: {
                return '403';
            }
            case 404: {
              return '404';
            }
          
            case 405: {
              return '405';
            }
            case 406: {
              return '406';
            }
            case 412: {
              return '412';
            }
            case 500: {
              return '500';
            }
            case 501: {
              return '501';
            }
            case 502: {
              return '502';
            }
            default: {
                return '500';
            }
    
        }
      }
  ngOnInit() {
    this.titleService.setTitle('72PI Performance');
    this.metaService.addTags([
      {name: 'keywords', content: '72PIperformance ,72piportfolioallocation ,72piportfolioskew ,72piportfoliohitrate ,72piportfolioholdings ,72piportfoliotransaction ,72piportfoliodetails ,72piportfoliohighlights,performance ,portfolioallocation ,portfolioskew ,portfoliohitrate ,portfolioholdings ,portfoliotransaction ,portfoliodetails ,portfoliohighlights'},
      {name: 'description', content: ' It analyses the performance of the 72PI portfolio stocks against benchmark indices. It also shows skew, allocation, transaction, and current holdings of the portfolio'},
    ]);
    window.scroll(0,0);
    let element = document.querySelector('.navbar-inverse');
    element.classList.remove('navbar');
    this.myApp.imgname = 0;
    this.country = this.route.snapshot.params.country;
    this.getOurPerformanceData(this.country);
  }
  getStockInfopage(selectedStock,selectedSymbol){
    let pagename = "/StockInfoPage/";
    this.router.navigate([]).then(result => {  window.open( this.country + pagename + "?stockname=" + (selectedStock+" ("+selectedSymbol+")").split("&").join("`") , '_blank','noopener') });
  }
  getOurPerformanceData(country){
    this.ourPerformanceService.geOurPerformanceViews(country).subscribe(ourperformance=>  {
    this.titleService.setTitle('72PI Performance');

        this.contentFlag = true;
        this.errorFlag = false;
        this.ourPerformanceData = ourperformance;
      this.portfolio_holdings=ourperformance['current_portfolio_holdings'];
      this.transactions=ourperformance['transactions'];
      this.equity_valuation=ourperformance['equity_valuation'];
      this.equity_investment=ourperformance['equity_investment'];
      this.profit_loss=ourperformance['profit_loss'];
      this.profit_loss_day=ourperformance['profit_loss_day'];
      this.portfolio_highlights_cash=ourperformance['portfolio_highlights_cash'];
      this.portfolio_highlights_realized=ourperformance['portfolio_highlights_realized'];
      this.portfolio_top_exposure_name=ourperformance['portfolio_top_exposure'][0]['SECTOR'];
      this.portfolio_top_exposure_value=ourperformance['portfolio_top_exposure'][0]['Exposure_Pct'];
      this.portfolio_highlights_mps_name=ourperformance['portfolio_highlights_mps'][0]['COMPANY'];
      this.portfolio_highlights_mps_value=ourperformance['portfolio_highlights_mps'][0]['PCT_CHG'];
      this.portfolio_highlights_lps_name=ourperformance['portfolio_highlights_lps'][0]['COMPANY'];
      this.portfolio_highlights_lps_value=ourperformance['portfolio_highlights_lps'][0]['PCT_CHG'];
      this.maxGainerCompany=ourperformance['max_gainer'][0]['COMPANY']
      this.maxGainervalue=ourperformance['max_gainer'][0]['Profit_Loss']
      this.maxLoserCompany=ourperformance['max_loser'][0]['COMPANY']
      this.maxLoservalue=ourperformance['max_loser'][0]['Profit_Loss']
      var ouputChart=this.plotChart(ourperformance['output_cumulative'],ourperformance['allocation_list'],ourperformance['equity_cashflow'],ourperformance['equity_cashflow_categories'],ourperformance['profit'],ourperformance['loss'])
      this.output_cumulative=ouputChart[0];
      this.allocation=ouputChart[1];
      this.equityCashflow=ouputChart[2];
      this.profitLoss=ouputChart[3];
      this.flag=1;
      this.performanceHitIndiaTable=ourperformance['performanceHitIndia'];
      this.performanceHitUSTable=ourperformance['performanceHitUS'];
      this.performanceTable=ourperformance['itd_mtd_list'];
      this.displayTableColumnsPerformanceTable=ourperformance['displayTableColumnsPerformanceTable']
      this.performanceDate=ourperformance['performance_date']
      this.current_date=ourperformance['current_date']
      this.performanceHitDate=ourperformance['performanceHitDate']
      this.symbol = this.symbols[this.country]
      this.currencysymbol = this.currencysymbols[this.country]
      this.loading = false
    }, error => {
        if (error.error instanceof ErrorEvent) {
          this.errorMsg = `Error: ${error.error.message}`;
        }
        else {
          this.contentFlag = false;
          this.errorFlag = true;
          this.loading = false;
          this.errorMsg = this.getServerErrorMessage(error);
        }
      })

  }
  sortData(sort: Sort,tableName){
    if (sort.active && sort.direction !== '')
        {
          this.ourPerformanceData[tableName] = this[tableName].sort((a, b) => {     
          const isAsc = (sort.direction === 'asc');      
          return this._compare(a[sort.active], b[sort.active], isAsc);
          });    
        }
        this[tableName] = Object.assign([], this.ourPerformanceData[tableName]);   
  }
  private _compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a > b ? 1 : -1) * (isAsc ? 1 : -1);
  }

  onChange(event: MatTabChangeEvent)
  {
    if (event.tab.textLabel=='Equity Cash Flow'){
      this.equityCashflowFlag=true;
    }
  }
  plotChart(inputdata,sector_wise,cashflow,cashflowCategories,profit,loss){
    for (let i = 0; i < inputdata.length; i++) {
      for (let j = 0; j < inputdata[i]['data'].length; j++) {
        let date = new Date(inputdata[i]['data'][j][0]);
        var now_utc = Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(),
                               date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds());
        inputdata[i]['data'][j][0] = now_utc;
      }
    }
      var cumulative={
      chart: {
          type: 'line',
          backgroundColor: "transparent",
      },
      exporting: {
          enabled: false,
          fallbackToExportServer: false
      },
      title: {
          text: null,
        
      },
      credits: {
          enabled: false
      },
      xAxis: {
          type: 'datetime',
          dateTimeLabelFormats: { // don't display the dummy year
              day: '%e-%b-%y',
              week: '%e-%b-%y',
              month: '%b-%y',
              year: '%Y'

          },
          labels: {
              style: {
                  color: '#031b4e',
                  font: '13px  ',
                  fontWeight: 'normal'
              },
          },
          tickLength: 6,
          tickWidth: 1,
          min: inputdata[0]['data'][0][0],
          startOnTick: true,
          endOnTick: false,

      },
      yAxis: {
          gridLineWidth: 0,
          lineWidth: 1,
          tickLength: 6,
          tickWidth: 1,
          labels: {
              formatter: function () {
                  return (this.value * 100).toFixed(0) + '%';
              },
              style: {
                  color: '#031b4e',
                  font: '13px  ',
                  fontWeight: 'normal'
              },
          },
          title: {
              text: "Return",
              style: {
                  color: '#031b4e',
                  font: '13px  ',
                  fontWeight: 'normal'
              },
          },
          plotLines: [{
              color: '#F2F2F2',
              width: 2,
              value: 0
          }]
      },
      tooltip: {

          formatter: function () {
              var s = [];
              var count = 0
            for(let i=0;i<this.points.length;i++){
                  if (count == 0)
                      s.push('Date: <b>' + Highcharts.dateFormat('%d-%b-%Y', this.x) + '</b><br>');
                  count = 1


                  s.push(this.points[i].series.name + ' : <b>' +
                      (this.points[i].y * 100).toFixed(1) + '%</b><br>');



              }

              return s.join('<br>');
          },
          shared: true,
          style: {
              color: '#031b4e',
              font: '13px  ',
              fontWeight: 'normal'
          },
      },
      plotOptions: {
          series: {
              marker: {
                  enabled: false,
                  symbol: "circle"
              }
          }
      },
      legend:
      {
          itemStyle: {
              color: '#031B4E',
              fontFamily: ' ',
              fontSize: '14px'

          },
      },
      series: inputdata,

  } 

  var allocation={
  chart: {
      type: 'pie',
      backgroundColor: 'transparent',
    //   height: (9 / 9 * 100) + '%',
    height:300

  },
  credits: {
      enabled: false
  },
  exporting: {
      buttons: {
          contextButton: {
              enabled: false
          },
      }
  },
  title: {
      style: {
          color: '#031b4e',
          fontSize: '12px',
          fontWeight: 'bold',
      },
      text: null,
  },

  legend: this.isBig? this.legendBig : this.legendSmall,
  tooltip: {
      formatter: function () {
          return 'Sector : <b>' + this.point.name + '</b><br>' + 'Investment Amount(%) : ' +
                '<b>' + (this.point.y*100).toFixed(1) +'%</b>';
      }
  },
  plotOptions: {
      pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          innerSize: "60%",
          size: '170',
          depth: "45%",
          showInLegend: true,
          dataLabels: {
              enabled: false,
              formatter: function () {
                  return '<b>' + this.point.name + '</b><br>';
              },
          }
      },
  },
  series: [{
      name: 'By Sector',
      data: sector_wise,
  }],
}
var equitycashflow={
  chart: {
    type: 'column',
    backgroundColor:"transparent",
    height:200
},
title: {
        text: null,
    },
    credits: {
                enabled: false
            },
            exporting: {
                buttons: {
                    contextButton: {
                    enabled:false
                    },
                    
                }
            },
xAxis: {
  categories:cashflowCategories,
    labels:{
        style: {
            color: '#031b4e',
            fontSize:'14px',
            fontWeight: 'normal'
        },
        formatter: function () {
            var m = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
            var dArr = (this.value).split("-");
            return dArr[2] + "-" + m[parseInt(dArr[1], 10) - 1]+'-'+dArr[0]
  
          }
    },
    tickLength: 6,  
    tickWidth: 1,
},
yAxis: {
    gridLineWidth:0,
    lineWidth:1,
    tickLength: 6,  
    tickWidth: 1,
    labels:{
        style: {
            color: '#031b4e',
            fontSize:'14px',
            fontWeight: 'normal'
        },
    },
    title:{
        text:null,
    },

},
    tooltip: {
        formatter: function() {
            return  "Date : "  +'<b>'+
                Highcharts.dateFormat('%d-%b-%Y',this.x)+'<br></b>'
            +this.series.name+" : "+'<b>'+ (this.point.y).toLocaleString('hi-IN', { maximumFractionDigits: 0 })+ '</b>';
        },
    },                                              

legend:
{
   enabled:false
},
series:[{
    name:'PNL',
    data:cashflow,
    color: 'rgba(34,139,34,1)',
    negativeColor: '#FF0000'
}]
}
var profitLoss={
  chart: {
    type: 'bar',
    height:200
},
title: {
    text: null
},

xAxis: {
        visible:false,
    title: {
        text: null
    },
    
},
yAxis: {
    min: 0,
    gridLineWidth:0,
    title: false,
    labels: {
        overflow: 'justify',
        enabled:false
    }
},
tooltip: {
    enabled:false
},
plotOptions: {
    bar: {
        dataLabels: {
            enabled: true
        },
        
    },
    series: {
animation: false
}
},

credits: {
    enabled: false
},
series: [{
    name: 'Stock(s) in Profit',
    data: [profit],
    color:'green'
}, {
    name: 'Stock(s) in Loss',
    data: [loss],
    color:'red'
}, ]
}
  return [cumulative,allocation,equitycashflow,profitLoss];
}

}
