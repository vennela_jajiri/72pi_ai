import { Component, OnInit } from '@angular/core';
import { AppComponent } from '../app.component';
import { Title, Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.scss'],
})
export class FaqComponent implements OnInit {
  
  constructor(public myApp: AppComponent,private titleService: Title) { }

  ngOnInit() {
    window.scroll(0,0);
    this.titleService.setTitle('72PI Portfolio Intelligence');
    let element = document.querySelector('.navbar-inverse');
    element.classList.remove('navbar');
    this.myApp.imgname = 0;
  }

}
