import { Component, OnInit, Inject ,HostListener} from '@angular/core';
import { AppComponent } from '../app.component';
import { UserPortfolioService } from '../service/userPortfolios.service';
import { PageService } from '../service/page.service'
import { ActivatedRoute, Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { Title, Meta } from '@angular/platform-browser';

import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
export interface subscription {
  dialogHeader:any;
  dialogContent:any;
}
@Component({
  selector: 'app-pricing-details',
  templateUrl: './pricing-details.component.html',
  styleUrls: ['./pricing-details.component.scss'],
})
export class PricingDetailsComponent implements OnInit {
  public country : string;
  public IndiaStyle;
  public UsStyle;
  public dialogHeader;
  public dialogContent;
  public freesubscription : boolean;
  public error : any;

  constructor(private titleService: Title, private metaService: Meta,public dialog: MatDialog,
              public myApp: AppComponent,
              private userService: UserPortfolioService,
              private pageService : PageService,
              private route: ActivatedRoute) {}

  ngOnInit() {
    this.titleService.setTitle('Pricing');
    this.country = this.route.snapshot.params.country;
    this.myApp.setDefaultValue(this.country);
    this.pageService.getPricingDetails(this.country).subscribe(response=>{
      this.freesubscription = response['freesubscription'];
    });
    if (this.country == 'India') {
      this.IndiaStyle = 'selectedOne';
      this.UsStyle = 'unSelectedOne';
    } else {
      this.IndiaStyle = 'unSelectedOne';
      this.UsStyle = 'selectedOne';
    }
    this.myApp.imgname = this.userService.getBodyFunction();
  }
  @HostListener('window:scroll', ['$event'])
  onWindowScroll(e) {
    this.myApp.imgname = this.userService.getBodyFunction();
  }
  
  changeCard1() {
    this.IndiaStyle = 'selectedOne';
    this.UsStyle = 'unSelectedOne';
  }
  changeCard2() {
    this.IndiaStyle = 'unSelectedOne';
    this.UsStyle = 'selectedOne';
  }
  ModelPortfolios() {
    this.dialogHeader = 'Model Portfolios';
    this.dialogContent =
      'Our portfolio designed with fundamental, quantitative, technical and situational analyses. Adjusted 1 to 3 times per month depending on market conditions. Our performance is time stamped and measured in a transparent manner. Other Model Portfolios available to users.';
    const dialogRef = this.dialog.open(SubscriptionDialogComponent, {
      width: '500px',
      height: 'fit-content',
      data: {
        dialogHeader: this.dialogHeader,
        dialogContent: this.dialogContent,
      },
    });
    dialogRef.afterClosed().subscribe((result) => {});
  }
  StockETFScreener() {
    this.dialogHeader = 'Stock & ETF Screener';
    this.dialogContent =
      'Create portfolios by selecting stocks by sector,ETF and market capitalization. Create portfolios by filtering with fundamental and technical data factors such as Sales Growth, PE multiple, EPS Growth, Dividend Yield, ROE and many other factors. You can also import portfolios.Automated quantitative filtering of different indices for stock and sector ideas.';
    const dialogRef = this.dialog.open(SubscriptionDialogComponent, {
      width: '500px',
      height: 'fit-content',
      data: {
        dialogHeader: this.dialogHeader,
        dialogContent: this.dialogContent,
      },
    });
    dialogRef.afterClosed().subscribe((result) => {});
  }
  PortfolioAnalyticTools() {
    this.dialogHeader = 'Portfolio Analytic Tools';
    this.dialogContent =
      'Analyze your portfolios with 72PI for deep dive into the portfolio performance and understand returns, risk / volatility of portfolio and overall recommendations.';
    const dialogRef = this.dialog.open(SubscriptionDialogComponent, {
      width: '500px',
      height: 'fit-content',
      data: {
        dialogHeader: this.dialogHeader,
        dialogContent: this.dialogContent,
      },
    });
    dialogRef.afterClosed().subscribe((result) => {});
  }
  CuratedMarketResearchInsights() {
    this.dialogHeader = 'Curated Market & Research Insights';
    this.dialogContent =
      '72PI team will curate market Insights into a structured content for easy digestion and understanding of markets. The 72PI research team provides custom research on a select list of companies.';
    const dialogRef = this.dialog.open(SubscriptionDialogComponent, {
      width: '500px',
      height: 'fit-content',
      data: {
        dialogHeader: this.dialogHeader,
        dialogContent: this.dialogContent,
      },
    });
    dialogRef.afterClosed().subscribe((result) => {});
  }
}

@Component({
  selector: 'app-subscription-dialog',
  templateUrl: './subscription-dialog.component.html',
  styleUrls: ['./pricing-details.component.scss'],
})
export class SubscriptionDialogComponent implements OnInit {
  public dialogHeader;
  constructor(
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<SubscriptionDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: subscription
  ) {}
  ngOnInit() {}
  onNoClick(): void {
    this.dialogRef.close();
  }
}
