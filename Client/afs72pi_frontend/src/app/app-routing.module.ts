import { AuthGuardService as AuthGuard } from './service/auth-guard.service';
import { NgModule } from '@angular/core';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';
import { CommonModule } from '@angular/common';
import { BodyComponent } from './body/body.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { BlogComponent } from './blog/blog.component';
import { PasswordResetComponent } from './password-reset/password-reset.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import { MarketComponent } from './market/market.component';
import { AboutusTeamComponent } from './aboutus-team/aboutus-team.component';
import { InvestmentPhilosophyComponent } from './investment-philosophy/investment-philosophy.component';
import { FeedbackComponent } from './feedback/feedback.component';
import { ETFmonitorComponent } from './etfmonitor/etfmonitor.component';
import { PortfolioDetailsComponent } from './portfolio-details/portfolio-details.component';
import { ModelPortfoliosComponent } from './model-portfolios/model-portfolios.component';
import { ModelPortfolioParentComponent } from './model-portfolio-parent/model-portfolio-parent.component';
import { OurMarketViewComponent } from './our-market-view/our-market-view.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { TermsConditionsComponent } from './terms-conditions/terms-conditions.component';
import { FaqComponent } from './faq/faq.component';
import { CreatePortfolioStocksSelectionComponent } from './create-portfolio-stocks-selection/create-portfolio-stocks-selection.component';
import { PersonalisationComponent } from './personalisation/personalisation.component';
import { FactorAnalysisComponent } from './factor-analysis/factor-analysis.component';
import { AnalystTargetPriceComponent } from './analyst-target-price/analyst-target-price.component';
import { ValuationMultiplesComponent } from './valuation-multiples/valuation-multiples.component';
import { BlogsReadmoreComponent } from './blogs-readmore/blogs-readmore.component';
import { StockInfoComponent } from './stock-info/stock-info.component';
import { FuturesOptionsComponent } from './futures-options/futures-options.component';
import { OurPortfolioComponent } from './our-portfolio/our-portfolio.component';
import { RiskOverViewComponent } from './risk-over-view/risk-over-view.component';
import { StocksChartsComponent } from './stocks-charts/stocks-charts.component';
import { ScreenerComponent } from './screener/screener.component';
import { PortfolioReturnsComponent } from './portfolio-returns/portfolio-returns.component';
import { DashboardSummaryComponent } from './dashboard-summary/dashboard-summary.component';
import { OurPerformanceComponent } from './our-performance/our-performance.component';
import { OurPortfolioPerformanceComponent } from './our-portfolio-performance/our-portfolio-performance.component';
import { WizardComponent, } from './wizard/wizard.component'
import { ConfirmOrderComponent } from './confirm-order/confirm-order.component';
import { SubscriptionInformationComponent } from './subscription-information/subscription-information.component';
import { PricingDetailsComponent } from './pricing-details/pricing-details.component';
import { SubscriptionExpiredComponent } from './subscription-expired/subscription-expired.component';
import { SubscriptionUpgradeComponent } from './subscription-upgrade/subscription-upgrade.component';
import { SubscriptionFreeComponent } from './subscription-free/subscription-free.component';
import { SubscriptionStatusPageComponent } from './subscription-status-page/subscription-status-page.component';
import { OrderSuccessFailureComponent } from './order-success-failure/order-success-failure.component';
import { Page401Component } from './page401/page401.component';
import { Page402Component } from './page402/page402.component';
import { Page403Component } from './page403/page403.component';
import { Page404Component } from './page404/page404.component';
import { Page405Component } from './page405/page405.component';
import { Page406Component } from './page406/page406.component';
import { Page412Component } from './page412/page412.component';
import { Page500Component } from './page500/page500.component';
import { Page501Component } from './page501/page501.component';
import { Page502Component } from './page502/page502.component';
import { PasswordResetPageComponent } from './password-reset-page/password-reset-page.component';
import { MyAlertComponent } from './my-alert/my-alert.component';
import { StockProfilerComponent } from './stock-profiler/stock-profiler.component';
import { EmailVerificationStatusComponent } from './email-verification-status/email-verification-status.component';
import { EmailVerificationComponent } from './email-verification/email-verification.component';
import { MySubscriptionComponent } from './my-subscription/my-subscription.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { SitemapComponent } from './sitemap/sitemap.component';


const routes: Routes = [
  { path: '', component: BodyComponent },
  { path: 'aboutus', component: AboutusComponent },
  { path: 'aboutus/team/:memberName', component: AboutusTeamComponent },
  { path: 'login', component: LoginComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'blog', component: BlogComponent },
  { path: 'password/reset', component: PasswordResetComponent },
  { path: 'InvestmentSolutions/InvestmentPhilosophy', component: InvestmentPhilosophyComponent },
  { path: ':country/market', component: MarketComponent },
  { path: ':country/etfmonitor', component: ETFmonitorComponent },
  { path: 'Personalisation', component: PersonalisationComponent, canActivate: [AuthGuard] },
  { path: ':country/PortfolioDetails', component: PortfolioDetailsComponent, canActivate: [AuthGuard] },
  { path: ':country/ModelPortfolios', component: ModelPortfolioParentComponent, canActivate: [AuthGuard] },
  { path: ':country/ViewModelPortfolios', component: ModelPortfoliosComponent, canActivate: [AuthGuard] },
  { path: ':country/ourmarketview', component: OurMarketViewComponent, canActivate: [AuthGuard] },
  { path: 'feedback', component: FeedbackComponent, canActivate: [AuthGuard] },
  { path: 'terms-conditions', component: TermsConditionsComponent },
  { path: 'privacy-policy', component: PrivacyPolicyComponent },
  { path: 'contact-us', component: ContactUsComponent },
  { path: 'faq', component: FaqComponent },
  { path: ':country/CreatePortfolio-SelectingStocks', component: CreatePortfolioStocksSelectionComponent, canActivate: [AuthGuard] },
  { path: ':country/FactorAnalysis', component: FactorAnalysisComponent, canActivate: [AuthGuard] },
  { path: ':country/valuationMultiples', component: ValuationMultiplesComponent, canActivate: [AuthGuard] },
  { path: ':country/AnalystTargetPrice', component: AnalystTargetPriceComponent, canActivate: [AuthGuard] },
  { path: 'blog/:blogTitle', component: BlogsReadmoreComponent },
  { path: ':country/StockInfoPage', component: StockInfoComponent },
  { path: ':country/FuturesOptions', component: FuturesOptionsComponent, canActivate: [AuthGuard] },
  { path: ':country/OurPortfolio', component: OurPortfolioComponent, canActivate: [AuthGuard] },
  { path: ':country/RiskOverview', component: RiskOverViewComponent, canActivate: [AuthGuard] },
  { path: ':country/StocksCharts', component: StocksChartsComponent, canActivate: [AuthGuard] },
  { path: ':country/Screener', component: ScreenerComponent, canActivate: [AuthGuard] },
  { path: ':country/portfolio-returns', component: PortfolioReturnsComponent, canActivate: [AuthGuard] },
  { path: ':country/DashboardSummary', component: DashboardSummaryComponent, canActivate: [AuthGuard] },
  { path: ':country/OurPerformance', component: OurPerformanceComponent, canActivate: [AuthGuard] },
  { path: ':country/wizard', component: WizardComponent, canActivate: [AuthGuard] },
  { path: ':country/OurPortfolioPerformance', component: OurPortfolioPerformanceComponent, canActivate: [AuthGuard] },
  { path: ':country/pricing-details', component: PricingDetailsComponent },
  { path: ':country/customer/subscription/:currency/premium', component: SubscriptionInformationComponent, canActivate: [AuthGuard] },
  { path: ':country/customer/subscription/premium/confirmation', component: ConfirmOrderComponent, canActivate: [AuthGuard] },
  { path: 'customer/access/subscription/expired', component: SubscriptionExpiredComponent, canActivate: [AuthGuard] },
  { path: 'customer/access/subscription/upgrade', component: SubscriptionUpgradeComponent, canActivate: [AuthGuard] },
  { path: ':country/customer/free/subscription/register', component: SubscriptionFreeComponent, canActivate: [AuthGuard] },
  { path: ':country/customer/free/subscription/status', component: SubscriptionStatusPageComponent, canActivate: [AuthGuard] },
  { path: ':country/customer/subscription/status', component: OrderSuccessFailureComponent, canActivate: [AuthGuard] },
  { path: ':country/StockProfiler', component: StockProfilerComponent, canActivate: [AuthGuard] },
  { path: ':country/myAlerts', component: MyAlertComponent, canActivate: [AuthGuard] },
  { path: 'accounts/auth/password/reset/confirm/:uid/:token', component: PasswordResetPageComponent },
  { path: 'new_user/registration/verify-email/verification_status', component: EmailVerificationStatusComponent },
  { path: 'new_user/registration/verify-email/:key', component: EmailVerificationComponent },
  { path: ':country/MySubscriptions', component: MySubscriptionComponent },
  { path: 'change-password', component: ChangePasswordComponent,canActivate: [AuthGuard] },
  {path: ':country/sitemap', component:SitemapComponent},
  { path: 'page401', component: Page401Component },
  { path: 'page402', component: Page402Component },
  { path: 'page403', component: Page403Component },
  { path: 'page405', component: Page405Component },
  { path: 'page406', component: Page406Component },
  { path: 'page412', component: Page412Component },
  { path: 'page500', component: Page500Component },
  { path: 'page501', component: Page501Component },
  { path: 'page502', component: Page502Component },
  { path: '**', component: Page404Component },

];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes,{ preloadingStrategy: PreloadAllModules}),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule { }