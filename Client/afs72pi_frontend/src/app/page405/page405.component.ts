import { Component, OnInit } from '@angular/core';
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'app-page405',
  templateUrl: './page405.component.html',
  styleUrls: ['./page405.component.scss'],
})
export class Page405Component implements OnInit {

  constructor(private titleService:Title) {
    this.titleService.setTitle("Sorry, Access is denied!");
  }

  ngOnInit() {
    window.scroll(0,0);
    let element = document.querySelector('.navbar-inverse');
    element.classList.remove('navbar');
  }

}
