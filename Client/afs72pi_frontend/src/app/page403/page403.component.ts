import { Component, OnInit } from '@angular/core';
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'app-page403',
  templateUrl: './page403.component.html',
  styleUrls: ['./page403.component.scss'],
})
export class Page403Component implements OnInit {

  constructor(private titleService:Title) {
    this.titleService.setTitle("Forbidden, Access is denied");
  }
  ngOnInit() {
    window.scroll(0,0);
    let element = document.querySelector('.navbar-inverse');
    element.classList.remove('navbar');
  }

}
