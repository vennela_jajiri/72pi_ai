import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../service/auth.service';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';
import { AppComponent } from '../app.component';
import { MatDialog } from '@angular/material/dialog';
import { SuccessDialogComponent } from '../navbar/navbar.component';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  error: any;
  sitekey: string;
  message : string;
  status : string;
  signupForm: FormGroup = new FormGroup({
    username: new FormControl(''),
    email: new FormControl('',[Validators.email, Validators.required ]),
    password: new FormControl('',[Validators.required, Validators.min(3) ]),
  })

  constructor( private authService: AuthService,
               private router : Router,
               public myApp: AppComponent,
               public dialog: MatDialog
              ) {}

  
  get formsubmit(){return this.signupForm.controls;}
  ngOnInit(): void {
    window.scroll(0,0);
    let element = document.querySelector('.navbar-inverse');
    element.classList.remove('navbar');
    this.myApp.imgname = 0;
    this.signupForm = new FormGroup({
      username : new FormControl(''),
      email: new FormControl(''),
      password: new FormControl(''),
    });
  }

  onSubmit(){
    this.authService.signup(this.formsubmit.username.value, this.formsubmit.email.value,
      this.formsubmit.password.value,this.formsubmit.password.value).pipe(first()).subscribe(
      success =>{
        this.message = 'We have sent an E-mail for account confirmation';
          this.status = 'success';
          const timeout = 5000;
          const dialogRef = this.dialog.open(SuccessDialogComponent, {
            width: '500px',
            data: { message: this.message, status: this.status },
          });
          dialogRef.afterOpened().subscribe((_) => {
            setTimeout(() => {
              dialogRef.close();
            }, timeout);
          });
          dialogRef.afterClosed().subscribe((result) => {
            this.router.navigate(['login']);
          });        
      },
      error => this.error = error
    )
  }

  hide = true;
}
