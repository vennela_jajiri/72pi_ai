import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../service/auth.service';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';
import { AppComponent } from '../app.component';
import { MatDialog } from '@angular/material/dialog';
import { SuccessDialogComponent } from '../navbar/navbar.component';

@Component({
  selector: 'app-password-reset',
  templateUrl: './password-reset.component.html',
  styleUrls: ['./password-reset.component.scss']
})
export class PasswordResetComponent implements OnInit {
  error: any;
  message : string;
  status : string;
  passwordResetForm: FormGroup = new FormGroup({
    email: new FormControl('',[Validators.email, Validators.required ]),
  })

  constructor(private authService: AuthService,
              public myApp:AppComponent,
              private router : Router,
              public dialog: MatDialog
   ) { }

   get formsubmit(){return this.passwordResetForm.controls;}

   ngOnInit(): void {
    window.scroll(0,0);
    let element = document.querySelector('.navbar-inverse');
    element.classList.remove('navbar');
    this.myApp.imgname = 0;
     this.passwordResetForm = new FormGroup({
      email: new FormControl('',[Validators.email, Validators.required ]),
     });
   }
 
   onSubmit(){
     this.authService.forgotPassword(this.formsubmit.email.value).pipe(first()).subscribe(
       success => {
        this.message = 'Sent E-mail with intstructions to reset your password';
        this.status = 'success';
        const timeout = 5000;
        const dialogRef = this.dialog.open(SuccessDialogComponent, {
          width: '500px',
          data: { message: this.message, status: this.status },
        });
        dialogRef.afterOpened().subscribe((_) => {
          setTimeout(() => {
            dialogRef.close();
          }, timeout);
        });
        dialogRef.afterClosed().subscribe((result) => {
          this.router.navigate(['login']);
        });
       },
       error => this.error = error
     )
   }
 
   hide = true;
 }
