import { Component, OnInit } from '@angular/core';
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'app-page500',
  templateUrl: './page500.component.html',
  styleUrls: ['./page500.component.scss'],
})
export class Page500Component implements OnInit {

  constructor(private titleService:Title) {
    this.titleService.setTitle("Sorry, Internal server error!");
  }

  ngOnInit() {
    window.scroll(0,0);
    let element = document.querySelector('.navbar-inverse');
    element.classList.remove('navbar');
  }

}
