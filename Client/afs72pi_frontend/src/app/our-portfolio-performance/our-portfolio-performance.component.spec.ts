import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { OurPortfolioPerformanceComponent } from './our-portfolio-performance.component';

describe('OurPortfolioPerformanceComponent', () => {
  let component: OurPortfolioPerformanceComponent;
  let fixture: ComponentFixture<OurPortfolioPerformanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OurPortfolioPerformanceComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(OurPortfolioPerformanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
