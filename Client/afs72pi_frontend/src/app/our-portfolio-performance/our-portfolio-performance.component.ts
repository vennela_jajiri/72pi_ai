import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { AppComponent } from '../app.component';
@Component({
  selector: 'app-our-portfolio-performance',
  templateUrl: './our-portfolio-performance.component.html',
  styleUrls: ['./our-portfolio-performance.component.scss'],
})
export class OurPortfolioPerformanceComponent implements OnInit {

  constructor(private myApp:AppComponent,private route: ActivatedRoute) { }
  country : string;

  ngOnInit() {
    window.scroll(0,0);
    let element = document.querySelector('.navbar-inverse');
    element.classList.remove('navbar');
    this.myApp.imgname = 0;
    this.country = this.route.snapshot.params.country;

  }

}
