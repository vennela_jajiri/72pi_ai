import { Component, OnInit ,HostListener} from '@angular/core';
import { AppComponent } from '../app.component';
import { UserPortfolioService } from '../service/userPortfolios.service';
import { PageService } from '../service/page.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common'

@Component({
  selector: 'app-subscription-information',
  templateUrl: './subscription-information.component.html',
  styleUrls: ['./subscription-information.component.scss'],
})
export class SubscriptionInformationComponent implements OnInit {
  public country : string;
  public currency : string;
  public firstName : string;
  public lastName : string;
  public email : string;
  public mobileNumber : number;
  public subscriptionPlan: any ;
  public RecurringCost : string;
  public months : string;
  public premiumSubscription : string;
  public promotion : string;
  public Discount : string;
  public EffectiveTotal : string;
  public GST : string;
  public PremiumTotal : string;
  public msgString : string;
  public selectedPlan : string;
  public plancostMonth : string;
  constructor(public myApp: AppComponent, 
              private userService: UserPortfolioService,
              private pageService : PageService,
              private route: ActivatedRoute,
              private router : Router,
              private location : Location) { }

  ngOnInit() {
      this.myApp.imgname = this.userService.getBodyFunction(); 
      this.country = this.route.snapshot.params.country;
      this.myApp.setDefaultValue(this.country);
      this.currency = this.route.snapshot.params.currency;
      if(this.currency == 'INR'){
        this.subscriptionPlan = [
          {plan:'Premium (Quarterly) - Rs.1440/month + GST 18%'},
          {plan:'Premium (Annual) - Rs.1440/month + GST 18%'}
        ];
        this.RecurringCost = 'Recurring Quarterly Cost';
        this.months = '3 months';
        this.premiumSubscription = '₹ 4,320';
        this.promotion = "Quarter Promotion";
        this.Discount = '- ₹ 332';
        this.EffectiveTotal = '₹ 3,988';
        this.GST = '+ ₹ 717.84';
        this.PremiumTotal = '₹ 4,705.84';
        this.plancostMonth ='₹ 1,440/month';
      }
      else{
        this.subscriptionPlan = [
          {plan:'Premium (Quarterly) - 20$/month'},
          {plan:'Premium (Annual) - 20$/month'}
        ];
        this.RecurringCost = 'Recurring Quarterly Cost';
        this.months = '3 months';
        this.premiumSubscription = '60 $';
        this.promotion = "Quarter Promotion";
        this.Discount = '-4 $';
        this.EffectiveTotal = '56 $';
        this.GST = null;
        this.PremiumTotal = '56 $';
        this.plancostMonth ='20 $/month';
      }
      this.selectedPlan = this.subscriptionPlan[0]['plan']
      this.pageService.createPremiumSubscription(this.country, this.currency).subscribe(respose=>{
        this.firstName = respose['data']['first_name'];
        this.lastName = respose['data']['last_name'];
        this.email = respose['data']['email'];
        this.mobileNumber = respose['data']['contact_no'];
        this.msgString = respose['status'];
      });
  }
  subscriptionPlanChange(val){
    this.selectedPlan = val;
    if(val=='Premium (Quarterly) - Rs.1440/month + GST 18%'){
      this.RecurringCost = 'Recurring Quarterly Cost';
      this.months ='3 months';
      this.premiumSubscription = '₹ 4,320';
      this.promotion = "Quarter Promotion";
      this.Discount = '- ₹ 332';
      this.EffectiveTotal = '₹ 3,988';
      this.GST = '+ ₹ 717.84';
      this.PremiumTotal = '₹ 4,705.84';
      this.plancostMonth ='₹ 1,440/month';
    }
    else if(val=='Premium (Annual) - Rs.1440/month + GST 18%'){
      this.RecurringCost = 'Recurring Annual Cost';
      this.months = '12 months';
      this.premiumSubscription = '₹ 17,280';
      this.promotion = "Annual Promotion";
      this.Discount = '- ₹ 2,880';
      this.EffectiveTotal = '₹ 14,400';
      this.GST = '+ ₹ 2,592';
      this.PremiumTotal = '₹ 16,992.00';
      this.plancostMonth ='₹ 1,440/month';
    }
    else if(val=='Premium (Quarterly) - 20$/month'){
      this.RecurringCost = 'Recurring Quarterly Cost';
      this.months = '3 months';
      this.premiumSubscription = '60 $';
      this.promotion = "Quarter Promotion";
      this.Discount = '-4 $';
      this.EffectiveTotal = '56 $';
      this.GST = null;
      this.PremiumTotal = '56 $';
      this.plancostMonth ='20 $/month';
    }
    else if(val=='Premium (Annual) - 20$/month'){
      this.RecurringCost = 'Recurring Annual Cost';
      this.months = '12 months';
      this.premiumSubscription = '240 $';
      this.promotion = "Annual Promotion";
      this.Discount = '-40 $';
      this.EffectiveTotal = '200 $';
      this.GST = null;
      this.PremiumTotal = '200 $';
      this.plancostMonth ='20 $/month';
    }
  }
  activateSubscription(){
    let data = {'first_name' : this.firstName, 'last_name': this.lastName,
               'email' : this.email, 'contact_no' : this.mobileNumber, 
               'subscription_type':this.selectedPlan};
    this.pageService.activatePremiumSubscription(this.country,this.currency,data).subscribe(
      response=>{localStorage.setItem('orderdetails',JSON.stringify(response));
                this.doRedirecting();}
  );
  }
  doRedirecting(){
    this.router.navigate([this.country+'/customer/subscription/premium/confirmation']);
  }
  @HostListener('window:scroll', ['$event'])
  onWindowScroll(e) {
    this.myApp.imgname = this.userService.getBodyFunction();
  }
  goBack(): void {
    this.router.navigate([this.country+'/pricing-details']);
  }
}
