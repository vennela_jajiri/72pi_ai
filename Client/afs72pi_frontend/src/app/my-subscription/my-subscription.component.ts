import { Component, OnInit } from '@angular/core';
import { PageService } from '../service/page.service';
import { NavbarComponent } from '../navbar/navbar.component';
import { ActivatedRoute,Router } from '@angular/router';

@Component({
  selector: 'app-my-subscription',
  templateUrl: './my-subscription.component.html',
  styleUrls: ['./my-subscription.component.scss'],
})
export class MySubscriptionComponent implements OnInit {

  constructor(private pageService: PageService,private navbar:NavbarComponent,private route:ActivatedRoute,private router: Router,) { }
  subscriptionsData;country;
  ngOnInit() {
    window.scroll(0,0);
    let element = document.querySelector('.navbar-inverse');
    element.classList.remove('navbar');
    this.navbar.imgname = 0;
    this.country = this.route.snapshot.params.country;
    this.navbar.setDefaultValue(this.country)
;
    this.subscriptionsData = [
      {
        'plan' : 'PREMIUM - ANNUAL',
        'active_from':'Oct. 8, 2020',
        'valid_to':'Oct. 8, 2021',
        'status':'ACTIVE'
      },
      {
        'plan' : 'PREMIUM - QUARTER',
        'active_from':'Feb. 26, 2021',
        'valid_to':'May 27, 2021',
        'status':'UPCOMING'
      },
      {
        'plan' : 'FREE - 14 DAY TRIAL',
        'active_from':'Nov. 12, 2020',
        'valid_to':'Nov. 26, 2020',
        'status':'EXPIRED'
      }
    ]
  }

}
