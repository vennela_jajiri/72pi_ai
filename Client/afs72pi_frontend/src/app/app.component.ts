import { Component, Injectable, Inject, OnInit } from '@angular/core';
import * as AOS from 'aos';
import { PageService } from './service/page.service';
import { CookieService } from 'ngx-cookie-service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NavbarComponent } from './navbar/navbar.component';

export var isUserLoggedIn = 'false';
interface Cookies {
  EssentialCookies : boolean;
  MarketingCookies : boolean;
  FunctionalCookies : boolean;
  AnalyticsCookies: boolean;
}
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
@Injectable({
  providedIn: 'root'
})
export class AppComponent {
  title = 'Home';
  public imgname: number;
  public portfolioName: string;
  public modelPortfolioName: string;
  public stocknames;
  public placeholder;
  public selectedStock;
  // public selectedCountry: string = 'India';

  public isUserLoggedIn = 'false';
  public cookiesOpen: boolean;

  constructor(public dialog: MatDialog,
              private pageService: PageService,
              private cookieService: CookieService,private navbar:NavbarComponent) {
          
          }


  ngOnInit() {
    AOS.init();
    if(localStorage.getItem('cookie_consent')){
      this.cookiesOpen = false;
    }else{
      this.cookiesOpen = true;
    }

  
  }

  setDefaultValue(country)
    {
     this.navbar.setDefaultValue(country)
    }
    getStockList(country_name)
    {
      this.pageService.getStocksListSearch(country_name).subscribe(subscribedData => {
        this.stocknames = subscribedData['companies'];
        this.placeholder = subscribedData['placeholder']       
      });
    }

  setPortfolioValue(modelPortfolio) {
    this.modelPortfolioName = modelPortfolio;
  }




  openManageCookies(): void {
    const dialogRef = this.dialog.open(CookiesComponent, {
      width: '800px',
      height: 'fit-content',
      data: {}
    });
    dialogRef.afterClosed().subscribe(result => {
    });
  }
  Accept() {
    localStorage.setItem('cookie_consent', JSON.stringify(true));
    this.cookiesOpen = false;
  }
}


@Component({
  selector: 'app-cookies',
  templateUrl: './cookies.component.html',
  styleUrls: ['./app.component.scss'],
})
export class CookiesComponent implements OnInit {
  EssentialCookies: boolean = true;
  MarketingCookies : boolean;
  FunctionalCookies : boolean;
  AnalyticsCookies: boolean;
  constructor(
    public dialogRef: MatDialogRef<CookiesComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Cookies
  ) {
    dialogRef.disableClose = true;
  }
  ngOnInit() {
    this.data.EssentialCookies = true
  }
}
