import { ActivatedRoute, Router } from '@angular/router';
import { AppComponent } from '../app.component';
import { PageService } from '../service/page.service';
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { FormGroup, FormBuilder, Validators, ValidatorFn, ValidationErrors } from '@angular/forms';
import { lineChart } from './../../charts/lineChart'
import { Chart } from 'angular-highcharts';
import * as Highcharts from 'highcharts'
import { pieChart } from './../../charts/pieChart'
import 'rxjs';
import { CookieService } from 'ngx-cookie-service';
import { SuccessDialogComponent } from '../navbar/navbar.component';
import { MessageService } from 'primeng/api';
import { Sort } from '@angular/material/sort';
import { Title, Meta } from '@angular/platform-browser';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

export interface SavePortfolio {
  PortfolioName: string;
  selectedValue: string;
  defaultInvest: number;
  currency: string;
  qunatMarketCapWiseStocks: any;
  displayedCapWiseColumns: any;
  displayedSectorWiseColumns: any;
  quantSectorWiseStocks: any;
}
export interface FundamentalData {
  pscore: number;
  divyield: number;
  roe: number;
  selectedValue : any;
}
export interface TechnicalData {
  RSI: number;
  CurrentPrice: number;
  MovingAvg: boolean;
}
export interface QuantData {
  BottomTier: boolean;
  MidTier: boolean;
  TopTier: boolean;
}
export interface PortfolioProfile {
  Username: string;
  OfStocks: number;
  OfSectors: number;
}

export interface PerformanceMetrics {
  PerformanceMetrics: string;
  CumulativeReturn: string;
  AnnualizedVolatility: string;
  Sharpe: number;
  MaxDrawdown: string;
}

export interface Top10Holdings {
  Company: string;
  InvestmentAmount: string;
  ActualReturnP: string;
  ActualReturn: string
}

@Component({
  selector: 'app-wizard',
  templateUrl: './wizard.component.html',
  styleUrls: ['./wizard.component.scss'],
})
export class WizardComponent implements OnInit {
  pscore: number;
  divyield: number;
  roe: number;
  RSI: number;
  CurrentPrice: number;
  MovingAvg: boolean = false;
  BottomTier: boolean = false;
  MidTier: boolean = true;
  TopTier: boolean = true;
  firstHighlight: boolean = true;
  secondHighlight: boolean;
  thirdHighlight: boolean;
  fourthHighlight: boolean;
  fifthHighlight: boolean;
  SavePortfolioName: string;
  public outputViewData: any = [];
  output_cumulative_return: any;
  pieChart1: any;
  symbols = { 'India': 'en-IN', 'US': 'en-US' }
  currencysymbols = { 'India': '₹', 'US': '$' }
  mcapsaless = { 'India': 'Cr', 'US': 'M' }
  symbol: string;
  currencysymbol: string;
  mcapsales: string;
  loading: boolean = true;
  divloading: boolean;
  wizardFilters: any = {};
  displayedSectorWiseColumns: string[] = ['Sector', 'OfStocks']
  displayedCapWiseColumns: string[] = ['MarketCap', 'OfStocks']
  displayedWizardColumns: string[] = ['Security_Code', 'Sector', 'Market Cap Category', 'Market Cap', 'Price'];
  PortfolioProfileColumns: string[] = ['Username', 'OfStocks', 'OfSectors']
  PortfolioProfileDataSource: any = [];
  // PerformanceMetricsColumns: string[] = ['Metric/Portfolio', 'Cumulative Return(%)', 'Annualized Volatility(%)', 'Sharpe', 'Maximum Drawdown(%)']
  PerformanceMetricsColumns: string[] = ['Metric/Portfolio', 'Cumulative Return', 'Annualized Volatility', 'Sharpe', 'Maximum Drawdown']
  PerformanceMetricsDataSource: any = []
  // Top10HoldingsColumns: string[] = ['Company', 'Investment Amount(₹)', 'AnnualReturn(%)', 'AnnualReturn(₹)']
  Top10HoldingsColumns: string[] = ['Company', 'Investment Amount', 'Annual Return', 'Annual_Return']

  Top10HoldingsDataSource: any = []
  wizardData: string[] = ['Step 1 of 5', 'Step 2 of 5', 'Step 3 of 5', 'Step 4 of 5', 'Step 5 of 5'];
  Heading: string[] = ['', 'Fundamentals', 'Technicals', 'Quant Overlay', 'Portfolio Summary']
  country: string;
  indexName: string;
  indices: any = [];
  marketCapWiseStocks: any = []
  sectorWiseStocks: any = [];
  fundamentalMarketCapWiseStocks: any = []
  fundamentalSectorWiseStocks: any = [];
  technicalMarketCapWiseStocks: any = []
  technicalSectorWiseStocks: any = [];
  qunatMarketCapWiseStocks: any = []
  quantSectorWiseStocks: any = [];
  summaryMarketCapWiseStocks: any = []
  summarySectorWiseStocks: any = [];
  selectedValue: string;
  firstWizard: any;
  secondWizard: any;
  ThirdWizard: any;
  fourthWizard: any;
  status: any;
  sectors: any;
  nextHide: boolean = true;
  startOver: boolean = false;
  fundamentalFilters: any = [];
  technicalFilters: any = [];
  quantFilters1: any = [];
  quantFilters2: any = [];
  finalCount: number;
  fundamentalCount: number;
  technicalCount: number;
  quantCount: number;
  display: boolean = false;
  filterDisplay: any;
  cookieValue;
  wizardtables;
  stockDatasource: MatTableDataSource<any> = new MatTableDataSource();
  contentFlag = true; errorFlag = false;
  errorMsg;

  constructor(
    private titleService: Title, private metaService: Meta, private pageService: PageService, private route: ActivatedRoute, private router: Router,
    public myApp: AppComponent, public dialog: MatDialog,
    private cookieService: CookieService, private http: HttpClient) { }
  applyTableFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.stockDatasource.filter = filterValue.trim().toLowerCase();
  }
  private getServerErrorMessage(error: HttpErrorResponse): string {
    switch (error.status) {
      case 401: {
        return '401';
      }
      case 402: {
        return '402';
      }
      case 403: {
        return '403';
      }
      case 404: {
        return '404';
      }

      case 405: {
        return '405';
      }
      case 406: {
        return '406';
      }
      case 412: {
        return '412';
      }
      case 500: {
        return '500';
      }
      case 501: {
        return '501';
      }
      case 502: {
        return '502';
      }
      default: {
        return '500';
      }

    }
  }
  openDialogFundamental(): void {
    const dialogRef = this.dialog.open(WizardDialogComponent, {
      width: '500px',
      height: 'fit-content',
      data: { pscore: this.pscore, divyield: this.divyield, roe: this.roe }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.wizardFilters['pscore'] = result['pscore'];
      this.wizardFilters['divyield'] = result['divyield'];
      this.wizardFilters['roe'] = result['roe'];
      this.cookieService.set('wizardFilters', JSON.stringify(this.wizardFilters));
    });
  }

  openDialogTechnical(): void {
    const dialogRef = this.dialog.open(WizardDialogTechnicalComponent, {
      width: '500px',
      height: 'fit-content',
      data: { RSI: this.RSI, CurrentPrice: this.CurrentPrice, MovingAvg: this.MovingAvg }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.wizardFilters['RSI'] = result['RSI'];
      this.wizardFilters['CurrentPrice'] = result['CurrentPrice'];
      this.wizardFilters['MovingAvg'] = result['MovingAvg'];
      this.cookieService.set('wizardFilters', JSON.stringify(this.wizardFilters));
    });
  }

  openDialogQuant(): void {
    const dialogRef = this.dialog.open(WizardDialogQuantComponent, {
      width: '500px',
      height: 'fit-content',
      data: { BottomTier: this.BottomTier, MidTier: this.MidTier, TopTier: this.TopTier }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.wizardFilters['BottomTier'] = result['BottomTier'];
      this.wizardFilters['MidTier'] = result['MidTier'];
      this.wizardFilters['TopTier'] = result['TopTier'];
      this.cookieService.set('wizardFilters', JSON.stringify(this.wizardFilters));
    });
  }

  openDialogSavePortfolio(): void {
    const dialogRef = this.dialog.open(WizardSavePortfolio, {
      width: '500px',
      height: 'fit-content',
      data: { SavePortfolioName: this.SavePortfolioName, qunatMarketCapWiseStocks: this.qunatMarketCapWiseStocks, displayedCapWiseColumns: this.displayedCapWiseColumns, displayedSectorWiseColumns: this.displayedSectorWiseColumns, quantSectorWiseStocks: this.quantSectorWiseStocks }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.cookieService.set('portfolio', result['SavePortfolioName']);
      this.cookieService.set('investment', result['defaultInvest']);
    });
  }

  getStockInfopage(selectedStock, selectedSymbol) {
    let pagename = "/StockInfoPage/";
    this.router.navigate([]).then(result => { window.open(this.country + pagename + "?stockname=" + (selectedStock + " (" + selectedSymbol + ")").split("&").join("`"), '_blank', 'noopener') });
  }

  sortData(sort: Sort, tableName) {
    if (sort.active && sort.direction !== '') {
      this.wizardtables[tableName] = this[tableName].sort((a, b) => {
        const isAsc = (sort.direction === 'asc');
        return this._compare(a[sort.active], b[sort.active], isAsc);
      });
    }
    this[tableName] = Object.assign([], this.wizardtables[tableName]);
  }
  private _compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a > b ? 1 : -1) * (isAsc ? 1 : -1);
  }

  ngOnInit() {
    this.titleService.setTitle('Portfolio Wizard');
    this.metaService.addTags([
      { name: 'keywords', content: '72piwizard ,72pistockfilters ,72piinvestmentopportunity ,72pifilters ,72piportfoliowizard ,72piquantfilter ,72pitechnicalfilter ,72pifundamentalfilter,wizard ,stockfilters ,investmentopportunity ,filters ,portfoliowizard ,quantfilter ,technicalfilter ,fundamentalfilter' },
      { name: 'description', content: 'It is used to identify investment opportunities fundamental, technical and the Quant filters on stock indices.' },
    ]);
    window.scroll(0, 0);
    let element = document.querySelector('.navbar-inverse');
    element.classList.remove('navbar');
    this.myApp.imgname = 0;
    this.fourthWizard = false;
    this.wizardFilters = { 'pscore': 5, 'divyield': 0.1, 'roe': 10, 'RSI': 45, 'CurrentPrice': 70, 'MovingAvg': true, 'BottomTier': false, 'MidTier': true, 'TopTier': true }
    this.cookieService.set('wizardFilters', JSON.stringify(this.wizardFilters));
    //This is get the data from database on pageload
    this.country = this.route.snapshot.params.country;
    this.myApp.setDefaultValue(this.country)
    this.cookieService.set('country', this.country);
    //change the index name based on the country change
    this.indexName = 'S&P 500';
    this.symbol = this.symbols[this.country]
    this.currencysymbol = this.currencysymbols[this.country]
    this.mcapsales = this.mcapsaless[this.country]
    if (this.country == 'India') {
      this.indexName = 'Nifty 500';
    }
    this.cookieService.set('index', this.indexName);
    //call this service function to get the stock woth default index based on country
    this.pageService.getWizardDefaultIndexStocks(this.country, this.indexName).subscribe(response => {
      this.wizardtables = response
      this.indices = response['indices'];
      this.stockDatasource = (response['stockDatasource']);
      this.selectedValue = response['selectedIndex'];
      this.Heading[0] = this.selectedValue;
      this.sectorWiseStocks = response['sectorwisestockscount'];
      this.marketCapWiseStocks = response['marketcapwisestockscount'];
      this.cookieService.set('index', response['selectedIndex']);
      this.loading = false
    }, error => {
      if (error.error instanceof ErrorEvent) {
        this.errorMsg = `Error: ${error.error.message}`;
      }
      else {
        this.contentFlag = false;
        this.errorFlag = true;
        this.loading = false;
        this.errorMsg = this.getServerErrorMessage(error);
      }
    });
  }
  //on index change get the stocks with new index
  onIndexSelectionChange(indexName) {
    this.divloading = true;
    this.pageService.getWizardChangedIndexStocks(this.country, indexName).subscribe(response => {
    this.titleService.setTitle('Portfolio Wizard');

      this.wizardtables = response
      this.indices = response['indices'];
      this.stockDatasource = (response['stockDatasource']);
      this.selectedValue = response['selectedIndex'];
      this.Heading[0] = this.selectedValue;
      this.sectorWiseStocks = response['sectorwisestockscount'];
      this.marketCapWiseStocks = response['marketcapwisestockscount'];
      this.cookieService.set('index', response['selectedIndex']);
      this.divloading = false;
    });
  }
  resetOnStartOver() {
    this.fundamentalCount = null;
    this.technicalCount = null;
    this.quantCount = null;
    this.finalCount = null;
    this.fundamentalSectorWiseStocks = [];
    this.fundamentalMarketCapWiseStocks = [];
    this.technicalSectorWiseStocks = [];
    this.technicalMarketCapWiseStocks = [];
    this.quantSectorWiseStocks = [];
    this.qunatMarketCapWiseStocks = [];
    this.summarySectorWiseStocks = [];
    this.summaryMarketCapWiseStocks = [];
    this.wizardFilters = { 'pscore': 5, 'divyield': 0.1, 'roe': 10, 'RSI': 45, 'CurrentPrice': 70, 'MovingAvg': true, 'BottomTier': false, 'MidTier': true, 'TopTier': true }
    this.cookieService.set('wizardFilters', JSON.stringify(this.wizardFilters));
    this.onIndexSelectionChange(this.indexName)
  }
  wizard(wizard) {
    if (wizard == 0) {
      this.firstWizard = false;
      this.secondWizard = false;
      this.ThirdWizard = false;
      this.fourthWizard = false;
      this.nextHide = true;
      this.startOver = false;
      this.firstHighlight = true;
    }
    if (wizard == 1) {
      this.firstWizard = true;
      this.nextHide = true;
      this.startOver = true;
      this.firstHighlight = false;
      this.secondHighlight = true;
      this.divloading=true;

      this.wizardFilters = JSON.parse(this.cookieService.get('wizardFilters'))
      this.fundamentalFilters = { 'Piotroski_Score': this.wizardFilters['pscore'], 'Div_Yield': this.wizardFilters['divyield'], 'ROE': this.wizardFilters['roe'] };
      this.pageService.getFundamentalWizardStocks(this.country, this.selectedValue, this.fundamentalFilters).subscribe(response => {
    this.titleService.setTitle('Portfolio Wizard');

        this.wizardtables = response
        this.stockDatasource = (response['stockDatasource']);
        this.fundamentalMarketCapWiseStocks = response['marketcapwisestockscount'];
        this.fundamentalSectorWiseStocks = response['sectorwisestockscount'];
        this.fundamentalCount = response['stocksCount'];
        this.filterDisplay = this.fundamentalFilters
        if (this.fundamentalCount == 0)
          this.display = true;
        this.divloading=false;
      }, error => {
        if (error.error instanceof ErrorEvent) {
          this.errorMsg = `Error: ${error.error.message}`;
        }
        else {
          this.divloading=false;

          alert(error.statusText)
        }
      }
      );
    }
    if (wizard == 2) {
      this.divloading = true

      this.secondWizard = true;
      this.nextHide = true;
      this.startOver = true;
      this.secondHighlight = false;
      this.thirdHighlight = true;
      this.wizardFilters = JSON.parse(this.cookieService.get('wizardFilters'))
      this.technicalFilters = {
        'Piotroski_Score': this.wizardFilters['pscore'], 'Div_Yield': this.wizardFilters['divyield'], 'ROE': this.wizardFilters['roe'],
        'RSI': this.wizardFilters['RSI'], 'Current_Price': this.wizardFilters['CurrentPrice'], 'MovingAvg': this.wizardFilters['MovingAvg']
      };
      this.pageService.getTechnicalWizardStocks(this.country, this.selectedValue, this.technicalFilters).subscribe(response => {
    this.titleService.setTitle('Portfolio Wizard');

        this.wizardtables = response
        this.stockDatasource = (response['stockDatasource']);
        this.technicalMarketCapWiseStocks = response['marketcapwisestockscount'];
        this.technicalSectorWiseStocks = response['sectorwisestockscount'];
        this.technicalCount = response['stocksCount'];
        if (this.technicalCount == 0)
          this.display = true;
        this.divloading = false

      }, error => {
        if (error.error instanceof ErrorEvent) {
          this.errorMsg = `Error: ${error.error.message}`;
        }
        else {
          this.divloading=false;

          alert(error.statusText)
        }
      });
    }
    if (wizard == 3) {
      this.divloading = true
      this.ThirdWizard = true;
      this.nextHide = true;
      this.startOver = true;
      this.thirdHighlight = false;
      this.fourthHighlight = true;
      this.wizardFilters = JSON.parse(this.cookieService.get('wizardFilters'))
      this.quantFilters1 = {
        'Piotroski_Score': this.wizardFilters['pscore'], 'Div_Yield': this.wizardFilters['divyield'], 'ROE': this.wizardFilters['roe'],
        'RSI': this.wizardFilters['RSI'], 'Current_Price': this.wizardFilters['CurrentPrice'], 'MovingAvg': this.wizardFilters['MovingAvg']
      };
      this.quantFilters2 = { 'Bottom Tier': this.wizardFilters['BottomTier'], 'Mid Tier': this.wizardFilters['MidTier'], 'Top Tier': this.wizardFilters['TopTier'] };
      this.pageService.getQuantOverlayWizardStocks(this.country, this.selectedValue, this.quantFilters1, this.quantFilters2).subscribe(response => {
    this.titleService.setTitle('Portfolio Wizard');
        
        this.wizardtables = response
        this.stockDatasource = (response['stockDatasource']);
        this.qunatMarketCapWiseStocks = response['marketcapwisestockscount'];
        this.quantSectorWiseStocks = response['sectorwisestockscount'];
        this.quantCount = response['stocksCount'];
        if (this.quantCount == 0)
          this.display = true;
        this.divloading = false
      }, error => {
        if (error.error instanceof ErrorEvent) {
          this.errorMsg = `Error: ${error.error.message}`;
        }
        else {
          this.divloading=false;

          alert(error.statusText)
        }
      });
    }
    if (wizard == 4) {
      this.divloading = true

      this.fourthWizard = true;
      this.nextHide = false;
      this.startOver = true;
      this.fourthHighlight = false;
      this.fifthHighlight = true;
      this.wizardFilters = JSON.parse(this.cookieService.get('wizardFilters'))
      this.quantFilters1 = {
        'Piotroski_Score': this.wizardFilters['pscore'], 'Div_Yield': this.wizardFilters['divyield'], 'ROE': this.wizardFilters['roe'],
        'RSI': this.wizardFilters['RSI'], 'Current_Price': this.wizardFilters['CurrentPrice'], 'MovingAvg': this.wizardFilters['MovingAvg']
      };
      this.quantFilters2 = { 'Bottom Tier': this.wizardFilters['BottomTier'], 'Mid Tier': this.wizardFilters['MidTier'], 'Top Tier': this.wizardFilters['TopTier'] };
      this.pageService.getStocksSummary(this.country, this.selectedValue, this.quantFilters1, this.quantFilters2).subscribe(response => {
        this.wizardtables = response
    this.titleService.setTitle('Portfolio Wizard');

        this.stockDatasource = response['stockDatasource'];
        this.summarySectorWiseStocks = response['sectorwisestockscount'];
        this.summaryMarketCapWiseStocks = response['marketcapwisestockscount'];
        this.PortfolioProfileDataSource = [response['profile']];
        this.finalCount = response['stocksCount'];
        if (this.finalCount == 0)
          this.display = true;
        this.Top10HoldingsDataSource = response['topholdings'];
        this.PerformanceMetricsDataSource = response['table_data'];
        this.outputViewData = response;
        for (let i = 0; i < this.outputViewData['output_cumulative_return'].length; i++)
          this.convertDateToUTC(this.outputViewData, 'output_cumulative_return', i)
        this.plotCharts('output_cumulative_return', [], 300, 100, 1, "%", "%", "1");
        this.pieChart1 = new Chart(pieChart);
        this.pieChart1.options.series = [{ data: response['sectorwiseinvstamnt'] }];
        this.pieChart1.options.title = pieChart.title;
        this.pieChart1.options.chart = pieChart.chart;
        this.pieChart1.options.credits = pieChart.credits;
        this.pieChart1.options.exporting = pieChart.exporting;
        this.pieChart1.options.legend = {
          itemStyle: {
            color: '#031B4E',
            fontSize: '12px',
            fontWeight: 'bold',
            textOverflow: null
          },
        };
        this.pieChart1.options.tooltip = pieChart.tooltip;
        this.pieChart1.options.plotOptions = pieChart.plotOptions;
        this.divloading = false

      }, error => {
        if (error.error instanceof ErrorEvent) {
          this.errorMsg = `Error: ${error.error.message}`;
        }
        else {
          this.divloading=false;
          alert(error.statusText)
        }
      });

    }
  }
  plotCharts(dynamicColumn, plotLinesData, chartHeight, multiplyFactor, dividendFactor, symbol, tooltipSymbol, roundDigits, chartTitle = null, formatCurrency = "", symbol_param = "") {
    this[dynamicColumn] = new Chart(lineChart)
    let outputData = [this.outputViewData[dynamicColumn]]

    if (dynamicColumn == 'output_cumulative_return')
      outputData = this.outputViewData[dynamicColumn];
    this[dynamicColumn].options.title = {
      text: chartTitle,
      style: {
        color: '#031b4e',
        fontWeight: 'bold',
        fontSize: '14px'
      },
    },
      this[dynamicColumn].options = { series: outputData }
    this[dynamicColumn].options.exporting = lineChart.exporting
    this[dynamicColumn].options.xAxis = {
      type: 'datetime',
      tickWidth: 1,
      tickLength: 6,
      startOnTick: false,
      endOnTick: false,
      dateTimeLabelFormats: {
        day: '%b-%y',
        week: '%b-%y',
        month: '%b-%y',
        year: '%Y',
      },
      labels: {
        style: {
          color: '#031b4e',
          fontWeight: 'normal',
          fontSize: '14px'
        },
      },
    }
    this[dynamicColumn].options.yAxis = {
      gridLineWidth: 0,
      lineWidth: 1,
      tickLength: 6,
      tickWidth: 1,
      labels: {
        formatter: function () {

          return ((this.value * multiplyFactor) / dividendFactor).toFixed(1) + symbol;
        },
        style: {
          color: '#031b4e',
          fontSize: '14px',
          fontWeight: 'normal'
        },
      },
      title: {
        text: null,
        style: {
          color: '#031b4e',
          fontSize: '14px',
          fontWeight: 'bold'
        },
      },

      plotLines:
        plotLinesData
    }
    this[dynamicColumn].options.credits = lineChart.credits;
    this[dynamicColumn].options.plotOptions = {
      series: {
        marker: {
          enabled: false
        }
      }
    },
      this[dynamicColumn].options.tooltip = {
        formatter: function () {
          var s = ["Date : " + '<b>' + Highcharts.dateFormat('%d-%b-%Y', this.x)];
          for (let i = 0; i < this.points.length; i++) {
            if (dynamicColumn == 'gold_lines') {
              s.push(this.points[i].series.name + ' : <b>' + symbol_param + parseFloat(this.points[i].y).toLocaleString(formatCurrency)
                + tooltipSymbol + '</b><br>');
            }
            else {
              s.push(this.points[i].series.name + " : " + '<b>' + (this.points[i].point.y * multiplyFactor).toFixed(roundDigits) + tooltipSymbol + '</b>');
            }
          }
          return s.join('<br>');
        },
        shared: true,

        style: {
          color: '#031b4e',
          fontSize: '14px',
          fontWeight: 'normal'
        },
      }
  }
  convertDateToUTC(inputdata, dynamicColumn, extraIndex) {
    var data;
    if (extraIndex != -1)
      data = inputdata[dynamicColumn][extraIndex];
    else
      data = inputdata[dynamicColumn];
    for (let i = 0; i < data['data'].length; i++) {
      let date = new Date(data['data'][i][0]);
      var now_utc = Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(),
        date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds());
      data['data'][i][0] = now_utc;
    }
    return inputdata
  }

}
@Component({
  selector: 'app-wizard-dialog',
  templateUrl: './wizard-dialog.component.html',
  styleUrls: ['./wizard.component.scss'],
})
export class WizardDialogComponent implements OnInit {

  customFilterApply: boolean = false;
  editorForm: FormGroup;
  constructor(
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<WizardDialogComponent>,
    private cookieService: CookieService,
    @Inject(MAT_DIALOG_DATA) public data: FundamentalData
  ) {
    dialogRef.disableClose = true;
    this.editorForm = this.formBuilder.group({
      displayLabel: ['', Validators.required],
      pscore: [''],
      divYield: [''],
      roe: [''],
      notification: this.formBuilder.group(
        {
          pscore: [''],
          divYield: [''],
          roe: [''],
        },
        { validators: this.atLeastOneValidator }
      ),
    });
  }
  public atLeastOneValidator: ValidatorFn = (
    control: FormGroup
  ): ValidationErrors | any => {
    let controls = control.controls;
    if (controls) {
      let theOne = Object.keys(controls).findIndex(
        (key) => controls[key].value !== '' && controls[key].value !== null
      );
      let pscoreValidation =
        (controls.pscore.value >= 1 && controls.pscore.value <= 9) ||
        controls.pscore.value == null;
      let divYieldValidation =
        controls.divYield.value >= 0 ||
        controls.divYield.value == null;
      if (theOne === -1) {
        this.customFilterApply = true;
        return {
          atLeastOneRequired: {
            text: 'At least one should be selected',
          },
        };
      } else {
        this.customFilterApply = false;
        if (!pscoreValidation) {
          this.customFilterApply = true;
          return {
            pscoreValidationRequired: {
              text: 'The value between 1 to 9 only',
            },
          };
        }
        if (!divYieldValidation) {
          this.customFilterApply = true;
          return {
            divYieldRequired: {
              text: 'The value must be greter than zero',
            },
          };
        }
      }
    }
  };
  FundamentalRadio: string[] = ['Default Filters (recommended)', 'Custom Filters', 'Select All']
  private defaultSelected = 0
  DefaultFilters: boolean = true;
  CustomFilters: boolean;
  select(val) {
    if (val == 1) {
      this.DefaultFilters = true;
      this.CustomFilters = false;
      this.data.pscore = 5;
      this.data.divyield = 0.10;
      this.data.roe = 10;
      this.cookieService.set('wizardFilters', JSON.stringify({ 'pscore': 5, 'divyield': 0.10, 'roe': 10 }));
    }
    if (val == 2) {
      this.CustomFilters = true;
      this.DefaultFilters = false;
      this.data.pscore = null;
      this.data.divyield = null;
      this.data.roe = null;
    }
    if (val == 3) {
      this.DefaultFilters = false;
      this.CustomFilters = false;
      this.data.pscore = null;
      this.data.divyield = null;
      this.data.roe = null;
      this.cookieService.set('wizardFilters', JSON.stringify({ 'pscore': null, 'divyield': null, 'roe': null }));
    }
  }
  ngOnInit() {
    this.data.pscore = 5;
    this.data.divyield = 0.10;
    this.data.roe = 10;
  }
}
@Component({
  selector: 'app-wizard-dialog-technical',
  templateUrl: './wizard-dialog-technical.component.html',
  styleUrls: ['./wizard.component.scss'],
})
export class WizardDialogTechnicalComponent implements OnInit {
  customFilterApply: boolean = false;
  editorForm: FormGroup;
  constructor(
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<WizardDialogTechnicalComponent>,
    private cookieService: CookieService,
    @Inject(MAT_DIALOG_DATA) public data: TechnicalData
  ) {
    dialogRef.disableClose = true;
    this.editorForm = this.formBuilder.group({
      displayLabel: ['', Validators.required],
      RSI: [''],
      CurrentPrice: [''],
      MovingAvg: [''],
      notification: this.formBuilder.group(
        {
          RSI: [''],
          CurrentPrice: [''],
          MovingAvg: [''],
        },
        { validators: this.atLeastOneValidator }
      ),
    });
  }
  public atLeastOneValidator: ValidatorFn = (
    control: FormGroup
  ): ValidationErrors | any => {
    let controls = control.controls;
    if (controls) {
      let theOne = Object.keys(controls).findIndex(
        (key) => controls[key].value !== '' && controls[key].value !== null && controls[key].value !== false
      );
      let RSIValidation = (controls.RSI.value >= 0 && controls.RSI.value <= 100) || controls.RSI.value == null;
      let CurrentPriceValidation = (controls.CurrentPrice.value > 0 && controls.CurrentPrice.value <= 100) || controls.CurrentPrice.value == null;
      if (theOne === -1) {
        this.customFilterApply = true;
        return {
          atLeastOneRequired: {
            text: 'At least one should be selected',
          },
        };
      }

      else {
        this.customFilterApply = false;
        if (!RSIValidation) {
          this.customFilterApply = true;
          return {
            RSIValidationRequired: {
              text: 'The value between 0 to 100'
            }
          }
        }
        if (!CurrentPriceValidation) {
          this.customFilterApply = true;
          return {
            CurrentPriceValidationRequired: {
              text: 'The value between 0 to 100'
            }
          }
        }
      }
    }
  };
  FundamentalRadio: string[] = [
    'Default Filters (recommended)',
    'Custom Filters',
    'Select All',
  ];
  private defaultSelected = 0;
  DefaultFilters: boolean = true;
  CustomFilters: boolean;
  MovingAvg: boolean;
  select(val) {
    if (val == 1) {
      this.DefaultFilters = true;
      this.CustomFilters = false;
      this.data.RSI = 45;
      this.data.CurrentPrice = 70;
      this.data.MovingAvg = true;
      this.cookieService.set('wizardFilters', JSON.stringify({ 'RSI': 45, 'CurrentPrice': 70, 'MovingAvg': JSON.stringify(true) }));
    }
    if (val == 2) {
      this.CustomFilters = true;
      this.DefaultFilters = false;
      this.data.RSI = null;
      this.data.CurrentPrice = null;
      this.data.MovingAvg = false;
      this.cookieService.set('wizardFilters', JSON.stringify({ 'RSI': null, 'CurrentPrice': null, 'MovingAvg': JSON.stringify(false) }));
    }
    if (val == 3) {
      this.DefaultFilters = false;
      this.CustomFilters = false;
      this.data.RSI = null;
      this.data.CurrentPrice = null;
      this.data.MovingAvg = false;
      this.cookieService.set('wizardFilters', JSON.stringify({ 'RSI': null, 'CurrentPrice': null, 'MovingAvg': JSON.stringify(false) }));
    }
  }
  ngOnInit() {
    this.data.RSI = 45;
    this.data.CurrentPrice = 70;
    this.data.MovingAvg = true;
  }
}
@Component({
  selector: 'app-wizard-dialog-quant',
  templateUrl: './wizard-dialog-quant.component.html',
  styleUrls: ['./wizard.component.scss'],
})
export class WizardDialogQuantComponent implements OnInit {
  customFilterApply: boolean = false;
  editorForm: FormGroup;
  constructor(
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<WizardDialogQuantComponent>,
    private cookieService: CookieService,
    @Inject(MAT_DIALOG_DATA) public data: QuantData
  ) {
    dialogRef.disableClose = true;
    this.editorForm = this.formBuilder.group({
      displayLabel: ['', Validators.required],
      BottomTier: false,
      MidTier: false,
      TopTier: false,
      notification: this.formBuilder.group(
        {
          BottomTier: false,
          MidTier: false,
          TopTier: false,
        },
        { validators: this.atLeastOneValidator }
      ),
    });
  }
  public atLeastOneValidator: ValidatorFn = (
    control: FormGroup
  ): ValidationErrors | any => {
    let controls = control.controls;
    if (controls) {
      let theOne = Object.keys(controls).findIndex((key) => controls[key].value !== false);

      if (theOne === -1) {
        this.customFilterApply = true;
        return {
          atLeastOneRequired: {
            text: 'At least one should be selected',
          },
        };
      } else {
        this.customFilterApply = false;
      }
    }
  };
  FundamentalRadio: string[] = ['Default Filters (recommended)', 'Custom Filters', 'Select All']
  private defaultSelected = 0
  DefaultFilters: boolean = true;
  CustomFilters: boolean;
  BottomTier: boolean;
  MidTier: boolean;
  TopTier: boolean;
  select(val) {
    if (val == 1) {
      this.DefaultFilters = true;
      this.CustomFilters = false;
      this.data.BottomTier = false;
      this.data.MidTier = true;
      this.data.TopTier = true;
      this.cookieService.set('wizardFilters', JSON.stringify({ 'BottomTier': false, 'MidTier': true, 'TopTier': true }));

    }
    if (val == 2) {
      this.CustomFilters = true;
      this.DefaultFilters = false;
      this.data.BottomTier = false;
      this.data.MidTier = false;
      this.data.TopTier = false;
    }
    if (val == 3) {
      this.DefaultFilters = false;
      this.CustomFilters = false;
      this.data.BottomTier = false;
      this.data.MidTier = false;
      this.data.TopTier = false;
      this.cookieService.set('wizardFilters', JSON.stringify({ 'BottomTier': false, 'MidTier': false, 'TopTier': false }));
    }
  }
  ngOnInit() {
    this.data.BottomTier = false;
    this.data.MidTier = true;
    this.data.TopTier = true;
  }
}

@Component({
  selector: 'app-wizard-save-portfolio',
  templateUrl: './wizard-save-portfolio.component.html',
  styleUrls: ['./wizard.component.scss'],
})
export class WizardSavePortfolio implements OnInit {
  currency: string;
  panelOpenState = false;
  disableInput: boolean = false;
  selectedValue;
  PortfolioName;
  message;
  status;
  portfolioList;
  displaySelect: boolean = true;
  addToExisting: boolean = false;
  errorMsg;
  constructor(
    private http: HttpClient,
    private messageService: MessageService,
    public dialogRef: MatDialogRef<WizardSavePortfolio>,
    private cookieService: CookieService,
    private pageservice: PageService,
    @Inject(MAT_DIALOG_DATA) public data: SavePortfolio,
    public dialog: MatDialog
  ) {
    dialogRef.disableClose = true;
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  saveMyPortfolio() {
    setTimeout(() => {
      let wizardFilters = JSON.parse(this.cookieService.get('wizardFilters'));
      let quantFilters1 = {
        Piotroski_Score: wizardFilters['pscore'],
        Div_Yield: wizardFilters['divyield'],
        ROE: wizardFilters['roe'],
        RSI: wizardFilters['RSI'],
        Current_Price: wizardFilters['CurrentPrice'],
        MovingAvg: wizardFilters['MovingAvg'],
      };
      let quantFilters2 = {
        'Bottom Tier': wizardFilters['BottomTier'],
        'Mid Tier': wizardFilters['MidTier'],
        'Top Tier': wizardFilters['TopTier'],
      };
      let portfolioName;
      if (this.data.selectedValue) {
        this.addToExisting = true;
        portfolioName = this.data.selectedValue
        portfolioName = portfolioName['portfolio_value']
      }
      else {
        portfolioName = this.data.PortfolioName
      }
      let invest_amnt = this.data.defaultInvest;
      let country = this.cookieService.get('country');
      let selectedIndex = this.cookieService.get('index');
      this.pageservice.saveWizardPortfolio(country, selectedIndex, portfolioName, invest_amnt, quantFilters1, quantFilters2, this.addToExisting).subscribe(response => {


        this.message = response['message'];
        this.status = response['status'];
        this.dialogRef.close();
        const timeout = 1600;
        const dialogRef = this.dialog.open(SuccessDialogComponent, {
          width: '500px',
          data: { message: this.message, status: this.status },
        });
        dialogRef.afterOpened().subscribe((_) => {
          setTimeout(() => {
            dialogRef.close();
          }, timeout);
        });
        dialogRef.afterClosed().subscribe((result) => { });
      }, error => {
        if (error.error instanceof ErrorEvent) {
          this.errorMsg = `Error: ${error.error.message}`;
        }
        else {
          alert(error.statusText)
        }
      });
    });
  }
  portifoliochange() {
    this.disableInput = true;
  }
  ngOnInit() {
    let country = this.cookieService.get('country');
    if (country == 'India') {
      this.data.defaultInvest = 200000;
      this.data.currency = '₹';
    } else {
      this.data.defaultInvest = 5000;
      this.data.currency = '$';
    }
    this.pageservice.getUserPortfoliosList(country).subscribe(response => {
      this.portfolioList = response['user_portfolios'];
      if (this.portfolioList.length == 0) {
        this.displaySelect = false;
      }
    })
  }
}
