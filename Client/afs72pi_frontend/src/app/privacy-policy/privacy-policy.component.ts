import { Component, OnInit } from '@angular/core';
import { AppComponent } from '../app.component';
import { Title, Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-privacy-policy',
  templateUrl: './privacy-policy.component.html',
  styleUrls: ['./privacy-policy.component.scss'],
})
export class PrivacyPolicyComponent implements OnInit {
  constructor(private titleService: Title,private myApp:AppComponent) { }

  ngOnInit() {
    window.scroll(0,0);
    this.titleService.setTitle('72PI Portfolio Intelligence');
    let element = document.querySelector('.navbar-inverse');
    element.classList.remove('navbar');
    this.myApp.imgname = 0;
  }

}
