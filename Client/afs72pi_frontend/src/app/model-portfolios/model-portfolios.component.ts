import { Component, OnInit,Injectable } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import { ActivatedRoute,Router } from '@angular/router';
import { AppComponent } from '../app.component';
import { PageService } from '../service/page.service';
import { MatTabChangeEvent } from '@angular/material/tabs';
import * as Highcharts from 'highcharts'
import { lineChart } from 'src/charts/lineChart';
import { Chart } from 'angular-highcharts';
import { ViewEncapsulation } from '@angular/core';
import { Sort} from '@angular/material/sort';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { NavbarComponent } from '../navbar/navbar.component';
import { Title, Meta } from '@angular/platform-browser';
@Component({
  selector: 'app-model-portfolios',
  templateUrl: './model-portfolios.component.html',
  styleUrls: ['./model-portfolios.component.scss'],
  encapsulation: ViewEncapsulation.Emulated
})
@Injectable({
  providedIn: 'root'
})
export class ModelPortfoliosComponent implements OnInit {
  country:string;
  symbols = {'India':'en-IN','US':'en-US'}
  currencysymbols = {'India':'₹','US':'$'}
  symbol:string;
  currencysymbol:string;
  Description;
  errorMsg: string;
  constructor(private titleService: Title, private metaService: Meta,  
    private navbar:NavbarComponent,private  pageService: PageService,private route:ActivatedRoute,public myApp:AppComponent,private router: Router,private http: HttpClient) { }

  modelPortfoliosData:any;
  modelPortfoliosMonthlyData :any;
  modelPortfolioName : string;
  displayedColumns = ['Security_Code', 'Sector', 'MarketCapCategory','Quantity','Price','Market_Value','Exposure'];
  displayedColumnsGraphs = ['Portfolio_Benchmark','Annualized_Return','Annualized_Volatility','Sharpe','Maximum_DrawDown'];
  header_value : string;
  dataSource: MatTableDataSource<any> = new MatTableDataSource();
  metricsTable1 :  MatTableDataSource<any> = new MatTableDataSource();
  metricsTable2 :  MatTableDataSource<any> = new MatTableDataSource();
  performanceTable :  MatTableDataSource<any> = new MatTableDataSource();
  ror_metricsTable:MatTableDataSource<any> = new MatTableDataSource();
  displayTableColumns1 = [];
  displayTableColumns2 = [];
  ttm_return;cagr;
  displayPerfomanceTableColumns=[];
  monthlyChartData ;
  benchmarks =[];
  selectedIndex=1;
  contentFlag = true; errorFlag = false;
  loading : boolean = true;
  cumulative_returns;
  plotCharts(dynamicColumn,plotLinesData,multiplyFactor,dividendFactor,symbol,tooltipSymbol,roundDigits,chartTitle=null,formatCurrency="",symbol_param="")   
  {
  this[dynamicColumn] = new Chart(lineChart)
  this[dynamicColumn].options.title= {
    text: chartTitle,
    style: {
      color: '#031b4e',
      fontWeight: 'bold',
      fontSize: '14px'},
    },
  this[dynamicColumn].options={series:this.modelPortfoliosData[dynamicColumn]}
  this[dynamicColumn].options.xAxis = {
    type: 'datetime',
    tickWidth: 1,
    tickLength: 6,
    startOnTick: false,
    endOnTick: false,
    dateTimeLabelFormats: {
      day: '%b-%y',
      week: '%b-%y',
      month: '%b-%y',
      year: '%Y',
    },
    labels: {
      style: {
        color: '#031b4e',
        fontWeight: 'normal',
        fontSize: '14px'
      },
    },
  }
  this[dynamicColumn].options.yAxis = {
    gridLineWidth: 0,
    lineWidth: 1,
    // tickLength: 6,
    // tickWidth: 1,
    labels: {
        formatter: function () {
              return ((this.value*multiplyFactor)/dividendFactor).toFixed(0) + symbol;
        },
        style: {
            color: '#031b4e',
            fontSize:'14px',
            fontWeight: 'normal'
        },
    },
    title: {
        text: null,
        style: {
            color: '#031b4e',
            fontSize:'14px',
            fontWeight: 'bold'
        },
    },      
    plotLines: 
      plotLinesData    
  }
  this[dynamicColumn].options.plotOptions = {line: {
    marker: {
        enabled: false,
        symbol:"circle"
    }
}}
  this[dynamicColumn].options.exporting = lineChart.exporting
  this[dynamicColumn].options.chart = {
    type: 'line',
    backgroundColor: "transparent",
    zoomType: 'x',
  }
  this[dynamicColumn].options.title = lineChart.title
  this[dynamicColumn].options.credits = lineChart.credits
  this[dynamicColumn].options.tooltip = {
    formatter: function () {
      var s = ["Date : " + '<b>' + Highcharts.dateFormat('%d-%b-%Y', this.x)];
      for (let i=0;i<this.points.length;i++){            
            s.push(this.points[i].series.name + " : " + '<b>' + (this.points[i].point.y*multiplyFactor).toFixed(roundDigits) + tooltipSymbol + '</b>');
          }
          return s.join('<br>');
    },
    shared:true,
    style: {
        color: '#031b4e',
        fontSize:'14px',
        fontWeight: 'normal'
    },
}
}
convertDateToUTC(inputdata,dynamicColumn,extraIndex){
  var data;
  if (extraIndex!=-1)
    data = inputdata[dynamicColumn][extraIndex];
  else
    data = inputdata[dynamicColumn];
  for (let i = 0; i < data['data'].length; i++) {
    let date = new Date(data['data'][i][0]);
    var now_utc = Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(),
                           date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds());
    data['data'][i][0] = now_utc;
    }
  return inputdata
}
  onChange(event: MatTabChangeEvent) {
    const tab = event.tab.textLabel;
    this.selectedIndex = event.index;
    this.getDataForModelPortfolio(this.country,this.modelPortfolioName,1,tab)
  }
  ngOnInit() {
    this.titleService.setTitle('Model portfolio');
    this.metaService.addTags([
      {name: 'keywords', content: '72pimodelportfolio ,72pimarketthemes ,72piaggresiveportfolio ,72piconservativeportfolio ,72piruraltheme ,72piruralportfolio ,curatedthematicportfolio ,flagshipportfolio,modelportfolio ,marketthemes ,aggresiveportfolio ,conservativeportfolio ,ruraltheme ,ruralportfolio ,curatedthematicportfolio ,flagshipportfolio'},
      {name: 'description', content: ': It has model portfolios by risk appetite, investment preference and different market themes. '},
    ]);
    window.scroll(0,0);
    let element = document.querySelector('.navbar-inverse');
    element.classList.remove('navbar');
    this.myApp.imgname = 0;
    this.country = this.route.snapshot.params.country;
    this.symbol = this.symbols[this.country];
    this.currencysymbol = this.currencysymbols[this.country];
    this.route.queryParams.subscribe(params => {
      this.modelPortfolioName = params['portfolio']
    });
    this.myApp.setDefaultValue(this.country)
    this.getDataForModelPortfolio(this.country,this.modelPortfolioName,0);
}

monthlyPerformance(id,data,indexData,indexName){
  Highcharts.chart(id, {
    title: {
        text: null
    },
    chart:{
      backgroundColor:"transparent",
      height:"380px"

    },
      credits: {
      enabled: false
    },
    exporting: {
      buttons: {
        contextButton: {
          enabled: false
        },
      }
    },
    xAxis: {
      type: 'datetime',
      dateTimeLabelFormats: { // don't display the dummy year
        day: '%e-%b-%y',
        week: '%e-%b-%y',
        month: '%b-%y',
        year: '%Y'
      },
      labels: {
        style: {
          color: '#031B4E',
          font: '13px  ',
          fontWeight: 'normal'
        },
      },
      tickLength: 6,
      tickWidth: 1,
    },
    plotOptions:{
      series:{
        marker:{
          enabled:false
        }
      }
    },
    yAxis: {
      gridLineWidth: 0,
      lineWidth: 1,
      tickLength: 6,
      tickWidth: 1,
      title: {
        text: null
      },
      labels: {
        formatter: function () {
          return (this.value * 100).toFixed(0) + '%';
        }
      },
      plotLines: [{
        color: '#F2F2F2',
        width: 2,
        value: 0
      }]
    },
 	legend:{
  enabled:true
  },
  tooltip: {
    formatter: function () {
      var s = ["Date : " + '<b>' + Highcharts.dateFormat('%b-%Y', this.x)];
      for (let i=0;i<this.points.length;i++){            
            s.push(this.points[i].series.name + " : " + '<b>' + (this.points[i].point.y*100).toFixed(1)  + '%</b>');
          }
          return s.join('<br>');
    },
    shared:true,
    },
    series: [{
    		type:'column',
        name:"Portfolio",
        data: data,
        color:'#002060'
    },
    {
      type:'spline',
      name:indexName,
      data: indexData,
      color:'#F79824'
  }
    ],
});
}
onIndexChange(event)
  {
    this.monthlyPerformance('monthlyReturns',this.monthlyChartData,this.modelPortfoliosMonthlyData[event.target.value+'_monthly']['data'],event.target.value)
  }
  private getServerErrorMessage(error: HttpErrorResponse): string {
    switch (error.status) {
        case 401: {
            return '401';
        }
        case 402: {
            return '402';
        }
        case 403: {
            return '403';
        }
        case 404: {
          return '404';
        }
      
        case 405: {
          return '405';
        }
        case 406: {
          return '406';
        }
        case 412: {
          return '412';
        }
        case 500: {
          return '500';
        }
        case 501: {
          return '501';
        }
        case 502: {
          return '502';
        }
        default: {
            return '500';
        }

    }
  }
getDataForModelPortfolio(country,model_portfolio_name,onchangevalue,period="12M"){
    this.pageService.getModelPortfoliosVisualData(country,model_portfolio_name,period,onchangevalue).subscribe(modelPorftolioTable=>  {
    this.modelPortfoliosData=modelPorftolioTable;
    this.titleService.setTitle('Model portfolio');
    this.contentFlag = true;
    this.errorFlag = false;
      if (onchangevalue==0)
              {
              this.modelPortfoliosMonthlyData = this.modelPortfoliosData;
              this.dataSource =  this.modelPortfoliosData[model_portfolio_name]
              this.ttm_return = this.modelPortfoliosData['ttm_return']
              this.cagr = this.modelPortfoliosData['CAGR']
              this.Description = this.modelPortfoliosData['Description']

              
              this.performanceTable=this.modelPortfoliosData['monthly_cumulative']
              this.displayPerfomanceTableColumns = this.modelPortfoliosData['performancetableColumns']
              this.ror_metricsTable= this.modelPortfoliosData['ror_metrics']
              this.benchmarks =  this.modelPortfoliosData['benchmarks']
              for (let i=0;i<this.benchmarks.length;i++)
                this.convertDateToUTC(this.modelPortfoliosData,this.benchmarks[i] + "_monthly",-1)
              this.convertDateToUTC(this.modelPortfoliosData,'monthly_cumulative_chart_data',-1)
              this.monthlyChartData = this.modelPortfoliosData['monthly_cumulative_chart_data']['data']
              this.monthlyPerformance('monthlyReturns',this.monthlyChartData,this.modelPortfoliosMonthlyData[this.benchmarks[0]+"_monthly"]['data'],this.benchmarks[0])
            }
      this.displayTableColumns1 = this.modelPortfoliosData['tableColumns1']
      this.displayTableColumns2 = this.modelPortfoliosData['tableColumns2']
      this.metricsTable1 = this.modelPortfoliosData['table_data1'];
      this.metricsTable2 = this.modelPortfoliosData['table_data2'];

      for (let i=0;i<this.modelPortfoliosData['cumulative_returns'].length;i++){      
          this.convertDateToUTC(this.modelPortfoliosData,'cumulative_returns',i)}
      this.header_value = "Portfolio Return - From " + period;
      this.plotCharts('cumulative_returns',[],100,1,"%","%","1");
      this.loading = false
      }, error => {
        if (error.error instanceof ErrorEvent) {
          this.errorMsg = `Error: ${error.error.message}`;
        }
        else {
          this.contentFlag = false;
          this.errorFlag = true;
          this.loading = false;
          this.errorMsg = this.getServerErrorMessage(error);
        }
      });
    }
    currentSection = 'Overview';

    scrollTo(section) {
  
      document.querySelector('#' + section)
      .scrollIntoView();
      this.currentSection=section;
      window.scroll({
        top: pageYOffset-200,
        behavior: 'smooth'
      });


    }
    getStockInfopage(selectedStock,selectedSymbol){
      let pagename = "/StockInfoPage/";
      this.router.navigate([]).then(result => {  window.open( this.country + pagename + "?stockname=" + (selectedStock+" ("+selectedSymbol+")").split("&").join("`") , '_blank','noopener') });
    }

    sortData(sort: Sort,tableName){
      if (sort.active && sort.direction !== '')
          {
            this.modelPortfoliosData[tableName] = this[tableName].sort((a, b) => {     
            const isAsc = (sort.direction === 'asc');      
            return this._compare(a[sort.active], b[sort.active], isAsc);
            });    
          }
          this[tableName]= Object.assign([], this.modelPortfoliosData[tableName]);   
    }
    private _compare(a: number | string, b: number | string, isAsc: boolean) {
      return (a > b ? 1 : -1) * (isAsc ? 1 : -1);
    }
}
