import { Component, ViewChild, ElementRef, Injectable } from '@angular/core';
import { Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { PageService } from '../service/page.service';
import { AppComponent } from '../app.component';
import { ActivatedRoute, Router } from '@angular/router';
import * as Highcharts from "highcharts/highstock";
import { MatTabChangeEvent } from '@angular/material/tabs';
import { FormControl } from '@angular/forms';
import { MatSelect } from '@angular/material/select';
import { MatOption } from '@angular/material/core';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { NavbarComponent } from '../navbar/navbar.component';
import { Title, Meta } from '@angular/platform-browser';


interface dropdownColumnsInterface {
  def: string;
  label: string;
  hide: boolean
}
interface XYaxisInterface {
  def: string;
  label: string;
  id: number
}
@Component({
  selector: 'app-etfmonitor',
  templateUrl: './etfmonitor.component.html',
  styleUrls: ['./etfmonitor.component.scss'],
})
export class ETFmonitorComponent {
  @ViewChild('search') searchTextBox: ElementRef;

  selectFormControl = new FormControl();
  searchTextboxControl = new FormControl();
  selectedValues = [];
  data: string[] = []
  divloading: boolean;
  filteredOptions: Observable<any[]>;
  mySelections: string[];
  contentFlag = true; errorFlag = false;

  changed() {
    if (this.selectFormControl.value.length <= 30) {
      this.mySelections = this.selectFormControl.value;
    } else {
      alert("Limit exceeded")
      this.selectFormControl.setValue(this.mySelections);
    }
  }
  handleInput(event: KeyboardEvent): void {
    event.stopPropagation();
  }

  /**
   * Used to filter data based on search input 
   */
  private _filter(name: string): String[] {

    const filterValue = name.toLowerCase();
    // Set selected values to retain the selected checkbox state 
    this.setSelectedValues();
    this.selectFormControl.patchValue(this.selectedValues);
    return this.data.filter(option => option.toLowerCase().includes(filterValue));
  }
  resetFiltersETFs() {
    this.selectedETFList = []
    if (this.selectFormControl.value && this.selectFormControl.value.length > 0) {
      this.selectFormControl.value.forEach((e) => {
        this.selectedValues.splice(0, e.length)
      });
    }

  }
  /**
   * Remove from selected values based on uncheck
   */
  selectionChange(event) {
    if (event.isUserInput && event.source.selected == false) {
      let index = this.selectedValues.indexOf(event.source.value);
      this.selectedValues.splice(index, 1)
    }
  }

  openedChange(e) {
    // Set search textbox value as empty while opening selectbox 
    this.searchTextboxControl.patchValue('');
    // Focus to search textbox while clicking on selectbox
    if (e == true) {
      this.searchTextBox.nativeElement.focus();
    }
  }

  /**
   * Clearing search textbox value 
   */
  // clearSearch(event) {
  //   event.stopPropagation();
  //   this.searchTextboxControl.patchValue('');
  // }

  /**
   * Set selected values to retain the state 
   */
  setSelectedValues() {
    if (this.selectFormControl.value && this.selectFormControl.value.length > 0) {
      this.selectFormControl.value.forEach((e) => {
        if (this.selectedValues.indexOf(e) == -1) {
          this.selectedValues.push(e);
        }
      });
    }
  }

  outputViewData: any[];
  outputViewData1: any[];
  country: string;
  etfNames: any[];
  selectedETFList = [];
  selectColumns;
  etfChartData: any[];
  filterSelectObj = [];
  filterValues = {};
  dataSource = new MatTableDataSource();
  selectedList: any = '';
  eftNamesList = new FormControl();
  defaultXaxis;
  defaultYaxis;
  Highcharts = Highcharts;
  chartOptions;
  chart; chartConstructor = "chart"; chartCallback; candleChartOptions;
  flag = 0;
  downTrend;
  upTrend;
  updateFlag = false;
  loading: boolean = true;
  symbols = { 'India': 'en-IN', 'US': 'en-US' }
  currencysymbols = { 'India': '₹', 'US': '$' }
  symbol: string;
  currencysymbol: string;
  etfmarketViewTable1: any[];
  etfmarketViewTable2: any[];
  errorMsg: string;
  etfmarketViewTable3: any[];
  item;
  displayedColumns = ['security_code', 'asset_class', 'category', 'region_country', 'adj_price_close', 'mtd', 'qtd', 'ytd', 'number_1m', 'number_3m', 'number_6m', 'number_12m', 'ema50', 'ema200', 'number_50ema_200ema', 'rsi_14d', 'issuer'];
  dropdownColumns: dropdownColumnsInterface[] = [
    { def: 'number_1m', label: '1M', hide: true },
    { def: 'number_3m', label: '3M', hide: true },
    { def: 'number_6m', label: '6M', hide: true },
    { def: 'number_12m', label: '12M', hide: true },
    { def: 'ema50', label: 'EMA50', hide: true },
    { def: 'ema200', label: 'EMA200', hide: true },
    { def: 'number_50ema_200ema', label: 'Up Trend/Down Trend', hide: true },
    { def: 'rsi_14d', label: 'RSI 14D', hide: true }]

  Xaxis: XYaxisInterface[] = [{ id: 1, def: 'number_3m', label: '3M' }, { id: 2, def: 'number_6m', label: '6M' },
  { id: 3, def: 'number_12m', label: '12M' },
  { id: 4, def: 'qtd', label: 'QTD' },
  { id: 5, def: 'ytd', label: 'YTD' },]
  Yaxis: XYaxisInterface[] = [
    { id: 0, def: 'number_15d', label: '15D' },
    { id: 1, def: 'number_1m', label: '1M' },
    { id: 2, def: 'number_3m', label: '3M' },
    { id: 3, def: 'mtd', label: 'MTD' },
    { id: 4, def: 'qtd', label: 'QTD' },
    { id: 5, def: 'ytd', label: 'YTD' },]


  getDisplayedColumns(): string[] {
    return this.selectColumns.filter(cd => !cd.hide).map(cd => cd.def);
  }

  constructor(private titleService: Title, private metaService: Meta, private navbar: NavbarComponent, private pageService: PageService, public myApp: AppComponent, private route: ActivatedRoute, private router: Router, private http: HttpClient) {
    // Object to create Filter for
    const self = this;
    this.chartCallback = chart => {
      self.chart = chart;
    };
    this.filterSelectObj = [
      {
        name: 'Asset Class',
        columnProp: 'asset_class',
        options: []
      }, {
        name: 'Category',
        columnProp: 'category',
        options: []
      }, {
        name: 'Region',
        columnProp: 'region_country',
        options: []
      }
    ];

  }
  @ViewChild('select') select: MatSelect;

  allSelected = false;
  toggleAllSelection() {


    if (this.allSelected) {
      this.select.options.forEach((item: MatOption) => item.select());
    } else {
      this.select.options.forEach((item: MatOption) => item.deselect());
    }
  }
  optionClick() {
    let newStatus = true;
    this.select.options.forEach((item: MatOption) => {
      if (!item.selected) {
        newStatus = false;
      }
    });
    this.allSelected = newStatus;
  }
  ngOnInit() {
    window.scroll(0, 0);
    let element = document.querySelector('.navbar-inverse');
    element.classList.remove('navbar');
    this.myApp.imgname = 0;
    this.country = this.route.snapshot.params.country
    this.myApp.setDefaultValue(this.country);
    this.defaultXaxis = this.Xaxis[2].def;
    this.defaultYaxis = this.Yaxis[2].def;
    this.symbol = this.symbols[this.country]
    this.currencysymbol = this.currencysymbols[this.country]
    this.selectColumns = [
      { def: 'security_code', label: 'ETF', hide: false },
      { def: 'asset_class', label: 'Asset Class', hide: false },
      { def: 'category', label: 'Category', hide: false },
      { def: 'region_country', label: 'Region', hide: false },
      { def: 'adj_price_close', label: 'Adj Price Close ', hide: false },
      { def: 'mtd', label: 'MTD', hide: false },
      { def: 'qtd', label: 'QTD', hide: false },
      { def: 'ytd', label: 'YTD', hide: false },
      { def: 'number_1m', label: '1M', hide: true },
      { def: 'number_3m', label: '3M', hide: true },
      { def: 'number_6m', label: '6M', hide: true },
      { def: 'number_12m', label: '12M', hide: true },
      { def: 'ema50', label: 'EMA50', hide: true },
      { def: 'ema200', label: 'EMA200', hide: true },
      { def: 'rsi_14d', label: 'RSI 14D', hide: true },
      { def: 'number_50ema_200ema', label: '50EMA > 200EMA', hide: true }, { def: 'issuer', label: 'Issuer', hide: false }];
    this.getETFDataMain(this.country, [], this.defaultXaxis, this.defaultYaxis);



    this.titleService.setTitle('Global Multi Asset Monitor');
    this.metaService.addTags([
      { name: 'keywords', content: 'ETF ,shorttrend ,intermediatetrend ,etfmonitor ,etfselection ,globaletf ,alternatives ,etfs ,fixedincome' },
      { name: 'description', content: 'The ETF Monitor is a useful tool to track the performance of various listed ETFs in India and US markets.Tracking relevant ETFs is a useful approach to track various asset classes and market segments. Select the relevant ETFs that you wish to track by using the selection filters provided below.' },
    ]);

  }

  sortData(sort: Sort) {
    if (sort.active && sort.direction !== '') {
      this.dataSource['data'] = this.outputViewData1.sort((a, b) => {
        const isAsc = (sort.direction === 'asc');
        return this._compare(a[sort.active], b[sort.active], isAsc);
      });
    }
    this.outputViewData1 = Object.assign([], this.dataSource['data']);
    this.dataSource = new MatTableDataSource(this.outputViewData1);
  }
  private _compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a > b ? 1 : -1) * (isAsc ? 1 : -1);
  }

  onChange(event: MatTabChangeEvent) {
    if (event.tab.textLabel == 'Analytics') {
      this.flag = 1;
      this.chartOptions = this.plotchart(this.etfChartData);
    }
    else {
      this.getRemoteData(this.outputViewData);
      this.flag = 0;
    }
  }

  plotchart(data) {
    var x = this.defaultXaxis.replace("number_", "").toUpperCase();
    var y = this.defaultYaxis.replace("number_", "").toUpperCase()
    var chartOptions = {
      chart: {
        type: 'scatter',
        backgroundColor: 'transparent',
        height: 660,
      },
      title: {
        text: null
      },
      credits: {
        enabled: false
      },
      exporting: {
        buttons: {
          contextButton: {
            enabled: false
          },
        }
      },
      xAxis: {
        title: {
          text: this.defaultXaxis.replace("number_", "").toUpperCase(),
          style: {
            color: '#031b4e',
            font: '18px ',
            fontWeight: 'bold',
            fontFamily: 'Poppins'

          },
        },
        tickLength: 6,
        tickWidth: 1,
        gridLineWidth: 1,
        startOnTick: true,
        endOnTick: true,
        showLastLabel: true,
        labels: {
          formatter: function () {
            return (this.value * 100).toFixed(1) + '%';
          }
        },
        plotLines: [{
          value: 0,
          dashStyle: 'shortdash',
          width: 2,
        }]
      },
      yAxis: {
        gridLineWidth: 1,
        lineWidth: 1,
        tickLength: 6,
        tickWidth: 1,
        startOnTick: true,
        endOnTick: true,
        showLastLabel: true,
        title: {
          text: this.defaultYaxis.replace("number_", "").toUpperCase(),
          style: {
            color: '#031b4e',
            font: '18px ',
            fontWeight: 'bold',
            fontFamily: 'Poppins'
          },

        },
        labels: {
          formatter: function () {
            return (this.value * 100).toFixed(1) + '%';
          }
        },
        plotLines: [{
          value: 0,
          dashStyle: 'shortdash',
          width: 2,
        }]
      },
      plotOptions: {
        scatter: {
          marker: {
            symbol: 'cricle',
            radius: 8,
            states: {
              hover: {
                enabled: true,
                lineColor: 'rgb(100,100,100)'
              }
            }
          },
          dataLabels: {
            enabled: true,
            formatter: function () {
              return this.series.userOptions.security_code;
            },
          },
          //   events: {
          //     legendItemClick: function (event) {
          //         if(document.getElementById(this.userOptions.security_code).style.color=='rgb(204, 204, 204)'){
          //           document.getElementById(this.userOptions.security_code).style.color="#495057"
          //         }else{
          //           document.getElementById(this.userOptions.security_code).style.color="#cccccc"
          //         }

          //     }
          // },
          showInLegend: true
        }
      },
      tooltip: {
        formatter: function () {
          return "Name : <b>" + this.series.userOptions.company_name + "</b><br>ETF : <b>" + this.series.userOptions.security_code + '</b><br>' + x + ' : <b>' + (this.x * 100).toFixed(1) + '%</b><br>' + y + ' : <b>' + (this.y * 100).toFixed(1) + '%</b>';
        },
        style: {
          color: '#031b4e',
          font: '14px ',
          fontWeight: 'normal',
          fontFamily: 'Poppins'

        },
      },
      legend: {
        itemMarginTop: 5,
        itemMarginBottom: 5,
      },
      series: data
    }
    return chartOptions;
  }
  // Get Uniqu values from columns to build filter
  getFilterObject(fullObj, key) {
    const uniqChk = [];
    fullObj.filter((obj) => {
      if (!uniqChk.includes(obj[key])) {
        uniqChk.push(obj[key]);
      }
      return obj;
    });
    return uniqChk;
  }
  applyFilter(event) {
    let filterValue = event.target.value;
    this.dataSource = new MatTableDataSource(this.outputViewData);
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
    this.outputViewData1 = Object.assign([], this.dataSource['filteredData']);
  }
  private getServerErrorMessage(error: HttpErrorResponse): string {
    switch (error.status) {
      case 401: {
        return '401';
      }
      case 402: {
        return '402';
      }
      case 403: {
        return '403';
      }
      case 404: {
        return '404';
      }

      case 405: {
        return '405';
      }
      case 406: {
        return '406';
      }
      case 412: {
        return '412';
      }
      case 500: {
        return '500';
      }
      case 501: {
        return '501';
      }
      case 502: {
        return '502';
      }
      default: {
        return '500';
      }

    }
  }
  // Get remote serve data using HTTP call
  getRemoteData(remoteDummyData) {
    this.dataSource.data = remoteDummyData;
    this.filterSelectObj.filter((o) => {
      o.options = this.getFilterObject(remoteDummyData, o.columnProp);
    });
  }
  // Called on Filter change
  filterChange(filter, event) {
    let filterExist = 0;
    for (let key of Object.keys(this.filterValues)) {

      if (filter.modelValue != "") {
        if (key == filter.columnProp) {
          this.filterValues[key] = filter.modelValue.trim().toLowerCase();
          filterExist = 1;
          break;
        }
        else
          filterExist = 0;
      }
      else {
        delete this.filterValues[filter.columnProp];
        filterExist = 1;
      }
    }
    if (filterExist == 0) {
      this.filterValues[filter.columnProp] = event.value.trim().toLowerCase()
    }
    let source = this.dataSource['data']
    var lengthval = source.length;
    for (let key of Object.keys(this.filterValues)) {
      let filteredData = []
      for (let i = 0; i < lengthval; i++) {

        if (source[i][key].trim().toLowerCase() == this.filterValues[key]) {
          filteredData.push(source[i])
        }
      }
      source = filteredData;
      lengthval = source.length;
    }
    this.outputViewData1 = Object.assign([], source);
    this.filterSelectObj.filter((o) => {
      o.options = this.getFilterObject(this.outputViewData1, o.columnProp);
    });
  }

  // Reset table filters
  resetFilters() {
    this.selectedList = '';
    this.filterValues = []
    this.outputViewData1 = Object.assign([], this.outputViewData);

    this.filterSelectObj.forEach((value, key) => {
      value.modelValue = undefined;
    })
    this.dataSource.filter = "";
    this.isSelectedTagIsCallback();
    this.filterSelectObj.filter((o) => {
      o.options = this.getFilterObject(this.outputViewData, o.columnProp);
    });
  }
  isSelectedTagIsCallback() {
    for (let i = 8; i < this.selectColumns.length - 1; i++) {
      if (this.selectedList.includes(this.selectColumns[i]['def'])) {
        this.selectColumns[i]['hide'] = false
      }
      else {
        this.selectColumns[i]['hide'] = true
      }
    }
  }
  selectedData() {
    this.divloading = true;
    this.getETFDataMain(this.country, this.selectedETFList, this.defaultXaxis, this.defaultYaxis);
  }


  getStockInfopage(selectedStock, selectedSymbol, extraParam = "") {
    let pagename = "/StockInfoPage/";
    let countryName = this.country;
    if (extraParam == 'commonPage')
      countryName = 'US'
    this.router.navigate([]).then(result => { window.open(countryName + pagename + "?stockname=" + (selectedStock + " (" + selectedSymbol + ")").split("&").join("`"), '_blank', 'noopener') });
  }


  getETFDataMain(country, selectedETFList, defaultXaxis, defaultYaxis) {
    this.pageService.getETFData(this.country, this.selectedETFList, this.defaultXaxis, this.defaultYaxis).subscribe(subscribedData => {
      this.loading = true;
      this.titleService.setTitle('Global Multi Asset Monitor');
      this.contentFlag = true;
      this.errorFlag = false;
      this.outputViewData = subscribedData['etf_data'];
      this.outputViewData1 = Object.assign([], this.outputViewData);
      this.etfNames = subscribedData['etfs'];
      this.etfChartData = subscribedData['etf_chart_data'];
      this.getRemoteData(this.outputViewData);
      this.selectedETFList = subscribedData['etfdefault']
      this.chartOptions = this.plotchart(this.etfChartData);
      this.updateFlag = true;
      this.loading = false;
      this.data = subscribedData['etfs'];
      this.upTrend = subscribedData['upTrend']
      this.downTrend = subscribedData['downTrend']
      this.etfmarketViewTable1 = subscribedData['etfmarketViewTable1']
      this.etfmarketViewTable2 = subscribedData['etfmarketViewTable2']
      this.etfmarketViewTable3 = subscribedData['etfmarketViewTable3']
      this.filteredOptions = this.searchTextboxControl.valueChanges
        .pipe(
          startWith<string>(''),
          map(name => this._filter(name))
        );
      this.divloading = false;
    }, error => {
      if (error.error instanceof ErrorEvent) {
        this.errorMsg = `Error: ${error.error.message}`;
      }
      else {
        this.contentFlag = false;
        this.errorFlag = true;
        this.loading = false;
        this.errorMsg = this.getServerErrorMessage(error);
      }
    });
  }
}
