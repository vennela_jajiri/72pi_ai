import { Component, OnInit,Injectable } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { AppComponent } from '../app.component';
import { PageService } from '../service/page.service';
import * as Highcharts from "highcharts/highstock";
import { UserPortfolioService } from '../service/userPortfolios.service';
import { MatTabChangeEvent } from '@angular/material/tabs';
const HighchartsMore = require('highcharts/highcharts-more.src');
import html2canvas from "html2canvas";
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { NavbarComponent } from '../navbar/navbar.component';
import { Title, Meta } from '@angular/platform-browser';
HighchartsMore(Highcharts);

const HC_solid_gauge = require('highcharts/modules/solid-gauge.src');
HC_solid_gauge(Highcharts);
declare var require: any

@Component({
  selector: 'app-stock-profiler',
  templateUrl: './stock-profiler.component.html',
  styleUrls: ['./stock-profiler.component.scss'],
})
export class StockProfilerComponent implements OnInit {

  stockInfoFlag = 1;
  technicalFlag = 0;
  message;
  country: string;
  moving_avg_table: MatTableDataSource<any> = new MatTableDataSource();
  momentum_table: MatTableDataSource<any> = new MatTableDataSource();
  trade_table: MatTableDataSource<any> = new MatTableDataSource();
  volume_table: MatTableDataSource<any> = new MatTableDataSource();
  Highcharts = Highcharts;
  moving_avg_data: any; moving_average_main_data: any;
  Volume_Volatility;
  RSI;
  Volatility;
  MACD;
  MA9;
  Price;
  updateFlag = false;
  chart; chartConstructor = "chart";
  chartCallback;
  candleChartOptions;
  stock_names_tickers; selectedPortfolio;
  finalRating;
  MovingAverage;
  Momentum;
  Trade;
  Volume;
  loading: boolean = true;
  divloading: boolean;
  StockInfoPageStock; errorMsg: string;
  public defaultTickers = { 'India': ['500325-IN', 'Reliance Industries Ltd (RELIANCE)'], 'US': ['AAPL-US', 'Apple Inc (AAPL)'] };
  moving_average_clicks = ['MA200', 'MA50', 'Price'];
  plotline = [{
    color: 'grey',
    dashStyle: "Dash",
    width: 1,
    value: 30, zIndex: 10,
    label: {
      text: "oversold=30",
      align: 'right',
      y: 15, /*moves label down*/
      x: -10,
      style: {
        color: '#031b4e',
        font: '10px ',
        fontWeight: 'normal'
      },
    }
  },
  {
    color: 'grey',
    dashStyle: "Dash",
    width: 1,
    value: 70, zIndex: 10,
    label: {
      text: "overbought=70",
      align: 'right',
      y: 15, /*moves label down*/
      x: -10,
      style: {
        color: '#031b4e',
        font: '10px',
        fontWeight: 'normal'
      },
    }
  }]

  company_name;
  security_code;
  show = true;
  showReadMore = true;
  stringToShow;
  text;
  length = 1100;
  isCollapsed = true;
  img_url;
  public charts: any = [];
  outputViewData: any;
  Chart: any;
  companyName: string = '';
  companyDescription: any;
  keyInformation: any;
  factorDataSource: any;
  stock_ranking_data: any;
  NiftyCheck: any;
  NiftyMipcapCheck: any;
  NiftySmallcapCheck: any;
  priceCheck: any;
  MA9Check: any;
  MA20Check: any;
  MA50Check: any;
  outputData: any;
  RSIDiv: boolean = true;
  VolatilityDiv: boolean = false;
  stockname;
  stockName;
  moving_average_main_data_Stock;
  displayedColumns: string[] = ['Factor', 'Value'];
  moving_average_clicks_Stock = ['MA200', 'MA50', 'Price'];
  updateFlagStock = false;
  movingAverageOptions;
  stockPriceOptions;
  volumeOptions;
  rsiOptions;
  volatilityOptions;
  stockIndexMainData;
  stockIndexOptions;
  stockIndexClicks = [];
  stockIndexFlag = false;
  chartOptions;
  value = 0;
  plotLinesData = [
    {
      width: 1,
      value: 70,
      dashStyle: 'dash',
      label: {
        text: 'overbought=70',
        align: 'left',
        x: 10,
      },
    },
    {
      width: 1,
      value: 30,
      dashStyle: 'dash',
      label: {
        text: 'oversold=30',
        align: 'left',
        x: 10,
      },
    },
  ];
  headings: string[] = [
    'Size',
    'Valuation',
    'Growth',
    'Profitability',
    'Leverage',
    'Risk',
    'Piotroski Score',
  ];
  benchmark_headers = {
    India: [
      'Nifty 50 Index',
      'Nifty Midcap 100 Index',
      'Nifty Smallcap 100 Index',
    ],
    US: ['S&P 500', 'Dow Jones Industrial Average', 'Russell 2000'],
  };
  factorColumns: string[] = ['Factor', 'Value'];
  currency_symbol: string;
  quality_score;
  growth_score;
  technical_score;
  indv_quality_scores: any[];
  indv_growth_scores: any[];
  indv_technical_scores: any[];
  week_52_prices: any;
  StockInfoPage;
  currency_format;
  chartCallbackStock;
  stockInfocontentFlag = true; errorFlag1 = false;
  technicalIndicatorscontentFlag = true; errorFlag2 = false;

  selected = ['MA200', 'MA50', 'Price'];
  constructor(private titleService: Title, private metaService: Meta,private navbar:NavbarComponent,private technicalIndicatorsService: PageService,
    private route: ActivatedRoute, public myApp: AppComponent,
    private userService: UserPortfolioService, private router: Router,
    private pageService: PageService,private http: HttpClient) {
    
  }
  save(fileName) {
    let section = document.querySelector('#mainContainer') as HTMLCanvasElement;
    html2canvas(section,{backgroundColor:null}).then(canvas => {
        var link = document.createElement('a');
        link.href = canvas.toDataURL();
        link.download = fileName;
        document.body.appendChild(link);
        link.click();
    });
}
  addDisableSeriesStock(value) {
    if (
      this.moving_average_clicks_Stock.includes(value) &&
      this.moving_average_clicks_Stock.indexOf(value) !== -1
    ) {
      this.moving_average_clicks_Stock.splice(
        this.moving_average_clicks_Stock.indexOf(value),
        1
      );

    }
    else {
      this.moving_average_clicks_Stock.push(value);
    }
    let source_data = JSON.parse(JSON.stringify(this.moving_average_main_data_Stock));
    let updatedChartData = [];
    this.updateFlagStock = false;
    for (let i = 0; i < source_data.length; i++) {
      let flag = 0;
      for (let j = 0; j < this.moving_average_clicks_Stock.length; j++) {
        if (source_data[i].name == this.moving_average_clicks_Stock[j]) {

          flag = 1;
          updatedChartData.push(JSON.parse(JSON.stringify(source_data[i])));
        }
      }
    }
    const self = this,
      chart = this.chart;
    self.movingAverageOptions.series = JSON.parse(
      JSON.stringify(updatedChartData)
    );
    self.updateFlagStock = true;
  }
  addDisableSeries(value) {
    if (
      this.moving_average_clicks.includes(value) &&
      this.moving_average_clicks.indexOf(value) !== -1
    ) {
      this.moving_average_clicks.splice(
        this.moving_average_clicks.indexOf(value),
        1
      );
    }
    else {
      this.moving_average_clicks.push(value);
    }
    let source_data = JSON.parse(JSON.stringify(this.moving_average_main_data));
    let updatedChartData = [];
    this.updateFlag = false;
    for (let i = 0; i < source_data.length; i++) {
      let flag = 0;
      for (let j = 0; j < this.moving_average_clicks.length; j++) {
        if (source_data[i].name == this.moving_average_clicks[j]) {
          flag = 1;
          updatedChartData.push(JSON.parse(JSON.stringify(source_data[i])));
        }
      }
    }
    const self = this,
      chart = this.chart;

    self.candleChartOptions.series = JSON.parse(
      JSON.stringify(updatedChartData)
    );
    self.updateFlag = true;
  }


  addDisableIndexSeries(tabIndex) {
    if (
      this.stockIndexClicks.includes(tabIndex) &&
      this.stockIndexClicks.indexOf(tabIndex) !== -1
    ) {
      this.stockIndexClicks.splice(this.stockIndexClicks.indexOf(tabIndex), 1);
    }
    else {
      this.stockIndexClicks.push(tabIndex);
    }
    let source_data = JSON.parse(JSON.stringify(this.stockIndexMainData));
    let updatedChartData = [];
    this.stockIndexFlag = false;
    const self = this,
      chart = this.chart;
    if (tabIndex != 0) {
      updatedChartData.push(JSON.parse(JSON.stringify(source_data[0])));
      updatedChartData.push(JSON.parse(JSON.stringify(source_data[tabIndex])));
      self.stockIndexOptions.series = JSON.parse(
        JSON.stringify(updatedChartData)
      );
      self.stockIndexFlag = true;
    } else {
      self.stockIndexOptions.series = JSON.parse(JSON.stringify(source_data));
      self.stockIndexFlag = true;
    }
  }
  stockChange(event) {
    this.moving_average_clicks_Stock = ['MA200', 'MA50', 'Price']
    this.moving_average_clicks = ['MA200', 'MA50', 'Price']
    this.selected = ['MA200', 'MA50', 'Price']
    this.getTechnicalIndicatorsData(this.country, event.fs_ticker);
    this.getStockInfoData(this.country, event.fs_name);
    this.divloading = true;
  }
  private getServerErrorMessage(error: HttpErrorResponse): string {
    switch (error.status) {
        case 401: {
            return '401';
        }
        case 402: {
            return '402';
        }
        case 403: {
            return '403';
        }
        case 404: {
          return '404';
        }
      
        case 405: {
          return '405';
        }
        case 406: {
          return '406';
        }
        case 412: {
          return '412';
        }
        case 500: {
          return '500';
        }
        case 501: {
          return '501';
        }
        case 502: {
          return '502';
        }
        default: {
            return '500';
        }

    }
  } 
  ngOnInit() {
    this.loading = true;
    window.scroll(0, 0);
    let element = document.querySelector('.navbar-inverse');
    element.classList.remove('navbar');
    this.myApp.imgname = 0;
    this.country = this.route.snapshot.params.country;
    this.selectedPortfolio = this.defaultTickers[this.country][0];
 

    this.getTechnicalIndicatorsData(this.country, this.defaultTickers[this.country][0]);
    this.StockInfoPageStock = this.defaultTickers[this.country][1];
    this.myApp.setDefaultValue(this.country);
    this.stockName = this.StockInfoPageStock;
    this.stockname = this.stockName.split('`').join('&');
    this.getStockInfoData(this.country, this.stockName);

    this.titleService.setTitle('Stock Profiler');
    this.metaService.addTags([
      {name: 'keywords', content: 'technicalindicator ,stockdescription ,stockvaluation ,tickerdescription ,tickersearch ,stocksearch ,PiotroskiScore	,72piportfoliostockprice , 72piportfoliostockpricema ,72piportfoliostockpricemovingaverage ,72piportfoliostockrsi ,72piportfoliostockvolume ,72piportfoliostockvolatility ,adx ,macd ,atr ,supertrend ,cci ,crossovers ,momentum ,STOCHRSI , Stochastics , Williams%R ,VolumeOscillator , ChaikinMoneyFlow,portfoliostockprice ,portfoliostockpricema ,portfoliostockpricemovingaverage ,portfoliostockrsi ,portfoliostockvolume ,portfoliostockvolatility' },
      {name: 'description', content: 'This is a simple page showing stock information & technical indicators for various stocks.'},
    ]);
    this.pageService.getStocksListSearch(this.country).subscribe(subscribedData => {
      this.stock_names_tickers = subscribedData['securities'];
   
    });
    
  }

  getTechnicalIndicatorsData(country, ticker) {
    this.technicalIndicatorsService.getTechnicalIndicatorsViews(country, ticker).subscribe(technicalIndicators => {
      this.technicalIndicatorscontentFlag = true;
      this.errorFlag2 = false;
    this.titleService.setTitle('Stock Profiler');
      
      // this.stock_names_tickers = technicalIndicators['stock_names_tickers']
      this.MovingAverage = technicalIndicators['moving_action_count']
      this.Momentum = technicalIndicators['momentum_action_count']
      this.Trade = technicalIndicators['trade_action_count']
      this.Volume = technicalIndicators['volume_action_count']
      this.finalRating = technicalIndicators['finalRating']
      var volume_data = this.date_conversion(technicalIndicators['output_volume'])
      var rsi_data = this.date_conversion(technicalIndicators['output_rsi'])
      var volatility_data = this.date_conversion(technicalIndicators['output_volatility'])
      var macd_data = this.date_conversion(technicalIndicators['output_macd'])
      let tickpostionslist = []
      this.moving_avg_data = technicalIndicators['output_ma_prices']
      this.moving_avg_data = this.date_conversion(this.moving_avg_data)
      this.moving_average_main_data = JSON.parse(JSON.stringify(this.moving_avg_data));
      this.moving_avg_data = technicalIndicators['output_ma_prices']
      this.candleChartOptions = this.technical_factors_graph('Price vs Moving Average', this.moving_avg_data, 300, null, true, 0, true, [], technicalIndicators['max_price'], technicalIndicators['min_price'], null);
      tickpostionslist = [Math.floor(technicalIndicators['min_volume']), Math.ceil((Math.floor(technicalIndicators['min_volume']) + Math.ceil(technicalIndicators['max_volume'])) / 2), Math.ceil(technicalIndicators['max_volume'])]
      this.Volume_Volatility = this.technical_factors_graph('Volume_Volatility', volume_data, 130, 'Volume (20)', false, 0, false, [], technicalIndicators['max_volume'], technicalIndicators['min_volume'], tickpostionslist);
      tickpostionslist = [Math.floor(technicalIndicators['min_rsi']), Math.ceil((Math.floor(technicalIndicators['min_rsi']) + Math.ceil(technicalIndicators['max_rsi'])) / 2), Math.ceil(technicalIndicators['max_rsi'])]
      this.RSI = this.technical_factors_graph('RSI', rsi_data, 130, 'RSI', false, 0, false, this.plotline, technicalIndicators['max_rsi'], technicalIndicators['min_rsi'], tickpostionslist);
      tickpostionslist = [Math.floor(technicalIndicators['min_volatility']), Math.ceil((Math.floor(technicalIndicators['min_volatility']) + Math.ceil(technicalIndicators['max_volatility'])) / 2), Math.ceil(technicalIndicators['max_volatility'])]
      this.Volatility = this.technical_factors_graph('Volatility', volatility_data, 130, 'Volatility', false, 1, false, [], technicalIndicators['max_volatility'], technicalIndicators['min_volatility'], tickpostionslist);
      tickpostionslist = [Math.floor(technicalIndicators['min_macd']), Math.ceil((Math.floor(technicalIndicators['min_macd']) + Math.ceil(technicalIndicators['max_macd'])) / 2), Math.ceil(technicalIndicators['max_macd'])]
      this.MACD = this.technical_factors_graph('MACD', macd_data, 150, 'MACD', false, 0, true, [], null, null, tickpostionslist);
      this.moving_avg_table = technicalIndicators['moving_avg_table']
      this.momentum_table = technicalIndicators['momentum_table']
      this.trade_table = technicalIndicators['trade_table']
      this.volume_table = technicalIndicators['volume_table']
      let updatedChartData = [];
      const self = this,
        chart = this.chart;
      updatedChartData.push(this.candleChartOptions.series[0], this.candleChartOptions.series[4], this.candleChartOptions.series[6]);
      self.candleChartOptions.series = JSON.parse(
        JSON.stringify(updatedChartData)
      );
      this.divloading = false
    }, error => {
         
      this.divloading = false;
      this.loading = false
      if (error.error instanceof ErrorEvent) {
        this.errorMsg = `Error: ${error.error.message}`;
      }
      else {
        this.technicalIndicatorscontentFlag = false;
        this.errorFlag2 = true;
        this.loading = false;
        this.errorMsg = this.getServerErrorMessage(error);
      }
    });
  }
  technical_factors_graph(chartID, output, chartHeight, chartTitle, legendDisplay, reformat, axisDisplay, plotline, max, min, tickpositionslist) {
    var chartOptions = {
      chart: {
        height: chartHeight,
        backgroundColor: 'transparent',
        zoomType: 'xy',
      },
      title: {
        text: chartTitle, margin: 0,
        y: -35,
        x: 0,
        align: 'left',

        verticalAlign: 'middle',
        style: {
          color: '#002060',
          fontSize: '18px ',
          fontWeight: 650,
          fontFamily: 'Poppins',
        },
      },
      credits: {
        enabled: false
      },
      exporting: {
        buttons: {
          contextButton: {
            enabled: false
          },

        }
      },
      xAxis: [{
        type: 'datetime',
        dateTimeLabelFormats: {
          day: '%e-%b-%y',
          week: '%e-%b-%y',
          month: '%b-%y',
          year: '%Y'
        },
        labels: {
          enabled: axisDisplay
        },
        max: output[0]['data'][output[0]['data'].length - 1][0],
        min: output[0]['data'][0][0],
        tickLength: 0,
        tickWidth: 0,
        crosshair: true
      }],
      yAxis: [{
        title: {
          text: null,
        },
        min: min,

        max: max,
        tickPositions: tickpositionslist,
        gridLineWidth: 0,
        lineWidth: 1,
        tickLength: 6,
        tickWidth: 1,
        opposite: true,

        labels: {
          formatter: function () {
            function test(labelValue) {

              // Nine Zeroes for Billions
              return Math.abs(Number(labelValue)) >= 1.0e+9

                ? Math.ceil(Math.abs(Number(labelValue)) / 1.0e+9) + "B"
                // Six Zeroes for Millions 
                : Math.abs(Number(labelValue)) >= 1.0e+6

                  ? Math.ceil(Math.abs(Number(labelValue)) / 1.0e+6) + "M"
                  // Three Zeroes for Thousands
                  : Math.abs(Number(labelValue)) >= 1.0e+3

                    ? Math.ceil(Math.abs(Number(labelValue)) / 1.0e+3) + "K"

                    : Math.abs(Number(labelValue));
            }
            if (reformat != 1) {
              if (chartID == 'Volume_Volatility')
                return test(this.value)
              else
                return (this.value).toLocaleString('hi-IN', { maximumFractionDigits: 0 });

            }
            else {
              return (this.value).toLocaleString('hi-IN', { maximumFractionDigits: 0 }) + '%';
            }

          },
          style: {
            color: '#031b4e',
            fontWeight: 'normal',
            width: '40px',
            'min-width': '40px'
          },
          useHTML: true
        },
        plotLines: plotline,
      }],
      tooltip: {
        formatter: function () {
          var s = [];
          var count = 0
          for (var i = 0; i < this.points.length; i++) {
            // $.each(this.points, function (i, point) {
            if (count == 0)
              s.push('Date: <b>' + Highcharts.dateFormat('%d-%b-%Y', this.x) + '</b><br>');
            count = 1
            if (reformat != 1) {
              if (this.points[i].series.name == 'Price') {
                s.push("Open" + ' : <b>' + (this.points[i]['point']["open"]).toLocaleString('hi-IN', { maximumFractionDigits: 0 }) + '</b><br>')
                s.push("High" + ' : <b>' + (this.points[i]['point']["high"]).toLocaleString('hi-IN', { maximumFractionDigits: 0 }) + '</b><br>')
                s.push("Low" + ' : <b>' + (this.points[i]['point']["low"]).toLocaleString('hi-IN', { maximumFractionDigits: 0 }) + '</b><br>')
                s.push("Close" + ' : <b>' + (this.points[i]['point']["close"]).toLocaleString('hi-IN', { maximumFractionDigits: 0 }) + '</b><br>')

              }
              else {
                s.push(this.points[i].series.name + ' : <b>' + (this.points[i].y).toLocaleString('hi-IN', { maximumFractionDigits: 0 }) + '</b><br>')
              }

            }
            else {
              s.push(this.points[i].series.name + ' : <b>' + (this.points[i].y).toLocaleString('hi-IN', { maximumFractionDigits: 0 }) + '%</b><br>')
            }
          };
          return s.join('<br>');
        },
        shared: true,
      },
      plotOptions: {
        candlestick: {
          color: '#F52A4C',
          upColor: '#136F63'
        },
        series: {
          marker: {
            enabled: false,
            symbol: "circle"
          }
        }, column: {
          zones: [{
            value: 0,
            color: '#F52A4C'
          }, {
            color: '#136F63'
          }]
        }
      },

      legend: {
        enabled: false
      },
      series: output
    }
    return chartOptions;

  }
  date_conversion(output) {

    for (let i = 0; i < output.length; i++) {
      for (let j = 0; j < output[i]['data'].length; j++) {
        var temp = output[i]['data'][j][0];
        var date = new Date(temp);
        var now_utc = Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(),
          date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds());
        output[i]['data'][j][0] = now_utc;
      }
    }
    return output
  }

  convertDateToUTC(inputdata, dynamicColumn, extraIndex) {
    var data;
    if (extraIndex != -1) data = inputdata[dynamicColumn][extraIndex];
    else data = inputdata[dynamicColumn];
    for (let i = 0; i < data['data'].length; i++) {
      let date = new Date(data['data'][i][0]);
      var now_utc = Date.UTC(
        date.getUTCFullYear(),
        date.getUTCMonth(),
        date.getUTCDate(),
        date.getUTCHours(),
        date.getUTCMinutes(),
        date.getUTCSeconds()
      );
      data['data'][i][0] = now_utc;
    }
    return inputdata;
  }
  getIndividualScores(inputdata, columns_list) {
    let output_array = [];
    for (var i = 0; i < columns_list.length; i++)
      output_array.push(inputdata[columns_list[i]] * 20);
    return output_array;
  }
  getStockInfoData(country, stockname) {
    let symbols = { India: ['/gm', 'en-IN', '₹'], US: ['/oz', 'en-US', '$'] };
    this.currency_symbol = symbols[country][2];
    this.currency_format = symbols[country[1]]
    this.pageService
      .getStockInfo(country, stockname)
      .subscribe((subscribedData) => {
        this.stockInfocontentFlag = true;
    this.titleService.setTitle('Stock Profiler');
    this.errorFlag1 = false;
        this.outputViewData = subscribedData;
        this.company_name = this.outputViewData['stockName'];
        this.security_code = this.outputViewData['security_code'];
        this.img_url = this.outputViewData['img_url'];
        this.stock_ranking_data = this.outputViewData['stock_ranking'];
        this.quality_score =
          this.stock_ranking_data['quality_score'].toFixed(1) * 20;
        this.growth_score =
          this.stock_ranking_data['growth_score'].toFixed(1) * 20;
        this.technical_score =
          this.stock_ranking_data['technical_score'].toFixed(1) * 20;
        this.indv_quality_scores = this.getIndividualScores(
          this.stock_ranking_data,
          [
            'q_sales',
            'q_piotroski_score',
            'q_roe',
            'q_debt_equity',
            'q_fcf_margin',
            'q_beta',
          ]
        );
        this.indv_growth_scores = this.getIndividualScores(
          this.stock_ranking_data,
          ['g_eps_growth', 'g_sales_growth', 'g_sales_qoq']
        );
        this.indv_technical_scores = this.getIndividualScores(
          this.stock_ranking_data,
          [
            't_price_vs_ema20',
            't_price_vs_ema50',
            't_price_vs_ema200',
            't_rsi',
            't_ema50_vs_ema200',
            't_current_price_vs_52_week_high',
          ]
        );

        this.week_52_prices = this.stock_ranking_data['week_52'];
        this.companyDescription = this.outputViewData['company_description'];
        this.stringToShow = this.companyDescription;
        if (this.companyDescription.length > this.length) {
          this.stringToShow =
            this.companyDescription.slice(0, this.length - 1) + '...';
        } else {
          this.showReadMore = false;
        }
        if (this.companyDescription.length === 0) {
          this.show = false;
        }
        this.keyInformation = this.outputViewData['key_factors'][0];
        this.factorDataSource = [
          this.outputViewData['size_factors'],
          this.outputViewData['valuation_factors'],
          this.outputViewData['growth_factors'],
          this.outputViewData['profitability_factors'],
          this.outputViewData['leverage_factors'],
          this.outputViewData['risk_factors'],
          this.outputViewData['score_factors'],
        ];

        var noExtraColumn = -1;
        this.convertDateToUTC(
          this.outputViewData,
          'stock_prices',
          noExtraColumn
        );
        this.convertDateToUTC(this.outputViewData, 'volatility', noExtraColumn);
        this.convertDateToUTC(this.outputViewData, 'rsi', noExtraColumn);
        for (let i = 0; i < this.outputViewData['stock_index'].length; i++) {
          this.convertDateToUTC(this.outputViewData, 'stock_index', i);
        }
        for (
          let i = 0;
          i < this.outputViewData['output_pricevsDMA'].length;
          i++
        ) {
          this.convertDateToUTC(this.outputViewData, 'output_pricevsDMA', i);
        }

        this.moving_average_main_data_Stock = JSON.parse(
          JSON.stringify(this.outputViewData['output_pricevsDMA'])
        );

        for (let i = 0; i < this.outputViewData['volume_list'].length; i++) {
          this.convertDateToUTC(this.outputViewData, 'volume_list', i);
        }

        this.stockIndexMainData = JSON.parse(
          JSON.stringify(this.outputViewData['stock_index'])
        );
        this.stockIndexOptions = this.technical_factors_graph_Stock(
          'Stock vs Index',
          this.outputViewData['stock_index'],
          300,
          null,
          true,
          1,
          true,
          [],
          null,
          100,
          true,
          []
        );
        this.movingAverageOptions = this.technical_factors_graph_Stock(
          'Price vs Moving Average',
          this.outputViewData['output_pricevsDMA'],
          300,
          null,
          true,
          0,
          true,
          [],
          null,
          1,
          false,
          []
        );
        this.stockPriceOptions = this.technical_factors_graph_Stock(
          'Stock Prices',
          [this.outputViewData['stock_prices']],
          300,
          null,
          true,
          0,
          true,
          [],
          null,
          1,
          false,
          []
        );
        this.volumeOptions = this.technical_factors_graph_Stock(
          'Volume',
          this.outputViewData['volume_list'],
          300,
          null,
          true,
          0,
          true,
          [],
          null,
          1,
          false,
          []
        );
        this.rsiOptions = this.technical_factors_graph_Stock(
          'RSI',
          [this.outputViewData['rsi']],
          300,
          null,
          true,
          0,
          true,
          [],
          null,
          1,
          false,
          this.plotLinesData
        );
        this.volatilityOptions = this.technical_factors_graph_Stock(
          'Volatility',
          [this.outputViewData['volatility']],
          300,
          null,
          true,
          1,
          true,
          [],
          null,
          1,
          false,
          []
        );
        this.chartOptions = this.generateStockRankingChartOptions(
          this.stock_ranking_data['stock_rank']
        );
        let updatedChartData = [];
        const self = this,
          chart = this.chart;
          updatedChartData.push(this.movingAverageOptions.series[0], this.movingAverageOptions.series[4], this.movingAverageOptions.series[6]);
        self.movingAverageOptions.series = JSON.parse(
          JSON.stringify(updatedChartData)
        );
        this.divloading = false;
        this.loading = false
      }, error => {
        this.divloading = false;
        this.loading = false
        if (error.error instanceof ErrorEvent) {
          this.errorMsg = `Error: ${error.error.message}`;
        }
        else {
          this.stockInfocontentFlag = false;
          this.errorFlag1 = true;
          this.stockIndexFlag=false;
          this.errorMsg = this.getServerErrorMessage(error);
        }
      });

  }
  technical_factors_graph_Stock(
    chartID,
    output,
    chartHeight,
    chartTitle,
    legendDisplay,
    reformat,
    axisDisplay,
    plotline,
    tickpositionslist,
    multiplyFactor,
    legend,
    plotLinesData,
  ) {
    var chartOptions = {
      chart: {
        height: chartHeight,
        backgroundColor: 'transparent',
        zoomType: 'xy',
      },
      title: {
        text: chartTitle,
        margin: 0,
        y: -35,
        x: 0,
        align: 'left',

        verticalAlign: 'middle',
        style: {
          color: '#031b4e',
          fontSize: '13px ',
          fontWeight: 'bold',
        },
      },
      credits: {
        enabled: false,
      },
      legend: {
        enabled: legend
      },
      exporting: {
        buttons: {
          contextButton: {
            enabled: false,
          },
        },
      },
      plotOptions: {
        series: {
          marker: {
            enabled: false
          }
        }
      },
      xAxis: [
        {
          type: 'datetime',
          dateTimeLabelFormats: {
            day: '%e-%b-%y',
            week: '%e-%b-%y',
            month: '%b-%y',
            year: '%Y',
          },
          labels: {
            enabled: axisDisplay,
          },
          max: output[0]['data'][output[0]['data'].length - 1][0],
          min: output[0]['data'][0][0],
          tickLength: 0,
          tickWidth: 0,
          crosshair: false,
        },
      ],
      yAxis: [
        {
          title: {
            text: null,
          },
          tickPositions: tickpositionslist,
          gridLineWidth: 0,
          lineWidth: 1,
          tickLength: 6,
          tickWidth: 1,
          opposite: false,
          labels: {
            formatter: function () {
              function test(labelValue) {
                // Nine Zeroes for Billions
                return Math.abs(Number(labelValue)) >= 1.0e9
                  ? Math.ceil(Math.abs(Number(labelValue)) / 1.0e9) + 'B'
                  : // Six Zeroes for Millions
                  Math.abs(Number(labelValue)) >= 1.0e6
                    ? Math.ceil(Math.abs(Number(labelValue)) / 1.0e6) + 'M'
                    : // Three Zeroes for Thousands
                    Math.abs(Number(labelValue)) >= 1.0e3
                      ? Math.ceil(Math.abs(Number(labelValue)) / 1.0e3) + 'K'
                      : Math.abs(Number(labelValue));
              }

              if (reformat != 1) {
                if (chartID == 'Volume') return test(this.value);
                else
                  return this.value.toLocaleString('hi-IN', {
                    maximumFractionDigits: 0,
                  });
              }
              else {
                return (
                  (this.value * multiplyFactor).toLocaleString('hi-IN', {
                    maximumFractionDigits: 0,
                  }) + '%'
                );
              }
            },

            style: {
              color: '#031b4e',
              fontWeight: 'normal',
              width: '40px',
              'min-width': '40px',
            },
            useHTML: true,
          },
          plotLines: plotLinesData,
        },
      ],
      tooltip: {
        formatter: function () {
          var s = [];
          var count = 0;
          for (var i = 0; i < this.points.length; i++) {
            // $.each(this.points, function (i, point) {

            if (count == 0)
              s.push(
                'Date: <b>' +
                Highcharts.dateFormat('%d-%b-%Y', this.x) +
                '</b><br>'
              );
            count = 1;
            if (reformat != 1) {
              s.push(
                this.points[i].series.name +
                ' : <b>' +
                (this.points[i].y * multiplyFactor).toLocaleString('hi-IN', {
                  maximumFractionDigits: 0,
                }) +
                '</b><br>'
              );
            } else {
              s.push(
                this.points[i].series.name +
                ' : <b>' +
                (this.points[i].y * multiplyFactor).toLocaleString('hi-IN', {
                  maximumFractionDigits: 0,
                }) +
                '%</b><br>'
              );
            }
          }
          return s.join('<br>');
        },
        shared: true,
      },
      series: output,
    };
    return chartOptions;
  }
  toggle() {
    this.isCollapsed = !this.isCollapsed;
    if (this.isCollapsed) {
      this.stringToShow =
        this.companyDescription.slice(0, this.length - 1) + '...';
    } else {
      this.stringToShow = this.companyDescription;
    }
  }
  generateStockRankingChartOptions(rankingData) {
    this.chartOptions = {
      chart: {
        type: 'gauge',
         backgroundColor: 'transparent',
          height: 50,
          width: 120,
          margin: 0,
          padding: 0,
          spacing: [0, 0, 0, 0],
      },
     title: {
          text: null,
        },
        credits: {
          enabled: false,
        },
        exporting: {
          buttons: {
            contextButton: {
              enabled: false,
            },
          },
        },
      tooltip:{
        enabled:false
      },
      pane: {
          size: '200%',
          center: ['50%', '100%'],
          startAngle: -90,
          endAngle: 90,
          background: {
            innerRadius: '60%',
            outerRadius: '100%',
            shape: 'arc',
          },
        },
      yAxis: {
        min: 0,
        max: 100,
        minorTickInterval: 'auto',
        minorTickWidth: 0,
        minorTickLength: 0,
        minorTickPosition: 'inside',
        minorTickColor: '#666',
        tickPixelInterval: 30,
        tickWidth: 0,
        tickPosition: 'inside',
        tickLength: 0,
        tickColor: '#666',
        title: {
          text: null
        },
           labels: {
            enabled: false,
          },
        plotBands: [{
          innerRadius: "60%",
          from: 0,
          to: 100,
          color: {
            linearGradient:  { x1: 0, x2: 1, y1: 1, y2: 1 },
            stops: [
              [0, '#ECF626'], //yellow
              [1, '#389C26'] //green
            ]
        }
      },
    ]  
      },
      plotOptions: {
        gauge: {
          dial: {
            backgroundColor: 'white',
            baseWidth: 5,
            radius: '100%',
          },
          dataLabels: {
            enabled: false,
          },
          pivot: {
            radius: 15,
            borderWidth: 1,
            borderColor: 'white',
            backgroundColor: {
                linearGradient:  { x1: 0, x2: 1, y1: 1, y2: 1 },
                stops: [
                  [0, '#ECF626'], //yellow
                  [1, '#389C26'] //green
                ]
            }
        }
        }
      },
      series: [{
        name: 'Rank',
        data: [rankingData * 20],
      }]
      };
    this.value = 1;

    return this.chartOptions;
  }
  RSIVolatility(divName, otherDivName) {
    this[divName] = true;
    this[otherDivName] = false;
  }
  onChange(event: MatTabChangeEvent)
  {
    if (event.tab.textLabel=='Fundamental Factors'){
      this.stockInfoFlag=1;
      this.technicalFlag = 0;
    }
    else if(event.tab.textLabel=='Technical Indicators'){
      this.technicalFlag = 1;
      this.stockInfoFlag = 0;
    }
  }
}
  