import { Component, Inject, OnInit, HostListener,Injectable,ViewChild} from '@angular/core';
import { Observable } from '../../../node_modules/rxjs';
import { Country } from '@angular-material-extensions/select-country';
import { ActivatedRoute, Router } from '@angular/router';
import { PageService } from '../service/page.service';
import { UserPortfolioService } from '../service/userPortfolios.service';
import { AuthService } from '../service/auth.service';
import { AppComponent } from '../app.component';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSidenav } from '@angular/material/sidenav';

interface MatSelectCountryModule {
  name: string;
  value: string;
  alpha2Code: string;
  alpha3Code: string;
  numericCode: string;
}
export interface Success {
  status:string;
  message:string;
}
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})

@Injectable({
  providedIn: 'root'
})
export class NavbarComponent implements OnInit {
  public imgname: number;
  public portfolioName: string;
  public modelPortfolioName: string;
  public selectedCountry: string = 'India';
  public stocknames; 
  public placeholder; 
  public selectedStock;
  public RouteURL="";
  selectAbout;
  selectTeam;
  constructor(private router: Router, private route: ActivatedRoute,
    public dialog: MatDialog,
    private pageService: PageService, private userService: UserPortfolioService, private authService: AuthService,) { }
  LoginStatus$: Observable<boolean>;
  @ViewChild('sidenav') sidenav: MatSidenav;
  isExpanded = true;
  showSubmenu: boolean = false;
  showSubmenuAboutUs: boolean = false;
  showSubmenuMarket: boolean = false;
  showSubmenuInvestment : boolean = false;
  showSubmenuStrategies: boolean = false;
  showSubmenuPortfolio: boolean = false;
  showSubmenuXRay: boolean = false;
  showSubmenuInvestors: boolean = false;
  showSubmenuLogout: boolean = false;
  isShowing = false;
  showSubSubMenu: boolean = false;
  reason = '';
  close(reason: string) {
    this.reason = reason;
    this.sidenav.close();
  }
  predefinedCountries: MatSelectCountryModule[] = [
    {
      name: 'INDIA',
      value: 'India',
      alpha2Code: 'IN',
      alpha3Code: '',
      numericCode: '356'
    },
    {
      name: 'US',
      value: 'US',
      alpha2Code: 'US',
      alpha3Code: '',
      numericCode: '840'
    },
  ];

  defaultValue: MatSelectCountryModule ;
  ngOnInit(): void {
    this.LoginStatus$ = this.authService.isLoggedIn;
    let currentURLVal = window.location.href;
    if (currentURLVal.includes("/US/")){
      this.defaultValue =  this.predefinedCountries[1];
      this.selectedCountry='US'}
    else{
      this.defaultValue = this.predefinedCountries[0];
      this.selectedCountry='India'}
    this.getStockList(this.selectedCountry)
    
  }
  @HostListener('window:scroll', ['$event'])
  onWindowScroll(e) {
    if (window.pageYOffset === 0) {
      this.imgname = 1;
      this.selectAbout = 'active-tab'
      this.selectTeam = ''
    } else {
      this.imgname = 0;
    }
  }
  getStockList(country_name) {
    this.pageService.getStocksListSearch(country_name).subscribe(subscribedData => {
      this.stocknames = subscribedData['companies'];
      this.placeholder = subscribedData['placeholder']
    });
  }
  focusFunction(){
    (document.getElementsByClassName('ng-placeholder')[0] as HTMLElement).style.transform="translateY(-1.28125em) scale(0.75) perspective(100px) translateZ(0.001px)";

  }
  onFocusOutEvent(){
    (document.getElementsByClassName('ng-placeholder')[0] as HTMLElement).style.transform="translateY(0em) scale(1) perspective(100px) translateZ(0.001px)";
    (document.querySelector('.ng-select input') as HTMLElement).blur();
  }
  getStockInfopageGlobal(selectedStock) {
    (document.getElementsByClassName('ng-placeholder')[0] as HTMLElement).style.transform="translateY(0em) scale(1) perspective(100px) translateZ(0.001px)";
    (document.querySelector('.ng-select input') as HTMLElement).blur();
    let pagename = "/StockInfoPage/";
    this.router.navigate([]).then(result => { window.open(this.selectedCountry + pagename + "?stockname=" + selectedStock.fs_name.split("&").join("`"), '_blank'); });
  }

  setDefaultValue(country) {
    let countryNames = { 'India': ['INDIA', 'IN', '356'], 'US': ['US', 'US', '840'], }
    this.defaultValue = {
      name: countryNames[country][0],
      value: country,
      alpha2Code: countryNames[country][1],
      alpha3Code: '',
      numericCode: countryNames[country][2]
    };
    this.getStockList(country)
    this.selectedCountry = country;
  }

  async onCountrySelected($event: Country) {
    this.selectedCountry = $event['value'];
    await this.userService.onCountryChangeReset();
    await this.getStockList(this.selectedCountry)
    await this.userService.getPortfoliosList(this.selectedCountry)   
    await this.onRefresh();
  }


  toTeam() {
    this.selectTeam = 'active-tab';
    this.selectAbout = '';
    window.scroll({
      top: 3070,
      behavior: 'smooth'
    });
  }
  toAbout() {
    this.selectAbout = 'active-tab';
    this.selectTeam = '';
    window.scroll({
      top: 0,
      behavior: 'smooth'
    });
  }

  onRefresh() {
    let currentUrl = window.location.pathname.split("?")[0] + '?';
    let urlparts = currentUrl.split("/");
    if ((['india','us'].includes(urlparts[1].toLowerCase()))) {
      urlparts[1] = this.selectedCountry;
      currentUrl = urlparts.join("/")
    }
    this.router.routeReuseStrategy.shouldReuseRoute = function () { return false; };
    this.router.onSameUrlNavigation = 'reload';
      this.router.navigateByUrl(currentUrl).then(() => {
        this.router.navigated = false;
        this.router.navigate([decodeURI(this.router.url)]);
      });
  }

  // onRefresh() {
  //   let currentUrl = window.location.pathname.split("?")[0] + '?';
  //   let urlparts = currentUrl.split("/");
  //   if (urlparts[1] != '?') {
  //     urlparts[1] = this.selectedCountry;
  //     currentUrl = urlparts.join("/")
  //   } 

  //   this.router.routeReuseStrategy.shouldReuseRoute = function () { return false; };
  //   this.router.onSameUrlNavigation = 'reload';
  //     this.router.navigateByUrl(currentUrl).then(() => {
  //       this.router.navigated = false;
  //       this.router.navigate([this.router.url]);
  //     });
  // }



  openDialogLogOut(): void {
    this.showSubmenuLogout = false;
    const dialogRef = this.dialog.open(LogoutDialogComponent, {
      width: '500px',
      height: 'fit-content',
      data: {},
    });
    dialogRef.afterClosed().subscribe((result) => {

    });
  }
}



@Component({
  selector: 'app-logout-dialog',
  templateUrl: 'logout-dialog.component.html',
  styleUrls: ['logout.component.scss'],
})
export class LogoutDialogComponent implements OnInit {
  message = "You are successfully logged out"
  status = "success"
  stars: number[] = [1, 2, 3, 4, 5];
  rating: number = 0;
  click: boolean = true;
  comment : string = '';
  public selectedCountry: string = 'India';
  onCountrySelected($event: Country) {
    this.selectedCountry = $event['value'];
  }
  countStar(star) {
    this.rating = star;
    if (star > 0) {
      this.click = false
    }
  }
  addStar(star) {
    let ab = "";
    for (let i = 0; i < star; i++) {
      ab = "starId" + i;
      document.getElementById(ab).classList.add("selected");
    }
  }
  removeStar(star) {
    let ab = "";
    for (let i = star - 1; i >= this.rating; i--) {
      ab = "starId" + i;
      document.getElementById(ab).classList.remove("selected");
    }
  }
  constructor(
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<LogoutDialogComponent>,
    private authService: AuthService,
    public router: Router, private route: ActivatedRoute,
    @Inject(MAT_DIALOG_DATA) public data: Success,
  ) {
    dialogRef.disableClose = true;
  }
  LogOut(): void {
    this.dialogRef.close();
    const timeout = 1600;
    const dialogRef = this.dialog.open(SuccessDialogComponent, {
      width: '500px',
      height: 'fit-content',
      data: {message: this.message, status : this.status}
      
    });

    dialogRef.afterOpened().subscribe(_ => {
      setTimeout(() => {
        dialogRef.close();
      }, timeout)
    })
    dialogRef.afterClosed().subscribe((result) => {
      this.authService.logout().subscribe();
      this.router.navigate(['/']);
    });
  }
  Submit() {
    this.dialogRef.close();
    const timeout1 = 4000;
    const timeout2 = 2000;
    let country = this.selectedCountry;
    if(this.rating == 0 && this.comment == ''){
      this.message = "Provide Feedback or if you don't wish click on logout";
      this.status = 'error';
      const dialogRef = this.dialog.open(SuccessDialogComponent, {
        width: '500px',
        height: 'fit-content',
        data: {message: this.message, status : this.status},
      });
      dialogRef.afterOpened().subscribe(_ => {
        setTimeout(() => {
          dialogRef.close();
        }, timeout1);
      })

    }
    else{
      this.authService.saveFeedback(this.rating,this.comment,country).subscribe(response=>{
        this.message = response['message']+' You have been logged out successfully!';
        this.status = 'success';
        const dialogRef = this.dialog.open(SuccessDialogComponent, {
          width: '500px',
          height: 'fit-content',
          data: {message: this.message, status : this.status},
        });
        dialogRef.afterOpened().subscribe(_ => {
          setTimeout(() => {
            dialogRef.close();
          }, timeout2);
        })
        dialogRef.afterClosed().subscribe((result) => {
          this.authService.logout().subscribe();
          this.router.navigate(['/']);
        });
    });
  }
}
  onNoClick(): void {
    this.dialogRef.close();
  }
  ngOnInit() {
  }
}
@Component({
  selector: 'app-success-dialog',
  templateUrl: 'success-dialog.component.html',
  styleUrls: ['logout.component.scss'],
})
export class SuccessDialogComponent implements OnInit {
  constructor(
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<LogoutDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Success
  ) {
    dialogRef.disableClose = false;
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  ngOnInit() {
  }
}