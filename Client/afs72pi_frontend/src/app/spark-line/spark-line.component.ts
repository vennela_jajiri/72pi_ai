import { Component, OnInit,Input,SimpleChanges } from '@angular/core';
import * as Highcharts from 'highcharts';
import { Options } from "highcharts";

@Component({
  selector: 'app-spark-line',
  templateUrl: './spark-line.component.html',
  styleUrls: ['./spark-line.component.scss'],
})
export class SparkLineComponent implements OnInit {

  constructor() { }
  ngOnInit() {
   
  }
  ngOnChanges(change: SimpleChanges) {
  
    this.chartOptions.series=this.data;
    this.updateFlag = true;
    this.chartOptions.yAxis={
      max:this.max_value,
      min:-this.max_value,
      gridLineWidth: 0,
      title: {
        text: null
      },
          lineColor: 'transparent',
          tickLength: 0,
          labels: {
          enabled: false
        }}

  
  }
  Highcharts: typeof Highcharts = Highcharts;
  @Input() data: any[];
  @Input() max_value: number;

  updateFlag = false;
  chartOptions: Options = {
    chart: {
      height: 40,
      width: 400,
      type: 'column',
      inverted: true,
      backgroundColor: "transparent"
    },
    title: {
      text: null
    },
    credits: {
      enabled: false
    },
    exporting: {
      buttons: {
        contextButton: {
          enabled: false
        },

      }
    },
    xAxis: {
      categories: null,
      lineColor: 'transparent',
      tickLength: 0,
      labels:
      {
        enabled: false,
      },
  },
  yAxis: {
    // min:0,
    // max:0,
    // gridLineWidth: 0,
    // title: {
    //   text: null
    // },
    //   lineColor: 'transparent',
    //   tickLength: 0,
    //   labels: {
    //     enabled: false
    //   },
},

    legend: {
      enabled: false
    },
    tooltip: {
      formatter: function () {
        var s = [];
        for (let i=0;i<this.points.length;i++){            
              s.push(this.points[i].series.name + " : " + '<b>' + (this.points[i].point.y*100).toFixed(1) + "%" + '</b>');
            }
            return s.join(' ');
      },
      shared:true,

      style: {
          color: '#031b4e',
          fontSize:'13px',
          fontWeight: 'normal'
      }},
    plotOptions: {
      series: {
        animation: false
      },
      column: {
        stacking: 'normal',
          threshold: 0,
          dataLabels: {
          enabled: true,
            style: {
              fontWeight: 'normal',
              fontSize: '12px',

          },
          formatter: function() {
            return (this.y*100).toFixed(1) + '%';
          },

        },
      },
    },
  series:[]
    // series: data_list
 
  };

}
