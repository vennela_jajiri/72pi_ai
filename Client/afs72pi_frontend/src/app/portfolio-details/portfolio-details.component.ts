import { Component, OnInit, ViewChild,Injectable } from '@angular/core';
import { PageService } from '../service/page.service';
import { ConfirmationService } from 'primeng/api';
import { MessageService } from 'primeng/api';
import { AppComponent } from '../app.component';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { NavbarComponent } from '../navbar/navbar.component';
import { timer } from 'rxjs';
import { Title, Meta } from '@angular/platform-browser';

export interface StocksData {
  id?: string;
  company_name?: string;
  portfolio_name? :string;
  sector_name?: string;
  market_cap_category?: string;
  current_price?: number;
  quantity?: number;
  current_mkt_val?: number;
  new_price? : number;
  gics?: string;
}

@Component({
  selector: 'app-portfolio-details',
  templateUrl: './portfolio-details.component.html',
  styleUrls: ['./portfolio-details.component.scss'],
})
export class PortfolioDetailsComponent implements OnInit {
  public notifications = [];
  public alerts = [];
  symbols = {'India':'en-IN','US':'en-US'}
  currencysymbols = {'India':'₹','US':'$'}
  currencyModes = {'India':'INR','US':'USD'}

  mcapsaless = {'India':'Cr','US':'M'}
  symbol:string;
  public notificationsLength : number;
  public alertsLength : number;
  currencysymbol:string;
  currencyMode:string;
  public customers: any = [];
  public customers_main: any = [];

  public customers1: any = [];
  stockDialog: boolean;
  addNewStockDialog: boolean;
  addAlertDialog: boolean;
  stock: StocksData;
  Newstock: StocksData;
  AlertData: StocksData;
  selectedstocks: StocksData[];
  selectedNewStock;
  selectedPortfolio;
  submitted: boolean;
  portfolioNames = [];
  country: string;
  stockNames_dict;
  latest_portfolio;
  price_value: number;
  price_percent: number;
  price_52wh: number;
  rsi_value: number;
  left_ma: string;
  right_ma: string;
  alerttitle : string;
  message: string;
  errorMsg: string;
  lastPrice : number;
  activeTab : number;
  expand1 : boolean = true
  expand2 : boolean = false
  loading : boolean = true;
  emptyStocks : boolean = false;
  contentFlag = true; errorFlag = false;
  pagename;
  constructor(private titleService: Title,private navbar:NavbarComponent,private PageService: PageService,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    public myApp: AppComponent,
    private activeRoute: ActivatedRoute,
    private router: Router,private http: HttpClient) { }
    ngOnInit(): void {
      this.titleService.setTitle('72PI Portfolio Intelligence');
      window.scroll(0,0);
      let element = document.querySelector('.navbar-inverse');
      element.classList.remove('navbar');
      this.myApp.imgname = 0;
      this.country = this.activeRoute.snapshot.params.country
      this.symbol = this.symbols[this.country]
      this.currencysymbol = this.currencysymbols[this.country]
      this.currencyMode = this.currencyModes[this.country]
      this.myApp.setDefaultValue(this.country)
      this.PageService.getPortfolioData(this.country).subscribe(subscribedData => {
      this.contentFlag = true;
      this.errorFlag = false;

      this.customers1 = subscribedData;
      this.customers_main = this.customers1['cust_portfolio'];
      this.customers = this.customers1['cust_portfolio'];
      if(this.customers.length == 0){
        this.emptyStocks = true;
        this.loading = false;
      }else{
        this.portfolioNames = this.customers1['portfolioNames'];
        this.stockNames_dict = this.customers1['stockDetails'];
        this.selectedPortfolio = this.portfolioNames.find(portfolio_name => portfolio_name.name === this.customers1['latest_portfolio'][0]);
        this.customers = this.customers_main.filter(val => val.portfolio_name === this.selectedPortfolio.name);
        this.loading = false
      }
    }, error => {
      if (error.error instanceof ErrorEvent) {
        this.errorMsg = `Error: ${error.error.message}`;
      }
      else {
        this.contentFlag = false;
        this.errorFlag = true;
        this.loading = false;
        this.errorMsg = this.getServerErrorMessage(error);
            this.pagename = "<app-page"+this.errorMsg+"></app-page"+this.errorMsg+">";
      }
    });
  }
  hideOverlay(event, element) {     
    element.hide(event);
  }
  openNew() {
    this.stock = {};
    this.submitted = false;
    this.stockDialog = true;
  }
  openNewStock() {
    this.Newstock = {};
    this.submitted = false;
    this.addNewStockDialog = true;
  }
  createAlert(stock:StocksData){
    this.AlertData = {...stock};
    this.submitted = false;
    this.addAlertDialog = true;
    this.price_52wh=null;
    this.price_percent = null;
    this.price_value = null;
    this.rsi_value = null;
    this.left_ma = null;
    this.right_ma = null;
    this.alerttitle = "Alert For "+this.AlertData.company_name;
    this.lastPrice = this.AlertData['price'];
  }
  deleteSelectedStocks() {
    this.confirmationService.confirm({
      message: 'Are you sure you want to delete the selected Stocks?',
      header: 'Confirm',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.PageService.deleteMultipleStockFromPortfolio(this.selectedstocks, this.country);
        this.customers = this.customers.filter(val => !this.selectedstocks.includes(val));
        this.selectedstocks = null;
        this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'Stocks Deleted', life: 3000 });
        window.location.reload();
      }
    });
  }
  deleteStock(stock: StocksData) {
    this.confirmationService.confirm({
      message: 'Are you sure you want to delete ' + stock.company_name + '?',
      header: 'Confirm',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.PageService.deleteSignleStockFromPortfolio(stock.id, this.country);
        this.customers = this.customers.filter(val => val.id !== stock.id);
        this.messageService.add({ severity: 'error', summary: 'Successful', detail: 'Stock Deleted', life: 3000 });
      }
    });
  }
  hideDialog() {
    this.stockDialog = false;
    this.addNewStockDialog = false;
    this.addAlertDialog = false;
    this.submitted = false;
  }
  editStock(stock: StocksData) {
    this.stock = { ...stock };
    this.stockDialog = true;
  }
  saveEditedStock() {
    this.submitted = true;
    if (this.stock.id) {
      this.PageService.updateSignleStockFromPortfolio(this.stock, this.country);
      this.customers[this.findIndexById(this.stock.id)] = this.stock;
      this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'Stock Updated', life: 3000 });
    } else {
      this.messageService.add({ severity: 'error', summary: 'Successful', detail: 'Stock Updated', life: 3000 });
    }
    this.customers = [...this.customers];
    this.stockDialog = false;
    this.stock = {};
  }
  findIndexById(id: string): number {
    let index = -1;
    for (let i = 0; i < this.customers.length; i++) {
      if (this.customers[i].id === id) {
        index = i;
        break;
      }
    }
    return index;
  }
  getCompanyDetails(selectedNewStock) {
    this.Newstock = this.stockNames_dict.filter(val => val.company === this.selectedNewStock.company)[0];
    this.selectedNewStock = this.selectedNewStock.name
  }
  private getServerErrorMessage(error: HttpErrorResponse): string {
    switch (error.status) {
        case 401: {
            return '401';
        }
        case 402: {
            return '402';
        }
        case 403: {
            return '403';
        }
        case 404: {
          return '404';
        }
      
        case 405: {
          return '405';
        }
        case 406: {
          return '406';
        }
        case 412: {
          return '412';
        }
        case 500: {
          return '500';
        }
        case 501: {
          return '501';
        }
        case 502: {
          return '502';
        }
        default: {
            return '500';
        }

    }
  }
  saveNewStock(newStock) {
    if (newStock) {
      let stockOBJ = {
        'company_name': newStock.company,
        'sector_name': newStock.gics,
        'quantity': newStock.quantity,
        'price': newStock.new_price,
        'fs_ticker': newStock.fs_ticker,
        'country': this.country,
        'portfolio_name': this.selectedPortfolio.name,
        'portfolio_type': 'General',
        'model_portfolio': 'no',
      }
      this.PageService.saveNewStockToPortfolio(stockOBJ, this.country);
      this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'Stock Added', life: 3000 });
      timer(1000).toPromise().then(res => {
       location.reload();
      });
      

    } else {
      this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Stock Not Added', life: 3000 });
    }
    this.addNewStockDialog = false;
  }
  selected() {
    this.customers = this.customers_main.filter(val => val.portfolio_name === this.selectedPortfolio.name);
  }
  buy_sell: any[] = [{ value: 'Buy', viewValue: 'Buy' },{ value: 'Sell', viewValue: 'Sell' }];
  increase_decrease: any[] = [{value: 'Increases Above', viewValue: 'Increases Above' },{ value: 'Decreases Below', viewValue: 'Decreases Below' }];
  greater_lesser : any[] = [{ value: 'Less than', viewValue: 'Less than' },{ value: 'Greater than', viewValue: 'Greater than' }];

  selectedValue11 = this.buy_sell[0].viewValue;
  selectedValue12 = this.buy_sell[0].viewValue;
  selectedValue13 = this.buy_sell[0].viewValue;

  selectedValue21 = this.increase_decrease[0].viewValue;
  selectedValue22 = this.increase_decrease[0].viewValue;
  selectedValue23 = this.increase_decrease[0].viewValue;

  selectedValue31 = this.greater_lesser[0].viewValue;
  selectedValue32 = this.greater_lesser[0].viewValue;
  selectedValue33 = this.greater_lesser[0].viewValue;

  selectedValue41 = this.buy_sell[0].viewValue;
  selectedValue42 = this.buy_sell[0].viewValue;

  selectedValue51 = this.greater_lesser[0].viewValue;
  selectedValue52 = this.greater_lesser[0].viewValue;

  savePriceData(){
    let priceMovementAlerts = {}
    if(this.price_value || this.price_percent || this.price_52wh){
      priceMovementAlerts = {'company_name':this.AlertData.company_name,'portfolio_name':this.AlertData.portfolio_name};
      priceMovementAlerts['price_value'] = this.price_value;
      priceMovementAlerts['price_percent'] = this.price_percent;
      priceMovementAlerts['price_52wh'] = this.price_52wh;
      priceMovementAlerts['price_value_condition'] = this.selectedValue21
      priceMovementAlerts['price_percent_condition'] = this.selectedValue22
      priceMovementAlerts['price_52wh_condition'] = this.selectedValue23
      this.PageService.getPriceData(this.country,priceMovementAlerts).subscribe(subscribedData => {
        this.message = subscribedData['status']['result'];
        if(this.message=='Price Movement alerts created successfully'){
          this.messageService.add({ severity: 'success', summary: 'Success', detail: this.message, life: 3000 });
        }else{
          this.messageService.add({ severity: 'warn', summary: 'Already Created', detail: this.message, life: 3000 });
        }
      })
    }else{
      this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Invalid Input Fields', life: 3000 });
    }
  }
  saveTechData(){
    let technicalIndicatorAlerts = {};
    if(!(this.rsi_value==null) || ((this.firstSelections).length>0 && (this.secondSelections).length>0)){
      technicalIndicatorAlerts = {'company_name':this.AlertData.company_name,'portfolio_name':this.AlertData.portfolio_name,'rsi_value':this.rsi_value,
                                'dmaleft_value':this.firstSelections,'dmaright_value':this.secondSelections,'rsi_condition':this.selectedValue51,'dma_condition':this.selectedValue52};
      this.PageService.getTechData(this.country,technicalIndicatorAlerts).subscribe(subscribedData => {
          this.message = subscribedData['status']['result'];
          if(this.message=='Technical Indication alerts created successfully'){
            this.messageService.add({ severity: 'success', summary: 'Success', detail: this.message, life: 3000 });
          }else{
            this.messageService.add({ severity: 'warn', summary: 'Already Created', detail: this.message, life: 3000 });
          }
      })
    }else{
      this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Invalid Input Fields', life: 3000 });
    }
  }

  ma_values = {
    time: [
      "9 Days MA",
      "26 Days MA",
      "50 Days MA",
      "100 Days MA",
      "200 Days MA",
    ]
  };

  @ViewChild("select2") _select2: any;
  @ViewChild("select1") _select1: any;
  firstSelections: string = "";
  secondSelections: string = "";

  setFirstValues(form) {
    this.firstSelections = form.value.select1;
    if (this._select2.value) {
      const secondSelectionsValues = this._select2.value.slice();
      for (var i = secondSelectionsValues.length - 1; i >= 0; i--) {
        if (this.firstSelections.includes(secondSelectionsValues[i])) {
          secondSelectionsValues.splice(i, 1);
          this._select2.writeValue(secondSelectionsValues);
        }
      }
    }
  }
  setSecondValues(form) {
    this.secondSelections = form.value.select2;
    if (this._select1.value) {
      const firstSelectionsValues = this._select1.value.slice();
      for (var i = firstSelectionsValues.length - 1; i >= 0; i--) {
        if (this.firstSelections.includes(firstSelectionsValues[i])) {
          firstSelectionsValues.splice(i, 1);
          this._select1.writeValue(firstSelectionsValues);
        }
      }
    }
  }
  viewAlert(stock,state){
      this.PageService.getViewAlert(stock,this.country,state).subscribe(data=>{
        if(data['notifications'].length){
          if(data['state'] == 'Notified'){
          this.notifications = data['notifications'];
          this.notificationsLength = 1;
          }else if(data['state'] == 'Waiting') {
          this.alerts = data['notifications'];
          this.alertsLength = 1;
          }
        }else{
          this.notifications = [];
          this.notificationsLength = 0;
          this.alerts = [];
          this.alertsLength = 0;
        }});
  }
  alterTab(index){
    let viewType = ['Create','Waiting','Notified'];
    this.viewAlert(this.AlertData,viewType[index]);
  }
  epandClose1(){
    this.expand1 = false;
    this.expand2 = true;
  }
  epandClose2(){
    this.expand1 = true;
    this.expand2 = false;
  }
  getStockInfopage(selectedStock,selectedSymbol){
    let pagename = "/StockInfoPage/";
    this.router.navigate([]).then(result => {  window.open( this.country + pagename + "?stockname=" + (selectedStock+" ("+selectedSymbol+")").split("&").join("`") , '_blank','noopener') });
  }
}
