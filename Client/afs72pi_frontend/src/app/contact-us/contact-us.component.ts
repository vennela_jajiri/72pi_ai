import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators,FormControl } from '@angular/forms';
import { AuthService } from '../service/auth.service';
import { timer } from 'rxjs';
import { Title, Meta } from '@angular/platform-browser';
import { AppComponent } from '../app.component';
@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.scss'],
})
export class ContactUsComponent implements OnInit {

  public contactUsForm: FormGroup;
  public response : string;
  public status : string;

  constructor(private myApp:AppComponent,private formBuilder:FormBuilder,private authService: AuthService,private titleService: Title, private metaService: Meta,  
    ) { }

  get f(){return this.contactUsForm.controls;}

  ngOnInit() {
    
    this.titleService.setTitle('Contact Us | 72PI');

    window.scroll(0,0);
    let element = document.querySelector('.navbar-inverse');
    element.classList.remove('navbar');
    this.myApp.imgname = 0;
    
    this.contactUsForm = this.formBuilder.group({
      firstname: new FormControl('',[Validators.required]),
      lastname: new FormControl(''),
      email: new FormControl('',[Validators.required,Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]),
      phoneno: new FormControl('',[Validators.pattern("^([0|\+[0-9]{1,5})?([6-9][0-9]{9})$")]),
      subject: new FormControl('',[Validators.required]),
      message: new FormControl('',[Validators.required]),
    });

  }

  onSubmit(){
    if(this.contactUsForm.invalid){
      this.response='Please enter the valid details and try again';
      this.status = 'error';
    }
    else{
      const firstName = this.f.firstname.value;
      const lastName = this.f.lastname.value;
      const eMail = this.f.email.value;
      const phoneNo = this.f.phoneno.value;
      const subJect = this.f.subject.value;
      const messAge = this.f.message.value;
      this.authService.contactUs({'firstName':firstName,'lastName':lastName,'eMail':eMail,'phoneNo':phoneNo,
      'subJect':subJect,'messAge':messAge}).subscribe(data =>{   
        this.response = data['response'];
        this.status = data['status'];
      });
    }
    timer(3000).toPromise().then(res => {
      this.response='';
    });
  }
}
