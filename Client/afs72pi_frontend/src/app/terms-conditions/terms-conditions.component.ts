import { Component, OnInit } from '@angular/core';
import { Title, Meta } from '@angular/platform-browser';

import { AppComponent } from '../app.component';
@Component({
  selector: 'app-terms-conditions',
  templateUrl: './terms-conditions.component.html',
  styleUrls: ['./terms-conditions.component.scss'],
})
export class TermsConditionsComponent implements OnInit {
  constructor(private titleService: Title,private myApp: AppComponent) { }

  ngOnInit() {
    window.scroll(0, 0);
    this.titleService.setTitle('72PI Portfolio Intelligence');

    let element = document.querySelector('.navbar-inverse');
    element.classList.remove('navbar');
    this.myApp.imgname = 0;
  }

}
