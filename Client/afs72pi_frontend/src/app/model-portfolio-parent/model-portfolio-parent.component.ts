import { Component,Injectable} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PageService } from '../service/page.service';
import { ModelPortfoliosComponent } from '../model-portfolios/model-portfolios.component'
import { AppComponent } from '../app.component';
import { ViewEncapsulation } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { NavbarComponent } from '../navbar/navbar.component';
import { Title, Meta } from '@angular/platform-browser';
@Component({
  selector: 'app-model-portfolio-parent',
  templateUrl: './model-portfolio-parent.component.html',
  styleUrls: ['./model-portfolio-parent.component.scss'],
  encapsulation: ViewEncapsulation.Emulated
})

export class ModelPortfolioParentComponent {
  exports: [ModelPortfoliosComponent]
  country: string;
  errorMsg: string;
  contentFlag = true; errorFlag = false;

  constructor(private titleService: Title, private metaService: Meta,  
    private navbar:NavbarComponent,private pageService: PageService, public route: ActivatedRoute,
    private router: Router, public myApp: AppComponent,private http: HttpClient) {


  }
  private getServerErrorMessage(error: HttpErrorResponse): string {
    switch (error.status) {
        case 401: {
            return '401';
        }
        case 402: {
            return '402';
        }
        case 403: {
            return '403';
        }
        case 404: {
          return '404';
        }
      
        case 405: {
          return '405';
        }
        case 406: {
          return '406';
        }
        case 412: {
          return '412';
        }
        case 500: {
          return '500';
        }
        case 501: {
          return '501';
        }
        case 502: {
          return '502';
        }
        default: {
            return '500';
        }

    }
  }
  modelPortfoliosData: any;
  dataSource; model_portfolio_ttm_return;
  portfolioNames = { 'India': '72PI India Portfolio', 'US': '72PI US Portfolio' }; portfolio_name; itd_date; itd_return; content; investment_amount;
  content_text = { 'India': 'Actively managed Multi-cap portfolio of around 25-35 India based stocks, focused on medium term horizon (2-4 months). Exploits insights from Fundamental, Quantitative and Technical factors for delivering consistent alpha with lower drawdowns across market cycles. Typically trades 2-3 holdings per month', 'US': 'Actively managed Multi-cap portfolio of around 25-35 USA listed stocks and selected ETFs, focused on medium term horizon (2-4 months). Exploits insights from Fundamental, Quantitative and Technical factors for delivering consistent alpha with lower drawdowns across market cycles. Typically trades 2-3 holdings per month' }
  inv_amount = { 'India': '10L', 'US': '50K' }
  currencySymbols= { 'India': '₹', 'US': '$' };
  currencySymbol;
  ngOnInit() {
    this.titleService.setTitle('Model portfolio');
    this.metaService.addTags([
      {name: 'keywords', content: '72pimodelportfolio ,72pimarketthemes ,72piaggresiveportfolio ,72piconservativeportfolio ,72piruraltheme ,72piruralportfolio ,curatedthematicportfolio ,flagshipportfolio,modelportfolio ,marketthemes ,aggresiveportfolio ,conservativeportfolio ,ruraltheme ,ruralportfolio ,curatedthematicportfolio ,flagshipportfolio'},
      {name: 'description', content: ': It has model portfolios by risk appetite, investment preference and different market themes. '},
    ]);
    window.scroll(0, 0);
    let element = document.querySelector('.navbar-inverse');
    element.classList.remove('navbar');
    this.myApp.imgname = 0;
    this.country = this.route.snapshot.params.country;
    this.myApp.setDefaultValue(this.country)
    this.portfolio_name = this.portfolioNames[this.country]
    this.content = this.content_text[this.country]
    this.investment_amount = this.inv_amount[this.country]
    this.currencySymbol=this.currencySymbols[this.country]
    this.pageService.getModelPortfoliosData(this.country).subscribe(modelPortfolioTable => {
      this.titleService.setTitle('Model portfolio');
      this.contentFlag = true;
      this.errorFlag = false;
      this.modelPortfoliosData = modelPortfolioTable;
      this.dataSource = this.modelPortfoliosData['modelPortfolioTable']
      this.itd_date = modelPortfolioTable['itd_date']
      this.itd_return = modelPortfolioTable['itd_return']
      this.model_portfolio_ttm_return = this.dataSource[0]['_1Y']
    }, error => {
      if (error.error instanceof ErrorEvent) {
        this.errorMsg = `Error: ${error.error.message}`;
      }
      else {
        this.contentFlag = false;
        this.errorFlag = true;
        this.errorMsg = this.getServerErrorMessage(error);
      }
    });

  }
  GetModelPortfolio(portfolioName) {
    let pagename = "/ViewModelPortfolios/"
    this.router.navigate([]).then(result => { window.open(this.country + pagename + "?portfolio=" + portfolioName, '_self'); });
  }
  GetourPortfolio() {
    let pagename = "/OurPortfolioPerformance/"
    window.open(this.country + pagename, '_self');
  }


}

