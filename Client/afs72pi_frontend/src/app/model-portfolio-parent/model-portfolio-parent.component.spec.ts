import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ModelPortfolioParentComponent } from './model-portfolio-parent.component';

describe('ModelPortfolioParentComponent', () => {
  let component: ModelPortfolioParentComponent;
  let fixture: ComponentFixture<ModelPortfolioParentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModelPortfolioParentComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ModelPortfolioParentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
