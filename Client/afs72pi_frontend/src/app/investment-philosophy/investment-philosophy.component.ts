import { Component, OnInit } from '@angular/core';
import { AppComponent } from '../app.component';

@Component({
  selector: 'app-investment-philosophy',
  templateUrl: './investment-philosophy.component.html',
  styleUrls: ['./investment-philosophy.component.scss']
})
export class InvestmentPhilosophyComponent implements OnInit {

  constructor(private myApp:AppComponent) { }
 
  ngOnInit(): void {
    window.scroll(0,0);
    let element = document.querySelector('.navbar-inverse');
    element.classList.remove('navbar');
    this.myApp.imgname = 0;
  }

}
