import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Page412Component } from './page412.component';

describe('Page412Component', () => {
  let component: Page412Component;
  let fixture: ComponentFixture<Page412Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Page412Component ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Page412Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
