import { Component, OnInit } from '@angular/core';
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'app-page412',
  templateUrl: './page412.component.html',
  styleUrls: ['./page412.component.scss'],
})
export class Page412Component implements OnInit {

  constructor(private titleService:Title) {
    this.titleService.setTitle("Sorry, Server evaluation failed!");
  }

  ngOnInit() {
    window.scroll(0,0);
    let element = document.querySelector('.navbar-inverse');
    element.classList.remove('navbar');
  }

}
