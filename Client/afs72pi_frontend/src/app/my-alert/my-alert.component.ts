import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { PrimeNGConfig } from 'primeng/api';
import { PageService } from '../service/page.service';
import { AppComponent } from '../app.component';
import { ActivatedRoute } from '@angular/router';
import { MessageService } from 'primeng/api';

export interface AlertData {
  id: number;
  CompanyName: string;
  PortfolioName: string;
  Type: string;
  BuySell: string;
  Condition: string;
  Price: number;
  Action: any;
}


'use strict';
export var deleteItem: any;
export var editBuySell: any;
export var editAboveBelow: any;
export var editPrice: any;

@Component({
  selector: 'app-my-alert',
  templateUrl: './my-alert.component.html',
  styleUrls: ['./my-alert.component.scss'],
})
export class MyAlertComponent implements OnInit {
  displayedColumns: string[] = ['id','Company','Portfolio Name','Type','Buy/Sell','Metric','Condition','Value','Action'];
  dataSource ;
  Conditions: string;
  dict: any;
  edit: boolean = false;
  delete: boolean = false;
  isAlertsExist : boolean = false;
  price_value;
  left_parameter;
  buy_sell: any[] = [{ value: 'Buy', viewValue: 'Buy' }, { value: 'Sell', viewValue: 'Sell' }];
  increase_decrease: any[] = [{ value: 'Increases Above', viewValue: 'Increases Above' }, { value: 'Decreases Below', viewValue: 'Decreases Below' }];
  less_greater: any[] = [{ value: 'Less than', viewValue: 'Less than' }, { value: 'Greater than', viewValue: 'Greater than' }];
  selectedValue11;
  selectedValue21;
  left_previous;
  header_name;
  obj;
  country;
  id;
  subtype;
  type;
  metric;
  ma_values = { time: [ "ma9", "ma26", "ma50", "ma100", "ma200",]};
  constructor(public dialog: MatDialog, 
              private primengConfig: PrimeNGConfig,
              private pageService: PageService, 
              public myApp: AppComponent, 
              private route: ActivatedRoute,
              private messageService:MessageService) { }

  @ViewChild("select2") _select2: any;
  @ViewChild("select1") _select1: any;
  firstSelections: string = "";
  secondSelections: string = "";

  setFirstValues(form) {
    this.firstSelections = form.value.select1;
    if (this._select2.value) {
      const secondSelectionsValues = this._select2.value.slice();
      for (var i = secondSelectionsValues.length - 1; i >= 0; i--) {
        if (this.secondSelections.includes(secondSelectionsValues[i])) {
          secondSelectionsValues.slice(i, 1);
          this._select2.writeValue(secondSelectionsValues);
        }
      }
    }
    this.left_parameter = this.firstSelections;
  }
  setSecondValues(form) {
    this.secondSelections = form.value.select2;
    if (this._select1.value) {
      const firstSelectionsValues = this._select1.value.slice();
      for (var i = firstSelectionsValues.length - 1; i >= 0; i--) {
        if (this.firstSelections.includes(firstSelectionsValues[i])) {
          firstSelectionsValues.slice(i, 1);
          this._select1.writeValue(firstSelectionsValues);
        }
      }
    }
    this.price_value = this.secondSelections;
  }


  editStock(element) {
    this.edit = true;
    this.obj=element;
    this.id = element.id;
    this.metric = element.symbol;
    this.subtype = element.subtype;
    this.type = element.Type;
    this.selectedValue11 = element['Buy/Sell'];
    this.selectedValue21 = element.Condition;
    this.left_previous = element.Metric;
    this.price_value = element.Value;
    this.header_name = element.Company;
  }

  closeExpand(){
    this.edit = false;
    this.delete = false;
  }

  deleteStock(element) {
    this.delete = true;
    this.obj=element;
    this.header_name = element.Company;
  }

  ngOnInit() {
    window.scroll(0, 0);
    let element = document.querySelector('.navbar-inverse');
    element.classList.remove('navbar');
    this.country = this.route.snapshot.params.country;
    this.primengConfig.ripple = true;
    this.pageService.getMyAlerts(this.country).subscribe(response=>{
      this.isAlertsExist = false;
      if(response['alerts'].length){
        for(var i=0; i<response['alerts'].length; i++){
          response['alerts'][i]['Actions'] = '';
          response['alerts'][i]['Value'] = response['alerts'][i]['Value'];
        }
        this.dataSource = response['alerts'];
        this.isAlertsExist = true;
      }
    });

  }

  saveData(){
    let params={'object':this.obj,'leftvalue':this.left_parameter,'condition':this.selectedValue21,'rightvalue':this.price_value};
    this.pageService.editAlertData(params,this.country).subscribe(response=>{
      if(response['response']['status'] == 'True'){
        this.messageService.add({severity: 'success', summary:'Edited Successfully!!!', detail:  response['response']['result']});
      }
      else{
        this.messageService.add({severity: 'error', summary: 'Cannot be Edited', detail: response['response']['result']});
      }
      this.pageService.getMyAlerts(this.country).subscribe(response=>{
        for(var i=0; i<response['alerts'].length; i++){
          response['alerts'][i]['Actions'] = '';
        }
        this.dataSource = response['alerts']
      });
    });
    this.edit = false;

  }

  deleteData(){
    let params={'object':this.obj};

    this.pageService.deleteAlertData(params,this.country).subscribe(response=>{
      if(response['response']['status'] == 'True'){
        this.messageService.add({severity: 'success', summary:'Edited Successfully!!!', detail:  response['response']['result']});
      }
      else{
        this.messageService.add({severity: 'error', summary: 'Cannot be Edited', detail: response['response']['result']});
      }
      this.pageService.getMyAlerts(this.country).subscribe(response=>{
        for(var i=0; i<response['alerts'].length; i++){
          response['alerts'][i]['BuySell'] = 'Buy';
          response['alerts'][i]['Actions'] = '';
        }
        this.dataSource = response['alerts']
      });
    });
    this.delete = false;
    
  }


}
