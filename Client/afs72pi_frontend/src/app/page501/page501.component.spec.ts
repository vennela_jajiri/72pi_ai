import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Page501Component } from './page501.component';

describe('Page501Component', () => {
  let component: Page501Component;
  let fixture: ComponentFixture<Page501Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Page501Component ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Page501Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
