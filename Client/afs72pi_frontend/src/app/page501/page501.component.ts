import { Component, OnInit } from '@angular/core';
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'app-page501',
  templateUrl: './page501.component.html',
  styleUrls: ['./page501.component.scss'],
})
export class Page501Component implements OnInit {

  constructor(private titleService:Title) {
    this.titleService.setTitle("Sorry, server error!");
  }

  ngOnInit() {
    window.scroll(0,0);
    let element = document.querySelector('.navbar-inverse');
    element.classList.remove('navbar');
  }

}
