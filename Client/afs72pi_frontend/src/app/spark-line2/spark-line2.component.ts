import { Component, OnInit,Input,SimpleChanges } from '@angular/core';
import * as Highcharts from 'highcharts';
import { Options } from "highcharts";
import { AppComponent } from '../app.component';
import { ActivatedRoute,Router } from '@angular/router';

@Component({
  selector: 'app-spark-line2',
  templateUrl: './spark-line2.component.html',
  styleUrls: ['./spark-line2.component.scss'],
})
export class SparkLine2Component implements OnInit {
  constructor( private route: ActivatedRoute,private router: Router,public myApp: AppComponent) { }
 
  ngOnInit() {}
  ngOnChanges(change: SimpleChanges) {
  var currencysymbols = {'India':'₹','US':'$'}
  var temp=this.route.snapshot.params.country
 var  formater={'India':'hi-IN','US':'en-US'}
    let colorValue={'0':'#002060','1':'black'}[this.colorVal]
    this.chartOptions.series= [{
      type: 'column',
      data: change.data.currentValue,
      color: {
        linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
        stops:
        [
          [0,'#136F63'],
          [0.3,'#408B82'],
          [1,'#F52A4C']
        ]
      }
    }];
    this.updateFlag  = true;
  
    this.chartOptions.yAxis={
      min: parseFloat(this.categories[0]),
        max: this.data[0]['y'],
        startOnTick: false,
        endOnTick: false,
        allowDecimals: false,
        tickPositions: [parseFloat(this.categories[0]), this.data[0]['y']],
        gridLineWidth: 0,
        title: {
          text: null
        },
        lineColor: 'transparent', tickLength: 0,
        labels: {
          enabled: false
        },
        plotLines: [{
          color: colorValue,
          width: 2,
          value: this.plot_line[0],
          zIndex: 10,
          label: {
            rotation: 0,
            text: currencysymbols[this.route.snapshot.params.country]+parseFloat(this.plot_line[0]).toLocaleString(formater[this.route.snapshot.params.country], { maximumFractionDigits: 0 }),
            verticalAlign: 'top',
            textAlign: 'center',
            // rotation: 0,
            y: -1,
            style: {
              fontWeight: 'bold',
              fontSize: '10px',
              color:colorValue,
              fontFamily:'verdana'
            }
          }
        },
        {
          color: colorValue,
          width: 0,
          value: this.data[0]['y'],
          label: {
            rotation: 0,
            text: currencysymbols[this.route.snapshot.params.country]+parseFloat(this.data[0]['y']).toLocaleString(formater[this.route.snapshot.params.country], { maximumFractionDigits: 0 }),
            x: 5,
            y: 12,
            style: {
              fontWeight: 'bold',
              fontSize: '10px',
              color:colorValue,
              fontFamily:'verdana'

            }
          }
        }
        ]
      }
      this.chartOptions.xAxis={
        categories: this.categories,
        lineColor: 'transparent', tickLength: 0,
        offset: -10,
        labels: {
          formatter: function () {
              return currencysymbols[temp]+(this.value ).toLocaleString(formater[temp], { maximumFractionDigits: 0 })
          },
           style: {
            width: 180,
            paddingLeft: 0,
            fontWeight: 'bold',
            fontSize: '10px',
            color: colorValue,
            fontFamily:'verdana'

          },
      },
       
      }
  }
  Highcharts: typeof Highcharts = Highcharts;
  @Input() data: any[];
  @Input() maxValue: number;
  @Input() minValue: number;
  @Input() categories: any[];
  @Input() plot_line: any[];
  @Input() colorVal: number;
  updateFlag  = false;
  chartOptions: Options = {
    chart: {
      height: 40,
      width: 250,
      type: 'bar',
      inverted: true,
      marginLeft: 60,
      marginRight: 60,
      backgroundColor: "transparent"
    },
    title: {
      text: null
    },
    credits: {
      enabled: false
    },
    exporting: {
      buttons: {
        contextButton: {
          enabled: false
        },
      }
    },
    xAxis: {
    },
  yAxis: {
},
    legend: {
      enabled: false
    },
    tooltip: {
      enabled: false,
    },
    plotOptions: {
      bar: {
        dataLabels: {
          enabled: false,
        }
      }
    },
  series:[]
  };
}
