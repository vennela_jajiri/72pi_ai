import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SparkLine2Component } from './spark-line2.component';

describe('SparkLine2Component', () => {
  let component: SparkLine2Component;
  let fixture: ComponentFixture<SparkLine2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SparkLine2Component ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SparkLine2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
