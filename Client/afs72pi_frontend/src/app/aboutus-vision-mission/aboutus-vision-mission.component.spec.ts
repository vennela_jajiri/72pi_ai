import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AboutusVisionMissionComponent } from './aboutus-vision-mission.component';

describe('AboutusVisionMissionComponent', () => {
  let component: AboutusVisionMissionComponent;
  let fixture: ComponentFixture<AboutusVisionMissionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AboutusVisionMissionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AboutusVisionMissionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
