from django.shortcuts import render
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import viewsets, status
from core.models import *
from core.serializers import *
import json
import pandas as pd
from django.db.models import Avg, Max, Min, Sum
from rest_framework.decorators import authentication_classes, permission_classes, action
from django.views.decorators.csrf import csrf_exempt
from rest_framework.decorators import api_view, renderer_classes
from rest_framework.renderers import JSONRenderer, TemplateHTMLRenderer
from datetime import date,timezone
import datetime
from .models import OurIndiaMarketView, OurUSMarketView
import warnings
import datetime as dt
import locale as lc
from babel.numbers import format_currency
import randomcolor
warnings.filterwarnings("ignore")
from .PortfolioReturns import *
from .InvestmentAmountByFactors import *
from .AnalystTargetPrice import *
from .RiskOverviewCalcs import *
from .ReturnsByStockSector import *
from .StyleMeasures import *
from .Personalisation import *
from django.db import connections
from django.conf import settings
from django.core.validators import MaxValueValidator, MinValueValidator
import random            
import re
SERVER = settings.DATABASES['default']['HOST']
PORT = settings.DATABASES['default']['PORT']
DATABASE = settings.DATABASES['default']['NAME']
UID = settings.DATABASES['default']['USER']
PWD = settings.DATABASES['default']['PASSWORD']

from rest_framework.permissions import IsAuthenticated
from rest_framework_jwt.authentication import JSONWebTokenAuthentication


#Model View set for My Portfolio page
class CustomerPortfolioDetailsViewSet(viewsets.ModelViewSet):
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]

    queryset = CustomerPortfolioDetails.objects.all()   #Fetching the data from CustomerPortfolioDetails
    serializer_class = CustomerPortfolioDetailsSerializer #Passing the Customer Portfolio Serializers
    
    #GET, POST, PUT and DELETE calls will be handled by default meth@ods based on primary key
    #PATCH -- partial = True

    #Get the list of portfolio stocks
    def list(self, request, *args, **kwargs):
        country = kwargs['country']
        requiredTableObjects = {'India':[UniversePricesMain,SecurityMaster,100000], 'US':[UsUniversePricesMain,UsSecurityMaster,5000]}
        defaultInvestment = requiredTableObjects[country][2]
        cust_portfolio = pd.DataFrame(list(CustomerPortfolioDetails.objects.filter(customer_name=request.user.username,country=country).values().order_by('-created_at')))
        if not cust_portfolio.empty:
            latest_portfolio = (list(cust_portfolio['portfolio_name'].head(1)))
            fsTickers = list(cust_portfolio['fs_ticker'].unique())
            universe_prices = pd.DataFrame(list(requiredTableObjects[country][0].objects.filter(factset_ticker__in=fsTickers,date = requiredTableObjects[country][0].objects.all().aggregate(Max('date'))['date__max']).values('factset_ticker', 'price')))
            universe_prices.columns = ['factset_ticker', 'current_price']
            cust_portfolio = pd.merge(cust_portfolio,universe_prices, how='inner', left_on='fs_ticker', right_on='factset_ticker')
            cust_portfolio['current_mkt_val'] = cust_portfolio['quantity'] * cust_portfolio['current_price']
            mcap_df = (pd.DataFrame(list(requiredTableObjects[country][1].objects.filter(fs_ticker__in=fsTickers).values('fs_ticker','market_cap_category','security_code'))))
            cust_portfolio = pd.merge(mcap_df,cust_portfolio, how='inner', on='fs_ticker')
            portfolioNames = list(cust_portfolio['portfolio_name'].unique())
            portfolioNames_dict=[{'name':portfolioNames[i]} for i in range(0,len(portfolioNames)) if portfolioNames[i]]
            logos_data = pd.read_sql("select Code 'security_code',IMGURL from Logos where country='"+country+"'",sql_conn)
            cust_portfolio = pd.merge(cust_portfolio,logos_data,on='security_code',how='inner').sort_values(by='current_mkt_val',ascending=False)
            cust_portfolio.fillna(0,inplace=True)
            cust_portfolio =  cust_portfolio.to_dict("records")
            stockPricesCurrent = pd.DataFrame(list(requiredTableObjects[country][0].objects.filter(date=(requiredTableObjects[country][0].objects.all().aggregate(Max('date'))['date__max'])).values('company','factset_ticker','price').order_by('company')))
            stockPricesCurrent.columns =['company', 'factset_ticker', 'new_price']
            stockPricesCurrent['quantity'] = (round(defaultInvestment / stockPricesCurrent['new_price'].apply(np.ceil),0)).astype(int)
            stockPricesCurrent['invest_amnt'] = defaultInvestment
            stockFundamentals = pd.DataFrame(list(requiredTableObjects[country][1].objects.values('fs_name', 'gics', 'fs_ticker','security_code').order_by('fs_name')))
            stockDetails = pd.merge(stockPricesCurrent, stockFundamentals, how='inner', left_on='factset_ticker', right_on='fs_ticker')
            stockDetails['company'] = stockDetails['company']+" ("+stockDetails['security_code']+")"
            stockDetails=stockDetails.replace([np.inf,-np.inf],np.nan)
            stockDetails['gics'].fillna('Other',inplace=True)
            stockDetails.fillna(0,inplace=True)
            return Response({'cust_portfolio':cust_portfolio,'portfolioNames':portfolioNames_dict,'stockDetails':stockDetails.to_dict("records"),'latest_portfolio':latest_portfolio,'username':request.user.username})
        else:
            return Response({'cust_portfolio':[]})
    
    #custom method to delete multiple selected stocks
    @action(methods=['post'], detail=True, permission_classes=[IsAuthenticated],
            authentication_classes = [JSONWebTokenAuthentication],
            url_path='delete_multiple', url_name='delete_multiple')
    def delete_multiple_stocks(self, request, *args, **kwargs):
        try:
            requestBody = json.loads(request.body.decode('utf-8'))
            if requestBody:   #fetch the Id's from the request body
                required_ids = [body['id'] for body in requestBody if body['id'] ] #storing the id's to list
                for id in required_ids:
                    CustomerPortfolioDetails.objects.filter(id=id).delete()  #deleting the stocks based on id
                return Response({'message': 'Stocks deleted successfully!'}, status=status.HTTP_204_NO_CONTENT) #success response
            else:
                return Response({'message': 'Invalid stock details'}, status=status.HTTP_400_BAD_REQUEST) #invlid client request
        except Exception as e:
            print(e)
            return Response({'message': 'Something went wrong'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR) #any exception occurs
    
    #custom method to save new stock
    @action(methods=['post'], detail=True, permission_classes=[IsAuthenticated],
            authentication_classes = [JSONWebTokenAuthentication],
            url_path='add_new', url_name='add_new')
    def add_new_stock(self, request, *args, **kwargs):
        try:
            requestBody = json.loads(request.body.decode('utf-8'))
            portfolio_object = CustomerPortfolioDetails.objects.filter(customer_name=request.user.username, country=requestBody['country'], portfolio_name=requestBody['portfolio_name']).first()
            requestBody['model_portfolio'] = portfolio_object.model_portfolio
            if requestBody:   #fetch the Id's from the request body
                if CustomerPortfolioDetails.objects.filter(customer_name=request.user.username, country=requestBody['country'], portfolio_name=requestBody['portfolio_name'],company_name=requestBody['company_name']).count():
                    stock = CustomerPortfolioDetails.objects.filter(customer_name=request.user.username, country=requestBody['country'], portfolio_name=requestBody['portfolio_name'],company_name=requestBody['company_name']).first()
                    stock.quantity = stock.quantity+requestBody['quantity']
                    stock.save()
                    return Response({'message': 'Stock updated successfully!'}, status=status.HTTP_201_CREATED) #error response
                else:
                    CustomerPortfolioDetails(customer_name=request.user.username,country=requestBody['country'],portfolio_name=requestBody['portfolio_name'],company_name=requestBody['company_name'],
                    sector_name=requestBody['sector_name'], price=requestBody['price'], fs_ticker=requestBody['fs_ticker'], quantity=requestBody['quantity'],
                    portfolio_type=requestBody['portfolio_type']).save()
                    return Response({'message': 'Stock added successfully!'}, status=status.HTTP_201_CREATED) #success response
            else:
                return Response({'message': 'Invalid stock details'}, status=status.HTTP_400_BAD_REQUEST) #invlid client request
        except Exception as e:
            print(e)
            return Response({'message': 'Something went wrong'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR) #any exception occurs

class GetPortfolios(APIView):
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]

    def post(self, request, *args, **kwargs):  
        params = json.loads(request.body.decode('utf-8'))
        country=params['country']      
        conn=pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server}; SERVER='+SERVER+'; PORT='+PORT+'; DATABASE='+DATABASE+';UID='+UID+';PWD='+PWD+';')
        listOfPortfolioNames = pd.read_sql("select distinct([Portfolio Name])  from Customer_Portfolio_Details where [Customer Name]='"+request.user.username+"' and Country='"+country+"'",conn)
        portfolioList=[]
        for i in range(len(listOfPortfolioNames[['Portfolio Name']])):
            portfolioList.append({'portfolio_name':listOfPortfolioNames['Portfolio Name'][i],'portfolio_value':listOfPortfolioNames['Portfolio Name'][i]})
        return Response({'user_portfolios':portfolioList})


# def getUserPortfolios(country):
#     portfolios_data = pd.DataFrame(list(CustomerPortfolioDetails.objects.filter(customer_name=self.request.user.username,country=country).values('portfolio_name','company_name','fs_ticker').order_by('created_at','portfolio_name')))
    
#     user_portfolios = list(portfolios_data['portfolio_name'].unique())        
#     return user_portfolios



class CreatePortfolioSelectingStocks(APIView):
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]

    def post(self,request,*args,**kwargs):
        params = json.loads(request.body.decode('utf-8'))
        country=params['country']
        sector=params['sector']
        customerName = request.user.username
        requiredTableObjects= {'India':['Universe_Prices_Main','Security_Master'," where flag='yes'",100000,'INR','en-IN', UniversePricesMain],'US':['Us_Universe_Prices_Main','Us_Security_Master'," where flag='yes'",5000,'USD','en-US', UsUniversePricesMain]}
        defaultInvestment = requiredTableObjects[country][3]
        conn = pyodbc.connect("DRIVER={ODBC Driver 17 for SQL Server}; SERVER=GHCDC01; PORT=1433; DATABASE=72PI;UID=aishva;PWD=Ghc2021$;")
        sector_list=list(pd.read_sql("""select distinct gics from """ + requiredTableObjects[country][1] + " " + requiredTableObjects[country][2], conn)['gics'])
        sectorlist_dict=[]
        sectorlist_dict.append({'name':'All'})
        for i in range(0,len(sector_list)):
            sectorlist_dict.append({'name':sector_list[i]})
        if sector=="All":
            sec_master_data=pd.read_sql("""select [FS Ticker] as factset_ticker,[FS Name] as company,[Market Cap] as market_cap,Market_Cap_Category
                                as market_cap_category,GICS as gics, [Security Code] as security_code from """ + requiredTableObjects[country][1] + " " + requiredTableObjects[country][2], conn)
        else:
            sec_master_data=pd.read_sql("""select [FS Ticker] as factset_ticker,[FS Name] as company,[Market Cap] as market_cap,Market_Cap_Category
                                as market_cap_category,GICS as gics, [Security Code] as security_code from """ + requiredTableObjects[country][1] + " " + requiredTableObjects[country][2]+" and GICS='"+sector+"'", conn)
        universe_prices = pd.DataFrame(list(requiredTableObjects[country][6].objects.filter(date = requiredTableObjects[country][6].objects.all().aggregate(Max('date'))['date__max']).values('factset_ticker', 'price')))
        stockData= pd.merge(sec_master_data,universe_prices, on='factset_ticker',how='inner')
        stockData.columns = ['FsTicker', 'Company', 'Market_Cap', 'Market_Cap_Category','GICS', 'Security_Code', 'Price']
        stockData['Price'] = stockData['Price'].round(2)
        stockData=stockData[stockData['Price']>0]
        stockData['Quantity']=defaultInvestment / stockData['Price']
        stockData=stockData.fillna(0)
        cust_portfolio=list(pd.read_sql("select distinct([Portfolio Name]) from [Customer_Portfolio_Details] where [Customer Name]='"+customerName+"'",conn)['Portfolio Name'])
        portfolioNames_dict=[{'name':cust_portfolio[i]} for i in range(0,len(cust_portfolio)) if cust_portfolio[i]]
        logos_data = pd.read_sql("select Code 'security_code',IMGURL from Logos where country='"+country+"'",sql_conn)
        stockData = pd.merge(stockData,logos_data,left_on='Security_Code', right_on='security_code',how='inner').sort_values(by=['Market_Cap'], ascending=False)
        conn.close()
        stockData =  stockData.to_dict("records")
        def_amnt = requiredTableObjects[country][3]
        currency = requiredTableObjects[country][4]
        currency_mode = requiredTableObjects[country][5]
        return Response({'stockData':stockData,'sectorlist_dict':sectorlist_dict,'portfolioNames_dict':portfolioNames_dict,'def_amnt':def_amnt,'currency':currency,'currency_mode':currency_mode})


class MarketView(APIView):
    authentication_classes = []
    permission_classes = []

    def post(self,request,*args,**kwargs):
        params = json.loads(request.body.decode('utf-8'))
        country=params['country']

        def plotLinesGeneration(inputdf,chartName,country):
            plotLines = []
            iterateNumcountryWise={'India':{'beer':7,'p_e':7,'vix':5,'nifty500_stocks_above_200ma':5},'US':{'vix':7,'p_e':7,'p_e_ttm':7}}
            iterateNum = iterateNumcountryWise[country]
            properties_dict={'colors':['#002060','#89BBFE','#00A8E8','#F52A4C','#CAE5FF','#FDCA40','#9BC53D'],
                            'lineWidth':[2,1.5,1.5,1.5,1.5,1.5,1.5],
                            'texts':['μ = ',"μ+σ = ","μ-σ = ","μ+2σ = ","μ-2σ = ","μ+3σ = ","μ-3σ = "],
                            'dynamicColumns':["average","number_1sd","field_1sd","number_2sd","field_2sd","number_3sd","field_3sd"]}
            for i in range(0,iterateNum[chartName]):
                if(chartName == 'beer' or chartName == 'nifty500_stocks_above_200ma'):
                    plotLines.append({'color':properties_dict['colors'][i],'dashStyle':'dash','width':properties_dict['lineWidth'][i],'zIndex':200,
                                        'value':(inputdf[properties_dict['dynamicColumns'][i]].values.tolist()[0]),'label':{'text':properties_dict['texts'][i] + str(round(inputdf[properties_dict['dynamicColumns'][i]].values.tolist()[0],1))+"%",'align':'left','y':-5}})
                else:
                    plotLines.append({'color':properties_dict['colors'][i],'dashStyle':'dash','width':properties_dict['lineWidth'][i],'zIndex':200,
                                        'value':inputdf[properties_dict['dynamicColumns'][i]].values.tolist()[0],'label':{'text':properties_dict['texts'][i] + str(round(inputdf[properties_dict['dynamicColumns'][i]].values.tolist()[0],1)),'align':'left','y':-5}})
            return plotLines
        def getData(objectName,dynamicColumn,country):
            countryToolTips = {'India':{'beer':'BEER','p_e':'P/E','vix':'India Vix','nifty500_stocks_above_200ma':'Nifty500_200MA'},
                                'US' : {'vix':'CBOE Volatility Index','p_e':'Schiller PE of S&P 500','p_e_ttm':'Trailing 12M PE of S&P 500'}}
            tooltipName = countryToolTips[country]
            df = pd.DataFrame(list(objectName.objects.all().values())).sort_values(by=['date'], ascending=True)
            df.dropna(inplace=True)
            df['date']=pd.to_datetime(df['date'])
            df1=df[['date',dynamicColumn]]
            df1['date'] = df1['date'].astype(str)
            df_line={"name":tooltipName[dynamicColumn],"data":df1.values.tolist(),'color':'#F79824','lineWidth':2,'zIndex':9}        
            return df,df_line
        tblObjects = {'India':[[BondEquityEarnings,'beer'],[PriceToEarnings,'p_e'],[IndiaVix,'vix'],[Nifty500Ma200,'nifty500_stocks_above_200ma']],
                        'US': [[CboeVix,'vix'],[Snp500SchillerPriceToEarnings,'p_e'],[Snp500TtmPriceToEarnings,'p_e_ttm']]}
        tblObjects1 = {'India':[MarketIndex,MacroData,MarketIndia,IndexPerformance],'US':[UsMarketIndex,UsMacroData,MarketUs,UsIndexPerformance]}
        charts_dict={'India':['beer','p_e','vix','nifty500_stocks_above_200ma'],
                'US':['vix','p_e','p_e_ttm']}
        charts = charts_dict[country]
        marketDataModel,macroDataModel,marketIndexModel,marketPerformanceModel =  tblObjects1[country][0],tblObjects1[country][1],tblObjects1[country][2],tblObjects1[country][3]
        output_df_lines={}
        for i in range (0,len(tblObjects[country])):
            df_data,lines_data = getData(tblObjects[country][i][0],tblObjects[country][i][1],country)
            output_df_lines[tblObjects[country][i][1]] = lines_data
            output_df_lines[tblObjects[country][i][1] + "_plotLines"] = plotLinesGeneration(df_data,tblObjects[country][i][1],country)
        benchmarkIndices = {'India': ['Nifty 50 Index','Nifty Midcap 100 Index','Nifty Smallcap 100 Index'],'US':["S&P 500","Russell 2000","Dow Jones Industrial Average"]}
        indexes=benchmarkIndices[country]
        indices_df = pd.DataFrame(list(marketDataModel.objects.filter(company__in=indexes,date__gte=str('2019-01-01')).values().order_by('date')))
        indices_df = indices_df[['date','company','return_field']]
        indices_df.columns=['Date','company','Return']
        indices_df=pd.pivot(indices_df,index='Date',columns='company',values='Return').reset_index()
        indices_df[indexes[0]] = (indices_df[indexes[0]]+1).cumprod()-1
        indices_df[indexes[1]] = (indices_df[indexes[1]]+1).cumprod()-1
        indices_df[indexes[2]] = (indices_df[indexes[2]]+1).cumprod()-1
        lineColor=['#002060','#89BBFE','#00A8E8','#ACEDFF','#CAE5FF','#FDCA40','#9BC53D']
        indices_lines=[{"name":indexes[0],'lineWidth': 2,'color':'#FDCA40','data':indices_df[['Date',indexes[0]]].values.tolist(),'marker' : {'symbol' : 'circle'}},
            {"name":indexes[1],"lineWidth": 2,'color':'#00A878','data':indices_df[['Date',indexes[1]]].values.tolist(),'marker' : {'symbol' : 'circle'}},
            {"name":indexes[2],"lineWidth": 2,'color':'#9BC53D','data':indices_df[['Date',indexes[2]]].values.tolist(),'marker' : {'symbol' : 'circle'}}]
        macro_df=pd.DataFrame(list(macroDataModel.objects.filter(date__gte=str('2019-01-01')).values().order_by('date')))
        macro_df.columns=['id',"Date","Oil (Dollars per Barrel)","FX Rate","Interest Rate","Vix","Inflation","Gold"]
        macro_df['Date'] = pd.to_datetime(macro_df['Date'])
        macro_df['Date']  = macro_df['Date'].astype(str)
        gold_lines={"name":'Gold','lineWidth': 2,'color':'#FDCA40','data':macro_df[['Date','Gold']].dropna().sort_values(by=['Date']).values.tolist()}
        
        oil_lines={"name":'Oil','lineWidth': 2,'color':'#00A8E8','data':macro_df[['Date','Oil (Dollars per Barrel)']].dropna().sort_values(by=['Date']).values.tolist()}
        fxrate_lines={"name":'Fx Rate','lineWidth': 2,'color':'#136F63','data':macro_df[['Date','FX Rate']].dropna().sort_values(by=['Date']).values.tolist()}
        interest_lines={"name":'Interest Rate','lineWidth': 2,'color':'#F52A4C','data':macro_df[['Date','Interest Rate']].dropna().sort_values(by=['Date']).values.tolist()}
        market_indices_data = pd.DataFrame(list(marketIndexModel.objects.values()))
        del market_indices_data['id']
        del market_indices_data['max_date']
        columnNames = {'India':[" ","BSE Sensex","Nifty 50",'S&P 500','Russell 2000','Oil ($/barrell)','Gold (₹/gm)','USD/INR'],
                        'US':[" ","S&P 500","Russell 2000","Dow Jones 30","BSE Sensex","Nifty 50","Oil ($/barrell)",'Gold ($/oz)','USD/INR']}
        columnNames2 = {'India':[" ",'Oil ($/barrell)','Gold (₹/gm)','USD/INR'],
                        'US':[" ","Oil ($/barrell)",'Gold ($/oz)','USD/INR']}
        market_indices_data.columns=columnNames[country]
        indices_data = market_indices_data.iloc[:, :-3]
        macro_data = market_indices_data[market_indices_data.columns[-3:]]
        macro_data[" "] = market_indices_data[" "]
        macro_data = macro_data[columnNames2[country]]
        macro_data = macro_data.to_dict("records")    
        indices_data = indices_data.to_dict("records")    
        market_index_performance =  pd.DataFrame(list(marketPerformanceModel.objects.values('ticker','period','return_field')))
        market_index_performance.columns=['Ticker','Period','Return']
        market_index_performance = market_index_performance.pivot(columns='Period',values='Return',index='Ticker').reset_index()
        maxPerformanceDate = marketPerformanceModel.objects.all().aggregate(Max('date'))['date__max']
        current_year = date.today().year
        columns=['Ticker']
        for monthnum in range(1,maxPerformanceDate.month+1):
            columns.append(date.today().replace(month=monthnum).strftime("%b") + "-" + str(current_year)[2:])
        columns.extend(['YTD',str(current_year-1),str(current_year-2)]) 
        req_columns = {'India':['NIFTY 50','NIFTY Smallcap 100','NIFTY Midcap 100'],'US':['Dow Jones 30','Russell 2000','S&P 500']}
        market_indices_performance1 = market_index_performance[market_index_performance['Ticker'].isin(req_columns[country])].sort_values(by='Ticker',ascending=False).reset_index()
        market_indices_performance2 = market_index_performance[~market_index_performance['Ticker'].isin(req_columns[country])].sort_values(by='Ticker').reset_index()
        market_indices_performance1 = market_indices_performance1[columns].to_dict(orient='records')
        market_indices_performance2 = market_indices_performance2[columns].to_dict(orient='records')
        sector_pe_pb_data,unique_dates,sec_pe_pb_cols=[],["Sector"],[]
        if country=='India':
            sector_pe_pb_data = pd.DataFrame(list(SectorIndicesPEPBPerformance.objects.values("gics","date",'factor',"value").order_by("date_new")))
            unique_dates.extend(list(sector_pe_pb_data ['date'].unique()))
            sector_pe_pb_data.columns=['Sector_Name','Date','Factor','Value']
            sector_pe_pb_data  = pd.pivot(sector_pe_pb_data,index='Sector_Name',columns=['Date',"Factor"],values='Value').reset_index()
            sector_pe_pb_data.columns = [col[0]+"_"+col[1] if col[0]!='Sector_Name' else col[0]+col[1] for col in list(sector_pe_pb_data.columns)]
            sector_pe_pb_data.fillna("",inplace=True)
            sec_pe_pb_cols.extend(list(sector_pe_pb_data.columns))
            sector_pe_pb_data=sector_pe_pb_data.to_dict(orient="records")

        #Slider Code
        sliderTableObjects = {'India':[GainersLosers,MarketByte,AdvancesDeclines,NiftyIndexesCorelation,Screening,Indexconstituents],
                                'US':[UsGainersLosers,UsMarketByte,UsAdvancesDeclines,UsIndicesCorrelation,UsScreening,UsIndexconstituents]}
        dynamicColumn = "nsesymbol" if country=='India' else 'factset_ticker'
        gainLosersObject,marketByteObject,advanceDeclinesObject,correlationObject,screeningObject,indexObject = sliderTableObjects[country][0],sliderTableObjects[country][1],sliderTableObjects[country][2],sliderTableObjects[country][3],sliderTableObjects[country][4],sliderTableObjects[country][5]
        gainersLosersData =  pd.DataFrame(list(gainLosersObject.objects.values(dynamicColumn,'type','companytype','price','return_field','date')))
        gainersLosersData.columns = ['Company','Type','Company_Type','Price','Return','Date']
        asofDate = list(gainersLosersData['Date'].unique())[0]
        gainers = gainersLosersData[gainersLosersData['Type']=='Top 5 Gainers'][['Company','Price','Return']].reset_index(drop=True)
        losers = gainersLosersData[gainersLosersData['Type']=='Top 5 Losers'][['Company','Price','Return']].reset_index(drop=True)
        gainers.columns=['Company','Price','Gain']
        losers.columns=['Company','Price','Loss']
        index_gain_losers = gainersLosersData[gainersLosersData['Company_Type']=='No'].sort_values(by='Return',ascending=False).reset_index(drop=True)    
        market_byte_df= pd.DataFrame(list(marketByteObject.objects.all().values('country','ticker','dailypercentchange','monthly_price_change_percent','yearly_price_change_percent')))
        advance_declines = pd.DataFrame(list(advanceDeclinesObject.objects.all().values('date','count','type')))
        advance_declines=advance_declines.sort_values(by=['date','type'],ascending=[True,True]).reset_index(drop=True)
        last_10_adv_dec = advance_declines.head(20)
        adv_dec_dates = list(last_10_adv_dec['date'].unique())
        advances = list(last_10_adv_dec.tail(2).head(1)['count'])
        declines = list(last_10_adv_dec.tail(2).tail(1)['count'])
        last_10_adv_dec_date_wise= last_10_adv_dec.groupby('date')['count'].sum().reset_index().rename(columns={'count':'Total_Adv_Dec'})
        last_10_adv_dec = pd.merge(last_10_adv_dec,last_10_adv_dec_date_wise,on='date',how='inner')
        last_10_adv_dec['pct_count'] = round(last_10_adv_dec['count'] / last_10_adv_dec['Total_Adv_Dec'],2)

        dynamiCompanyColumn ='ticker' if country=='India' else 'company'
        correlation_df=pd.DataFrame(list(correlationObject.objects.all().values()))     
        cols = ['Ticker']
        cols.extend(list(correlation_df[dynamiCompanyColumn]))           
        del correlation_df['id']
        del correlation_df['date']
        correlation_df.columns = cols
        correlation_df=correlation_df.set_index("Ticker")
        correlation_categories = correlation_df.columns
        cf_values = {list(correlation_df.columns)[i]:i for i in range (0,len(list(correlation_df.columns)))}
        correlation_df= correlation_df.where(pd.np.triu(pd.np.ones(correlation_df.shape), k=0).astype(bool)).stack().reset_index()
        correlation_df.columns =['Ticker','Against_Ticker','Correlation']
        correlation_df['Ticker_Index'] = [cf_values.get(val)  for val in list(correlation_df['Ticker'])]
        correlation_df['Against_Ticker_Index'] = correlation_df.groupby('Ticker').cumcount()
        correlation_df['Against_Ticker_Index'] = correlation_df.groupby('Ticker')['Against_Ticker_Index'].transform(lambda x: x[::-1])
        correlation_df['Correlation'] = correlation_df['Correlation'].round(2)
        correlation_df['data'] = list(map(list,  correlation_df[['Ticker_Index','Against_Ticker_Index','Correlation']].itertuples(index=False)))
        correlation_output = {'name':'Correlation','borderWidth':1,'data':correlation_df['data'].values.tolist()}

        screening_data=pd.DataFrame(list(screeningObject.objects.values('fs_name','fs_ticker','security_code','market_cap','debt_equity','sales_growth','eps_growth','pe','beta_1y','sales')))        
        screening_data.columns = ['Company','FS_Ticker','Security_Code','Market_Cap','Debt_Equity','Sales_Growth','EPS_Growth','PE','Beta_1Y','Sales']
        screening_data =screening_data[screening_data['Debt_Equity'].notna()]
        screening_data.fillna(0,inplace=True)
        screening_data['Sales_Growth']=(screening_data['Sales_Growth'] * 100).round(2)
        screening_data['EPS_Growth']=(screening_data['EPS_Growth'] * 100).round(2)
        screening_data=screening_data[(screening_data['Debt_Equity']<=0.4) & (screening_data['Sales_Growth']>=5)]
        screening_data['name'] = screening_data['Security_Code']
        screening_data['value'] = screening_data['Sales']    
        benchmark_indices = {'India':['Nifty 500','Company','lname'],'US':['S&P 500','FS_Ticker','fs_ticker']}
        index_data=pd.DataFrame(list(indexObject.objects.filter(flag='yes',index_name=benchmark_indices[country][0]).values(benchmark_indices[country][2],'index_name').distinct()))        
        screening_bubble_data=screening_data[screening_data[benchmark_indices[country][1]].isin(list(index_data[benchmark_indices[country][2]]))].reset_index(drop=True)        
        screening_bubble_data['color']  = [randomcolor.RandomColor().generate()[0] for i in range(len(list(screening_bubble_data['Company'])))]
        gainers = gainers.to_dict(orient='records')
        losers = losers.to_dict(orient='records')
        index_gain_losers = index_gain_losers.to_dict(orient='records')
        market_byte_df = market_byte_df.to_dict(orient='records')
        screening_bubble_data = screening_bubble_data.to_dict(orient='records')
        if country == 'India':
            gics_list_nifty50,mcap_nifty50=MultiLayerHeapmap("select  * from Daily_Stock_Returns","Nifty_50",'NSE_Symbol')
            output_df_lines['gics_list_nifty50']=gics_list_nifty50
            output_df_lines['mcap_nifty50']=mcap_nifty50
        else:
            gics_list_snp500,mcap_snp500=MultiLayerHeapmap("select  * from US_Daily_Stock_Returns","SnP_500",'Ticker')
            gics_list_dow30,mcap_dow30=MultiLayerHeapmap("select  * from US_Daily_Stock_Returns","DOW_30",'Ticker')
            output_df_lines['gics_list_snp500']=gics_list_snp500
            output_df_lines['mcap_snp500']=mcap_snp500
            output_df_lines['gics_list_dow30']=gics_list_dow30
            output_df_lines['mcap_dow30']=mcap_dow30

        last_10_adv_dec = [{'color':'#136F63','name':'Advances','data':list(last_10_adv_dec.loc[last_10_adv_dec['type']=='Advances']['pct_count'])}
                        ,{'color':'#F52A4C','name':'Declines','data':list(last_10_adv_dec.loc[last_10_adv_dec['type']=='Declines']['pct_count'])}]       
        #slider code end

        output_df_lines2={'adv_dec_dates':adv_dec_dates,'indices_lines':indices_lines,'gold_lines':gold_lines,'oil_lines':oil_lines,'fxrate_lines':fxrate_lines,'interest_rate_lines':interest_lines,
                          'indices_data':indices_data,'macro_data':macro_data,'charts':charts,'displayColumns':columnNames[country][:-3],'displayColumns2':columnNames2[country],'displayPerformanceColumns':columns,
                          'market_performance1':market_indices_performance1,'market_performance2':market_indices_performance2,'sector_pe_pb':sector_pe_pb_data,
                          'sector_group_dates':unique_dates,'sector_pe_pb_columns':sec_pe_pb_cols,'gainers':gainers,'losers':losers,'market_byte':market_byte_df,
                          'index_gain_losers':index_gain_losers,'advances':advances,'declines':declines,'last_10_adv_dec':last_10_adv_dec,
                          'correlation_output':correlation_output,'screening_bubble_data':screening_bubble_data,'correlation_categories':correlation_categories,'asofDate':asofDate}
        context = {**output_df_lines, **output_df_lines2}
        return Response(context)



def MultiLayerHeapmap(query,requiredColumn,securityCode):
    sql_conn=pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server}; SERVER='+SERVER+'; PORT='+PORT+'; DATABASE='+DATABASE+';UID='+UID+';PWD='+PWD+';')
    daily_stock_returns = pd.read_sql(query,sql_conn)
    sql_conn.close()
    daily_stock_returns=daily_stock_returns.fillna(0) 
    daily_stock_returns['DailyPercentChange']=daily_stock_returns['DailyPercentChange']*100
    daily_stock_returns  = daily_stock_returns[[securityCode,'DailyPercentChange','GICS_Sector','Gics_SubIndustry_Name','Market Cap',requiredColumn]]
    daily_stock_returns=daily_stock_returns[daily_stock_returns[requiredColumn]=='Yes']
    data = daily_stock_returns.groupby(['GICS_Sector','Gics_SubIndustry_Name',securityCode])['Market Cap','DailyPercentChange'].sum().reset_index()
    gics_sub_group = daily_stock_returns.groupby(['GICS_Sector','Gics_SubIndustry_Name'])['Market Cap','DailyPercentChange'].sum().reset_index()
    gics_main_group  = daily_stock_returns.groupby(['GICS_Sector'])['Market Cap','DailyPercentChange'].sum().reset_index()
    Final_output = data.copy()    
    gics_sub_group.columns=['GICS_Sector', 'Gics_SubIndustry_Name', 'SUB_GICS_size', 'SUB_GICS_FrequencyPercentChange']
    gics_main_group.columns=['GICS_Sector', 'GICS_size', 'GICS_FrequencyPercentChange']
    Final_output = Final_output.merge(gics_sub_group,on=['GICS_Sector', 'Gics_SubIndustry_Name'],how='inner')
    Final_output = Final_output.merge(gics_main_group,on=['GICS_Sector'],how='inner')
    final_dict_list=[]
    gics = Final_output['GICS_Sector'].unique().tolist()
    gics_list = []
    mcap=0
    colorRange=[-3,-2,-1,0,1,2,3]
    for main_gics in gics:
        i=0
        gics_wise = Final_output[Final_output['GICS_Sector']==main_gics]
        gics_wise.reset_index(drop=True,inplace=True)
        sub_gics = gics_wise['Gics_SubIndustry_Name'].unique().tolist()
        sub_gics_list=[]
        for each_gics in sub_gics:
            j=0
            sub_gics_wise = gics_wise[gics_wise['Gics_SubIndustry_Name']==each_gics]
            sub_gics_wise.reset_index(drop=True,inplace=True)
            sub_gics_companies_list=[]
            for k in range(0,len(sub_gics_wise)):
                if sub_gics_wise.loc[k,'DailyPercentChange']>=colorRange[6]:
                    color='35764E'
                elif sub_gics_wise.loc[k,'DailyPercentChange']>=colorRange[5]:
                    color='2F9E4F'
                elif sub_gics_wise.loc[k,'DailyPercentChange']>=colorRange[4]:
                    color='30CC5A'
                elif sub_gics_wise.loc[k,'DailyPercentChange']>=colorRange[3]:
                    color='414554'
                elif sub_gics_wise.loc[k,'DailyPercentChange']>=colorRange[2]:
                    color='F63538'
                elif sub_gics_wise.loc[k,'DailyPercentChange']>=colorRange[1]:
                    color='BF4045'
                else:
                    color='8B444E'
                if(sub_gics_wise.loc[k,'DailyPercentChange']>=0):
                    sub_gics_wise_dict={'label':sub_gics_wise.loc[k,securityCode]+"<br/> +"+str(round(sub_gics_wise.loc[k,'DailyPercentChange'],2))+"%",'value':sub_gics_wise.loc[k,'Market Cap'],'svalue':round(sub_gics_wise.loc[k,'DailyPercentChange'],2),'fillcolor':color}
                else:
                    sub_gics_wise_dict={'label':sub_gics_wise.loc[k,securityCode]+"<br/>"+str(round(sub_gics_wise.loc[k,'DailyPercentChange'],2))+"%",'value':sub_gics_wise.loc[k,'Market Cap'],'svalue':round(sub_gics_wise.loc[k,'DailyPercentChange'],2),'fillcolor':color}
                sub_gics_companies_list.append(sub_gics_wise_dict)
                mcap=mcap+sub_gics_wise.loc[k,'Market Cap']
                if sub_gics_wise.loc[j,'DailyPercentChange']<=colorRange[0]:
                    color='F63538'
                elif sub_gics_wise.loc[j,'DailyPercentChange']>colorRange[0] and sub_gics_wise.loc[j,'DailyPercentChange']<=colorRange[1]:
                    color='BF4045'
                elif sub_gics_wise.loc[j,'DailyPercentChange']>colorRange[1] and sub_gics_wise.loc[j,'DailyPercentChange']<=colorRange[2]:
                    color='8B444E'
                elif sub_gics_wise.loc[j,'DailyPercentChange']>colorRange[2] and sub_gics_wise.loc[j,'DailyPercentChange']<=colorRange[3]:
                    color='414554'
                elif sub_gics_wise.loc[j,'DailyPercentChange']>colorRange[3] and sub_gics_wise.loc[j,'DailyPercentChange']<=colorRange[4]:
                    color='35764E'
                elif sub_gics_wise.loc[j,'DailyPercentChange']>colorRange[4] and sub_gics_wise.loc[j,'DailyPercentChange']<=colorRange[5]:
                    color='2F9E4F'
                else:
                    color='30CC5A'
            sub_gics_dict = { "fillcolor": color,'label':sub_gics_wise.loc[j,'Gics_SubIndustry_Name'],'value':sub_gics_wise.loc[j,'SUB_GICS_size'],'svalue':sub_gics_wise.loc[j,'SUB_GICS_FrequencyPercentChange']/len(sub_gics_wise),'data':sub_gics_companies_list}
            sub_gics_list.append(sub_gics_dict)
            j=j+1
        gics_dict = { "fillcolor": "20261b",'label':gics_wise.loc[i,'GICS_Sector'],'value':gics_wise.loc[i,'GICS_size'],'svalue':gics_wise.loc[i,'GICS_FrequencyPercentChange'],'data':sub_gics_list}
        gics_list.append(gics_dict)
        i=i+1
    return gics_list,mcap
class ETFMonitorView(APIView):   
    authentication_classes = []
    permission_classes = []
    def post(self, request, *args, **kwargs):
        params = json.loads(request.body.decode('utf-8'))
        country=params['country']
        xaxis=params['xaxis']
        yaxis=params['yaxis']
        etfList=params['etfList']
        default_etf={'India':['NIFTYBEES - Nippon India ETF Nifty Bees ','BANKBEES - Nippon India ETF Bank Bees','PSUBNKBEES - Nippon India ETF PSU Bank Bees','JUNIORBEES - Nippon India ETF Junior BeES',
        'LIQUIDBEES - Nippon India ETF Liquid BeES','GOLDBEES - Nippon India ETF Gold Bees','NETFNIF100 - Nippon India ETF NIFTY 100','NETFDIVOPP - Nippon India ETF Dividend Opportunities',
        'NETFNV20 - Nippon India ETF NV20','MAESGETF - Mirae Asset ESG Sector Leaders ETF','CPSEETF - CPSE ETF*'],'US':['SPY - SPDR S&P 500 ETF Trust','QQQ - Invesco QQQ Trust',
        'IWM - iShares Russell 2000 ETF','MDY - SPDR S&P MIDCAP 400 ETF Trust','IWC - iShares Micro-Cap ETF','TLT - iShares 20+ Year Treasury Bond ETF','AGG - iShares Core U.S. Aggregate Bond ETF',
        'LQD - iShares iBoxx $ Investment Grade Corporate Bond ETF','HYG - iShares iBoxx $ High Yield Corporate Bond ETF','TIP - iShares TIPS Bond ETF','EFA - iShares MSCI EAFE ETF',
        'EEM - iShares MSCI Emerging Markets ETF','GLD - SPDR Gold Shares','SLV - iShares Silver Trust','PLTM - GraniteShares Platinum Trust','PFF - iShares Preferred and Income Securities ETF',
        'EMB - iShares J.P. Morgan USD Emerging Markets Bond ETF','CWB - SPDR Bloomberg Barclays Convertible Securities ETF']}
        if len(etfList)==0:
            etfList = default_etf[country]
        tableObjects= {'India':EtfMonitor,'US':UsEtfMonitor}
        etf_data = pd.DataFrame(list(tableObjects[country].objects.values().order_by('order')))
        marketData = pd.DataFrame(list(UsEtfMonitor.objects.values().order_by('order')))
        marketViewTable=marketData[marketData['market_view_flag']=="Yes"][['type','company_name','security_code','short_name','short_term','intermediate_term']]


        # types = marketViewTable["type"].unique()
        types=['US Equity','International Equity','Fixed Income','Global Real Estate','Precious Metals','Bit Coin']
        output1=[{'type':'US Equity','data':marketViewTable[marketViewTable['type']=='US Equity'].to_dict(orient='records')},{'type':'International Equity','data':marketViewTable[marketViewTable['type']=='International Equity'].to_dict(orient='records')}]
        output2=[{'type':'Fixed Income','data':marketViewTable[marketViewTable['type']=='Fixed Income'].to_dict(orient='records')}]
        output3=[{'type':'Global Real Estate','data':marketViewTable[marketViewTable['type']=='Global Real Estate'].to_dict(orient='records')},{'type':'Precious Metals','data':marketViewTable[marketViewTable['type']=='Precious Metals'].to_dict(orient='records')},
        {'type':'Bit Coin','data':marketViewTable[marketViewTable['type']=='Bit Coin'].to_dict(orient='records')}]
        # output=[]
        # for type in types:
        #     result = {}
        #     result['type']=type
        #     result['data']=marketViewTable[marketViewTable['type']==type].to_dict(orient='records')
        #     output.append(result)
        etf_data['number_50ema_200ema']=etf_data['number_50ema_200ema'].replace(to_replace =["yes","Yes"],  value ="Up Trend") 
        etf_data['number_50ema_200ema']=etf_data['number_50ema_200ema'].replace(to_replace =["no","No"],  value ="Down Trend") 
        etf_data[['category','asset_class','region_country']] = etf_data[['category','asset_class','region_country']].fillna("Other")
        etf_data.fillna(0,inplace=True)
        etf_data['company_name2'] =etf_data['security_code']+" - "+ etf_data['company_name']
        etf_data.sort_values(by=['priority','security_code'],ascending=[False,True],inplace=True)
        etfs = list(etf_data['company_name2'].unique())
        etfChartTable=etf_data[etf_data['company_name2'].isin(etfList)]
        upTrend=sorted(list(etfChartTable[etfChartTable['number_50ema_200ema']=="Up Trend"]['security_code']))
        downTrend=sorted(list(etfChartTable[etfChartTable['number_50ema_200ema']=="Down Trend"]['security_code']))
        etf_chart_data = etfChartTable[['security_code','company_name','category',xaxis,yaxis,'company_name2']].reset_index(drop=True)
        # if xaxis==yaxis:
        #     etf_chart_data.columns =['security_code','company_name','category',xaxis,"_"+yaxis]
        #     yaxis = "_"+yaxis
        etf_chart_data['data'] = etf_chart_data[[xaxis,yaxis]].values.tolist()
        etf_chart_data['data'] = etf_chart_data['data'].apply(lambda x: [x])
        etf_chart_data['groupid'] = etf_chart_data.groupby(['category'], sort=False).ngroup().add(1)
        number_of_colors = etf_chart_data['groupid'].max()
        number_of_colors = 1 if number_of_colors==np.nan else number_of_colors
        colors_list = ["#"+''.join([random.choice('0123456789ABCDEF') for j in range(6)])  for i in range(number_of_colors)]
        color_values = [colors_list[elem-1] for elem in list(etf_chart_data['groupid'])  ]
        etf_chart_data['color'] = color_values
        etf_chart_data = etf_chart_data[['security_code','company_name','category','data','color','company_name2']]
        etf_chart_data.columns=['security_code','company_name','category','data','color','name']
        etf_chart_data.sort_values(by=['security_code'],ascending=[True],inplace=True)
        etf_chart_data =  etf_chart_data.to_dict(orient='records')
        etf_data =  etf_data.to_dict("records")
        return Response({'etf_data':etf_data,'etfs':etfs,'etf_chart_data':etf_chart_data,'etfdefault':etfList,'upTrend':upTrend,'downTrend':downTrend,'etfmarketViewTable1':output1,'etfmarketViewTable2':output2,'etfmarketViewTable3':output3})


class ModelPortfolios(APIView):
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]

    def post(self, request, *args, **kwargs):
        params = json.loads(request.body.decode('utf-8'))
        country=params['country']
        tableObjects={'India':[UniversePricesMain,SecurityMaster,'72PI India Portfolio'],'US':[UsUniversePricesMain,UsSecurityMaster,'72PI US Portfolio']}
        pricesObject = tableObjects[country][0]
        securityMasterObject = tableObjects[country][1]
        modelportfolios = ModelPortfolioDetails.objects.filter(country=country).values('model_portfolio_name').distinct()
        model_portfolio_list = list(modelportfolios)
        model_portfolio_list.append({'model_portfolio_name':tableObjects[country][2]})
        ModelPortfolios = pd.DataFrame(model_portfolio_list)
        ModelPortfolios.columns = ['PortfolioName']
        portfolioCumulativeData = pd.DataFrame(list(ModelPortfoliosCumulative.objects.filter(country=country).values('portfolio_name','dtd','mtd','number_1y','itd','itd_date','description')))
        portfolioCumulativeData.columns=['PortfolioName','DTD','MTD','_1Y','ITD','itd_date','description']
        ModelPortfolios  =  pd.merge(ModelPortfolios,portfolioCumulativeData,on='PortfolioName',how='inner')
        itd_date = list(ModelPortfolios[ModelPortfolios['PortfolioName']==tableObjects[country][2]]['itd_date'])[0]
        itd_return = list(ModelPortfolios[ModelPortfolios['PortfolioName']==tableObjects[country][2]]['ITD'])[0]
        ModelPortfolios =  ModelPortfolios.to_dict(orient='records')
        context={'modelPortfolioTable':ModelPortfolios,'itd_date':str(itd_date),'itd_return':itd_return}
        return Response(context)

class ModelPortfoliosReturnView(APIView):
    authentication_classes = []
    permission_classes = []

    def post(self, request, *args, **kwargs):
        params = json.loads(request.body.decode('utf-8'))
        country=params['country']
        tableObjects={'India':[UniversePricesMain,SecurityMaster,'72PI India Portfolio'],'US':[UsUniversePricesMain,UsSecurityMaster,'72PI US Portfolio']}
        pricesObject = tableObjects[country][0]
        securityMasterObject = tableObjects[country][1]
        modelportfolios = ModelPortfolioDetails.objects.filter(country=country).values('model_portfolio_name').distinct()
        model_portfolio_list = list(modelportfolios)
        model_portfolio_list.append({'model_portfolio_name':tableObjects[country][2]})
        ModelPortfolios = pd.DataFrame(model_portfolio_list)
        ModelPortfolios.columns = ['PortfolioName']
        portfolioCumulativeData = pd.DataFrame(list(ModelPortfoliosCumulative.objects.filter(country=country).values('portfolio_name','dtd','mtd','number_1y','itd','itd_date','description')))
        portfolioCumulativeData.columns=['PortfolioName','DTD','MTD','_1Y','ITD','itd_date','description']
        ModelPortfolios  =  pd.merge(ModelPortfolios,portfolioCumulativeData,on='PortfolioName',how='inner')
        itd_date = list(ModelPortfolios[ModelPortfolios['PortfolioName']==tableObjects[country][2]]['itd_date'])[0]
        itd_return = list(ModelPortfolios[ModelPortfolios['PortfolioName']==tableObjects[country][2]]['ITD'])[0]
        context={'itd_date':str(itd_date),'itd_return':itd_return}
        return Response(context)


        
class ViewModelPortfolioData(APIView):
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]

    def post(self, request, *args, **kwargs):
        params = json.loads(request.body.decode('utf-8'))
        country=params['country']
        model_portfolio_name = params['modelPortfolioName']
        period = params['period']
        onchangevalue = params['onChangeValue']
        context={}
        requiredTableObjects = {'India':[UniversePricesMain,SecurityMaster,MarketIndex,RiskFreeRate,'Universe_Prices_Main','Market_Index'],
                                'US':[UsUniversePricesMain,UsSecurityMaster,UsMarketIndex,UsRiskFreeRate,'Us_Universe_Prices_Main','US_Market_Index']}
        table_headerlist = {'India':['PNL','Nifty 50 Index','Nifty Smallcap 100 Index','Nifty Midcap 100 Index'] ,
                            'US':['PNL','S&P 500','Dow Jones Industrial Average','Russell 2000']}
        universeModel,securityModel,marketIndexModel,riskFreeRateModel = requiredTableObjects[country][0],requiredTableObjects[country][1],requiredTableObjects[country][2],requiredTableObjects[country][3]    
        prices_table,market_table= requiredTableObjects[country][4],requiredTableObjects[country][5]  
        sql_conn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server}; SERVER='+SERVER+'; PORT='+PORT+'; DATABASE='+DATABASE+';UID='+UID+';PWD='+PWD+';')         
        logos_data = pd.read_sql("select Code  Security_Code,IMGURL from  Logos where country='" + country+"'",sql_conn)
        sql_conn.close()
        
        modelportfolios = pd.DataFrame(list(ModelPortfolioDetails.objects.filter(model_portfolio_name=model_portfolio_name,country=country).values('company_name','fs_ticker','model_portfolio_name','quantity').order_by('created_at','company_name')))
        Stock_Data = modelportfolios[['company_name','fs_ticker','quantity']]
        maxPrices = pd.DataFrame(list(universeModel.objects.filter(date = universeModel.objects.all().aggregate(Max('date'))['date__max']).values('company','factset_ticker','price')))    
        maxPrices.columns=['company_name','fs_ticker','price']
        modelportfolios.columns = ['CompanyName','Factset_Ticker','ModelPortfolioName','Quantity']
        modelPortfolioStocks = list(modelportfolios['Factset_Ticker'].unique())
        sec_factors = pd.DataFrame(list(securityModel.objects.filter(fs_ticker__in=modelPortfolioStocks).values('fs_name','fs_ticker','security_code','gics','market_cap_category','market_cap').order_by('fs_name')))
        sec_factors.columns =['CompanyName','Factset_Ticker','Security_Code','Sector','MarketCapCategory','MarketCap']        
        prices = pd.DataFrame(list(universeModel.objects.filter(date=universeModel.objects.all().aggregate(Max('date'))['date__max'],factset_ticker__in=modelPortfolioStocks).values('company','factset_ticker','price').order_by('company')))
        prices.columns = ['CompanyName','Factset_Ticker','Price']
        modelportfolios = pd.merge(modelportfolios,sec_factors,on=["CompanyName",'Factset_Ticker'],how="inner")
        modelportfolios = pd.merge(modelportfolios,prices,on=["CompanyName",'Factset_Ticker'],how="inner")
        modelportfolios = pd.merge(modelportfolios,logos_data,on='Security_Code',how='left')
        
        modelportfolios = modelportfolios.fillna("")
        modelportfolios['Market_Value'] = modelportfolios['Price'] * modelportfolios['Quantity']
        modelportfolios['Exposure']  = modelportfolios['Market_Value']/(modelportfolios['Market_Value'].sum())
        modelportfolios['CompanyName'] = modelportfolios['CompanyName']
        modelportfolios = modelportfolios.sort_values(by='Exposure', ascending=False)

        Stock_Data = pd.merge(Stock_Data,maxPrices,on=['company_name','fs_ticker'],how='inner') 
        Stock_Data = Stock_Data[['company_name','fs_ticker','quantity','price']]        
        Stock_Data.columns = ['Company','Factset_Ticker','Quantity','Price']
        list_of_tickers = list(Stock_Data["Factset_Ticker"])
        current_date= universeModel.objects.all().aggregate(Max('date'))['date__max']
        dates = {'YTD':current_date.replace(day=1,month=1),'MTD':current_date.replace(month=1,day=1),'QTD':current_date.replace(month = (((current_date.month-1)//3)*3)+1,day=1),
                '1M':current_date+timedelta(-30),'3M':current_date+timedelta(-90),'12M':current_date+timedelta(-365),'2019':current_date.replace(day=1,year=2019,month=1)}
        monthly_table_start_date =   current_date.replace(day=1,month=1,year=current_date.year-3)
        monthly_table_prev_start_date = universeModel.objects.filter(date__lt=str(monthly_table_start_date)).all().aggregate(Max('date'))['date__max']
        start_date = dates[period]
        prev_start_date = universeModel.objects.filter(date__lt=str(start_date)).all().aggregate(Max('date'))['date__max']
        list_of_tickers_query = ', '.join('"{0}"'.format(w) for w in [list_of_tickers[i].replace("'","''") for i in range (0,len(list_of_tickers))] ).replace('"',"'")
        if onchangevalue==0:
            sql_conn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server}; SERVER='+SERVER+'; PORT='+PORT+'; DATABASE='+DATABASE+';UID='+UID+';PWD='+PWD+';')         
            total_prices_data = pd.read_sql("select Company,Factset_Ticker,Date,Price,[Return] from " + prices_table + " where date between '" + str(monthly_table_prev_start_date) + "' and '" + str(current_date) + "' and factset_ticker in (" + list_of_tickers_query +")  order by company,date",sql_conn)
            total_index_data = pd.read_sql("select Date,Company,[Return] from " + market_table + " where date between '" + str(monthly_table_prev_start_date) + "' and '" + str(current_date) + "' order by company,date",sql_conn)
            model_portfolios_data = pd.read_sql("select [1Y] [TTM],CAGR,Description from Model_Portfolios_Cumulative where country ='" + country + "' and [Portfolio Name] = '" + model_portfolio_name + "' ",sql_conn)
            sql_conn.close()
            model_portfolios_data = model_portfolios_data.to_dict(orient='records')
            context['ttm_return'] = model_portfolios_data[0]['TTM']
            context['CAGR'] = model_portfolios_data[0]['CAGR']
            context['Description'] = model_portfolios_data[0]['Description']

            
            monthly_cumulative_returns,ror_metrics,monthly_cumulative_chart_data = MonthlyCumulativeReturnsMain (total_prices_data,Stock_Data)
            benchmark_indices = {'India':['Nifty 50 Index','Nifty Smallcap 100 Index','Nifty Midcap 100 Index'],
                                'US':['S&P 500','Russell 2000','Dow Jones Industrial Average']}
            benchmark1,benchmark2,benchmark3=benchmark_indices[country][0],benchmark_indices[country][1],benchmark_indices[country][2]
            benchmark1_monthly,benchmark2_monthly,benchmark3_monthly =  BenchmarkMonthlyCumulativeReturns (total_index_data,benchmark1,benchmark2,benchmark3)
            monthly_cumulative_chart_data['Date'] = monthly_cumulative_chart_data['Date'].astype(str)                    
            context['monthly_cumulative'] = monthly_cumulative_returns.to_dict(orient='records')
            context['ror_metrics'] = ror_metrics.to_dict(orient='records')
            context['monthly_cumulative_chart_data'] = {'data':monthly_cumulative_chart_data.values.tolist()}
            context[benchmark1 + "_monthly"] = {'data':benchmark1_monthly.values.tolist()}
            context[benchmark2 + "_monthly"] = {'data':benchmark2_monthly.values.tolist()}
            context[benchmark3 + "_monthly"] = {'data':benchmark3_monthly.values.tolist()}
            context['benchmarks'] = [benchmark1,benchmark2,benchmark3]
            context['performancetableColumns'] = list(monthly_cumulative_returns.columns)
        else:
            sql_conn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server}; SERVER='+SERVER+'; PORT='+PORT+'; DATABASE='+DATABASE+';UID='+UID+';PWD='+PWD+';')         
            total_prices_data = pd.read_sql("select Company,Factset_Ticker,Date,Price,[Return] from " + prices_table + " where date between '" + str(prev_start_date) + "' and '" + str(current_date) + "' and factset_ticker in (" + list_of_tickers_query +")  order by company,date",sql_conn)
            total_index_data = pd.read_sql("select Date,Company,[Return] from " + market_table + " where date between '" + str(prev_start_date) + "' and '" + str(current_date) + "' order by company,date",sql_conn)
            sql_conn.close()
        prices_data = total_prices_data[total_prices_data['Date']>=prev_start_date]
        market_index_data = total_index_data[total_index_data['Date']>=prev_start_date]
        risk_free_rate_data = pd.DataFrame(list(riskFreeRateModel.objects.filter(date__range=(start_date, current_date)).order_by('date').values('date','risk_free_rate')))
        risk_free_rate_data.columns = ['Date','Risk Free Rate']
        cumulative_returns,daily_returns,metrics_table_data=CalcsMain(country,Stock_Data,prices_data,market_index_data,risk_free_rate_data,model_portfolio_name,period)
        metrics_table_data1 = metrics_table_data.iloc[:8,:]
        metrics_table_data2 = metrics_table_data.iloc[8:, :]
        cumulative_returns['Date'] = cumulative_returns['Date'].astype(str)
        output=pd.DataFrame(columns=['name','lineWidth','color','data'])
        final_dict = {'names':list(cumulative_returns.columns)[1:],'lineWidth':[4,2,2,2,2],
                        'color':['#002060','#00A8E8','#FDCA40','#00A878','#9BC53D']}
        for i in range(0,5):
            output.loc[i]=[final_dict['names'][i],final_dict['lineWidth'][i],final_dict['color'][i],cumulative_returns[['Date',final_dict['names'][i]]].values.tolist()]
        output_cumulative_returns=output.to_dict(orient='records')
        context['tableColumns1'] = list(metrics_table_data1.columns)
        context['tableColumns2'] = list(metrics_table_data2.columns)
        metrics_table_data1 = metrics_table_data1.to_dict(orient='records')
        metrics_table_data2 = metrics_table_data2.to_dict(orient='records')
        context[model_portfolio_name] = modelportfolios.to_dict(orient='records')    
        context["table_data1"] = metrics_table_data1  
        context["table_data2"] = metrics_table_data2
        context['cumulative_returns'] = output_cumulative_returns
        s2 = dt.datetime.now()
        return Response(context)


class ValuationMultiples(APIView):
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]

    def post(self, request, *args, **kwargs):
        params = json.loads(request.body.decode('utf-8'))
        username,country,portfolio_name,factset_ticker = request.user.username,params['country'],params['portfolio_name'],params['factset_ticker']
        portfolios = pd.DataFrame(list(CustomerPortfolioDetails.objects.filter(customer_name=username,country=country).values('portfolio_name')))
        context={}
        if(len(portfolios)>0):

            portfolios_data = pd.DataFrame(list(CustomerPortfolioDetails.objects.filter(customer_name=request.user.username,country=country).values('portfolio_name','company_name','fs_ticker').order_by('created_at','portfolio_name')))
            user_portfolios = list(portfolios_data['portfolio_name'].unique())
            if portfolio_name == '':
                portfolio_name = user_portfolios[0] 
            portfolio_stocks = portfolios_data [portfolios_data['portfolio_name']==portfolio_name].reset_index(drop=True)
            tickers_list = list(portfolio_stocks['fs_ticker'])
            if factset_ticker=="":
                factset_ticker = portfolio_stocks.loc[0,'fs_ticker']
            requiredTableObjects = {'India':[HistoricalMultiples,SecurityMaster],'US':[UsHistoricalMultiples,UsSecurityMaster]}
            historical_model,security_model = requiredTableObjects[country][0], requiredTableObjects[country][1]
            securities = pd.DataFrame(list(security_model.objects.filter(fs_ticker__in=tickers_list).values('fs_name','fs_ticker','security_code')))
            portfolio_stocks = pd.merge(portfolio_stocks,securities[['fs_ticker','security_code']],on='fs_ticker')
            portfolio_stocks['company_name'] = portfolio_stocks['company_name'] + " (" + portfolio_stocks['security_code']  +")"
            securities.columns = ['FS Name', 'FS Ticker','Security_Code']
            historical_mutliples_data = pd.DataFrame(list(historical_model.objects.filter(ticker=factset_ticker).values('date','factor','ticker','value').order_by('date')))        
            historical_mutliples_data.columns = ['Date', 'Factor','Ticker', 'Value']
            historical_mutliples_data.dropna(subset=['Date', 'Factor','Ticker', 'Value'], inplace=True)
            historical_mutliples_data['Date'] = pd.to_datetime(historical_mutliples_data['Date'])
            historical_mutliples_data['Date'] = historical_mutliples_data['Date'].astype(str)
            def getLinesData (inputdf,factor,text,colorName):
                df = inputdf[inputdf['Factor']==factor].sort_values(by='Date').reset_index(drop=True)
                avg_line=0
                if len(df)>0:
                    avg_line = df['Value'].mean()
                data=df[['Date','Value']].values.tolist()
                output_line={"name":text,"data":data,'color':colorName,'lineWidth':1}
                plotLines=[{'color':'#F52A4C','dashStyle':'dash','width':2,'zIndex':20,'value':avg_line,'label':{'text':'Average','align':'left','y':-5}}]
                return output_line,plotLines

            pe_line,pe_avg_line = getLinesData(historical_mutliples_data,'PE',"Price-to-Earnings (PE)","#F79824")
            pb_line,pb_avg_line = getLinesData(historical_mutliples_data,'PB',"Price-to-Book (PB)","#00A8E8")
            debt_equity_line,de_avg_line = getLinesData(historical_mutliples_data,'DE',"Debt-to-Equity (DE)","#9BC53D")
            roe_line,roe_avg_line = getLinesData(historical_mutliples_data,'ROE',"Return-on-Equity (ROE)","#136F63")
            context={'pe_line':pe_line,'pb_line':pb_line,'debt_equity_line':debt_equity_line,'roe_line':roe_line,'pe_avg':pe_avg_line,
            'pb_avg':pb_avg_line,'de_avg':de_avg_line,'roe_avg':roe_avg_line,'user_portfolios':user_portfolios,'portfolio_stocks':portfolio_stocks.to_dict("records"),'status':1}
        else:
            context['status']=0
        return Response(context)        

class FactorAnalysis(APIView):
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]

    def post(self, request, *args, **kwargs):
        params = json.loads(request.body.decode('utf-8'))
        country = params['country']
        portfolio_name = params['portfolio_name']
        tableObjects = {'India':['Universe_Prices_Main',SecurityMaster,UniversePricesMain],'US':['Us_Universe_Prices_Main',UsSecurityMaster,UsUniversePricesMain]}
        universeModel = tableObjects[country][2]
        security_model =  tableObjects[country][1]
        sql_conn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server}; SERVER='+SERVER+'; PORT='+PORT+'; DATABASE='+DATABASE+';UID='+UID+';PWD='+PWD+';') 
        portfolios = pd.DataFrame(list(CustomerPortfolioDetails.objects.filter(customer_name=request.user.username,country=country).values('portfolio_name')))
        context={}   
        if(len(portfolios)>0):
            cust_portfolio = pd.DataFrame(list(CustomerPortfolioDetails.objects.filter(customer_name=request.user.username,country=country,portfolio_name=portfolio_name).values()))
            myStocks = list(cust_portfolio['fs_ticker'].unique())
            stocks_data = cust_portfolio[['company_name','fs_ticker','quantity']]
            stocks_data.columns = ['Company','Factset_Ticker','Quantity']
            factorinfoDF = pd.DataFrame(list(security_model.objects.filter(fs_ticker__in=myStocks).values()))
            factorinfoDF.fillna(0,inplace=True) 

            logos_data = pd.read_sql("select Code  security_code,IMGURL from  Logos where country='" + country+"'",sql_conn)
            factorinfoDF = pd.merge(factorinfoDF,logos_data,on='security_code',how='left')
            factorinfoDF.sort_values(by=['market_cap'],ascending=[False],inplace=True)
            current_date =  date.today()
            ly_start_date = current_date.replace(day=1,month=1,year=current_date.year-1)
            prev_ly_start_date = universeModel.objects.filter(date__lt=str(ly_start_date)).all().aggregate(Max('date'))['date__max']
            list_of_tickers_query = ', '.join('"{0}"'.format(w) for w in [myStocks[i].replace("'","''") for i in range (0,len(myStocks))] ).replace('"',"'")
            prices_data = pd.read_sql("select Company,Factset_Ticker,Date,Price,[Return] from "  + tableObjects[country][0] + " where date between '" + str(prev_ly_start_date) + "' and '" + str(current_date) + "' and factset_ticker in (" + list_of_tickers_query +")  order by company,date",sql_conn)
            constraints_data = pd.read_sql("Select Factor,Low,High from Factors_Constraints",sql_conn)
            sql_conn.close()
            factors_data = factorinfoDF[['fs_name','fs_ticker','beta_1y','volatility_1y','idiosyncratic_vol', 'pe', 'pb', 'div_yield', 'div_payout', 'sales_growth','eps_growth', 'roe', 'roce', 'net_debt_ebitda', 'debt_equity',  'piotroski_score']]
            factors_data.columns = ['Company','Factset_Ticker','Beta_1Y','Volatility_1Y','Idiosyncratic_Vol','PE','PB','Div_Yield','Div_Payout','Sales_Growth','EPS_Growth','ROE' ,'ROCE','Net_Debt/EBITDA','Debt/Equity','Piotroski_Score']
            investment_factors_data = investementAmountFactors(prices_data,factors_data,constraints_data,stocks_data,'view')
            if(len(investment_factors_data)>0):
                investment_factors_data['Factor'] = investment_factors_data.Factor.str.replace('_Bucket', '')
                factors=(investment_factors_data[['Factor']].drop_duplicates('Factor')).values.tolist()
                investment_factors_output=[]
                for i in range(0,len(factors)):
                    temp_df=investment_factors_data[investment_factors_data['Factor']==factors[i][0]]
                    investment_factors_output.append({'Factor':factors[i][0],'data':[temp_df['Exposure'].values.tolist(),temp_df['Return'].values.tolist(),temp_df['count'].values.tolist()]})
            else:
                investment_factors_output=[]
            context={'factorInfo':factorinfoDF.to_dict("records"),'factors': ['Beta_1Y', 'Volatility_1Y', 'PE', 'PB', 'Div_Yield', 'Div_Payout', 'Sales_Growth', 'EPS_Growth', 'ROE', 'ROCE', 'Net_Debt/EBITDA', 'Debt/Equity', 'Piotroski_Score'],
            'output':investment_factors_output,'status':1}
        else:
            context['status']=0
        return Response(context)


class AnalystTargetPrice(APIView):
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]

    def post(self, request, *args, **kwargs):        
        params = json.loads(request.body.decode('utf-8'))
        country = params['country'] 
        portfolio_name = params['portfolio_name']
        context = {}
        portfolios = pd.DataFrame(list(CustomerPortfolioDetails.objects.filter(customer_name=request.user.username,country=country).values('portfolio_name')))   
        if(len(portfolios)>0):
            cust_portfolio = pd.DataFrame(list(CustomerPortfolioDetails.objects.filter(customer_name=request.user.username,country=country,portfolio_name=portfolio_name).values()))
            stocks_data = cust_portfolio[['company_name','fs_ticker','quantity']]
            stocks_data.columns = ['Company','Factset_Ticker','Quantity']
            list_of_tickers = list(stocks_data['Factset_Ticker'])
            list_of_tickers_query = ', '.join('"{0}"'.format(w) for w in [list_of_tickers[i].replace("'","''") for i in range (0,len(list_of_tickers))] ).replace('"',"'")
            tblObjects={'India':['Target_Prices','Universe_Prices_Main',SecurityMaster],'US':['US_Target_Prices','Us_Universe_Prices_Main',UsSecurityMaster]}
            security_model = tblObjects[country][2]
            sql_conn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server}; SERVER='+SERVER+'; PORT='+PORT+'; DATABASE='+DATABASE+';UID='+UID+';PWD='+PWD+';') 
            target_data = pd.read_sql("select Company,Ticker as Factset_Ticker,Current_Price,Avg_Upside,Avg_Downside,[52_Week_High],[52_Week_Low] from " +  tblObjects[country][0] + " where ticker in ( " + list_of_tickers_query+ " )",sql_conn)        
            target_data = analystTargetPricesData(target_data,stocks_data)
            target_data['categories'] = target_data['52_Week_Low'].apply(lambda x: [np.array(x)])
            target_data['plot_line'] = target_data['Current_Price'].apply(lambda x: [np.array(x)])
            target_data['_52_Wk_L_H'] = target_data['52_Week_High'].apply(lambda x: [{'y':x}])
            target_data['Upside_Return'] = target_data['up_return'].apply(lambda x:{'name':'Up Return','data':[x],'color': '#00A8E8', 'dataLabels': {'align': 'right', 'padding': -40, 'style': {'fontWeight': 'normal', 'textOutline': 'false', 'color': 'black','fontSize':'12px'}}})
            target_data['Down_side_Return'] = target_data['down_return'].apply(lambda x:{'name':'Down Return','data':[x],'color': '#CBCBCB', 'dataLabels': {'align': 'left', 'padding': -40, 'style': {'fontWeight': 'normal', 'textOutline': 'false', 'color': 'black','fontSize':'12px'}}})
            target_data['Upside_Downside_Potential'] =  target_data[['Upside_Return','Down_side_Return']].apply(lambda x: [np.array(x)], axis=1).apply(lambda x: x[0])        
            up_max = target_data['up_return'].max()
            week_52_max = target_data['52_Week_High'].max()
            week_52_min = target_data['52_Week_Low'].max()
            target_data.rename(columns={'Factset_Ticker':'fs_ticker'},inplace=True)
            securities = pd.DataFrame(list(security_model.objects.filter(fs_ticker__in=list_of_tickers).values('fs_name','fs_ticker','security_code')))        
            target_data = pd.merge(target_data,securities[['fs_ticker','security_code']],on='fs_ticker')
            logos_data = pd.read_sql("select Code  security_code,IMGURL from  Logos where country='" + country+"'",sql_conn)
            target_data = pd.merge(target_data,logos_data,on='security_code',how='left')
            sql_conn.close()
            target_data=(target_data.sort_values(by='Investment_Amount',ascending=False)).reset_index(drop=True)

            output = target_data.to_dict(orient='Records')        
            context={'output':output,'max_value':up_max, 'max_value2': week_52_max, 'min_value2': week_52_min,'status':1}
        else:
            context['status']=0
        return Response(context)


class VolumeVolatilityMA(APIView):
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]

    def post(self, request, *args, **kwargs):        

        params = json.loads(request.body.decode('utf-8'))
        country = params['country']
        portfolio_name = params['portfolio_name']
        user_name = request.user.username      
        required_stock = params['factset_ticker']                 
        global symbol
        symbols={'India':'₹','US':'$'}
        symbol=symbols[country]
        requiredTableObjects={'India':['Universe_Prices_Main','Volume_Data','Moving_Average','Volume_20_Data',UniversePricesMain,SecurityMaster],
                                'US':['US_Universe_Prices_Main','Us_Volume_Data','US_Moving_Average','Us_Volume_20_Data',UsUniversePricesMain,UsSecurityMaster]}
        universe_table,volume_table,movingavg_table,volume_20_table=requiredTableObjects[country][0],requiredTableObjects[country][1],requiredTableObjects[country][2],requiredTableObjects[country][3]            
        universe_model,security_model=requiredTableObjects[country][4],requiredTableObjects[country][5]        
        current_date= universe_model.objects.all().aggregate(Max('date'))['date__max']
        cust_portfolio = pd.DataFrame(list(CustomerPortfolioDetails.objects.filter(customer_name=request.user.username,country=country,portfolio_name=portfolio_name).values()))
        stocks_data = cust_portfolio[['company_name','fs_ticker','quantity']]
        stocks_data.columns = ['Company','Factset_Ticker','Quantity']
        list_of_tickers = list(stocks_data['Factset_Ticker'])
        list_of_tickers_query = ', '.join('"{0}"'.format(w) for w in [list_of_tickers[i].replace("'","''") for i in range (0,len(list_of_tickers))] ).replace('"',"'")        
        securites = pd.DataFrame(list(security_model.objects.filter(fs_ticker__in=list_of_tickers).values('fs_name','fs_ticker','security_code')))
        portfolio_stocks = securites[['fs_name','fs_ticker','security_code']]
        portfolio_stocks['fs_name'] = portfolio_stocks['fs_name'] + " (" + portfolio_stocks['security_code'] +")"
        if required_stock=='':
            required_stock = list(securities['FS Ticker'])[0]        
        start_date = current_date+timedelta(-365)
        sql_conn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server}; SERVER='+SERVER+'; PORT='+PORT+'; DATABASE='+DATABASE+';UID='+UID+';PWD='+PWD+';')         
        universe_prices = pd.read_sql("select date,price from " + universe_table + " where date >= '" + str(start_date) + "' and factset_ticker = '" + required_stock + "'order by date",sql_conn)
        rsi_vol_volatility_data=pd.read_sql("select [date],factor,value from " + volume_table+ " where Ticker='"+required_stock+"' and date >='" + str(start_date) + "' order by date",sql_conn)
        volume_20=pd.read_sql("select * from " + volume_20_table + " where date>='" +str(start_date)+"' and Ticker='" +required_stock+"' order by date",sql_conn)
        moving_avg_df=pd.read_sql("select date,price,ma9,ma20,ma26,ma50,ma100,ma200 from " + movingavg_table + " where factset_ticker = '" + required_stock + " ' and date >= '" + str(start_date) + "' order by date ",sql_conn)
        sql_conn.close()
        rsi_vol_volatility_data.columns = ['Date', 'Factor',required_stock]
        rsi_vol_volatility_data.dropna(subset=['Date', 'Factor',required_stock], inplace=True)
        rsi_vol_volatility_data.sort_values(by=['Date'],ascending=[True],inplace=True)
        rsi_vol_volatility_data['Date'] = rsi_vol_volatility_data['Date'].astype(str)
        universe_prices['date'] = universe_prices['date'].astype(str)
        universe_prices.columns=['Date','Price']
        volumedf=(rsi_vol_volatility_data[rsi_vol_volatility_data['Factor']=='Volume'])[['Date',required_stock]]
        volumedf.columns=['Date','Volume']
        volume_20.sort_values(by='Date',inplace=True)
        volume_20 = volume_20.dropna().reset_index(drop=True)
        volumedf.sort_values(by='Date',inplace=True)
        volumedf = volumedf.dropna().reset_index(drop=True)
        df = pd.merge(universe_prices,volumedf,on='Date',how='inner')
        df.sort_values(inplace=True,by='Date')
        df['Date'] = df['Date'].astype(str)
        volume_data=[df[['Date','Volume']].values.tolist()]
        max_vol=df[['Volume']].dropna()
        max_vol=int(max_vol['Volume'].max())
        rsi_df=(rsi_vol_volatility_data[rsi_vol_volatility_data['Factor']=='RSI'])[['Date',required_stock]].reset_index(drop=True)
        rsi_df.columns=['Date','RSI']
        rsi_df.dropna(inplace=True)
        volatility_df=(rsi_vol_volatility_data[rsi_vol_volatility_data['Factor']=='Volatility'])[['Date',required_stock]].reset_index(drop=True)
        volatility_df.columns=['Date','Volatility']
        volatility_df.dropna(inplace=True)
        #Price_Vs_moving_Avg
        moving_avg_df.columns=['Date','Price','MA9','MA20','MA26','MA50','MA100','MA200']
        moving_avg_df['Date'] = moving_avg_df['Date'].astype(str)
        output=pd.DataFrame(columns=['name','lineWidth','color','data'])
        final_dict = {'names':list(moving_avg_df.columns)[1:],'lineWidth':[3,2,2,2,2,2,2],'color':['#6610F2','#28a745','#02BCD4','#ffc107','#dc3545','#5A6268','#23272B']}
        for i in range(0,len(list(moving_avg_df.columns)[1:])):
            output.loc[i]=[final_dict['names'][i],final_dict['lineWidth'][i],final_dict['color'][i],moving_avg_df[['Date',final_dict['names'][i]]].dropna().values.tolist()]
        output_pricevsDMA=output.to_dict(orient='records')
        volume_20['Date'] =volume_20['Date'].astype(str)
        volume_20_list=[volume_20[['Date','Volume_20']].values.tolist()]
        rsi_df['Date'] =rsi_df['Date'].astype(str)
        rsi_list =  [(rsi_df[['Date','RSI']].dropna()).values.tolist()]
        volatility_df['Date'] = volatility_df['Date'].astype(str)
        volatiity_list = [(volatility_df[['Date','Volatility']].dropna()).values.tolist()]      
        context={'max_vol':max_vol,'volume_20_data':volume_20_list,'volume_list':volume_data,'rsi':rsi_list,'volatility':volatiity_list,'output':output_pricevsDMA,'portfolio_stocks':portfolio_stocks}
        return Response(context)

#APIView for our market view
class OurMarketView(APIView):
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request, *args, **kwargs):
        country = kwargs['country']  #selected country from the user
        requiredTableObjects = {'India':[OurIndiaMarketView],'US':[OurUSMarketView]} #choose the required table based on country selected
        data = requiredTableObjects[country][0].objects.live().order_by('-posted_at')[:6]#get market views
        marketViews = [] #declare the empty list
        for d in data:
            temp = {}
            temp['Title'] = d.title #store title in dictionary
            for k in d.body:
                temp['Description'] = str(k) #string representation of description
            marketViews.append(temp) #append the dictionaries to the list
        return Response(marketViews)


class StockInfoView(APIView):
    authentication_classes = []
    permission_classes = []
    def post(self, request, *args, **kwargs):
        params = json.loads(request.body.decode('utf-8'))
        country = params['country']
        stockname = params['stockname']
        stockname = stockname.replace("`","&")
        requiredTable= {'India':[SecurityMaster,UniversePricesMain,MovingAverage,CompanyDescriptions,CompanyAddressDetails2,StockRanking,MarketIndex,'Volume_Data','Volume_20_Data','Screening','Target_Prices'],
                        'US':[UsSecurityMaster,UsUniversePricesMain,UsMovingAverage,UsCompanyDescriptions,UsCompanyAddressDetails2,UsStockRanking,UsMarketIndex,'Us_Volume_Data','Us_Volume_20_Data','US_Screening','US_Target_Prices']}
        ticker_val = stockname[stockname.rfind("(")+1 : stockname.rfind(")")].replace("`","&")
        
        security_model,prices_model,moving_average_model,company_desc_model,company_address_model,stock_ranking_model = requiredTable[country][0],requiredTable[country][1],requiredTable[country][2],requiredTable[country][3],requiredTable[country][4],requiredTable[country][5]
        market_model,volume_table,volume_20_table,screening_table,target_prices_table = requiredTable[country][6], requiredTable[country][7], requiredTable[country][8],requiredTable[country][9],requiredTable[country][10]
        stock = security_model.objects.filter(security_code=ticker_val).first()
        
        stockName,fsTicker = stock.fs_name,stock.fs_ticker
        indices={'India':['Nifty 50 Index','Nifty Midcap 100 Index','Nifty Smallcap 100 Index'],'US':['S&P 500','Dow Jones Industrial Average','Russell 2000']}
        benchmark_indices = indices[country]
        symbols={'India':'₹','US':'$'}
        localeCurrencySymbols = {'India':'en_IN','US':'en_US'}
        symbol = symbols[country]
        currencyFormats ={'India':"in Cr",'US':'M'}
        formats = currencyFormats[country]
        factors = pd.DataFrame(list(security_model.objects.filter(fs_name=stockName,fs_ticker=fsTicker).values()))
        company_details = pd.DataFrame(list(company_address_model.objects.filter(ticker=fsTicker).values()))
        del factors['id']
        extraColumns = {'US':['id','ind_l_name','regdist','regstate','tel1','internet'],'India':['id','chairman','ind_l_name','inc_dt','regdist','regstate','tel1','email','internet']}
        try:            
            company_details = company_details[extraColumns[country]]
            del company_details['id']           
        except:
            company_details = pd.DataFrame(columns=extraColumns[country])
            company_details.fillna("",inplace=True)
            del company_details['id']
            pass
        newColumns = {'India':['chairman','industry','founded','tel1','email','internet','address'],
                        'US':['industry','tel1','internet','address']}
        factors_columns = list(factors.columns)
        factors = pd.concat([factors,company_details],axis=1)
        factors['address'] =  factors['regdist'] + ", " + factors['regstate']
        del factors['regdist']
        del factors['regstate']       
        factors_columns.extend(newColumns[country])
        factors.columns = factors_columns
        company_description_set = company_desc_model.objects.filter(company=stockName,co_code=fsTicker).values('description','shortdescription')
        company_description=''
        if(company_description_set):
            if  company_description_set[0]['description'] is not None:
                company_description='<br /> '.join(company_description_set[0]['description'].splitlines())
            else:
                company_description='<br /> '.join(company_description_set[0]['shortdescription'].splitlines())
        prices_data = pd.DataFrame(list(prices_model.objects.filter(factset_ticker = fsTicker).values ("date",'price',"return_field").order_by("date")))        
        prices_data['date'] = pd.to_datetime(prices_data['date'])
        

        stock_prices = prices_data[['date','price']]
        stock_prices['date'] = stock_prices['date'].astype(str)
        stock_prices=stock_prices.values.tolist()

        stock_prices = {'name':stockName,'lineWidth':2,'data':stock_prices,'color': '#002060'}
        del prices_data['price']
        prices_data.columns = ['Date',stockname + ' Return']
        benchmark_data =  pd.DataFrame(list(market_model.objects.filter(company__in=indices[country]).values ('date','company','return_field').order_by('date')))
        benchmark_data.columns = ['Date','Company','Return']
        benchmark_data['Date'] = pd.to_datetime(benchmark_data['Date'])
        benchmark_data = pd.pivot(benchmark_data,index='Date',columns='Company',values='Return').reset_index()
        cumulative_data = pd.merge(prices_data,benchmark_data,on='Date',how='inner')
        cumulative_data['Date'] = cumulative_data['Date'].astype(str)
        cumulative_data.fillna(0,inplace=True)
        cols = list(cumulative_data.columns)
        cols = cols[0:2]
        cols.extend(indices[country])
        cumulative_data = cumulative_data[cols]
        lineColor=['#136F63','#FDCA40','#9BC53D','#00A878']
        linewidth=[2.5,2,2,2]  
        stock_index_output=[]      
        for i in range (1,len(cols)):
            cumulative_data[cols[i]] =  (1+cumulative_data[cols[i]]).cumprod()-1
            text = 'Return' if i==1 else cols[i]
            stock_index_output.append({"name":text,"lineWidth": linewidth[i-1],'color':lineColor[i-1],'data':cumulative_data[['Date',cols[i]]].values.tolist()})
        sql_conn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server}; SERVER='+SERVER+'; PORT='+PORT+'; DATABASE='+DATABASE+';UID='+UID+';PWD='+PWD+';')         
        rsi_vol_volatility_data=pd.read_sql("select [date],factor,value from " + volume_table+ " where Ticker='"+ fsTicker +"' order by date",sql_conn)
        volume_20=pd.read_sql("select * from " + volume_20_table + " where Ticker='" +fsTicker+"' order by date",sql_conn)      
        technical_factors = pd.read_sql("select MA50,MA200,Price,RSI from " + screening_table + " where [fs ticker]='" + fsTicker + "'",sql_conn)
        week_52_prices = pd.read_sql("select [52_Week_High],[52_Week_Low],Current_Price Price from " + target_prices_table + " where ticker ='" + fsTicker + "'",sql_conn)
        rsi_vol_volatility_data.columns = ['Date', 'Factor',fsTicker]
        rsi_vol_volatility_data.dropna(subset=['Date', 'Factor',fsTicker], inplace=True)
        rsi_vol_volatility_data.sort_values(by=['Date'],ascending=[True],inplace=True)
        rsi_vol_volatility_data['Date'] = rsi_vol_volatility_data['Date'].astype(str)
        volumedf=(rsi_vol_volatility_data[rsi_vol_volatility_data['Factor']=='Volume'])[['Date',fsTicker]]
        volumedf.columns=['Date','Volume']
        volume_20.sort_values(by='Date',inplace=True)
        volume_20 = volume_20.dropna().reset_index(drop=True)
        volumedf.sort_values(by='Date',inplace=True)
        volumedf = volumedf.dropna().reset_index(drop=True)
        rsi_df=(rsi_vol_volatility_data[rsi_vol_volatility_data['Factor']=='RSI'])[['Date',fsTicker]].reset_index(drop=True)
        rsi_df.columns=['Date','RSI']
        rsi_df.dropna(inplace=True)
        volatility_df=(rsi_vol_volatility_data[rsi_vol_volatility_data['Factor']=='Volatility'])[['Date',fsTicker]].reset_index(drop=True)
        volatility_df.columns=['Date','Volatility']
        volatility_df.dropna(inplace=True)
        volumedf['Date'] = volumedf['Date'].astype(str)
        volume_20['Date'] = volume_20['Date'].astype(str)
        rsi_df['Date'] = rsi_df['Date'].astype(str)
        volatility_df['Date'] = volatility_df['Date'].astype(str)

        volume_data=volumedf[['Date','Volume']].values.tolist()
        volume_20_list=volume_20[['Date','Volume_20']].values.tolist()
        rsi_list =  (rsi_df[['Date','RSI']].dropna()).values.tolist()
        volatiity_list = (volatility_df[['Date','Volatility']].dropna()).values.tolist()     
        volatiity_list ={'name':'Volatility','linewidth':2,'color':'#89BBFE ','data':volatiity_list}
        rsi_list ={'name':'RSI','linewidth':2,'color':'#89BBFE ','data':rsi_list}
        volume_list = [{'name':'Volume','linewidth':2,'color':'#136F63','type':'column','data':volume_data},
                        {'name':'20D Volume','linewidth':2,'color':'#002060','type':'spline','data':volume_20_list}]
        moving_avg_df=pd.DataFrame(list(moving_average_model.objects.filter(company=stockName,factset_ticker=fsTicker).values('date','price','ma9','ma20','ma26','ma50','ma100','ma200').order_by("date")))  
        moving_avg_df.columns=['Date','Price','MA9','MA20','MA26','MA50','MA100','MA200']
        moving_avg_df['Date'] = moving_avg_df['Date'].astype(str)
        output=pd.DataFrame(columns=['name','lineWidth','color','data'])  
        final_dict = {'names':list(moving_avg_df.columns)[1:],'lineWidth':[3,2,2,2,2,2,2],'color':['#002060','#00A878','#9BC53D','#CACF85','#00A8E8','#FDCA40','#F79824']}
        for i in range(0,len(list(moving_avg_df.columns)[1:])):
            output.loc[i]=[final_dict['names'][i],final_dict['lineWidth'][i],final_dict['color'][i],moving_avg_df[['Date',final_dict['names'][i]]].dropna().values.tolist()]
        output_pricevsDMA=output.to_dict(orient='records')
        stock_ranking=pd.DataFrame(list(stock_ranking_model.objects.filter(fs_name=stockName,fs_ticker=fsTicker).values('nsesymbol','q_sales','q_piotroski_score','q_roe','q_debt_equity','q_fcf_margin','q_beta','g_eps_growth',
        'g_sales_growth','g_sales_qoq','t_price_vs_ema20','t_price_vs_ema50','t_price_vs_ema200','t_rsi','t_ema50_vs_ema200','t_current_price_vs_52_week_high','quality_score',
        'growth_score','technical_score','stock_rank','price','net_change','return_field')))
        stock_ranking= stock_ranking.fillna(0)
        stock_ranking=stock_ranking.to_dict(orient='records')[0]
        stock_ranking['net_change'] = round(stock_ranking['net_change'],2)
        stock_ranking['price'] = round(stock_ranking['price'],1)
        stock_ranking['return_field'] = round(stock_ranking['return_field']*100,1)
        stock_ranking['stock_rank'] = round(stock_ranking['stock_rank'],1)
        if len(week_52_prices)>0:
            week_52_prices['categories1'] = week_52_prices['52_Week_Low'].apply(lambda x: [np.array(x)])
            week_52_prices['plot_line'] = week_52_prices['Price'].apply(lambda x: [np.array(x)])
            week_52_prices['_52_Wk_L_H'] = week_52_prices['52_Week_High'].apply(lambda x: [{'y':x, 'dataLabels': {'color': 'white'}}])
            week_52_max = week_52_prices['52_Week_High'].max()
            week_52_min = week_52_prices['52_Week_Low'].max()
            week_52_prices = week_52_prices.to_dict(orient='records')[0]
            week_52_prices['week_52_max'] =  week_52_max
            week_52_prices['week_52_min'] =  week_52_min
            stock_ranking['week_52'] = week_52_prices
        #Converting factors df to required format
        def meltDf(df,currencySymbol="",formatSymbol=""):
            df = pd.melt(df)
            df.columns = ['Factor','Value']
            if currencySymbol!="":
                df['Value'] = currencySymbol + df['Value'].astype(str).replace('\.0*', '', regex=True) + formatSymbol
            else:
                df['Value'] =   (df['Value'].astype(str)+ formatSymbol).replace("None%","N/A").replace("None","N/A")
            return df.to_dict(orient='records')
        key_factor_columns = {'India':{'cols':['gics','industry','market_cap_category','founded','chairman','address','tel1','email','internet'],
                                        'names':['Sector Name','Industry Name','Market Cap Category','Founded','Chairman','Head Quarters','Phone Number','Email','Website']},
                                'US':{'cols':['gics','industry','market_cap_category','tel1','internet','address'],
                                        'names':['Sector Name','Industry Name','Market Cap Category','Phone Number','Website','Head Quarters']}}
        key_factors = factors[key_factor_columns[country]['cols']]
        trail_zeros = re.compile(r'(?:(\.)|(\.\d*?[1-9]\d*?))0+(?=\b|[^0-9])')
        size_columns = {'US':['Mcap (in $)','EV (in $)','Sales (ttm) (in $)'],'India':['Mcap (in Cr)','EV (in Cr)','Sales (ttm) (in Cr)']}
        size_factors = factors[['market_cap','ev','sales_rm_field']]
        size_factors[['market_cap','ev','sales_rm_field']]=size_factors[['market_cap','ev','sales_rm_field']].apply(lambda x:x.fillna(0).apply(lambda x:(format_currency(round(x,0),'',locale=localeCurrencySymbols[country]))))
        size_factors.replace("₹8,14,29,28,227","None",inplace=True)
        size_factors.columns = size_columns[country]               
        size_factors = meltDf(size_factors,symbol)
        valuation_columns = ['Div.Yield (ttm)','EV/EBITDA (ttm)','EV/Sales (ttm)','Price-To-Book (PB) (mrq)','Price-To-Earnings (PE) (ttm)']
        valuation_factors = factors[['div_yield','ev_ebitda','ev_sales','pb','pe']]
        valuation_factors['div_yield'] = valuation_factors['div_yield'] * 100
        valuation_factors = valuation_factors.apply(lambda x:x.fillna(8142928227).round(2))
        valuation_factors.replace(8142928227,"None",inplace=True)
        valuation_factors['div_yield'] = valuation_factors['div_yield'].astype(str) + "%"
        valuation_factors.columns = valuation_columns
        valuation_factors = meltDf(valuation_factors)
        growth_factors_columns = ['Sales Growth (lfy)', 'Sales Growth (qoq)', 'Quarterly Sales Growth (yoy)', 'Sales - 3Yr CAGR', 'EPS Growth (lfy)', 'EPS Growth (qoq)', 'Quarterly EPS Growth (yoy)', 'EPS - 3Yr CAGR']
        growth_factors = factors[['sales_growth','sales_growth_qoq_growth','qtr_sales_growth_yoy','sales_growth_3y_5r_cagr','eps_growth','eps_growth_qoq_growth','qtr_eps_growth_yoy','eps_growth_3y_5r_cagr']]
        growth_factors[['sales_growth','sales_growth_qoq_growth','qtr_sales_growth_yoy','eps_growth','eps_growth_qoq_growth','qtr_eps_growth_yoy']] = growth_factors[['sales_growth','sales_growth_qoq_growth','qtr_sales_growth_yoy','eps_growth','eps_growth_qoq_growth','qtr_eps_growth_yoy']].apply(lambda x:(x*100))
        growth_factors = growth_factors.apply(lambda x:x.fillna(8142928227).round(1))
        growth_factors.replace(8142928227,"None",inplace=True)
        growth_factors.columns = growth_factors_columns
        growth_factors = meltDf(growth_factors,'','%')
        profiability_factor_columns = [ 'ROE (ttm)', 'ROCE (ttm)', 'EBITDA Margin (ttm)', 'Net Income Margin (ttm)', 'FCF Margin (lfy)', 'Sales/Total Assets (ttm)']
        profiability_factors = factors[['roe','roce','ebitda_margin','net_income_margin','fcf_margin','sales_total_assets']]
        profiability_factors['fcf_margin'] =  profiability_factors['fcf_margin'] * 100
        profiability_factors = profiability_factors.apply(lambda x:x.fillna(8142928227).round(1))
        profiability_factors.replace(8142928227,"None",inplace=True)
        profiability_factors.columns = profiability_factor_columns
        profiability_factors = meltDf(profiability_factors,"","%")
        leverage_factors_columns = ['Net Debt/FCF (lfy)', 'Net Debt/EBITDA (lfy)', 'Debt/Equity (mrq)']
        leverage_factors = factors[['net_debt_fcf','net_debt_ebitda','debt_equity']]
        leverage_factors = leverage_factors.apply(lambda x:x.fillna(8142928227).round(2))
        leverage_factors.replace(8142928227,"None",inplace=True)
        leverage_factors.columns = leverage_factors_columns
        leverage_factors = meltDf(leverage_factors)
        risk_factor_columns = ['Beta 1Yr', 'Volatility 1Yr']
        risk_factors = factors[['beta_1y','volatility_1y']]
        risk_factors['volatility_1y'] = (risk_factors['volatility_1y']*100).fillna(8142928227).round(0).astype(str) + "%"
        risk_factors['beta_1y'] =  risk_factors['beta_1y'].fillna(8142928227).round(2)
        risk_factors = risk_factors.replace(8142928227,"None").replace("8142928227%","None")
        risk_factors.columns = risk_factor_columns
        risk_factors = meltDf(risk_factors,"","")
        piotroskiscore_factor_columns = ['Piotroski Score']
        score_factors = factors[['piotroski_score']].apply(lambda x:x.fillna(8142928227))
        score_factors.replace(8142928227,"None",inplace=True)
        score_factors.columns = piotroskiscore_factor_columns
        score_factors = meltDf(score_factors)
        technical_factors['%50 DMA'] = ((technical_factors['MA50'] / technical_factors['Price'])-1)*100
        technical_factors['%200 DMA'] = ((technical_factors['MA200'] / technical_factors['Price'])-1)*100
        technical_factors = technical_factors[['%50 DMA','%200 DMA','RSI']]
        technical_factors.columns = ['%50 DMA','%200 DMA','RSI 14D']
        technical_factors = technical_factors.apply(lambda x:x.fillna(8142928227).round(1))
        technical_factors.replace(8142928227,"None",inplace=True)
        technical_factors[['%50 DMA','%200 DMA']] = technical_factors[['%50 DMA','%200 DMA']].apply(lambda x:x.astype(str) + "%")
        technical_factors = meltDf(technical_factors)
        key_factors.fillna("-",inplace=True)
        # To retrieve the URL for the company name 
        logos_data = pd.read_sql("select IMGURL from  Logos where country='" + country+"' and Code='"+ticker_val+"'",sql_conn)
        img_url = logos_data.values
        if len(img_url)!=0:
            img_url=img_url[0][0]
       
        sql_conn.close()
        context1={'volume_list':volume_list,'rsi':rsi_list,'volatility':volatiity_list}
        context2 = {'img_url':img_url,'symbol':symbol,'security_code':ticker_val,'stockName':stockName,'stock_index':stock_index_output,'key_factors':key_factors.to_dict("records"),'indices':benchmark_indices,
                    'stock_prices':stock_prices,'company_description':company_description,'size_factors':size_factors,'valuation_factors':valuation_factors,
                    'growth_factors':growth_factors,'profitability_factors':profiability_factors,'leverage_factors':leverage_factors,'risk_factors':risk_factors,
                    'score_factors' : score_factors,'technical_factors':technical_factors ,
                    'output_pricevsDMA':output_pricevsDMA,'stock_ranking':stock_ranking,'formats':formats}
        context = dict(**context1,**context2)
        return Response(context)
#APIView for our market view
class StockListSearch(APIView):
    authentication_classes = []
    permission_classes = []

    def post(self, request, *args, **kwargs):
        params = json.loads(request.body.decode('utf-8'))
        country = params['country']
        requiredTableObjects = {'India':[UniversePricesMain,SecurityMaster,'yes','Find Stock details (only NSE traded)'],
                                'US':[UsUniversePricesMain,UsSecurityMaster,'yes','Find Stock Details (S&P 500,  Russell 1000 and Russell 2000 traded stocks)']} #choose the required table based on country selected
        dist_companies=pd.DataFrame(list(requiredTableObjects[country][0].objects.filter(price__isnull=True).values('company').distinct()))
        filters=[]
        if len(dist_companies)>0:
            filters = list(dist_companies['company'])
        companies = pd.DataFrame(list(requiredTableObjects[country][1].objects.exclude(fs_name__in=filters).filter(flag=requiredTableObjects[country][2]).values('fs_name','security_code','fs_ticker').distinct()))
        companies =  companies.sort_values(by='fs_name').reset_index()
        companies['fs_name'] = companies['fs_name'] +" (" + companies['security_code']  +")"
        securities = companies[['fs_name','fs_ticker']].to_dict(orient='records')
        companies= companies.to_dict(orient='records')
        context={'companies':companies,'placeholder':requiredTableObjects[country][3],'securities':securities}
        return Response(context)


#Ourportfolio view
class OurPortfolioView(APIView):
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]
    #on GET type of request
    def get(self, request, *args, **kwargs):
        country = kwargs['country']
        #required tables based on country
        requiredTableObjects = {'India':[UniversePricesMain,SecurityMaster,PositionsInput,TradeIdeasPerformance,TradeSignals],
                                'US':[UsUniversePricesMain,UsSecurityMaster,UsPositionsInput,USTradeIdeasPerformance,USTradeSignals]}
        sql_conn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server}; SERVER='+SERVER+'; PORT='+PORT+'; DATABASE='+DATABASE+';UID='+UID+';PWD='+PWD+';')
        logos_data = pd.read_sql("select Code  security_code,IMGURL from  Logos where country='" + country+"'",sql_conn)
        sql_conn.close()
        tradeSignalsDF = pd.DataFrame(list(requiredTableObjects[country][4].objects.values()))
        tradeideas = requiredTableObjects[country][3].objects.values().order_by('-issue_date')
        tradeIdeasDF = pd.DataFrame(list(tradeideas))
        tradeIdeasDF2 =tradeIdeasDF['issue_date'].groupby(tradeIdeasDF['company_name']).min().reset_index()
        transactionsDF = pd.DataFrame(list(requiredTableObjects[country][2].objects.filter(customer='suresh').values('tradedate','action','price','secdescription').order_by('secdescription')))
        transactionsDF.columns = ['tradedate','action','purchase_price','secdescription']
        tradeStocks = transactionsDF['secdescription'].unique()
        buyDF = transactionsDF[transactionsDF['action'].isin(['BUY','Buy','buy'])]
        sellDF = transactionsDF[transactionsDF['action'].isin(['SELL','Sell','sell'])]
        buyDF.columns =['tradedate','action','buy_price','secdescription']
        sellDF.columns =['tradedate','action','sell_price','secdescription']
        buyDF2 =pd.merge(tradeIdeasDF2,buyDF,how='left',left_on='company_name',right_on='secdescription')
        sellDF2 =pd.merge(tradeIdeasDF2,sellDF,how='left',left_on='company_name',right_on='secdescription')
        pricesDF = pd.DataFrame(list(requiredTableObjects[country][0].objects.filter(date= 
        requiredTableObjects[country][0].objects.all().aggregate(Max('date'))['date__max'],company__in=tradeStocks).values('company','price')))
        gicsDF = pd.DataFrame(list(requiredTableObjects[country][1].objects.filter(fs_name__in=tradeStocks).values('fs_name','fs_ticker','gics','security_code')))
        transactionsDF = pd.merge(transactionsDF,gicsDF, left_on='secdescription',right_on='fs_name',how='inner')
        tradeSignalsDF = pd.merge(tradeSignalsDF, gicsDF, how='inner', left_on='fs_ticker',right_on='fs_ticker')
        buyDF = pd.merge(buyDF, pricesDF, how='left', left_on='secdescription',right_on='company')
        additionDF = pd.merge(buyDF, gicsDF, left_on='secdescription',right_on='fs_name' )
        additionDF = pd.merge(additionDF,logos_data,on='security_code',how='left')
        sellDF = pd.merge(sellDF, pricesDF, how='left', left_on='secdescription',right_on='company')
        exitDF = pd.merge(sellDF, gicsDF, left_on='secdescription',right_on='fs_name' )
        exitDF = pd.merge(exitDF,additionDF,how='inner', on='secdescription')
        exitDF = exitDF[['tradedate_x','action_x','buy_price','sell_price','secdescription','company_x','price_x','fs_name_x','gics_x','security_code_y','IMGURL']]
        exitDF.columns = ['tradedate','action','buy_price','sell_price','secdescription','company','price','fs_name','gics','security_code','IMGURL']
        exitCompanies = list(exitDF['secdescription'].unique())
        activeDF = additionDF[~additionDF['secdescription'].isin(exitCompanies)]
        additionDF = additionDF[['company', 'gics','tradedate','buy_price','price','security_code','IMGURL']]
        additionDF.columns = ['stock_name', 'sector_name','date','buy_price','price','security_code','IMGURL']
        additionDF.sort_values(by=['security_code'],ascending=[True],inplace=True)
        activeDF = activeDF[['secdescription', 'gics','tradedate','buy_price','price','security_code','IMGURL']]
        activeDF.columns = ['stock_name', 'sector_name','date','buy_price','price','security_code','IMGURL']
        activeDF.sort_values(by=['security_code'],ascending=[True],inplace=True)
        exitDF = exitDF[['secdescription','gics','tradedate','buy_price','sell_price','price','security_code','IMGURL']]
        exitDF.columns = ['stock_name', 'sector_name','date','buy_price','sell_price','price','security_code','IMGURL']
        exitDF.sort_values(by=['security_code'],ascending=[True],inplace=True)
        context = {'tradesignals': tradeSignalsDF.to_dict("records"), 'tradeideas': tradeideas,'activeIdeas': activeDF.to_dict("records"),'additionsIdeas':additionDF.to_dict("records"),'exitIdeas':exitDF.to_dict("records")}
        return Response(context)


class RiskOverview(APIView): 
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]

    def post(self, request, *args, **kwargs):
        params = json.loads(request.body.decode('utf-8'))
        portfolio_name,customer_name,country,period= params['portfolio_name'],request.user.username,params['country'],params['period']
        requiredTableObjects={'India':['Universe_Prices_Main','Security_Master',MarketIndex,'Risk_Free_Rate',StocksDailyBeta,UniversePricesMain,RiskMonitor],
                                'US':['Us_Universe_Prices_Main','US_Security_Master',UsMarketIndex,'US_Risk_Free_Rate',UsStocksDailyBeta,UsUniversePricesMain,UsRiskMonitor]}
        universe_table,security_table,marketIndexModel,risk_free_rate_table = requiredTableObjects[country][0] ,requiredTableObjects[country][1] ,requiredTableObjects[country][2] ,requiredTableObjects[country][3]
        betaModel,universeModel,risk_monitor_model=requiredTableObjects[country][4],requiredTableObjects[country][5],requiredTableObjects[country][6]
        benchmarks={'India':'Nifty 50 Index','US':'S&P 500'}
        index_ticker=benchmarks[country]
        current_date= universeModel.objects.all().aggregate(Max('date'))['date__max']
        dates = {'YTD':current_date.replace(day=1,month=1),'MTD':current_date.replace(day=1),'QTD':current_date.replace(month = (((current_date.month-1)//3)*3)+1,day=1),
            '1M':current_date+timedelta(-30),'3M':current_date+timedelta(-90),'1Y':current_date+timedelta(-365),'Max':current_date.replace(day=1,year=2019,month=1),
            '2020':current_date.replace(day=1,year=2020,month=1)}
        var_start_date = current_date + timedelta(-365)
        context = {}
        portfolios = pd.DataFrame(list(CustomerPortfolioDetails.objects.filter(customer_name=request.user.username,country=country).values('portfolio_name')))
        if(len(portfolios)>0):
            start_date = dates[period]
            prev_start_date = universeModel.objects.filter(date__lt=str(start_date)).all().aggregate(Max('date'))['date__max']
        
            Stock_Data = pd.DataFrame(list(CustomerPortfolioDetails.objects.filter(customer_name=customer_name,country=country,portfolio_name=portfolio_name).values('company_name','price','fs_ticker','quantity')))
            Stock_Data.columns=['Company','Price','Factset_Ticker','Quantity']
        
            list_of_tickers = list(Stock_Data["Factset_Ticker"])
            list_of_tickers_query = ', '.join('"{0}"'.format(w) for w in [list_of_tickers[i].replace("'","''") for i in range (0,len(list_of_tickers))] ).replace('"',"'")
            sql_conn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server}; SERVER='+SERVER+'; PORT='+PORT+'; DATABASE='+DATABASE+';UID='+UID+';PWD='+PWD+';')
            sec_code_data = pd.read_sql("select [fs ticker] as [Factset_Ticker],[Security Code] as Security_Code from " +  security_table + " where [fs ticker] in (" + list_of_tickers_query + " )",sql_conn)
            prices_data = pd.read_sql("select Company,Factset_Ticker,Date,Price,[Return] from " + universe_table + " where date between '" + str(prev_start_date) + "' and '" + str(current_date) + "' and factset_ticker in (" + list_of_tickers_query +")  order by company,date",sql_conn)
            var_prices_data = pd.read_sql("select Company,Factset_Ticker,Date,Price,[Return] from " + universe_table + " where date between '" + str(var_start_date) + "' and '" + str(current_date) + "' and factset_ticker in (" + list_of_tickers_query +")  order by company,date",sql_conn)
            logos_data = pd.read_sql("select Code  Security_Code,IMGURL from  Logos where country='" + country+"'",sql_conn)
            sql_conn.close()
            market_index_data = pd.DataFrame(list(marketIndexModel.objects.filter(date__range=(start_date,current_date),company=index_ticker).order_by('date').values('date', 'company', 'return_field')))
            market_index_data.columns = ['Date' , 'Company' , 'Return']
            beta_data=pd.DataFrame(list(betaModel.objects.filter(factset_ticker__in=list_of_tickers,date=(betaModel.objects.all().aggregate(Max('date'))['date__max'])).values('date','factset_ticker','company','beta_1y')))
            beta_data=beta_data[['company','factset_ticker','beta_1y']]
            portfolio_df,Portfolio_VaR_Component,Individual_VaR=riskPortfolioCalcs(Stock_Data,prices_data,var_prices_data,market_index_data,index_ticker,current_date,portfolio_name)
            portfolio_df['VaR$']=Portfolio_VaR_Component['VaR$']
            portfolio_df['VaR%']=Portfolio_VaR_Component['VaR%']
            Individual_VaR.columns=["Date","Company",'Factset_Ticker','VaR$',"Net$","VaR%"]
            stockwise_df = riskStockCalcs(Stock_Data,prices_data,market_index_data,beta_data)
            output_master=pd.merge(stockwise_df,Individual_VaR[['Company','Factset_Ticker','VaR$',"VaR%"]],on=["Company","Factset_Ticker"],how="inner")
            stockwise_df=output_master[["Company","Factset_Ticker",'Annualized_Return', 'Annualized_Volatility', 'Beta','VaR$',"VaR%"]]
            stockwise_df = pd.merge(stockwise_df,sec_code_data,on='Factset_Ticker',how='inner')
            # stockwise_df['Company'] = stockwise_df['Company'] + " (" + stockwise_df['Security_Code'] + ")"
            stockwise_df = pd.merge(stockwise_df,logos_data,on='Security_Code',how='left')
            
            # del stockwise_df['Security_Code']
            del stockwise_df['Factset_Ticker']
            beta_df=(stockwise_df.sort_values(by='Beta',ascending=False)).reset_index(drop=True)
            beta_df.dropna(inplace=True)
            vol_df=(stockwise_df.sort_values(by='Annualized_Volatility',ascending=False)).reset_index(drop=True)
            vol_df.dropna(inplace=True)
            var_df=(stockwise_df.sort_values(by='VaR$',ascending=False)).reset_index(drop=True)
            var_df.dropna(inplace=True)
            max_beta_list=pd.DataFrame(beta_df[['Company','Beta']].head(1).values)
            max_beta_list.columns=['Company','Beta']
            max_beta_list=max_beta_list.to_dict("records")
            max_vol_list=pd.DataFrame(vol_df[['Company','Annualized_Volatility']].head(1).values)
            max_vol_list.columns=['Company','Annualized_Volatility']
            max_vol_list=max_vol_list.to_dict("records")
            max_var_list=pd.DataFrame(var_df[['Company','VaR$']].head(1).values)
            max_var_list.columns=['Company','VaR']
            max_var_list=max_var_list.to_dict("records")
            min_beta_list=pd.DataFrame(beta_df[['Company','Beta']].tail(1).values)
            min_beta_list.columns=['Company','Beta']
            min_beta_list=min_beta_list.to_dict("records")
            min_vol_list=pd.DataFrame(vol_df[['Company','Annualized_Volatility']].tail(1).values)
            min_vol_list.columns=['Company','Annualized_Volatility']
            min_vol_list=min_vol_list.to_dict("records")
            min_var_list=pd.DataFrame(var_df[['Company','VaR$']].tail(1).values)
            min_var_list.columns=['Company','VaR']
            min_var_list=min_var_list.to_dict("records")
            # portfolio_tabledata=portfolio_df.to_dict(orient='records')
            # stockwise_df=pd.concat([stockwise_df,portfolio_df])
            stockwise_df.columns=['Company','Annualized_Return','Annualized_Volatility','Beta','VaR_rm','VaR_Percent','Security_Code','IMGURL']
            stockwise_df.fillna(0,inplace=True)
            stockwise_df.sort_values(by=['Annualized_Return'],ascending=[False],inplace=True)

            stockwise_table=stockwise_df.to_dict(orient='records')
            risk_monitor_data = pd.DataFrame(list(risk_monitor_model.objects.filter(fs_ticker__in = list_of_tickers).values()))
            del risk_monitor_data['id']
            risk_monitor_data.columns = ['Factset_Ticker', 'Company_Name', '_1Y_Cumulative_Return','_1M_Cumulative_Return', '_3M_Cumulative_Return', 'Price','_1M_Excess_Return',
            '_3M_Excess_Return','_1Y_Excess_Return', '52_Week_High', '52_Week_Low','1_Month_High', '1_Month_Low', 'RSI', '_20_EMA','_50_DMA', 'VFX', 'Views']
            risk_monitor_data = pd.merge(risk_monitor_data,sec_code_data,on='Factset_Ticker',how='inner')
            # risk_monitor_data['Company_Name'] = risk_monitor_data['Company_Name'] + " (" + risk_monitor_data['Security_Code'] + ")"
            risk_monitor_data = pd.merge(risk_monitor_data,logos_data,on='Security_Code',how='left')
            risk_monitor_data['categories1'] = risk_monitor_data['52_Week_Low'].apply(lambda x: [np.array(x)])
            risk_monitor_data['plot_line'] = risk_monitor_data['Price'].apply(lambda x: [np.array(x)])
            risk_monitor_data['_52_Wk_L_H'] = risk_monitor_data['52_Week_High'].apply(lambda x: [{'y':x}])
            week_52_max = risk_monitor_data['52_Week_High'].max()
            week_52_min = risk_monitor_data['52_Week_Low'].max()
            risk_monitor_data['categories2'] = risk_monitor_data['1_Month_Low'].apply(lambda x: [np.array(x)])
            risk_monitor_data['_1M_L_H'] = risk_monitor_data['1_Month_High'].apply(lambda x: [{'y':x}])
            month_max = risk_monitor_data['1_Month_High'].max()
            month_min = risk_monitor_data['1_Month_Low'].max()
            risk_monitor_data = risk_monitor_data.fillna("-")
            risk_monitor_table = risk_monitor_data.to_dict(orient='records')
            context['status']=1
            context={'stockwise_table':stockwise_table,'max_beta_list':max_beta_list,'max_vol_list':max_vol_list,'max_var_list':max_var_list,'min_beta_list':min_beta_list,
            'min_vol_list':min_vol_list,'min_var_list':min_var_list,'min_value1':week_52_min,'max_value1':week_52_max,
            'min_value2':month_min,'max_value2':month_max,'risk_monitor_table' : risk_monitor_table,'status':1}
        else:
            context['status']=0

        return Response(context)



class StocksCharts(APIView):
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]

    def post(self, request, *args, **kwargs): 
        params = json.loads(request.body.decode("utf-8"))
        country = params['country']
        list_of_stocks = params['list_of_stocks']    
        etf_benchmarks_list = params['etf_benchmarks_list']
        period = params['period']

        requiredTableObjects ={'India':['Universe_Prices_Main','Security_Master','Volume_Data',UniversePricesMain,SecurityMaster],'US':['Us_Universe_Prices_Main','Us_Security_Master','US_Volume_Data',UsUniversePricesMain,UsSecurityMaster]}   
        universe_table,security_table,volume_table = requiredTableObjects[country][0] ,requiredTableObjects[country][1],requiredTableObjects[country][2]
        universeModel,security_model  = requiredTableObjects[country][3],requiredTableObjects[country][4]
        

        if country=='India':
            total_stocks_df =  pd.DataFrame(list(security_model.objects.filter(flag='yes').values('fs_name','security_code')))
        else:
            total_stocks_df =  pd.DataFrame(list(security_model.objects.exclude(gics='etf').values('fs_name','security_code')))
        
        first_stocks_df=[]
        if(len(list_of_stocks)==0):
            first_stocks_df = total_stocks_df.iloc[0:1,:]
            list_of_stocks.append( first_stocks_df['security_code'][0])
            first_stocks_df =first_stocks_df.to_dict(orient="records")
        total_stocks_df['fs_name'] = total_stocks_df['fs_name']+" ("+total_stocks_df['security_code']+")"

        total_stocks_df = total_stocks_df.to_dict(orient = "records")
       
        etf_list =  pd.DataFrame(list(security_model.objects.filter(gics='etf').values('fs_name','security_code')))
        etf_list =etf_list.to_dict(orient= "records")
        current_date= universeModel.objects.all().aggregate(Max('date'))['date__max']
        dates = {'YTD':current_date.replace(day=1,month=1),'MTD':current_date.replace(day=1),'QTD':current_date.replace(month = (((current_date.month-1)//3)*3)+1,day=1),
            '1M':current_date+timedelta(-30),'3M':current_date+timedelta(-90),'12M':current_date+timedelta(-365),'2019':current_date.replace(day=1,year=2019,month=1),'2020':current_date.replace(day=1,year=2020,month=1),
            'WTD': current_date + timedelta(-7) if current_date.weekday()==0 else current_date+timedelta(-(current_date.weekday()))}
    
        sec_code_data =pd.DataFrame(list(security_model.objects.filter(security_code__in=list_of_stocks).values('fs_ticker','security_code')))
       
        sec_code_data.columns =['Factset_Ticker','Security_Code']
        sec_code_tickers = pd.Series(sec_code_data.Security_Code.values,index=sec_code_data.Factset_Ticker).to_dict()
        list_of_tickers = list(sec_code_data['Factset_Ticker'])
        list_of_tickers_query = ', '.join('"{0}"'.format(w) for w in [list_of_tickers[i].replace("'","''") for i in range (0,len(list_of_tickers))]).replace('"',"'")        
        start_date,end_date = dates[period],current_date        
        sql_conn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server}; SERVER='+SERVER+'; PORT='+PORT+'; DATABASE='+DATABASE+';UID='+UID+';PWD='+PWD+';')         
        prices_data = pd.read_sql("select Factset_Ticker,Date,[Return] from "+requiredTableObjects[country][0]+" where date between '" + str(start_date) + "' and '" + str(current_date) + "' and factset_ticker in (" + list_of_tickers_query +")  order by company,date",sql_conn)
        rsi_vol_volatility_data=pd.read_sql("select [Date],Ticker,Factor,Value from " + volume_table+ " where Ticker in ("+ list_of_tickers_query+") and date >='" + str(start_date) + "' order by date",sql_conn)
        sql_conn.close() 
        prices_data.dropna(inplace=True)
        prices_data =  prices_data.sort_values(by='Date').reset_index()
        prices_data = pd.pivot(prices_data,columns='Factset_Ticker',index='Date',values='Return').reset_index()
        prices_data.fillna(0,inplace=True)
        prices_data['Date'] = prices_data['Date'].astype(str)
        rsi_df=(rsi_vol_volatility_data[rsi_vol_volatility_data['Factor']=='RSI'])[['Date','Ticker','Value']].reset_index(drop=True)
        rsi_df.dropna(inplace=True)
        rsi_df =  rsi_df.sort_values(by='Date').reset_index()
        rsi_df = pd.pivot(rsi_df,columns='Ticker',index='Date',values='Value').reset_index()
        rsi_df.fillna(0,inplace=True)
        rsi_df['Date'] = rsi_df['Date'].astype(str)
        volatility_df=(rsi_vol_volatility_data[rsi_vol_volatility_data['Factor']=='Volatility'])[['Date','Ticker','Value']].reset_index(drop=True)
        volatility_df.dropna(inplace=True)
        volatility_df =  volatility_df.sort_values(by='Date').reset_index()
        volatility_df = pd.pivot(volatility_df,columns='Ticker',index='Date',values='Value').reset_index()
        volatility_df.fillna(0,inplace=True)
        volatility_df['Date'] = volatility_df['Date'].astype(str)
        cols = list(prices_data.columns)  

        cumulative_output,rsi_output ,volatility_output=[] ,[],[]
        for i in range (1,len(cols)):
            prices_data[cols[i]] = (1+prices_data[cols[i]]).cumprod()-1
            cumulative_output.append({"name":sec_code_tickers[cols[i]],"lineWidth": 2,'data':prices_data[['Date',cols[i]]].values.tolist()})
            rsi_output.append({"name":sec_code_tickers[cols[i]],"lineWidth": 2,'data':rsi_df[['Date',cols[i]]].values.tolist()})
            volatility_output.append({"name":sec_code_tickers[cols[i]],"lineWidth": 2,'data':volatility_df[['Date',cols[i]]].values.tolist()})
        return Response({'cumulative_output':cumulative_output,'rsi_output':rsi_output,'volatility_output':volatility_output,'total_stocks':total_stocks_df,
                        'etf_list':etf_list,'first_stocks_df':first_stocks_df})
        # return Response({'h':'hi'})

def getGicsValues(filterCondition):
    output=None
    gics_values = {'Cyclical':['Materials','Finance','Consumer Discretionary','Realty'],
                    'Defensive':['Health Care','Utilities','Consumer Staples'],
                    'Sensitive':['Telecom','Information Technology','Industrials','Energy']}
    if 'Cyclical' in filterCondition:
        output=gics_values['Cyclical']
    elif 'Defensive' in filterCondition:
        output=gics_values['Defensive']
    elif 'Sensitive' in filterCondition:
        output=gics_values['Sensitive']
    else:
        output=[filterCondition]
    return output

def getIndexCompanies(condition,country):
    if country=='India':
        nse_data=pd.DataFrame(list(Indexconstituents.objects.filter(flag='yes').values('lname','index_name').distinct()))
        bse_data=pd.DataFrame(list(IndexconstituentsBse.objects.filter(flag='yes').values('lname','index_name').distinct()))
        data=pd.concat([nse_data,bse_data])
    else:
        data = pd.DataFrame(list(UsIndexconstituents.objects.filter(flag='yes').values('lname','index_name').distinct()))
    data=data[data['index_name'].str.replace("&","")==condition]
    return list(data['lname'].unique())

def FormatScreenerData(inputdata):
    def multiply_by_100(number):
        return round(number*100,1)
    def round_zero(number): 
        return round(number,0)
    def round_two(number): 
        return round(number,1)
    inputdata['Piotroski_Score']=inputdata['Piotroski_Score'].fillna(0).astype(int)
    inputdata['F_O'].fillna("No",inplace=True)          
    inputdata[['Market Cap','Sales','Piotroski_Score','Volume','Volume_20']] = inputdata[['Market Cap','Sales','Piotroski_Score','Volume','Volume_20']].apply(lambda x: x.round(0))
    inputdata[['Beta_1Y','PE','PB','Sales/Total Assets','Net_Debt/EBITDA','Debt/Equity','EV/Sales','EV/EBITDA','Net_Debt/FCF']] = inputdata[['Beta_1Y','PE','PB','Sales/Total Assets','Net_Debt/EBITDA','Debt/Equity','EV/Sales','EV/EBITDA','Net_Debt/FCF']].apply(round_two)
    inputdata[['Div_Yield','Sales_Growth','EPS_Growth','FCF_Margin','RSI_30','RSI_90','MTD','YTD','QTD','1Y','1M','3M','6M']] =inputdata[['Div_Yield','Sales_Growth','EPS_Growth','FCF_Margin','RSI_30','RSI_90','MTD','YTD','QTD','1Y','1M','3M','6M']].apply(multiply_by_100) 
    inputdata[['Volatility_1Y','Sales_Growth_3Y/5R_CAGR','EPS_Growth_3Y/5R_CAGR','ROE','ROCE','RSI']]= inputdata[['Volatility_1Y','Sales_Growth_3Y/5R_CAGR','EPS_Growth_3Y/5R_CAGR','ROE','ROCE','RSI']].apply(round_two)
    inputdata[['Target_Price','Volume_Change','Share Turnover']]=inputdata[['Target_Price','Volume_Change','Share Turnover']].apply(lambda x: x.round(0))
    inputdata['Price'] = inputdata['Price'].round(2)
    inputdata[['MA50','MA200','MA20','MTD','YTD','QTD','1Y','1M','3M','6M','52_Week_High']]=inputdata[['MA50','MA200','MA20','MTD','YTD','QTD','1Y','1M','3M','6M','52_Week_High']].apply(round_zero)
    return inputdata


# def FormatScreenerData(inputdata):
#     def multiply_by_100(number):
#         return round(number*100,2)
#     def round_zero(number): 
#         return round(number,0)
#     def round_two(number): 
#         return round(number,2)
#     inputdata['Piotroski_Score']=inputdata['Piotroski_Score'].fillna(0).astype(int)
#     inputdata['F_O'].fillna("No",inplace=True)          
#     inputdata[['Market Cap','Sales','Piotroski_Score','Volume','Volume_20']] = inputdata[['Market Cap','Sales','Piotroski_Score','Volume','Volume_20']].apply(lambda x: x.round(0))
#     inputdata[['Beta_1Y','PE','PB','Sales/Total Assets','Net_Debt/EBITDA','Debt/Equity','EV/Sales','EV/EBITDA','Net_Debt/FCF']] = inputdata[['Beta_1Y','PE','PB','Sales/Total Assets','Net_Debt/EBITDA','Debt/Equity','EV/Sales','EV/EBITDA','Net_Debt/FCF']].apply(round_two)
#     inputdata[['Div_Yield','Sales_Growth','EPS_Growth','FCF_Margin','RSI_30','RSI_90','MTD','YTD','QTD','1Y','1M','3M','6M']] =inputdata[['Div_Yield','Sales_Growth','EPS_Growth','FCF_Margin','RSI_30','RSI_90','MTD','YTD','QTD','1Y','1M','3M','6M']].apply(multiply_by_100) 
#     inputdata[['Volatility_1Y','Sales_Growth_3Y/5R_CAGR','EPS_Growth_3Y/5R_CAGR','ROE','ROCE','RSI']]= inputdata[['Volatility_1Y','Sales_Growth_3Y/5R_CAGR','EPS_Growth_3Y/5R_CAGR','ROE','ROCE','RSI']].apply(round_two)
#     inputdata[['Target_Price','Volume_Change','Share Turnover']]=inputdata[['Target_Price','Volume_Change','Share Turnover']].apply(lambda x: x.round(0))
#     inputdata['Price'] = inputdata['Price'].round(2)
#     inputdata[['MA50','MA200','MA20','MTD','YTD','QTD','1Y','1M','3M','6M','52_Week_High']]=inputdata[['MA50','MA200','MA20','MTD','YTD','QTD','1Y','1M','3M','6M','52_Week_High']].apply(round_zero)
#     return inputdata

# def FormatScreenerCurrencyData(inputdata,localeSymbol):
#     inputdata['Market Cap']=inputdata['Market Cap'].apply(lambda x:x if math.isnan(float(x)) else(format_currency(x,'',locale=localeSymbol)))
#     inputdata['Price']=inputdata['Price'].apply(lambda x:x if math.isnan(float(x)) else (format_currency(x,'',locale=localeSymbol)))
#     inputdata['Target_Price']=inputdata['Target_Price'].apply(lambda x:x if math.isnan(float(x)) else (format_currency(x,'',locale=localeSymbol)))
#     inputdata['Volume_20']=inputdata['Volume_20'].apply(lambda x:x if math.isnan(float(x)) else (format_currency(x,'',locale=localeSymbol)))
#     inputdata['Sales']=inputdata['Sales'].apply(lambda x:x if math.isnan(float(x)) else (format_currency(x,'',locale=localeSymbol)))
#     inputdata['Volume']=inputdata['Volume'].apply(lambda x:x if math.isnan(float(x)) else (format_currency(x,'',locale=localeSymbol)))
#     return inputdata


class Screener(APIView):
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]

    def post(self, request, *args, **kwargs):
        params = json.loads(request.body.decode("utf-8"))
        country = params['country']
        filterList = params['filterlist']
        tblObjects = {'India':['Screener_Quantitative','Screener_Fundamental','Screener_Technical','Screener_Risk','Screening', 'Universe_Prices_Main'],
                                'US':['US_Screener_Quantitative','US_Screener_Fundamental','US_Screener_Technical','US_Screener_Risk','US_Screening','Us_Universe_Prices_Main']}
        sql_conn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server}; SERVER='+SERVER+'; PORT='+PORT+'; DATABASE='+DATABASE+';UID='+UID+';PWD='+PWD+';') 
        screener_quantitative = pd.read_sql("select * from " + tblObjects[country][0],sql_conn).drop('id',axis=1)
        screener_fundamental = pd.read_sql("select * from " + tblObjects[country][1],sql_conn).drop('id',axis=1)
        screener_technical = pd.read_sql("select * from " + tblObjects[country][2],sql_conn).drop('id',axis=1)
        screener_risk = pd.read_sql("select * from " + tblObjects[country][3],sql_conn).drop('id',axis=1)
        screener_data = pd.read_sql("select * from " + tblObjects[country][4] + " where price>0" ,sql_conn)
        logos_data = pd.read_sql("select Code  security_code,IMGURL from  Logos where country='" + country+"'",sql_conn)
        logos_data.columns = ['Security Code','IMGURL']
        sql_conn.close()
        if country=='India':
            nse_data=pd.DataFrame(list(Indexconstituents.objects.filter(flag='yes').values('lname','index_name').distinct()))
            bse_data=pd.DataFrame(list(IndexconstituentsBse.objects.filter(flag='yes').values('lname','index_name').distinct()))
            data=pd.concat([nse_data,bse_data])
        else:
            data=pd.DataFrame(list(UsIndexconstituents.objects.filter(flag='yes').values('lname','index_name').distinct()))
        data=pd.pivot(data,columns='index_name',values='lname')
        data.columns=[col.replace("&","") for col in data.columns]
        index_output={column:data[column].dropna().unique().tolist() for column in data.columns}
        quantitative_list =  screener_quantitative.apply(lambda x: x, axis=1).to_dict(orient='list')
        quantitative_list ={ elem.replace(" ","_"):[ val for val in quantitative_list[elem] if val is not None] for elem in quantitative_list.keys()}
        quant_index=['Any']
        quant_index.extend(list(index_output.keys()))
        quantitative_list['Quant_Index'] = quant_index
        quantitative_list_keys = list(quantitative_list.keys())
        quant_names = ['Market Cap','Sales (TTM)','Sector','FO Stocks','Current Price','Target Price','Volume','Index']
        fundamental_list =  screener_fundamental.apply(lambda x: x, axis=1).to_dict(orient='list')
        fundamental_list ={'Fund_' +  elem.replace(" ","_"):[ val for val in fundamental_list[elem] if val is not None] for elem in fundamental_list.keys()}
        fundamental_list_keys = list(fundamental_list.keys())
        fundamental_names = ['PE','PB','EV/Sales', 'EV/EBITDA', 'Div Yield', 
                            'Sales Growth', 'Sales Growth 3Y/5R CAGR', 'EPS Growth', 'EPS Growth 3Y/5R CAGR', 'ROE', 
                            'ROCE', 'FCF Margin', 'Sales/Total Assets', 'Net Debt/FCF', 'Net Debt/EBITDA', 'Debt/Equity']
        technical_list =  screener_technical.apply(lambda x: x, axis=1).to_dict(orient='list')
        technical_list ={'Tech_' +  elem.replace(" ","_"):[ val for val in technical_list[elem] if val is not None] for elem in technical_list.keys()}        
        technical_list_keys = list(technical_list.keys())
        technical_names =['Relative Strength 30D','Relative Strength 90D','Avg 10D vs Avg 90D Vol','52 Week High',
                          'DMA 20','DMA 50','DMA 200','RSI (14D)','MTD','QTD','YTD','1M','3M','6M','1Y']

        risk_list =  screener_risk.apply(lambda x: x, axis=1).to_dict(orient='list')
        risk_list ={'Risk_' +  elem.replace(" ","_"):[ val for val in risk_list[elem] if val is not None] for elem in risk_list.keys()}
        risk_list_keys = list(risk_list.keys())
        risk_names = ['Beta', 'Volatility', 'Piotroski Score','Avg 20D Volume','Share Turnover']
        del screener_data['id']
        screener_data = FormatScreenerData(screener_data)
        screener_data = screener_data[screener_data['Price']>0]
        if len(filterList)>0:            
            min_value,max_value,category_value=None,None,None
            greaterthanequal_strings=['Above','above','Over','over','>']
            lessthanequal_strings=['Below','below','Under','under','<','Negative']                                         
            columns_check = {'Quant_Market_Cap':'Market Cap', 'Quant_Sales':'Sales', 'Quant_Sector':'GICS', 'Quant_fo_stocks':'F_O', 'Quant_Price':'Price', 
                            'Quant_Target_Price':'Target_Price','Quant_Volume':'Volume', 'Quant_Index' : 'FS Name',
                            'Fund_PE':'PE', 'Fund_PB':'PB', 'Fund_EV_Sales':'EV/Sales', 'Fund_EV_EBITDA':'EV/EBITDA','Fund_Div_Yield':'Div_Yield','Fund_Sales_Growth':'Sales_Growth',
                            'Fund_Sales_growth_3Y_CAGR':'Sales_Growth_3Y/5R_CAGR','Fund_EPS_Growth':'EPS_Growth', 'Fund_EPS_growth_3Y_CAGR':'EPS_Growth_3Y/5R_CAGR', 'Fund_ROE':'ROE',
                            'Fund_ROCE':'ROCE','Fund_FCF_Margin':'FCF_Margin', 'Fund_Sales_Total_Assets':'Sales/Total Assets', 'Fund_Net_Debt_FCF':'Net_Debt/FCF', 'Fund_Net_Debt_EBITDA':'Net_Debt/EBITDA',
                            'Fund_Debt_Equity':'Debt/Equity','Tech_Relative_Strength_30D':'RSI_30', 
                            'Tech_Relative_Strength_90D':'RSI_90', 'Tech_Relative_Strength_180D':'RSI_180', 'Tech_Relative_Strength_365D':'RSI_365', 'Tech_Volume_Change':'Volume_Change',
                            'Tech_52_Week_High':'52_Week_High', 'Tech_MA50':'MA50', 'Tech_MA200':'MA200', 'Tech_MA20':'MA20','Tech_RSI_(14D)':'RSI', 'Tech_Absolute_Return_MTD' : 'MTD',
                            'Tech_Absolute_Return_YTD':'YTD', 'Tech_Absolute_Return_QTD':'QTD', 'Tech_Absolute_Return_1Y':'1Y', 'Tech_Absolute_Return_1M':'1M', 'Tech_Absolute_Return_3M':'3M', 'Tech_Absolute_Return_6M':'6M',
                            'Risk_Beta':'Beta_1Y', 'Risk_Volatility':'Volatility_1Y', 'Risk_Piotroski_Score':'Piotroski_Score', 'Risk_Average_Daily_Traded_Volume':'Volume_20', 
                            'Risk_Share_Turnover':'Share Turnover'}
            screener_data['F_O'].fillna("No",inplace=True)       
            filterList = [elem for elem in filterList if len(elem)!=0]
            for i in range(0,len(filterList)):        
                key = filterList[i]['factor']
                condition  = filterList[i]['condition'].replace(",","").replace("50DMA","").replace("200DMA","")
                dynamic_column = columns_check[key]
                if (key in ['Quant_Volume','Risk_Average_Daily_Traded_Volume']):
                    condition=condition.replace("lakh",'00000').replace("cr","0000000").replace("10k","10000").replace("100k","100000").replace("500k","500000").replace("1M","1000000").replace("2.5M","2500000").replace("5M","5000000")
                if key == 'Quant_Sector':
                    gicsvalues = getGicsValues(condition)
                    screener_data=screener_data[screener_data[dynamic_column].isin(gicsvalues)]
                elif key == 'Quant_Index':               
                    companies = getIndexCompanies(condition,country)
                    screener_data=screener_data[screener_data[dynamic_column].isin(companies)]
                elif key == 'Quant_fo_stocks':
                    companies = getIndexCompanies(condition,country)
                    screener_data=screener_data[screener_data[dynamic_column]==condition]
                elif key == 'Quant_Market_Cap'  and any(elem in condition for elem in ['Large Cap','Small Cap','Mid Cap']) and country=='India':
                    screener_data=screener_data[screener_data['Market_Cap_Category'].str.contains(condition,na=False)]                
                else:
                    values = re.findall(r'-?\b\d+(?:\.\d+)?(?!\d)', condition)
                    if len(values)>0:
                        values =[float(val) for val in values]                             
                    if len(values)>1:
                        if key in ('Quant_Target_Price','Tech_MA50','Tech_MA200','Tech_MA20'):
                            screener_data=screener_data[(screener_data[dynamic_column]>=screener_data['Price'] + (screener_data['Price'] * (values[0]/100))) & (screener_data[dynamic_column]<=(screener_data['Price'] +screener_data['Price'] * (values[1]/100)))]
                        else:   
                            screener_data=screener_data[(screener_data[dynamic_column]>=values[0]) & (screener_data[dynamic_column]<=values[1])]
                    else:         
                        if any(elem in condition for elem in greaterthanequal_strings):
                            if len(values)>=0 and key in ('Quant_Target_Price','Tech_MA50','Tech_MA200','Tech_MA20'):
                                try:
                                    val=values[0]
                                except:
                                    val = 0                                
                                screener_data=screener_data[(screener_data[dynamic_column]>=screener_data['Price'] + (screener_data['Price'] * (val/100)))]                           
                            elif len(values)>0:
                                screener_data=screener_data[screener_data[dynamic_column]>=values[0]]
                        elif any(elem in condition for elem in lessthanequal_strings):
                        
                            if len(values)>=0 and key in ('Quant_Target_Price','Tech_MA50','Tech_MA200','Tech_MA20'):
                                try:
                                    val=values[0]
                                except:
                                    val = 0
                                if(key == 'Tech_52_Week_High'):
                                    screener_data=screener_data[(screener_data['Price']>=screener_data[dynamic_column] - (screener_data[dynamic_column] * (val/100)))] 
                                else:
                                    screener_data=screener_data[(screener_data[dynamic_column]<screener_data['Price'] - (screener_data['Price'] * (val/100)))]                            
                            elif len(values)>0:
                                screener_data=screener_data[screener_data[dynamic_column]<values[0]]                        
                        elif any(elem in ['None','Zero'] for elem in condition.split(" ")):
                            screener_data=screener_data[screener_data[dynamic_column]==0]
                        else:

                            if key in ('Quant_Target_Price','Tech_MA50','Tech_MA200','Tech_MA20','Tech_52_Week_High'):
                                screener_data=screener_data[(screener_data[dynamic_column]==screener_data['Price'] )]                           
                            # else:
                            #     screener_data=screener_data[screener_data[dynamic_column]==values[0]]
        defaultInvestmentAmount =  {'India':100000,'US':5000}
        localeSymbol  = 'en_IN' if country=='India' else 'en_US'
        screener_data['Quantity']  =np.floor((defaultInvestmentAmount[country]/screener_data['Price'])) 

        # screener_data =  FormatScreenerCurrencyData(screener_data,localeSymbol)
        # screener_data = screener_data.applymap(str)
        # screener_data[['Div_Yield','Sales_Growth','EPS_Growth','FCF_Margin','RSI_30','RSI_90','RSI','MTD','YTD','QTD','1Y','1M','3M','6M','Volatility_1Y','Sales_Growth_3Y/5R_CAGR','EPS_Growth_3Y/5R_CAGR','ROE','ROCE','Volume_Change','Share Turnover']] = screener_data[['Div_Yield','Sales_Growth','EPS_Growth','FCF_Margin','RSI_30','RSI_90','RSI','MTD','YTD','QTD','1Y','1M','3M','6M','Volatility_1Y','Sales_Growth_3Y/5R_CAGR','EPS_Growth_3Y/5R_CAGR','ROE','ROCE','Volume_Change','Share Turnover']]+'%'        
        # screener_data=screener_data.replace("nan","")
        # screener_data=screener_data.replace("nan%","N/A")
        screener_data = screener_data.replace([np.inf, -np.inf], np.nan)
        screener_data = screener_data.fillna(-1234567890123) 

        screener_data['FS Name'] = screener_data['FS Name']
        screener_data = pd.merge(screener_data,logos_data,on='Security Code',how='inner')

        screener_data = screener_data[['Market Cap','Market_Cap_Category', 'Price','Quantity','Sales', 'GICS', 'Beta_1Y', 'Volatility_1Y',
        'Piotroski_Score', 'PE', 'PB', 'EV/Sales', 'EV/EBITDA', 'Div_Yield','Sales_Growth', 'Sales_Growth_3Y/5R_CAGR', 'EPS_Growth',
        'EPS_Growth_3Y/5R_CAGR', 'ROE', 'ROCE', 'FCF_Margin','Sales/Total Assets', 'Net_Debt/FCF', 'Net_Debt/EBITDA', 'Debt/Equity',
        'Volume', 'RSI','MA20', 'MA50', 'MA200', '52_Week_High', 'Target_Price', 'Volume_20', 'RSI_30', 'RSI_90','MTD', 'QTD','YTD',  '1M', '3M', '6M','1Y',
        'Share Turnover', 'Volume_Change',  'F_O','FS Ticker','Security Code','IMGURL','FS Name']]

        if country=="India":
            mcap_name = 'MCap (in Cr)'
        else:
            mcap_name = 'MCap (in M)'
        screener_data.columns = [mcap_name,'Market Cap Category', 'Price','Quantity','Sales (TTM)', 'GICS', 'Beta', 'Volatility',
        'Piotroski Score', 'PE', 'PB', 'EV/Sales', 'EV/EBITDA', 'Div Yield','Sales Growth', 'Sales Growth 3Y/5R CAGR', 'EPS Growth',
        'EPS Growth 3Y/5R CAGR', 'ROE', 'ROCE', 'FCF Margin','Sales/Total Assets', 'Net Debt/FCF', 'Net Debt/EBITDA', 'Debt/Equity',
         'Volume', 'RSI (14D)', 'DMA 20', 'DMA 50', 'DMA 200','52 Week High', 'Target Price', 'Avg 20D Vol', 'Relative Strength 30D', 'Relative Strength 90D',
         'MTD', 'QTD','YTD',  '1M', '3M', '6M','1Y','Share Turnover', 'Avg 10D vs Avg 90D Vol', 'F&O','FsTicker','Security_Code','IMGURL','Company']
        colnames = list(screener_data.columns)

        screener_data=screener_data.sort_values(by=mcap_name,ascending=False)

        colnames_dict=[{'field':colnames[i],'header':colnames[i].replace("_"," ")} for i in range(0,len(colnames)) ]
        selectedVals = {elem:'Any' for elem in quantitative_list_keys+risk_list_keys+technical_list_keys+fundamental_list_keys}
        screener_data = screener_data.to_dict(orient='records')
        user_portfolios = pd.DataFrame(list(CustomerPortfolioDetails.objects.filter(customer_name=request.user.username,country=country).values('portfolio_name').distinct())).to_dict(orient='records')

        return Response({'selectedVals':selectedVals,'screener_data':screener_data,'quantitative_list':quantitative_list,'quantitative_keys':quantitative_list_keys,'quantitative_names':quant_names,'fundamental_list':fundamental_list,'fundamental_keys':fundamental_list_keys,
                        'fundamental_names':fundamental_names,'technical_names':technical_names,'risk_names':risk_names,'user_portfolios':user_portfolios,
                        'technical_list':technical_list,'technical_keys':technical_list_keys,'risk_list':risk_list,'risk_keys':risk_list_keys,'colnames_dict':colnames_dict})


class Treemap(APIView): 
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]

    def post(self, request, *args, **kwargs):
        sql_conn=pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server}; SERVER='+SERVER+'; PORT='+PORT+'; DATABASE='+DATABASE+';UID='+UID+';PWD='+PWD+';')
        params = json.loads(request.body.decode("utf-8"))
        selected_list =[params['frequency'],params['scale'],params['size'],params['parent'],params['child'],params['layer']]
        country=params['country']
        if country=="US":
            securityCode="Ticker"
            daily_stock_returns = pd.read_sql("select  * from US_Daily_Stock_Returns",sql_conn)
        else:
            daily_stock_returns = pd.read_sql("select  * from Daily_Stock_Returns",sql_conn)
            securityCode="NSE_Symbol"
        sql_conn.close()
        portfolio_name = params['portfolio_name']
        country = params['country']
        stocks_data = pd.DataFrame(list(CustomerPortfolioDetails.objects.filter(portfolio_name=portfolio_name,customer_name=request.user.username,country=country).values('company_name','fs_ticker','portfolio_name','quantity','price').order_by('created_at','company_name')))
        stocks_data = stocks_data[['company_name','fs_ticker','quantity','price']]
        stocks_data.columns = ['Company','Factset_Ticker','Quantity','Price']
        stocks = list(stocks_data['Factset_Ticker'].unique())
        daily_stock_returns= (daily_stock_returns[daily_stock_returns["FS_Ticker"].isin(stocks)]).fillna(0) 
        daily_stock_returns  = daily_stock_returns[[securityCode,selected_list[0],selected_list[3],selected_list[4],selected_list[2]]]
        daily_stock_returns[selected_list[0]]=  daily_stock_returns[selected_list[0]]*100
        data = daily_stock_returns.groupby([selected_list[3],selected_list[4],securityCode])[selected_list[2],selected_list[0]].sum().reset_index()
        gics_sub_group = daily_stock_returns.groupby([selected_list[3],selected_list[4]])[selected_list[2],selected_list[0]].sum().reset_index()
        gics_main_group  = daily_stock_returns.groupby([selected_list[3]])[selected_list[2],selected_list[0]].sum().reset_index()
        Final_output = data.copy()
        gics_sub_group.columns=[selected_list[3], selected_list[4], 'SUB_GICS_size', 'SUB_GICS_FrequencyPercentChange']
        gics_main_group.columns=[selected_list[3], 'GICS_size', 'GICS_FrequencyPercentChange']
        Final_output = Final_output.merge(gics_sub_group,on=[selected_list[3],selected_list[4]],how='inner')
        Final_output = Final_output.merge(gics_main_group,on=[selected_list[3]],how='inner')
        final_dict_list=[]
        gics = Final_output[selected_list[3]].unique().tolist()
        gics_list = []
        mcap=0
        oneLayer=[]
        colorRange=selected_list[1]
        twoLayer=[]
        subIndustry=[]
        for main_gics in gics:
            i=0
            gics_wise = Final_output[Final_output[selected_list[3]]==main_gics]
            gics_wise.reset_index(drop=True,inplace=True)
            sub_gics = gics_wise[selected_list[4]].unique().tolist()
            sub_gics_list=[]
            twoLayerList=[]
            subIndustryList=[]
            for each_gics in sub_gics:
                j=0
                sub_gics_wise = gics_wise[gics_wise[selected_list[4]]==each_gics]
                sub_gics_wise.reset_index(drop=True,inplace=True)
                sub_gics_companies_list=[]
                for k in range(0,len(sub_gics_wise)):
                    if sub_gics_wise.loc[k,selected_list[0]]>=colorRange[6]:
                        color='35764E'
                    elif sub_gics_wise.loc[k,selected_list[0]]>=colorRange[5]:
                        color='2F9E4F'
                    elif sub_gics_wise.loc[k,selected_list[0]]>=colorRange[4]:
                        color='30CC5A'
                    elif sub_gics_wise.loc[k,selected_list[0]]>=colorRange[3]:
                        color='414554'
                    elif sub_gics_wise.loc[k,selected_list[0]]>=colorRange[2]:
                        color='F63538'
                    elif sub_gics_wise.loc[k,selected_list[0]]>=colorRange[1]:
                        color='BF4045'
                    else:
                        color='8B444E'
                    if(sub_gics_wise.loc[k,selected_list[0]]>=0):
                        sub_gics_wise_dict={'label':sub_gics_wise.loc[k,securityCode]+"<br/> +"+str(round(sub_gics_wise.loc[k,selected_list[0]],2))+"%",'value':sub_gics_wise.loc[k,selected_list[2]],'svalue':round(sub_gics_wise.loc[k,selected_list[0]],2),'fillcolor':color}
                    else:
                        sub_gics_wise_dict={'label':sub_gics_wise.loc[k,securityCode]+"<br/>"+str(round(sub_gics_wise.loc[k,selected_list[0]],2))+"%",'value':sub_gics_wise.loc[k,selected_list[2]],'svalue':round(sub_gics_wise.loc[k,selected_list[0]],2),'fillcolor':color}
                    oneLayer.append(sub_gics_wise_dict)
                    twoLayerList.append(sub_gics_wise_dict)
                    subIndustryList.append(sub_gics_wise_dict)
                    sub_gics_companies_list.append(sub_gics_wise_dict)
                    mcap=mcap+sub_gics_wise.loc[k,selected_list[2]]
                    if sub_gics_wise.loc[j,selected_list[0]]>=colorRange[6]:
                        color='35764E'
                    elif sub_gics_wise.loc[j,selected_list[0]]>=colorRange[5]:
                        color='2F9E4F'
                    elif sub_gics_wise.loc[j,selected_list[0]]>=colorRange[4]:
                        color='30CC5A'
                    elif sub_gics_wise.loc[j,selected_list[0]]>=colorRange[3]:
                        color='414554'
                    elif sub_gics_wise.loc[j,selected_list[0]]>=colorRange[2]:
                        color='F63538'
                    elif sub_gics_wise.loc[j,selected_list[0]]>=colorRange[1]:
                        color='BF4045'
                    else:
                        color='8B444E'
                sub_gics_dict = { "fillcolor": color,'label':sub_gics_wise.loc[j,selected_list[4]],'value':sub_gics_wise.loc[j,'SUB_GICS_size'],'svalue':sub_gics_wise.loc[j,'SUB_GICS_FrequencyPercentChange']/len(sub_gics_wise),'data':sub_gics_companies_list}
                sub_gics_list.append(sub_gics_dict)
                subIndustry.append(sub_gics_dict)
                j=j+1
            gics_dict = {"fillcolor": "20261b",'label':gics_wise.loc[i,selected_list[3]],'value':gics_wise.loc[i,'GICS_size'],'svalue':gics_wise.loc[i,'GICS_FrequencyPercentChange'],'data':sub_gics_list}
            gics_list.append(gics_dict)
            gics_dict_twolayer = { "fillcolor": "20261b",'label':gics_wise.loc[i,selected_list[3]],'value':gics_wise.loc[i,'GICS_size'],'svalue':gics_wise.loc[i,'GICS_FrequencyPercentChange'],'data':twoLayerList}
            twoLayer.append(gics_dict_twolayer)
            i=i+1
        if(selected_list[5]=="0"):
            gics_list=oneLayer
        elif(selected_list[5]=="1" or selected_list[5]=="2" or selected_list[5]=="3"):
            gics_list=twoLayer
        elif  selected_list[5]=="4":
            gics_list=subIndustry
        return Response({'gics_list':gics_list,'mcap':mcap})


class PortfolioReturns(APIView):  
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]

    def post(self, request, *args, **kwargs):
        import datetime
        params = json.loads(request.body.decode('utf-8'))
        country=params['country']
        portfolio_name = params['portfolio_name']
        period = params['period']
        onchangevalue = params['onChangeValue']
        username=request.user.username
        context={}
        requiredTableObjects = {'India':[UniversePricesMain,SecurityMaster,MarketIndex,RiskFreeRate,'Universe_Prices_Main','Performance','Market_Index'],
                                'US':[UsUniversePricesMain,UsSecurityMaster,UsMarketIndex,UsRiskFreeRate,'Us_Universe_Prices_Main','Us_Performance','Us_Market_Index']}
        table_headerlist = {'India':['PNL','Nifty 50 Index','Nifty Smallcap 100 Index','Nifty Midcap 100 Index'] ,
                            'US':['PNL','S&P 500','Dow Jones Industrial Average','Russell 2000']}
        universeModel,securityModel,marketIndexModel,riskFreeRateModel = requiredTableObjects[country][0],requiredTableObjects[country][1],requiredTableObjects[country][2],requiredTableObjects[country][3]    
        prices_table,performance_table,market_table =  requiredTableObjects[country][4],requiredTableObjects[country][5],requiredTableObjects[country][6]
        current_date= universeModel.objects.all().aggregate(Max('date'))['date__max']
        portfolios = pd.DataFrame(list(CustomerPortfolioDetails.objects.filter(customer_name=username,country=country).values('portfolio_name')))
        if(len(portfolios)>0):       
            stocks_data = pd.DataFrame(list(CustomerPortfolioDetails.objects.filter(portfolio_name=portfolio_name,customer_name=request.user.username,country=country).values('company_name','fs_ticker','portfolio_name','quantity','price','created_at').order_by('created_at','company_name')))

            try:
                createdDate = list(stocks_data['created_at'].dt.date)[0]
            except:
                createdDate =  current_date
            stocks_data = stocks_data[['company_name','fs_ticker','quantity','price']]
            stocks_data.columns = ['Company','Factset_Ticker','Quantity','Price']
            list_of_tickers = list(stocks_data['Factset_Ticker'].unique())
            sec_factors = pd.DataFrame(list(securityModel.objects.filter(fs_ticker__in=list_of_tickers).values('fs_name','fs_ticker','security_code','gics','market_cap_category').order_by('fs_name')))
            sec_factors.columns =['Company','Factset_Ticker','Security_Code','Sector','MarketCapCategory']
            dates = {'YTD':current_date.replace(day=1,month=1),'MTD':current_date.replace(day=1),'QTD':current_date.replace(month = (((current_date.month-1)//3)*3)+1,day=1),
                    '1M':current_date+timedelta(-30),'3M':current_date+timedelta(-90),'1Y':current_date+timedelta(-365),'Max':current_date.replace(day=1,year=2019,month=1)}
            monthly_table_start_date =   current_date.replace(day=1,month=1,year=current_date.year-3)
            monthly_table_prev_start_date = universeModel.objects.filter(date__lt=str(monthly_table_start_date)).all().aggregate(Max('date'))['date__max']
            start_date = params['custom_date'] if period=='Custom' else dates[period]
            prev_start_date = universeModel.objects.filter(date__lt=str(start_date)).all().aggregate(Max('date'))['date__max']
            list_of_tickers_query = ', '.join('"{0}"'.format(w) for w in [list_of_tickers[i].replace("'","''") for i in range (0,len(list_of_tickers))] ).replace('"',"'")
            if onchangevalue==0:
                sql_conn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server}; SERVER='+SERVER+'; PORT='+PORT+'; DATABASE='+DATABASE+';UID='+UID+';PWD='+PWD+';')         
                total_prices_data = pd.read_sql("select Company,Factset_Ticker,Date,Price,[Return] from " + prices_table + " where date between '" + str(monthly_table_prev_start_date) + "' and '" + str(current_date) + "' and factset_ticker in (" + list_of_tickers_query +")  order by company,date",sql_conn)
                total_index_data = pd.read_sql("select Date,Company,[Return] from " + market_table + " where date between '" + str(monthly_table_start_date) + "' and '" + str(current_date) + "' order by company,date",sql_conn)
                performance_data = pd.read_sql("select * from " + performance_table,sql_conn)
                logos_data = pd.read_sql("select Code  Security_Code,IMGURL from  Logos where country='" + country+"'",sql_conn)

                sql_conn.close()
                monthly_cumulative_returns,ror_metrics,monthly_cumulative_chart_data = MonthlyCumulativeReturnsMain (total_prices_data,stocks_data)
                benchmark_indices = {'India':['Nifty 50 Index','Nifty Smallcap 100 Index','Nifty Midcap 100 Index'],
                                    'US':['S&P 500','Russell 2000','Dow Jones Industrial Average']}
                benchmark1,benchmark2,benchmark3=benchmark_indices[country][0],benchmark_indices[country][1],benchmark_indices[country][2]
                benchmark1_monthly,benchmark2_monthly,benchmark3_monthly =  BenchmarkMonthlyCumulativeReturns (total_index_data,benchmark1,benchmark2,benchmark3)
                monthly_cumulative_chart_data['Date'] = monthly_cumulative_chart_data['Date'].astype(str)                    
                #Performance calc
                performance_data = pd.merge(performance_data,sec_factors[['Factset_Ticker','Security_Code']],on='Factset_Ticker',how='inner')
                performance_data = pd.merge(performance_data,logos_data,on='Security_Code',how='left')
                # performance_data ['Company'] = performance_data['Company'] + " (" + performance_data['Security_Code'] + ")"
                # del performance_data['Security_Code']
                del performance_data['id']
                del performance_data['Factset_Ticker']
                performance_data.columns=['Company','ITD','YTD','MTD','YEAR','Security_Code','IMGURL']
                performance_data = performance_data.sort_values(by='YTD', ascending=False)

                context['performancetableColumns']=list(performance_data.columns)
                context['performance_data'] = performance_data.to_dict(orient='records')    
                context['monthly_cumulative'] = monthly_cumulative_returns.to_dict(orient='records')
                context['ror_metrics'] = ror_metrics.to_dict(orient='records')
                context['monthly_cumulative_chart_data'] = {'data':monthly_cumulative_chart_data.values.tolist()}
                context[benchmark1 + "_monthly"] = {'data':benchmark1_monthly.values.tolist()}
                context[benchmark2 + "_monthly"] = {'data':benchmark2_monthly.values.tolist()}
                context[benchmark3 + "_monthly"] = {'data':benchmark3_monthly.values.tolist()}
                context['benchmarks'] = [benchmark1,benchmark2,benchmark3]
                context['MonthlyperformancetableColumns'] = list(monthly_cumulative_returns.columns)
            else:
                sql_conn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server}; SERVER='+SERVER+'; PORT='+PORT+'; DATABASE='+DATABASE+';UID='+UID+';PWD='+PWD+';')         
                total_prices_data = pd.read_sql("select Company,Factset_Ticker,Date,Price,[Return] from " + prices_table + " where date between '" + str(prev_start_date) + "' and '" + str(current_date) + "' and factset_ticker in (" + list_of_tickers_query +")  order by company,date",sql_conn)
                total_index_data = pd.read_sql("select Date,Company,[Return] from " + market_table + " where date between '" + str(prev_start_date) + "' and '" + str(current_date) + "' order by company,date",sql_conn)            
                sql_conn.close()
            prices_data = total_prices_data[total_prices_data['Date']>=prev_start_date]
            market_index_data = total_index_data[total_index_data['Date']>=prev_start_date]
            risk_free_rate_data = pd.DataFrame(list(riskFreeRateModel.objects.filter(date__range=(start_date, current_date)).order_by('date').values('date','risk_free_rate')))
            risk_free_rate_data.columns = ['Date','Risk Free Rate']
            #Daily and Monthly Cumulative calculations
            cumulative_returns,daily_returns,metrics_table_data=CalcsMain(country,stocks_data,prices_data,market_index_data,risk_free_rate_data,portfolio_name,period)
            metrics_table_data1 = metrics_table_data.iloc[:8,:]
            metrics_table_data2 = metrics_table_data.iloc[8:, :]
            cumulative_returns['Date'] = cumulative_returns['Date'].astype(str)
            output=pd.DataFrame(columns=['name','lineWidth','color','data'])
            final_dict = {'names':list(cumulative_returns.columns)[1:],'lineWidth':[3,2,2,2,2],
                            'color':['#002060','#00A8E8','#FDCA40','#00A878','#9BC53D']}
            for i in range(0,5):
                output.loc[i]=[final_dict['names'][i],final_dict['lineWidth'][i],final_dict['color'][i],cumulative_returns[['Date',final_dict['names'][i]]].values.tolist()]
            output_cumulative_returns=output.to_dict(orient='records')        
            #Returns by stock and sector
            gics_data = sec_factors[['Company','Factset_Ticker','Sector']]
            gics_data.columns =['Company','Factset_Ticker','GICS']
            stock_df,sector_wise = Stocks_Investment_Returns(prices_data,stocks_data,gics_data,period)         
            stock_df = pd.merge(stock_df,sec_factors[['Factset_Ticker','Security_Code']],on='Factset_Ticker',how='inner')
            stock_df ['Company'] = stock_df['Company'] + " (" + stock_df['Security_Code'] + ")"
            del stock_df['Security_Code']   
            context['tableColumns1'] = list(metrics_table_data1.columns)
            context['tableColumns2'] = list(metrics_table_data2.columns)
            metrics_table_data1 = metrics_table_data1.to_dict(orient='records')
            metrics_table_data2 = metrics_table_data2.to_dict(orient='records')
            context["table_data1"] = metrics_table_data1 
            context["table_data2"] = metrics_table_data2         
            context['cumulative_returns'] = output_cumulative_returns
            def formatData(inputdf,indexColumn,valueColumn,period):
                output_list = inputdf.copy()
                output_list['y'] = inputdf[valueColumn]
                output_list.rename(columns={indexColumn:"name",'Current Exposure':'investment',period + "_PNL":"pnl",period+"_Return":'return'},inplace=True)
                output_list = output_list.to_dict(orient='records')
                return output_list 
            context['stock_exposure_list'] = formatData(stock_df,'Company','Current Exposure',period)
            context['stock_return_list'] = formatData(stock_df,'Company',period + '_Return',period)
            context['stock_pnl_list'] = formatData(stock_df,'Company',period + '_PNL',period)
            context['sector_exposure_list'] = formatData(sector_wise,'GICS','Current Exposure',period)
            context['sector_return_list'] = formatData(sector_wise,'GICS',period + '_Return',period)
            context['sector_pnl_list'] = formatData(sector_wise,'GICS',period + '_PNL',period)
            context['created_date'] = createdDate
            context['status']=1
        else:
            context['status']=0
        return Response(context)
        


class DailyMarketData(APIView):
    authentication_classes = []
    permission_classes = []
      
    def get(self,request,*args,**kwargs):
        sql_conn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server}; SERVER='+SERVER+'; PORT='+PORT+'; DATABASE='+DATABASE+';UID='+UID+';PWD='+PWD+';')
        scroll_data = pd.read_sql("select * from Daily_Market_Data order by [Order]",sql_conn)
        sql_conn.close()
        scroll_data['Current_Price'] =  scroll_data['Current_Price'] .round(1)
        scroll_data.loc[scroll_data['Ticker'].isin(['BSE SENSEX', 'NIFTY 50', 'INDIA VIX', 'USD/INR', 'NIFTY BANK', 'NIFTY MIDCAP 100', 'NIFTY SMALLCAP 100', 'GOLD']),'Current_Price'] = scroll_data['Current_Price'].astype(int).apply(lambda x:format_currency(x,'',locale='en_IN'))
        scroll_data.loc[scroll_data['Ticker'].isin(['S&P 500', 'RUSSELL 2000', 'US VIX', 'OIL ($/Barrel)', 'DOW JONES 30', 'NASDAQ', 'BITCOIN', 'GOLD ($/oz)']),'Current_Price'] = scroll_data['Current_Price'].astype(str).str.replace(",","").astype(float).astype(int).apply(lambda x:format_currency(x,'',locale='en_US'))
        scroll_data['Net_Change'] = np.select([scroll_data['Net_Change']>=0],["+" + round(scroll_data['Net_Change'],1).astype(str)],default=round(scroll_data['Net_Change'],1).astype(str))
        scroll_data['Percentage_Change'] = np.select([scroll_data['Percentage_Change']>=0],["+" + round(scroll_data['Percentage_Change']*100,1).astype(str)],default=round(scroll_data['Percentage_Change']*100,1).astype(str))
        scroll_data['Current_Price'] = scroll_data['Current_Price'].astype(str).replace('\.0*', '', regex=True)
        scroll_data = scroll_data.to_dict(orient='records')
        return Response({'market_data':scroll_data})





class DashboardSummary(APIView):  
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]
    def post(self, request, *args, **kwargs):
        params = json.loads(request.body.decode('utf-8'))
        country=params['country']
        portfolio_name = params['portfolio_name']
        period = '12M'
        username = request.user.username
        context={}
        requiredTableObjects = {'India':[UniversePricesMain,SecurityMaster,MarketIndex,RiskFreeRate,'Universe_Prices_Main','Market_Index','Security_Master'],
                                'US':[UsUniversePricesMain,UsSecurityMaster,UsMarketIndex,UsRiskFreeRate,'Us_Universe_Prices_Main','Us_Market_Index','US_Security_Master']}
        table_headerlist = {'India':['PNL','Nifty 50 Index','Nifty Smallcap 100 Index','Nifty Midcap 100 Index'] ,
                            'US':['PNL','S&P 500','Dow Jones Industrial Average','Russell 2000']}
        universeModel,securityModel,marketIndexModel,riskFreeRateModel = requiredTableObjects[country][0],requiredTableObjects[country][1],requiredTableObjects[country][2],requiredTableObjects[country][3]    
        prices_table,market_table,sec_table =  requiredTableObjects[country][4],requiredTableObjects[country][5],requiredTableObjects[country][6]
        portfolios = pd.DataFrame(list(CustomerPortfolioDetails.objects.filter(customer_name=username,country=country).values('portfolio_name')))
        if(len(portfolios)>0):
            stocks_data = pd.DataFrame(list(CustomerPortfolioDetails.objects.filter(portfolio_name=portfolio_name,customer_name=username,country=country).values('company_name','fs_ticker','portfolio_name','quantity','price').order_by('created_at','company_name')))
            stocks_data = stocks_data[['company_name','fs_ticker','quantity','price']]
            stocks_data.columns = ['Company','Factset_Ticker','Quantity','Price']
            list_of_tickers = list(stocks_data['Factset_Ticker'].unique())
            sec_factors = pd.DataFrame(list(securityModel.objects.filter(fs_ticker__in=list_of_tickers).values('fs_name','fs_ticker','security_code','gics','market_cap_category').order_by('fs_name')))
            sec_factors.columns =['Company','Factset_Ticker','Security_Code','Sector','MarketCapCategory']
            current_date= universeModel.objects.all().aggregate(Max('date'))['date__max']
            dates = {'YTD':current_date.replace(day=1,month=1),'MTD':current_date.replace(month=1,day=1),'QTD':current_date.replace(month = (((current_date.month-1)//3)*3)+1,day=1),
                    '1M':current_date+timedelta(-30),'3M':current_date+timedelta(-90),'12M':current_date+timedelta(-365),'2019':current_date.replace(day=1,year=2019,month=1),
                        '2020':current_date.replace(year=2020,day=1,month=1)}
            start_date = dates[period]
            prev_start_date = universeModel.objects.filter(date__lt=str(start_date)).all().aggregate(Max('date'))['date__max']
            list_of_tickers_query = ', '.join('"{0}"'.format(w) for w in [list_of_tickers[i].replace("'","''") for i in range (0,len(list_of_tickers))] ).replace('"',"'")
            sql_conn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server}; SERVER='+SERVER+'; PORT='+PORT+'; DATABASE='+DATABASE+';UID='+UID+';PWD='+PWD+';')         
            total_prices_data = pd.read_sql("select Company,Factset_Ticker,Date,Price,[Return] from " + prices_table + " where date between '" + str(prev_start_date) + "' and '" + str(current_date) + "' and factset_ticker in (" + list_of_tickers_query +")  order by company,date",sql_conn)
            total_index_data = pd.read_sql("select Date,Company,[Return] from " + market_table + " where date between '" + str(prev_start_date) + "' and '" + str(current_date) + "' order by company,date",sql_conn)            
            sec_master = pd.read_sql("select * from "+ sec_table +"",sql_conn)
            logos_data = pd.read_sql("select Code  Security_Code,IMGURL from  Logos where country='" + country+"'",sql_conn)

            sql_conn.close()
            prices_data = total_prices_data[total_prices_data['Date']>=prev_start_date]
            market_index_data = total_index_data[total_index_data['Date']>=prev_start_date]
            risk_free_rate_data = pd.DataFrame(list(riskFreeRateModel.objects.filter(date__range=(start_date, current_date)).order_by('date').values('date','risk_free_rate')))
            risk_free_rate_data.columns = ['Date','Risk Free Rate']
            #Daily and Monthly Cumulative calculations
            cumulative_returns,daily_returns,metrics_table_data=CalcsMain(country,stocks_data,prices_data,market_index_data,risk_free_rate_data,portfolio_name,period)
            cumulative_returns = cumulative_returns.dropna().reset_index(drop=True)
            daily_returns = daily_returns.dropna().reset_index(drop=True)
            cumulative_returns['Date'] = cumulative_returns['Date'].astype(str)
            output=pd.DataFrame(columns=['name','lineWidth','color','data'])
            final_dict = {'names':list(cumulative_returns.columns)[1:],'lineWidth':[3,2,2,2,2],
                            'color':['#002060','#00A8E8','#FDCA40','#00A878','#9BC53D']}
            for i in range(0,5):
                output.loc[i]=[final_dict['names'][i],final_dict['lineWidth'][i],final_dict['color'][i],cumulative_returns[['Date',final_dict['names'][i]]].values.tolist()]
            output_cumulative_returns=output.to_dict(orient='records')        
            #Returns by stock and sector
            gics_mcap_data = sec_factors[['Company','Factset_Ticker','Sector','MarketCapCategory']]
            gics_mcap_data.columns =['Company','Factset_Ticker','GICS','Market_Cap_Category']
            stock_wise_data,sector_wise_data = Stocks_Investment_Returns(prices_data,stocks_data,gics_mcap_data,period) 
            mcap_df =  pd.merge(stock_wise_data,gics_mcap_data[['Factset_Ticker','Market_Cap_Category']],on='Factset_Ticker',how='inner')
            mcap_df = mcap_df[['Company','Factset_Ticker','Market_Cap_Category','Current Exposure']]
            mcap_df['Exposure%'] = mcap_df['Current Exposure']/mcap_df['Current Exposure'].sum()
            mcap_df = mcap_df.groupby('Market_Cap_Category')['Current Exposure','Exposure%'].sum().reset_index()
            stock_wise_data = pd.merge(stock_wise_data,sec_factors[['Factset_Ticker','Security_Code']],on='Factset_Ticker',how='inner')
            stock_wise_data ['Company'] = stock_wise_data['Company'] + " (" + stock_wise_data['Security_Code'] + ")"
            stock_df = stock_wise_data.head(10).reset_index(drop=True)
            stock_df = pd.merge(stock_df,logos_data,on='Security_Code',how='left')
            del stock_wise_data['Security_Code']           
            cum_returns_list,investment_amt_list,risk_output_list,port_index_returns=[],[],[],[]
            benchmark_indices={'India':'Nifty 50 Index','US':'S&P 500'}
            stock_wise_data=stock_wise_data.sort_values(by=period+'_Return',ascending=False).reset_index(drop=True)                        
            sector_wise_data=sector_wise_data.sort_values(by=period+'_Return',ascending=False).reset_index(drop=True)                        

            #Returns Summary Code
            port_index_returns.append(cumulative_returns.loc[len(cumulative_returns)-1,'Portfolio'])
            port_index_returns.append(cumulative_returns.loc[len(cumulative_returns)-1,benchmark_indices[country]])
            cum_returns_list.append(port_index_returns)
            cum_returns_list.append([stock_wise_data['Company'][0],str(round(stock_wise_data[period + '_PNL'][0],2)),str(round(stock_wise_data[period + '_Return'][0]*100,2))])
            cum_returns_list.append([stock_wise_data['Company'][len(stock_wise_data)-1],str(round(stock_wise_data[period +'_PNL'][len(stock_wise_data)-1],2)),str(round(stock_wise_data[period  + '_Return'][len(stock_wise_data)-1]*100,2))])
            cum_returns_list.append([sector_wise_data['GICS'][0],str(round(sector_wise_data[period + '_PNL'][0],2)),str(round(sector_wise_data[period + '_Return'][0]*100,2))])
            cum_returns_list.append([sector_wise_data['GICS'][len(sector_wise_data)-1],str(round(sector_wise_data[period + '_PNL'][len(sector_wise_data)-1],2)),str(round(sector_wise_data[period + '_Return'][len(sector_wise_data)-1]*100,2))])
            #Risk Summary
            returns_data=prices_data.pivot(index='Date',columns='Factset_Ticker',values='Return').reset_index()
            benchmark_data=market_index_data[market_index_data['Company']==benchmark_indices[country]]
            returns_data=pd.merge(returns_data,benchmark_data[['Date','Return']],on='Date',how='inner')
            returns_data.fillna(0,inplace=True)
            res,cols={},list(returns_data.columns)
            for i in range (1,len(cols)-1):
                beta = stats.linregress(returns_data['Return'].values,returns_data[cols[i]].values)[0:1][0]
                res[cols[i]]=beta
            risk_output_list.append([daily_returns['Portfolio'].std()*math.sqrt(252)])
            risk_output_list.append([daily_returns[benchmark_indices[country]].std()*math.sqrt(252)])               
            beta = stats.linregress(daily_returns[benchmark_indices[country]].values,daily_returns["Portfolio"].values)[0:1][0]
            risk_output_list.append([round(beta,2)])
            risk_output_list.append([max(res, key=res.get),round(res[max(res, key=res.get)],2)])
            #Investment amount summary
            stock_wise_data=stock_wise_data.sort_values(by='Current Exposure',ascending=False).reset_index(drop=True)                        
            stock_wise_data['Exposure%'] = stock_wise_data['Current Exposure'] / (stock_wise_data['Current Exposure'].sum())
            sector_wise_data=sector_wise_data.sort_values(by='Current Exposure',ascending=False).reset_index(drop=True)                        
            sector_wise_data['Exposure%'] = sector_wise_data['Current Exposure'] / (sector_wise_data['Current Exposure'].sum())
            investment_amt_list.append([sector_wise_data['GICS'][0],str(round(sector_wise_data['Current Exposure'][0],2)),str(round(sector_wise_data['Exposure%'][0],2))])
            output_list=[]
            for i in range(0,len(stock_wise_data)):
                if i<=2:
                    output_list.append([stock_wise_data['Company'][i],str(round(stock_wise_data['Current Exposure'][i],2)),str(round(stock_wise_data['Exposure%'][i],2))])
                else:
                    break
            
            investment_amt_list.append(output_list)
            investment_output = []
            investment_output2 = []
            for i in range (0,len(sector_wise_data)):      
                x={"name":sector_wise_data['GICS'][i]+' ('+str(float(round((sector_wise_data['Exposure%'][i]*100),1)))+'%)',"y":sector_wise_data['Exposure%'][i],"Exposure":sector_wise_data['Current Exposure'][i],"new_name":sector_wise_data['GICS'][i]}
                investment_output.append(x)
            for i in range (0,len(mcap_df)):
                val={"name":mcap_df['Market_Cap_Category'][i]+' ('+str(float(round((mcap_df['Exposure%'][i]*100),1)))+'%)',"y":mcap_df['Exposure%'][i],"Exposure":mcap_df['Current Exposure'][i],"new_name":mcap_df['Market_Cap_Category'][i]}        
                investment_output2.append(val)
            prices_data.columns = ['Company', 'Factset_Ticker', 'Date', 'Price', 'Return']
            stock_prices_qty_data = pd.merge(prices_data[['Date','Company','Factset_Ticker','Price']],stocks_data[['Factset_Ticker','Quantity']],on='Factset_Ticker',how='inner')
            style_measures_data =  style_measures(country,stock_prices_qty_data,sec_master)


            benchmarks = {'India':['Nifty 50 Index','Nifty Smallcap 100 Index','Nifty Midcap 100 Index'],
                            'US':['S&P 500','Dow Jones Industrial Average']}
            volatility_list = ['Volatility',str(round(daily_returns['Portfolio'].std()*math.sqrt(252)*100,2))+"%"]
            beta_list = ['Beta',round(stats.linregress(daily_returns[benchmarks[country][0]].values,daily_returns["Portfolio"].values)[0:1][0],2)]
            for i in range(len(benchmarks[country])):
                volatility_list.append( str(round(daily_returns[benchmarks[country][i]].std()*math.sqrt(252)*100,2))+"%")
                beta_list.append(round(stats.linregress(daily_returns[benchmarks[country][0]].values,daily_returns[benchmarks[country][i]].values)[0:1][0],2))
            style_measures_data.loc[6,:] =volatility_list                                        
            style_measures_data.loc[7,:] = beta_list
            risk_monitor_model = RiskMonitor if country=='India' else UsRiskMonitor
            risk_monitor_data = pd.DataFrame(list(risk_monitor_model.objects.filter(fs_ticker__in = list_of_tickers).values()))
            risk_monitor_data = pd.merge(risk_monitor_data,sec_factors[['Factset_Ticker','Security_Code']],left_on="fs_ticker",right_on="Factset_Ticker",how="inner")
            risk_monitor_data = pd.merge(risk_monitor_data,logos_data,on='Security_Code',how='left')
            risk_monitor_data=risk_monitor_data[['company_name','views','Security_Code','IMGURL']]
            risk_monitor_data.sort_values(by=['Security_Code'],ascending=[False],inplace=True)

            stock_df.columns=["Index",'Company','Fs_tcker','CurrentExposure','Return','PNL','Security_Code','IMGURL']
            stock_df.sort_values(by=['CurrentExposure'],ascending=[False],inplace=True)
            metrics_table_data1 = metrics_table_data.iloc[:8,:]
            metrics_table_data2 = metrics_table_data.iloc[8:, :]
            context['username'] = username
            context['tableColumns1'] = list(metrics_table_data1.columns)
            context['tableColumns2'] = list(metrics_table_data2.columns)
            context["stocks_count"] = len(stocks_data)  
            context["sector_count"] = len(sec_factors['Sector'].unique())  
            context["table_data1"] = metrics_table_data1.to_dict(orient='records')
            context["table_data2"] = metrics_table_data2.to_dict(orient='records') 
            context['cumulative_returns'] = output_cumulative_returns
            context['stock_df'] = stock_df.to_dict(orient='records')        
            context['sector_wise'] = investment_output    
            context['mcap_wise'] = investment_output2     
            context['returns_summary'] =  cum_returns_list
            context['risk_summary'] =  risk_output_list
            context['investment_summary'] =  investment_amt_list
            context['risk_monitor'] = risk_monitor_data.to_dict(orient='records')
            context['styleMeasuresColumns']=list(style_measures_data.columns)
            context['style_measures'] = style_measures_data.to_dict(orient='records')
            context['status']=1
        else:
            context['status']=0
        
        return Response(context)


def df_melt(df_val,df_actions,columns):
    df_val.columns= columns
    df_actions.columns = columns
    df_output = pd.merge(pd.melt(df_val),pd.melt(df_actions),on='variable',how='inner')
    df_output.columns =["Metric","Value","Action"]
    return df_output


class TechnicalIndicators(APIView):
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]

    def post(self, request, *args, **kwargs):
        params = json.loads(request.body.decode("utf-8"))
        country = params['country']
        factset_ticker = params['factset_ticker']
        context,context1={},{}
        lineColor=['#00A8E8','#28a745','#02BCD4','#ffc107','#dc3545','#5A6268','#23272B']
        requiredTableObjects = {'India':[TechnicalIndicatorsDaily,UniversePricesMain,SecurityMaster],'US':[UsTechnicalIndicatorsDaily,UsUniversePricesMain,UsSecurityMaster]}
        tables = {"India":["Moving_Average","Average_True_Range","Volume_Data","Volume_20_Data","Moving_Average_Convergence_Divergence"],
        "US":["US_Moving_Average","US_Average_True_Range","US_Volume_Data","US_Volume_20_Data","US_Moving_Average_Convergence_Divergence"]}
        technical_model,universeModel,securityModel =  requiredTableObjects[country][0],requiredTableObjects[country][1],requiredTableObjects[country][2]  
        moving_average_table,avg_true_table,volume_table,volume_20_table,macd_table = tables[country][0],tables[country][1],tables[country][2],tables[country][3],tables[country][4]
        security_names = pd.DataFrame(list(securityModel.objects.filter(flag='yes').values('fs_name','fs_ticker','security_code')))
        security_names ['fs_name'] = security_names['fs_name'] + " (" +security_names['security_code'] + ")"
        security_names.sort_values(by=['fs_name'],ascending=[True],inplace=True)

        securities = security_names[['fs_name','fs_ticker']].to_dict(orient='records')
        rating_data = pd.DataFrame(list(technical_model.objects.filter(fs_ticker = factset_ticker).values())) 
        try:
            finalRatingVal = list(rating_data['final_rating'])[0]
        except:
            finalRatingVal = ""       
        moving_avg_val = rating_data[["ma9","ma20","ma50","ma200","ma9_ma20_value","ma20_ma50_value","ma50_ma200_value"]]
        moving_avg_actions =rating_data[["ma9_actions","ma50_actions","ma20_actions","ma200_actions","ma9_ma20_actions","ma20_ma50_actions","ma50_ma200_actions"]]
        columns = ['MA(9)','MA(20)','MA(50)','MA(200)','9 and 20 crossover','20 and 50 crossover','50 and 200 crossover']
        moving_avg = df_melt(moving_avg_val,moving_avg_actions,columns)
        momentum_val = rating_data[["rsi_value","cci_value","williams_value","stochrsi_value","stochastics_value"]]
        momentum_actions = rating_data[["rsi_actions","cci_actions","williams_actions","stochrsi_actions","stochastics_actions"]]
        columns = ['RSI','CCI','Williams %R','STOCHRSI','Stochastics']
        momentum = df_melt(momentum_val,momentum_actions,columns)
        trade_val = rating_data[["macd_value","atr_value","adx_value","super_trend_value"]]
        trade_actions = rating_data[["macd_actions","atr_actions","adx_actions","super_trend_actions"]]
        columns = ['MACD','ATR','ADX','Supertrend']
        trade = df_melt(trade_val,trade_actions,columns)
        volume_val = rating_data[["mfi_value","pvo_value","cmf_value"]]
        volume_actions = rating_data[["mfi_actions","pvo_actions","cmf_actions"]]
        columns = ['Money Flow Index','Volume Oscillator','Chaikin Money Flow']
        volume = df_melt(volume_val,volume_actions,columns)
        current_date= universeModel.objects.all().aggregate(Max('date'))['date__max']
        start_date = current_date+timedelta(-365)        
        sql_conn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server}; SERVER='+SERVER+'; PORT='+PORT+'; DATABASE='+DATABASE+';UID='+UID+';PWD='+PWD+';') 
        context1['moving_avg_table'] = moving_avg.to_dict(orient='records')
        context1['momentum_table'] = momentum.to_dict(orient='records')
        context1['trade_table'] = trade.to_dict(orient='records')
        context1['volume_table'] = volume.to_dict(orient='records')
        context1['moving_action_count'] = list(rating_data['moving_average_rating'])[0]
        context1['momentum_action_count'] = list(rating_data['momentum_rating'])[0]
        context1['trade_action_count'] = list(rating_data['trend_rating'])[0]
        context1['volume_action_count'] = list(rating_data['volume_rating'])[0]
        moving_avg = pd.read_sql("select * from "+ moving_average_table +" where Factset_Ticker='"+factset_ticker+"' and date between '"+str(start_date)+"' and '"+str(current_date)+"' order by date",sql_conn)
        avg_true_range = pd.read_sql("select * from "+ avg_true_table +" where FS_Ticker='"+factset_ticker+"' and date between '"+str(start_date)+"' and '"+str(current_date)+"' order by date",sql_conn)
        volume_rsi_volatility = pd.read_sql("select * from "+  volume_table +" where Ticker='"+factset_ticker+"' and date between '"+str(start_date)+"' and '"+str(current_date)+"' order by date",sql_conn)
        volume_20d = pd.read_sql("select * from "+volume_20_table+" where Ticker='"+factset_ticker+"' and date between '"+str(start_date)+"' and '"+str(current_date)+"' order by date",sql_conn)
        MACD = pd.read_sql("select * from "+macd_table+" where FS_Ticker='"+factset_ticker+"' and date between '"+str(start_date)+"' and '"+str(current_date)+"' order by date",sql_conn)
        sql_conn.close()
        moving_avg = moving_avg[["Date","MA9","MA20","MA26","MA50","MA100","MA200"]] 
        moving_avg['Date'] = pd.to_datetime(moving_avg['Date']).dt.date
        avg_true_range = avg_true_range[["Date","Open","Low","High","Close",]]
        avg_true_range['Date'] = pd.to_datetime(avg_true_range['Date']).dt.date    
        moving_avg = pd.merge(moving_avg,avg_true_range,on='Date',how='inner')
        moving_avg['Date'] = moving_avg['Date'].astype(str)        
        volume = volume_rsi_volatility[volume_rsi_volatility['Factor']=='Volume'] [["Date","value"]]
        volume = volume.rename(columns={"value":"Volume"})
        volume_20d = volume_20d[["Date","Volume_20"]]
        volume = pd.merge(volume,volume_20d,on='Date',how='inner')
        volume['Date'] = volume['Date'].astype(str)
        RSI =  volume_rsi_volatility[volume_rsi_volatility['Factor']=='RSI'] [["Date","value"]]
        RSI = RSI.rename(columns={"value":"RSI"})
        RSI['Date'] = RSI['Date'].astype(str)        
        volatility =  volume_rsi_volatility[volume_rsi_volatility['Factor']=='Volatility'] [["Date","value"]]
        volatility = volatility.rename(columns={"value":"Volatility"})
        volatility['Date'] = volatility['Date'].astype(str)        
        MACD = MACD[["Date","MACD Line","Signal Line","MACD Histogram"]]
        MACD['Date'] = MACD['Date'].astype(str)   
        output_ma_prices=[
                        {"type":"candlestick","name":"Price","data":moving_avg[["Date","Open","High","Low","Close"]].dropna().values.tolist()},
                        {"type":"spline","name":"MA9","lineWidth": 2,'color':"#00A878","data":moving_avg[['Date',"MA9"]].dropna().values.tolist()},
                        {"type":"spline","name":"MA20","lineWidth": 2,'color':"#9BC53D","data":moving_avg[['Date',"MA20"]].dropna().values.tolist()},
                        {"type":"spline","name":"MA26","lineWidth": 2,'color':"#CACF85","data":moving_avg[['Date',"MA26"]].dropna().values.tolist()},
                        {"type":"spline","name":"MA50","lineWidth": 2,'color':"#00A8E8","data":moving_avg[['Date',"MA50"]].dropna().values.tolist()},
                        {"type":"spline","name":"MA100","lineWidth": 2,'color':"#FDCA40","data":moving_avg[['Date',"MA100"]].dropna().values.tolist()},
                        {"type":"spline","name":"MA200","lineWidth": 2,'color':"#F79824","data":moving_avg[['Date',"MA200"]].dropna().values.tolist()}]
        output_volume=[{"type":"column","name":"Volume","data":volume[['Date',"Volume"]].dropna().values.tolist()},
                        {"type":"spline","name":"Volume_20Day","data":volume[['Date','Volume_20']].dropna().values.tolist(),"lineWidth": 2,'color':'#002060'}]
        output_volatility=[{"type":"spline","name":"Volatility","data":volatility[['Date',"Volatility"]].dropna().values.tolist(),"lineWidth":1.5,'color':"#0E131F"}]
        output_rsi=[{"type":"spline","name":"RSI","data":RSI[['Date',"RSI"]].dropna().values.tolist(),'color':"#89BBFE",'zIndex':9}]
        output_macd=[{"type":"spline","name":"MACD Line","data":MACD[['Date',"MACD Line"]].dropna().values.tolist(),"lineWidth": 1.5,'color':lineColor[0]},
                    {"type":"spline","name":"Signal Line","data":MACD[['Date',"Signal Line"]].dropna().values.tolist(),"lineWidth": 1.5,'color':"#89BBFE"},
                    {"type":"column","name":"MACD Histogram","data":MACD[['Date',"MACD Histogram"]].dropna().values.tolist()}]
        context2={'max_price':moving_avg['High'].max(),'min_price':moving_avg['Low'].min(),'max_volume':volume['Volume'].max(),'min_volume':volume['Volume'].min(),
                'max_rsi':RSI['RSI'].max(),'min_rsi':RSI['RSI'].min(),'max_volatility':volatility['Volatility'].max(),'min_volatility':volatility['Volatility'].min(),
                 'max_macd':max(MACD['MACD Line'].max(),MACD['Signal Line'].max()),'min_macd':min(MACD['MACD Line'].min(),MACD['Signal Line'].min()),'output_volume':output_volume,
                 'output_volatility':output_volatility,'output_rsi':output_rsi,'output_macd':output_macd,'output_ma_prices':output_ma_prices,
                 'stock_names_tickers':securities,'finalRating':finalRatingVal }
        context = {**context1,**context2}
        return Response(context)



class OurPerformance(APIView):
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]

    def post(self, request, *args, **kwargs):
        params= json.loads(request.body.decode("utf-8"))
        country=params['country']
        tables = {"India":["Transactions","Portfolio_V5","Daily_Positions",UniversePricesMain,'Market_Index',SecurityMaster],
                "US":["US_Transactions","US_Portfolio_V5","US_Daily_Positions",UsUniversePricesMain,'Us_Market_Index',UsSecurityMaster]}
        transactions_table,portfolio_v5_table,daily_positions_table,universeModel= tables[country][0],tables[country][1],tables[country][2],tables[country][3]
        market_table,security_model =  tables[country][4],tables[country][5]
        start_date = date.today().replace(year=2020,day=1,month=1)
        current_date= universeModel.objects.all().aggregate(Max('date'))['date__max']
        sql_conn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server}; SERVER='+SERVER+'; PORT='+PORT+'; DATABASE='+DATABASE+';UID='+UID+';PWD='+PWD+';') 
        transactions = pd.read_sql("select * from "+transactions_table+" where CUSTOMER='suresh' ORDER BY SECDESCRIPTION,TRADEDATE",sql_conn)
        portfolio_holdings_data = pd.read_sql("select * from "+portfolio_v5_table+" where CUSTOMER_NAME='suresh' ",sql_conn)
        equity_cashflow = pd.read_sql("select eomonth(DATE) Month_End ,sum([P&L]) PNL from "+daily_positions_table+" where DATE>='" + str(start_date) + "' and customer_name='suresh'  group by eomonth(DATE)",sql_conn)
        daily_positions = pd.read_sql("select * from "+daily_positions_table+" where CUSTOMER_NAME='suresh' and date = (select max(date) from "+daily_positions_table+")",sql_conn)
        min_start_date = pd.read_sql("select min(date) as mindate from " + daily_positions_table + " where customer_name='suresh'",sql_conn).loc[0,'mindate']
        daily_pnl_data = pd.read_sql("""select Date,sum(pnl_pct) Portfolio_Daily from (select date,stock,([P&L])/AUM_BOD as pnl_pct from """ + daily_positions_table + """ where customer_name='suresh' ) a
                                        group by date order by date""",sql_conn)
        benchmark_data = pd.read_sql("select Date,Company,[Return] from " + market_table + " where date between '" + str(min_start_date) + "' and '" + str(current_date) + "' order by company,date",sql_conn)        
        performance_date = pd.read_sql("select max(date) performance_date from " + daily_positions_table + " where month(date)=month(getdate())-1",sql_conn).loc[0,'performance_date']
        logos_data = pd.read_sql("select Code  Security_Code,IMGURL from  Logos where country='" + country+"'",sql_conn)
        sql_conn.close()
        securities = pd.DataFrame(list(security_model.objects.values('fs_ticker','security_code')))
        securities.columns = ['Factset_Ticker','Security_Code']
        transactions = pd.merge(transactions,securities,left_on='SECURITY ID',right_on='Factset_Ticker')
        portfolio_holdings_data = pd.merge(portfolio_holdings_data,securities,on='Factset_Ticker')
        transactions = pd.merge(transactions,logos_data,on='Security_Code',how='left')
        portfolio_holdings_data = pd.merge(portfolio_holdings_data,logos_data,on='Security_Code',how='left')
        context={}
        current_portfolio_holdings = portfolio_holdings_data[portfolio_holdings_data["DATE"]==portfolio_holdings_data["DATE"].max()].reset_index(drop=True)
        transactions = transactions[['SECDESCRIPTION','Security_Code','TRADEDATE','ACTION','QUANTITY','PRICE','NETMONEY','IMGURL']].fillna("")   
        transactions.sort_values(by='TRADEDATE',ascending=False,inplace=True)

        portfolio_holdings = current_portfolio_holdings.copy()
        portfolio_holdings= portfolio_holdings[['COMPANY','CURR_QTY','VALUE_COST','VALUE_MKT_PRICE','PCT_CHG','PNL_REALIZED','PNL_UNREALIZED','Security_Code','IMGURL']].fillna("")
        portfolio_holdings["Profit_Loss"]= portfolio_holdings["PNL_REALIZED"]+portfolio_holdings["PNL_UNREALIZED"]
        portfolio_holdings =portfolio_holdings[[ "COMPANY","CURR_QTY","VALUE_COST","VALUE_MKT_PRICE","PCT_CHG","Profit_Loss",'Security_Code','IMGURL']] 
        allocation = current_portfolio_holdings.groupby(['SECTOR'])['VALUE_MKT_PRICE'].sum().to_frame().reset_index()
        allocation["Exposure_Pct"] = allocation["VALUE_MKT_PRICE"]/(allocation["VALUE_MKT_PRICE"].sum())
        allocation = allocation[["SECTOR","Exposure_Pct"]] 
        my_stock_performance = current_portfolio_holdings.groupby(['PROFIT_LOSS'])['PROFIT_LOSS'].count().astype(int).reset_index(name='count')
        starting_investment ={'India':5000000,'US':1000000}        
        portfolio_highlights_cash = (starting_investment[country]+sum(portfolio_holdings_data["PNL_REALIZED"]))-sum(current_portfolio_holdings["VALUE_COST"])
        portfolio_highlights_realized = sum(portfolio_holdings_data["PNL_REALIZED"])
        portfolio_top_exposure = allocation[allocation['Exposure_Pct']== max(allocation["Exposure_Pct"])][["SECTOR","Exposure_Pct"]]
        portfolio_highlights_mps = current_portfolio_holdings[current_portfolio_holdings["PCT_CHG"]==(current_portfolio_holdings["PCT_CHG"].max())]
        portfolio_highlights_mps = portfolio_highlights_mps[["COMPANY","PCT_CHG"]]
        portfolio_highlights_lps = current_portfolio_holdings[current_portfolio_holdings["PCT_CHG"]==(current_portfolio_holdings["PCT_CHG"].min())]
        portfolio_highlights_lps = portfolio_highlights_lps[["COMPANY","PCT_CHG"]]    
        equity_valuation = sum(current_portfolio_holdings["VALUE_MKT_PRICE"])
        equity_investment  = sum(current_portfolio_holdings["VALUE_COST"])
        current_holdings_profit_loss = sum(current_portfolio_holdings["PNL_UNREALIZED"])
        profit_loss_day = sum(daily_positions["P&LUnrealized"])  
        max_gainer = portfolio_holdings[portfolio_holdings['Profit_Loss']==portfolio_holdings['Profit_Loss'].max()][['COMPANY','Profit_Loss']]
        max_loser = portfolio_holdings[portfolio_holdings['Profit_Loss']==portfolio_holdings['Profit_Loss'].min()][['COMPANY','Profit_Loss']]
        equity_cashflow['Month_End']=equity_cashflow['Month_End'].astype(str)
        equity_cashflow_data = equity_cashflow['PNL'].values.tolist()
        equity_cashflow_categories = equity_cashflow['Month_End'].values.tolist()
        portfolio_holdings.sort_values(by=['VALUE_COST'],ascending=[False],inplace=True)
        portfolio_holdings = portfolio_holdings.to_dict(orient='records')
        transactions = transactions.to_dict(orient='records')
        allocation_list = []
        for i in range (0,len(allocation)):      
            allocation_list.append({"name":allocation['SECTOR'][i],"y":allocation['Exposure_Pct'][i]})
        profit_list=my_stock_performance[my_stock_performance['PROFIT_LOSS']=='PROFIT'].values.tolist()
        loss_list=my_stock_performance[my_stock_performance['PROFIT_LOSS']=='LOSS'].values.tolist()
        profit=profit_list[0][1] if len(profit_list)>0 else 0 
        loss=loss_list[0][1] if len(loss_list)>0 else 0 
        daily_pnl_data['Date'] = pd.to_datetime(daily_pnl_data['Date'])
        benchmark_data['Date'] = pd.to_datetime(benchmark_data['Date'])
        benchmark_indices = {'India':['Nifty 50 Index','Nifty Smallcap 100 Index','Nifty Midcap 100 Index'],
                                'US':['S&P 500','Russell 2000','Dow Jones Industrial Average']}
        benchmark1,benchmark2,benchmark3=benchmark_indices[country][0],benchmark_indices[country][1],benchmark_indices[country][2]
        mtd_pnl_data = daily_pnl_data[daily_pnl_data['Date'].dt.month==current_date.month]
        mtd_benchmark_data =  benchmark_data[benchmark_data['Date'].dt.month == current_date.month]
        daily_pnl_data['Portfolio'] =  (1+daily_pnl_data['Portfolio_Daily']).cumprod()-1
        mtd_pnl_data['Portfolio'] = (1+mtd_pnl_data['Portfolio_Daily']).cumprod()-1
        benchmark1_cumulative,benchmark1_daily =  BenchmarkCumulativeReturns(benchmark1,benchmark_data)
        benchmark2_cumulative,benchmark2_daily =  BenchmarkCumulativeReturns(benchmark2,benchmark_data)
        benchmark3_cumulative,benchmark3_daily =  BenchmarkCumulativeReturns(benchmark3,benchmark_data)
        benchmark1_mtd_cumulative,benchmark1_daily =  BenchmarkCumulativeReturns(benchmark1,mtd_benchmark_data)
        benchmark2_mtd_cumulative,benchmark2_daily =  BenchmarkCumulativeReturns(benchmark2,mtd_benchmark_data)
        benchmark3_mtd_cumulative,benchmark3_daily =  BenchmarkCumulativeReturns(benchmark3,mtd_benchmark_data)
        itd_cumulative_data = pd.merge(daily_pnl_data[['Date','Portfolio']],benchmark1_cumulative,on='Date',how='inner')
        itd_cumulative_data = pd.merge(itd_cumulative_data,benchmark2_cumulative,on='Date',how='inner')
        itd_cumulative_data = pd.merge(itd_cumulative_data,benchmark3_cumulative,on='Date',how='inner')
        mtd_cumulative_data = pd.merge(mtd_pnl_data[['Date','Portfolio']],benchmark1_mtd_cumulative,on='Date',how='inner')
        mtd_cumulative_data = pd.merge(mtd_cumulative_data,benchmark2_mtd_cumulative,on='Date',how='inner')
        mtd_cumulative_data = pd.merge(mtd_cumulative_data,benchmark3_mtd_cumulative,on='Date',how='inner')
        output=pd.DataFrame(columns=['name','lineWidth','color','data'])
        final_dict = {'names':list(itd_cumulative_data.columns)[1:],'lineWidth':[4,2,2,2],
                        'color':['#002060','#FDCA40','#00A878','#9BC53D']}
        itd_cumulative_data['Date'] = itd_cumulative_data['Date'].astype(str)                   
        for i in range(0,4):
            output.loc[i]=[final_dict['names'][i],final_dict['lineWidth'][i],final_dict['color'][i],itd_cumulative_data[['Date',final_dict['names'][i]]].values.tolist()]
        output_cumulative_returns=output.to_dict(orient='records')        
        itd_output_list = {'date':min_start_date.strftime('%Y-%b-%d'),'Portfolio':itd_cumulative_data.loc[len(itd_cumulative_data)-1,'Portfolio'],benchmark1:itd_cumulative_data.loc[len(itd_cumulative_data)-1,benchmark1],
                            benchmark2:itd_cumulative_data.loc[len(itd_cumulative_data)-1,benchmark2],benchmark3:itd_cumulative_data.loc[len(itd_cumulative_data)-1,benchmark3]}
        mtd_output_list = {'date':current_date.replace(day=1).strftime('%Y-%b-%d'),'Portfolio':mtd_cumulative_data.loc[len(mtd_cumulative_data)-1,'Portfolio'],benchmark1:mtd_cumulative_data.loc[len(mtd_cumulative_data)-1,benchmark1],
                            benchmark2:mtd_cumulative_data.loc[len(mtd_cumulative_data)-1,benchmark2],benchmark3:mtd_cumulative_data.loc[len(mtd_cumulative_data)-1,benchmark3]}
        displayTableColumnsPerformanceTable=['date','Portfolio',benchmark1,benchmark2,benchmark3]
        performanceHitIndia=[
            {'name':'BUY', 'signalscount': '47', 'win': '89%', 'avggain': '29.2%','avgloss':'-5.4%','overallgain':'52.66%'},
            {'name':'SELL', 'signalscount': '-', 'win': '-', 'avggain': '-','avgloss':'-','overallgain':'-'},
            {'name':'TOTAL', 'signalscount': '47', 'win': '89%', 'avggain': '29.2%','avgloss':'-5.4%','overallgain':'52.66%'}]
        performanceHitUS= [{'name':'BUY', 'signalscount': '26', 'win': '62%', 'avggain': '20.7%','avgloss':'-6.0%','overallgain':'10.88%'},
        {'name':'SELL', 'signalscount': '-', 'win': '-', 'avggain': '-','avgloss':'-','overallgain':'-'},
        {'name':'TOTAL','signalscount': '26', 'win': '62%', 'avggain': '20.7%','avgloss':'-6.0%','overallgain':'10.88%'}]
        context={'current_portfolio_holdings':portfolio_holdings,'transactions':transactions,'equity_valuation':equity_valuation,'equity_investment':equity_investment,
        'profit_loss':current_holdings_profit_loss,'profit_loss_day':profit_loss_day,'portfolio_highlights_cash':portfolio_highlights_cash,'portfolio_highlights_realized':portfolio_highlights_realized,
        'portfolio_top_exposure':portfolio_top_exposure.to_dict(orient='records'),'portfolio_highlights_mps':portfolio_highlights_mps.to_dict(orient='records'),
        'portfolio_highlights_lps':portfolio_highlights_lps.to_dict(orient='records'),'max_gainer':max_gainer.to_dict(orient='records'),'max_loser':max_loser.to_dict(orient='records'),
        'equity_cashflow':equity_cashflow_data,'equity_cashflow_categories':equity_cashflow_categories,'output_cumulative':output_cumulative_returns,'allocation_list':allocation_list,
        'profit':profit,'loss':loss,'performanceHitIndia':performanceHitIndia,'performanceHitUS':performanceHitUS,'itd_mtd_list':[itd_output_list,mtd_output_list],
        'displayTableColumnsPerformanceTable':displayTableColumnsPerformanceTable,'performance_date':performance_date.strftime('%Y-%b-%d'),'current_date':current_date.strftime('%Y-%b-%d'),'performanceHitDate':current_date.replace(day=1).strftime('%Y-%b-%d')}
        return Response(context)


        
        context ={'stocksCount':len(indexWiseStocksDF), 'stocks':indexWiseStocksDF.to_dict("records"),'sectorwisestockscount':sectorwisestockscount,
                    'marketcapwisestockscount':marketcapwisestockscount, 'selectedIndex':selectedIndex}
        conn.close()
        return Response(context)
    

class PortfolioCreationWizardViewSet(viewsets.ViewSet):
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]
    # authentication_classes = []
    # permission_classes = []

    queryset = UniversePricesMain.objects.none()

    def list(self, request, *args, **kwargs):
        country = kwargs['country']
        selectedIndex = kwargs['index']
        request.session['wizardindex'] = selectedIndex
        tableObjects={'India':['wizardTable',],'US':['US_wizardTable']}
        conn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server}; SERVER='+SERVER+'; PORT='+PORT+'; DATABASE='+DATABASE+';UID='+UID+';PWD='+PWD+';')
        wizardTable=tableObjects[country][0]
        indexWiseStocksDF = pd.read_sql("select Company, gics, [Market Cap], Market_Cap_Category,Current_Price,[Security Code] Security_Code from "+wizardTable+" where index_name='"+selectedIndex+"'",conn)
        logos_data = pd.read_sql("select Code Security_Code,IMGURL from Logos where country='"+country+"'",sql_conn)
        indexWiseStocksDF = pd.merge(indexWiseStocksDF,logos_data,on='Security_Code',how='inner')
        if country =='India':
            indices=list(pd.read_sql('''select distinct index_name from Indexconstituents where flag='yes' union 
                                    select distinct index_name from IndexConstituents_bse where flag='yes' ''',conn)['index_name'])
        else:
            indices=list(pd.read_sql('''select distinct index_name from Us_Indexconstituents where flag='yes' ''',conn)['index_name'])
            indexWiseStocksDF.rename(columns={'GICS':'gics'},inplace=True)
        indexWiseStocksDF.fillna(0, inplace=True)
        sectorwisestockscount = pd.read_sql("select gics as Sector,count(gics) as OfStocks from "+wizardTable+"  where index_name='"+selectedIndex+"' group by gics",conn).to_dict(orient="records")
        marketcapwisestockscount = pd.read_sql("select Market_Cap_Category as MarketCap,count(Market_Cap_Category) as OfStocks from "+wizardTable+"  where index_name='"+selectedIndex+"' group by Market_Cap_Category",conn).to_dict(orient="records")
        indexWiseStocksDF.columns = ['Company', 'Sector', 'Market Cap', 'Market Cap Category', 'Price','Security_Code', 'IMGURL']
        context ={'stocksCount':len(indexWiseStocksDF), 'stockDatasource':indexWiseStocksDF.to_dict("records"),'sectorwisestockscount':sectorwisestockscount,
                    'marketcapwisestockscount':marketcapwisestockscount, 'indices':indices, 'selectedIndex':selectedIndex}
        conn.close()
        return Response(context)
    
    #Fundamental Filtering
    @action(methods=['post'], detail=True, permission_classes=[],
            url_path='fundamental', url_name='fundamental')
    def fundamental_filtering(self, request, *args, **kwargs):
        country = kwargs['country']
        selectedIndex = kwargs['index']
        fundamentalFilters = json.loads(request.body.decode('utf-8'))
        filter_condition = ''
        for filter in fundamentalFilters:
            if fundamentalFilters[filter] is not None:
                filter_condition += ' and '+filter+' >= '+str(fundamentalFilters[filter])
        tableObjects={'India':['wizardTable'],'US':['US_wizardTable']}
        conn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server}; SERVER='+SERVER+'; PORT='+PORT+'; DATABASE='+DATABASE+';UID='+UID+';PWD='+PWD+';')
        wizardTable=tableObjects[country][0]
        if filter_condition is not None:
            indexWiseStocksDF = pd.read_sql("SELECT * FROM "+wizardTable+" WHERE INDEX_NAME='"+selectedIndex+"' "+filter_condition+" ",conn)
        indexWiseStocksDF = indexWiseStocksDF.fillna(0)
        if country=='US':
            indexWiseStocksDF.rename(columns={'GICS':'gics'},inplace=True)
        sectorwisestockscount = pd.read_sql("select gics as Sector,count(gics) as OfStocks from "+wizardTable+"  where index_name='"+selectedIndex+"'" +filter_condition+"  group by gics",conn).to_dict(orient="records")
        marketcapwisestockscount = pd.read_sql("select Market_Cap_Category as MarketCap,count(Market_Cap_Category) as OfStocks from "+wizardTable+"  where index_name='"+selectedIndex+"'"+filter_condition+" group by Market_Cap_Category",conn).to_dict(orient="records")
        indexWiseStocksDF = indexWiseStocksDF[['Company', 'gics', 'Market Cap', 'Market_Cap_Category','Current_Price','Security Code']]
        indexWiseStocksDF.columns = ['Company','Sector', 'Market Cap', 'Market Cap Category', 'Price','Security_Code' ]
        logos_data = pd.read_sql("select Code Security_Code,IMGURL from Logos where country='"+country+"'",sql_conn)
        indexWiseStocksDF = pd.merge(indexWiseStocksDF,logos_data,on='Security_Code',how='inner')
        indexWiseStocksDF.columns = ['Company', 'Sector', 'Market Cap', 'Market Cap Category', 'Price','Security_Code','IMGURL']

        context ={'stocksCount':len(indexWiseStocksDF), 'stockDatasource':indexWiseStocksDF.to_dict("records"),'sectorwisestockscount':sectorwisestockscount,
                    'marketcapwisestockscount':marketcapwisestockscount, 'selectedIndex':selectedIndex}
        conn.close()
        return Response(context)

    
    #Technical Filtering
    @action(methods=['post'], detail=True, permission_classes=[],
            url_path='technical', url_name='technical')
    def technical_filtering(self, request, *args, **kwargs):
        country = kwargs['country']
        selectedIndex = kwargs['index']
        technicalFilters = json.loads(request.body.decode('utf-8'))
        filter_condition = ''
        for filter in technicalFilters:
            if technicalFilters[filter] is not None:
                if filter == 'Current_Price':
                    filter_condition += ' and Current_Price >= '+str(float(float(technicalFilters[filter])/100))+'*[52_Week_High]'
                elif filter == 'MovingAvg':
                    if technicalFilters[filter]:
                        filter_condition += ' and MA9 > MA26'
                else:
                    filter_condition += ' and '+filter+' >= '+str(technicalFilters[filter])
        tableObjects={'India':['wizardTable'],'US':['US_wizardTable']}
        conn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server}; SERVER='+SERVER+'; PORT='+PORT+'; DATABASE='+DATABASE+';UID='+UID+';PWD='+PWD+';')
        wizardTable=tableObjects[country][0]   
        indexWiseStocksDF = pd.read_sql("SELECT * FROM "+wizardTable+" WHERE INDEX_NAME='"+selectedIndex+"' "+filter_condition+" ",conn)
        indexWiseStocksDF = indexWiseStocksDF.fillna(0)
        if country=='US':
            indexWiseStocksDF.rename(columns={'GICS':'gics'},inplace=True)
        sectorwisestockscount = pd.read_sql("select gics as Sector,count(gics) as OfStocks from "+wizardTable+"  where index_name='"+selectedIndex+"'" +filter_condition+"  group by gics",conn).to_dict(orient="records")
        marketcapwisestockscount = pd.read_sql("select Market_Cap_Category as MarketCap,count(Market_Cap_Category) as OfStocks from "+wizardTable+"  where index_name='"+selectedIndex+"'"+filter_condition+" group by Market_Cap_Category",conn).to_dict(orient="records")
        indexWiseStocksDF = indexWiseStocksDF[['Company', 'gics', 'Market Cap', 'Market_Cap_Category','Current_Price','Security Code']]
        indexWiseStocksDF.columns = ['Company','Sector', 'Market Cap', 'Market Cap Category', 'Price','Security_Code']
        logos_data = pd.read_sql("select Code Security_Code,IMGURL from Logos where country='"+country+"'",sql_conn)
        indexWiseStocksDF = pd.merge(indexWiseStocksDF,logos_data,on='Security_Code',how='inner')
        indexWiseStocksDF.columns = ['Company', 'Sector', 'Market Cap', 'Market Cap Category', 'Price','Security_Code','IMGURL']
        context ={'stocksCount':len(indexWiseStocksDF), 'stockDatasource':indexWiseStocksDF.to_dict("records"),'sectorwisestockscount':sectorwisestockscount,
                    'marketcapwisestockscount':marketcapwisestockscount, 'selectedIndex':selectedIndex}
        conn.close()
        return Response(context)

    
    #Quant Overlay Filtering
    @action(methods=['post'], detail=True, permission_classes=[],
            url_path='quant_overlay', url_name='quant_overlay')
    def quant_overlay_filtering(self, request, *args, **kwargs):
        country = kwargs['country']
        selectedIndex = kwargs['index']
        tableObjects={'India':['wizardTable'],'US':['US_wizardTable']}
        filters = json.loads(request.body.decode('utf-8'))
        oldFilters = filters['oldfilters']
        quantFilters = filters['quantfilters']
        filter_condition= ''
        conn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server}; SERVER='+SERVER+'; PORT='+PORT+'; DATABASE='+DATABASE+';UID='+UID+';PWD='+PWD+';')
        quantCoulumn,filterstocks,matches,stocks ='','',[],[]
        for filter in quantFilters:
            if quantFilters[filter]:
                matches.append(filter)
        quantCoulumn =', '.join('"{0}"'.format(w) for w in [matches[i].replace("'","''") for i in range (0,len(matches))] ).replace('"',"'")
        for filter in oldFilters:
            if oldFilters[filter] is not None:
                if filter == 'Current_Price':
                    filter_condition += ' and Current_Price >= '+str(float(float(oldFilters[filter])/100))+'*[52_Week_High]'
                elif filter == 'MovingAvg':
                    if oldFilters[filter]:
                        filter_condition += ' and MA9 > MA26'
                elif filter not in ['BottomTier', 'MidTier', 'TopTier']:
                    filter_condition += ' and '+filter+' >= '+str(oldFilters[filter])
        if quantCoulumn != '':
            indexWiseStocksDF = pd.read_sql("SELECT * FROM "+tableObjects[country][0]+" WHERE INDEX_NAME='"+selectedIndex+"' "+filter_condition+" and (Tier in ("+quantCoulumn+") or Tier is null )",conn)
        else:
            indexWiseStocksDF = pd.read_sql("SELECT * FROM "+tableObjects[country][0]+" WHERE INDEX_NAME='"+selectedIndex+"' "+filter_condition+" ",conn)
        indexWiseStocksDF = indexWiseStocksDF.fillna(0)
        if country=='US':
            indexWiseStocksDF.rename(columns={'GICS':'gics'},inplace=True)
        if quantCoulumn!='':
            sectorwisestockscount = pd.read_sql("select gics as Sector,count(gics) as OfStocks from "+tableObjects[country][0]+"  where index_name='"+selectedIndex+"'" +filter_condition+"  and (Tier in ("+quantCoulumn+") or Tier is null ) group by gics",conn).to_dict(orient="records")
            marketcapwisestockscount = pd.read_sql("select Market_Cap_Category as MarketCap,count(Market_Cap_Category) as OfStocks from "+tableObjects[country][0]+"  where index_name='"+selectedIndex+"'"+filter_condition+"  and (Tier in ("+quantCoulumn+") or Tier is null ) group by Market_Cap_Category",conn).to_dict(orient="records")
        else:
            sectorwisestockscount = pd.read_sql("select gics as Sector,count(gics) as OfStocks from "+tableObjects[country][0]+"  where index_name='"+selectedIndex+"'" +filter_condition+"  group by gics",conn).to_dict(orient="records")
            marketcapwisestockscount = pd.read_sql("select Market_Cap_Category as MarketCap,count(Market_Cap_Category) as OfStocks from "+tableObjects[country][0]+"  where index_name='"+selectedIndex+"'"+filter_condition+" group by Market_Cap_Category",conn).to_dict(orient="records")
        indexWiseStocksDF = indexWiseStocksDF[['Company', 'gics', 'Market Cap', 'Market_Cap_Category','Current_Price','Security Code']]
        indexWiseStocksDF.columns = ['Company','Sector', 'Market Cap', 'Market Cap Category', 'Price','Security_Code' ]
        logos_data = pd.read_sql("select Code Security_Code,IMGURL from Logos where country='"+country+"'",sql_conn)
        indexWiseStocksDF = pd.merge(indexWiseStocksDF,logos_data,on='Security_Code',how='inner')
        indexWiseStocksDF.columns = ['Company', 'Sector', 'Market Cap', 'Market Cap Category', 'Price','Security_Code','IMGURL']
        context ={'stocksCount':len(indexWiseStocksDF), 'stockDatasource':indexWiseStocksDF.to_dict("records"),'sectorwisestockscount':sectorwisestockscount,
                    'marketcapwisestockscount':marketcapwisestockscount, 'selectedIndex':selectedIndex}
        conn.close()
        return Response(context)



    #Stock Summary Calculation
    @action(methods=['post'], detail=True, permission_classes=[],
            url_path='summary', url_name='summary')
    def stocks_summary(self, request, *args, **kwargs):
        country = kwargs['country']
        selectedIndex = kwargs['index']
        requiredObjects={'India':['wizardTable',UniversePricesMain,SecurityMaster,MarketIndex,RiskFreeRate,'Universe_Prices_Main','Market_Index',200000,'₹','GICS_Classification'],
                        'US':['US_wizardTable',UsUniversePricesMain,UsSecurityMaster,UsMarketIndex,UsRiskFreeRate,'US_Universe_Prices_Main','US_Market_Index', 5000,'$','US_GICS_Classification']}
        filters = json.loads(request.body.decode('utf-8'))
        portfolio_name = "wizardPortfolio"
        oldFilters = filters['oldfilters']
        quantFilters = filters['quantfilters']
        filter_condition= ''
        conn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server}; SERVER='+SERVER+'; PORT='+PORT+'; DATABASE='+DATABASE+';UID='+UID+';PWD='+PWD+';')
        quantCoulumn,filterstocks,matches,stocks ='','',[],[]
        for filter in quantFilters:
            if quantFilters[filter]:
                matches.append(filter)
        quantCoulumn =', '.join('"{0}"'.format(w) for w in [matches[i].replace("'","''") for i in range (0,len(matches))] ).replace('"',"'")
        for filter in oldFilters:
            if oldFilters[filter] is not None:
                if filter == 'Current_Price':
                    filter_condition += ' and Current_Price >= '+str(float(float(oldFilters[filter])/100))+'*[52_Week_High]'
                elif filter == 'MovingAvg':
                    if oldFilters[filter]:
                        filter_condition += ' and MA9 > MA26'
                elif filter not in ['BottomTier', 'MidTier', 'TopTier']:
                    filter_condition += ' and '+filter+' >= '+str(oldFilters[filter])
        if quantCoulumn != '':
            indexWiseStocksDF = pd.read_sql("SELECT * FROM "+requiredObjects[country][0]+" WHERE INDEX_NAME='"+selectedIndex+"' "+filter_condition+" and (Tier in ("+quantCoulumn+") or Tier is null )",conn)
        else:
            indexWiseStocksDF = pd.read_sql("SELECT * FROM "+requiredObjects[country][0]+" WHERE INDEX_NAME='"+selectedIndex+"' "+filter_condition+" ",conn)
        indexWiseStocksDF = indexWiseStocksDF.fillna(0)
        if country=='US':
            indexWiseStocksDF.rename(columns={'GICS':'gics'},inplace=True)
        list_of_tickers = list(indexWiseStocksDF['Factset_Ticker'].unique())
        list_of_tickers_query = ', '.join('"{0}"'.format(w) for w in [list_of_tickers[i].replace("'","''") for i in range (0,len(list_of_tickers))] ).replace('"',"'")
        period = '12M'
        table_headerlist = {'India':['PNL','Nifty 50 Index','Nifty Smallcap 100 Index','Nifty Midcap 100 Index'] ,
                            'US':['PNL','S&P 500','Dow Jones Industrial Average','Russell 2000']}
        sec_factors = pd.DataFrame(list(requiredObjects[country][2].objects.filter(fs_ticker__in=list_of_tickers).values('fs_name','fs_ticker','security_code','gics','market_cap_category').order_by('fs_name')))
        sec_factors.columns =['Company','Factset_Ticker','Security_Code','Sector','MarketCapCategory']
        current_date= requiredObjects[country][1].objects.all().aggregate(Max('date'))['date__max']
        dates = {'YTD':current_date.replace(day=1,month=1),'MTD':current_date.replace(month=1,day=1),'QTD':current_date.replace(month = (((current_date.month-1)//3)*3)+1,day=1),
                '1M':current_date+timedelta(-30),'3M':current_date+timedelta(-90),'12M':current_date+timedelta(-365),'2019':current_date.replace(day=1,year=2019,month=1),
                    '2020':current_date.replace(year=2020,day=1,month=1)}
        start_date = dates[period]
        prev_start_date = requiredObjects[country][1].objects.filter(date__lt=str(start_date)).all().aggregate(Max('date'))['date__max']
        sql_conn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server}; SERVER='+SERVER+'; PORT='+PORT+'; DATABASE='+DATABASE+';UID='+UID+';PWD='+PWD+';')         
        stocks_data = pd.read_sql("select Company,a.Factset_Ticker,Price from " + requiredObjects[country][5] + " a join (select c.factset_ticker,max(date) MaxDate from " + requiredObjects[country][5] + " c where date>='2019-01-01' and c.factset_ticker in (" + list_of_tickers_query + ") group by c.factset_ticker) b on a.date=b.MaxDate and a.factset_ticker = b.factset_ticker",sql_conn)
        stocks_data.dropna(inplace=True)
        logos_data = pd.read_sql("select Code Security_Code,IMGURL from Logos where country='"+country+"'",sql_conn)
        stocks_data['Quantity'] = (requiredObjects[country][7]/stocks_data['Price']).astype(int)        
        total_prices_data = pd.read_sql("select Company,Factset_Ticker,Date,Price,[Return] from " + requiredObjects[country][5] + " where date between '" + str(prev_start_date) + "' and '" + str(current_date) + "' and factset_ticker in (" + list_of_tickers_query +")  order by company,date",sql_conn)
        total_index_data = pd.read_sql("select Date,Company,[Return] from " + requiredObjects[country][6] + " where date between '" + str(prev_start_date) + "' and '" + str(current_date) + "' order by company,date",sql_conn)           

        prices_data = total_prices_data[total_prices_data['Date']>=prev_start_date]
        market_index_data = total_index_data[total_index_data['Date']>=prev_start_date]
        risk_free_rate_data = pd.DataFrame(list(requiredObjects[country][4].objects.filter(date__range=(start_date, current_date)).order_by('date').values('date','risk_free_rate')))
        risk_free_rate_data.columns = ['Date','Risk Free Rate']
        #Daily and Monthly Cumulative calculations
        cumulative_returns,daily_returns,metrics_table_data=CalcsMain(country,stocks_data,prices_data,market_index_data,risk_free_rate_data,portfolio_name,period)
        cumulative_returns.dropna(inplace=True)
        cumulative_returns['Date'] = cumulative_returns['Date'].astype(str)
        output=pd.DataFrame(columns=['name','lineWidth','color','data'])
        final_dict = {'names':list(cumulative_returns.columns)[1:],'lineWidth':[3,2,2,2,2],
                        'color':['#002060','#00A8E8','#FDCA40','#00A878','#9BC53D']}
        for i in range(0,5):
            output.loc[i]=[final_dict['names'][i],final_dict['lineWidth'][i],final_dict['color'][i],cumulative_returns[['Date',final_dict['names'][i]]].values.tolist()]
        output_cumulative_returns=output.to_dict(orient='records')        
        #Returns by stock and sector
        gics_mcap_data = sec_factors[['Company','Factset_Ticker','Sector','MarketCapCategory']]
        gics_mcap_data.columns =['Company','Factset_Ticker','GICS','Market_Cap_Category']
        stock_wise_data,sector_wise_data = Stocks_Investment_Returns(prices_data,stocks_data,gics_mcap_data,period) 
        mcap_df =  pd.merge(stock_wise_data,gics_mcap_data[['Factset_Ticker','Market_Cap_Category']],on='Factset_Ticker',how='inner')
        mcap_df = mcap_df[['Company','Factset_Ticker','Market_Cap_Category','Current Exposure']]
        mcap_df = mcap_df.groupby('Market_Cap_Category')['Current Exposure'].sum().reset_index()
        stock_wise_data = pd.merge(stock_wise_data,sec_factors[['Factset_Ticker','Security_Code']],on='Factset_Ticker',how='inner')
        stock_wise_data = pd.merge(stock_wise_data,logos_data,on='Security_Code',how='left')
        stock_wise_data = stock_wise_data[['Company','Current Exposure','12M_Return','12M_PNL','Security_Code','IMGURL']]
        stock_wise_data['Current Exposure'] = stock_wise_data['Current Exposure'].astype(int)
        stock_wise_data['12M_PNL'] = stock_wise_data['12M_PNL'].astype(int)
        stock_wise_data['12M_Return'] = round(stock_wise_data['12M_Return']*100,1)
        stock_wise_data.columns = ['Company', 'Investment Amount', 'Annual Return', 'Annual_Return','Security_Code','IMGURL']
        if quantCoulumn!='':
            sectorwisestockscount = pd.read_sql("select gics as Sector,count(gics) as OfStocks from "+requiredObjects[country][0]+"  where index_name='"+selectedIndex+"'" +filter_condition+"  and (Tier in ("+quantCoulumn+") or Tier is null ) group by gics",conn).to_dict(orient="records")
            marketcapwisestockscount = pd.read_sql("select Market_Cap_Category as MarketCap,count(Market_Cap_Category) as OfStocks from "+requiredObjects[country][0]+"  where index_name='"+selectedIndex+"'"+filter_condition+"  and (Tier in ("+quantCoulumn+") or Tier is null ) group by Market_Cap_Category",conn).to_dict(orient="records")
        else:
            sectorwisestockscount = pd.read_sql("select gics as Sector,count(gics) as OfStocks from "+requiredObjects[country][0]+"  where index_name='"+selectedIndex+"'" +filter_condition+"  group by gics",conn).to_dict(orient="records")
            marketcapwisestockscount = pd.read_sql("select Market_Cap_Category as MarketCap,count(Market_Cap_Category) as OfStocks from "+requiredObjects[country][0]+"  where index_name='"+selectedIndex+"'"+filter_condition+" group by Market_Cap_Category",conn).to_dict(orient="records")
        indexWiseStocksDF = indexWiseStocksDF[['Company', 'gics', 'Market Cap', 'Market_Cap_Category','Current_Price','Security Code']]
        indexWiseStocksDF.columns = ['Company','Sector', 'Market Cap', 'Market Cap Category', 'Price','Security_Code' ]
        metrics_table_data = pd.DataFrame(metrics_table_data).set_index('Metric/Portfolio').T
        metrics_table_data = pd.DataFrame(metrics_table_data).reset_index()
        metrics_table_data = metrics_table_data[['index','Cumulative Return','Annualized Volatility','Sharpe','Maximum Drawdown']]
        metrics_table_data['Cumulative Return'] = metrics_table_data['Cumulative Return'].map(lambda x: x.replace('%',''))
        metrics_table_data['Cumulative Return'] = round(metrics_table_data['Cumulative Return'].astype(float),1)
        metrics_table_data['Annualized Volatility'] = metrics_table_data['Annualized Volatility'].map(lambda x: x.replace('%',''))
        metrics_table_data['Annualized Volatility'] = round(metrics_table_data['Annualized Volatility'].astype(float),1)
        metrics_table_data['Maximum Drawdown'] = metrics_table_data['Maximum Drawdown'].map(lambda x: x.replace('%',''))
        metrics_table_data['Maximum Drawdown'] = round(metrics_table_data['Maximum Drawdown'].astype(float),1)
        metrics_table_data.columns = ['Metric/Portfolio','Cumulative Return','Annualized Volatility','Sharpe','Maximum Drawdown']
        color_pie = ['#95CEFF','#5C5C61','#A9FF96','#FFBC75','#EFCACF','#999EFF','#FF7599','#FDEC6D','#44A9A8','#FF7474','#B0FFFA']
        sector_wise_data = sector_wise_data[['GICS','Current Exposure']]
        sector_wise_data.columns=['name','y']
        sector_wise_data["color"]= list(color_pie[:len(sector_wise_data)])
        sql_conn.close()
        context ={'stockDatasource':indexWiseStocksDF.to_dict("records"),'sectorwisestockscount':sectorwisestockscount,
                'marketcapwisestockscount':marketcapwisestockscount, 'selectedIndex':selectedIndex,'output_cumulative_return':output_cumulative_returns,
                'table_data':metrics_table_data.to_dict("records"),'topholdings':stock_wise_data.head(10).to_dict("records"),'sectorwiseinvstamnt':sector_wise_data.to_dict('records'),
                'username':request.user.username,'profile':{'Username':request.user.username,'OfStocks':len(indexWiseStocksDF),'OfSectors':len(sectorwisestockscount)},'stocksCount':len(indexWiseStocksDF)}
        return Response(context)
    
    

    #Portfolio Saving
    @action(methods=['post'], detail=True, permission_classes=[],
            url_path='portfolio_saving', url_name='portfolio_saving')
    def portfolio_saving(self, request, *args, **kwargs):
        httpResponse = {}
        httpStatus = status.HTTP_201_CREATED
        try:
            counTRY = kwargs['country']
            selectedIndex = kwargs['index']
            requestData = json.loads(request.body.decode('utf-8'))
            addToExisting = bool(requestData['addToExisting'])
            portfolio_name = requestData['portfolio_name']
            invst_amnt = requestData['invst_amnt']
            oldFilters = requestData['oldfilters']
            quantFilters = requestData['quantfilters']
            requiredObjects={'India':['wizardTable','GICS_Classification', 'Universe_Prices_Main'],
                            'US':['US_wizardTable','US_GICS_Classification', 'US_Universe_Prices_Main']}
            filter_condition= ''
            conn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server}; SERVER='+SERVER+'; PORT='+PORT+'; DATABASE='+DATABASE+';UID='+UID+';PWD='+PWD+';')
            quantCoulumn,filterstocks,matches ='','',[]
            for filter in quantFilters:
                if quantFilters[filter]:
                    matches.append(filter)
            quantCoulumn =', '.join('"{0}"'.format(w) for w in [matches[i].replace("'","''") for i in range (0,len(matches))] ).replace('"',"'")
            if quantCoulumn != '':
                quantdf = pd.read_sql("select FS_Ticker from "+requiredObjects[counTRY][1]+" where Tier in ("+quantCoulumn+") ",conn)
                quantdf.dropna(inplace=True)
                stocks = list(quantdf['FS_Ticker'].unique())
                filterstocks =', '.join('"{0}"'.format(w) for w in [stocks[i].replace("'","''") for i in range (0,len(stocks))] ).replace('"',"'")
            for filter in oldFilters:
                if oldFilters[filter] is not None:
                    if filter == 'Current_Price':
                        filter_condition += ' and Current_Price >= '+str(float(float(oldFilters[filter])/100))+'*[52_Week_High]'
                    elif filter == 'MovingAvg':
                        if oldFilters[filter]:
                            filter_condition += ' and MA9 > MA26'
                    elif filter not in ['BottomTier', 'MidTier', 'TopTier']:
                        filter_condition += ' and '+filter+' >= '+str(oldFilters[filter])
            if quantCoulumn != '':
                indexWiseStocksDF = pd.read_sql("SELECT * FROM "+requiredObjects[counTRY][0]+" WHERE INDEX_NAME='"+selectedIndex+"' "+filter_condition+" and (Tier in ("+quantCoulumn+") or Tier is null )",conn)
            else:
                indexWiseStocksDF = pd.read_sql("SELECT * FROM "+requiredObjects[counTRY][0]+" WHERE INDEX_NAME='"+selectedIndex+"' "+filter_condition+" ",conn)
            indexWiseStocksDF = indexWiseStocksDF.fillna(0)
            if counTRY=='US':
                indexWiseStocksDF.rename(columns={'GICS':'gics'},inplace=True)
            try:
                indexWiseStocksDF['Quantity']= (float(invst_amnt)/indexWiseStocksDF['Price']).astype(int)
            except Exception: #ZeroDivisionError
                indexWiseStocksDF['Quantity'] = 0
            if not indexWiseStocksDF.empty:
                if addToExisting:
                    for index, row in indexWiseStocksDF.iterrows():
                        if row['Quantity']:
                            if not CustomerPortfolioDetails.objects.filter(customer_name=request.user.username,country=counTRY, portfolio_name=portfolio_name,company_name=row['Company']).exists() :
                                CustomerPortfolioDetails(customer_name=request.user.username, portfolio_name=portfolio_name, company_name=row['Company'],fs_ticker=row['Factset_Ticker'],
                                quantity=row['Quantity'], price=row['Price'],sector_name=row['gics'],country=counTRY, model_portfolio='no', portfolio_type='General').save()
                            else:
                                stock = CustomerPortfolioDetails.objects.filter(customer_name=request.user.username, portfolio_name=portfolio_name, company_name=row['Company'],fs_ticker=row['Factset_Ticker'],
                                sector_name=row['gics'],country=counTRY, model_portfolio='no', portfolio_type='General').first()
                                old_quantity = stock.quantity
                                new_quantity = old_quantity + row['Quantity']
                                CustomerPortfolioDetails.objects.filter(customer_name=request.user.username, portfolio_name=portfolio_name, company_name=row['Company'],fs_ticker=row['Factset_Ticker'],
                                sector_name=row['gics'],country=counTRY, model_portfolio='no', portfolio_type='General').update(quantity=new_quantity)

                        else:
                            print('Invalid Quantity!!!')
                    httpResponse = {'message': 'Stocks Added Successfully!','status':'success'}
                    httpStatus = status.HTTP_201_CREATED
                else:
                    if not CustomerPortfolioDetails.objects.filter(customer_name=request.user.username,country=counTRY, portfolio_name=portfolio_name).exists():
                        for index, row in indexWiseStocksDF.iterrows():     # Iterating the stocks (if more than one stock is adding at a time)
                            if row['Quantity']:
                                if not CustomerPortfolioDetails.objects.filter(customer_name=request.user.username,country=counTRY, portfolio_name=portfolio_name,company_name=row['Company']).exists() :
                                    CustomerPortfolioDetails(customer_name=request.user.username, portfolio_name=portfolio_name, company_name=row['Company'],fs_ticker=row['Factset_Ticker'],
                                    quantity=row['Quantity'], price=row['Price'],sector_name=row['gics'],country=counTRY, model_portfolio='no', portfolio_type='General').save()
                            else:
                                print("Invalid quantity!!!")
                        httpResponse = {'message': 'Portfolio saved successfully!','status':'success'}
                        httpStatus = status.HTTP_201_CREATED                          
                    else:
                        httpResponse = {'message': 'Portfolio Name Already Taken!','status':'error'}
                        httpStatus = status.HTTP_208_ALREADY_REPORTED
            else:
                httpResponse = {'message': 'Unable to create portfolio!','status':'error'}
                httpStatus = status.HTTP_400_BAD_REQUEST
        except Exception as e:
            print(e)
            httpResponse = {'message': 'Internal server error!','status':'error'}
            httpStatus = status.HTTP_500_INTERNAL_SERVER_ERROR
        finally:
            return Response(data=httpResponse, status=httpStatus)


class FuturesAndOptionsView(APIView):
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]
    #on GET type of request
    def get(self, request, *args, **kwargs):
        country = kwargs['country']
        stockOptions = pd.DataFrame(list(FuturesAndOptions.objects.filter(country=country).values().order_by('-end_date')))
        indexOptions = pd.DataFrame(list(IndexFuturesandoptions.objects.filter(country=country).values().order_by('-end_date')))
        indexOptions.fillna('', inplace=True)
        requiredTableObjects={'India':'Security_Master','US':'US_Security_Master'}
        sql_conn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server}; SERVER='+SERVER+'; PORT='+PORT+'; DATABASE='+DATABASE+';UID='+UID+';PWD='+PWD+';')
        sec_code_data = pd.read_sql("select [fs ticker] as fs_ticker,[Security Code] as Security_Code from "+requiredTableObjects[country],sql_conn)
        logos_data = pd.read_sql("select Code 'Security_Code',IMGURL from Logos where country='"+country+"'",sql_conn)
        sql_conn.close()
        stockOptions = pd.merge(stockOptions,sec_code_data,on='fs_ticker',how='inner')
        stockOptions = pd.merge(stockOptions,logos_data,on='Security_Code',how='inner')
        stockOptions.fillna('', inplace=True)
        indexOptions.fillna('', inplace=True)
        resultStockOptions = []
        stockOptions = stockOptions.sort_values(by='end_date', ascending=False)

        for month, option_df in stockOptions.groupby('month'):
            result = {}
            result['tab'] = month
            result['startdate'] = option_df.iloc[0]['start_date']
            result['enddate'] = option_df.iloc[0]['end_date']
            result['stockdatasource'] = option_df.to_dict("records")
            try:
                result['percentage'] = round((float((len(option_df[option_df['Signal_Efficiency']=='Y'])/len(option_df))*100)),2)
            except Exception as e: #ZeroDivisionError
                pass
            resultStockOptions.append(result)
        for month, option_df in indexOptions.groupby('month'):
            for item in resultStockOptions:
                if item['tab']==month:
                    item['indexdatasource'] = option_df.to_dict("records")

        resultStockOptions = sorted(resultStockOptions, key=lambda result:result['startdate'], reverse=True)
        context = {'stockoptions':resultStockOptions}
        return Response(context)



class Personalisation(APIView):
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]
    
    def post(self, request, *args, **kwargs):
        params= json.loads(request.body.decode("utf-8"))
        etfList = params['etf_list']
        targetRisk = round(params['riskValue']/100,2)
        etfListQuery=', '.join('"{0}"'.format(w) for w in [etfList[i].replace("'","''") for i in range (0,len(etfList))] ).replace('"',"'")
        sql_conn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server}; SERVER='+SERVER+'; PORT='+PORT+'; DATABASE='+DATABASE+';UID='+UID+';PWD='+PWD+';')         
        pricesData = pd.read_sql(""" select Date,Ticker Factset_Ticker,Adjusted_Close Price from US_ETF_Prices 
                                where Ticker in ( """  + etfListQuery + """)
                                and date >='2011-01-01' order by date""",sql_conn)
        sql_conn.close()    
        pricesData['Date'] = pd.to_datetime(pricesData['Date'])    
        regularizationFactor = 0.2
        opt_weights,stats,allMethodsWeights,allMethodsStats = PersonalisationMain(pricesData,etfList,targetRisk,regularizationFactor)
        outputChartData={}
        try:
            cols = list(opt_weights.columns)[1:]
            for i in range(0,len(cols)):
                req_df = opt_weights[['ETF',cols[i]]]
                req_df.columns = ['name','y']
                outputChartData[cols[i]] = [{'type':'pie','name':cols[i],'data':req_df.to_dict(orient='records')}]
        except:
            pass
        if len(opt_weights)==0:
            print ("Optimal weights are not feasible with selected assets and risk type")
        context = {'optimal_weights':opt_weights.to_dict(orient='records'),'stats':stats.to_dict(orient='records'),'output_chart_data':outputChartData,
                    'all_methods_weights':allMethodsWeights.to_dict(orient='records'),'all_methods_stats':allMethodsStats.to_dict(orient='records')}
        return Response(context)
        # return Response({'h':'hi'})