from pages.views import PortfolioCreationWizardViewSet
from rest_framework import routers
router = routers.DefaultRouter()
router.register('step', PortfolioCreationWizardViewSet)