import pandas as pd
import pyodbc
from datetime import date
from django.db.models import Max

def analystTargetPricesData(target_data,stocks_data):
    try:
        stock = list(stocks_data['Factset_Ticker'])
        if target_data.empty:
            return target_data        
        else:
            prices_data=pd.merge(stocks_data,target_data,on=['Company','Factset_Ticker'],how='left')
            prices_data['Net Exposure']=prices_data['Current_Price']*prices_data['Quantity']
            prices_data['Investment_Amount']=prices_data['Net Exposure']/(prices_data['Net Exposure'].sum())
            prices_data['up_return']=(prices_data['Avg_Upside']/prices_data['Current_Price'])-1
            prices_data['down_return']=(prices_data['Avg_Downside']/prices_data['Current_Price'])-1
            prices_data['up_down_ratio']=prices_data['up_return']/abs(prices_data['down_return'])
            prices_data=prices_data[['Company','Factset_Ticker', 'Current_Price','Investment_Amount','Avg_Upside',  'Avg_Downside',  '52_Week_High',  '52_Week_Low','up_return',  'down_return' , 'up_down_ratio']]
            prices_data.fillna(0,inplace=True)
            return prices_data
    except Exception as e:
        emptyDataFrame=pd.DataFrame()
        return emptyDataFrame