# import warnings
# warnings.filterwarnings("ignore")
# import pandas as pd
# import pyodbc
# import numpy as np
# from pypfopt import EfficientFrontier,EfficientSemivariance,risk_models,expected_returns,objective_functions

# def LedoitShrinkangeRegularization(historicalPricesData,riskType,targetRisk,regularizationFactor):
#     try:
#         prices = pd.pivot(historicalPricesData,index='Date',values='Price',columns='Factset_Ticker')
#         mu = expected_returns.mean_historical_return(prices)
#         S = risk_models.CovarianceShrinkage(prices).ledoit_wolf(shrinkage_target='constant_correlation')          
#         ef = EfficientFrontier(mu, S)
#         ef.add_objective(objective_functions.L2_reg, gamma=regularizationFactor)        
#         opt_weights = ef.efficient_risk(targetRisk)
#         opt_weights_df = pd.DataFrame([opt_weights], columns=opt_weights.keys())
#         opt_weights_df = opt_weights_df.multiply(100).round(2)
#         opt_weights_df = opt_weights_df.T.reset_index()
#         opt_weights_df.columns = ['ETF',riskType]
#         statsList = list(ef.portfolio_performance())
#         return opt_weights_df,statsList
#     except Exception as e:
#         print(e)
# def LedoitShrinkangeRegularizationAllMethods(historicalPricesData,methodName,regularizationFactor,riskAversion=0.5):
#     try:
#         prices = pd.pivot(historicalPricesData,index='Date',values='Price',columns='Factset_Ticker')
#         mu = expected_returns.mean_historical_return(prices)
#         S = risk_models.CovarianceShrinkage(prices).ledoit_wolf(shrinkage_target='constant_correlation')          
#         ef = EfficientFrontier(mu, S)
#         ef.add_objective(objective_functions.L2_reg, gamma=regularizationFactor)        
#         if methodName=='Minimum Volatility':
#             opt_weights = ef.min_volatility()
#         elif methodName =='Maximum Sharpe':
#             opt_weights = ef.max_sharpe()
#         elif methodName =='Max Quadratic Utility':
#             opt_weights = ef.max_quadratic_utility(riskAversion)
#         opt_weights_df = pd.DataFrame([opt_weights], columns=opt_weights.keys())
#         opt_weights_df = opt_weights_df.multiply(100).round(2)
#         opt_weights_df = opt_weights_df.T.reset_index()
#         opt_weights_df.columns = ['ETF',methodName]
#         statsList = list(ef.portfolio_performance())
#         return opt_weights_df,statsList
#     except Exception as e:
#         print(e)        
# def PersonalisationMain(pricesData,etfList,targetRisk,regularizationFactor):
#     try:
#         allMethodsData = pd.DataFrame()
#         allMethodsStatsData = pd.DataFrame()
#         riskTypes = {'Risk Averse':0.06,'Conservative':0.09,'Moderate':0.11,'Aggressive':0.14,'Very Aggressive':0.16}
#         if targetRisk>=0 and targetRisk<=0.06:
#             riskTypes['Risk Averse'] = targetRisk
#         elif targetRisk>=0.07 and targetRisk<=0.09:
#             riskTypes['Conservative'] = targetRisk
#         elif targetRisk>=0.10 and targetRisk<=0.11:
#             riskTypes['Moderate'] = targetRisk
#         elif targetRisk>=0.12 and targetRisk<=0.14:
#             riskTypes['Aggressive'] = targetRisk
#         else:
#             riskTypes['Very Aggressive'] = targetRisk
#         methodNames = ['Minimum Volatility','Maximum Sharpe','Max Quadratic Utility']
#         #Calling LedoitShrinkangeRegularization function
#         statsListDf = pd.DataFrame()
#         statsListDf['Metric'] = ['Expected Return','Annual Volatility','Sharpe']
#         allMethodsStatsData['Metric'] = ['Expected Return','Annual Volatility','Sharpe']
#         finalWeights = pd.DataFrame()
#         finalWeights['ETF'] = etfList
#         for riskType in riskTypes:
#             opt_weights,statsList = pd.DataFrame(),[]
#             try:
#                 opt_weights,statsList = LedoitShrinkangeRegularization(pricesData,riskType,riskTypes[riskType],regularizationFactor)
#             except:
#                 print("Not feasible for " , riskType)
#             if len(opt_weights)>0:
#                 finalWeights = pd.merge(finalWeights,opt_weights,on='ETF',how='inner')
#                 statsListDf[riskType] =  [ str(round(statsList[i]*100,2))+"%" if i!=2 else round(statsList[i],2) for i in range(0,len(statsList))]                
#             else:
#                 finalWeights[riskType] = [0 for elem in etfList]
#                 statsList = [0.00,riskTypes[riskType],0.00]
#                 statsListDf[riskType] =  [ str(round(statsList[i]*100,2))+"%" if i!=2 else round(statsList[i],2) for i in range(0,len(statsList))]                
#             if riskType == 'Very Aggressive':
#                 allMethodsData = opt_weights
#                 allMethodsData.columns =['ETF','Target Risk (16%)']
#                 allMethodsStatsData['Target Risk (16%)'] = statsListDf[riskType] 
#         for method in methodNames:
#             optimal_weights,statsList = LedoitShrinkangeRegularizationAllMethods(pricesData,method,regularizationFactor)
#             allMethodsData = pd.merge(allMethodsData ,optimal_weights,on='ETF',how='inner')
#             allMethodsStatsData[method] =  [ str(round(statsList[i]*100,2))+"%" if i!=2 else round(statsList[i],2) for i in range(0,len(statsList))]
#         return finalWeights,statsListDf,allMethodsData,allMethodsStatsData
#     except Exception as e:
#         print(e)
#         return pd.DataFrame(),pd.DataFrame(),pd.DataFrame(),pd.DataFrame()