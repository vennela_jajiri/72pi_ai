from django.urls import path, include
from rest_framework.urlpatterns import format_suffix_patterns
from pages import views
from .routers import router


urlpatterns = [
    path('<str:country>/create-portfolio-selecting-stocks/', views.CreatePortfolioSelectingStocks.as_view()),
    path('<str:country>/market/', views.MarketView.as_view()),
    path('<str:country>/etfmonitor/', views.ETFMonitorView.as_view()),
    path('<str:country>/modelportfolios/', views.ModelPortfolios.as_view()),
    path('<str:country>/viewmodelportfolios/',views.ViewModelPortfolioData.as_view()),
    path('<str:country>/modelportfoliosreturns/', views.ModelPortfoliosReturnView.as_view()),
    path('<str:country>/Analyzer/valuationMultiples/',views.ValuationMultiples.as_view()),
    path('<str:country>/Analyzer/factorAnalysis/',views.FactorAnalysis.as_view()),
    path('PortfoliosList/',views.GetPortfolios.as_view()),
    path('<str:country>/Analyzer/analystTargetPrice/', views.AnalystTargetPrice.as_view()),
    path('<str:country>/Analyzer/volume-volatility/',views.VolumeVolatilityMA.as_view()),
    path('<str:country>/ourmarketview/', views.OurMarketView.as_view()),
    path('<str:country>/StockInfo/',views.StockInfoView.as_view()),
    path('<str:country>/StockListSearch/',views.StockListSearch.as_view()),
    path('<str:country>/ourportfolio/', views.OurPortfolioView.as_view()),
    path('<str:country>/risk-overview/', views.RiskOverview.as_view()),
    path('<str:country>/StockChart/',views.StocksCharts.as_view()),
    path('<str:country>/screener',views.Screener.as_view()),
    path('<str:country>/portfolioReturns/', views.PortfolioReturns.as_view()),
    path('<str:country>/treemap/', views.Treemap.as_view()),
    path('dailymarket/', views.DailyMarketData.as_view()),
    path('<str:country>/dashboardSummary/', views.DashboardSummary.as_view()),
    path('<str:country>/technicalIndicators/', views.TechnicalIndicators.as_view()),
    path('<str:country>/ourperformance/', views.OurPerformance.as_view()),
    path('<str:country>/wizard/<str:index>/',include(router.urls)),
    path('<str:country>/futures-options/', views.FuturesAndOptionsView.as_view()),
    path('<str:country>/personalisation/', views.Personalisation.as_view()),
]