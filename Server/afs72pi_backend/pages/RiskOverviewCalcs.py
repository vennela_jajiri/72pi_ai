
import pandas as pd
import statistics
import pyodbc
from datetime import date,timedelta
import datetime
from scipy import stats
import math
from django.db.models import Avg, Max, Min, Sum
import numpy as np
from .PortfolioReturns import *

def riskPortfolioCalcs(stocks_data,prices_data,var_prices_data,benchmark_data,benchmark,current_date,portfolio_name):
    try:
        stocks_exposure_data = ExposureCalculation(stocks_data,prices_data)
        portfolio_cumulative_return,portfolio_daily_returns =portfolioCumulativeReturns(stocks_exposure_data,prices_data)       
        benchmark_cumulative_returns,benchmark_daily_returns = BenchmarkCumulativeReturns (benchmark ,benchmark_data)
        returns = pd.merge(portfolio_daily_returns,benchmark_daily_returns,on=['Date'],how='inner')
        beta = 1
        if abs(len(portfolio_daily_returns)-len(benchmark_daily_returns))<=5:
            beta = stats.linregress(returns[benchmark].values,returns['Portfolio'].values)[0:1][0]
        portfolio_annualize_return = ((portfolio_cumulative_return.loc[len(portfolio_cumulative_return)-1,'Portfolio'] +1) ** (252/len(portfolio_cumulative_return)))-1 
        portfolio_annualize_volatility = (portfolio_daily_returns['Portfolio'].std()) * (math.sqrt(252))
        Results=pd.DataFrame(columns=["C","Annualized Return","Annualized Volatility"])
        Results['C']=['Portfolio']
        Results["Annualized Return"] = [portfolio_annualize_return]
        Results["Annualized Volatility"] = [portfolio_annualize_volatility]
        Results['Beta'] =[beta]
        Results.columns=["Company","Annualized_Return","Annualized_Volatility",'Beta']
        Portfolio_VaR_Component,Individual_VaR=portfolioVaR(stocks_data,var_prices_data,current_date,portfolio_name)        
        return Results,Portfolio_VaR_Component,Individual_VaR
    except Exception as e:
        print(e)
        return pd.DataFrame(),pd.DataFrame(),pd.DataFrame()

def portfolioVaR(stocks_data,prices_data,Trade_Dt,portfolio_name):
    stocks_exposure_data=stocks_data.copy()
    stocks_exposure_data=stocks_exposure_data[['Company','Factset_Ticker','Quantity']]    
    stock_list = set((stocks_exposure_data["Factset_Ticker"]))
    lookback,horizon,i,significant_value=252,1,0,2.32635  
    Individual_VaR=pd.DataFrame()
    Portfolio_VaR_Component=pd.DataFrame()
    Portfolio_VaR_Component["Date"]=""
    Portfolio_VaR_Component["Portfolio"]=""
    Portfolio_VaR_Component["VaR$"]=""
    Portfolio_VaR_Component["Net$"]=""
    Portfolio_VaR_Component["VaR%"]=""        
    Start_Date,End_Date = Trade_Dt+timedelta(-365),Trade_Dt
    prices_data = prices_data[(prices_data ['Date']>=Start_Date)&(prices_data['Date']<=End_Date)].reset_index(drop=True)
    prices_data.columns = ['Company', 'Factset_Ticker','Date','Price','Return']
    prices_data=pd.merge(prices_data,stocks_exposure_data,on=["Company","Factset_Ticker"]).reset_index(drop=True)  
    prices_data['Net'] = prices_data['Price'] * prices_data['Quantity']       
    DailyPortfolio_Net=prices_data.groupby(['Date'])["Net"].agg('sum').reset_index()      
    DailyPortfolio_Net.columns=['Date','DailyPortfolioNet']       
    prices_data=pd.merge(prices_data,DailyPortfolio_Net,on='Date',how='inner')       
    prices_data["Net%"] = prices_data["Net"]/prices_data['DailyPortfolioNet']       
    weight_matrix=prices_data[prices_data['Date']==Trade_Dt]
    weight_matrix=weight_matrix[['Company','Factset_Ticker','Net','Net%']]
    prices_data['Date']=pd.to_datetime(prices_data['Date'])
    prices_data['Max_Date']= prices_data.groupby(['Company',"Factset_Ticker"])['Date'].transform(max)
    prices_data=prices_data[prices_data['Max_Date'].astype(str)==str(Trade_Dt)]
    returns_data=(prices_data.pivot(index='Date', columns="Factset_Ticker", values='Return')).reset_index(drop=True)
    returns_data.fillna(0,inplace=True)    
    Returns=np.array(returns_data.values,dtype=float)                            
    variance_covariance_matrix=np.matmul(Returns.T,Returns)/lookback      
    weight_matrix.sort_values(by=['Company','Factset_Ticker'],inplace=True)       
    weights=np.array(weight_matrix['Net'].values,dtype=float)      
    #Portfolio_Component_Var Calculation
    portfolio_risk=np.matmul(np.matmul(np.array(weight_matrix['Net%'].values,dtype=float),variance_covariance_matrix),np.array(weight_matrix['Net%'].values,dtype=float).T)   
    portfolio_var=math.sqrt(portfolio_risk) * math.sqrt(horizon) * significant_value * np.sum(weights,axis=0)       
    Portfolio_VaR_Component.loc[i,"Date"]=Trade_Dt
    Portfolio_VaR_Component.loc[i,"Portfolio"]=portfolio_name
    Portfolio_VaR_Component.loc[i,"VaR$"]=portfolio_var
    Portfolio_VaR_Component.loc[i,"Net$"]=np.sum(weights,axis=0)
    Portfolio_VaR_Component.loc[i,"VaR%"]=Portfolio_VaR_Component["VaR$"][i]/Portfolio_VaR_Component["Net$"][i]        
    #Individual var calculation
    temp=pd.DataFrame()
    Variance  =np.diag(variance_covariance_matrix).tolist()
    Stock_Values = prices_data[['Company','Factset_Ticker','Net']][prices_data['Date'].astype(str)==str(Trade_Dt)].reset_index(drop=True)    
    Stock_Values.sort_values(by=["Company","Factset_Ticker"],inplace=True)
    Stock_Values['Variance']=Variance
    temp["Stock"]=Stock_Values['Company']
    temp["Factset_Ticker"]=Stock_Values['Factset_Ticker']
    temp["Date"]=Trade_Dt
    temp["VaR$"]=np.sqrt(Stock_Values['Variance']) * Stock_Values['Net'] * math.sqrt(horizon) * significant_value
    temp["Net$"]=Stock_Values['Net']
    temp["VaR%"]=temp["VaR$"]/ temp["Net$"]
    Individual_VaR=Individual_VaR.append(temp)
    Individual_VaR=Individual_VaR[['Date','Stock','Factset_Ticker','VaR$','Net$','VaR%']]
    return Portfolio_VaR_Component,Individual_VaR
    

def riskStockCalcs(stocks_data,prices_data,benchamrks_data,beta_data):
    try:
        beta_data.columns=['Company','Factset_Ticker','Beta']
        beta=pd.merge(beta_data,stocks_data,on=["Company",'Factset_Ticker'],how="inner")
        stocks_data=stocks_data[['Company','Factset_Ticker','Quantity']]
        prices_data=pd.merge(prices_data,stocks_data,on=['Company','Factset_Ticker'],how='inner')
        prices_data['Net']=prices_data['Price'] * prices_data['Quantity']
        prices_data['Net%'] = prices_data['Net']/prices_data['Net']
        prices_data = prices_data.groupby('Factset_Ticker', group_keys=False).apply(lambda x:x.iloc[1:])
        prices_data = pd.pivot(prices_data,index='Date',values='Return',columns='Factset_Ticker').reset_index()
        cols = list(prices_data.columns)
        annual_vol=[]
        annual_ret =[]
        for i in range (1,len(cols)):
            annual_vol.append((prices_data[cols[i]].std()) * math.sqrt(252))
            prices_data[cols[i]] = (1+prices_data[cols[i]]).cumprod()-1
            annual_ret.append (((prices_data.loc[len(prices_data)-1,cols[i]] +1) ** (252/len(prices_data)))-1) 
        output=pd.DataFrame()
        output['Factset_Ticker'] = cols[1:]
        output['Annualized_Return'] = annual_ret
        output['Annualized_Volatility'] = annual_vol
        output=pd.merge(output,beta_data,on=['Factset_Ticker'],how='outer')
        output=output[["Company","Factset_Ticker",'Annualized_Return', 'Annualized_Volatility', 'Beta']]
        return output
    except Exception as e:
       print(e)