import pandas as pd
from datetime import datetime,date
import pyodbc
import math
import numpy as np
from scipy import stats
from statistics import mean
import calendar


def ExposureCalculation(stocks_data,prices_data):    
    stocks_data_df=stocks_data.copy()
    prices_data_df =  prices_data.copy()
    stocks_data_df=stocks_data_df[['Company','Factset_Ticker','Quantity']]
    exposure_data = pd.merge(prices_data_df,stocks_data_df,on=['Factset_Ticker','Company'],how='inner')    
    exposure_data["Stock_Daily_Exposure"]=exposure_data["Price"] * exposure_data["Quantity"]   
    exposure_data=exposure_data[['Company','Factset_Ticker','Date','Stock_Daily_Exposure']]
    return exposure_data

def portfolioCumulativeReturns(exposure_data,prices_data):    
    exposure_data_df=exposure_data.copy()
    prices_data_df =  prices_data.copy()
    exposure_data_df = exposure_data_df[["Company",'Factset_Ticker',"Date",'Stock_Daily_Exposure']]
    daily_exposure_data = pd.merge(exposure_data_df,prices_data_df,on=['Company','Factset_Ticker','Date'])                             
    exposure_data_df.fillna(0,inplace=True)
    portfolio_daily_exposure_data=daily_exposure_data.groupby(['Date'])["Stock_Daily_Exposure"].agg('sum').reset_index()     
    portfolio_daily_exposure_data.columns=['Date','Portfolio_Daily_Exposure']
    daily_exposure_data=pd.merge(daily_exposure_data,portfolio_daily_exposure_data,on='Date',how='inner')
    daily_exposure_data["Exposure%"] = daily_exposure_data["Stock_Daily_Exposure"]/daily_exposure_data['Portfolio_Daily_Exposure']         
    daily_exposure_data['Date'] = pd.to_datetime(daily_exposure_data['Date'])
    daily_exposure_data['PNL%']=daily_exposure_data['Return'] *  daily_exposure_data.groupby(['Company','Factset_Ticker'])['Exposure%'].shift(1)     
    PNL_data=daily_exposure_data.groupby(['Date'])['PNL%'].agg('sum').reset_index()      
    PNL_data = PNL_data.loc[1:,:].reset_index(drop=True)
    PNL_data.columns=["Date","Daily_Portfolio_Return"]
    PNL_data["Daily_Portfolio_Cum_Return"] = (PNL_data['Daily_Portfolio_Return']+1).cumprod()-1      
    PNL_data['Date']=pd.to_datetime(PNL_data['Date'])       
    Cumulative_Returns=PNL_data[["Date","Daily_Portfolio_Cum_Return"]]  
    Cumulative_Returns['Date']=pd.to_datetime(Cumulative_Returns['Date'])    
    Cumulative_Returns.columns = ['Date','Portfolio']
    Daily_Returns=PNL_data[["Date","Daily_Portfolio_Return"]]
    Daily_Returns.columns=['Date','Portfolio']
    Cumulative_Returns=Cumulative_Returns.reset_index(drop=True)
    return Cumulative_Returns,Daily_Returns 

def BenchmarkCumulativeReturns (benchmark,benchmark_data):   #function for calculating index cumulative returns
    benchmark_index_data = benchmark_data.copy()
    benchmark_index_data=benchmark_index_data[benchmark_index_data['Company']==benchmark]   #extracting the data of a given benchmark ticker
    benchmark_index_data.fillna(0,inplace=True)
    benchmark_index_data["Cumulative Return"] = (benchmark_index_data['Return']+1).cumprod()-1   #finding running cumulative 
    benchmark_index_data=benchmark_index_data[["Date","Company","Return","Cumulative Return"]]
    benchmark_index_data['Date'] = pd.to_datetime(benchmark_index_data['Date'])
    benchmark_daily_returns=benchmark_index_data[["Date","Return"]]
    benchmark_daily_returns.columns = ['Date',benchmark]
    benchmark_cumulative_return=benchmark_index_data[["Date","Cumulative Return"]]
    benchmark_cumulative_return.columns=["Date",benchmark]
    benchmark_cumulative_return=benchmark_cumulative_return.reset_index(drop=True)
    return benchmark_cumulative_return,benchmark_daily_returns

def equalPortfolioCumulativeReturns(exposure_data,prices_data):
    stocks_data=exposure_data[['Company','Factset_Ticker','Quantity']]
    stock=list(stocks_data['Factset_Ticker'])
    prices_data = pd.merge(prices_data,stocks_data,on=['Factset_Ticker','Company'],how='inner')
    if (len(prices_data)!=0):
        Equal_weighted_Portfolio= prices_data[['Date','Company','Factset_Ticker','Price','Return','Quantity']]
        Equal_weighted_Portfolio.sort_values(by=['Date','Company','Factset_Ticker'],ascending=[True,True,True],inplace=True)
        Equal_weighted_Portfolio.reset_index(drop=True,inplace=True)
        starting_day_prices =  Equal_weighted_Portfolio.groupby(['Company','Factset_Ticker']).first().reset_index()
        starting_day_prices=starting_day_prices[['Company','Factset_Ticker','Date','Price']]
        distinct_stocks_data=exposure_data[['Company','Factset_Ticker','Quantity','Price']].drop_duplicates()
        distinct_stocks_data['Exposure']=distinct_stocks_data['Quantity']*distinct_stocks_data['Price']
        new_exposure = (distinct_stocks_data['Exposure'].sum())/(len(distinct_stocks_data))
        distinct_stocks_data['New Quantity']=new_exposure/(distinct_stocks_data['Price'])
        distinct_stocks_data=distinct_stocks_data[['Company','Factset_Ticker','New Quantity']]
        distinct_stocks_data.columns=['Company','Factset_Ticker','Quantity']        
        Equal_weighted_Portfolio=Equal_weighted_Portfolio[['Date','Company','Factset_Ticker','Price','Return']]
        Equal_weighted_Portfolio=pd.merge(Equal_weighted_Portfolio,distinct_stocks_data,on=["Company","Factset_Ticker"],how="inner")
        Equal_weighted_Portfolio['StockDayNet']=Equal_weighted_Portfolio['Price']*Equal_weighted_Portfolio['Quantity']
        Equal_Net_Data=Equal_weighted_Portfolio.groupby(['Date'])["StockDayNet"].agg('sum').reset_index() 
        Equal_Net_Data.columns=['Date','PortfolioDayNet']
        Equal_weighted_Portfolio=pd.merge(Equal_weighted_Portfolio,Equal_Net_Data,on="Date",how="inner")
        Equal_weighted_Portfolio["Exposure%"] = Equal_weighted_Portfolio["StockDayNet"]/Equal_weighted_Portfolio['PortfolioDayNet']        
        Equal_weighted_Portfolio['Equal_Portfolio_Return']= Equal_weighted_Portfolio['Return'] *  Equal_weighted_Portfolio.groupby(['Company','Factset_Ticker'])['Exposure%'].shift(1) 
        Equal_PNL_data=Equal_weighted_Portfolio.groupby(['Date'])['Equal_Portfolio_Return'].agg('sum').reset_index()      
        Equal_PNL_data.columns=["Date","Equal_Portfolio_Return"]        
        Equal_PNL_data['Date']=pd.to_datetime(Equal_PNL_data['Date'])
        Equal_PNL_data = Equal_PNL_data.loc[1:,:].reset_index(drop=True)
        Equal_PNL_data["Equal_Portfolio"] = (Equal_PNL_data['Equal_Portfolio_Return']+1).cumprod()-1  
        Equal_Cumulative_PNL_data=Equal_PNL_data[['Date','Equal_Portfolio']]
        Equal_Cumulative_PNL_data.columns=['Date','Equal_Portfolio']
        Equal_PNL_data=Equal_PNL_data[['Date','Equal_Portfolio_Return']]
        Equal_PNL_data.columns=['Date','Equal_Portfolio']
        return Equal_Cumulative_PNL_data,Equal_PNL_data

def AnnualizedReturn (cumulative_returns,columnName):
    daily_cumulative_return = cumulative_returns.copy()
    daily_cumulative_return = daily_cumulative_return.reset_index(drop=True)
    annualize_ret=((daily_cumulative_return.loc[len(daily_cumulative_return)-1,columnName] +1) ** (252/len(daily_cumulative_return)))-1   # annualize returns by taking cumulative returns
    return annualize_ret

def AnnualizeVolatility (daily_returns,columnName):    
    annualize_vol=daily_returns.loc[:,columnName].std()     #volatiity taking returns and applying standard deviation and then multiply with annualize number
    annualize_volatility = annualize_vol*(math.sqrt(252))
    down_annualize_vol =  daily_returns[daily_returns[columnName]<=0].reset_index(drop=True)
    down_annualize_volatility = down_annualize_vol.loc[:,columnName].std()
    down_annualize_volatility = down_annualize_volatility * (math.sqrt(252))
    return annualize_volatility,down_annualize_volatility

    
def MaxDrawDown(cumulative_returns,columnName):
    returns=cumulative_returns.copy()
    returns["Max Cumulative"]=returns[columnName].cummax(axis = 0)     #find max value of cumulatives at given point
    draw_down_input=pd.DataFrame()
    draw_down_input["Cumulative"]=returns[columnName]
    draw_down_input["Max Cumulative"]=returns["Max Cumulative"]
    draw_down_input["Draw_down"]=1-((1+draw_down_input["Cumulative"])/(1+draw_down_input["Max Cumulative"]))
    max_draw_down_res=draw_down_input["Draw_down"].max()
    return max_draw_down_res

def AnnualizeRiskFreeRate(risk_free_rate):
    risk_free_rate_data=risk_free_rate.copy()
    risk_free_rate_data = risk_free_rate_data.reset_index(drop=True)
    risk_free_rate_data["Risk Free Rate_cumulate"] = (risk_free_rate_data['Risk Free Rate']+1).cumprod()-1                               #Calculate the Cumulative PNL
    annualize_risk_free=((risk_free_rate_data.loc[len(risk_free_rate_data)-1,"Risk Free Rate_cumulate"] +1) ** (252/len(risk_free_rate_data)))-1
    return annualize_risk_free


def Correlation_Calc(input_returns,benchmark_index_returns,columnName,benchmark_index):   
    inputdf=input_returns.copy()
    if columnName==benchmark_index:
        columnName=columnName+"_"
        inputdf.columns=['Date',columnName]
    required_df = pd.merge(inputdf,benchmark_index_returns,on='Date',how='inner')
    required_df=required_df.sort_values(by='Date').reset_index(drop=True)
    correlation_coef= required_df[columnName].corr(required_df[benchmark_index])
    return correlation_coef
def UpsideCapture(input_returns,benchmark_index_returns,columnName,benchmark_index):   
    inputdf=input_returns.copy()
    if columnName==benchmark_index:
        columnName=columnName+"_"
        inputdf.columns=['Year','Month',columnName] 
    required_df=pd.merge(inputdf,benchmark_index_returns,on=['Year','Month'],how="inner")        
    required_df=required_df[required_df[benchmark_index] >0].sort_values(by=['Year','Month']).reset_index(drop=True)   
    if len(required_df)>0:
        required_df[columnName] = (required_df[columnName]+1).cumprod()-1                              
        required_df[benchmark_index] = (required_df[benchmark_index]+1).cumprod()-1                              
        portfolio_upside=((required_df.loc[len(required_df)-1,columnName] +1) ** (12/len(required_df)))        
        benchmark_upside=((required_df.loc[len(required_df)-1,benchmark_index] +1) ** (12/len(required_df)))    
        return round((portfolio_upside-1)/(benchmark_upside-1),2)
        # portfolio_geomean = np.exp(np.mean(np.log(required_df[columnName])))
        # index_geomean = np.exp(np.mean(np.log(required_df[benchmark_index])))
        # return portfolio_geomean/index_geomean
    else:
        return 0
def DownsideCapture(input_returns,benchmark_index_returns,columnName,benchmark_index):    
    inputdf=input_returns.copy()
    if columnName==benchmark_index:
        columnName=columnName+"_"
        inputdf.columns=['Year','Month',columnName]
    required_df=pd.merge(inputdf,benchmark_index_returns,on=['Year','Month'],how="inner")        
    required_df=required_df[required_df[benchmark_index]<=0].sort_values(by=['Year','Month']).reset_index(drop=True)   
    if len(required_df)>0:
        required_df[columnName] = (required_df[columnName]+1).cumprod()-1                              
        required_df[benchmark_index] = (required_df[benchmark_index]+1).cumprod()-1                       
        portfolio_downside=((required_df.loc[len(required_df)-1,columnName] +1) ** (12/len(required_df)))        
        benchmark_downside=((required_df.loc[len(required_df)-1,benchmark_index] +1) ** (12/len(required_df)))    
        return round((portfolio_downside-1)/(benchmark_downside-1),2)
    else:
        return 0
def TrackingError(input_returns,benchmark_index_returns,columnName,benchmark_index):
    inputdf=input_returns.copy()
    if columnName==benchmark_index:
        columnName=columnName+"_"
        inputdf.columns=['Date',columnName]
    required_df=pd.merge(inputdf,benchmark_index_returns,on="Date",how="inner")        
    required_df["Active Return Gross"]=required_df[columnName] - required_df[benchmark_index]    
    tracking_error_result=(required_df.loc[:,"Active Return Gross"].std())*(math.sqrt(252))    
    return tracking_error_result

def MonthlyCumulativeReturns(daily_pnl_returns,dynamicColumn):
    daily_pnl = daily_pnl_returns.copy()
    daily_pnl = daily_pnl.sort_values(by='Date')
    daily_pnl['Date'] = pd.to_datetime(daily_pnl['Date'])
    daily_pnl["Year"] = daily_pnl['Date'].dt.year    
    daily_pnl['Month'] = pd.DatetimeIndex(daily_pnl['Date']).month
    daily_pnl[dynamicColumn] =daily_pnl[dynamicColumn]+1
    daily_pnl[dynamicColumn]=daily_pnl.groupby(['Year','Month'])[dynamicColumn].cumprod()-1
    monthly_cum_return=daily_pnl.groupby (['Year','Month']).tail(1).reset_index(drop=True)
    monthly_cum_return = monthly_cum_return[["Year","Month",dynamicColumn]]
    return monthly_cum_return
def ror_calc(monthly_cumulative):
    l=[]
    currentYear = date.today().year
    one_month= monthly_cumulative.tail(2).head(1)
    three_month = monthly_cumulative.tail(4).head(3)
    three_month["three_months"] = three_month["Portfolio"]+1
    three_months_ror = three_month["three_months"].prod()-1
    ytd = monthly_cumulative[monthly_cumulative["Year"]==currentYear].reset_index()
    ytd = ytd["1+Portfolio"].prod()-1
    month_12 = monthly_cumulative.tail(12)
    month_12_ror = month_12["1+Portfolio"].prod()-1
    positive_month_count=len(list(filter(lambda x:x>=0,monthly_cumulative['Portfolio'].tolist())))
    negative_month_count=len(list(filter(lambda x:x<0,monthly_cumulative['Portfolio'].tolist())))
    total_months = len(monthly_cumulative)
    winning_months = positive_month_count/total_months
    positive_sum = monthly_cumulative["Portfolio"].agg(lambda x: x[x>=0].sum())
    avg_winning_month = positive_sum/positive_month_count
    negative_sum = monthly_cumulative["Portfolio"].agg(lambda x: x[x<0].sum())
    avg_losing_month = negative_sum/negative_month_count
    l.append(["1 Month ROR",str(round(list(one_month["Portfolio"])[0]*100,2)) + "%"])        
    l.append(["3 Month ROR",str(round(three_months_ror*100,2))+"%"])    
    l.append(["YTD",str(round(ytd*100,2))+"%"])    
    l.append(["12 Month ROR",str(round(month_12_ror*100,2))+"%"])    
    l.append(["Winning Months %",str(round(winning_months*100,2)) + "%"])    
    l.append(["Average Winning Month",str(round(avg_winning_month*100,2))+"%"])    
    l.append(["Average Losing Month",str(round(avg_losing_month*100,2))+"%"])
    result = pd.DataFrame(l, columns = ['ROR', 'Value']) 
    return result

def MonthlyCumulativeReturnsMain (prices_data,stocks_data):
    stocks_exposure_data = ExposureCalculation(stocks_data,prices_data) 
    portfolio_cumulative_returns,portfolio_daily_returns =portfolioCumulativeReturns(stocks_exposure_data,prices_data)       
    monthly_cumulative =  MonthlyCumulativeReturns(portfolio_daily_returns,'Portfolio')
    monthly_cumulative["1+Portfolio"]=monthly_cumulative["Portfolio"]+1    
    ror_metrics=ror_calc(monthly_cumulative)
    yearly_cum = monthly_cumulative.groupby("Year")["1+Portfolio"].prod()-1
    yearly_cum = yearly_cum.reset_index()
    yearly_cum = yearly_cum.rename(columns={'1+Portfolio': 'Yearly'})    
    monthly_cumulative_chart_data = monthly_cumulative.copy()
    monthly_cumulative_chart_data['Day'] = 1
    monthly_cumulative_chart_data[['Month','Day']]=monthly_cumulative_chart_data[['Month','Day']].astype(str).apply(lambda x: x.str.zfill(2))
    monthly_cumulative_chart_data ['Date'] =monthly_cumulative_chart_data['Year'].astype(str) + monthly_cumulative_chart_data['Month']  +monthly_cumulative_chart_data['Day'] 
    monthly_cumulative_chart_data['Date'] = pd.to_datetime(monthly_cumulative_chart_data['Date'],format='%Y%m%d')
    monthly_cumulative_chart_data= monthly_cumulative_chart_data[['Date','Portfolio']]
    monthly_cumulative['Month'] = monthly_cumulative['Month'].apply(lambda x: calendar.month_abbr[x])
    monthly_cumulative = monthly_cumulative.pivot("Year","Month","Portfolio")
    monthly_cumulative = monthly_cumulative.sort_values("Year",ascending=[False]).reset_index()
    monthly_cumulative.iloc[0]=monthly_cumulative.iloc[0].fillna("")
    monthly_cumulative = monthly_cumulative[['Year','Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']]
    monthly_cumulative = pd.merge(monthly_cumulative,yearly_cum,on='Year',how='inner')
    return monthly_cumulative,ror_metrics,monthly_cumulative_chart_data

def getIndexData(total_index_data,benchmark):
    total_index_data_df = total_index_data.copy()
    total_index_data_df =  total_index_data_df[total_index_data_df['Company']==benchmark].reset_index()
    total_index_data_df =  total_index_data_df[['Date','Return']]
    total_index_data_df.columns =['Date',benchmark]    
    return total_index_data_df

def FormatDates(inputdf,benchmark):
    df = inputdf.copy()
    df['Day'] = 1
    df[['Month','Day']]=df[['Month','Day']].astype(str).apply(lambda x: x.str.zfill(2))
    df ['Date'] =df['Year'].astype(str) + df['Month']  +df['Day'] 
    df['Date'] = pd.to_datetime(df['Date'],format='%Y%m%d') 
    df['Date'] = df['Date'].astype(str)
    df= df[['Date',benchmark]].reset_index(drop=True)
    return df

def BenchmarkMonthlyCumulativeReturns(total_index_data,benchmark1,benchmark2,benchmark3):
    benchmark1_monthly = MonthlyCumulativeReturns(getIndexData(total_index_data,benchmark1),benchmark1)
    benchmark2_monthly = MonthlyCumulativeReturns(getIndexData(total_index_data,benchmark2),benchmark2)
    benchmark3_monthly = MonthlyCumulativeReturns(getIndexData(total_index_data,benchmark3),benchmark3)

    benchmark1_monthly = FormatDates(benchmark1_monthly,benchmark1)
    benchmark2_monthly = FormatDates(benchmark2_monthly,benchmark2)
    benchmark3_monthly = FormatDates(benchmark3_monthly,benchmark3)

    return benchmark1_monthly,benchmark2_monthly,benchmark3_monthly

def CalcsMain(country,stocks_data,prices_data,benchmark_indices_data,risk_free_data,portfolio_name,period):     
    try:
        stocks_exposure_data = ExposureCalculation(stocks_data,prices_data) 
        benchmark_indices = {'India':['Nifty 50 Index','Nifty Smallcap 100 Index','Nifty Midcap 100 Index'],
                            'US':['S&P 500','Russell 2000','Dow Jones Industrial Average']}
        benchmark1,benchmark2,benchmark3=benchmark_indices[country][0],benchmark_indices[country][1],benchmark_indices[country][2]
        portfolio_cumulative_returns,portfolio_daily_returns =portfolioCumulativeReturns(stocks_exposure_data,prices_data) 
        portfolio_monthly_returns = MonthlyCumulativeReturns(portfolio_daily_returns,'Portfolio')  
        equal_portfolio_cumulative_returns,equal_portfolio_daily_returns= equalPortfolioCumulativeReturns(stocks_data,prices_data)
        equal_portfolio_monthly_returns = MonthlyCumulativeReturns(equal_portfolio_daily_returns,'Equal_Portfolio')
        benchmark1_cumulative_returns,benchmark1_daily_returns = BenchmarkCumulativeReturns (benchmark1,benchmark_indices_data)
        benchmark1_monthly_returns = MonthlyCumulativeReturns(benchmark1_daily_returns,benchmark1)
        benchmark2_cumulative_returns,benchmark2_daily_returns = BenchmarkCumulativeReturns (benchmark2,benchmark_indices_data)
        benchmark2_monthly_returns = MonthlyCumulativeReturns(benchmark2_daily_returns,benchmark2)
        benchmark3_cumulative_returns,benchmark3_daily_returns = BenchmarkCumulativeReturns (benchmark3,benchmark_indices_data)
        benchmark3_monthly_returns = MonthlyCumulativeReturns(benchmark3_daily_returns,benchmark3)
   
        if(period=='12M'):
            portfolio_monthly_returns = portfolio_monthly_returns.tail(12) 
            equal_portfolio_monthly_returns = equal_portfolio_monthly_returns.tail(12)
            benchmark1_monthly_returns=benchmark1_monthly_returns.tail(12)
            benchmark2_monthly_returns = benchmark2_monthly_returns.tail(12)
            benchmark3_monthly_returns = benchmark3_monthly_returns.tail(12)

        #Annualize_Returns
        portfolio_annualized_return = AnnualizedReturn (portfolio_cumulative_returns,'Portfolio')
        equal_portfolio_annualized_return = AnnualizedReturn (equal_portfolio_cumulative_returns,'Equal_Portfolio')
        benchmark1_annualized_return = AnnualizedReturn (benchmark1_cumulative_returns,benchmark1)
        benchmark2_annualized_return = AnnualizedReturn (benchmark2_cumulative_returns,benchmark2)
        benchmark3_annualized_return = AnnualizedReturn (benchmark3_cumulative_returns,benchmark3)
        #Annualize_Volatiliy
        portfolio_annualized_volatility,portfolio_down_annualized_volatilty = AnnualizeVolatility(portfolio_daily_returns,'Portfolio')
        equal_portfolio_annualized_volatility,equal_portfolio_down_annualized_volatilty = AnnualizeVolatility(equal_portfolio_daily_returns,'Equal_Portfolio')
        benchmark1_annualized_volatility,benchmark1_annualized_down_volatility = AnnualizeVolatility(benchmark1_daily_returns,benchmark1)
        benchmark2_annualized_volatility,benchmark2_annualized_down_volatility =AnnualizeVolatility(benchmark2_daily_returns,benchmark2)
        benchmark3_annualized_volatility,benchmark3_annualized_down_volatility =AnnualizeVolatility(benchmark3_daily_returns,benchmark3)
        #Max_DrawDown
        portfolio_max_drawdown = MaxDrawDown(portfolio_cumulative_returns,'Portfolio')
        equal_portfolio_max_drawdown =MaxDrawDown(equal_portfolio_cumulative_returns,'Equal_Portfolio')
        benchmark1_max_drawdown = MaxDrawDown(benchmark1_cumulative_returns,benchmark1)
        benchmark2_max_drawdown = MaxDrawDown(benchmark2_cumulative_returns,benchmark2)
        benchmark3_max_drawdown = MaxDrawDown(benchmark3_cumulative_returns,benchmark3)
        #Annualize Risk Free Rate
        annualize_risk_free_rate=AnnualizeRiskFreeRate (risk_free_data)
        #Sharpe
        portfolio_sharpe = (portfolio_annualized_return - annualize_risk_free_rate)/portfolio_annualized_volatility
        equal_portfolio_sharpe = (equal_portfolio_annualized_return - annualize_risk_free_rate)/equal_portfolio_annualized_volatility
        benchmark1_sharpe = (benchmark1_annualized_return - annualize_risk_free_rate)/benchmark1_annualized_volatility
        benchmark2_sharpe = (benchmark2_annualized_return - annualize_risk_free_rate)/benchmark2_annualized_volatility
        benchmark3_sharpe = (benchmark3_annualized_return - annualize_risk_free_rate)/benchmark3_annualized_volatility    
        #Sortino
        portfolio_sortino = (portfolio_annualized_return - annualize_risk_free_rate)/portfolio_down_annualized_volatilty
        equal_portfolio_sortino = (equal_portfolio_annualized_return - annualize_risk_free_rate)/equal_portfolio_down_annualized_volatilty
        benchmark1_sortino = (benchmark1_annualized_return - annualize_risk_free_rate)/benchmark1_annualized_down_volatility
        benchmark2_sortino = (benchmark2_annualized_return - annualize_risk_free_rate)/benchmark2_annualized_down_volatility
        benchmark3_sortino= (benchmark3_annualized_return - annualize_risk_free_rate)/benchmark3_annualized_down_volatility    

        #Correlation
        portfolio_correlation = Correlation_Calc(portfolio_daily_returns,benchmark1_daily_returns,'Portfolio',benchmark1)
        equal_portfolio_correlation = Correlation_Calc(equal_portfolio_daily_returns,benchmark1_daily_returns,'Equal_Portfolio',benchmark1)
        benchmark1_correlation = Correlation_Calc(benchmark1_daily_returns,benchmark1_daily_returns,benchmark1,benchmark1)
        benchmark2_correlation = Correlation_Calc(benchmark2_daily_returns,benchmark1_daily_returns,benchmark2,benchmark1)
        benchmark3_correlation = Correlation_Calc(benchmark3_daily_returns,benchmark1_daily_returns,benchmark3,benchmark1)

        #Upside capture
        portfolio_upside_capture = UpsideCapture(portfolio_monthly_returns,benchmark1_monthly_returns,'Portfolio',benchmark1)
        equal_portfolio_upside_capture = UpsideCapture(equal_portfolio_monthly_returns,benchmark1_monthly_returns,'Equal_Portfolio',benchmark1)
        benchmark1_upside_capture = UpsideCapture(benchmark1_monthly_returns,benchmark1_monthly_returns,benchmark1,benchmark1)
        benchmark2_upside_capture = UpsideCapture(benchmark2_monthly_returns,benchmark1_monthly_returns,benchmark2,benchmark1)
        benchmark3_upside_capture = UpsideCapture(benchmark3_monthly_returns,benchmark1_monthly_returns,benchmark3,benchmark1)

        #Downside capture
        portfolio_downside_capture = DownsideCapture(portfolio_monthly_returns,benchmark1_monthly_returns,'Portfolio',benchmark1)
        equal_portfolio_downside_capture = DownsideCapture(equal_portfolio_monthly_returns,benchmark1_monthly_returns,'Equal_Portfolio',benchmark1)
        benchmark1_downside_capture = DownsideCapture(benchmark1_monthly_returns,benchmark1_monthly_returns,benchmark1,benchmark1)
        benchmark2_downside_capture = DownsideCapture(benchmark2_monthly_returns,benchmark1_monthly_returns,benchmark2,benchmark1)
        benchmark3_downside_capture = DownsideCapture(benchmark3_monthly_returns,benchmark1_monthly_returns,benchmark3,benchmark1)
        
        #Tracking Error
        portfolio_tracking_error = TrackingError(portfolio_daily_returns,benchmark1_daily_returns,'Portfolio',benchmark1)
        equal_portfolio_tracking_error = TrackingError(equal_portfolio_daily_returns,benchmark1_daily_returns,'Equal_Portfolio',benchmark1)
        benchmark1_tracking_error = TrackingError(benchmark1_daily_returns,benchmark1_daily_returns,benchmark1,benchmark1)
        benchmark2_tracking_error = TrackingError(benchmark2_daily_returns,benchmark1_daily_returns,benchmark2,benchmark1)
        benchmark3_tracking_error = TrackingError(benchmark3_daily_returns,benchmark1_daily_returns,benchmark3,benchmark1)

        #Information Ratio
        portfolio_information_ratio = (portfolio_annualized_return-benchmark1_annualized_return)/portfolio_tracking_error
        equal_portfolio_information_ratio = (equal_portfolio_annualized_return-benchmark1_annualized_return)/equal_portfolio_tracking_error
        benchmark1_information_ratio = (benchmark1_annualized_return-benchmark1_annualized_return)/benchmark1_tracking_error
        benchmark2_information_ratio = (benchmark2_annualized_return-benchmark1_annualized_return)/benchmark2_tracking_error
        benchmark3_information_ratio = (benchmark3_annualized_return-benchmark1_annualized_return)/benchmark3_tracking_error

        #Cumulative Return
        Cumulative_Returns = pd.merge(portfolio_cumulative_returns,equal_portfolio_cumulative_returns,on="Date",how="inner")  
        Cumulative_Returns = pd.merge(Cumulative_Returns,benchmark1_cumulative_returns,on="Date",how="inner")   
        Cumulative_Returns = pd.merge(Cumulative_Returns,benchmark2_cumulative_returns,on="Date",how="inner")
        Cumulative_Returns = pd.merge(Cumulative_Returns,benchmark3_cumulative_returns,on="Date",how="inner")
        Cumulative_Returns =  Cumulative_Returns[["Date","Portfolio",'Equal_Portfolio',benchmark1,benchmark2,benchmark3]]  
       
        #Daily_Returns
        Daily_Returns = pd.merge(portfolio_daily_returns,equal_portfolio_daily_returns,on="Date",how="inner")
        Daily_Returns = pd.merge(Daily_Returns,benchmark1_daily_returns,on="Date",how="inner")
        Daily_Returns = pd.merge(Daily_Returns,benchmark2_daily_returns,on="Date",how="inner")
        Daily_Returns = pd.merge(Daily_Returns,benchmark3_daily_returns,on="Date",how="inner")
        Daily_Returns =  Daily_Returns[["Date","Portfolio",'Equal_Portfolio',benchmark1,benchmark2,benchmark3]] 

        #last day cumulative returns
        portfolio_cumulative_return = Cumulative_Returns.loc[len(Cumulative_Returns)-1,'Portfolio']
        equal_portfolio_cumulative_return = Cumulative_Returns.loc[len(Cumulative_Returns)-1,'Equal_Portfolio']
        benchmark1_cumulative_return = Cumulative_Returns.loc[len(Cumulative_Returns)-1,benchmark1]
        benchmark2_cumulative_return = Cumulative_Returns.loc[len(Cumulative_Returns)-1,benchmark2]
        benchmark3_cumulative_return = Cumulative_Returns.loc[len(Cumulative_Returns)-1,benchmark3]

        #Sterling
        portfolio_sterling = portfolio_annualized_return/(portfolio_max_drawdown+0.1)
        equal_portfolio_sterling= equal_portfolio_annualized_return/(equal_portfolio_max_drawdown+0.1)
        benchmark1_sterling = benchmark1_annualized_return/(benchmark1_max_drawdown+0.1)
        benchmark2_sterling= benchmark2_annualized_return/(benchmark2_max_drawdown+0.1)
        benchmark3_sterling = benchmark3_annualized_return/(benchmark3_max_drawdown+0.1)

        #Calmar
        portfolio_calmar = portfolio_annualized_return/portfolio_max_drawdown
        equal_portfolio_calmar = equal_portfolio_annualized_return/equal_portfolio_max_drawdown
        benchmark1_calmar = benchmark1_annualized_return/benchmark1_max_drawdown
        benchmark2_calmar = benchmark2_annualized_return/benchmark2_max_drawdown
        benchmark3_calmar = benchmark3_annualized_return/benchmark3_max_drawdown
        
        #Skew
        portfolio_skew = Daily_Returns['Portfolio'].skew()
        equal_portfolio_skew =Daily_Returns['Equal_Portfolio'].skew()
        benchmark1_skew = Daily_Returns[benchmark1].skew()
        benchmark2_skew = Daily_Returns[benchmark2].skew()
        benchmark3_skew = Daily_Returns[benchmark3].skew()

        #Kurtosis
        portfolio_kurtosis =Daily_Returns['Portfolio'].kurtosis()
        equal_portfolio_kurtosis = Daily_Returns['Equal_Portfolio'].kurtosis()
        benchmark1_kurtosis = Daily_Returns[benchmark1].kurtosis()
        benchmark2_kurtosis =Daily_Returns[benchmark2].kurtosis()
        benchmark3_kurtosis = Daily_Returns[benchmark3].kurtosis()

        Results=pd.DataFrame(columns=["Metric/Portfolio","Portfolio",'Equal Weighted Portfolio',benchmark1,benchmark2,benchmark3])
        Results['Metric/Portfolio'] = ['Cumulative Return','Annualized Return','Annualized Volatility','Annualized Downside Volatility','Sharpe','Sortino','Maximum Drawdown',
                                        'Correlation','Upside Capture','Downside Capture','Tracking Error','Information Ratio','Sterling','Calmar','Skewness','Kurtosis']
        Results['Portfolio'] = [portfolio_cumulative_return,portfolio_annualized_return,portfolio_annualized_volatility,portfolio_down_annualized_volatilty,portfolio_sharpe,
                                portfolio_sortino,portfolio_max_drawdown,portfolio_correlation,portfolio_upside_capture,portfolio_downside_capture,portfolio_tracking_error,portfolio_information_ratio,
                                portfolio_sterling,portfolio_calmar,portfolio_skew,portfolio_kurtosis]
        Results['Equal Weighted Portfolio'] = [equal_portfolio_cumulative_return,equal_portfolio_annualized_return,equal_portfolio_annualized_volatility,equal_portfolio_down_annualized_volatilty,equal_portfolio_sharpe,
                                equal_portfolio_sortino,equal_portfolio_max_drawdown,equal_portfolio_correlation,equal_portfolio_upside_capture,equal_portfolio_downside_capture,equal_portfolio_tracking_error,equal_portfolio_information_ratio,
                                equal_portfolio_sterling,equal_portfolio_calmar,equal_portfolio_skew,equal_portfolio_kurtosis]

        Results[benchmark1] =[benchmark1_cumulative_return,benchmark1_annualized_return,benchmark1_annualized_volatility,benchmark1_annualized_down_volatility,benchmark1_sharpe,
                                benchmark1_sortino,benchmark1_max_drawdown,benchmark1_correlation,benchmark1_upside_capture,benchmark1_downside_capture,benchmark1_tracking_error,benchmark1_information_ratio,
                                    benchmark1_sterling,benchmark1_calmar,benchmark1_skew,benchmark1_kurtosis]

        Results[benchmark2] =[benchmark2_cumulative_return,benchmark2_annualized_return,benchmark2_annualized_volatility,benchmark2_annualized_down_volatility,benchmark2_sharpe,
                                benchmark2_sortino,benchmark2_max_drawdown,benchmark2_correlation,benchmark2_upside_capture,benchmark2_downside_capture,benchmark2_tracking_error,benchmark2_information_ratio,
                                benchmark2_sterling,benchmark2_calmar,benchmark2_skew,benchmark2_kurtosis]

        Results[benchmark3] =[benchmark3_cumulative_return,benchmark3_annualized_return,benchmark3_annualized_volatility,benchmark3_annualized_down_volatility,benchmark3_sharpe,
                                benchmark3_sortino,benchmark3_max_drawdown,benchmark3_correlation,benchmark3_upside_capture,benchmark3_downside_capture,benchmark3_tracking_error,benchmark3_information_ratio,
                                benchmark3_sterling,benchmark3_calmar,benchmark3_skew,benchmark3_kurtosis]

        Daily_Returns_values = pd.merge(portfolio_daily_returns,equal_portfolio_daily_returns,on="Date",how="inner")  
        Daily_Returns_values = pd.merge(Daily_Returns_values,benchmark1_daily_returns,on="Date",how="inner")  
        Daily_Returns_values = pd.merge(Daily_Returns_values,benchmark2_daily_returns,on="Date",how="inner")
        Daily_Returns_values = pd.merge(Daily_Returns_values,benchmark3_daily_returns,on="Date",how="inner")
        Daily_Returns_values =  Daily_Returns_values[["Date","Portfolio",'Equal_Portfolio',benchmark1,benchmark2,benchmark3]]  
        Results.fillna(0,inplace=True)
        round_metric_cols = {'Sharpe','Sortino','Correlation','Upside Capture','Downside Capture','Tracking Error','Information Ratio','Sterling','Calmar','Skewness','Kurtosis'}
        metrics_cols = list(set(Results['Metric/Portfolio'])-round_metric_cols)
        Results.loc[Results['Metric/Portfolio'].isin(list(round_metric_cols)),list(Results.columns)[1:]] =(Results[list(Results.columns)[1:]]).round(2)
        Results.loc[Results['Metric/Portfolio'].isin(metrics_cols),list(Results.columns)[1:]] =((Results[list(Results.columns)[1:]] )*100).round(2).astype(str) + "%"
        Results = Results.replace([np.inf,-np.inf],np.nan)
        Results = Results.fillna(0)
        return Cumulative_Returns,Daily_Returns_values,Results
    except Exception as e:
        print(e)
        return pd.DataFrame(),pd.DataFrame(),pd.DataFrame()