from django.db import models
from modelcluster.fields import ParentalKey
from wagtail.core.models import Page
from wagtail.core.fields import StreamField
from wagtail.core import blocks
from wagtail.admin.edit_handlers import FieldPanel, MultiFieldPanel, InlinePanel, StreamFieldPanel
from wagtail.images.edit_handlers import ImageChooserPanel
from wagtail.documents.models import Document
from wagtail.documents.edit_handlers import DocumentChooserPanel
from wagtail.contrib.settings.models import BaseSetting, register_setting
from wagtail.contrib.routable_page.models import RoutablePageMixin, route
from wagtail.core import hooks
from wagtail.api import APIField
from django.core.validators import MaxValueValidator, MinValueValidator
from wagtail.images.api.fields import ImageRenditionField
from core.models import SecurityMaster, UsSecurityMaster

# Create your models here.
class OurIndiaMarketView(Page):
    posted_at = models.DateTimeField("Date",auto_now_add=True,null=True)
    body = StreamField([
        ('description', blocks.RichTextBlock(null=True)),
    ],null=True)
    content_panels = Page.content_panels + [
        StreamFieldPanel('body'),
    ]
class OurUSMarketView(Page):
    posted_at = models.DateTimeField("Date",auto_now_add=True,null=True)
    body = StreamField([
        ('description', blocks.RichTextBlock(null=True)),
    ],null=True)
    content_panels = Page.content_panels + [
        StreamFieldPanel('body'),
    ]
# Create your models here.
class UniversePricesMain(models.Model):
    date = models.DateField(db_column='Date')  # Field name made lowercase.
    company = models.TextField(db_column='Company')  # Field name made lowercase.
    price = models.FloatField(db_column='Price', blank=True, null=True)  # Field name made lowercase.
    factset_ticker = models.TextField(db_column='Factset_Ticker', blank=True, null=True)  # Field name made lowercase.
    return_field = models.FloatField(db_column='Return', blank=True, null=True)  # Field name made lowercase. Field renamed because it was a Python reserved word.
    s_no = models.AutoField(db_column='s_no', primary_key=True)  # Field name made lowercase.


    class Meta:
        managed = False
        db_table = 'universe_prices_main'

    def __str__(self):
        return self.company+"-"+self.date


class TradeSignals(models.Model):
    indiancompanies = SecurityMaster.objects.filter(flag='yes').values_list('fs_ticker','fs_name','security_code').order_by('fs_name')
    companies = ([[item[1],item[1]+" ("+item[2]+")"] for item in indiancompanies])
    tickers = ([[item[0],item[1]+" ("+item[2]+")"] for item in indiancompanies])
    diamond =  models.CharField("Only For Diamond", max_length=255, choices=(('yes','Yes'),('no','No')), default='Yes')
    company_name = models.CharField("Company Name", max_length=255, choices=companies)
    fs_ticker = models.CharField("FS Ticker", max_length=255, choices=tickers,null=True, blank=True)
    fundamental= models.TextField("Fundamental")
    fund_rating = models.PositiveIntegerField("Fundamental Rating", default=1, validators=[MinValueValidator(1), MaxValueValidator(5)])
    quantitative = models.TextField("Quantative")
    quant_rating = models.PositiveIntegerField("Quantitative Rating",default=1, validators=[MinValueValidator(1), MaxValueValidator(5)])
    technical= models.TextField("Technical")
    technical_rating = models.PositiveIntegerField("Technical Rating",default=1, validators=[MinValueValidator(1), MaxValueValidator(5)])
    situations = models.TextField("Situations")
    situations_rating = models.PositiveIntegerField("Situations Rating",default=1, validators=[MinValueValidator(1), MaxValueValidator(5)])
    def __str__(self):
        return self.company_name
    class Meta:
        verbose_name_plural = 'TradeSignals'
class TradeIdeasPerformance(models.Model):
    choices=(('BUY','BUY'),('SELL','SELL'))
    indiancompanies = SecurityMaster.objects.filter(flag='yes').values_list('fs_ticker','fs_name','security_code').order_by('fs_name')
    companies = ([[item[1],item[1]+" ("+item[2]+")"] for item in indiancompanies])
    tickers = ([[item[0],item[1]+" ("+item[2]+")"] for item in indiancompanies])
    company_name = models.CharField("Company Name", max_length=255, choices=companies)
    fs_ticker = models.CharField("FS Ticker", max_length=255, choices=tickers,null=True, blank=True)
    buy_sell=models.CharField("BUY/SELL",max_length=4,choices=choices, default='BUY')
    issue_date = models.DateTimeField("Issue date")
    purchased_price=models.CharField("Purchased Price(₹)",max_length=255,null=True, blank=True)
    target_price=models.CharField("Target Price(₹)",max_length=255,null=True, blank=True)
    time_period=models.CharField("Time Period",max_length=255,null=True, blank=True)
    performance=models.CharField("Performance",max_length=255, null=True, blank=True)
    action=models.CharField("Action",max_length=255, null=True, blank=True)
    signal_message= models.TextField("Signal_message", null=True, blank=True)
    def __str__(self):
        return str(self.company_name)
    class Meta:
        verbose_name_plural = 'TradeIdeasPerformances'
class USTradeSignals(models.Model):
    uscompanies = UsSecurityMaster.objects.values_list('fs_ticker','fs_name','security_code').order_by('fs_name')
    companies = ([[item[1],item[1]+" ("+item[2]+")"] for item in uscompanies])
    tickers = ([[item[0],item[1]+" ("+item[2]+")"] for item in uscompanies])
    diamond =  models.CharField("Only For Diamond", max_length=255, choices=(('yes','Yes'),('no','No')), default='Yes')
    company_name = models.CharField("Company Name", max_length=255, choices=companies)
    fs_ticker = models.CharField("FS Ticker", max_length=255, choices=tickers,null=True, blank=True)
    fundamental= models.TextField("Fundamental")
    fund_rating = models.PositiveIntegerField("Fundamental Rating", default=1, validators=[MinValueValidator(1), MaxValueValidator(5)])
    quantitative = models.TextField("Quantative")
    quant_rating = models.PositiveIntegerField("Quantitative Rating",default=1, validators=[MinValueValidator(1), MaxValueValidator(5)])
    technical= models.TextField("Technical")
    technical_rating = models.PositiveIntegerField("Technical Rating",default=1, validators=[MinValueValidator(1), MaxValueValidator(5)])
    situations = models.TextField("Situations")
    situations_rating = models.PositiveIntegerField("Situations Rating",default=1, validators=[MinValueValidator(1), MaxValueValidator(5)])
    def __str__(self):
        return self.company_name
    class Meta:
        verbose_name_plural = 'USTradeSignals'

class USTradeIdeasPerformance(models.Model):
    choices=(('BUY','BUY'),('SELL','SELL'))
    uscompanies = UsSecurityMaster.objects.values_list('fs_ticker','fs_name','security_code').order_by('fs_name')
    companies = ([[item[1],item[1]+" ("+item[2]+")"] for item in uscompanies])
    tickers = ([[item[0],item[1]+" ("+item[2]+")"] for item in uscompanies])
    company_name = models.CharField("Company Name", max_length=255, choices=companies)
    fs_ticker = models.CharField("FS Ticker", max_length=255, choices=tickers,null=True, blank=True)
    buy_sell=models.CharField("BUY/SELL",max_length=4,choices=choices, default='BUY')
    issue_date = models.DateTimeField("Issue date")
    purchased_price=models.CharField("Purchased Price(₹)",max_length=255,null=True, blank=True)
    target_price=models.CharField("Target Price(₹)",max_length=255,null=True, blank=True)
    time_period=models.CharField("Time Period",max_length=255,null=True, blank=True)
    performance=models.CharField("Performance",max_length=255, null=True, blank=True)
    action=models.CharField("Action",max_length=255, null=True, blank=True)
    signal_message= models.TextField("Signal_message", null=True, blank=True)
    def __str__(self):
        return str(self.company_name)
    class Meta:
        verbose_name_plural = 'USTradeIdeasPerformances'


class FuturesAndOptions(models.Model):
    country = models.CharField(max_length=5,null=True, blank=True,choices=[('India','INDIA'),('US','US')])
    companies = (SecurityMaster.objects.filter(flag='yes').values_list('fs_name','fs_name').distinct().union(UsSecurityMaster.objects.values_list('fs_name','fs_name').distinct()))
    month_choices = [('JUN-20','JUN-20'),('JUL-20','JUL-20'),('AUG-20','AUG-20'),('SEP-20','SEP-20'),('OCT-20','OCT-20'),('NOV-20','NOV-20'),('DEC-20','DEC-20')]
    company_name = models.CharField("Company Name", max_length=255, choices=companies)
    fs_ticker = models.CharField("FS_Ticker", max_length=255, null=True, blank=True)
    sector = models.CharField("Sector", max_length=255, null=True, blank=True)
    month = models.CharField("Month", max_length=255, choices=month_choices)
    begin_price=models.FloatField("Month Begin Price",null=True,blank=True)
    High_Probability_Range=models.CharField("High Probability Range",max_length=255,null=True,blank=True)
    Remark_1=models.CharField(max_length=255,null=True,blank=True)
    signal=models.CharField(max_length=255,null=True,blank=True)
    end_price=models.FloatField("Month End Price",null=True,blank=True)
    Signal_Efficiency=models.CharField(max_length=2,null=True, blank=True,choices=[('Y','Y'),('N','N')])
    start_date = models.DateField(null=True, blank=True)
    end_date = models.DateField(null=True, blank=True)
    def __str__(self):
        return self.company_name

    class Meta:
        verbose_name_plural = 'FuturesAndOptions'


class IndexFuturesandoptions(models.Model):
    country = models.CharField(max_length=30,null=True, blank=True,choices=[('India','INDIA'),('US','US')])
    index = models.CharField(db_column='Index', blank=True, null=True,max_length=255)  # Field name made lowercase.
    month = models.CharField(blank=True, null=True,max_length=255)
    begin_price = models.FloatField(blank=True, null=True)
    high_probability_range = models.TextField(db_column='High_Probability_Range', blank=True, null=True)  # Field name made lowercase.
    remark_1 = models.TextField(db_column='Remark_1', blank=True, null=True)  # Field name made lowercase.
    end_price = models.FloatField(blank=True, null=True)
    signal_efficiency = models.CharField(db_column='Signal_Efficiency', max_length=2, blank=True, null=True)  # Field name made lowercase.
    start_date = models.DateField(blank=True, null=True)
    end_date = models.DateField(blank=True, null=True)

    def __str__(self):
        return self.index

    class Meta:
        verbose_name_plural = 'NiftyFuturesandoptions'

