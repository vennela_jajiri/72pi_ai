import pandas as pd
import datetime
from datetime import date, timedelta
import pyodbc
import math

def Stocks_Investment_Returns(prices_data,stocks_data,gics_data,perType):
    try:
        stocks_data = stocks_data[['Company', 'Factset_Ticker','Quantity']]
        stocks_data = pd.merge(stocks_data,gics_data[['Factset_Ticker','GICS']],on='Factset_Ticker',how='inner')
        prices_data=pd.merge(prices_data,stocks_data,on=['Factset_Ticker','Company'],how="inner")
        prices_data['InvestmentAmount'] = prices_data ['Price'] * prices_data['Quantity']

        input_data = prices_data.copy()
        current_investment_amount= input_data.sort_values('Date', ascending=True).groupby(['Company','Factset_Ticker'], as_index=False).first()
        current_investment_amount=current_investment_amount[['Company','Factset_Ticker','GICS','InvestmentAmount']]
        current_investment_amount['InvestmentAmount%']=current_investment_amount['InvestmentAmount']/current_investment_amount['InvestmentAmount'].sum()
        investment_amount=current_investment_amount
        current_actual_data=input_data.sort_values('Date', ascending=False).groupby(['Company','Factset_Ticker'], as_index=False).first()
        current_actual_data=current_actual_data[['Company','Factset_Ticker','Date','InvestmentAmount','Price']]
        current_actual_data.columns=['Company','Factset_Ticker','CurrentDate','CurrentInvestmentAmount','CurrentPrice']
        start_actual_data=input_data.sort_values('Date', ascending=True).groupby(['Company','Factset_Ticker'], as_index=False).first()
        start_actual_data=start_actual_data[['Company','Factset_Ticker','Date','InvestmentAmount','Price']]
        start_actual_data.columns=['Company','Factset_Ticker','StartDate','StartInvestmentAmount','StartPrice']    
        actual_returns=pd.merge(current_actual_data,start_actual_data,on=['Company','Factset_Ticker'])   
        actual_returns_value=actual_returns.copy()
        actual_returns['ActualReturn_%']= (actual_returns['CurrentPrice'] - actual_returns['StartPrice'])/actual_returns['StartPrice']
        actual_returns=actual_returns[['Company','Factset_Ticker','ActualReturn_%']]
        actual_returns_value['ActualReturn_$']=actual_returns_value['CurrentInvestmentAmount']-actual_returns_value['StartInvestmentAmount']
        actual_returns_value=actual_returns_value[['Company','Factset_Ticker','ActualReturn_$','StartInvestmentAmount']]    
        final_results=pd.merge(pd.merge(investment_amount,actual_returns,on=['Factset_Ticker','Company']),actual_returns_value,on=['Company','Factset_Ticker'])    
        gics_final_results = final_results.groupby('GICS')['InvestmentAmount','InvestmentAmount%','StartInvestmentAmount','ActualReturn_$'].sum().reset_index()
        gics_final_results['ActualReturn_%']=gics_final_results['ActualReturn_$']/gics_final_results['StartInvestmentAmount']
        final_results=final_results[['Company','Factset_Ticker','InvestmentAmount','ActualReturn_%','ActualReturn_$']]
        final_results.columns=['Company','Factset_Ticker','Current Exposure',perType + '_Return',perType + '_PNL']   
        gics_final_results=gics_final_results[['GICS','InvestmentAmount','ActualReturn_%','ActualReturn_$']]
        gics_final_results.columns=['GICS','Current Exposure',perType + '_Return',perType + '_PNL']
        final_results = final_results.sort_values(by='Current Exposure',ascending=False).reset_index()
        gics_final_results = gics_final_results.sort_values(by='Current Exposure',ascending=False).reset_index()
        return final_results,gics_final_results
    except Exception as e:
        return pd.DataFrame(),pd.DataFrame()
