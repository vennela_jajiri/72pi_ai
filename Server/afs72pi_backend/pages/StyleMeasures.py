import pandas as pd
import numpy as np
import pyodbc
import datetime
from datetime import timedelta
from pandas import DataFrame
from django.conf import settings

SERVER = settings.DATABASES['default']['HOST']
PORT = settings.DATABASES['default']['PORT']
DATABASE = settings.DATABASES['default']['NAME']
UID = settings.DATABASES['default']['USER']
PWD = settings.DATABASES['default']['PASSWORD']
sql_conn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server}; SERVER='+SERVER+'; PORT='+PORT+'; DATABASE='+DATABASE+';UID='+UID+';PWD='+PWD+';')

def fillnan_values(column,change_column,df):
    df[change_column]=np.where((df[column].isnull()),np.nan,df[change_column])
    df[change_column].fillna(0,inplace=True)
    df[column].fillna(0,inplace=True)
    return df
def style_measures(country,prices_data,sec_master):
    prices_data1 = prices_data.drop_duplicates(subset='Factset_Ticker')
    output  = pd.merge(prices_data1,sec_master,left_on='Factset_Ticker',right_on='FS Ticker',how='inner')
    output["investment_amount"] = output["Price"]*output["Quantity"]
    output["PE_product"] = output["investment_amount"]*output["PE"]
    output["PB_product"] = output["investment_amount"]*output["PB"]
    output["Div_Yield_product"] = output["investment_amount"]*output["Div_Yield"]
    output["Sales_Growth_product"] = output["investment_amount"]*output["Sales_Growth"]
    output["ROE_product"] = output["investment_amount"]*(output["ROE"]/100)
    output["EPS_product"] = output["investment_amount"]*output["EPS_Growth"]
    output= fillnan_values("PE_product","investment_amount",output)
    output= fillnan_values("PB_product","investment_amount",output)
    output= fillnan_values("Div_Yield_product","investment_amount",output)
    output= fillnan_values("Sales_Growth_product","investment_amount",output)
    output= fillnan_values("ROE_product","investment_amount",output)
    output= fillnan_values("EPS_product","investment_amount",output)
    output1 = output.copy()
    output = sec_master.copy()
    output["index_PE_product"] = output["Market Cap"]*output["PE"]
    output["index_PB_product"] = output["Market Cap"]*output["PB"]
    output["index_Div_Yield_product"] = output["Market Cap"]*output["Div_Yield"]
    output["index_Sales_Growth_product"] = output["Market Cap"]*output["Sales_Growth"]
    output["index_ROE_product"] = output["Market Cap"]*(output["ROE"]/100)
    output["index_EPS_product"] = output["Market Cap"]*output["EPS_Growth"]
    output= fillnan_values("index_PE_product","Market Cap",output)
    output= fillnan_values("index_PB_product","Market Cap",output)
    output= fillnan_values("index_Div_Yield_product","Market Cap",output)
    output= fillnan_values("index_Sales_Growth_product","Market Cap",output)
    output= fillnan_values("index_ROE_product","Market Cap",output)
    output= fillnan_values("index_EPS_product","Market Cap",output)
    portfolio_list =[int(output1["PE_product"].sum()/output1["investment_amount"].sum()),
              int(output1["PB_product"].sum()/output1["investment_amount"].sum()),
              str(round(output1["Div_Yield_product"].sum()/output1["investment_amount"].sum()*100,1))+"%",
              str(round(output1["Sales_Growth_product"].sum()/output1["investment_amount"].sum()*100,1))+"%",
              str(round(output1["ROE_product"].sum()/output1["investment_amount"].sum()*100,1))+"%",
              str(round(output1["EPS_product"].sum()/output1["investment_amount"].sum()*100,1))+"%",
            ]
    factors = ['PE','PB','Div Yield','Sales Growth','ROE','EPS Growth']
    final_output=pd.DataFrame()
    final_output['Factor'] =  factors
    final_output['Portfolio'] =  portfolio_list
    index_name = {'India':["Nifty 50","Nifty Small Cap 100","Nifty Mid Cap 100"],
            'US':['S&P 500','Dow Jones 30']}
    for i in range(0,len(index_name[country])):
        index_df = output[output[index_name[country][i]]=='Yes']
        index_df = index_df[["index_PE_product","index_PB_product","index_Div_Yield_product","index_Sales_Growth_product","index_ROE_product","index_EPS_product","Market Cap"]]
        sum_list = [ int(index_df["index_PE_product"].sum()/index_df["Market Cap"].sum()),
                     int(index_df["index_PB_product"].sum()/index_df["Market Cap"].sum()),
                     str(round(index_df["index_Div_Yield_product"].sum()/index_df["Market Cap"].sum()*100,1))+"%",
                     str(round(index_df["index_Sales_Growth_product"].sum()/index_df["Market Cap"].sum()*100,1))+"%",
                     str(round(index_df["index_ROE_product"].sum()/index_df["Market Cap"].sum()*100,1))+"%",
                     str(round(index_df["index_EPS_product"].sum()/index_df["Market Cap"].sum()*100,1))+"%",
                    ]
        final_output[index_name[country][i]] = sum_list
    return final_output