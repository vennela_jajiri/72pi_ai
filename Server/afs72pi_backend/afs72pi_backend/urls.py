"""afs72pi_backend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.views.generic import TemplateView
from wagtail.admin import urls as wagtailadmin_urls
from wagtail.core import urls as wagtail_urls
from wagtail.documents import urls as wagtaildocs_urls
from blogs.api import api_router
from django.conf import settings
from django.conf.urls.static import static
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token,verify_jwt_token
from rest_auth.registration.views import VerifyEmailView

urlpatterns = [
    path('accounts/auth/', include('rest_auth.urls')),
    path('accounts/auth/signup/', include('rest_auth.registration.urls')),
    path('accounts/', include('accounts.urls')),
    path('blogs/', include('blogs.urls')),
    path('pages/', include('pages.urls')),
    path('core/<str:country>/', include('core.urls')),
    path('api-auth/', include('rest_framework.urls')),
    path('admin/', admin.site.urls),
    path('api/v2/', api_router.urls),
    path('cms/', include(wagtailadmin_urls)),
    path('accounts/auth/access-token/login/', obtain_jwt_token),
    path('accounts/auth/refresh-token/', refresh_jwt_token),
    path('accounts/auth/verify-token/', verify_jwt_token),
    path('accounts/auth/signup/account-confirm-email/', VerifyEmailView.as_view(), name='account_email_verification_sent'),
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)