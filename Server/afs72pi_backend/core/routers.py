from pages.views import CustomerPortfolioDetailsViewSet
from rest_framework import routers

router = routers.DefaultRouter()
router.register('actions', CustomerPortfolioDetailsViewSet)