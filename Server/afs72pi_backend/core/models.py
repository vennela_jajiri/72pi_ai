from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
# Create your models here.

class UniversePricesMain(models.Model):
    date = models.DateField(db_column='Date')  
    company = models.TextField(db_column='Company') 
    price = models.FloatField(db_column='Price', blank=True, null=True)  
    factset_ticker = models.TextField(db_column='Factset_Ticker', blank=True, null=True) 
    return_field = models.FloatField(db_column='Return', blank=True, null=True)
    objects = models.Manager()

    class Meta:
        db_table = 'universe_prices_main'

    def __str__(self):
        return self.company+"-"+self.date

class UsUniversePricesMain(models.Model):
    date = models.DateField(db_column='Date', blank=True, null=True)  
    company = models.TextField(db_column='Company')  
    price = models.FloatField(db_column='Price', blank=True, null=True) 
    factset_ticker = models.TextField(db_column='Factset_Ticker', blank=True, null=True) 
    return_field = models.FloatField(db_column='Return', blank=True, null=True)
    company_type = models.TextField(db_column='Company_Type', blank=True, null=True)
    objects = models.Manager() 

    class Meta:
        db_table = 'US_Universe_Prices_Main'

    def __str__(self):
        return self.company+"-"+self.date
        

class RiskFreeRate(models.Model):
    date = models.DateField(db_column='Date', blank=True, null=True)  
    risk_free_rate = models.FloatField(db_column='Risk Free Rate', blank=True, null=True) 
    class Meta:
        db_table = 'Risk_Free_Rate'

class UsRiskFreeRate(models.Model):
    date = models.DateField(db_column='Date', blank=True, null=True) 
    risk_free_rate = models.FloatField(db_column='Risk Free Rate', blank=True, null=True) 
    class Meta:
        managed = False
        db_table = 'US_Risk_Free_Rate'
   

class MarketIndex(models.Model):
    company = models.CharField(db_column='Company', max_length=255, blank=True, null=True)  
    factset_ticker = models.CharField(db_column='Factset_Ticker', max_length=255, blank=True, null=True) 
    date = models.DateField(db_column='Date', blank=True, null=True)  
    price = models.FloatField(db_column='Price', blank=True, null=True)  
    return_field = models.FloatField(db_column='Return', blank=True, null=True)  
    objects = models.Manager()
    class Meta:
        db_table = 'Market_Index'    
    def __str__(self):
        return self.company+"-"+self.date

class UsMarketIndex(models.Model):
    company = models.TextField(db_column='Company', blank=True, null=True)  # Field name made lowercase.
    factset_ticker = models.TextField(db_column='Factset_Ticker', blank=True, null=True)  # Field name made lowercase.
    date = models.DateField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    price = models.FloatField(db_column='Price', blank=True, null=True)  # Field name made lowercase.
    return_field = models.FloatField(db_column='Return', blank=True, null=True)  # Field name made lowercase. Field renamed because it was a Python reserved word.

    class Meta:
        db_table = 'US_Market_Index'

class MacroData(models.Model):
    date = models.DateTimeField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    oil_field = models.FloatField(db_column='Oil', blank=True, null=True) 
    fx_rate = models.FloatField(db_column='FX_Rate', blank=True, null=True) 
    interest_rate = models.FloatField(db_column='Interest_Rate', blank=True, null=True) 
    vix = models.FloatField(db_column='VIX', blank=True, null=True)
    inflation_rate = models.FloatField(db_column='Inflation_Rate', blank=True, null=True) 
    gold = models.FloatField(db_column='Gold', blank=True, null=True) 

    class Meta:
        db_table = 'Macro_Data'
    
    def __str__(self):
        return self.date

class UsMacroData(models.Model):
    date = models.DateField(db_column='Date', blank=True, null=True) 
    oil_field = models.FloatField(db_column='Oil', blank=True, null=True)  
    fx_rate = models.FloatField(db_column='FX_Rate', blank=True, null=True)  
    interest_rate = models.FloatField(db_column='Interest_Rate', blank=True, null=True)  
    vix = models.FloatField(db_column='VIX', blank=True, null=True)  
    inflation_rate = models.FloatField(db_column='Inflation_Rate', blank=True, null=True)  
    gold = models.FloatField(db_column='Gold', blank=True, null=True) 

    class Meta:
        db_table = 'US_Macro_Data'
    
    def __str__(self):
        return self.date


class BondEquityEarnings(models.Model):
    date = models.DateTimeField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    number_10yr_g_sec = models.FloatField(db_column='10Yr G-Sec', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    p_e = models.FloatField(db_column='P/E', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    beer = models.FloatField(db_column='Beer', blank=True, null=True)  # Field name made lowercase.
    average = models.FloatField(db_column='Average', blank=True, null=True)  # Field name made lowercase.
    std = models.FloatField(db_column='StD', blank=True, null=True)  # Field name made lowercase.
    number_1sd = models.FloatField(db_column='1sd', blank=True, null=True)  # Field renamed because it wasn't a valid Python identifier.
    number_2sd = models.FloatField(db_column='2sd', blank=True, null=True)  # Field renamed because it wasn't a valid Python identifier.
    number_3sd = models.FloatField(db_column='3sd', blank=True, null=True)  # Field renamed because it wasn't a valid Python identifier.
    field_1sd = models.FloatField(db_column='-1sd', blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it started with '_'.
    field_2sd = models.FloatField(db_column='-2sd', blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it started with '_'.
    field_3sd = models.FloatField(db_column='-3sd', blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it started with '_'.

    class Meta:
        db_table = 'bond_equity_earnings'

    def __str__(self):
        return self.date

class Nifty500Ma200(models.Model):
    date = models.DateField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    nifty500_stocks_above_200ma = models.FloatField(db_column='NIFTY500 stocks above 200MA', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    average = models.FloatField(db_column='Average', blank=True, null=True)  # Field name made lowercase.
    std = models.FloatField(db_column='StD', blank=True, null=True)  # Field name made lowercase.
    number_1sd = models.FloatField(db_column='1sd', blank=True, null=True)  # Field renamed because it wasn't a valid Python identifier.
    number_2sd = models.FloatField(db_column='2sd', blank=True, null=True)  # Field renamed because it wasn't a valid Python identifier.
    number_3sd = models.FloatField(db_column='3sd', blank=True, null=True)  # Field renamed because it wasn't a valid Python identifier.
    field_1sd = models.FloatField(db_column='-1sd', blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it started with '_'.
    field_2sd = models.FloatField(db_column='-2sd', blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it started with '_'.
    field_3sd = models.FloatField(db_column='-3sd', blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it started with '_'.

    class Meta:

        db_table = 'NIFTY500_200MA'

    def __str__(self):
        return self.date

class PriceToEarnings(models.Model):
    date = models.DateTimeField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    p_e = models.FloatField(db_column='P/E', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    average = models.FloatField(db_column='Average', blank=True, null=True)  # Field name made lowercase.
    std = models.FloatField(db_column='StD', blank=True, null=True)  # Field name made lowercase.
    number_1sd = models.FloatField(db_column='1sd', blank=True, null=True)  # Field renamed because it wasn't a valid Python identifier.
    number_2sd = models.FloatField(db_column='2sd', blank=True, null=True)  # Field renamed because it wasn't a valid Python identifier.
    number_3sd = models.FloatField(db_column='3sd', blank=True, null=True)  # Field renamed because it wasn't a valid Python identifier.
    field_1sd = models.FloatField(db_column='-1sd', blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it started with '_'.
    field_2sd = models.FloatField(db_column='-2sd', blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it started with '_'.
    field_3sd = models.FloatField(db_column='-3sd', blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it started with '_'.

    class Meta:
        db_table = 'price_to_earnings'

    def __str__(self):
        return self.date
        
class IndiaVix(models.Model):
    date = models.DateField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    vix = models.FloatField(db_column='VIX', blank=True, null=True)  # Field name made lowercase.
    average = models.FloatField(db_column='Average', blank=True, null=True)  # Field name made lowercase.
    std = models.FloatField(db_column='StD', blank=True, null=True)  # Field name made lowercase.
    number_1sd = models.FloatField(db_column='1sd', blank=True, null=True)  # Field renamed because it wasn't a valid Python identifier.
    number_2sd = models.FloatField(db_column='2sd', blank=True, null=True)  # Field renamed because it wasn't a valid Python identifier.
    number_3sd = models.FloatField(db_column='3sd', blank=True, null=True)  # Field renamed because it wasn't a valid Python identifier.
    field_1sd = models.FloatField(db_column='-1sd', blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it started with '_'.
    field_2sd = models.FloatField(db_column='-2sd', blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it started with '_'.
    field_3sd = models.FloatField(db_column='-3sd', blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it started with '_'.

    class Meta:
 
        db_table = 'india_vix'

class EtfMonitor(models.Model):
    date = models.DateField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    security_code = models.TextField(db_column='Security_Code', blank=True, null=True)  # Field name made lowercase.
    fs_ticker = models.TextField(db_column='FS_Ticker', blank=True, null=True)  # Field name made lowercase.
    company_name = models.TextField(db_column='Company_Name', blank=True, null=True)  # Field name made lowercase.
    asset_class = models.TextField(db_column='Asset Class', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    region_country = models.TextField(db_column='Region/Country', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    category = models.TextField(db_column='Category', blank=True, null=True)  # Field name made lowercase.
    issuer = models.TextField(db_column='Issuer', blank=True, null=True)  # Field name made lowercase.
    adj_price_close = models.FloatField(db_column='Adj Price Close', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    number_15d = models.FloatField(db_column='15D', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    mtd = models.FloatField(db_column='MTD', blank=True, null=True)  # Field name made lowercase.
    qtd = models.FloatField(db_column='QTD', blank=True, null=True)  # Field name made lowercase.
    ytd = models.FloatField(db_column='YTD', blank=True, null=True)  # Field name made lowercase.
    number_1m = models.FloatField(db_column='1M', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_3m = models.FloatField(db_column='3M', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_6m = models.FloatField(db_column='6M', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_12m = models.FloatField(db_column='12M', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    ema9 = models.FloatField(db_column='EMA9', blank=True, null=True)  # Field name made lowercase.
    ema50 = models.FloatField(db_column='EMA50', blank=True, null=True)  # Field name made lowercase.
    ema200 = models.FloatField(db_column='EMA200', blank=True, null=True)  # Field name made lowercase.
    number_50ema_200ema = models.TextField(db_column='50EMA>200EMA', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_50ema_200ema_0 = models.TextField(db_column='50EMA<200EMA', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier. Field renamed because of name conflict.
    rsi_14d = models.FloatField(db_column='RSI_14D', blank=True, null=True)  # Field name made lowercase.
    adx = models.FloatField(db_column='ADX', blank=True, null=True)  # Field name made lowercase.
    rsi_14d_weekly = models.FloatField(db_column='RSI_14D_WEEKLY', blank=True, null=True)  # Field name made lowercase.
    adx_weekly = models.FloatField(db_column='ADX_WEEKLY', blank=True, null=True)  # Field name made lowercase.
    priority = models.CharField(db_column='Priority', max_length=5, blank=True, null=True)  # Field name made lowercase.
    short_term = models.CharField(db_column='Short Term', max_length=5, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    intermediate_term = models.CharField(db_column='Intermediate Term', max_length=5, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    type = models.CharField(db_column='Type', max_length=5, blank=True, null=True)  # Field name made lowercase.
    market_view_flag = models.CharField(db_column='Market_View_Flag', max_length=5, blank=True, null=True)  # Field name made lowercase.
    short_name = models.CharField(db_column='Short Name', max_length=50, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    order = models.IntegerField(db_column='Order', blank=True, null=True)  # Field name made lowercase.
    class Meta:
        managed = False
        db_table = 'ETF_Monitor'
class UsEtfMonitor(models.Model):
    date = models.DateField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    security_code = models.TextField(db_column='Security_Code', blank=True, null=True)  # Field name made lowercase.
    fs_ticker = models.TextField(db_column='FS_Ticker', blank=True, null=True)  # Field name made lowercase.
    company_name = models.TextField(db_column='Company_Name', blank=True, null=True)  # Field name made lowercase.
    asset_class = models.TextField(db_column='Asset Class', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    region_country = models.TextField(db_column='Region/Country', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    category = models.TextField(db_column='Category', blank=True, null=True)  # Field name made lowercase.
    issuer = models.TextField(db_column='Issuer', blank=True, null=True)  # Field name made lowercase.
    adj_price_close = models.FloatField(db_column='Adj Price Close', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    number_15d = models.FloatField(db_column='15D', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    mtd = models.FloatField(db_column='MTD', blank=True, null=True)  # Field name made lowercase.
    qtd = models.FloatField(db_column='QTD', blank=True, null=True)  # Field name made lowercase.
    ytd = models.FloatField(db_column='YTD', blank=True, null=True)  # Field name made lowercase.
    number_1m = models.FloatField(db_column='1M', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_3m = models.FloatField(db_column='3M', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_6m = models.FloatField(db_column='6M', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_12m = models.FloatField(db_column='12M', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    ema9 = models.FloatField(db_column='EMA9', blank=True, null=True)  # Field name made lowercase.
    ema50 = models.FloatField(db_column='EMA50', blank=True, null=True)  # Field name made lowercase.
    ema200 = models.FloatField(db_column='EMA200', blank=True, null=True)  # Field name made lowercase.
    number_50ema_200ema = models.TextField(db_column='50EMA>200EMA', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_50ema_200ema_0 = models.TextField(db_column='50EMA<200EMA', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier. Field renamed because of name conflict.
    rsi_14d = models.FloatField(db_column='RSI_14D', blank=True, null=True)  # Field name made lowercase.
    adx = models.FloatField(db_column='ADX', blank=True, null=True)  # Field name made lowercase.
    rsi_14d_weekly = models.FloatField(db_column='RSI_14D_WEEKLY', blank=True, null=True)  # Field name made lowercase.
    adx_weekly = models.FloatField(db_column='ADX_WEEKLY', blank=True, null=True)  # Field name made lowercase.
    short_term = models.TextField(db_column='Short Term', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    intermediate_term = models.TextField(db_column='Intermediate Term', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    short_name = models.TextField(db_column='Short Name', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    priority = models.CharField(db_column='Priority', max_length=5, blank=True, null=True)  # Field name made lowercase.
    type = models.TextField(db_column='Type', blank=True, null=True)  # Field name made lowercase.
    market_view_flag = models.CharField(db_column='Market_View_Flag', max_length=5, blank=True, null=True)  # Field name made lowercase.
    order = models.IntegerField(db_column='Order', blank=True, null=True)  # Field name made lowercase.
    class Meta:
        managed = False
        db_table = 'US_ETF_Monitor'


class MarketIndia(models.Model):
    returns = models.TextField(db_column='Returns', blank=True, null=True)  # Field name made lowercase.
    bse_sensex = models.TextField(db_column='BSE Sensex', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    nifty_50 = models.TextField(db_column='Nifty 50', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    s_p_500 = models.TextField(db_column='S&P 500', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    russell_2000 = models.TextField(db_column='Russell 2000', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    oil = models.TextField(db_column='Oil', blank=True, null=True)  # Field name made lowercase.
    gold = models.TextField(db_column='Gold', blank=True, null=True)  # Field name made lowercase.
    usd_inr = models.TextField(db_column='USD/INR', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    max_date = models.DateTimeField(db_column='Max_Date', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        db_table = 'Market_India'

class MarketUs(models.Model):
    returns = models.TextField(db_column='Returns', blank=True, null=True)  # Field name made lowercase.
    s_p_500 = models.TextField(db_column='S&P 500', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    russell_2000 = models.TextField(db_column='Russell 2000', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    dow_jones_30 = models.TextField(db_column='Dow Jones 30', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    bse_sensex = models.TextField(db_column='BSE Sensex', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    nifty_50 = models.TextField(db_column='Nifty 50', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    oil = models.TextField(db_column='Oil', blank=True, null=True)  # Field name made lowercase.
    gold = models.TextField(db_column='Gold', blank=True, null=True)  # Field name made lowercase.
    usd_inr = models.TextField(db_column='USD/INR', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    max_date = models.DateTimeField(db_column='Max_Date', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        db_table = 'Market_US'


class ModelPortfolioDetails(models.Model):
    company_name = models.CharField(db_column='Company Name', max_length=255)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    quantity = models.IntegerField(db_column='Quantity', blank=True, null=True)  # Field name made lowercase.
    model_portfolio_name = models.CharField(db_column='Model Portfolio Name', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    country = models.CharField(db_column='Country', max_length=25, blank=True, null=True)  # Field name made lowercase.
    sector_name = models.CharField(db_column='Sector Name', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    add_on = models.CharField(db_column='Add On', max_length=5, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    created_at = models.DateTimeField(blank=True, null=True)
    fs_ticker = models.TextField(db_column='FS_Ticker', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        db_table = 'Model_Portfolio_Details'
        
class SecurityMaster(models.Model):
    security_code = models.CharField(db_column='Security Code', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    security_id = models.CharField(db_column='Security Id', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    isin_no = models.CharField(db_column='ISIN No', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    fs_ticker = models.CharField(db_column='FS Ticker', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    bbg_ticker = models.CharField(db_column='BBg Ticker', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    sedol = models.CharField(db_column='SEDOL', max_length=255, blank=True, null=True)  # Field name made lowercase.
    security_name = models.CharField(db_column='Security Name', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    fs_name = models.CharField(db_column='FS Name', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    market_cap = models.FloatField(db_column='Market Cap', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    gics = models.CharField(db_column='GICS', max_length=255, blank=True, null=True)  # Field name made lowercase.
    market_cap_category = models.CharField(db_column='Market_Cap_Category', max_length=50, blank=True, null=True)  # Field name made lowercase.
    flag = models.CharField(db_column='Flag', max_length=10, blank=True, null=True)  # Field name made lowercase.
    beta_1y = models.FloatField(db_column='Beta_1Y', blank=True, null=True)  # Field name made lowercase.
    volatility_1y = models.FloatField(db_column='Volatility_1Y', blank=True, null=True)  # Field name made lowercase.
    idiosyncratic_vol = models.FloatField(db_column='Idiosyncratic_Vol', blank=True, null=True)  # Field name made lowercase.
    mcap_rm_field = models.FloatField(db_column='Mcap(RM)', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it ended with '_'.
    sales_rm_field = models.FloatField(db_column='Sales(RM)', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it ended with '_'.
    pe = models.FloatField(db_column='PE', blank=True, null=True)  # Field name made lowercase.
    pb = models.FloatField(db_column='PB', blank=True, null=True)  # Field name made lowercase.
    div_yield = models.FloatField(db_column='Div_Yield', blank=True, null=True)  # Field name made lowercase.
    div_payout = models.FloatField(db_column='Div_Payout', blank=True, null=True)  # Field name made lowercase.
    sales_growth = models.FloatField(db_column='Sales_Growth', blank=True, null=True)  # Field name made lowercase.
    sales_growth_qoq_growth = models.FloatField(db_column='Sales_Growth_QoQ_Growth', blank=True, null=True)  # Field name made lowercase.
    sales_growth_3y_5r_cagr = models.FloatField(db_column='Sales_Growth_3Y/5R_CAGR', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    eps_growth = models.FloatField(db_column='EPS_Growth', blank=True, null=True)  # Field name made lowercase.
    eps_growth_qoq_growth = models.FloatField(db_column='EPS_Growth_QoQ_Growth', blank=True, null=True)  # Field name made lowercase.
    eps_growth_3y_5r_cagr = models.FloatField(db_column='EPS_Growth_3Y/5R_CAGR', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    roe = models.FloatField(db_column='ROE', blank=True, null=True)  # Field name made lowercase.
    roce = models.FloatField(db_column='ROCE', blank=True, null=True)  # Field name made lowercase.
    ebitda_margin = models.FloatField(db_column='EBITDA_Margin', blank=True, null=True)  # Field name made lowercase.
    net_income_margin = models.FloatField(db_column='Net_Income_Margin', blank=True, null=True)  # Field name made lowercase.
    fcf_margin = models.FloatField(db_column='FCF_Margin', blank=True, null=True)  # Field name made lowercase.
    sales_total_assets = models.FloatField(db_column='Sales/Total Assets', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    net_debt_fcf = models.FloatField(db_column='Net_Debt/FCF', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    net_debt_ebitda = models.FloatField(db_column='Net_Debt/EBITDA', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    debt_equity = models.FloatField(db_column='Debt/Equity', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    piotroski_score = models.FloatField(db_column='Piotroski_Score', blank=True, null=True)  # Field name made lowercase.
    nifty_50 = models.CharField(db_column='Nifty 50', max_length=25, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    nifty_small_cap_100 = models.CharField(db_column='Nifty Small Cap 100', max_length=25, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    nifty_mid_cap_100 = models.CharField(db_column='Nifty Mid Cap 100', max_length=25, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    shares_outstanding = models.BigIntegerField(db_column='Shares Outstanding', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ev_sales = models.FloatField(db_column='EV/Sales', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ev_ebitda = models.FloatField(db_column='EV/EBITDA', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    nseflag = models.CharField(max_length=255, blank=True, null=True)
    bseflag = models.CharField(max_length=255, blank=True, null=True)
    qtr_eps_growth_yoy = models.FloatField(db_column='QTR EPS_Growth_YoY', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    qtr_sales_growth_yoy = models.FloatField(db_column='QTR Sales_Growth_YoY', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ev = models.FloatField(db_column='EV', blank=True, null=True)  # Field name made lowercase.
    etf_flag = models.TextField(db_column='ETF_Flag', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField(primary_key=True)

    class Meta:
        managed = False
        db_table = 'Security_Master'

class UsSecurityMaster(models.Model):
    isin_no = models.CharField(db_column='ISIN No', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    security_code = models.TextField(db_column='Security Code', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    bbg_ticker = models.TextField(db_column='BBG_Ticker', blank=True, null=True)  # Field name made lowercase.
    fs_ticker = models.CharField(db_column='FS Ticker', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    fs_name = models.CharField(db_column='FS Name', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    market_cap = models.FloatField(db_column='Market Cap', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    gics = models.CharField(db_column='GICS', max_length=255, blank=True, null=True)  # Field name made lowercase.
    market_cap_category = models.CharField(db_column='Market_Cap_Category', max_length=255, blank=True, null=True)  # Field name made lowercase.
    beta_1y = models.FloatField(db_column='Beta_1Y', blank=True, null=True)  # Field name made lowercase.
    volatility_1y = models.FloatField(db_column='Volatility_1Y', blank=True, null=True)  # Field name made lowercase.
    idiosyncratic_vol = models.FloatField(db_column='Idiosyncratic_Vol', blank=True, null=True)  # Field name made lowercase.
    mcap_rm_field = models.FloatField(db_column='Mcap(RM)', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it ended with '_'.
    sales_rm_field = models.FloatField(db_column='Sales(RM)', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it ended with '_'.
    pe = models.FloatField(db_column='PE', blank=True, null=True)  # Field name made lowercase.
    pb = models.FloatField(db_column='PB', blank=True, null=True)  # Field name made lowercase.
    div_yield = models.FloatField(db_column='Div_Yield', blank=True, null=True)  # Field name made lowercase.
    div_payout = models.FloatField(db_column='Div_Payout', blank=True, null=True)  # Field name made lowercase.
    sales_growth = models.FloatField(db_column='Sales_Growth', blank=True, null=True)  # Field name made lowercase.
    sales_growth_qoq_growth = models.FloatField(db_column='Sales_Growth_QoQ_Growth', blank=True, null=True)  # Field name made lowercase.
    sales_growth_3y_5r_cagr = models.FloatField(db_column='Sales_Growth_3Y/5R_CAGR', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    eps_growth = models.FloatField(db_column='EPS_Growth', blank=True, null=True)  # Field name made lowercase.
    eps_growth_qoq_growth = models.FloatField(db_column='EPS_Growth_QoQ_Growth', blank=True, null=True)  # Field name made lowercase.
    eps_growth_3y_5r_cagr = models.FloatField(db_column='EPS_Growth_3Y/5R_CAGR', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    roe = models.FloatField(db_column='ROE', blank=True, null=True)  # Field name made lowercase.
    roce = models.FloatField(db_column='ROCE', blank=True, null=True)  # Field name made lowercase.
    ebitda_margin = models.FloatField(db_column='EBITDA_Margin', blank=True, null=True)  # Field name made lowercase.
    net_income_margin = models.FloatField(db_column='Net_Income_Margin', blank=True, null=True)  # Field name made lowercase.
    fcf_margin = models.FloatField(db_column='FCF_Margin', blank=True, null=True)  # Field name made lowercase.
    sales_total_assets = models.FloatField(db_column='Sales/Total Assets', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    net_debt_fcf = models.FloatField(db_column='Net_Debt/FCF', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    net_debt_ebitda = models.FloatField(db_column='Net_Debt/EBITDA', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    debt_equity = models.FloatField(db_column='Debt/Equity', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    piotroski_score = models.FloatField(db_column='Piotroski_Score', blank=True, null=True)  # Field name made lowercase.
    s_p_500 = models.CharField(db_column='S&P 500', max_length=25, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    dow_jones_30 = models.CharField(db_column='Dow Jones 30', max_length=25, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    shares_outstanding = models.FloatField(db_column='Shares Outstanding', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ev_sales = models.FloatField(db_column='EV/Sales', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ev_ebitda = models.FloatField(db_column='EV/EBITDA', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    qtr_eps_growth_yoy = models.FloatField(db_column='QTR EPS_Growth_YoY', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    qtr_sales_growth_yoy = models.FloatField(db_column='QTR Sales_Growth_YoY', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ev = models.FloatField(db_column='EV', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField(primary_key=True)
    flag = models.CharField(db_column='Flag', max_length=255, blank=True, null=True)  # Field name made lowercase.
    etf_flag = models.CharField(db_column='ETF_Flag', max_length=255, blank=True, null=True)  # Field name made lowercase.
    class Meta:
        managed = False
        db_table = 'US_Security_Master'

class CboeVix(models.Model):
    date = models.DateTimeField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    vix = models.FloatField(db_column='VIX', blank=True, null=True)  # Field name made lowercase.
    average = models.FloatField(db_column='Average', blank=True, null=True)  # Field name made lowercase.
    std = models.FloatField(db_column='StD', blank=True, null=True)  # Field name made lowercase.
    number_1sd = models.FloatField(db_column='1sd', blank=True, null=True)  # Field renamed because it wasn't a valid Python identifier.
    number_2sd = models.FloatField(db_column='2sd', blank=True, null=True)  # Field renamed because it wasn't a valid Python identifier.
    number_3sd = models.FloatField(db_column='3sd', blank=True, null=True)  # Field renamed because it wasn't a valid Python identifier.
    field_1sd = models.FloatField(db_column='-1sd', blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it started with '_'.
    field_2sd = models.FloatField(db_column='-2sd', blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it started with '_'.
    field_3sd = models.FloatField(db_column='-3sd', blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it started with '_'.

    class Meta:
        db_table = 'CBOE_VIX'


class Snp500SchillerPriceToEarnings(models.Model):
    date = models.DateTimeField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    p_e = models.FloatField(db_column='P/E', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    average = models.FloatField(db_column='Average', blank=True, null=True)  # Field name made lowercase.
    std = models.FloatField(db_column='StD', blank=True, null=True)  # Field name made lowercase.
    number_1sd = models.FloatField(db_column='1sd', blank=True, null=True)  # Field renamed because it wasn't a valid Python identifier.
    number_2sd = models.FloatField(db_column='2sd', blank=True, null=True)  # Field renamed because it wasn't a valid Python identifier.
    number_3sd = models.FloatField(db_column='3sd', blank=True, null=True)  # Field renamed because it wasn't a valid Python identifier.
    field_1sd = models.FloatField(db_column='-1sd', blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it started with '_'.
    field_2sd = models.FloatField(db_column='-2sd', blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it started with '_'.
    field_3sd = models.FloatField(db_column='-3sd', blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it started with '_'.

    class Meta:
        db_table = 'SnP500_Schiller_Price_to_Earnings'


class Snp500TtmPriceToEarnings(models.Model):
    date = models.DateTimeField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    p_e_ttm = models.FloatField(db_column='P/E', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    average = models.FloatField(db_column='Average', blank=True, null=True)  # Field name made lowercase.
    std = models.FloatField(db_column='StD', blank=True, null=True)  # Field name made lowercase.
    number_1sd = models.FloatField(db_column='1sd', blank=True, null=True)  # Field renamed because it wasn't a valid Python identifier.
    number_2sd = models.FloatField(db_column='2sd', blank=True, null=True)  # Field renamed because it wasn't a valid Python identifier.
    number_3sd = models.FloatField(db_column='3sd', blank=True, null=True)  # Field renamed because it wasn't a valid Python identifier.
    field_1sd = models.FloatField(db_column='-1sd', blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it started with '_'.
    field_2sd = models.FloatField(db_column='-2sd', blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it started with '_'.
    field_3sd = models.FloatField(db_column='-3sd', blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it started with '_'.

    class Meta:
        db_table = 'SnP500_TTM_Price_to_Earnings'


class IndexPerformance(models.Model):
    ticker = models.TextField(db_column='Ticker', blank=True, null=True)  # Field name made lowercase.
    period = models.TextField(db_column='Period', blank=True, null=True)  # Field name made lowercase.
    return_field = models.FloatField(db_column='Return', blank=True, null=True)  # Field name made lowercase. Field renamed because it was a Python reserved word.
    date = models.DateField(db_column='downloadDate',blank=True, null=True)

    class Meta:
        db_table = 'Index_Performance'

class UsIndexPerformance(models.Model):
    ticker = models.TextField(db_column='Ticker', blank=True, null=True)  # Field name made lowercase.
    period = models.TextField(db_column='Period', blank=True, null=True)  # Field name made lowercase.
    return_field = models.FloatField(db_column='Return', blank=True, null=True)  # Field name made lowercase. Field renamed because it was a Python reserved word.
    date = models.DateField(db_column='downloadDate',blank=True, null=True)

    class Meta:
        db_table = 'US_Index_Performance'

class SectorIndicesPEPBPerformance(models.Model):
    gics = models.CharField(db_column='GICS', max_length=255, blank=True, null=True)  # Field name made lowercase.
    factor = models.CharField(db_column='Factor', max_length=255, blank=True, null=True)  # Field name made lowercase.
    date = models.CharField(db_column='Date', max_length=255, blank=True, null=True)  # Field name made lowercase.
    value = models.FloatField(db_column='Value', blank=True, null=True)  # Field name made lowercase.
    date_new = models.DateField(blank=True, null=True)

    class Meta:
        db_table = 'Sector_Indices_PE_PB_Performance'

class HistoricalMultiples(models.Model):
    date = models.DateTimeField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    factor = models.TextField(db_column='Factor', blank=True, null=True)  # Field name made lowercase.
    ticker = models.TextField(db_column='Ticker', blank=True, null=True)  # Field name made lowercase.
    value = models.FloatField(blank=True, null=True)

    class Meta:
        db_table = 'Historical_Multiples'

class UsHistoricalMultiples(models.Model):
    date = models.DateField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    factor = models.TextField(db_column='Factor', blank=True, null=True)  # Field name made lowercase.
    ticker = models.TextField(db_column='Ticker', blank=True, null=True)  # Field name made lowercase.
    value = models.FloatField(blank=True, null=True)

    class Meta:
        db_table = 'Us_Historical_Multiples'


class MovingAverage(models.Model):
    date = models.DateField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    company = models.TextField(db_column='Company')  # Field name made lowercase.
    price = models.FloatField(db_column='Price', blank=True, null=True)  # Field name made lowercase.
    factset_ticker = models.TextField(db_column='Factset_Ticker', blank=True, null=True)  # Field name made lowercase.
    ma9 = models.FloatField(db_column='MA9', blank=True, null=True)  # Field name made lowercase.
    ma20 = models.FloatField(db_column='MA20', blank=True, null=True)  # Field name made lowercase.
    ma26 = models.FloatField(db_column='MA26', blank=True, null=True)  # Field name made lowercase.
    ma50 = models.FloatField(db_column='MA50', blank=True, null=True)  # Field name made lowercase.
    ma100 = models.FloatField(db_column='MA100', blank=True, null=True)  # Field name made lowercase.
    ma200 = models.FloatField(db_column='MA200', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        db_table = 'Moving_Average'


class UsMovingAverage(models.Model):
    date = models.DateField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    company = models.TextField(db_column='Company')  # Field name made lowercase.
    price = models.FloatField(db_column='Price', blank=True, null=True)  # Field name made lowercase.
    factset_ticker = models.TextField(db_column='Factset_Ticker', blank=True, null=True)  # Field name made lowercase.
    ma9 = models.FloatField(db_column='MA9', blank=True, null=True)  # Field name made lowercase.
    ma20 = models.FloatField(db_column='MA20', blank=True, null=True)  # Field name made lowercase.
    ma26 = models.FloatField(db_column='MA26', blank=True, null=True)  # Field name made lowercase.
    ma50 = models.FloatField(db_column='MA50', blank=True, null=True)  # Field name made lowercase.
    ma100 = models.FloatField(db_column='MA100', blank=True, null=True)  # Field name made lowercase.
    ma200 = models.FloatField(db_column='MA200', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        db_table = 'US_Moving_Average'


class CompanyDescriptions(models.Model):
    co_code = models.CharField(max_length=255, blank=True, null=True)
    company = models.TextField(db_column='Company', blank=True, null=True)  # Field name made lowercase.
    description = models.TextField(db_column='Description', blank=True, null=True)  # Field name made lowercase.
    shortdescription = models.TextField(db_column='ShortDescription', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        db_table = 'Company_Descriptions'

class UsCompanyDescriptions(models.Model):
    co_code = models.TextField(blank=True, null=True)
    company = models.TextField(db_column='Company', blank=True, null=True)  # Field name made lowercase.
    description = models.TextField(db_column='Description', blank=True, null=True)  # Field name made lowercase.
    shortdescription = models.TextField(db_column='ShortDescription', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        db_table = 'US_Company_Descriptions'



class CompanyAddressDetails2(models.Model):
    lname = models.TextField(db_column='LNAME', blank=True, null=True)  # Field name made lowercase.
    isin = models.TextField(db_column='ISIN', blank=True, null=True)  # Field name made lowercase.
    hse_s_name = models.TextField(db_column='HSE_S_NAME', blank=True, null=True)  # Field name made lowercase.
    inc_dt = models.TextField(db_column='INC_DT', blank=True, null=True)  # Field name made lowercase.
    regadd1 = models.TextField(db_column='REGADD1', blank=True, null=True)  # Field name made lowercase.
    regadd2 = models.TextField(db_column='REGADD2', blank=True, null=True)  # Field name made lowercase.
    regdist = models.TextField(db_column='REGDIST', blank=True, null=True)  # Field name made lowercase.
    regstate = models.TextField(db_column='REGSTATE', blank=True, null=True)  # Field name made lowercase.
    regpin = models.TextField(db_column='REGPIN', blank=True, null=True)  # Field name made lowercase.
    tel1 = models.TextField(db_column='TEL1', blank=True, null=True)  # Field name made lowercase.
    ind_l_name = models.TextField(blank=True, null=True)
    tel2 = models.TextField(db_column='TEL2', blank=True, null=True)  # Field name made lowercase.
    fax1 = models.TextField(db_column='FAX1', blank=True, null=True)  # Field name made lowercase.
    fax2 = models.TextField(db_column='FAX2', blank=True, null=True)  # Field name made lowercase.
    auditor = models.TextField(db_column='AUDITOR', blank=True, null=True)  # Field name made lowercase.
    fv = models.FloatField(db_column='FV', blank=True, null=True)  # Field name made lowercase.
    mkt_lot = models.BigIntegerField(db_column='MKT_LOT', blank=True, null=True)  # Field name made lowercase.
    chairman = models.TextField(db_column='CHAIRMAN', blank=True, null=True)  # Field name made lowercase.
    co_sec = models.TextField(db_column='CO_SEC', blank=True, null=True)  # Field name made lowercase.
    co_code = models.FloatField(db_column='CO_CODE', blank=True, null=True)  # Field name made lowercase.
    email = models.TextField(db_column='EMAIL', blank=True, null=True)  # Field name made lowercase.
    internet = models.TextField(db_column='INTERNET', blank=True, null=True)  # Field name made lowercase.
    dir_name = models.TextField(blank=True, null=True)
    dir_desg = models.TextField(blank=True, null=True)
    ticker = models.TextField(db_column='Ticker', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        db_table = 'Company_address_details_2'

    
class UsCompanyAddressDetails2(models.Model):
    ticker = models.TextField(db_column='Ticker', blank=True, null=True)  # Field name made lowercase.
    lname = models.TextField(db_column='LNAME', blank=True, null=True)  # Field name made lowercase.
    regdist = models.TextField(db_column='REGDIST', blank=True, null=True)  # Field name made lowercase.
    regstate = models.TextField(db_column='REGSTATE', blank=True, null=True)  # Field name made lowercase.
    ind_l_name = models.TextField(blank=True, null=True)
    tel1 = models.TextField(db_column='TEL1', blank=True, null=True)  # Field name made lowercase.
    internet = models.TextField(db_column='INTERNET', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        db_table = 'US_Company_address_details_2'



class StockRanking(models.Model):
    date = models.DateField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    co_code = models.FloatField(blank=True, null=True)
    nsesymbol = models.TextField(blank=True, null=True)
    fs_name = models.TextField(db_column='FS Name', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    fs_ticker = models.TextField(db_column='FS Ticker', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    sales = models.FloatField(db_column='Sales', blank=True, null=True)  # Field name made lowercase.
    piotroski_score = models.FloatField(db_column='Piotroski_Score', blank=True, null=True)  # Field name made lowercase.
    roe = models.FloatField(db_column='ROE', blank=True, null=True)  # Field name made lowercase.
    debt_equity = models.FloatField(db_column='Debt/Equity', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    fcf_margin = models.FloatField(db_column='FCF_Margin', blank=True, null=True)  # Field name made lowercase.
    beta = models.FloatField(db_column='Beta', blank=True, null=True)  # Field name made lowercase.
    sales_growth = models.FloatField(db_column='Sales_Growth', blank=True, null=True)  # Field name made lowercase.
    eps_growth = models.FloatField(db_column='EPS_Growth', blank=True, null=True)  # Field name made lowercase.
    sales_qoq = models.FloatField(db_column='Sales_QoQ', blank=True, null=True)  # Field name made lowercase.
    price_vs_ema20 = models.FloatField(db_column='Price_vs_EMA20', blank=True, null=True)  # Field name made lowercase.
    price_vs_ema50 = models.FloatField(db_column='Price_vs_EMA50', blank=True, null=True)  # Field name made lowercase.
    price_vs_ema200 = models.FloatField(db_column='Price_vs_EMA200', blank=True, null=True)  # Field name made lowercase.
    ema50_vs_ema200 = models.FloatField(db_column='EMA50_vs_EMA200', blank=True, null=True)  # Field name made lowercase.
    current_price_vs_52_week_high = models.FloatField(db_column='Current_Price_vs_52_Week_High', blank=True, null=True)  # Field name made lowercase.
    rsi = models.FloatField(db_column='RSI', blank=True, null=True)  # Field name made lowercase.
    price = models.FloatField(db_column='Price', blank=True, null=True)  # Field name made lowercase.
    return_field = models.FloatField(db_column='Return', blank=True, null=True)  # Field name made lowercase. Field renamed because it was a Python reserved word.
    net_change = models.FloatField(db_column='Net Change', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    q_sales = models.FloatField(db_column='Q_Sales', blank=True, null=True)  # Field name made lowercase.
    q_piotroski_score = models.FloatField(db_column='Q_Piotroski_Score', blank=True, null=True)  # Field name made lowercase.
    q_roe = models.FloatField(db_column='Q_ROE', blank=True, null=True)  # Field name made lowercase.
    q_debt_equity = models.FloatField(db_column='Q_Debt/Equity', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    q_fcf_margin = models.FloatField(db_column='Q_FCF_Margin', blank=True, null=True)  # Field name made lowercase.
    q_beta = models.IntegerField(db_column='Q_Beta', blank=True, null=True)  # Field name made lowercase.
    g_eps_growth = models.FloatField(db_column='G_EPS_Growth', blank=True, null=True)  # Field name made lowercase.
    g_sales_growth = models.FloatField(db_column='G_Sales_Growth', blank=True, null=True)  # Field name made lowercase.
    g_sales_qoq = models.FloatField(db_column='G_Sales_QoQ', blank=True, null=True)  # Field name made lowercase.
    t_price_vs_ema20 = models.IntegerField(db_column='T_Price_vs_EMA20', blank=True, null=True)  # Field name made lowercase.
    t_price_vs_ema50 = models.IntegerField(db_column='T_Price_vs_EMA50', blank=True, null=True)  # Field name made lowercase.
    t_price_vs_ema200 = models.IntegerField(db_column='T_Price_vs_EMA200', blank=True, null=True)  # Field name made lowercase.
    t_rsi = models.IntegerField(db_column='T_RSI', blank=True, null=True)  # Field name made lowercase.
    t_ema50_vs_ema200 = models.IntegerField(db_column='T_EMA50_vs_EMA200', blank=True, null=True)  # Field name made lowercase.
    t_current_price_vs_52_week_high = models.IntegerField(db_column='T_Current_Price_vs_52_Week_High', blank=True, null=True)  # Field name made lowercase.
    quality_score = models.FloatField(db_column='Quality_Score', blank=True, null=True)  # Field name made lowercase.
    growth_score = models.FloatField(db_column='Growth_Score', blank=True, null=True)  # Field name made lowercase.
    technical_score = models.FloatField(db_column='Technical_Score', blank=True, null=True)  # Field name made lowercase.
    stock_rank = models.FloatField(db_column='Stock_Rank', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        db_table = 'Stock_Ranking'

class UsStockRanking(models.Model):
    date = models.DateTimeField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    co_code = models.TextField(blank=True, null=True)
    nsesymbol = models.TextField(blank=True, null=True)
    fs_name = models.TextField(db_column='FS Name', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    fs_ticker = models.TextField(db_column='FS Ticker', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    sales = models.FloatField(db_column='Sales', blank=True, null=True)  # Field name made lowercase.
    piotroski_score = models.FloatField(db_column='Piotroski_Score', blank=True, null=True)  # Field name made lowercase.
    roe = models.FloatField(db_column='ROE', blank=True, null=True)  # Field name made lowercase.
    debt_equity = models.FloatField(db_column='Debt/Equity', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    fcf_margin = models.FloatField(db_column='FCF_Margin', blank=True, null=True)  # Field name made lowercase.
    beta = models.FloatField(db_column='Beta', blank=True, null=True)  # Field name made lowercase.
    sales_growth = models.FloatField(db_column='Sales_Growth', blank=True, null=True)  # Field name made lowercase.
    eps_growth = models.FloatField(db_column='EPS_Growth', blank=True, null=True)  # Field name made lowercase.
    sales_qoq = models.FloatField(db_column='Sales_QoQ', blank=True, null=True)  # Field name made lowercase.
    price_vs_ema20 = models.FloatField(db_column='Price_vs_EMA20', blank=True, null=True)  # Field name made lowercase.
    price_vs_ema50 = models.FloatField(db_column='Price_vs_EMA50', blank=True, null=True)  # Field name made lowercase.
    price_vs_ema200 = models.FloatField(db_column='Price_vs_EMA200', blank=True, null=True)  # Field name made lowercase.
    ema50_vs_ema200 = models.FloatField(db_column='EMA50_vs_EMA200', blank=True, null=True)  # Field name made lowercase.
    current_price_vs_52_week_high = models.FloatField(db_column='Current_Price_vs_52_Week_High', blank=True, null=True)  # Field name made lowercase.
    rsi = models.FloatField(db_column='RSI', blank=True, null=True)  # Field name made lowercase.
    price = models.FloatField(db_column='Price', blank=True, null=True)  # Field name made lowercase.
    return_field = models.FloatField(db_column='Return', blank=True, null=True)  # Field name made lowercase. Field renamed because it was a Python reserved word.
    net_change = models.FloatField(db_column='Net Change', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    q_sales = models.FloatField(db_column='Q_Sales', blank=True, null=True)  # Field name made lowercase.
    q_piotroski_score = models.FloatField(db_column='Q_Piotroski_Score', blank=True, null=True)  # Field name made lowercase.
    q_roe = models.FloatField(db_column='Q_ROE', blank=True, null=True)  # Field name made lowercase.
    q_debt_equity = models.FloatField(db_column='Q_Debt/Equity', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    q_fcf_margin = models.FloatField(db_column='Q_FCF_Margin', blank=True, null=True)  # Field name made lowercase.
    q_beta = models.FloatField(db_column='Q_Beta', blank=True, null=True)  # Field name made lowercase.
    g_sales_growth = models.FloatField(db_column='G_Sales_Growth', blank=True, null=True)  # Field name made lowercase.
    g_eps_growth = models.FloatField(db_column='G_EPS_Growth', blank=True, null=True)  # Field name made lowercase.
    g_sales_qoq = models.FloatField(db_column='G_Sales_QoQ', blank=True, null=True)  # Field name made lowercase.
    t_price_vs_ema20 = models.FloatField(db_column='T_Price_vs_EMA20', blank=True, null=True)  # Field name made lowercase.
    t_price_vs_ema50 = models.FloatField(db_column='T_Price_vs_EMA50', blank=True, null=True)  # Field name made lowercase.
    t_price_vs_ema200 = models.FloatField(db_column='T_Price_vs_EMA200', blank=True, null=True)  # Field name made lowercase.
    t_ema50_vs_ema200 = models.FloatField(db_column='T_EMA50_vs_EMA200', blank=True, null=True)  # Field name made lowercase.
    t_current_price_vs_52_week_high = models.FloatField(db_column='T_Current_Price_vs_52_Week_High', blank=True, null=True)  # Field name made lowercase.
    t_rsi = models.FloatField(db_column='T_RSI', blank=True, null=True)  # Field name made lowercase.
    quality_score = models.FloatField(db_column='Quality_Score', blank=True, null=True)  # Field name made lowercase.
    growth_score = models.FloatField(db_column='Growth_Score', blank=True, null=True)  # Field name made lowercase.
    technical_score = models.FloatField(db_column='Technical_Score', blank=True, null=True)  # Field name made lowercase.
    stock_rank = models.FloatField(db_column='Stock_Rank', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        db_table = 'US_Stock_Ranking'

class ModelPortfoliosCumulative(models.Model):
    portfolio_name = models.TextField(db_column='Portfolio Name', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    country = models.TextField(db_column='Country', blank=True, null=True)  # Field name made lowercase.
    dtd = models.FloatField(db_column='DTD', blank=True, null=True)  # Field name made lowercase.
    mtd = models.FloatField(db_column='MTD', blank=True, null=True)  # Field name made lowercase.
    ytd = models.FloatField(db_column='YTD', blank=True, null=True)  # Field name made lowercase.
    number_1m = models.FloatField(db_column='1M', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_3m = models.FloatField(db_column='3M', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_1y = models.FloatField(db_column='1Y', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_3y = models.FloatField(db_column='3Y', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    itd = models.FloatField(db_column='ITD', blank=True, null=True)  # Field name made lowercase.
    itd_date = models.TextField(db_column='ITD Date', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    cagr = models.FloatField(db_column='CAGR', blank=True, null=True)  # Field name made lowercase.
    description = models.TextField(db_column='Description', blank=True, null=True)  # Field name made lowercase. This field type is a guess.
    class Meta:
        managed = False
        db_table = 'Model_Portfolios_Cumulative'



class StocksDailyBeta(models.Model):
    date = models.DateTimeField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    company = models.CharField(db_column='Company', max_length=255, blank=True, null=True)  # Field name made lowercase.
    index = models.CharField(db_column='Index', max_length=255, blank=True, null=True)  # Field name made lowercase.
    return_field = models.FloatField(db_column='Return', blank=True, null=True)  # Field name made lowercase. Field renamed because it was a Python reserved word.
    index_return = models.FloatField(db_column='Index Return', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    beta_1y = models.FloatField(db_column='Beta_1Y', blank=True, null=True)  # Field name made lowercase.
    factset_ticker = models.TextField(db_column='Factset_Ticker', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        db_table = 'Stocks_Daily_Beta'

class UsStocksDailyBeta(models.Model):
    date = models.DateTimeField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    company = models.TextField(db_column='Company', blank=True, null=True)  # Field name made lowercase.
    factset_ticker = models.TextField(db_column='Factset_Ticker', blank=True, null=True)  # Field name made lowercase.
    index = models.TextField(db_column='Index', blank=True, null=True)  # Field name made lowercase.
    return_field = models.FloatField(db_column='Return', blank=True, null=True)  # Field name made lowercase. Field renamed because it was a Python reserved word.
    index_return = models.FloatField(db_column='Index Return', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    beta_1y = models.FloatField(db_column='Beta_1Y', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        db_table = 'US_Stocks_Daily_Beta'


class ExponentialMovingAverage(models.Model):
    company = models.TextField(db_column='Company', blank=True, null=True)  # Field name made lowercase.
    factset_ticker = models.TextField(db_column='Factset_Ticker', blank=True, null=True)  # Field name made lowercase.
    date = models.DateField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    price = models.FloatField(db_column='Price', blank=True, null=True)  # Field name made lowercase.
    ema = models.FloatField(db_column='EMA', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Exponential_Moving_Average'

class UsExponentialMovingAverage(models.Model):
    company = models.TextField(db_column='Company', blank=True, null=True)  # Field name made lowercase.
    factset_ticker = models.TextField(db_column='Factset_Ticker', blank=True, null=True)  # Field name made lowercase.
    date = models.DateField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    price = models.FloatField(db_column='Price', blank=True, null=True)  # Field name made lowercase.
    ema = models.FloatField(db_column='EMA', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        db_table = 'US_Exponential_Moving_Average'

class VolumeData(models.Model):
    date = models.DateField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    factor = models.TextField(db_column='Factor', blank=True, null=True)  # Field name made lowercase.
    ticker = models.TextField(db_column='Ticker', blank=True, null=True)  # Field name made lowercase.
    value = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'volume_data'
        
class UsVolumeData(models.Model):
    date = models.TextField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    factor = models.TextField(db_column='Factor', blank=True, null=True)  # Field name made lowercase.
    ticker = models.TextField(db_column='Ticker', blank=True, null=True)  # Field name made lowercase.
    value = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'US_Volume_Data'




class TargetPrices(models.Model):
    ticker = models.CharField(db_column='Ticker', max_length=255, blank=True, null=True)  # Field name made lowercase.
    current_price = models.FloatField(db_column='Current_Price', blank=True, null=True)  # Field name made lowercase.
    number_52_week_high = models.FloatField(db_column='52_Week_High', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_52_week_low = models.FloatField(db_column='52_Week_Low', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_1_month_high = models.FloatField(db_column='1 Month High', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_1_month_low = models.FloatField(db_column='1 Month Low', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    target_price = models.FloatField(db_column='Target_Price', blank=True, null=True)  # Field name made lowercase.
    avg_upside = models.FloatField(db_column='Avg_Upside', blank=True, null=True)  # Field name made lowercase.
    avg_downside = models.FloatField(db_column='Avg_Downside', blank=True, null=True)  # Field name made lowercase.
    company = models.CharField(db_column='Company', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        db_table = 'Target_prices'

class UsTargetPrices(models.Model):
    ticker = models.TextField(db_column='Ticker', blank=True, null=True)  # Field name made lowercase.
    company = models.TextField(db_column='Company', blank=True, null=True)  # Field name made lowercase.
    current_price = models.FloatField(db_column='Current_Price', blank=True, null=True)  # Field name made lowercase.
    target_price = models.FloatField(db_column='Target_Price', blank=True, null=True)  # Field name made lowercase.
    avg_upside = models.FloatField(db_column='Avg_Upside', blank=True, null=True)  # Field name made lowercase.
    avg_downside = models.FloatField(db_column='Avg_Downside', blank=True, null=True)  # Field name made lowercase.
    number_52_week_high = models.FloatField(db_column='52_Week_High', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_52_week_low = models.FloatField(db_column='52_Week_Low', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_1_month_high = models.FloatField(db_column='1 Month High', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_1_month_low = models.FloatField(db_column='1 Month Low', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.

    class Meta:
        db_table = 'US_Target_prices'



        
class CustomerPortfolioDetails(models.Model):
    customer_name = models.CharField(db_column='Customer Name', max_length=255)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    portfolio_name = models.CharField(db_column='Portfolio Name', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    company_name = models.CharField(db_column='Company Name', max_length=255)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    price = models.FloatField(db_column='Price', blank=True, null=True)  # Field name made lowercase.
    quantity = models.IntegerField(db_column='Quantity', blank=True, null=True)  # Field name made lowercase.
    portfolio_type = models.CharField(max_length=255, blank=True, null=True)
    sector_name = models.CharField(db_column='Sector Name', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    model_portfolio = models.CharField(max_length=5, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    country = models.TextField(db_column='Country', blank=True, null=True)  # Field name made lowercase.
    fs_ticker = models.TextField(db_column='FS_Ticker', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Customer_Portfolio_Details'



class PositionsInput(models.Model):
    security_id = models.CharField(db_column='SECURITY ID', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    tradedate = models.DateTimeField(db_column='TRADEDATE', blank=True, null=True)  # Field name made lowercase.
    action = models.CharField(db_column='ACTION', max_length=255, blank=True, null=True)  # Field name made lowercase.
    status = models.CharField(db_column='STATUS', max_length=255, blank=True, null=True)  # Field name made lowercase.
    allocation_id = models.CharField(db_column='ALLOCATION ID', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    secdescription = models.CharField(db_column='SECDESCRIPTION', max_length=255, blank=True, null=True)  # Field name made lowercase.
    sedol = models.FloatField(db_column='SEDOL', blank=True, null=True)  # Field name made lowercase.
    isin = models.CharField(db_column='ISIN', max_length=255, blank=True, null=True)  # Field name made lowercase.
    cusip = models.CharField(db_column='CUSIP', max_length=255, blank=True, null=True)  # Field name made lowercase.
    trader = models.CharField(db_column='TRADER', max_length=255, blank=True, null=True)  # Field name made lowercase.
    portfolio = models.CharField(db_column='PORTFOLIO', max_length=255, blank=True, null=True)  # Field name made lowercase.
    customer = models.CharField(db_column='Customer', max_length=255, blank=True, null=True)  # Field name made lowercase.
    quantity = models.FloatField(db_column='QUANTITY', blank=True, null=True)  # Field name made lowercase.
    price = models.FloatField(db_column='PRICE', blank=True, null=True)  # Field name made lowercase.
    fx_rate = models.FloatField(db_column='FX RATE', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    grossmoney = models.FloatField(db_column='GROSSMONEY', blank=True, null=True)  # Field name made lowercase.
    totalcomm = models.FloatField(db_column='TOTALCOMM', blank=True, null=True)  # Field name made lowercase.
    totalfees = models.FloatField(db_column='TOTALFEES', blank=True, null=True)  # Field name made lowercase.
    netmoney = models.FloatField(db_column='NETMONEY', blank=True, null=True)  # Field name made lowercase.
    execcurrency = models.CharField(db_column='EXECCURRENCY', max_length=255, blank=True, null=True)  # Field name made lowercase.
    broker = models.CharField(db_column='BROKER', max_length=255, blank=True, null=True)  # Field name made lowercase.
    custodian = models.CharField(db_column='CUSTODIAN', max_length=255, blank=True, null=True)  # Field name made lowercase.
    accrued_int = models.CharField(db_column='ACCRUED INT', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    strategy = models.CharField(db_column='STRATEGY', max_length=255, blank=True, null=True)  # Field name made lowercase.
    bid_qty = models.CharField(db_column='BID QTY', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    bid_price = models.CharField(db_column='BID PRICE', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    pricing_date = models.DateTimeField(db_column='PRICING DATE', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    first_trade_date = models.DateTimeField(db_column='FIRST TRADE DATE', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    deal_captian = models.CharField(db_column='DEAL CAPTIAN', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    hedge = models.CharField(db_column='HEDGE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    intial_target_price = models.CharField(db_column='INTIAL TARGET PRICE', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    target_price = models.CharField(db_column='TARGET PRICE', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ultimate_stop = models.CharField(db_column='ULTIMATE STOP', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    country = models.CharField(db_column='COUNTRY', max_length=255, blank=True, null=True)  # Field name made lowercase.
    s_no = models.AutoField(primary_key=True)
    class Meta:
        managed = False
        db_table = 'Positions_Input'
class UsPositionsInput(models.Model):
    security_id = models.CharField(db_column='SECURITY ID', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    tradedate = models.DateTimeField(db_column='TRADEDATE', blank=True, null=True)  # Field name made lowercase.
    action = models.CharField(db_column='ACTION', max_length=255, blank=True, null=True)  # Field name made lowercase.
    status = models.CharField(db_column='STATUS', max_length=255, blank=True, null=True)  # Field name made lowercase.
    allocation_id = models.CharField(db_column='ALLOCATION ID', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    secdescription = models.CharField(db_column='SECDESCRIPTION', max_length=255, blank=True, null=True)  # Field name made lowercase.
    sedol = models.FloatField(db_column='SEDOL', blank=True, null=True)  # Field name made lowercase.
    isin = models.CharField(db_column='ISIN', max_length=255, blank=True, null=True)  # Field name made lowercase.
    cusip = models.CharField(db_column='CUSIP', max_length=255, blank=True, null=True)  # Field name made lowercase.
    trader = models.CharField(db_column='TRADER', max_length=255, blank=True, null=True)  # Field name made lowercase.
    portfolio = models.CharField(db_column='PORTFOLIO', max_length=255, blank=True, null=True)  # Field name made lowercase.
    customer = models.CharField(db_column='Customer', max_length=255, blank=True, null=True)  # Field name made lowercase.
    quantity = models.FloatField(db_column='QUANTITY', blank=True, null=True)  # Field name made lowercase.
    price = models.FloatField(db_column='PRICE', blank=True, null=True)  # Field name made lowercase.
    fx_rate = models.FloatField(db_column='FX RATE', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    grossmoney = models.FloatField(db_column='GROSSMONEY', blank=True, null=True)  # Field name made lowercase.
    totalcomm = models.FloatField(db_column='TOTALCOMM', blank=True, null=True)  # Field name made lowercase.
    totalfees = models.FloatField(db_column='TOTALFEES', blank=True, null=True)  # Field name made lowercase.
    netmoney = models.FloatField(db_column='NETMONEY', blank=True, null=True)  # Field name made lowercase.
    execcurrency = models.CharField(db_column='EXECCURRENCY', max_length=255, blank=True, null=True)  # Field name made lowercase.
    broker = models.CharField(db_column='BROKER', max_length=255, blank=True, null=True)  # Field name made lowercase.
    custodian = models.CharField(db_column='CUSTODIAN', max_length=255, blank=True, null=True)  # Field name made lowercase.
    accrued_int = models.CharField(db_column='ACCRUED INT', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    strategy = models.CharField(db_column='STRATEGY', max_length=255, blank=True, null=True)  # Field name made lowercase.
    bid_qty = models.CharField(db_column='BID QTY', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    bid_price = models.CharField(db_column='BID PRICE', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    pricing_date = models.DateTimeField(db_column='PRICING DATE', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    first_trade_date = models.DateTimeField(db_column='FIRST TRADE DATE', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    deal_captian = models.CharField(db_column='DEAL CAPTIAN', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    hedge = models.CharField(db_column='HEDGE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    intial_target_price = models.CharField(db_column='INTIAL TARGET PRICE', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    target_price = models.CharField(db_column='TARGET PRICE', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ultimate_stop = models.CharField(db_column='ULTIMATE STOP', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    country = models.CharField(db_column='COUNTRY', max_length=255, blank=True, null=True)  # Field name made lowercase.
    s_no = models.AutoField(primary_key=True)
    class Meta:
        managed = False
        db_table = 'US_Positions_Input'


class RiskMonitor(models.Model):
    fs_ticker = models.TextField(db_column='FS_Ticker', blank=True, null=True)  # Field name made lowercase.
    company_name = models.TextField(db_column='Company_Name', blank=True, null=True)  # Field name made lowercase.
    number_1y_cumulative_return = models.FloatField(db_column='1Y_Cumulative_Return', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_1m_cumulative_return = models.FloatField(db_column='1M_Cumulative_Return', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_3m_cumulative_return = models.FloatField(db_column='3M_Cumulative_Return', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    price = models.FloatField(db_column='Price', blank=True, null=True)  # Field name made lowercase.
    number_1m_excess_return = models.FloatField(db_column='1M_Excess_Return', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_3m_excess_return = models.FloatField(db_column='3M_Excess_Return', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_1y_excess_return = models.FloatField(db_column='1Y_Excess_Return', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_52_week_high = models.FloatField(db_column='52_Week_High', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_52_week_low = models.FloatField(db_column='52_Week_Low', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_1_month_high = models.FloatField(db_column='1_Month_High', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_1_month_low = models.FloatField(db_column='1_Month_Low', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    rsi = models.FloatField(db_column='RSI', blank=True, null=True)  # Field name made lowercase.
    number_20_ema = models.FloatField(db_column='20_EMA', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_50_dma = models.FloatField(db_column='50_DMA', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    vfx = models.FloatField(db_column='VFX', blank=True, null=True)  # Field name made lowercase.
    views = models.TextField(db_column='Views', blank=True, null=True)  # Field name made lowercase.
    class Meta:
        managed = False
        db_table = 'Risk_Monitor'
class UsRiskMonitor(models.Model):
    fs_ticker = models.TextField(db_column='FS_Ticker', blank=True, null=True)  # Field name made lowercase.
    company_name = models.TextField(db_column='Company_Name', blank=True, null=True)  # Field name made lowercase.
    number_1y_cumulative_return = models.FloatField(db_column='1Y_Cumulative_Return', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_1m_cumulative_return = models.FloatField(db_column='1M_Cumulative_Return', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_3m_cumulative_return = models.FloatField(db_column='3M_Cumulative_Return', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    price = models.FloatField(db_column='Price', blank=True, null=True)  # Field name made lowercase.
    number_1m_excess_return = models.FloatField(db_column='1M_Excess_Return', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_3m_excess_return = models.FloatField(db_column='3M_Excess_Return', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_1y_excess_return = models.FloatField(db_column='1Y_Excess_Return', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_52_week_high = models.FloatField(db_column='52_Week_High', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_52_week_low = models.FloatField(db_column='52_Week_Low', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_1_month_high = models.FloatField(db_column='1_Month_High', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_1_month_low = models.FloatField(db_column='1_Month_Low', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    rsi = models.FloatField(db_column='RSI', blank=True, null=True)  # Field name made lowercase.
    number_20_ema = models.FloatField(db_column='20_EMA', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_50_dma = models.FloatField(db_column='50_DMA', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    vfx = models.FloatField(db_column='VFX', blank=True, null=True)  # Field name made lowercase.
    views = models.TextField(db_column='Views', blank=True, null=True)  # Field name made lowercase.
    class Meta:
        managed = False
        db_table = 'US_Risk_Monitor'

class IndexconstituentsBse(models.Model):
    index_code = models.FloatField(blank=True, null=True)
    index_name = models.TextField(blank=True, null=True)
    co_code = models.FloatField(blank=True, null=True)
    lname = models.TextField(blank=True, null=True)
    flag = models.TextField(db_column='Flag', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'IndexConstituents_BSE'

class Indexconstituents(models.Model):
    index_code = models.FloatField(blank=True, null=True)
    index_name = models.TextField(blank=True, null=True)
    co_code = models.FloatField(blank=True, null=True)
    lname = models.TextField(blank=True, null=True)
    flag = models.TextField(db_column='Flag', blank=True, null=True)  # Field name made lowercase.
    class Meta:
        db_table = 'IndexConstituents'
        
class UsIndexconstituents(models.Model):
    index_code = models.FloatField(blank=True, null=True)
    index_name = models.TextField(blank=True, null=True)
    index_ticker = models.CharField(max_length=28)
    co_code = models.FloatField(blank=True, null=True)
    fs_ticker = models.TextField(db_column='FS_Ticker', blank=True, null=True)  # Field name made lowercase.
    lname = models.TextField(blank=True, null=True)
    flag = models.TextField(db_column='Flag', blank=True, null=True)  # Field name made lowercase.
    class Meta:
        db_table = 'Us_IndexConstituents'

class GainersLosers(models.Model):
    date = models.TextField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    price = models.FloatField(db_column='Price', blank=True, null=True)  # Field name made lowercase.
    return_field = models.FloatField(db_column='Return', blank=True, null=True)  # Field name made lowercase. Field renamed because it was a Python reserved word.
    nsesymbol = models.TextField(blank=True, null=True)
    type = models.TextField(db_column='Type', blank=True, null=True)  # Field name made lowercase.
    companytype = models.TextField(db_column='CompanyType', blank=True, null=True)  # Field name made lowercase.
    class Meta:
        db_table = 'Gainers_Losers'
class UsGainersLosers(models.Model):
    date = models.TextField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    price = models.FloatField(db_column='Price', blank=True, null=True)  # Field name made lowercase.
    return_field = models.FloatField(db_column='Return', blank=True, null=True)  # Field name made lowercase. Field renamed because it was a Python reserved word.
    factset_ticker = models.TextField(db_column='Factset_Ticker', blank=True, null=True)  # Field name made lowercase.
    type = models.TextField(db_column='Type', blank=True, null=True)  # Field name made lowercase.
    companytype = models.TextField(db_column='CompanyType', blank=True, null=True)  # Field name made lowercase.
    class Meta:
        db_table = 'US_Gainers_Losers'
class UsMarketByte(models.Model):
    ticker = models.TextField(db_column='Ticker', blank=True, null=True)  # Field name made lowercase.
    currentprice = models.FloatField(db_column='CurrentPrice', blank=True, null=True)  # Field name made lowercase.
    dailychange = models.FloatField(db_column='dailyChange', blank=True, null=True)  # Field name made lowercase.
    dailypercentchange = models.FloatField(db_column='dailyPercentChange', blank=True, null=True)  # Field name made lowercase.
    weekly_price_change = models.FloatField(blank=True, null=True)
    weekly_price_change_percent = models.FloatField(blank=True, null=True)
    monthly_price_change = models.FloatField(blank=True, null=True)
    monthly_price_change_percent = models.FloatField(blank=True, null=True)
    yearly_price_change = models.FloatField(blank=True, null=True)
    yearly_price_change_percent = models.FloatField(blank=True, null=True)
    date = models.DateTimeField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    country = models.TextField(db_column='Country', blank=True, null=True)  # Field name made lowercase.
    order = models.IntegerField(db_column='Order', blank=True, null=True)  # Field name made lowercase.
    class Meta:
        db_table = 'US_Market_Byte'
class MarketByte(models.Model):
    ticker = models.TextField(db_column='Ticker', blank=True, null=True)  # Field name made lowercase.
    currentprice = models.FloatField(db_column='CurrentPrice', blank=True, null=True)  # Field name made lowercase.
    dailychange = models.FloatField(db_column='dailyChange', blank=True, null=True)  # Field name made lowercase.
    dailypercentchange = models.FloatField(db_column='dailyPercentChange', blank=True, null=True)  # Field name made lowercase.
    weekly_price_change = models.FloatField(blank=True, null=True)
    weekly_price_change_percent = models.FloatField(blank=True, null=True)
    monthly_price_change = models.FloatField(blank=True, null=True)
    monthly_price_change_percent = models.FloatField(blank=True, null=True)
    yearly_price_change = models.FloatField(blank=True, null=True)
    yearly_price_change_percent = models.FloatField(blank=True, null=True)
    date = models.DateTimeField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    country = models.TextField(db_column='Country', blank=True, null=True)  # Field name made lowercase.
    class Meta:
        db_table = 'Market_Byte'
class AdvancesDeclines(models.Model):
    date = models.TextField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    count = models.BigIntegerField(db_column='Count', blank=True, null=True)  # Field name made lowercase.
    type = models.TextField(db_column='Type', blank=True, null=True)  # Field name made lowercase.
    class Meta:
        db_table = 'Advances_Declines'
class UsAdvancesDeclines(models.Model):
    date = models.TextField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    count = models.BigIntegerField(db_column='Count', blank=True, null=True)  # Field name made lowercase.
    type = models.TextField(db_column='Type', blank=True, null=True)  # Field name made lowercase.
    class Meta:
        db_table = 'US_Advances_Declines'
class NiftyIndexesCorelation(models.Model):
    date = models.DateField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    ticker = models.TextField(db_column='Ticker', blank=True, null=True)  # Field name made lowercase.
    nifty_50 = models.FloatField(db_column='Nifty 50', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    auto = models.FloatField(db_column='Auto', blank=True, null=True)  # Field name made lowercase.
    bank = models.FloatField(db_column='Bank', blank=True, null=True)  # Field name made lowercase.
    energy = models.FloatField(db_column='Energy', blank=True, null=True)  # Field name made lowercase.
    fin_serv = models.FloatField(db_column='Fin Serv', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    fmcg = models.FloatField(db_column='FMCG', blank=True, null=True)  # Field name made lowercase.
    infra = models.FloatField(db_column='Infra', blank=True, null=True)  # Field name made lowercase.
    it = models.FloatField(db_column='IT', blank=True, null=True)  # Field name made lowercase.
    media = models.FloatField(db_column='Media', blank=True, null=True)  # Field name made lowercase.
    metal = models.FloatField(db_column='Metal', blank=True, null=True)  # Field name made lowercase.
    midcap_100 = models.FloatField(db_column='MidCap 100', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    pharma = models.FloatField(db_column='Pharma', blank=True, null=True)  # Field name made lowercase.
    pvt_bank = models.FloatField(db_column='PVT Bank', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    psu_bank = models.FloatField(db_column='PSU Bank', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    realty = models.FloatField(db_column='Realty', blank=True, null=True)  # Field name made lowercase.
    smallcap_100 = models.FloatField(db_column='SmallCap 100', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    class Meta:
        db_table = 'Nifty_Indexes_Corelation'
class UsIndicesCorrelation(models.Model):
    company = models.TextField(db_column='Company', blank=True, null=True)  # Field name made lowercase.
    s_p_500 = models.FloatField(db_column='S&P 500', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    russell_2000 = models.FloatField(db_column='Russell 2000', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    dow_jones_industrial_average = models.FloatField(db_column='Dow Jones Industrial Average', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    xlb_us = models.FloatField(db_column='XLB-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    xlc_us = models.FloatField(db_column='XLC-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    xle_us = models.FloatField(db_column='XLE-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    xlf_us = models.FloatField(db_column='XLF-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    xli_us = models.FloatField(db_column='XLI-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    xlk_us = models.FloatField(db_column='XLK-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    xlp_us = models.FloatField(db_column='XLP-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    xlre_us = models.FloatField(db_column='XLRE-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    xlu_us = models.FloatField(db_column='XLU-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    xlv_us = models.FloatField(db_column='XLV-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    xly_us = models.FloatField(db_column='XLY-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    date = models.DateField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    class Meta:
        db_table = 'US_Indices_Correlation'

class Screening(models.Model):
    fs_name = models.TextField(db_column='FS Name', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    security_code = models.TextField(db_column='Security Code', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    fs_ticker = models.TextField(db_column='FS Ticker', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    market_cap = models.FloatField(db_column='Market Cap', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    market_cap_category = models.TextField(db_column='Market_Cap_Category', blank=True, null=True)  # Field name made lowercase.
    sales = models.FloatField(db_column='Sales', blank=True, null=True)  # Field name made lowercase.
    gics = models.TextField(db_column='GICS', blank=True, null=True)  # Field name made lowercase.
    beta_1y = models.FloatField(db_column='Beta_1Y', blank=True, null=True)  # Field name made lowercase.
    volatility_1y = models.FloatField(db_column='Volatility_1Y', blank=True, null=True)  # Field name made lowercase.
    piotroski_score = models.FloatField(db_column='Piotroski_Score', blank=True, null=True)  # Field name made lowercase.
    pe = models.FloatField(db_column='PE', blank=True, null=True)  # Field name made lowercase.
    pb = models.FloatField(db_column='PB', blank=True, null=True)  # Field name made lowercase.
    ev_sales = models.FloatField(db_column='EV/Sales', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ev_ebitda = models.FloatField(db_column='EV/EBITDA', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    div_yield = models.FloatField(db_column='Div_Yield', blank=True, null=True)  # Field name made lowercase.
    sales_growth = models.FloatField(db_column='Sales_Growth', blank=True, null=True)  # Field name made lowercase.
    sales_growth_3y_5r_cagr = models.FloatField(db_column='Sales_Growth_3Y/5R_CAGR', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    eps_growth = models.FloatField(db_column='EPS_Growth', blank=True, null=True)  # Field name made lowercase.
    eps_growth_3y_5r_cagr = models.FloatField(db_column='EPS_Growth_3Y/5R_CAGR', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    roe = models.FloatField(db_column='ROE', blank=True, null=True)  # Field name made lowercase.
    roce = models.FloatField(db_column='ROCE', blank=True, null=True)  # Field name made lowercase.
    fcf_margin = models.FloatField(db_column='FCF_Margin', blank=True, null=True)  # Field name made lowercase.
    sales_total_assets = models.FloatField(db_column='Sales/Total Assets', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    net_debt_fcf = models.FloatField(db_column='Net_Debt/FCF', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    net_debt_ebitda = models.FloatField(db_column='Net_Debt/EBITDA', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    debt_equity = models.FloatField(db_column='Debt/Equity', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    price = models.FloatField(db_column='Price', blank=True, null=True)  # Field name made lowercase.
    f_o = models.TextField(db_column='F_O', blank=True, null=True)  # Field name made lowercase.
    volume = models.FloatField(db_column='Volume', blank=True, null=True)  # Field name made lowercase.
    rsi = models.FloatField(db_column='RSI', blank=True, null=True)  # Field name made lowercase.
    ma50 = models.FloatField(db_column='MA50', blank=True, null=True)  # Field name made lowercase.
    ma200 = models.FloatField(db_column='MA200', blank=True, null=True)  # Field name made lowercase.
    ma20 = models.FloatField(db_column='MA20', blank=True, null=True)  # Field name made lowercase.
    number_52_week_high = models.FloatField(db_column='52_Week_High', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    target_price = models.FloatField(db_column='Target_Price', blank=True, null=True)  # Field name made lowercase.
    volume_20 = models.FloatField(db_column='Volume_20', blank=True, null=True)  # Field name made lowercase.
    rsi_30 = models.FloatField(db_column='RSI_30', blank=True, null=True)  # Field name made lowercase.
    rsi_90 = models.FloatField(db_column='RSI_90', blank=True, null=True)  # Field name made lowercase.
    share_turnover = models.FloatField(db_column='Share Turnover', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    volume_change = models.FloatField(db_column='Volume_Change', blank=True, null=True)  # Field name made lowercase.
    mtd = models.FloatField(db_column='MTD', blank=True, null=True)  # Field name made lowercase.
    ytd = models.FloatField(db_column='YTD', blank=True, null=True)  # Field name made lowercase.
    number_1y = models.FloatField(db_column='1Y', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_1m = models.FloatField(db_column='1M', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_3m = models.FloatField(db_column='3M', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_6m = models.FloatField(db_column='6M', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    qtd = models.FloatField(db_column='QTD', blank=True, null=True)  # Field name made lowercase.
    gross_profit_margin = models.FloatField(blank=True, null=True)
    altmanz_score = models.FloatField(db_column='Altmanz_score', blank=True, null=True)  # Field name made lowercase.
    id = models.BigIntegerField(primary_key=True)

    class Meta:
        managed = False
        db_table = 'Screening'



class UsScreening(models.Model):
    fs_name = models.TextField(db_column='FS Name', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    security_code = models.TextField(db_column='Security Code', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    fs_ticker = models.TextField(db_column='FS Ticker', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    market_cap = models.FloatField(db_column='Market Cap', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    market_cap_category = models.TextField(db_column='Market_Cap_Category', blank=True, null=True)  # Field name made lowercase.
    sales = models.FloatField(db_column='Sales', blank=True, null=True)  # Field name made lowercase.
    gics = models.TextField(db_column='GICS', blank=True, null=True)  # Field name made lowercase.
    beta_1y = models.FloatField(db_column='Beta_1Y', blank=True, null=True)  # Field name made lowercase.
    volatility_1y = models.FloatField(db_column='Volatility_1Y', blank=True, null=True)  # Field name made lowercase.
    piotroski_score = models.FloatField(db_column='Piotroski_Score', blank=True, null=True)  # Field name made lowercase.
    pe = models.FloatField(db_column='PE', blank=True, null=True)  # Field name made lowercase.
    pb = models.FloatField(db_column='PB', blank=True, null=True)  # Field name made lowercase.
    ev_sales = models.FloatField(db_column='EV/Sales', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ev_ebitda = models.FloatField(db_column='EV/EBITDA', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    div_yield = models.FloatField(db_column='Div_Yield', blank=True, null=True)  # Field name made lowercase.
    sales_growth = models.FloatField(db_column='Sales_Growth', blank=True, null=True)  # Field name made lowercase.
    sales_growth_3y_5r_cagr = models.FloatField(db_column='Sales_Growth_3Y/5R_CAGR', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    eps_growth = models.FloatField(db_column='EPS_Growth', blank=True, null=True)  # Field name made lowercase.
    eps_growth_3y_5r_cagr = models.FloatField(db_column='EPS_Growth_3Y/5R_CAGR', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    roe = models.FloatField(db_column='ROE', blank=True, null=True)  # Field name made lowercase.
    roce = models.FloatField(db_column='ROCE', blank=True, null=True)  # Field name made lowercase.
    fcf_margin = models.FloatField(db_column='FCF_Margin', blank=True, null=True)  # Field name made lowercase.
    sales_total_assets = models.FloatField(db_column='Sales/Total Assets', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    net_debt_fcf = models.FloatField(db_column='Net_Debt/FCF', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    net_debt_ebitda = models.FloatField(db_column='Net_Debt/EBITDA', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    debt_equity = models.FloatField(db_column='Debt/Equity', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    price = models.FloatField(db_column='Price', blank=True, null=True)  # Field name made lowercase.
    f_o = models.TextField(db_column='F_O', blank=True, null=True)  # Field name made lowercase.
    volume = models.FloatField(db_column='Volume', blank=True, null=True)  # Field name made lowercase.
    rsi = models.FloatField(db_column='RSI', blank=True, null=True)  # Field name made lowercase.
    ma20 = models.FloatField(db_column='MA20', blank=True, null=True)  # Field name made lowercase.
    ma50 = models.FloatField(db_column='MA50', blank=True, null=True)  # Field name made lowercase.
    ma200 = models.FloatField(db_column='MA200', blank=True, null=True)  # Field name made lowercase.
    number_52_week_high = models.FloatField(db_column='52_Week_High', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    target_price = models.FloatField(db_column='Target_Price', blank=True, null=True)  # Field name made lowercase.
    volume_20 = models.FloatField(db_column='Volume_20', blank=True, null=True)  # Field name made lowercase.
    rsi_30 = models.FloatField(db_column='RSI_30', blank=True, null=True)  # Field name made lowercase.
    rsi_90 = models.FloatField(db_column='RSI_90', blank=True, null=True)  # Field name made lowercase.
    share_turnover = models.FloatField(db_column='Share Turnover', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    volume_change = models.FloatField(db_column='Volume_Change', blank=True, null=True)  # Field name made lowercase.
    mtd = models.FloatField(db_column='MTD', blank=True, null=True)  # Field name made lowercase.
    ytd = models.FloatField(db_column='YTD', blank=True, null=True)  # Field name made lowercase.
    number_1y = models.FloatField(db_column='1Y', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_1m = models.FloatField(db_column='1M', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_3m = models.FloatField(db_column='3M', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_6m = models.FloatField(db_column='6M', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    qtd = models.FloatField(db_column='QTD', blank=True, null=True)  # Field name made lowercase.
    gross_profit_margin = models.FloatField(blank=True, null=True)
    id = models.BigIntegerField(primary_key=True)

    class Meta:
        managed = False
        db_table = 'Us_Screening'


class UsTechnicalIndicatorsDaily(models.Model):
    date = models.DateTimeField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    fs_ticker = models.TextField(db_column='FS_Ticker', blank=True, null=True)  # Field name made lowercase.
    symbol = models.TextField(db_column='Symbol', blank=True, null=True)  # Field name made lowercase.
    company_name = models.TextField(db_column='Company_Name', blank=True, null=True)  # Field name made lowercase.
    price = models.FloatField(db_column='Price', blank=True, null=True)  # Field name made lowercase.
    return_field = models.FloatField(db_column='Return', blank=True, null=True)  # Field name made lowercase. Field renamed because it was a Python reserved word.
    net_change = models.FloatField(db_column='Net_Change', blank=True, null=True)  # Field name made lowercase.
    ma9 = models.FloatField(db_column='MA9', blank=True, null=True)  # Field name made lowercase.
    ma20 = models.FloatField(db_column='MA20', blank=True, null=True)  # Field name made lowercase.
    ma50 = models.FloatField(db_column='MA50', blank=True, null=True)  # Field name made lowercase.
    ma200 = models.FloatField(db_column='MA200', blank=True, null=True)  # Field name made lowercase.
    ma9_ma20_value = models.TextField(db_column='MA9_MA20_Value', blank=True, null=True)  # Field name made lowercase.
    ma20_ma50_value = models.TextField(db_column='MA20_MA50_Value', blank=True, null=True)  # Field name made lowercase.
    ma50_ma200_value = models.TextField(db_column='MA50_MA200_Value', blank=True, null=True)  # Field name made lowercase.
    ma9_actions = models.TextField(db_column='MA9_Actions', blank=True, null=True)  # Field name made lowercase.
    ma50_actions = models.TextField(db_column='MA50_Actions', blank=True, null=True)  # Field name made lowercase.
    ma20_actions = models.TextField(db_column='MA20_Actions', blank=True, null=True)  # Field name made lowercase.
    ma200_actions = models.TextField(db_column='MA200_Actions', blank=True, null=True)  # Field name made lowercase.
    ma9_ma20_actions = models.TextField(db_column='MA9_MA20_Actions', blank=True, null=True)  # Field name made lowercase.
    ma20_ma50_actions = models.TextField(db_column='MA20_MA50_Actions', blank=True, null=True)  # Field name made lowercase.
    ma50_ma200_actions = models.TextField(db_column='MA50_MA200_Actions', blank=True, null=True)  # Field name made lowercase.
    rsi_value = models.FloatField(db_column='RSI_Value', blank=True, null=True)  # Field name made lowercase.
    cci_value = models.FloatField(db_column='CCI_Value', blank=True, null=True)  # Field name made lowercase.
    williams_value = models.FloatField(db_column='Williams_Value', blank=True, null=True)  # Field name made lowercase.
    stochrsi_value = models.FloatField(db_column='StochRSI_Value', blank=True, null=True)  # Field name made lowercase.
    stochastics_value = models.FloatField(db_column='Stochastics_Value', blank=True, null=True)  # Field name made lowercase.
    rsi_actions = models.TextField(db_column='RSI_Actions', blank=True, null=True)  # Field name made lowercase.
    cci_actions = models.TextField(db_column='CCI_Actions', blank=True, null=True)  # Field name made lowercase.
    williams_actions = models.TextField(db_column='Williams_Actions', blank=True, null=True)  # Field name made lowercase.
    stochrsi_actions = models.TextField(db_column='StochRSI_Actions', blank=True, null=True)  # Field name made lowercase.
    stochastics_actions = models.TextField(db_column='Stochastics_Actions', blank=True, null=True)  # Field name made lowercase.
    macd_value = models.FloatField(db_column='MACD_Value', blank=True, null=True)  # Field name made lowercase.
    atr_value = models.FloatField(db_column='ATR_Value', blank=True, null=True)  # Field name made lowercase.
    adx_value = models.FloatField(db_column='ADX_Value', blank=True, null=True)  # Field name made lowercase.
    super_trend_value = models.FloatField(db_column='Super_Trend_Value', blank=True, null=True)  # Field name made lowercase.
    macd_actions = models.TextField(db_column='MACD_Actions', blank=True, null=True)  # Field name made lowercase.
    atr_actions = models.TextField(db_column='ATR_Actions', blank=True, null=True)  # Field name made lowercase.
    adx_actions = models.TextField(db_column='ADX_Actions', blank=True, null=True)  # Field name made lowercase.
    super_trend_actions = models.TextField(db_column='Super_Trend_Actions', blank=True, null=True)  # Field name made lowercase.
    mfi_value = models.FloatField(db_column='MFI_Value', blank=True, null=True)  # Field name made lowercase.
    pvo_value = models.FloatField(db_column='PVO_Value', blank=True, null=True)  # Field name made lowercase.
    cmf_value = models.FloatField(db_column='CMF_Value', blank=True, null=True)  # Field name made lowercase.
    mfi_actions = models.TextField(db_column='MFI_Actions', blank=True, null=True)  # Field name made lowercase.
    pvo_actions = models.TextField(db_column='PVO_Actions', blank=True, null=True)  # Field name made lowercase.
    cmf_actions = models.TextField(db_column='CMF_Actions', blank=True, null=True)  # Field name made lowercase.
    moving_average_rating = models.TextField(db_column='Moving_Average_Rating', blank=True, null=True)  # Field name made lowercase.
    momentum_rating = models.TextField(db_column='Momentum_Rating', blank=True, null=True)  # Field name made lowercase.
    trend_rating = models.TextField(db_column='Trend_Rating', blank=True, null=True)  # Field name made lowercase.
    volume_rating = models.TextField(db_column='Volume_Rating', blank=True, null=True)  # Field name made lowercase.
    final_rating = models.TextField(db_column='Final_Rating', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'US_Technical_Indicators_Daily'

class TechnicalIndicatorsDaily(models.Model):
    date = models.DateTimeField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    fs_ticker = models.TextField(db_column='FS_Ticker', blank=True, null=True)  # Field name made lowercase.
    symbol = models.TextField(db_column='Symbol', blank=True, null=True)  # Field name made lowercase.
    company_name = models.TextField(db_column='Company_Name', blank=True, null=True)  # Field name made lowercase.
    price = models.FloatField(db_column='Price', blank=True, null=True)  # Field name made lowercase.
    return_field = models.FloatField(db_column='Return', blank=True, null=True)  # Field name made lowercase. Field renamed because it was a Python reserved word.
    net_change = models.FloatField(db_column='Net_Change', blank=True, null=True)  # Field name made lowercase.
    ma9 = models.FloatField(db_column='MA9', blank=True, null=True)  # Field name made lowercase.
    ma20 = models.FloatField(db_column='MA20', blank=True, null=True)  # Field name made lowercase.
    ma50 = models.FloatField(db_column='MA50', blank=True, null=True)  # Field name made lowercase.
    ma200 = models.FloatField(db_column='MA200', blank=True, null=True)  # Field name made lowercase.
    ma9_ma20_value = models.TextField(db_column='MA9_MA20_Value', blank=True, null=True)  # Field name made lowercase.
    ma20_ma50_value = models.TextField(db_column='MA20_MA50_Value', blank=True, null=True)  # Field name made lowercase.
    ma50_ma200_value = models.TextField(db_column='MA50_MA200_Value', blank=True, null=True)  # Field name made lowercase.
    ma9_actions = models.TextField(db_column='MA9_Actions', blank=True, null=True)  # Field name made lowercase.
    ma50_actions = models.TextField(db_column='MA50_Actions', blank=True, null=True)  # Field name made lowercase.
    ma20_actions = models.TextField(db_column='MA20_Actions', blank=True, null=True)  # Field name made lowercase.
    ma200_actions = models.TextField(db_column='MA200_Actions', blank=True, null=True)  # Field name made lowercase.
    ma9_ma20_actions = models.TextField(db_column='MA9_MA20_Actions', blank=True, null=True)  # Field name made lowercase.
    ma20_ma50_actions = models.TextField(db_column='MA20_MA50_Actions', blank=True, null=True)  # Field name made lowercase.
    ma50_ma200_actions = models.TextField(db_column='MA50_MA200_Actions', blank=True, null=True)  # Field name made lowercase.
    rsi_value = models.FloatField(db_column='RSI_Value', blank=True, null=True)  # Field name made lowercase.
    cci_value = models.FloatField(db_column='CCI_Value', blank=True, null=True)  # Field name made lowercase.
    williams_value = models.FloatField(db_column='Williams_Value', blank=True, null=True)  # Field name made lowercase.
    stochrsi_value = models.FloatField(db_column='StochRSI_Value', blank=True, null=True)  # Field name made lowercase.
    stochastics_value = models.FloatField(db_column='Stochastics_Value', blank=True, null=True)  # Field name made lowercase.
    rsi_actions = models.TextField(db_column='RSI_Actions', blank=True, null=True)  # Field name made lowercase.
    cci_actions = models.TextField(db_column='CCI_Actions', blank=True, null=True)  # Field name made lowercase.
    williams_actions = models.TextField(db_column='Williams_Actions', blank=True, null=True)  # Field name made lowercase.
    stochrsi_actions = models.TextField(db_column='StochRSI_Actions', blank=True, null=True)  # Field name made lowercase.
    stochastics_actions = models.TextField(db_column='Stochastics_Actions', blank=True, null=True)  # Field name made lowercase.
    macd_value = models.FloatField(db_column='MACD_Value', blank=True, null=True)  # Field name made lowercase.
    atr_value = models.FloatField(db_column='ATR_Value', blank=True, null=True)  # Field name made lowercase.
    adx_value = models.FloatField(db_column='ADX_Value', blank=True, null=True)  # Field name made lowercase.
    super_trend_value = models.FloatField(db_column='Super_Trend_Value', blank=True, null=True)  # Field name made lowercase.
    macd_actions = models.TextField(db_column='MACD_Actions', blank=True, null=True)  # Field name made lowercase.
    atr_actions = models.TextField(db_column='ATR_Actions', blank=True, null=True)  # Field name made lowercase.
    adx_actions = models.TextField(db_column='ADX_Actions', blank=True, null=True)  # Field name made lowercase.
    super_trend_actions = models.TextField(db_column='Super_Trend_Actions', blank=True, null=True)  # Field name made lowercase.
    mfi_value = models.FloatField(db_column='MFI_Value', blank=True, null=True)  # Field name made lowercase.
    pvo_value = models.FloatField(db_column='PVO_Value', blank=True, null=True)  # Field name made lowercase.
    cmf_value = models.FloatField(db_column='CMF_Value', blank=True, null=True)  # Field name made lowercase.
    mfi_actions = models.TextField(db_column='MFI_Actions', blank=True, null=True)  # Field name made lowercase.
    pvo_actions = models.TextField(db_column='PVO_Actions', blank=True, null=True)  # Field name made lowercase.
    cmf_actions = models.TextField(db_column='CMF_Actions', blank=True, null=True)  # Field name made lowercase.
    moving_average_rating = models.TextField(db_column='Moving_Average_Rating', blank=True, null=True)  # Field name made lowercase.
    momentum_rating = models.TextField(db_column='Momentum_Rating', blank=True, null=True)  # Field name made lowercase.
    trend_rating = models.TextField(db_column='Trend_Rating', blank=True, null=True)  # Field name made lowercase.
    volume_rating = models.TextField(db_column='Volume_Rating', blank=True, null=True)  # Field name made lowercase.
    final_rating = models.TextField(db_column='Final_Rating', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Technical_Indicators_Daily'


