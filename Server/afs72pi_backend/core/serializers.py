from rest_framework import serializers
from .models import CustomerPortfolioDetails

class CustomerPortfolioDetailsSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomerPortfolioDetails
        fields = '__all__'