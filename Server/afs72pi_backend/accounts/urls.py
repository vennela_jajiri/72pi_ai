from django.urls import path
from accounts import views

urlpatterns = [
    path('auth/captcha/validation/', views.GoogleCaptchaValidation.as_view(), name='captcha_verify'),
    path('auth/user/subscription/verification/', views.CustomerSubscriptionValidation().as_view(), name='subscription_verify'),
    path('<str:country>/savepricemovements/', views.SaveCustomerPricemovementsView.as_view()),
    path('<str:country>/savetechnicaldata/', views.SaveCustomerTechnicalIndicationsView.as_view()),
    path('newsletter/subscription/', views.NewsLetterSubscriptionView.as_view()),
    path('feedback/', views.CustomerFeedbackView.as_view()),
    path('<str:country>/customer/portfolio/saving/', views.CustomerPortfolioSavingView.as_view()),
    path('<str:country>/viewAlert/notifications/', views.ViewNotification.as_view()),
    path('<str:country>/pricing/', views.PricingDetailsPageView.as_view()),
    path('<str:country>/freesubscription/', views.CustomerFreeSubscriptionView.as_view()),
    path('<str:country>/subscription/status/', views.CustomerSubscriptionStatusView.as_view()),
    path('<str:country>/INR/premium/subscription/', views.CustomerINRSubscriptionView.as_view()),
    path('<str:country>/USD/premium/subscription/', views.CustomerUSDSubscriptionView.as_view()),
    path('payment/signature/validation/', views.PaymentSignatureValidationView.as_view()),
    path('contactUs/', views.CustomerContactUsView.as_view()),
    path('<str:country>/myAlerts/', views.MyAlerts.as_view()),
    path('<str:country>/myAlerts/edit/', views.MyAlertsEdit.as_view()),
    path('<str:country>/myAlerts/delete/', views.MyAlertsDelete.as_view()),
]