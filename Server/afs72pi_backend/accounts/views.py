from django.shortcuts import render,get_object_or_404, redirect
from django.core.cache import cache
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from django.views import View
from rest_framework.authtoken.models import Token
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from allauth.socialaccount.providers.facebook.views import FacebookOAuth2Adapter
from allauth.socialaccount.providers.google.views import GoogleOAuth2Adapter
from allauth.socialaccount.providers.oauth2.client import OAuth2Client
from rest_auth.registration.views import SocialLoginView
from rest_framework_jwt.settings import api_settings
from rest_framework import status
from django.contrib.auth.base_user import BaseUserManager
from django.contrib.auth.hashers import make_password
from rest_framework.utils import json
import requests
from rest_framework_simplejwt.tokens import RefreshToken
from django.contrib.auth.models import User, Group
from core.models import *
from .models import *
from rest_framework.decorators import authentication_classes, permission_classes, action
import json
from django.views.decorators.csrf import csrf_exempt
from django.utils import timezone
from django.utils.timezone import localdate
from django.db.models import Avg, Max, Min, Sum,Q
from django.conf import settings
from rest_framework.permissions import IsAuthenticated
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
# Razorpay 
import razorpay
from django.conf import settings
from datetime import datetime, timedelta, date
from django.http import Http404
import re
import pandas as pd

RAZORPAY_CLIENT_KEY = settings.RAZORPAY_CLIENT_KEY
RAZORPAY_SECRET_KEY = settings.RAZORPAY_SECRET_KEY


class GoogleLoginAPIView(SocialLoginView):
    adapter_class = GoogleOAuth2Adapter


class FacebookLoginAPIView(SocialLoginView):
    adapter_class = FacebookOAuth2Adapter


class GoogleCaptchaValidation(APIView):
    authentication_classes = []
    permission_classes = []

    def post(self, request):
        try:
            recaptcha_response = request.data['captcha_response']
            data = {'secret': settings.GOOGLE_RECAPTCHA_SECRET_KEY,
                    'response': recaptcha_response}
            response = requests.post('https://www.google.com/recaptcha/api/siteverify', data=data)
            result = response.json()
            if result['success']:
                return Response({'verification': 'SUCCESS', 'code':200}, status.HTTP_200_OK)
            else:
                return Response({'verification': 'FAIL', 'code':400}, status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            print(e)
            return Response({'verification': 'FAIL', 'code':500}, status.HTTP_500_INTERNAL_SERVER_ERROR)
    


class GoogleLogin(APIView):
    authentication_classes = []
    permission_classes = []

    def post(self, request, *args, **kwargs):
        payload = {'access_token': request.data.get("token")}  # validate the token
        r = requests.get('https://www.googleapis.com/oauth2/v2/userinfo', params=payload)
        data = json.loads(r.text)
        if 'error' in data:
            content = {'message': 'wrong google token / this google token is already expired.'}
            return Response(content)
        # create user if not exist
        try:
            user = User.objects.get(email=data['email'])
        except User.DoesNotExist:
            user = User()
            user.username = data['given_name']
            # provider random default password
            user.password = make_password(BaseUserManager().make_random_password())
            user.email = data['email']
            user.first_name = data['name']
            user.last_name = data['family_name']
            user.save()
        token = RefreshToken.for_user(user)  # generate token without username & password
        response = {}
        response['username'] = user.username
        response['token'] = str(token.access_token)
        # response['access_token'] = str(token.access_token)
        response['refresh_token'] = str(token)
        return Response(response)

class SaveCustomerPricemovementsView(APIView):
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]

    status = dict() #Defining empty dictionary 
  

    def get(self, request, *args, **kwargs):
        self.status['result'] ='Something went wrong, try again'
        self.status['status'] = False
        return Response({'status':self.status})

    def post(self, request, *args, **kwargs):
        try:
            params = json.loads(request.body.decode('utf-8'))
            country = params['country']
            values = params['values']
            portfolioName,companyName = values['portfolio_name'],values['company_name']
            priceValue,priceCondition = values['price_value'],values['price_value_condition']
            pricePercent,percentCondition = values['price_percent'],values['price_percent_condition']
            _52whPercent,_52whCondition = values['price_52wh'],values['price_52wh_condition']
            customerId,customerEmail = User.objects.get(id=4),'vinaykumarch@goldenhillsindia.com'
            tables={'India':[UniversePricesMain],'US':[UsUniversePricesMain]}
            universeModel = tables[country][0]
            priceStatus,percentStatus,_52Status = 'Waiting', 'Waiting', 'Waiting'
            priceCreated,percentCreated,price52whCreated = timezone.now(),timezone.now(),timezone.now()
            if priceValue == 'NaN':
                priceValue, priceCondition, priceStatus, priceCreated = None, None, 'NA', None
            if pricePercent == 'NaN':
                pricePercent, percentCondition, percentStatus, percentCreated = None, None, 'NA', None
            if _52whPercent == 'NaN':
                _52whPercent, _52whCondition, _52Status, price52whCreated = None, None, 'NA', None
            lastPrice = 0.0
            # status = {}
            prices = universeModel.objects.filter(date=universeModel.objects.all().aggregate(Max('date'))['date__max'], company=companyName).values('price')[0]
            
            lastPrice = prices['price']
            
            if priceValue and priceCondition and pricePercent and percentCondition and _52whPercent and _52whCondition:
                if CustomerPriceMovementsAlerts.objects.filter(customer_name=customerId, portfolio_name=portfolioName,company_name=companyName,
                price_value=priceValue,price_value_condition=priceCondition,price_percent=pricePercent,price_percent_condition=percentCondition,
                price_52wh_percent=_52whPercent,price_52wh_condition=_52whCondition,price_status='Waiting',percent_status='Waiting',price52wh_status='Waiting',country=country).exists():
                    self.status['portfolio'] = portfolioName
                    self.status['result'] ='Same alerts already created, and are in waiting state too!'
                    self.status['status'] = False
                else:
                    alert1 = CustomerPriceMovementsAlerts.objects.filter(customer_name=customerId, portfolio_name=portfolioName,company_name=companyName,
                    price_value=priceValue,price_value_condition=priceCondition,price_status='Waiting').all()
                    if alert1:
                        priceValue, priceCondition, priceStatus, priceCreated = None, None, 'NA', None
                    alert2 = CustomerPriceMovementsAlerts.objects.filter(customer_name=customerId,portfolio_name=portfolioName,company_name=companyName,
                    price_percent=pricePercent,price_percent_condition=percentCondition,percent_status='Waiting').all()
                    if alert2:
                        pricePercent, percentCondition, percentStatus, percentCreated = None, None, 'NA', None
                    alert3 =CustomerPriceMovementsAlerts.objects.filter(customer_name=customerId,portfolio_name=portfolioName,company_name=companyName,
                    price_52wh_percent=_52whPercent,price_52wh_condition=_52whCondition,price52wh_status='Waiting').all()
                    if alert3:
                        _52whPercent, _52whCondition, _52Status, price52whCreated = None, None, 'NA', None
                    CustomerPriceMovementsAlerts(customer_name=customerId, customer_email=customerEmail,portfolio_name=portfolioName,company_name=companyName,
                    alert_type='Price Movement', last_price=lastPrice,price_value_condition=priceCondition,price_value=priceValue,price_percent_condition=percentCondition,
                    price_percent=pricePercent,price_52wh_condition=_52whCondition,price_52wh_percent=_52whPercent,percent_status=percentStatus, price_status=priceStatus,
                    price52wh_status=_52Status, price_createdat=priceCreated, pricepercent_createdat=percentCreated, price52wh_createdat=price52whCreated,country=country).save()
                    self.status['portfolio'] = portfolioName
                    self.status['result'] = 'Price Movement alerts created successfully'
                    self.status['status'] = True
            elif priceValue and priceCondition:
                if CustomerPriceMovementsAlerts.objects.filter(customer_name=customerId, portfolio_name=portfolioName,company_name=companyName,
                price_value=priceValue,price_value_condition=priceCondition,price_status='Waiting').exists():
                    self.status['portfolio'] = portfolioName
                    self.status['result'] ='Same alerts already created, and are in waiting state too!'
                    self.status['status'] = False
                else:
                    CustomerPriceMovementsAlerts(customer_name=customerId, customer_email=customerEmail,portfolio_name=portfolioName,company_name=companyName,
                    alert_type='Price Movement', last_price=lastPrice,price_value_condition=priceCondition,price_value=priceValue,price_percent_condition=percentCondition,
                    price_percent=pricePercent,price_52wh_condition=_52whCondition,price_52wh_percent=_52whPercent,percent_status=percentStatus, price_status=priceStatus,
                    price52wh_status=_52Status, price_createdat=priceCreated, pricepercent_createdat=percentCreated, price52wh_createdat=price52whCreated,country=country).save()
                    self.status['portfolio'] = portfolioName
                    self.status['result'] = 'Price Movement alerts created successfully'
                    self.status['status'] = True
            elif pricePercent and percentCondition:
                if CustomerPriceMovementsAlerts.objects.filter(customer_name=customerId, portfolio_name=portfolioName,company_name=companyName,
                price_percent=pricePercent,price_percent_condition=percentCondition,percent_status='Waiting').exists():
                    self.status['portfolio'] = portfolioName
                    self.status['result'] ='Same alerts already created, and are in waiting state too!'
                    self.status['status'] = False
                else:
                    CustomerPriceMovementsAlerts(customer_name=customerId, customer_email=customerEmail,portfolio_name=portfolioName,company_name=companyName,
                    alert_type='Price Movement', last_price=lastPrice,price_value_condition=priceCondition,price_value=priceValue,price_percent_condition=percentCondition,
                    price_percent=pricePercent,price_52wh_condition=_52whCondition,price_52wh_percent=_52whPercent,percent_status=percentStatus, price_status=priceStatus,
                    price52wh_status=_52Status, price_createdat=priceCreated, pricepercent_createdat=percentCreated, price52wh_createdat=price52whCreated,country=country).save()
                    self.status['portfolio'] = portfolioName
                    self.status['result'] = 'Price Movement alerts created successfully'
                    self.status['status'] = True
            elif  _52whPercent and _52whCondition:
                if CustomerPriceMovementsAlerts.objects.filter(customer_name=customerId, portfolio_name=portfolioName,company_name=companyName,
                price_52wh_percent=_52whPercent,price_52wh_condition=_52whCondition,price52wh_status='Waiting').exists():
                    self.status['portfolio'] = portfolioName
                    self.status['result'] ='Same alerts already created, and are in waiting state too!'
                    self.status['status'] = False
                else:
                    CustomerPriceMovementsAlerts(customer_name=customerId, customer_email=customerEmail,portfolio_name=portfolioName,company_name=companyName,
                    alert_type='Price Movement', last_price=lastPrice,price_value_condition=priceCondition,price_value=priceValue,price_percent_condition=percentCondition,
                    price_percent=pricePercent,price_52wh_condition=_52whCondition,price_52wh_percent=_52whPercent,percent_status=percentStatus, price_status=priceStatus,
                    price52wh_status=_52Status, price_createdat=priceCreated, pricepercent_createdat=percentCreated, price52wh_createdat=price52whCreated,country=country).save()
                    self.status['portfolio'] = portfolioName
                    self.status['result'] = 'Price Movement alerts created successfully'
                    self.status['status'] = True
        except Exception as e:  #Any Exception
            self.status['portfolio'] = portfolioName
            self.status['result'] ='Something went wrong, please try again'
            self.status['status'] = False
            print(e)
        return Response({'status':self.status})
            

class SaveCustomerTechnicalIndicationsView(APIView):
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]

    status = dict() #Defining empty dictionary 
   
    def get(self, request, *args, **kwargs):
        self.status['result'] ='Something went wrong, please try again'
        self.status['status'] = False
        return Response({'status':self.status})

    def post(self, request, *args, **kwargs):
        try:
            params = json.loads(request.body.decode('utf-8'))
            country = params['country']
            values = params['values']
            portfolioName,companyName = values['portfolio_name'],values['company_name']
            rsiValue,rsiCondition = values['rsi_value'],values['rsi_condition']
            dmaLeft,dmaRight,dmaCondition = values['dmaleft_value'],values['dmaright_value'],values['dma_condition']
            if(len(dmaLeft)>0 and len(dmaRight)>0):
                dmaLeft='ma'+dmaLeft[:len(dmaLeft)-8]
                dmaRight='ma'+dmaRight[:len(dmaRight)-8]
            rsiStatus,dmaStatus = 'Waiting', 'Waiting'
            rsiCreated, dmaCreated = timezone.now(), timezone.now()
            customerId,customerEmail = User.objects.get(id=4),'vinaykumarch@goldenhillsindia.com'
            if rsiValue == 'null':
                rsiValue, rsiCondition, rsiStatus, rsiCreated = None, None, 'NA', None
            if dmaLeft == '' or dmaRight == '':
                dmaLeft, dmaRight, dmaCondition, dmaStatus, dmaCreated = None, None, None, 'NA', None
            if rsiValue and rsiCondition and rsiStatus and dmaLeft and dmaRight and dmaCondition:
                if CustomerTechnicalIndicationAlerts.objects.filter(customer_name=customerId, portfolio_name=portfolioName,company_name=companyName,
                rsi_operand1=rsiValue,rsi_condition=rsiCondition,dma_operand1=dmaLeft,dma_condition=dmaCondition,dma_operand2=dmaRight,
                rsi_status='Waiting',dma_status='Waiting').exists():
                    self.status['portfolio'] = portfolioName
                    self.status['result'] ='Same alerts already created, and are in waiting state too!'
                    self.status['status'] = False
                else:
                    alert1 = CustomerTechnicalIndicationAlerts.objects.filter(customer_name=customerId, portfolio_name=portfolioName,company_name=companyName,
                    rsi_operand1=rsiValue,rsi_condition=rsiCondition).all()
                    if alert1:
                        rsiValue, rsiCondition, rsiStatus, rsiCreated = None, None, 'NA', None
                    alert2 = CustomerTechnicalIndicationAlerts.objects.filter(customer_name=customerId,portfolio_name=portfolioName,company_name=companyName,
                    dma_operand1=dmaLeft,dma_condition=dmaCondition,dma_operand2=dmaRight).all()
                    if alert2:
                        dmaLeft, dmaRight, dmaCondition, dmaStatus, dmaCreated = None, None, None, 'NA', None
                    CustomerTechnicalIndicationAlerts(customer_name=customerId, customer_email=customerEmail,portfolio_name=portfolioName,company_name=companyName,
                    alert_type='Technical Indicator',rsi_operand1=rsiValue,rsi_condition=rsiCondition,dma_operand1=dmaLeft,dma_condition=dmaCondition,dma_operand2=dmaRight,
                    rsi_status=rsiStatus,dma_status=dmaStatus, rsi_createdat=rsiCreated, dma_createdat=dmaCreated,country=country).save()
                    self.status['portfolio'] = portfolioName
                    self.status['result'] = 'Technical Indication alerts created successfully'
                    self.status['status'] = True
            elif rsiValue and rsiCondition and rsiStatus:
                if CustomerTechnicalIndicationAlerts.objects.filter(customer_name=customerId, portfolio_name=portfolioName,company_name=companyName,
                rsi_operand1=rsiValue,rsi_condition=rsiCondition,rsi_status='Waiting').exists():
                    self.status['portfolio'] = portfolioName
                    self.status['result'] ='Same alerts already created, and are in waiting state too!'
                    self.status['status'] = False
                else:
                    CustomerTechnicalIndicationAlerts(customer_name=customerId, customer_email=customerEmail,portfolio_name=portfolioName,company_name=companyName,
                    alert_type='Technical Indicator',rsi_operand1=rsiValue,rsi_condition=rsiCondition,dma_operand1=dmaLeft,dma_condition=dmaCondition,dma_operand2=dmaRight,
                    rsi_status=rsiStatus,dma_status=dmaStatus, rsi_createdat=rsiCreated, dma_createdat=dmaCreated,country=country).save()
                    self.status['portfolio'] = portfolioName
                    self.status['result'] = 'Technical Indication alerts created successfully'
                    self.status['status'] = True
            elif dmaLeft and dmaRight and dmaCondition:
                if CustomerTechnicalIndicationAlerts.objects.filter(customer_name=customerId,portfolio_name=portfolioName,company_name=companyName,
                dma_operand1=dmaLeft,dma_condition=dmaCondition,dma_operand2=dmaRight,dma_status='Waiting').exists():
                    self.status['portfolio'] = portfolioName
                    self.status['result'] ='Same alerts already created, and are in waiting state too!'
                    self.status['status'] = False
                else:
                    CustomerTechnicalIndicationAlerts(customer_name=customerId, customer_email=customerEmail,portfolio_name=portfolioName,company_name=companyName,
                    alert_type='Technical Indicator',rsi_operand1=rsiValue,rsi_condition=rsiCondition,dma_operand1=dmaLeft,dma_condition=dmaCondition,dma_operand2=dmaRight,
                    rsi_status=rsiStatus,dma_status=dmaStatus, rsi_createdat=rsiCreated, dma_createdat=dmaCreated,country=country).save()
                    self.status['portfolio'] = portfolioName
                    self.status['result'] = 'Technical Indication alerts created successfully'
                    self.status['status'] = True
            else:
                pass
        except Exception as e:
            self.status['portfolio'] = portfolioName
            self.status['result'] ='Something went wrong, please try again'
            self.status['status'] = False
            print(e)
        return Response({'status':self.status})


class NewsLetterSubscriptionView(APIView):
    authentication_classes = []
    permission_classes = []

    def post(self, request, *args, **kwargs):
        try:
            params = json.loads(request.body.decode('utf-8'))
            context = {}
            email,country = params['eMail'],params['country']
            if(NewsLetterSubscription.objects.filter(email=email).exists()):
                context['response']= 'This E-Mail is already subscribed to us'
                context['status'] = 'error'
            else:
                NewsLetterSubscription(email=email,country=country).save()
                context['response'] = 'Successfully subscribed to 72PI News Letter'
                context['status'] = 'success'
        except Exception as e:
            context['response'] = 'Unknown Internal Server Error'
            context['status'] = 'error'
            print(e)
        finally:
            return Response(context)


class CustomerFeedbackView(APIView):
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]

    def post(self, request, *args, **kwargs):
        context = {}
        try:
            params = json.loads(request.body.decode('utf-8'))
            rating,feedback,country=params['rating'],params['feedback'],params['country']
            CustomerFeedback(user=request.user,rating=rating,feedback=feedback,country=country).save()
            context['message'] = 'Thank You For Your Feedback!'
        except Exception as e:
            print(e)
            context['message'] = 'Sorry, Your Feedback not submitted!'
        finally:
            return Response(context)


class CustomerPortfolioSavingView(APIView):
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]

    def post(self, request, *args, **kwargs):
        try:
            requestData = json.loads(request.body.decode('utf-8'))
            counTRY = kwargs['country']
            portfolio_name = requestData['portfolioName']
            stocks = requestData['stocks']
            addToExisting = bool(requestData['addToExisting'])
            if stocks:
                if addToExisting:
                    for stock in stocks:
                        if int(float(stock['Quantity'])):
                            if not CustomerPortfolioDetails.objects.filter(customer_name=request.user.username,country=counTRY, portfolio_name=portfolio_name,company_name=stock['Company'],fs_ticker=stock['FsTicker']).exists() :
                                CustomerPortfolioDetails(customer_name=request.user.username, portfolio_name=portfolio_name, company_name=stock['Company'],fs_ticker=stock['FsTicker'],
                                quantity=int(float(stock['Quantity'])), price=float(stock['Price']),sector_name=stock['GICS'],country=counTRY, model_portfolio='no', portfolio_type='General').save()
                            else:
                                stockObject = CustomerPortfolioDetails.objects.filter(customer_name=request.user.username, portfolio_name=portfolio_name, company_name=stock['Company'],fs_ticker=stock['FsTicker'],
                                sector_name=stock['GICS'],country=counTRY, model_portfolio='no', portfolio_type='General').first()
                                old_quantity = stockObject.quantity
                                new_quantity = old_quantity + int(float(stock['Quantity']))
                                CustomerPortfolioDetails.objects.filter(customer_name=request.user.username, portfolio_name=portfolio_name, company_name=stock['Company'],fs_ticker=stock['FsTicker'],
                                sector_name=stock['GICS'],country=counTRY, model_portfolio='no', portfolio_type='General').update(quantity=new_quantity)
                        else:
                            print('Invalid Quantity!!!')
                    httpResponse = {'message': 'Stocks Added Successfully!','status':'success'}
                    httpStatus = status.HTTP_201_CREATED
                else:
                    if not CustomerPortfolioDetails.objects.filter(customer_name=request.user.username, country=counTRY, portfolio_name=portfolio_name).exists():
                        for stock in stocks:
                            if stock['Quantity']:
                                if not CustomerPortfolioDetails.objects.filter(customer_name=request.user.username,country=counTRY, portfolio_name=portfolio_name,company_name=stock['Company'], fs_ticker=stock['FsTicker']).exists():
                                    CustomerPortfolioDetails(customer_name=request.user.username, portfolio_name=portfolio_name, company_name=stock['Company'],fs_ticker=stock['FsTicker'],
                                    quantity=int(float(stock['Quantity'])), price=float(stock['Price']),sector_name=stock['GICS'],country=counTRY, model_portfolio='no', portfolio_type='General').save()
                            else:
                                print('Invalid Quantity!!!')
                        httpResponse = {'message': 'Portfolio saved successfully!','status':'success'}
                        httpStatus = status.HTTP_201_CREATED
                    else:
                        httpResponse = {'message': 'Portfolio Name Already Taken!','status':'error'}
                        httpStatus = status.HTTP_208_ALREADY_REPORTED
            else:
                httpResponse = {'message': 'Unable to create portfolio!','status':'error'}
                httpStatus = status.HTTP_400_BAD_REQUEST
        except Exception as e:
            print(e)
            httpResponse = {'message': 'Internal server error!','status':'error'}
            httpStatus = status.HTTP_500_INTERNAL_SERVER_ERROR
        finally:
            return Response(data=httpResponse, status=httpStatus)

class ViewNotification(APIView):
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]


    def post(self, request, *args, **kwargs):
        notifications, context = [], {}
        try:
            counTRY = kwargs['country']
            params = json.loads(request.body.decode('utf-8'))
            companyName = params['AlertData']['company_name']
            portfolioName = params['AlertData']['portfolio_name']
            state = params['state']
            loggedINuser = self.request.user
            notifications1 = CustomerPriceMovementsAlerts.objects.filter(Q(customer_name=loggedINuser) & Q(company_name=companyName) & Q(country=counTRY) & Q(portfolio_name=portfolioName)  &
                            (Q(price_status=state) | Q(percent_status=state) | Q(price52wh_status=state))).all()
            notifications2 = CustomerTechnicalIndicationAlerts.objects.filter(Q(customer_name=loggedINuser) & Q(company_name=companyName) &  Q(country=counTRY) & Q(portfolio_name=portfolioName) & 
                            (Q(rsi_status=state) | Q(dma_status=state))).all()
            # Prive Movement Notifications
            for item in notifications1:
                # Conditions for Price in Rs.
                if item.price_value and item.price_value_condition and item.price_status == state:
                    if item.price_value_condition == 'Increases Above':
                        if counTRY == 'India':
                            notifications.append('Last Price >= '+str(item.price_value)+' INR | Price Movement ')
                        else:
                            notifications.append('Last Price >= '+str(item.price_value)+' USD | Price Movement ')
                    if item.price_value_condition == 'Decreases Below':
                        if counTRY == 'India':
                            notifications.append('Last Price <= '+str(item.price_value)+' INR | Price Movement')
                        else:
                            notifications.append('Last Price <= '+str(item.price_value)+' USD | Price Movement')
                # Conditions for Price in % 
                if item.price_percent and item.price_percent_condition and item.percent_status == state:
                    if item.price_percent_condition == 'Increases Above':
                        notifications.append('Last Price >= '+str(item.price_percent)+'% | Price Movement')
                    if item.price_percent_condition == 'Decreases Below':
                        notifications.append('Last Price <= '+str(item.price_percent)+'% | Price Movement')
                # Conditions for 52-week high
                if item.price_52wh_percent and item.price_52wh_condition and item.price52wh_status == state:
                    if item.price_52wh_condition == 'Increases Above':
                        notifications.append('Last Price >= '+str(item.price_52wh_percent)+'% of 52-WH | Price Movement')
                    if item.price_52wh_condition == 'Decreases Below':
                        notifications.append('Last Price <= '+str(item.price_52wh_percent)+'% of 52-WH | Price Movement')
            # Technical Indication Notifications
            for item in notifications2:
                # Conditions for 14 Day RSI
                if item.rsi_operand1 and item.rsi_condition and item.rsi_metric and item.rsi_status == state:
                    if item.rsi_condition == 'Greater than':
                        notifications.append('14 Day RSI >= '+str(item.rsi_operand1)+' | Technical Indication')
                    if item.rsi_condition == 'Less than':
                        notifications.append('14 Day RSI <= '+str(item.rsi_operand1)+' | Technical Indication')
                # Conditions for DMA
                if item.dma_operand1 and item.dma_operand2 and item.dma_condition and item.dma_status == state:
                    if item.dma_condition == 'Greater than':
                        notifications.append(str(re.search(r'\d+', item.dma_operand1).group())+' Day MA >= '+str(re.search(r'\d+', item.dma_operand2).group())+' Day MA | Technical Indication')
                    if item.dma_condition == 'Less than':
                        notifications.append(str(re.search(r'\d+', item.dma_operand1).group())+' Day MA <= '+str(re.search(r'\d+', item.dma_operand2).group())+' Day MA | Technical Indication')
            #Check notifications and send always 3 alerts/notifications only
            if len(notifications):
                context = {'notifications' : notifications[:3], 'state':params['state']}
            else:
                context = {'notifications' : notifications, 'state':'Create'}
        except Exception as e:
            print(e)
            context = {'notifications' : notifications, 'state': 'Create'}
        finally:
            return Response(context)

class PricingDetailsPageView(APIView):
    permission_classes = []
    authentication_classes = []

    def get(self, request, *args,  **kwargs):
        context = {}
        try:
            if self.request.user.is_authenticated:
                if not CustomerSubscription.objects.filter(customer_name=self.request.user).exists():
                    context['freesubscription'] = False
                else:
                    context['freesubscription'] = True
            else:
                context['freesubscription'] = True
        except Exception as e:
            print(e)
            context['freesubscription'] = True
        finally:
            return Response(context)

class CustomerFreeSubscriptionView(APIView):
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request, *args, **kwargs):
        context = {}
        if CustomerSubscription.objects.filter(customer_name=request.user).exists():
            context['free_eligible'] = False
        else:
            context['free_eligible'] = True
        context['first_name'] = self.request.user.first_name
        context['last_name'] = self.request.user.last_name
        context['email'] = self.request.user.email
        context['contact'] = ''
        return Response(context)

    def post(self, request, *args, **kwargs):
        context = {}
        try:
            requestData = json.loads(request.body.decode('utf-8'))
            if requestData:
                first_name = requestData['first_name']
                last_name = requestData['last_name']
                email = requestData['email']
                contact_number = requestData['contact_no']
                if not CustomerSubscription.objects.filter(customer_name=request.user).exists():
                    from datetime import date
                    endsubscription = date.today() + timedelta(days=30)
                    startsubscription = date.today()
                    CustomerSubscription(customer_name=request.user, first_name=first_name,last_name=last_name, email=email,contact_no=contact_number,
                    subscription_type='FREE',subscription_starts=startsubscription,subscription_ends=endsubscription,
                    subscription_status='ACTIVE', subscription_plan='30 DAY TRIAL', paid='NO', currency='NA').save()
                    try:
                        subscription = CustomerSubscription.objects.filter(customer_name=self.request.user).first()
                        if subscription:
                            context['status'] = 'Free Subscription Activated Successfully!'
                            context['reason'] = 'Your subscription ends on '+str(subscription.subscription_ends)
                        else:
                            context['status'] = 'Something went wrong, try again!'
                            context['reason'] = 'Subscription not activated, try again!'
                    except Exception as e:
                        print(e)
                        context['status'] = 'Unknown Internal Server Error!'
                        context['reason'] = 'Unable to Process Subscription this time!'
                else:
                    context['status'] = 'Subscription not activated!'
                    context['reason'] = 'You are not authorized to get this subscription!'
            else:
                context['status'] = 'Something went wrong, please try again!'
                context['reason'] = 'Invalid Data Submitted!'
        except Exception as e:
            print(e)
            context['status'] = 'Unknown Internal Server Error!'
            context['reason'] = 'Come back after some time!'
        finally:
            return Response(context)

class CustomerSubscriptionStatusView(APIView):
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request, *args, **kwargs):
        context = {}
        try:
            subscription = CustomerSubscription.objects.filter(customer_name=self.request.user).order_by('-subscription_ends').first()
            if subscription:
                context['status'] = 'Subscription Activated Successfully!'
                context['reason'] = 'Your subscription ends on '+str(subscription.subscription_ends)
                context['code'] = 'success'
            else:
                context['status'] = 'Subscription not Activated!'
                context['reason'] = 'Please try again after some time'
                context['code'] = 'error'
        except Exception as e:
            print(e)
            context['status'] = 'Unknown Internal Server Error!'
            context['reason'] = 'Unable to Process Subscription this time!'
            context['code'] = 'error'
        finally:
            return Response(context)
        
class PaymentSignatureValidationView(APIView):
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request, *args, **kwargs):
        context = {'status': 'Payment Faliure!!!', 
                  'reson' : 'Please, try again after some time!'}
        return Response(context)
    
    def post(self, request, *args, **kwargs):
        context = {}
        try:
            # Retreving the data from cache
            plantype = cache.get(request.user.username+'rplan_type')
            currency = cache.get(request.user.username+'rcurrency')
            customer_firstname = cache.get(request.user.username+'rfirst_name')
            customer_lastname = cache.get(request.user.username+'rlast_name')
            customer_email = cache.get(request.user.username+'remail')
            customer_contact = cache.get(request.user.username+'rcontact')
            subscription_type = cache.get(request.user.username+'rsubscription_type')
            order_amount = cache.get(request.user.username+'rsubscription_price')
            response = json.loads(request.body.decode('utf-8'))
            params_dict = {
                'razorpay_payment_id' : response['razorpay_payment_id'],
                'razorpay_order_id' : response['razorpay_order_id'],
                'razorpay_signature' : response['razorpay_signature']
            }
            client = razorpay.Client(auth=(RAZORPAY_CLIENT_KEY, RAZORPAY_SECRET_KEY))
            # Signature Verification
            client.utility.verify_payment_signature(params_dict)
            subscription_startdate, subscription_enddate = date.today(),date.today() #Consider today is the start and expiry day
            #Creating subscripton
            if CustomerSubscription.objects.filter(customer_name=request.user).exists():
                if CustomerSubscription.objects.filter(customer_name=request.user, subscription_status='ACTIVE').exists():
                    if CustomerSubscription.objects.filter(customer_name=request.user, subscription_status='UPCOMING').exists():
                        upcoming = CustomerSubscription.objects.filter(customer_name=request.user, subscription_status='UPCOMING').order_by('-subscription_starts').first()
                        subscription_startdate = upcoming.subscription_ends+timedelta(days=1)
                        if plantype == 'QUARTER':
                            subscription_enddate = subscription_startdate + timedelta(days=91)
                        elif plantype == 'ANNUAL':
                            subscription_enddate = subscription_startdate + timedelta(days=364)
                        CustomerSubscription(customer_name=request.user, first_name=customer_firstname, last_name=customer_lastname,
                        email=customer_email, contact_no=customer_contact,
                        subscription_type=subscription_type, subscription_starts=subscription_startdate,
                        subscription_ends=subscription_enddate,subscription_status='UPCOMING', subscription_plan=plantype,paid='YES',currency=currency).save()
                        context = {'payment_verification' : 'success', 'subscription' : 'activated', 'status':'verification_success','subscription_type' :'premium',
                                   'first_name' : customer_firstname, 'email' :customer_email, 'contact' :customer_contact, 'price' :order_amount, 
                                   'start' : subscription_startdate, 'end' :subscription_enddate ,'plan' :plantype,'currency':currency}
                    else:
                        active = CustomerSubscription.objects.filter(customer_name=request.user, subscription_status='ACTIVE').order_by('-subscription_starts').first()
                        subscription_startdate = active.subscription_ends+timedelta(days=1)
                        if plantype == 'QUARTER':
                            subscription_enddate = subscription_startdate + timedelta(days=91)
                        elif plantype == 'ANNUAL':
                            subscription_enddate = subscription_startdate + timedelta(days=364)
                        CustomerSubscription(customer_name=request.user, first_name=customer_firstname, last_name=customer_lastname,
                        email=customer_email, contact_no= customer_contact,
                        subscription_type=subscription_type, subscription_starts=subscription_startdate,
                        subscription_ends=subscription_enddate,subscription_status='UPCOMING', subscription_plan=plantype,paid='YES',currency=currency).save()
                        context = {'payment_verification' : 'success', 'subscription' : 'activated', 'status':'verification_success','subscription_type' :'premium',
                                   'first_name' : customer_firstname, 'email' :customer_email, 'contact' :customer_contact, 'price' :order_amount, 
                                   'start' : subscription_startdate, 'end' :subscription_enddate ,'plan' :plantype,'currency':currency}
                else:
                    if CustomerSubscription.objects.filter(customer_name=request.user, subscription_status='UPCOMING').exists():
                        upcoming = CustomerSubscription.objects.filter(customer_name=request.user, subscription_status='UPCOMING').order_by('-subscription_starts').first()
                        subscription_startdate = upcoming.subscription_ends+timedelta(days=1)
                        if plantype == 'QUARTER':
                            subscription_enddate = subscription_startdate + timedelta(days=91)
                        elif plantype == 'ANNUAL':
                            subscription_enddate = subscription_startdate + timedelta(days=364)
                        CustomerSubscription(customer_name=request.user, first_name=customer_firstname, last_name=customer_lastname,
                        email=customer_email, contact_no=customer_contact,
                        subscription_type=subscription_type, subscription_starts=subscription_startdate,
                        subscription_ends=subscription_enddate,subscription_status='UPCOMING', subscription_plan=plantype,paid='YES',currency=currency).save()
                        context = {'payment_verification' : 'success', 'subscription' : 'activated', 'status':'verification_success','subscription_type' :'premium',
                                    'first_name' : customer_firstname, 'email' :customer_email, 'contact' :customer_contact, 'price' :order_amount, 
                                    'start' : subscription_startdate, 'end' :subscription_enddate ,'plan' :plantype,'currency':currency}
                    else:
                        if plantype == 'QUARTER':
                            subscription_enddate = date.today()+timedelta(days=91)
                        elif plantype == 'ANNUAL':
                            subscription_enddate = date.today()+timedelta(days=364)
                        CustomerSubscription(customer_name=request.user, first_name=customer_firstname, last_name=customer_lastname,
                        email=customer_email, contact_no=customer_contact,
                        subscription_type=subscription_type, subscription_starts=subscription_startdate,
                        subscription_ends=subscription_enddate,subscription_status='ACTIVE', subscription_plan=plantype,paid='YES',currency=currency).save()
                        context = {'payment_verification' : 'success', 'subscription' : 'activated', 'status':'verification_success','subscription_type' :'premium',
                                   'first_name' : customer_firstname, 'email' :customer_email, 'contact' :customer_contact, 'price' :order_amount, 
                                   'start' : subscription_startdate, 'end' :subscription_enddate ,'plan' :plantype,'currency':currency}
            else:
                # This case not possible mostly
                if plantype == 'QUARTER':
                    subscription_enddate = date.today()+timedelta(days=91)
                elif plantype == 'ANNUAL':
                    subscription_enddate = date.today()+timedelta(days=364)
                CustomerSubscription(customer_name=request.user, first_name=customer_firstname, last_name=customer_lastname,
                email=customer_email, contact_no=customer_contact,
                subscription_type=subscription_type, subscription_starts=subscription_startdate,
                subscription_ends=subscription_enddate,subscription_status='ACTIVE', subscription_plan=plantype,paid='YES',currency=currency).save()
                context = {'payment_verification' : 'success', 'subscription' : 'activated', 'status':'verification_success','subscription_type' :'premium',
                            'first_name' : customer_firstname, 'email' :customer_email, 'contact' :customer_contact, 'price' :order_amount, 
                            'start' : subscription_startdate, 'end' :subscription_enddate ,'plan' :plantype,'currency':currency}
        except Exception as e:
            print(e)
            context = {'payment_verification' : 'failed', 'subscription' : 'not_activated', 'status':'error_occured','subscription_type' :'premium'}
        finally:
            return Response(context)


class GetCustomerSubscriptionsLogView(APIView):
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request, *args, **kwargs):
        context = {}
        context['activesubscriptions'] = CustomerSubscription.objects.filter(customer_name=self.request.user, subscription_status='ACTIVE').all()
        context['upcomingsubscriptions'] = CustomerSubscription.objects.filter(customer_name=self.request.user, subscription_status='UPCOMING').all().order_by('subscription_starts')
        context['expiredsubscriptions'] = CustomerSubscription.objects.filter(customer_name=self.request.user, subscription_status='EXPIRED').all().order_by('subscription_ends')
        return Response(context)



class CustomerINRSubscriptionView(APIView):
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request, *args, **kwargs):
        context = {}
        try:
            if CustomerSubscription.objects.filter(customer_name=request.user).exists():
                subscriberDetails = CustomerSubscription.objects.filter(customer_name=request.user).first()
                data = {'first_name':subscriberDetails.first_name, 'last_name': subscriberDetails.last_name,
                'email':subscriberDetails.email, 'contact_no':subscriberDetails.contact_no}
                context['data'] = data
                context['status'] = 'These are your previous subscription details'
            else:
                data = {'first_name':self.request.user.first_name, 'last_name': self.request.user.last_name,
                'email':self.request.user.email, 'contact_no':''}
                context['data'] = data
                context['status'] = 'Welcome, This is your first subscription!'
        except Exception as e:
            prit(e)
            context['data'] = {}
            context['status'] = 'Unknown Internal Server Error!'
        finally:
            return Response(context)

    def post(self, request, *args, **kwargs):
        requestData = json.loads(request.body.decode('utf-8'))
        context = {}
        try:
            if requestData:
                first_name = requestData['first_name']
                last_name = requestData['last_name']
                email = requestData['email']
                contact_number = requestData['contact_no']
                subscription = requestData['subscription_type']
                order_amount = 0
                plan_type = ' '
                if subscription == 'Premium (Quarterly) - Rs.1440/month + GST 18%':
                    order_amount = 4705.84
                    plan_type = 'QUARTER'
                elif subscription == 'Premium (Annual) - Rs.1440/month + GST 18%':
                    order_amount = 16992
                    plan_type = 'ANNUAL'
                client = razorpay.Client(auth=(RAZORPAY_CLIENT_KEY, RAZORPAY_SECRET_KEY))
                order_currency = 'INR'
                # CREAING ORDER
                response = client.order.create(dict(amount=order_amount*100, currency=order_currency, payment_capture='0'))
                order_id = response['id']
                order_status = response['status']
                context = dict()
                if order_status=='created':
                    # Setting data into cache for creating the subscription
                    cache.set(request.user.username+'rplan_type',plan_type,1*60*60)
                    cache.set(request.user.username+'rcurrency',order_currency,1*60*60)
                    cache.set(request.user.username+'rfirst_name',first_name,1*60*60)
                    cache.set(request.user.username+'rlast_name',last_name,1*60*60)
                    cache.set(request.user.username+'remail',email,1*60*60)
                    cache.set(request.user.username+'rcontact',contact_number,1*60*60)
                    cache.set(request.user.username+'rsubscription_type',plan_type,1*60*60)
                    cache.set(request.user.username+'rsubscription_price',order_amount,1*60*60)
                    # Server data for user convinience
                    context['product_id'] = '72PI Premium Subscription'
                    context['price'] = order_amount
                    context['currency'] = order_currency
                    context['name'] = first_name
                    context['phone'] = contact_number
                    context['email'] = email
                    from datetime import date
                    context['start'] = date.today()
                    if plan_type == 'ANNUAL':
                        context['end'] = date.today()+timedelta(days=364)
                    elif plan_type == 'QUARTER':
                        context['end'] = date.today()+timedelta(days=91)
                    context['payfor'] = plan_type
                    # data that'll be send to the razorpay for payment
                    context['order_id'] = order_id
                    context['status'] = 201
                    context['reason'] = 'success'
                else:
                    context['status'] = 'Something went wrong, please try again!'
                    context['reason'] = 'Sorry, we are unable to create order right now'
            else:
                context['status'] = 'Something went wrong, please try again!'
                context['reason'] = 'Invalid Data Submitted!'
        except Exception as e:
            print(e)
            context['status'] = 'Unknown Internal Server Error!'
            context['reason'] = 'Please try again after some time!'
        finally:
            return Response(context)


class CustomerUSDSubscriptionView(APIView):
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request, *args, **kwargs):
        context = {}
        try:
            if CustomerSubscription.objects.filter(customer_name=request.user).exists():
                subscriberDetails = CustomerSubscription.objects.filter(customer_name=request.user).first()
                data = {'first_name':subscriberDetails.first_name, 'last_name': subscriberDetails.last_name,
                'email':subscriberDetails.email, 'contact_no':subscriberDetails.contact_no}
                context['data'] = data
                context['status'] = 'These are your previous subscription details'
            else:
                data = {'first_name':self.request.user.first_name, 'last_name': self.request.user.last_name,
                'email':self.request.user.email, 'contact_no':''}
                context['data'] = data
                context['status'] = 'Welcome, This is your first subscription!'
        except Exception as e:
            prit(e)
            context['data'] = {}
            context['status'] = 'Unknown Internal Server Error!'
        finally:
            return Response(context)

    def post(self, request, *args, **kwargs):
        requestData = json.loads(request.body.decode('utf-8'))
        context = {}
        try:
            if requestData:
                first_name = requestData['first_name']
                last_name = requestData['last_name']
                email = requestData['email']
                contact_number = requestData['contact_no']
                subscription = requestData['subscription_type']
                order_amount = 0
                plan_type = ' '
                if subscription == 'Premium (Quarterly) - 20$/month':
                    order_amount = 56
                    plan_type = 'QUARTER'
                elif subscription == 'Premium (Annual) - 20$/month':
                    order_amount = 200
                    plan_type = 'ANNUAL'
                client = razorpay.Client(auth=(RAZORPAY_CLIENT_KEY, RAZORPAY_SECRET_KEY))
                order_currency = 'USD'
                # CREAING ORDER
                response = client.order.create(dict(amount=order_amount*100, currency=order_currency, payment_capture='0'))
                order_id = response['id']
                order_status = response['status']
                context = dict()
                if order_status=='created':
                    cache.set(request.user.username+'rplan_type',plan_type,1*60*60)
                    cache.set(request.user.username+'rcurrency',order_currency,1*60*60)
                    cache.set(request.user.username+'rfirst_name',first_name,1*60*60)
                    cache.set(request.user.username+'rlast_name',last_name,1*60*60)
                    cache.set(request.user.username+'remail',email,1*60*60)
                    cache.set(request.user.username+'rcontact',contact_number,1*60*60)
                    cache.set(request.user.username+'rsubscription_type',plan_type,1*60*60)
                    cache.set(request.user.username+'rsubscription_price',order_amount,1*60*60)
                    # Server data for user convinience
                    context['product_id'] = '72PI Premium Subscription'
                    context['price'] = order_amount
                    context['currency'] = order_currency
                    context['name'] = first_name
                    context['phone'] = contact_number
                    context['email'] = email
                    from datetime import date
                    context['start'] = date.today()
                    if plan_type == 'ANNUAL':
                        context['end'] = date.today()+timedelta(days=364)
                    elif plan_type == 'QUARTER':
                        context['end'] = date.today()+timedelta(days=91)
                    context['payfor'] = plan_type
                    # data that'll be send to the razorpay for
                    context['order_id'] = order_id
                    context['status'] = 201
                    context['reason'] = 'success'
                else:
                    context['status'] = 'Something went wrong, please try again!'
                    context['reason'] = 'Sorry, we are unable to create order right now'
            else:
                context['status'] = 'Something went wrong, please try again!'
                context['reason'] = 'Invalid Data Submitted!'
        except Exception as e:
            print(e)
            context['status'] = 'Unknown Internal Server Error!'
            context['reason'] = 'Please try again after some time!'
        return Response(context)


class CustomerSubscriptionValidation(APIView):
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request, *args, **kwargs):
        context = {}
        try:
            if request.user.is_authenticated:
                if not CustomerSubscription.objects.filter(customer_name= request.user).exists():
                    from datetime import date
                    endsubscription = date.today() + timedelta(days=30)
                    startsubscription = date.today()
                    CustomerSubscription(customer_name=request.user, first_name=request.user.first_name,last_name=request.user.last_name,
                    email=request.user.email,contact_no='',subscription_type='FREE',subscription_starts=startsubscription,subscription_ends=endsubscription,
                    subscription_status='ACTIVE', subscription_plan='30 DAY TRIAL', paid='NO',currency='NA').save()
                    context = {'status' : 'activated'}
                else:
                    context = {'status' : 'already_activated'}
            else:
                context = {'status' : 'not_activated'}
        except Exception as e:
            print(e)
            context = {'status' : 'error_occured'}
        finally:
            return Response(context)


class CustomerContactUsView(APIView):
    authentication_classes = []
    permission_classes = []

    def post(self, request, *args, **kwargs):
        context = {}
        try:
            params = json.loads(request.body.decode('utf-8'))
            firstname,lastname,email = params['data']['firstName'],params['data']['lastName'],params['data']['eMail']
            phoneno,subject,message = params['data']['phoneNo'],params['data']['subJect'],params['data']['messAge']
            CustomerContactUs(first_name=firstname,last_name=lastname,email=email,contact_no=phoneno,subject=subject,message=message).save()
            context['response'] = 'Thanks for contacting us, we will be back to you soon!'
            context['status'] = 'success'
        except Exception as e:
            print(e)
            context['response'] = 'Your request is not received, please try again!'
            context['status'] = 'error'
        finally:
            return Response(context)


class MyAlerts(APIView):
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]

    def post(self, request, *args, **kwargs):
        alerts = list() #Empty List
        try:
            params= json.loads(request.body.decode('utf-8'))
            counTRY = params['country']
            notifications1 = pd.DataFrame(list(CustomerPriceMovementsAlerts.objects.filter(customer_name=self.request.user,price_status='Waiting', country=counTRY).values('id','company_name','portfolio_name','alert_type','price_value_condition','price_value','country','buy_sell')))
            if(len(notifications1)>0):
                notifications1.fillna(0,inplace=True)
                notifications1.columns = ['id','Company','Portfolio Name','Type','Condition','Value','country','Buy/Sell']
                notifications1 = notifications1.to_dict('records')
            notifications2 = pd.DataFrame(list(CustomerPriceMovementsAlerts.objects.filter(customer_name=self.request.user,percent_status='Waiting',country=counTRY).values('id','company_name','portfolio_name','alert_type','price_percent_condition','price_percent','country','buy_sell')))
            if(len(notifications2)>0):
                notifications2.fillna(0,inplace=True)
                notifications2.columns = ['id','Company','Portfolio Name','Type','Condition','Value','country','Buy/Sell']
                notifications2 = notifications2.to_dict('records')
            notifications3 = pd.DataFrame(list(CustomerPriceMovementsAlerts.objects.filter(customer_name=self.request.user,price52wh_status='Waiting',country=counTRY).values('id','company_name','portfolio_name','alert_type','price_52wh_condition','price_52wh_percent','country','buy_sell')))
            if(len(notifications3)>0):
                notifications3.fillna(0,inplace=True)
                notifications3.columns = ['id','Company','Portfolio Name','Type','Condition','Value','country','Buy/Sell']
                notifications3 = notifications3.to_dict('records')
            notifications4 = pd.DataFrame(list(CustomerTechnicalIndicationAlerts.objects.filter(customer_name=self.request.user,rsi_status='Waiting',country=counTRY).values('id','company_name','portfolio_name','alert_type','rsi_condition','rsi_operand1','country','buy_sell')))
            if(len(notifications4)>0):
                notifications4.fillna(0,inplace=True)
                notifications4.columns = ['id','Company','Portfolio Name','Type','Condition','Value','country','Buy/Sell']
                notifications4 = notifications4.to_dict('records')
            notifications5 = pd.DataFrame(list(CustomerTechnicalIndicationAlerts.objects.filter(customer_name=self.request.user,dma_status='Waiting',country=counTRY).values('id','company_name','portfolio_name','alert_type','dma_operand1','dma_condition','dma_operand2','country','buy_sell')))
            if(len(notifications5)>0):
                notifications5.fillna(0,inplace=True)
                notifications5.columns = ['id','Company','Portfolio Name','Type','Metric','Condition','Value','country','Buy/Sell']
                notifications5 = notifications5.to_dict('records')
            #Price Movement Notifications
            for item in notifications1:
                # Conditions for Price in Rs.
                if item['Value'] and item['Condition']:
                    item['Metric'] = 'Last Price'
                    item['subtype'] = 'pricealert'
                    item['symbol'] = '₹'
                    if item['country'] == 'US':
                        item['symbol'] = '$'
                    alerts.append(item)
            for item in notifications2:
                # Conditions for Price in %
                if item['Value'] and item['Condition']:
                    item['Metric'] = 'Last Price'
                    item['subtype'] = 'percentalert'
                    item['symbol'] = '%'
                    alerts.append(item)
            for item in notifications3:
                # Conditions for 52-week high
                if item['Value'] and item['Condition']:
                    item['Metric'] = 'Last Price'
                    item['subtype'] = '52whalert'
                    item['symbol'] = '% of 52-WH'
                    alerts.append(item)
            # # Technical Indication Notifications
            for item in notifications4:
                # Conditions for 14 Day RSI
                if item['Condition'] and item['Value']:
                    item['Metric'] = '14 Days RSI'
                    item['subtype'] = 'rsialert'
                    item['symbol'] = ''
                    alerts.append(item)
            for item in notifications5:
                # Conditions for DMA
                if item['Metric'] and item['Condition'] and item['Value']:
                    item['subtype'] = 'dmaalert'
                    item['symbol'] = ''
                    alerts.append(item)
        except Exception as e:
            print(e)
        finally:
            return Response({'alerts':alerts})


class MyAlertsEdit(APIView):
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]

    def post(self, request, *args, **kwargs):   
        params = json.loads(request.body.decode('utf-8'))
        response = {}
        try:
            _id = params['object']['id']
            company = params['object']['Company']
            portfolio = params['object']['Portfolio Name']
            alerttype = params['object']['Type']
            condition = params['condition']
            rightvalue = params['rightvalue']
            subtype = params['object']['subtype']
            if alerttype == 'Price Movement':
                if subtype == 'pricealert':
                    CustomerPriceMovementsAlerts.objects.filter(id=_id).update(price_value_condition = condition,price_value = rightvalue)
                    response['status'] = 'True'
                    response['result'] = 'Success'
                elif subtype == 'percentalert':
                    CustomerPriceMovementsAlerts.objects.filter(id=_id).update(price_percent_condition = condition,price_percent = rightvalue)
                    response['status'] = 'True'
                    response['result'] = 'Success'
                elif subtype == '52whalert':
                    CustomerPriceMovementsAlerts.objects.filter(id=_id).update(price_52wh_condition = condition,price_52wh_percent = rightvalue)
                    response['status'] = 'True'
                    response['result'] = 'Success'
                else:
                    response['status'] = 'False'
                    response['result'] = 'Could not update the alert at this moment, try again!'
            elif alerttype == 'Technical Indicator':
                if subtype == 'rsialert':
                    CustomerTechnicalIndicationAlerts.objects.filter(id=_id).update(rsi_condition=condition,rsi_operand1=rightvalue)
                    response['status'] = 'True'
                    response['result'] = 'Success'
                elif subtype == 'dmaalert':
                    leftvalue = params['leftvalue']
                    CustomerTechnicalIndicationAlerts.objects.filter(id=_id).update(dma_condition=condition,dma_operand1=leftvalue,dma_operand2=rightvalue)
                    response['status'] = 'True'
                    response['result'] = 'Success'
                else:
                    response['status'] = 'False'
                    response['result'] = 'Could not update the alert at this moment, try again!'
            else:
                response['status'] = 'False'
                response['result'] = 'Could not update the alert at this moment, try again!'
        except Exception as e:
            print(e)
        context={'response':response}
        return Response(context)


class MyAlertsDelete(APIView):
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]

    def post(self, request, *args, **kwargs):   
        params = json.loads(request.body.decode('utf-8'))
        response = {}
        _id = params['object']['id']
        alerttype = params['object']['Type']
        subtype = params['object']['subtype']
        if alerttype == 'Price Movement':
            if subtype == 'pricealert':
                CustomerPriceMovementsAlerts.objects.filter(id=_id).update(price_value_condition = None,price_value = None,price_status='Deleted',price_read='NA')
                response['status'] = 'True'
                response['result'] = 'Success'
            elif subtype == 'percentalert':
                CustomerPriceMovementsAlerts.objects.filter(id=_id).update(price_percent_condition = None,price_percent = None,percent_status='Deleted',percent_read='NA')
                response['status'] = 'True'
                response['result'] = 'Success'
            elif subtype == '52whalert':
                CustomerPriceMovementsAlerts.objects.filter(id=_id).update(price_52wh_condition = None,price_52wh_percent = None,price52wh_status='Deleted',price52wh_read='No')
                response['status'] = 'True'
                response['result'] = 'Success'
            else:
                response['status'] = 'False'
                response['result'] = 'Could not delete the alert at this moment, try again!'
        elif alerttype == 'Technical Indicator':
            if subtype == 'rsialert':
                CustomerTechnicalIndicationAlerts.objects.filter(id=_id).update(rsi_condition=None,rsi_operand1=None,rsi_metric=None,rsi_status='Deleted',rsi_read='No')
                response['status'] = 'True'
                response['result'] = 'Success'
            elif subtype == 'dmaalert':
                CustomerTechnicalIndicationAlerts.objects.filter(id=_id).update(dma_condition=None,dma_operand1=None,dma_operand2=None,dma_status='Deleted',dma_read='No')
                response['status'] = 'True'
                response['result'] = 'Success'
            else:
                response['status'] = 'False'
                response['result'] = 'Could not delete the alert at this moment, try again!'
        else:
            response['status'] = 'False'
            response['result'] = 'Could not delete the alert at this moment, try again!'
        context={'response':response}
        return Response(context) 