from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class CustomerPriceMovementsAlerts(models.Model):
    customer_name = models.ForeignKey(User, on_delete=models.CASCADE)
    customer_email = models.EmailField(max_length=255)
    portfolio_name = models.CharField(max_length=255)
    company_name = models.CharField(max_length=255)
    alert_type = models.CharField(max_length=25, default="Price Movement")
    last_price = models.FloatField(null=True, blank=True)
    current_price = models.FloatField(null=True, blank=True)
    price_52wh = models.FloatField(null=True, blank=True)
    price_value_condition = models.CharField(max_length=25, null=True, blank=True)
    price_value = models.FloatField(null=True, blank=True)
    price_percent_condition = models.CharField(max_length=25, null=True, blank=True)
    price_percent = models.FloatField(null=True, blank=True)
    price_52wh_condition = models.CharField(max_length=25, null=True, blank=True)
    price_52wh_percent = models.FloatField(null=True, blank=True)
    price_createdat = models.DateTimeField(blank=True, null=True)
    pricepercent_createdat = models.DateTimeField(blank=True, null=True)
    price52wh_createdat = models.DateTimeField(blank=True, null=True)
    price_triggeredat = models.DateTimeField(blank=True, null=True)
    percent_triggeredat = models.DateTimeField(blank=True, null=True)
    price52wh_triggeredat = models.DateTimeField(blank=True, null=True)
    price_status = models.CharField(max_length=10, default='NA')
    percent_status = models.CharField(max_length=10, default='NA')
    price52wh_status = models.CharField(max_length=10, default='NA')
    price_read = models.CharField(max_length=5, default='No')
    percent_read = models.CharField(max_length=5, default='No')
    price52wh_read = models.CharField(max_length=5, default='No')
    price_checked = models.CharField(max_length=5, default='No')
    percent_checked = models.CharField(max_length=5, default='No')
    price52wh_checked = models.CharField(max_length=5, default='No')
    country = models.CharField(max_length=10, default='India')
    buy_sell = models.CharField(max_length=5, default='Buy')

    def __str__(self):
        return self.company_name

    class Meta:
        verbose_name_plural = "CustomerPriceMovementsAlerts"


class CustomerTechnicalIndicationAlerts(models.Model):
    customer_name = models.ForeignKey(User, on_delete=models.CASCADE)
    customer_email = models.EmailField(max_length=255)
    portfolio_name = models.CharField(max_length=255)
    company_name = models.CharField(max_length=255)
    alert_type = models.CharField(max_length=25, default="Technical Indicatior")
    factset_ticker = models.CharField(max_length=25, blank=True, null=True)
    rsi_metric = models.CharField(max_length=25, null=True, blank=True,default='14 Day RSI')
    rsi_condition = models.CharField(max_length=25, null=True, blank=True)
    rsi_operand1 = models.FloatField(null=True, blank=True)
    rsi_operand2 = models.FloatField(null=True, blank=True) # Variable
    dma_operand1 = models.CharField(max_length=25, null=True, blank=True)
    dma_condition = models.CharField(max_length=25, null=True, blank=True)
    dma_operand2 = models.CharField(max_length=25, null=True, blank=True) 
    ma9 = models.FloatField(db_column='MA9', blank=True, null=True)  
    ma20 = models.FloatField(db_column='MA20', blank=True, null=True)  
    ma26 = models.FloatField(db_column='MA26', blank=True, null=True)  
    ma50 = models.FloatField(db_column='MA50', blank=True, null=True)  
    ma100 = models.FloatField(db_column='MA100', blank=True, null=True) 
    ma200 = models.FloatField(db_column='MA200', blank=True, null=True)
    current_price = models.FloatField(null=True, blank=True) 
    rsi_createdat = models.DateTimeField(blank=True, null=True)
    dma_createdat = models.DateTimeField(blank=True, null=True)
    rsi_triggeredat = models.DateTimeField(blank=True, null=True)
    dma_triggeredat = models.DateTimeField(blank=True, null=True)
    rsi_status = models.CharField(max_length=10, default='NA')
    dma_status = models.CharField(max_length=10, default='NA')
    rsi_read = models.CharField(max_length=10, default='No')
    dma_read = models.CharField(max_length=10, default='No')
    rsi_checked = models.CharField(max_length=10, default='No')
    dma_checked = models.CharField(max_length=10, default='No')
    country = models.CharField(max_length=10, default='India')
    buy_sell = models.CharField(max_length=5, default='Buy')

    def __str__(self):
        return self.company_name

    class Meta:
        verbose_name_plural = "CustomerTechnicalIndicationAlerts"


class NewsLetterSubscription(models.Model):
    email = models.EmailField(db_column='Email',null=True, blank=True)
    subscribed_at = models.DateTimeField(db_column='Subscribed_at', auto_now_add=True)
    country = models.CharField(max_length=10, default='India')
    def __str__(self):
        self.email

class CustomerFeedback(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE,db_column='Customer_Id')
    rating = models.FloatField(db_column='Rating', null=True,blank=True)
    feedback = models.TextField(db_column='Feedback', null=True,blank=True)
    country = models.CharField(db_column='country',max_length=10, default='India')
    
    def __str__(self):
        self.user

class CustomerSubscription(models.Model):
    customer_name = models.ForeignKey(User, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=255, null=True, blank=True)
    last_name= models.CharField(max_length=255, null=True, blank=True)
    email = models.EmailField(null=True, blank=True, max_length=255)
    contact_no = models.CharField(max_length=15, null=True, blank=True)
    address = models.CharField(max_length=255, null=True, blank=True)
    pan_no= models.CharField(max_length=255, null=True, blank=True)
    gst_no = models.CharField(max_length=255, null=True, blank=True)
    subscription_type = models.CharField(max_length=255, null=True, blank=True)
    subscription_starts = models.DateField(blank=True, null=True)
    subscription_ends = models.DateField(blank=True, null=True)
    subscription_status = models.CharField(choices=(('ACTIVE','ACTIVE'),('EXPIRED','EXPIRED'), ('UPCOMING','UPCOMING')),default='ACTIVE',max_length=255)
    subscription_plan = models.CharField(max_length=25, null=True, blank=True)
    paid = models.CharField(max_length=4, choices=(('YES','YES'),('NO','NO')), default='YES')
    currency = models.CharField(max_length=4, choices=(('INR','INR'),('USD','USD'),('NA','NA')), default='NA')

    def __str__(self):
        return self.first_name

    class Meta:
        verbose_name_plural = 'CustomerSubscriptions'


class CustomerContactUs(models.Model):
    first_name = models.CharField(max_length=255, null=True, blank=True)
    last_name= models.CharField(max_length=255, null=True, blank=True) 
    email = models.EmailField(null=True, blank=True, max_length=255) 
    contact_no = models.CharField(max_length=15, null=True, blank=True) 
    subject = models.CharField(max_length=255, null=True, blank=True) 
    message = models.TextField(max_length=255, null=True, blank=True) 
    contacted_at = models.DateTimeField(auto_now_add=True)
    
    def __str__(self):
        return self.first_name

    class Meta:
        verbose_name_plural = 'CustomerContactUs'