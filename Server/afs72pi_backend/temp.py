# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class AdvancesDeclines(models.Model):
    date = models.TextField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    count = models.BigIntegerField(db_column='Count', blank=True, null=True)  # Field name made lowercase.
    type = models.TextField(db_column='Type', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Advances_Declines'


class AverageTrueRange(models.Model):
    date = models.DateTimeField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    company_name = models.TextField(db_column='Company_Name', blank=True, null=True)  # Field name made lowercase.
    company_code = models.FloatField(db_column='Company_Code', blank=True, null=True)  # Field name made lowercase.
    open = models.FloatField(db_column='Open', blank=True, null=True)  # Field name made lowercase.
    low = models.FloatField(db_column='Low', blank=True, null=True)  # Field name made lowercase.
    high = models.FloatField(db_column='High', blank=True, null=True)  # Field name made lowercase.
    close = models.FloatField(db_column='Close', blank=True, null=True)  # Field name made lowercase.
    volume = models.FloatField(db_column='Volume', blank=True, null=True)  # Field name made lowercase.
    prev_close = models.FloatField(db_column='Prev_Close', blank=True, null=True)  # Field name made lowercase.
    tr = models.FloatField(db_column='TR', blank=True, null=True)  # Field name made lowercase.
    typical_price = models.FloatField(db_column='Typical_Price', blank=True, null=True)  # Field name made lowercase.
    atr = models.FloatField(db_column='ATR', blank=True, null=True)  # Field name made lowercase.
    fs_ticker = models.CharField(db_column='FS_Ticker', max_length=255, blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Average_True_Range'


class BseMapping(models.Model):
    isin_no = models.CharField(db_column='ISIN No', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    security_name = models.CharField(db_column='Security Name', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    fs_name = models.CharField(db_column='FS Name', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    bseindia_name = models.CharField(db_column='BSEIndia Name', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'BSE_Mapping'


class BsePredictions(models.Model):
    ticker = models.CharField(db_column='Ticker', max_length=50, blank=True, null=True)  # Field name made lowercase.
    company_name = models.CharField(db_column='Company_Name', max_length=500, blank=True, null=True)  # Field name made lowercase.
    return_prediction = models.FloatField(db_column='Return_Prediction', blank=True, null=True)  # Field name made lowercase.
    confidence_level = models.CharField(db_column='Confidence LEVEL', max_length=50, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    period = models.CharField(db_column='Period', max_length=255, blank=True, null=True)  # Field name made lowercase.
    flag = models.CharField(db_column='Flag', max_length=50, blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'BSE_Predictions'


class BondEquityEarnings(models.Model):
    date = models.DateTimeField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    number_10yr_g_sec = models.FloatField(db_column='10Yr G-Sec', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    p_e = models.FloatField(db_column='P/E', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    beer = models.FloatField(db_column='Beer', blank=True, null=True)  # Field name made lowercase.
    average = models.FloatField(db_column='Average', blank=True, null=True)  # Field name made lowercase.
    std = models.FloatField(db_column='StD', blank=True, null=True)  # Field name made lowercase.
    number_1sd = models.FloatField(db_column='1sd', blank=True, null=True)  # Field renamed because it wasn't a valid Python identifier.
    number_2sd = models.FloatField(db_column='2sd', blank=True, null=True)  # Field renamed because it wasn't a valid Python identifier.
    number_3sd = models.FloatField(db_column='3sd', blank=True, null=True)  # Field renamed because it wasn't a valid Python identifier.
    field_1sd = models.FloatField(db_column='-1sd', blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it started with '_'.
    field_2sd = models.FloatField(db_column='-2sd', blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it started with '_'.
    field_3sd = models.FloatField(db_column='-3sd', blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it started with '_'.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Bond_Equity_Earnings'


class BuyTrades(models.Model):
    stock = models.TextField(db_column='Stock', blank=True, null=True)  # Field name made lowercase.
    trade_time = models.DateTimeField(db_column='Trade_Time', blank=True, null=True)  # Field name made lowercase.
    remaining_position = models.BigIntegerField(db_column='Remaining_Position', blank=True, null=True)  # Field name made lowercase.
    price = models.FloatField(db_column='Price', blank=True, null=True)  # Field name made lowercase.
    customer_name = models.TextField(db_column='Customer_Name', blank=True, null=True)  # Field name made lowercase.
    portfolio_name = models.TextField(db_column='Portfolio_name', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Buy_Trades'


class CboeVix(models.Model):
    date = models.DateTimeField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    vix = models.FloatField(db_column='VIX', blank=True, null=True)  # Field name made lowercase.
    average = models.FloatField(db_column='Average', blank=True, null=True)  # Field name made lowercase.
    std = models.FloatField(db_column='StD', blank=True, null=True)  # Field name made lowercase.
    number_1sd = models.FloatField(db_column='1sd', blank=True, null=True)  # Field renamed because it wasn't a valid Python identifier.
    number_2sd = models.FloatField(db_column='2sd', blank=True, null=True)  # Field renamed because it wasn't a valid Python identifier.
    number_3sd = models.FloatField(db_column='3sd', blank=True, null=True)  # Field renamed because it wasn't a valid Python identifier.
    field_1sd = models.FloatField(db_column='-1sd', blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it started with '_'.
    field_2sd = models.FloatField(db_column='-2sd', blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it started with '_'.
    field_3sd = models.FloatField(db_column='-3sd', blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it started with '_'.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'CBOE_VIX'


class CompanyDescriptions(models.Model):
    co_code = models.CharField(max_length=255, blank=True, null=True)
    company = models.TextField(db_column='Company', blank=True, null=True)  # Field name made lowercase.
    description = models.TextField(db_column='Description', blank=True, null=True)  # Field name made lowercase.
    shortdescription = models.TextField(db_column='ShortDescription', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Company_Descriptions'


class CompanyAddressDetails2(models.Model):
    lname = models.TextField(db_column='LNAME', blank=True, null=True)  # Field name made lowercase.
    isin = models.TextField(db_column='ISIN', blank=True, null=True)  # Field name made lowercase.
    hse_s_name = models.TextField(db_column='HSE_S_NAME', blank=True, null=True)  # Field name made lowercase.
    inc_dt = models.TextField(db_column='INC_DT', blank=True, null=True)  # Field name made lowercase.
    regadd1 = models.TextField(db_column='REGADD1', blank=True, null=True)  # Field name made lowercase.
    regadd2 = models.TextField(db_column='REGADD2', blank=True, null=True)  # Field name made lowercase.
    regdist = models.TextField(db_column='REGDIST', blank=True, null=True)  # Field name made lowercase.
    regstate = models.TextField(db_column='REGSTATE', blank=True, null=True)  # Field name made lowercase.
    regpin = models.TextField(db_column='REGPIN', blank=True, null=True)  # Field name made lowercase.
    tel1 = models.TextField(db_column='TEL1', blank=True, null=True)  # Field name made lowercase.
    ind_l_name = models.TextField(blank=True, null=True)
    tel2 = models.TextField(db_column='TEL2', blank=True, null=True)  # Field name made lowercase.
    fax1 = models.TextField(db_column='FAX1', blank=True, null=True)  # Field name made lowercase.
    fax2 = models.TextField(db_column='FAX2', blank=True, null=True)  # Field name made lowercase.
    auditor = models.TextField(db_column='AUDITOR', blank=True, null=True)  # Field name made lowercase.
    fv = models.FloatField(db_column='FV', blank=True, null=True)  # Field name made lowercase.
    mkt_lot = models.BigIntegerField(db_column='MKT_LOT', blank=True, null=True)  # Field name made lowercase.
    chairman = models.TextField(db_column='CHAIRMAN', blank=True, null=True)  # Field name made lowercase.
    co_sec = models.TextField(db_column='CO_SEC', blank=True, null=True)  # Field name made lowercase.
    co_code = models.FloatField(db_column='CO_CODE', blank=True, null=True)  # Field name made lowercase.
    email = models.TextField(db_column='EMAIL', blank=True, null=True)  # Field name made lowercase.
    internet = models.TextField(db_column='INTERNET', blank=True, null=True)  # Field name made lowercase.
    dir_name = models.TextField(blank=True, null=True)
    dir_desg = models.TextField(blank=True, null=True)
    ticker = models.TextField(db_column='Ticker', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Company_address_details_2'


class ConsFlags(models.Model):
    fs_ticker = models.CharField(db_column='FS Ticker', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    fs_name = models.CharField(db_column='FS Name', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    isin_no = models.CharField(db_column='ISIN No', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    nifty_50 = models.CharField(db_column='Nifty 50', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    nifty_small_cap_100 = models.CharField(db_column='Nifty Small Cap 100', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    nifty_mid_cap_100 = models.CharField(db_column='Nifty Mid Cap 100', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Cons_Flags'


class CorporateActions(models.Model):
    eventtype = models.CharField(max_length=255, blank=True, null=True)
    co_code = models.IntegerField(blank=True, null=True)
    co_name = models.CharField(max_length=255, blank=True, null=True)
    bse_code = models.IntegerField(blank=True, null=True)
    symbol = models.CharField(max_length=255, blank=True, null=True)
    isin = models.CharField(max_length=255, blank=True, null=True)
    exdate = models.CharField(db_column='exDate', max_length=255, blank=True, null=True)  # Field name made lowercase.
    recorddate = models.CharField(db_column='recordDate', max_length=255, blank=True, null=True)  # Field name made lowercase.
    existing_value = models.CharField(max_length=255, blank=True, null=True)
    adjusted_value = models.CharField(max_length=255, blank=True, null=True)
    dividend_amount = models.CharField(max_length=255, blank=True, null=True)
    dividend_pct = models.CharField(max_length=255, blank=True, null=True)
    divd_type = models.CharField(max_length=255, blank=True, null=True)
    premium = models.CharField(max_length=255, blank=True, null=True)
    anndate = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'Corporate_Actions'


class CustomerDematPortfolio(models.Model):
    customer_name = models.CharField(db_column='Customer_Name', max_length=100, blank=True, null=True)  # Field name made lowercase.
    portfolio_name = models.CharField(db_column='Portfolio_Name', max_length=100, blank=True, null=True)  # Field name made lowercase.
    company = models.CharField(db_column='Company', max_length=256, blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Customer_DEMAT_Portfolio'


class CustomerPortfolioDetails(models.Model):
    customer_name = models.CharField(db_column='Customer Name', max_length=255)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    portfolio_name = models.CharField(db_column='Portfolio Name', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    company_name = models.CharField(db_column='Company Name', max_length=255)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    price = models.FloatField(db_column='Price', blank=True, null=True)  # Field name made lowercase.
    quantity = models.IntegerField(db_column='Quantity', blank=True, null=True)  # Field name made lowercase.
    portfolio_type = models.CharField(max_length=255, blank=True, null=True)
    sector_name = models.CharField(db_column='Sector Name', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    model_portfolio = models.CharField(max_length=5, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    country = models.TextField(db_column='Country', blank=True, null=True)  # Field name made lowercase.
    fs_ticker = models.TextField(db_column='FS_Ticker', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Customer_Portfolio_Details'


class CustomerPortfolioLog(models.Model):
    customer_name = models.CharField(db_column='Customer Name', max_length=255)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    portfolio_name = models.CharField(db_column='Portfolio Name', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    created_at = models.DateTimeField(db_column='Created at', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    last_modified_at = models.DateTimeField(db_column='Last Modified at', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    last_action = models.CharField(db_column='Last Action', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    status = models.CharField(db_column='Status', max_length=255, blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Customer_Portfolio_Log'


class CustomerTradeSignals(models.Model):
    buy_sell = models.CharField(db_column='Buy_Sell', max_length=5, blank=True, null=True)  # Field name made lowercase.
    customer_name = models.CharField(db_column='Customer Name', max_length=255)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    company_name = models.CharField(db_column='Company Name', max_length=255)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    limited_price = models.FloatField(db_column='Limited Price', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    quantity = models.IntegerField(db_column='Quantity', blank=True, null=True)  # Field name made lowercase.
    expiry_date = models.DateField(db_column='Expiry Date', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Customer_Trade_Signals'


class DailyEveningEmailTemplate(models.Model):
    date = models.DateField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    headers = models.TextField(db_column='Headers', blank=True, null=True)  # Field name made lowercase.
    country = models.TextField(db_column='Country', blank=True, null=True)  # Field name made lowercase.
    index = models.TextField(db_column='Index', blank=True, null=True)  # Field name made lowercase.
    price = models.TextField(db_column='Price', blank=True, null=True)  # Field name made lowercase.
    dtd = models.FloatField(db_column='DTD', blank=True, null=True)  # Field name made lowercase.
    description = models.TextField(db_column='Description', blank=True, null=True)  # Field name made lowercase.
    dtd_color = models.TextField(db_column='DTD_Color', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Daily_Evening_Email_Template'


class DailyMarketData(models.Model):
    ticker = models.TextField(db_column='Ticker', blank=True, null=True)  # Field name made lowercase.
    current_price = models.FloatField(db_column='Current_Price', blank=True, null=True)  # Field name made lowercase.
    net_change = models.FloatField(db_column='Net_Change', blank=True, null=True)  # Field name made lowercase.
    percentage_change = models.FloatField(db_column='Percentage_Change', blank=True, null=True)  # Field name made lowercase.
    date = models.DateTimeField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    order = models.IntegerField(db_column='Order', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Daily_Market_Data'


class DailyMorningEmailTemplate(models.Model):
    date = models.DateField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    headers = models.TextField(db_column='Headers', blank=True, null=True)  # Field name made lowercase.
    country = models.TextField(db_column='Country', blank=True, null=True)  # Field name made lowercase.
    index = models.TextField(db_column='Index', blank=True, null=True)  # Field name made lowercase.
    sub_index = models.TextField(db_column='Sub Index', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    price = models.TextField(db_column='Price', blank=True, null=True)  # Field name made lowercase.
    dtd = models.FloatField(db_column='DTD', blank=True, null=True)  # Field name made lowercase.
    wtd = models.FloatField(db_column='WTD', blank=True, null=True)  # Field name made lowercase.
    dtd_color = models.TextField(db_column='DTD_Color', blank=True, null=True)  # Field name made lowercase.
    wtd_color = models.TextField(db_column='WTD_Color', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Daily_Morning_Email_Template'


class DailyPositions(models.Model):
    date = models.DateField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    stock = models.TextField(db_column='Stock', blank=True, null=True)  # Field name made lowercase.
    buy_count = models.BigIntegerField(db_column='Buy_Count', blank=True, null=True)  # Field name made lowercase.
    buy_quantity = models.BigIntegerField(db_column='Buy_Quantity', blank=True, null=True)  # Field name made lowercase.
    sell_count = models.FloatField(db_column='Sell_Count', blank=True, null=True)  # Field name made lowercase.
    sell_quantity = models.FloatField(db_column='Sell_Quantity', blank=True, null=True)  # Field name made lowercase.
    close_price = models.FloatField(db_column='Close_Price', blank=True, null=True)  # Field name made lowercase.
    prev_close_price = models.FloatField(db_column='Prev_Close_Price', blank=True, null=True)  # Field name made lowercase.
    prev_position = models.BigIntegerField(db_column='Prev_Position', blank=True, null=True)  # Field name made lowercase.
    prev_p_lunrealized = models.BigIntegerField(db_column='Prev_P&LUnrealized', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    bbg_ticker = models.TextField(db_column='BBG_Ticker', blank=True, null=True)  # Field name made lowercase.
    position = models.FloatField(db_column='Position', blank=True, null=True)  # Field name made lowercase.
    p_lrealized = models.FloatField(db_column='P&LRealized', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    p_lunrealized = models.FloatField(db_column='P&LUnrealized', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    p_l = models.FloatField(db_column='P&L', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    average_buy_price = models.FloatField(db_column='Average_Buy_Price', blank=True, null=True)  # Field name made lowercase.
    average_sell_price = models.FloatField(db_column='Average_Sell_Price', blank=True, null=True)  # Field name made lowercase.
    long_term_qty = models.BigIntegerField(db_column='Long_Term_Qty', blank=True, null=True)  # Field name made lowercase.
    net = models.FloatField(db_column='Net', blank=True, null=True)  # Field name made lowercase.
    aum_eod = models.FloatField(db_column='AUM_EOD', blank=True, null=True)  # Field name made lowercase.
    aum_bod = models.FloatField(db_column='AUM_BOD', blank=True, null=True)  # Field name made lowercase.
    customer_name = models.TextField(blank=True, null=True)
    portfolio_name = models.TextField(blank=True, null=True)
    idhp = models.TextField(db_column='IDHP', blank=True, null=True)  # Field name made lowercase.
    divamount = models.FloatField(db_column='DivAmount', blank=True, null=True)  # Field name made lowercase.
    dividend_pnl = models.FloatField(db_column='Dividend_PNL', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Daily_Positions'


class DailyStockReturns(models.Model):
    date = models.DateTimeField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    company_code = models.FloatField(db_column='Company_Code', blank=True, null=True)  # Field name made lowercase.
    nse_symbol = models.TextField(db_column='NSE_Symbol', blank=True, null=True)  # Field name made lowercase.
    company_name = models.TextField(db_column='Company_Name', blank=True, null=True)  # Field name made lowercase.
    currentprice = models.FloatField(db_column='CurrentPrice', blank=True, null=True)  # Field name made lowercase.
    dailychange = models.FloatField(db_column='DailyChange', blank=True, null=True)  # Field name made lowercase.
    dailypercentchange = models.FloatField(db_column='DailyPercentChange', blank=True, null=True)  # Field name made lowercase.
    weeklypricechange = models.FloatField(db_column='WeeklyPriceChange', blank=True, null=True)  # Field name made lowercase.
    weeklypricechangepercent = models.FloatField(db_column='WeeklyPriceChangePercent', blank=True, null=True)  # Field name made lowercase.
    monthlypricechange = models.FloatField(db_column='MonthlyPriceChange', blank=True, null=True)  # Field name made lowercase.
    monthlypricechangepercent = models.FloatField(db_column='MonthlyPriceChangePercent', blank=True, null=True)  # Field name made lowercase.
    fs_ticker = models.TextField(db_column='FS_Ticker', blank=True, null=True)  # Field name made lowercase.
    gics_sector = models.TextField(db_column='GICS_Sector', blank=True, null=True)  # Field name made lowercase.
    gics_industry = models.TextField(db_column='GICS_Industry', blank=True, null=True)  # Field name made lowercase.
    gics_industry_group = models.TextField(db_column='GICS_Industry_Group', blank=True, null=True)  # Field name made lowercase.
    gics_subindustry_name = models.TextField(db_column='Gics_SubIndustry_Name', blank=True, null=True)  # Field name made lowercase.
    nifty_50 = models.TextField(db_column='Nifty_50', blank=True, null=True)  # Field name made lowercase.
    nifty_100 = models.TextField(db_column='Nifty_100', blank=True, null=True)  # Field name made lowercase.
    nifty_200 = models.TextField(db_column='Nifty_200', blank=True, null=True)  # Field name made lowercase.
    nifty_500 = models.TextField(db_column='Nifty_500', blank=True, null=True)  # Field name made lowercase.
    nifty_midcap_100 = models.TextField(db_column='Nifty_MidCap_100', blank=True, null=True)  # Field name made lowercase.
    nifty_smallcap_100 = models.TextField(db_column='Nifty_SmallCap_100', blank=True, null=True)  # Field name made lowercase.
    equalsize = models.FloatField(db_column='EqualSize', blank=True, null=True)  # Field name made lowercase.
    market_cap = models.FloatField(db_column='Market Cap', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    sales_rm_field = models.FloatField(db_column='Sales(RM)', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it ended with '_'.
    volatility_1y = models.FloatField(db_column='Volatility_1Y', blank=True, null=True)  # Field name made lowercase.
    beta_1y = models.FloatField(db_column='Beta_1Y', blank=True, null=True)  # Field name made lowercase.
    pe = models.FloatField(db_column='PE', blank=True, null=True)  # Field name made lowercase.
    pb = models.FloatField(db_column='PB', blank=True, null=True)  # Field name made lowercase.
    roe = models.FloatField(db_column='ROE', blank=True, null=True)  # Field name made lowercase.
    debt_equity = models.FloatField(db_column='Debt/Equity', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    rsi = models.FloatField(db_column='RSI', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Daily_Stock_Returns'


class Dow30StockReturns(models.Model):
    date = models.DateTimeField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    ticker = models.TextField(db_column='Ticker', blank=True, null=True)  # Field name made lowercase.
    name = models.TextField(db_column='Name', blank=True, null=True)  # Field name made lowercase.
    currentprice = models.FloatField(db_column='CurrentPrice', blank=True, null=True)  # Field name made lowercase.
    dailychange = models.FloatField(db_column='DailyChange', blank=True, null=True)  # Field name made lowercase.
    dailypercentchange = models.FloatField(db_column='DailyPercentChange', blank=True, null=True)  # Field name made lowercase.
    weeklypricechange = models.FloatField(db_column='WeeklyPriceChange', blank=True, null=True)  # Field name made lowercase.
    weeklypricechangepercent = models.FloatField(db_column='WeeklyPriceChangePercent', blank=True, null=True)  # Field name made lowercase.
    monthlypricechange = models.FloatField(db_column='MonthlyPriceChange', blank=True, null=True)  # Field name made lowercase.
    monthlypricechangepercent = models.FloatField(db_column='MonthlyPriceChangePercent', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Dow30_Stock_Returns'


class DummyPortfolio(models.Model):
    company = models.CharField(db_column='Company', max_length=255, blank=True, null=True)  # Field name made lowercase.
    weight = models.FloatField(db_column='Weight', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Dummy_Portfolio'


class DummyPortfolioEf(models.Model):
    company = models.CharField(db_column='Company', max_length=255, blank=True, null=True)  # Field name made lowercase.
    weight = models.FloatField(db_column='Weight', blank=True, null=True)  # Field name made lowercase.
    slno = models.IntegerField(blank=True, null=True)
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Dummy_Portfolio_EF'


class DummyPortfolioMontecarlo(models.Model):
    company = models.CharField(db_column='Company', max_length=255, blank=True, null=True)  # Field name made lowercase.
    weight = models.FloatField(db_column='Weight', blank=True, null=True)  # Field name made lowercase.
    slno = models.IntegerField(blank=True, null=True)
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Dummy_Portfolio_MonteCarlo'


class EodMarket(models.Model):
    date = models.DateField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    open = models.FloatField(db_column='Open', blank=True, null=True)  # Field name made lowercase.
    high = models.FloatField(db_column='High', blank=True, null=True)  # Field name made lowercase.
    low = models.FloatField(db_column='Low', blank=True, null=True)  # Field name made lowercase.
    close = models.FloatField(db_column='Close', blank=True, null=True)  # Field name made lowercase.
    adjusted_close = models.FloatField(db_column='Adjusted_close', blank=True, null=True)  # Field name made lowercase.
    volume = models.FloatField(db_column='Volume', blank=True, null=True)  # Field name made lowercase.
    ticker = models.TextField(db_column='Ticker', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'EOD_Market'


class EtfMarketView(models.Model):
    type = models.TextField(db_column='Type', blank=True, null=True)  # Field name made lowercase.
    company_name = models.TextField(db_column='Company_Name', blank=True, null=True)  # Field name made lowercase.
    security_code = models.TextField(db_column='Security_Code', blank=True, null=True)  # Field name made lowercase.
    short_name = models.TextField(db_column='Short Name', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    short_term = models.TextField(db_column='Short Term', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    intermediate_term = models.TextField(db_column='Intermediate Term', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    order = models.IntegerField(db_column='Order', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ETF_Market_View'


class EtfMonitor(models.Model):
    date = models.DateField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    security_code = models.TextField(db_column='Security_Code', blank=True, null=True)  # Field name made lowercase.
    fs_ticker = models.TextField(db_column='FS_Ticker', blank=True, null=True)  # Field name made lowercase.
    company_name = models.TextField(db_column='Company_Name', blank=True, null=True)  # Field name made lowercase.
    asset_class = models.TextField(db_column='Asset Class', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    region_country = models.TextField(db_column='Region/Country', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    category = models.TextField(db_column='Category', blank=True, null=True)  # Field name made lowercase.
    issuer = models.TextField(db_column='Issuer', blank=True, null=True)  # Field name made lowercase.
    adj_price_close = models.FloatField(db_column='Adj Price Close', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    number_15d = models.FloatField(db_column='15D', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    mtd = models.FloatField(db_column='MTD', blank=True, null=True)  # Field name made lowercase.
    qtd = models.FloatField(db_column='QTD', blank=True, null=True)  # Field name made lowercase.
    ytd = models.FloatField(db_column='YTD', blank=True, null=True)  # Field name made lowercase.
    number_1m = models.FloatField(db_column='1M', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_3m = models.FloatField(db_column='3M', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_6m = models.FloatField(db_column='6M', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_12m = models.FloatField(db_column='12M', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    ema9 = models.FloatField(db_column='EMA9', blank=True, null=True)  # Field name made lowercase.
    ema50 = models.FloatField(db_column='EMA50', blank=True, null=True)  # Field name made lowercase.
    ema200 = models.FloatField(db_column='EMA200', blank=True, null=True)  # Field name made lowercase.
    number_50ema_200ema = models.TextField(db_column='50EMA>200EMA', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_50ema_200ema_0 = models.TextField(db_column='50EMA<200EMA', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier. Field renamed because of name conflict.
    rsi_14d = models.FloatField(db_column='RSI_14D', blank=True, null=True)  # Field name made lowercase.
    adx = models.FloatField(db_column='ADX', blank=True, null=True)  # Field name made lowercase.
    rsi_14d_weekly = models.FloatField(db_column='RSI_14D_WEEKLY', blank=True, null=True)  # Field name made lowercase.
    adx_weekly = models.FloatField(db_column='ADX_WEEKLY', blank=True, null=True)  # Field name made lowercase.
    id = models.BigIntegerField(blank=True, null=True)
    priority = models.CharField(db_column='Priority', max_length=5, blank=True, null=True)  # Field name made lowercase.
    short_term = models.CharField(db_column='Short Term', max_length=5, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    intermediate_term = models.CharField(db_column='Intermediate Term', max_length=5, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    type = models.CharField(db_column='Type', max_length=5, blank=True, null=True)  # Field name made lowercase.
    market_view_flag = models.CharField(db_column='Market_View_Flag', max_length=5, blank=True, null=True)  # Field name made lowercase.
    short_name = models.CharField(db_column='Short Name', max_length=50, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    order = models.IntegerField(db_column='Order', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ETF_Monitor'


class Emailcampaigndetails(models.Model):
    id = models.AutoField()
    first_name = models.CharField(db_column='First Name', max_length=200, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    last_name = models.CharField(db_column='Last Name', max_length=200, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    email = models.CharField(db_column='Email', max_length=254, blank=True, null=True)  # Field name made lowercase.
    contact = models.CharField(db_column='Contact', max_length=15, blank=True, null=True)  # Field name made lowercase.
    dob = models.DateField(db_column='DOB', blank=True, null=True)  # Field name made lowercase.
    unsubscribed = models.CharField(db_column='Unsubscribed', max_length=5)  # Field name made lowercase.
    unsubscribe_reason = models.TextField(db_column='Unsubscribe Reason')  # Field name made lowercase. Field renamed to remove unsuitable characters.
    active = models.CharField(db_column='Active', max_length=5)  # Field name made lowercase.
    remarks = models.TextField(db_column='Remarks')  # Field name made lowercase.
    batch = models.CharField(db_column='Batch', max_length=255)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'EmailCampaignDetails'


class EmployeeCount(models.Model):
    yrc = models.BigIntegerField(db_column='YRC', blank=True, null=True)  # Field name made lowercase.
    totalempoyee_male = models.FloatField(db_column='TotalEmpoyee_Male', blank=True, null=True)  # Field name made lowercase.
    totalempoyee_female = models.FloatField(db_column='TotalEmpoyee_Female', blank=True, null=True)  # Field name made lowercase.
    totalempoyee = models.FloatField(db_column='TotalEmpoyee', blank=True, null=True)  # Field name made lowercase.
    totalemployee_contractbasis = models.FloatField(db_column='TotalEmployee_Contractbasis', blank=True, null=True)  # Field name made lowercase.
    permenantwomenemployee = models.FloatField(db_column='PermenantWomenEmployee', blank=True, null=True)  # Field name made lowercase.
    permenantdisabledemployee = models.FloatField(db_column='PermenantDisabledEmployee', blank=True, null=True)  # Field name made lowercase.
    co_code = models.IntegerField(blank=True, null=True)
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Employee Count'


class ExponentialMovingAverage(models.Model):
    company = models.TextField(db_column='Company', blank=True, null=True)  # Field name made lowercase.
    factset_ticker = models.TextField(db_column='Factset_Ticker', blank=True, null=True)  # Field name made lowercase.
    date = models.DateField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    price = models.FloatField(db_column='Price', blank=True, null=True)  # Field name made lowercase.
    ema9 = models.FloatField(db_column='EMA9', blank=True, null=True)  # Field name made lowercase.
    ema12 = models.FloatField(db_column='EMA12', blank=True, null=True)  # Field name made lowercase.
    ema = models.FloatField(db_column='EMA', blank=True, null=True)  # Field name made lowercase.
    ema26 = models.FloatField(db_column='EMA26', blank=True, null=True)  # Field name made lowercase.
    ema50 = models.FloatField(db_column='EMA50', blank=True, null=True)  # Field name made lowercase.
    ema200 = models.FloatField(db_column='EMA200', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Exponential_Moving_Average'


class FiiDiiDataDaily(models.Model):
    date = models.DateField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    fii_gross_purchase = models.FloatField(db_column='FII Gross Purchase', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    fii_gross_sales = models.FloatField(db_column='FII Gross Sales', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    fii = models.FloatField(db_column='FII', blank=True, null=True)  # Field name made lowercase.
    dii_gross_purchase = models.FloatField(db_column='DII Gross Purchase', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    dii_gross_sales = models.FloatField(db_column='DII Gross Sales', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    dii = models.FloatField(db_column='DII', blank=True, null=True)  # Field name made lowercase.
    nifty_500_return = models.FloatField(db_column='Nifty_500_Return', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'FII_DII_Data_Daily'


class FiiDiiDataMonthly(models.Model):
    year = models.BigIntegerField(db_column='Year', blank=True, null=True)  # Field name made lowercase.
    month = models.BigIntegerField(db_column='Month', blank=True, null=True)  # Field name made lowercase.
    date = models.TextField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    fii_gross_purchase = models.FloatField(db_column='FII Gross Purchase', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    fii_gross_sales = models.FloatField(db_column='FII Gross Sales', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    fii = models.FloatField(db_column='FII', blank=True, null=True)  # Field name made lowercase.
    dii_gross_purchase = models.FloatField(db_column='DII Gross Purchase', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    dii_gross_sales = models.FloatField(db_column='DII Gross Sales', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    dii = models.FloatField(db_column='DII', blank=True, null=True)  # Field name made lowercase.
    nifty_500_monthly_return = models.FloatField(db_column='Nifty_500_Monthly_Return', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'FII_DII_Data_Monthly'


class FactorLimits(models.Model):
    category = models.TextField(db_column='Category', blank=True, null=True)  # Field name made lowercase.
    factor = models.TextField(db_column='Factor', blank=True, null=True)  # Field name made lowercase.
    range1 = models.FloatField(db_column='Range1', blank=True, null=True)  # Field name made lowercase.
    range2 = models.FloatField(db_column='Range2', blank=True, null=True)  # Field name made lowercase.
    range3 = models.FloatField(db_column='Range3', blank=True, null=True)  # Field name made lowercase.
    range4 = models.FloatField(db_column='Range4', blank=True, null=True)  # Field name made lowercase.
    range5 = models.FloatField(db_column='Range5', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Factor_Limits'


class FactorsConstraints(models.Model):
    factor = models.CharField(db_column='Factor', max_length=255, blank=True, null=True)  # Field name made lowercase.
    low = models.FloatField(db_column='Low', blank=True, null=True)  # Field name made lowercase.
    high = models.FloatField(db_column='High', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Factors_Constraints'


class FactorsNew20210109(models.Model):
    gics = models.CharField(db_column='GICS', max_length=255, blank=True, null=True)  # Field name made lowercase.
    factor = models.CharField(db_column='Factor', max_length=255, blank=True, null=True)  # Field name made lowercase.
    date = models.CharField(db_column='Date', max_length=255, blank=True, null=True)  # Field name made lowercase.
    value = models.FloatField(db_column='Value', blank=True, null=True)  # Field name made lowercase.
    date_new = models.DateField(blank=True, null=True)
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Factors_New_20210109'


class FamaData(models.Model):
    date = models.CharField(db_column='Date', max_length=255, blank=True, null=True)  # Field name made lowercase.
    smb = models.FloatField(db_column='SMB', blank=True, null=True)  # Field name made lowercase.
    hml = models.FloatField(db_column='HML', blank=True, null=True)  # Field name made lowercase.
    wml = models.FloatField(db_column='WML', blank=True, null=True)  # Field name made lowercase.
    rm = models.FloatField(db_column='Rm', blank=True, null=True)  # Field name made lowercase.
    rf = models.FloatField(db_column='Rf', blank=True, null=True)  # Field name made lowercase.
    rm_rf = models.FloatField(db_column='Rm-Rf', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Fama_Data'


class FibonacciPivotPoint(models.Model):
    date = models.DateField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    fs_ticker = models.CharField(db_column='FS_Ticker', max_length=255, blank=True, null=True)  # Field name made lowercase.
    company_name = models.CharField(db_column='Company_Name', max_length=255, blank=True, null=True)  # Field name made lowercase.
    open = models.FloatField(db_column='Open', blank=True, null=True)  # Field name made lowercase.
    high = models.FloatField(db_column='High', blank=True, null=True)  # Field name made lowercase.
    low = models.FloatField(db_column='Low', blank=True, null=True)  # Field name made lowercase.
    close = models.FloatField(db_column='Close', blank=True, null=True)  # Field name made lowercase.
    pp = models.FloatField(db_column='PP', blank=True, null=True)  # Field name made lowercase.
    s1 = models.FloatField(db_column='S1', blank=True, null=True)  # Field name made lowercase.
    s2 = models.FloatField(db_column='S2', blank=True, null=True)  # Field name made lowercase.
    s3 = models.FloatField(db_column='S3', blank=True, null=True)  # Field name made lowercase.
    r1 = models.FloatField(db_column='R1', blank=True, null=True)  # Field name made lowercase.
    r2 = models.FloatField(db_column='R2', blank=True, null=True)  # Field name made lowercase.
    r3 = models.FloatField(db_column='R3', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Fibonacci_Pivot_Point'


class FiltersIn(models.Model):
    filter = models.CharField(db_column='Filter', max_length=255, blank=True, null=True)  # Field name made lowercase.
    search_filter = models.CharField(db_column='Search Filter', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    sql_columns = models.CharField(db_column='SQL_Columns', max_length=255, blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Filters_IN'


class FiltersUs(models.Model):
    filter = models.CharField(db_column='Filter', max_length=255, blank=True, null=True)  # Field name made lowercase.
    search_filter = models.CharField(db_column='Search Filter', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    sql_columns = models.CharField(db_column='SQL_Columns', max_length=255, blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Filters_US'


class FnoFlags(models.Model):
    security_id = models.CharField(db_column='Security Id', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    period = models.CharField(db_column='Period', max_length=255, blank=True, null=True)  # Field name made lowercase.
    flag = models.CharField(db_column='Flag', max_length=255, blank=True, null=True)  # Field name made lowercase.
    fs_ticker = models.TextField(db_column='Fs Ticker', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'FnO_Flags'


class GicsClassification(models.Model):
    company = models.CharField(db_column='Company', max_length=255, blank=True, null=True)  # Field name made lowercase.
    tier = models.CharField(db_column='Tier', max_length=11)  # Field name made lowercase.
    fs_ticker = models.TextField(db_column='FS_Ticker', blank=True, null=True)  # Field name made lowercase.
    gics = models.CharField(db_column='GICS', max_length=255, blank=True, null=True)  # Field name made lowercase.
    description = models.TextField(db_column='Description', blank=True, null=True)  # Field name made lowercase. This field type is a guess.
    rating = models.CharField(db_column='Rating', max_length=255, blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'GICS_Classification'


class GainersLosers(models.Model):
    date = models.TextField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    price = models.FloatField(db_column='Price', blank=True, null=True)  # Field name made lowercase.
    return_field = models.FloatField(db_column='Return', blank=True, null=True)  # Field name made lowercase. Field renamed because it was a Python reserved word.
    nsesymbol = models.TextField(blank=True, null=True)
    type = models.TextField(db_column='Type', blank=True, null=True)  # Field name made lowercase.
    companytype = models.TextField(db_column='CompanyType', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Gainers_Losers'


class HistoricalMultiples(models.Model):
    date = models.DateTimeField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    factor = models.TextField(db_column='Factor', blank=True, null=True)  # Field name made lowercase.
    ticker = models.TextField(db_column='Ticker', blank=True, null=True)  # Field name made lowercase.
    value = models.FloatField(blank=True, null=True)
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Historical_Multiples'


class HistoricalMultiplesOld(models.Model):
    date = models.DateField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    factor = models.CharField(db_column='Factor', max_length=50)  # Field name made lowercase.
    number_506590_in = models.FloatField(db_column='506590-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_506655_in = models.FloatField(db_column='506655-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_506943_in = models.FloatField(db_column='506943-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_507685_in = models.FloatField(db_column='507685-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_507880_in = models.FloatField(db_column='507880-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_509243_in = models.FloatField(db_column='509243-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_509930_in = models.FloatField(db_column='509930-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_509966_in = models.FloatField(db_column='509966-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_511218_in = models.FloatField(db_column='511218-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_512070_in = models.FloatField(db_column='512070-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_512179_in = models.FloatField(db_column='512179-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_512237_in = models.FloatField(db_column='512237-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_513023_in = models.FloatField(db_column='513023-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_513377_in = models.FloatField(db_column='513377-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_513434_in = models.FloatField(db_column='513434-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_513683_in = models.FloatField(db_column='513683-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_514162_in = models.FloatField(db_column='514162-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_517334_in = models.FloatField(db_column='517334-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_517385_in = models.FloatField(db_column='517385-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_517506_in = models.FloatField(db_column='517506-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_517569_in = models.FloatField(db_column='517569-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_520051_in = models.FloatField(db_column='520051-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_520111_in = models.FloatField(db_column='520111-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_521064_in = models.FloatField(db_column='521064-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_522113_in = models.FloatField(db_column='522113-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_522287_in = models.FloatField(db_column='522287-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_523261_in = models.FloatField(db_column='523261-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_523385_in = models.FloatField(db_column='523385-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_523398_in = models.FloatField(db_column='523398-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_523405_in = models.FloatField(db_column='523405-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_523457_in = models.FloatField(db_column='523457-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_523598_in = models.FloatField(db_column='523598-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_523642_in = models.FloatField(db_column='523642-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_523756_in = models.FloatField(db_column='523756-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_524000_in = models.FloatField(db_column='524000-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_524200_in = models.FloatField(db_column='524200-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_524230_in = models.FloatField(db_column='524230-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_524715_in = models.FloatField(db_column='524715-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_524816_in = models.FloatField(db_column='524816-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_526299_in = models.FloatField(db_column='526299-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_526371_in = models.FloatField(db_column='526371-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_526947_in = models.FloatField(db_column='526947-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_530007_in = models.FloatField(db_column='530007-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_530019_in = models.FloatField(db_column='530019-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_530239_in = models.FloatField(db_column='530239-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_530517_in = models.FloatField(db_column='530517-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_530549_in = models.FloatField(db_column='530549-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_530813_in = models.FloatField(db_column='530813-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_531213_in = models.FloatField(db_column='531213-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_531335_in = models.FloatField(db_column='531335-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_531426_in = models.FloatField(db_column='531426-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_531500_in = models.FloatField(db_column='531500-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_531548_in = models.FloatField(db_column='531548-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_531642_in = models.FloatField(db_column='531642-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_531910_in = models.FloatField(db_column='531910-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532144_in = models.FloatField(db_column='532144-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532189_in = models.FloatField(db_column='532189-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532209_in = models.FloatField(db_column='532209-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532218_in = models.FloatField(db_column='532218-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532221_in = models.FloatField(db_column='532221-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532234_in = models.FloatField(db_column='532234-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532276_in = models.FloatField(db_column='532276-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532286_in = models.FloatField(db_column='532286-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532300_in = models.FloatField(db_column='532300-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532301_in = models.FloatField(db_column='532301-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532313_in = models.FloatField(db_column='532313-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532343_in = models.FloatField(db_column='532343-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532374_in = models.FloatField(db_column='532374-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532388_in = models.FloatField(db_column='532388-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532461_in = models.FloatField(db_column='532461-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532466_in = models.FloatField(db_column='532466-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532477_in = models.FloatField(db_column='532477-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532478_in = models.FloatField(db_column='532478-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532497_in = models.FloatField(db_column='532497-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532498_in = models.FloatField(db_column='532498-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532500_in = models.FloatField(db_column='532500-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532504_in = models.FloatField(db_column='532504-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532505_in = models.FloatField(db_column='532505-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532508_in = models.FloatField(db_column='532508-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532509_in = models.FloatField(db_column='532509-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532515_in = models.FloatField(db_column='532515-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532522_in = models.FloatField(db_column='532522-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532524_in = models.FloatField(db_column='532524-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532525_in = models.FloatField(db_column='532525-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532531_in = models.FloatField(db_column='532531-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532532_in = models.FloatField(db_column='532532-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532538_in = models.FloatField(db_column='532538-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532539_in = models.FloatField(db_column='532539-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532540_in = models.FloatField(db_column='532540-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532541_in = models.FloatField(db_column='532541-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532555_in = models.FloatField(db_column='532555-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532636_in = models.FloatField(db_column='532636-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532638_in = models.FloatField(db_column='532638-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532642_in = models.FloatField(db_column='532642-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532644_in = models.FloatField(db_column='532644-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532648_in = models.FloatField(db_column='532648-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532652_in = models.FloatField(db_column='532652-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532659_in = models.FloatField(db_column='532659-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532667_in = models.FloatField(db_column='532667-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532689_in = models.FloatField(db_column='532689-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532705_in = models.FloatField(db_column='532705-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532706_in = models.FloatField(db_column='532706-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532710_in = models.FloatField(db_column='532710-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532712_in = models.FloatField(db_column='532712-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532714_in = models.FloatField(db_column='532714-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532720_in = models.FloatField(db_column='532720-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532725_in = models.FloatField(db_column='532725-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532733_in = models.FloatField(db_column='532733-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532755_in = models.FloatField(db_column='532755-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532756_in = models.FloatField(db_column='532756-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532777_in = models.FloatField(db_column='532777-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532779_in = models.FloatField(db_column='532779-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532784_in = models.FloatField(db_column='532784-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532800_in = models.FloatField(db_column='532800-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532805_in = models.FloatField(db_column='532805-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532810_in = models.FloatField(db_column='532810-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532814_in = models.FloatField(db_column='532814-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532819_in = models.FloatField(db_column='532819-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532822_in = models.FloatField(db_column='532822-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532827_in = models.FloatField(db_column='532827-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532832_in = models.FloatField(db_column='532832-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532856_in = models.FloatField(db_column='532856-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532865_in = models.FloatField(db_column='532865-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532872_in = models.FloatField(db_column='532872-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532880_in = models.FloatField(db_column='532880-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532889_in = models.FloatField(db_column='532889-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532890_in = models.FloatField(db_column='532890-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532892_in = models.FloatField(db_column='532892-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532898_in = models.FloatField(db_column='532898-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532926_in = models.FloatField(db_column='532926-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532939_in = models.FloatField(db_column='532939-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532942_in = models.FloatField(db_column='532942-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532947_in = models.FloatField(db_column='532947-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532953_in = models.FloatField(db_column='532953-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532955_in = models.FloatField(db_column='532955-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532960_in = models.FloatField(db_column='532960-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_533023_in = models.FloatField(db_column='533023-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_533088_in = models.FloatField(db_column='533088-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_533098_in = models.FloatField(db_column='533098-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_533106_in = models.FloatField(db_column='533106-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_533148_in = models.FloatField(db_column='533148-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_533155_in = models.FloatField(db_column='533155-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_533171_in = models.FloatField(db_column='533171-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_533179_in = models.FloatField(db_column='533179-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_533206_in = models.FloatField(db_column='533206-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_533269_in = models.FloatField(db_column='533269-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_533273_in = models.FloatField(db_column='533273-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_533274_in = models.FloatField(db_column='533274-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_533286_in = models.FloatField(db_column='533286-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_533398_in = models.FloatField(db_column='533398-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_533519_in = models.FloatField(db_column='533519-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_533520_in = models.FloatField(db_column='533520-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_533655_in = models.FloatField(db_column='533655-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_534091_in = models.FloatField(db_column='534091-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_534139_in = models.FloatField(db_column='534139-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_534309_in = models.FloatField(db_column='534309-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_534690_in = models.FloatField(db_column='534690-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_534809_in = models.FloatField(db_column='534809-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_534816_in = models.FloatField(db_column='534816-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_534976_in = models.FloatField(db_column='534976-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_535322_in = models.FloatField(db_column='535322-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_535648_in = models.FloatField(db_column='535648-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_535754_in = models.FloatField(db_column='535754-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_535789_in = models.FloatField(db_column='535789-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_538835_in = models.FloatField(db_column='538835-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_538962_in = models.FloatField(db_column='538962-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_539083_in = models.FloatField(db_column='539083-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_539150_in = models.FloatField(db_column='539150-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_539268_in = models.FloatField(db_column='539268-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_539437_in = models.FloatField(db_column='539437-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_539448_in = models.FloatField(db_column='539448-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_539450_in = models.FloatField(db_column='539450-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_539524_in = models.FloatField(db_column='539524-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_539542_in = models.FloatField(db_column='539542-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_539551_in = models.FloatField(db_column='539551-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_539597_in = models.FloatField(db_column='539597-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_539658_in = models.FloatField(db_column='539658-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_539807_in = models.FloatField(db_column='539807-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_539871_in = models.FloatField(db_column='539871-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_539874_in = models.FloatField(db_column='539874-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_539889_in = models.FloatField(db_column='539889-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_539957_in = models.FloatField(db_column='539957-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_539978_in = models.FloatField(db_column='539978-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_539981_in = models.FloatField(db_column='539981-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_540005_in = models.FloatField(db_column='540005-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_540064_in = models.FloatField(db_column='540064-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_540065_in = models.FloatField(db_column='540065-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_540115_in = models.FloatField(db_column='540115-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_540133_in = models.FloatField(db_column='540133-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_540173_in = models.FloatField(db_column='540173-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_540180_in = models.FloatField(db_column='540180-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_540203_in = models.FloatField(db_column='540203-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_540222_in = models.FloatField(db_column='540222-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_540425_in = models.FloatField(db_column='540425-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_540530_in = models.FloatField(db_column='540530-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_540575_in = models.FloatField(db_column='540575-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_540595_in = models.FloatField(db_column='540595-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_540673_in = models.FloatField(db_column='540673-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_540716_in = models.FloatField(db_column='540716-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_540719_in = models.FloatField(db_column='540719-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_540743_in = models.FloatField(db_column='540743-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_540749_in = models.FloatField(db_column='540749-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_540750_in = models.FloatField(db_column='540750-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_540755_in = models.FloatField(db_column='540755-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_540762_in = models.FloatField(db_column='540762-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_540767_in = models.FloatField(db_column='540767-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_540768_in = models.FloatField(db_column='540768-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_540769_in = models.FloatField(db_column='540769-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_540777_in = models.FloatField(db_column='540777-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_540935_in = models.FloatField(db_column='540935-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_541154_in = models.FloatField(db_column='541154-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_541179_in = models.FloatField(db_column='541179-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_541233_in = models.FloatField(db_column='541233-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_541301_in = models.FloatField(db_column='541301-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_541336_in = models.FloatField(db_column='541336-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_541556_in = models.FloatField(db_column='541556-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_541578_in = models.FloatField(db_column='541578-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_541700_in = models.FloatField(db_column='541700-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_541729_in = models.FloatField(db_column='541729-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_541956_in = models.FloatField(db_column='541956-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_542649_in = models.FloatField(db_column='542649-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_542650_in = models.FloatField(db_column='542650-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_542652_in = models.FloatField(db_column='542652-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_523395_in = models.FloatField(db_column='523395-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_541988_in = models.FloatField(db_column='541988-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500002_in = models.FloatField(db_column='500002-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500488_in = models.FloatField(db_column='500488-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_540691_in = models.FloatField(db_column='540691-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_535755_in = models.FloatField(db_column='535755-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500410_in = models.FloatField(db_column='500410-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_512599_in = models.FloatField(db_column='512599-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_542066_in = models.FloatField(db_column='542066-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_541450_in = models.FloatField(db_column='541450-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532921_in = models.FloatField(db_column='532921-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_533096_in = models.FloatField(db_column='533096-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_539254_in = models.FloatField(db_column='539254-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_540025_in = models.FloatField(db_column='540025-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500003_in = models.FloatField(db_column='500003-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532683_in = models.FloatField(db_column='532683-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532331_in = models.FloatField(db_column='532331-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500710_in = models.FloatField(db_column='500710-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532480_in = models.FloatField(db_column='532480-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_539523_in = models.FloatField(db_column='539523-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532749_in = models.FloatField(db_column='532749-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500008_in = models.FloatField(db_column='500008-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500425_in = models.FloatField(db_column='500425-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532418_in = models.FloatField(db_column='532418-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532259_in = models.FloatField(db_column='532259-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_533758_in = models.FloatField(db_column='533758-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_533573_in = models.FloatField(db_column='533573-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_508869_in = models.FloatField(db_column='508869-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500877_in = models.FloatField(db_column='500877-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_533271_in = models.FloatField(db_column='533271-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500477_in = models.FloatField(db_column='500477-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500820_in = models.FloatField(db_column='500820-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_540975_in = models.FloatField(db_column='540975-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532830_in = models.FloatField(db_column='532830-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_506820_in = models.FloatField(db_column='506820-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500027_in = models.FloatField(db_column='500027-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_540611_in = models.FloatField(db_column='540611-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_524804_in = models.FloatField(db_column='524804-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_512573_in = models.FloatField(db_column='512573-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532215_in = models.FloatField(db_column='532215-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532977_in = models.FloatField(db_column='532977-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_533229_in = models.FloatField(db_column='533229-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500031_in = models.FloatField(db_column='500031-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532978_in = models.FloatField(db_column='532978-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500490_in = models.FloatField(db_column='500490-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500034_in = models.FloatField(db_column='500034-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_502355_in = models.FloatField(db_column='502355-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_523319_in = models.FloatField(db_column='523319-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500038_in = models.FloatField(db_column='500038-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_541153_in = models.FloatField(db_column='541153-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532134_in = models.FloatField(db_column='532134-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532149_in = models.FloatField(db_column='532149-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500042_in = models.FloatField(db_column='500042-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500043_in = models.FloatField(db_column='500043-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_506285_in = models.FloatField(db_column='506285-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_501425_in = models.FloatField(db_column='501425-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_541143_in = models.FloatField(db_column='541143-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500049_in = models.FloatField(db_column='500049-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500048_in = models.FloatField(db_column='500048-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_509480_in = models.FloatField(db_column='509480-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500493_in = models.FloatField(db_column='500493-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532454_in = models.FloatField(db_column='532454-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500103_in = models.FloatField(db_column='500103-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532523_in = models.FloatField(db_column='532523-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500335_in = models.FloatField(db_column='500335-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_506197_in = models.FloatField(db_column='506197-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_526612_in = models.FloatField(db_column='526612-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500067_in = models.FloatField(db_column='500067-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500020_in = models.FloatField(db_column='500020-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500530_in = models.FloatField(db_column='500530-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500547_in = models.FloatField(db_column='500547-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500825_in = models.FloatField(db_column='500825-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532321_in = models.FloatField(db_column='532321-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532483_in = models.FloatField(db_column='532483-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_511196_in = models.FloatField(db_column='511196-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_524742_in = models.FloatField(db_column='524742-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_513375_in = models.FloatField(db_column='513375-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_534804_in = models.FloatField(db_column='534804-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500870_in = models.FloatField(db_column='500870-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_519600_in = models.FloatField(db_column='519600-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500878_in = models.FloatField(db_column='500878-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532885_in = models.FloatField(db_column='532885-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_501150_in = models.FloatField(db_column='501150-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532548_in = models.FloatField(db_column='532548-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532443_in = models.FloatField(db_column='532443-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500093_in = models.FloatField(db_column='500093-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_542399_in = models.FloatField(db_column='542399-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500085_in = models.FloatField(db_column='500085-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500110_in = models.FloatField(db_column='500110-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_511243_in = models.FloatField(db_column='511243-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_504973_in = models.FloatField(db_column='504973-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500087_in = models.FloatField(db_column='500087-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_533278_in = models.FloatField(db_column='533278-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_540678_in = models.FloatField(db_column='540678-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_539436_in = models.FloatField(db_column='539436-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500830_in = models.FloatField(db_column='500830-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_531344_in = models.FloatField(db_column='531344-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_506395_in = models.FloatField(db_column='506395-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532179_in = models.FloatField(db_column='532179-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_541770_in = models.FloatField(db_column='541770-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500092_in = models.FloatField(db_column='500092-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_539876_in = models.FloatField(db_column='539876-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532210_in = models.FloatField(db_column='532210-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500480_in = models.FloatField(db_column='500480-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532175_in = models.FloatField(db_column='532175-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500096_in = models.FloatField(db_column='500096-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_542216_in = models.FloatField(db_column='542216-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_533151_in = models.FloatField(db_column='533151-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_540047_in = models.FloatField(db_column='540047-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_540701_in = models.FloatField(db_column='540701-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532772_in = models.FloatField(db_column='532772-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_523367_in = models.FloatField(db_column='523367-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500645_in = models.FloatField(db_column='500645-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_506401_in = models.FloatField(db_column='506401-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532848_in = models.FloatField(db_column='532848-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_507717_in = models.FloatField(db_column='507717-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_511072_in = models.FloatField(db_column='511072-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532839_in = models.FloatField(db_column='532839-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532488_in = models.FloatField(db_column='532488-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532868_in = models.FloatField(db_column='532868-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_540376_in = models.FloatField(db_column='540376-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500124_in = models.FloatField(db_column='500124-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532927_in = models.FloatField(db_column='532927-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532922_in = models.FloatField(db_column='532922-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_505200_in = models.FloatField(db_column='505200-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500125_in = models.FloatField(db_column='500125-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500840_in = models.FloatField(db_column='500840-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_531162_in = models.FloatField(db_column='531162-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_540153_in = models.FloatField(db_column='540153-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532178_in = models.FloatField(db_column='532178-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_539844_in = models.FloatField(db_column='539844-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_540596_in = models.FloatField(db_column='540596-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500495_in = models.FloatField(db_column='500495-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500135_in = models.FloatField(db_column='500135-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_531508_in = models.FloatField(db_column='531508-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500086_in = models.FloatField(db_column='500086-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_533400_in = models.FloatField(db_column='533400-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_531599_in = models.FloatField(db_column='531599-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500469_in = models.FloatField(db_column='500469-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500144_in = models.FloatField(db_column='500144-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_541557_in = models.FloatField(db_column='541557-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500940_in = models.FloatField(db_column='500940-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_536507_in = models.FloatField(db_column='536507-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500033_in = models.FloatField(db_column='500033-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532843_in = models.FloatField(db_column='532843-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532809_in = models.FloatField(db_column='532809-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532155_in = models.FloatField(db_column='532155-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532767_in = models.FloatField(db_column='532767-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532622_in = models.FloatField(db_column='532622-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532309_in = models.FloatField(db_column='532309-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500620_in = models.FloatField(db_column='500620-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_522275_in = models.FloatField(db_column='522275-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500171_in = models.FloatField(db_column='500171-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_511676_in = models.FloatField(db_column='511676-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_507815_in = models.FloatField(db_column='507815-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500660_in = models.FloatField(db_column='500660-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532296_in = models.FloatField(db_column='532296-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532181_in = models.FloatField(db_column='532181-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532754_in = models.FloatField(db_column='532754-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500670_in = models.FloatField(db_column='500670-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500163_in = models.FloatField(db_column='500163-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532424_in = models.FloatField(db_column='532424-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500164_in = models.FloatField(db_column='500164-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_533150_in = models.FloatField(db_column='533150-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_533248_in = models.FloatField(db_column='533248-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532482_in = models.FloatField(db_column='532482-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_509488_in = models.FloatField(db_column='509488-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500300_in = models.FloatField(db_column='500300-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_501455_in = models.FloatField(db_column='501455-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_506076_in = models.FloatField(db_column='506076-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500690_in = models.FloatField(db_column='500690-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500676_in = models.FloatField(db_column='500676-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532702_in = models.FloatField(db_column='532702-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_530001_in = models.FloatField(db_column='530001-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_539336_in = models.FloatField(db_column='539336-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_538567_in = models.FloatField(db_column='538567-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_533162_in = models.FloatField(db_column='533162-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_517354_in = models.FloatField(db_column='517354-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532281_in = models.FloatField(db_column='532281-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500010_in = models.FloatField(db_column='500010-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500180_in = models.FloatField(db_column='500180-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_509631_in = models.FloatField(db_column='509631-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500292_in = models.FloatField(db_column='500292-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_519552_in = models.FloatField(db_column='519552-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500182_in = models.FloatField(db_column='500182-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532129_in = models.FloatField(db_column='532129-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500183_in = models.FloatField(db_column='500183-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_514043_in = models.FloatField(db_column='514043-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500440_in = models.FloatField(db_column='500440-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_513599_in = models.FloatField(db_column='513599-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500104_in = models.FloatField(db_column='500104-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500696_in = models.FloatField(db_column='500696-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500188_in = models.FloatField(db_column='500188-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_517174_in = models.FloatField(db_column='517174-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500184_in = models.FloatField(db_column='500184-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532174_in = models.FloatField(db_column='532174-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500116_in = models.FloatField(db_column='500116-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_505726_in = models.FloatField(db_column='505726-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500106_in = models.FloatField(db_column='500106-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532514_in = models.FloatField(db_column='532514-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500850_in = models.FloatField(db_column='500850-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_530005_in = models.FloatField(db_column='530005-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532187_in = models.FloatField(db_column='532187-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500209_in = models.FloatField(db_column='500209-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_530965_in = models.FloatField(db_column='530965-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_524494_in = models.FloatField(db_column='524494-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500875_in = models.FloatField(db_column='500875-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_509496_in = models.FloatField(db_column='509496-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_523610_in = models.FloatField(db_column='523610-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500378_in = models.FloatField(db_column='500378-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500219_in = models.FloatField(db_column='500219-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500380_in = models.FloatField(db_column='500380-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500228_in = models.FloatField(db_column='500228-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500233_in = models.FloatField(db_column='500233-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500165_in = models.FloatField(db_column='500165-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500247_in = models.FloatField(db_column='500247-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500252_in = models.FloatField(db_column='500252-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500253_in = models.FloatField(db_column='500253-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500510_in = models.FloatField(db_column='500510-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500257_in = models.FloatField(db_column='500257-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500520_in = models.FloatField(db_column='500520-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500266_in = models.FloatField(db_column='500266-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500265_in = models.FloatField(db_column='500265-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500271_in = models.FloatField(db_column='500271-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500290_in = models.FloatField(db_column='500290-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500109_in = models.FloatField(db_column='500109-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500294_in = models.FloatField(db_column='500294-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_505355_in = models.FloatField(db_column='505355-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500790_in = models.FloatField(db_column='500790-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500730_in = models.FloatField(db_column='500730-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500312_in = models.FloatField(db_column='500312-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500315_in = models.FloatField(db_column='500315-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500302_in = models.FloatField(db_column='500302-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500680_in = models.FloatField(db_column='500680-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500459_in = models.FloatField(db_column='500459-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500126_in = models.FloatField(db_column='500126-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_503100_in = models.FloatField(db_column='503100-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500331_in = models.FloatField(db_column='500331-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500338_in = models.FloatField(db_column='500338-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500339_in = models.FloatField(db_column='500339-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500355_in = models.FloatField(db_column='500355-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500260_in = models.FloatField(db_column='500260-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500330_in = models.FloatField(db_column='500330-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500111_in = models.FloatField(db_column='500111-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500325_in = models.FloatField(db_column='500325-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500390_in = models.FloatField(db_column='500390-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500113_in = models.FloatField(db_column='500113-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500674_in = models.FloatField(db_column='500674-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500112_in = models.FloatField(db_column='500112-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_505790_in = models.FloatField(db_column='505790-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500387_in = models.FloatField(db_column='500387-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500550_in = models.FloatField(db_column='500550-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500472_in = models.FloatField(db_column='500472-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500285_in = models.FloatField(db_column='500285-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_503806_in = models.FloatField(db_column='503806-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500403_in = models.FloatField(db_column='500403-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_503310_in = models.FloatField(db_column='503310-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500770_in = models.FloatField(db_column='500770-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500408_in = models.FloatField(db_column='500408-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500800_in = models.FloatField(db_column='500800-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_501301_in = models.FloatField(db_column='501301-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500570_in = models.FloatField(db_column='500570-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500400_in = models.FloatField(db_column='500400-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500470_in = models.FloatField(db_column='500470-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500411_in = models.FloatField(db_column='500411-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500413_in = models.FloatField(db_column='500413-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500114_in = models.FloatField(db_column='500114-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500420_in = models.FloatField(db_column='500420-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500251_in = models.FloatField(db_column='500251-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500148_in = models.FloatField(db_column='500148-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500295_in = models.FloatField(db_column='500295-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500575_in = models.FloatField(db_column='500575-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_502986_in = models.FloatField(db_column='502986-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_505533_in = models.FloatField(db_column='505533-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500238_in = models.FloatField(db_column='500238-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_505537_in = models.FloatField(db_column='505537-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_504067_in = models.FloatField(db_column='504067-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Historical_Multiples_Old'


class Indexconstituents(models.Model):
    index_code = models.FloatField(blank=True, null=True)
    index_name = models.TextField(blank=True, null=True)
    co_code = models.FloatField(blank=True, null=True)
    lname = models.TextField(blank=True, null=True)
    flag = models.TextField(db_column='Flag', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'IndexConstituents'


class IndexconstituentsBse(models.Model):
    index_code = models.FloatField(blank=True, null=True)
    index_name = models.TextField(blank=True, null=True)
    co_code = models.FloatField(blank=True, null=True)
    lname = models.TextField(blank=True, null=True)
    flag = models.TextField(db_column='Flag', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'IndexConstituents_BSE'


class IndexPerformance(models.Model):
    ticker = models.TextField(db_column='Ticker', blank=True, null=True)  # Field name made lowercase.
    period = models.TextField(db_column='Period', blank=True, null=True)  # Field name made lowercase.
    return_field = models.FloatField(db_column='Return', blank=True, null=True)  # Field name made lowercase. Field renamed because it was a Python reserved word.
    downloaddate = models.DateField(db_column='downloadDate', blank=True, null=True)  # Field name made lowercase.
    id = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'Index_Performance'


class IndexSectorBreakdown(models.Model):
    sector = models.TextField(db_column='Sector', blank=True, null=True)  # Field name made lowercase.
    breakdown = models.FloatField(db_column='Breakdown', blank=True, null=True)  # Field name made lowercase.
    date = models.DateField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Index_Sector_Breakdown'


class India10YrBy(models.Model):
    date = models.DateTimeField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    price = models.FloatField(db_column='Price', blank=True, null=True)  # Field name made lowercase.
    change_field = models.FloatField(db_column='Change %', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it ended with '_'.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'India_10YR_BY'


class IndiaVix(models.Model):
    date = models.DateTimeField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    vix = models.FloatField(db_column='VIX', blank=True, null=True)  # Field name made lowercase.
    average = models.FloatField(db_column='Average', blank=True, null=True)  # Field name made lowercase.
    std = models.FloatField(db_column='StD', blank=True, null=True)  # Field name made lowercase.
    number_1sd = models.FloatField(db_column='1sd', blank=True, null=True)  # Field renamed because it wasn't a valid Python identifier.
    number_2sd = models.FloatField(db_column='2sd', blank=True, null=True)  # Field renamed because it wasn't a valid Python identifier.
    number_3sd = models.FloatField(db_column='3sd', blank=True, null=True)  # Field renamed because it wasn't a valid Python identifier.
    field_1sd = models.FloatField(db_column='-1sd', blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it started with '_'.
    field_2sd = models.FloatField(db_column='-2sd', blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it started with '_'.
    field_3sd = models.FloatField(db_column='-3sd', blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it started with '_'.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'India_VIX'


class IndiaVixData(models.Model):
    date = models.DateField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    open = models.FloatField(db_column='Open', blank=True, null=True)  # Field name made lowercase.
    high = models.FloatField(db_column='High', blank=True, null=True)  # Field name made lowercase.
    low = models.FloatField(db_column='Low', blank=True, null=True)  # Field name made lowercase.
    close = models.FloatField(db_column='Close', blank=True, null=True)  # Field name made lowercase.
    previous = models.FloatField(db_column='Previous', blank=True, null=True)  # Field name made lowercase.
    change = models.FloatField(db_column='Change', blank=True, null=True)  # Field name made lowercase.
    field_change = models.FloatField(db_column='%Change', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it started with '_'.
    ticker = models.TextField(db_column='Ticker', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'India_VIX_Data'


class Logos(models.Model):
    code = models.TextField(db_column='Code', blank=True, null=True)  # Field name made lowercase.
    fs_ticker = models.TextField(db_column='FS_Ticker', blank=True, null=True)  # Field name made lowercase.
    company_type = models.CharField(db_column='Company_Type', max_length=255, blank=True, null=True)  # Field name made lowercase.
    name = models.TextField(db_column='Name', blank=True, null=True)  # Field name made lowercase.
    imgurl = models.TextField(db_column='IMGURL', blank=True, null=True)  # Field name made lowercase.
    country = models.CharField(db_column='Country', max_length=25, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Logos'


class MacroData(models.Model):
    date = models.DateTimeField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    oil_field = models.FloatField(db_column='Oil ', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it ended with '_'.
    fx_rate = models.FloatField(db_column='FX_Rate', blank=True, null=True)  # Field name made lowercase.
    interest_rate = models.FloatField(db_column='Interest_Rate', blank=True, null=True)  # Field name made lowercase.
    vix = models.FloatField(db_column='VIX', blank=True, null=True)  # Field name made lowercase.
    inflation_rate = models.FloatField(db_column='Inflation_Rate', blank=True, null=True)  # Field name made lowercase.
    gold = models.FloatField(db_column='Gold', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Macro_Data'


class MarketByte(models.Model):
    ticker = models.TextField(db_column='Ticker', blank=True, null=True)  # Field name made lowercase.
    currentprice = models.FloatField(db_column='CurrentPrice', blank=True, null=True)  # Field name made lowercase.
    dailychange = models.FloatField(db_column='dailyChange', blank=True, null=True)  # Field name made lowercase.
    dailypercentchange = models.FloatField(db_column='dailyPercentChange', blank=True, null=True)  # Field name made lowercase.
    weekly_price_change = models.FloatField(blank=True, null=True)
    weekly_price_change_percent = models.FloatField(blank=True, null=True)
    monthly_price_change = models.FloatField(blank=True, null=True)
    monthly_price_change_percent = models.FloatField(blank=True, null=True)
    yearly_price_change = models.FloatField(blank=True, null=True)
    yearly_price_change_percent = models.FloatField(blank=True, null=True)
    date = models.DateTimeField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    country = models.TextField(db_column='Country', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Market_Byte'


class MarketIndex(models.Model):
    company = models.CharField(db_column='Company', max_length=255, blank=True, null=True)  # Field name made lowercase.
    factset_ticker = models.CharField(db_column='Factset_Ticker', max_length=255, blank=True, null=True)  # Field name made lowercase.
    date = models.DateField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    price = models.FloatField(db_column='Price', blank=True, null=True)  # Field name made lowercase.
    return_field = models.FloatField(db_column='Return', blank=True, null=True)  # Field name made lowercase. Field renamed because it was a Python reserved word.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Market_Index'


class MarketIndia(models.Model):
    returns = models.TextField(db_column='Returns', blank=True, null=True)  # Field name made lowercase.
    bse_sensex = models.TextField(db_column='BSE Sensex', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    nifty_50 = models.TextField(db_column='Nifty 50', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    s_p_500 = models.TextField(db_column='S&P 500', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    russell_2000 = models.TextField(db_column='Russell 2000', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    oil = models.TextField(db_column='Oil', blank=True, null=True)  # Field name made lowercase.
    gold = models.TextField(db_column='Gold', blank=True, null=True)  # Field name made lowercase.
    usd_inr = models.TextField(db_column='USD/INR', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    max_date = models.DateTimeField(db_column='Max_Date', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Market_India'


class MarketUs(models.Model):
    returns = models.TextField(db_column='Returns', blank=True, null=True)  # Field name made lowercase.
    s_p_500 = models.TextField(db_column='S&P 500', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    russell_2000 = models.TextField(db_column='Russell 2000', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    dow_jones_30 = models.TextField(db_column='Dow Jones 30', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    bse_sensex = models.TextField(db_column='BSE Sensex', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    nifty_50 = models.TextField(db_column='Nifty 50', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    oil = models.TextField(db_column='Oil', blank=True, null=True)  # Field name made lowercase.
    gold = models.TextField(db_column='Gold', blank=True, null=True)  # Field name made lowercase.
    usd_inr = models.TextField(db_column='USD/INR', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    max_date = models.DateTimeField(db_column='Max_Date', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Market_US'


class ModelPortfolioDetails(models.Model):
    company_name = models.CharField(db_column='Company Name', max_length=255)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    quantity = models.IntegerField(db_column='Quantity', blank=True, null=True)  # Field name made lowercase.
    model_portfolio_name = models.CharField(db_column='Model Portfolio Name', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    country = models.CharField(db_column='Country', max_length=25, blank=True, null=True)  # Field name made lowercase.
    sector_name = models.CharField(db_column='Sector Name', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    add_on = models.CharField(db_column='Add On', max_length=5, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    created_at = models.DateTimeField(blank=True, null=True)
    fs_ticker = models.TextField(db_column='FS_Ticker', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Model_Portfolio_Details'


class ModelPortfolioDetails20210308(models.Model):
    company_name = models.CharField(db_column='Company Name', max_length=255)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    quantity = models.IntegerField(db_column='Quantity', blank=True, null=True)  # Field name made lowercase.
    model_portfolio_name = models.CharField(db_column='Model Portfolio Name', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    country = models.CharField(db_column='Country', max_length=25, blank=True, null=True)  # Field name made lowercase.
    sector_name = models.CharField(db_column='Sector Name', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    add_on = models.CharField(db_column='Add On', max_length=5, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    created_at = models.DateTimeField(blank=True, null=True)
    fs_ticker = models.TextField(db_column='FS_Ticker', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Model_Portfolio_Details_20210308'


class ModelPortfoliosCumulative(models.Model):
    portfolio_name = models.TextField(db_column='Portfolio Name', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    country = models.TextField(db_column='Country', blank=True, null=True)  # Field name made lowercase.
    dtd = models.FloatField(db_column='DTD', blank=True, null=True)  # Field name made lowercase.
    mtd = models.FloatField(db_column='MTD', blank=True, null=True)  # Field name made lowercase.
    ytd = models.FloatField(db_column='YTD', blank=True, null=True)  # Field name made lowercase.
    number_1m = models.FloatField(db_column='1M', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_3m = models.FloatField(db_column='3M', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_1y = models.FloatField(db_column='1Y', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_3y = models.FloatField(db_column='3Y', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    itd = models.FloatField(db_column='ITD', blank=True, null=True)  # Field name made lowercase.
    itd_date = models.TextField(db_column='ITD Date', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    cagr = models.FloatField(db_column='CAGR', blank=True, null=True)  # Field name made lowercase.
    description = models.TextField(db_column='Description', blank=True, null=True)  # Field name made lowercase. This field type is a guess.

    class Meta:
        managed = False
        db_table = 'Model_Portfolios_Cumulative'


class MoneyControlMapping(models.Model):
    money_control_name = models.CharField(db_column='Money_Control_Name', max_length=255, blank=True, null=True)  # Field name made lowercase.
    security_name = models.CharField(db_column='Security Name', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    fs_name = models.CharField(db_column='FS Name', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Money_Control_Mapping'


class MonthEndPrice(models.Model):
    factset_ticker = models.TextField(db_column='Factset_Ticker', blank=True, null=True)  # Field name made lowercase.
    med = models.DateField(db_column='MED', blank=True, null=True)  # Field name made lowercase.
    price = models.FloatField(db_column='Price', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Month_end_Price'


class MovingAverage(models.Model):
    date = models.DateField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    company = models.TextField(db_column='Company')  # Field name made lowercase.
    price = models.FloatField(db_column='Price', blank=True, null=True)  # Field name made lowercase.
    factset_ticker = models.TextField(db_column='Factset_Ticker', blank=True, null=True)  # Field name made lowercase.
    ma9 = models.FloatField(db_column='MA9', blank=True, null=True)  # Field name made lowercase.
    ma20 = models.FloatField(db_column='MA20', blank=True, null=True)  # Field name made lowercase.
    ma26 = models.FloatField(db_column='MA26', blank=True, null=True)  # Field name made lowercase.
    ma50 = models.FloatField(db_column='MA50', blank=True, null=True)  # Field name made lowercase.
    ma100 = models.FloatField(db_column='MA100', blank=True, null=True)  # Field name made lowercase.
    ma200 = models.FloatField(db_column='MA200', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Moving_Average'


class MovingAverageConvergenceDivergence(models.Model):
    date = models.DateTimeField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    company_name = models.TextField(db_column='Company_Name', blank=True, null=True)  # Field name made lowercase.
    company_code = models.FloatField(db_column='Company_Code', blank=True, null=True)  # Field name made lowercase.
    ema12 = models.FloatField(db_column='EMA12', blank=True, null=True)  # Field name made lowercase.
    ema26 = models.FloatField(db_column='EMA26', blank=True, null=True)  # Field name made lowercase.
    macd_line = models.FloatField(db_column='MACD Line', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    signal_line = models.FloatField(db_column='Signal Line', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    macd_histogram = models.FloatField(db_column='MACD Histogram', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    fs_ticker = models.CharField(db_column='FS_Ticker', max_length=255, blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Moving_Average_Convergence_Divergence'


class Nifty500200Ma(models.Model):
    date = models.DateTimeField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    nifty500_stocks_above_200ma = models.FloatField(db_column='NIFTY500 stocks above 200MA', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    average = models.FloatField(db_column='Average', blank=True, null=True)  # Field name made lowercase.
    std = models.FloatField(db_column='StD', blank=True, null=True)  # Field name made lowercase.
    number_1sd = models.FloatField(db_column='1sd', blank=True, null=True)  # Field renamed because it wasn't a valid Python identifier.
    number_2sd = models.FloatField(db_column='2sd', blank=True, null=True)  # Field renamed because it wasn't a valid Python identifier.
    number_3sd = models.FloatField(db_column='3sd', blank=True, null=True)  # Field renamed because it wasn't a valid Python identifier.
    field_1sd = models.FloatField(db_column='-1sd', blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it started with '_'.
    field_2sd = models.FloatField(db_column='-2sd', blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it started with '_'.
    field_3sd = models.FloatField(db_column='-3sd', blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it started with '_'.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'NIFTY500_200MA'


class NlpTags2(models.Model):
    fs_ticker = models.CharField(db_column='FS Ticker', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    bbg_ticker = models.CharField(db_column='BBg Ticker', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    fs_name = models.CharField(db_column='FS Name', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    gics = models.CharField(db_column='GICS', max_length=255, blank=True, null=True)  # Field name made lowercase.
    security_id = models.CharField(db_column='Security Id', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    tag = models.TextField(db_column='Tag', blank=True, null=True)  # Field name made lowercase. This field type is a guess.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'NLP_Tags2'


class NewsArticlesWithDates(models.Model):
    company = models.TextField(db_column='Company')  # Field name made lowercase.
    news_link = models.TextField(db_column='News_link')  # Field name made lowercase.
    news_headline = models.TextField(db_column='News_Headline')  # Field name made lowercase.
    article_content = models.TextField(db_column='Article_content')  # Field name made lowercase.
    website = models.TextField(db_column='Website')  # Field name made lowercase.
    avg_positive_polarity = models.FloatField(db_column='Avg_positive_polarity')  # Field name made lowercase.
    positive_count = models.IntegerField(db_column='Positive_count')  # Field name made lowercase.
    avg_negative_polarity = models.FloatField(db_column='Avg_negative_polarity')  # Field name made lowercase.
    negative_count = models.IntegerField(db_column='Negative_count')  # Field name made lowercase.
    date = models.DateField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'News_Articles_with_dates'


class Nifty500Ma200(models.Model):
    date = models.DateField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    nifty500_stocks_above_200ma = models.FloatField(db_column='NIFTY500 stocks above 200MA', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    average = models.FloatField(db_column='Average', blank=True, null=True)  # Field name made lowercase.
    std = models.FloatField(db_column='StD', blank=True, null=True)  # Field name made lowercase.
    number_1sd = models.FloatField(db_column='1sd', blank=True, null=True)  # Field renamed because it wasn't a valid Python identifier.
    number_2sd = models.FloatField(db_column='2sd', blank=True, null=True)  # Field renamed because it wasn't a valid Python identifier.
    number_3sd = models.FloatField(db_column='3sd', blank=True, null=True)  # Field renamed because it wasn't a valid Python identifier.
    field_1sd = models.FloatField(db_column='-1sd', blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it started with '_'.
    field_2sd = models.FloatField(db_column='-2sd', blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it started with '_'.
    field_3sd = models.FloatField(db_column='-3sd', blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it started with '_'.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Nifty500_MA200'


class Nifty50PePb(models.Model):
    date = models.DateField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    p_e = models.FloatField(db_column='P/E', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    p_b = models.FloatField(db_column='P/B', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    div_yield = models.FloatField(db_column='Div Yield', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ticker = models.TextField(db_column='Ticker', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Nifty50_PE_PB'


class Nifty50StockReturns(models.Model):
    companyname = models.TextField(db_column='CompanyName', blank=True, null=True)  # Field name made lowercase.
    currentprice = models.FloatField(db_column='CurrentPrice', blank=True, null=True)  # Field name made lowercase.
    dailychange = models.FloatField(db_column='DailyChange', blank=True, null=True)  # Field name made lowercase.
    dailypercentchange = models.FloatField(db_column='DailyPercentChange', blank=True, null=True)  # Field name made lowercase.
    weeklypricechange = models.FloatField(db_column='WeeklyPriceChange', blank=True, null=True)  # Field name made lowercase.
    weeklypricechangepercent = models.FloatField(db_column='WeeklyPriceChangePercent', blank=True, null=True)  # Field name made lowercase.
    monthlypricechange = models.FloatField(db_column='MonthlyPriceChange', blank=True, null=True)  # Field name made lowercase.
    monthlypricechangepercent = models.FloatField(db_column='MonthlyPriceChangePercent', blank=True, null=True)  # Field name made lowercase.
    date = models.DateTimeField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    co_code = models.FloatField(db_column='Co_Code', blank=True, null=True)  # Field name made lowercase.
    nsesymbol = models.TextField(db_column='NSESymbol', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Nifty50_Stock_Returns'


class Nifty500Above200MaInput(models.Model):
    date = models.DateField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    pct_of_nifty500_above_200ma = models.FloatField(db_column='pct_of_NIFTY500_Above_200MA', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Nifty_500_Above_200MA_Input'


class NiftyIndexesCorelation(models.Model):
    date = models.DateField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    ticker = models.TextField(db_column='Ticker', blank=True, null=True)  # Field name made lowercase.
    nifty_50 = models.FloatField(db_column='Nifty 50', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    auto = models.FloatField(db_column='Auto', blank=True, null=True)  # Field name made lowercase.
    bank = models.FloatField(db_column='Bank', blank=True, null=True)  # Field name made lowercase.
    energy = models.FloatField(db_column='Energy', blank=True, null=True)  # Field name made lowercase.
    fin_serv = models.FloatField(db_column='Fin Serv', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    fmcg = models.FloatField(db_column='FMCG', blank=True, null=True)  # Field name made lowercase.
    infra = models.FloatField(db_column='Infra', blank=True, null=True)  # Field name made lowercase.
    it = models.FloatField(db_column='IT', blank=True, null=True)  # Field name made lowercase.
    media = models.FloatField(db_column='Media', blank=True, null=True)  # Field name made lowercase.
    metal = models.FloatField(db_column='Metal', blank=True, null=True)  # Field name made lowercase.
    midcap_100 = models.FloatField(db_column='MidCap 100', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    pharma = models.FloatField(db_column='Pharma', blank=True, null=True)  # Field name made lowercase.
    pvt_bank = models.FloatField(db_column='PVT Bank', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    psu_bank = models.FloatField(db_column='PSU Bank', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    realty = models.FloatField(db_column='Realty', blank=True, null=True)  # Field name made lowercase.
    smallcap_100 = models.FloatField(db_column='SmallCap 100', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Nifty_Indexes_Corelation'


class NiftySectorPrices(models.Model):
    date = models.DateField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    ticker = models.CharField(db_column='Ticker', max_length=255, blank=True, null=True)  # Field name made lowercase.
    price = models.FloatField(db_column='Price', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Nifty_Sector_Prices'


class NiftySectorReturns(models.Model):
    date = models.DateTimeField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    ticker = models.CharField(db_column='Ticker', max_length=255, blank=True, null=True)  # Field name made lowercase.
    return_field = models.FloatField(db_column='Return', blank=True, null=True)  # Field name made lowercase. Field renamed because it was a Python reserved word.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Nifty_Sector_Returns'


class OptSavedPortfolioEff(models.Model):
    user = models.TextField(db_column='User', blank=True, null=True)  # Field name made lowercase.
    date_time = models.DateTimeField(db_column='Date Time', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    portfolio_name = models.TextField(db_column='Portfolio Name', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    company = models.TextField(db_column='Company', blank=True, null=True)  # Field name made lowercase.
    model = models.FloatField(db_column='Model', blank=True, null=True)  # Field name made lowercase.
    actual = models.FloatField(db_column='Actual', blank=True, null=True)  # Field name made lowercase.
    risk = models.FloatField(db_column='Risk', blank=True, null=True)  # Field name made lowercase.
    return_field = models.FloatField(db_column='Return', blank=True, null=True)  # Field name made lowercase. Field renamed because it was a Python reserved word.
    base_portfolio = models.TextField(db_column='Base_Portfolio', blank=True, null=True)  # Field name made lowercase.
    actual_risk = models.TextField(db_column='Actual_Risk', blank=True, null=True)  # Field name made lowercase.
    actual_return = models.TextField(db_column='Actual_Return', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Opt_Saved_Portfolio_Eff'


class OptSavedPortfolioMont(models.Model):
    user = models.TextField(db_column='User', blank=True, null=True)  # Field name made lowercase.
    date_time = models.DateTimeField(db_column='Date Time', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    portfolio_name = models.TextField(db_column='Portfolio Name', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    company = models.TextField(db_column='Company', blank=True, null=True)  # Field name made lowercase.
    model = models.FloatField(db_column='Model', blank=True, null=True)  # Field name made lowercase.
    actual = models.FloatField(db_column='Actual', blank=True, null=True)  # Field name made lowercase.
    risk = models.FloatField(db_column='Risk', blank=True, null=True)  # Field name made lowercase.
    return_field = models.FloatField(db_column='Return', blank=True, null=True)  # Field name made lowercase. Field renamed because it was a Python reserved word.
    base_portfolio = models.TextField(db_column='Base_Portfolio', blank=True, null=True)  # Field name made lowercase.
    actual_risk = models.FloatField(db_column='Actual_Risk', blank=True, null=True)  # Field name made lowercase.
    actual_return = models.FloatField(db_column='Actual_Return', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Opt_Saved_Portfolio_Mont'


class OptSavedPortfolioVol(models.Model):
    user = models.TextField(db_column='User', blank=True, null=True)  # Field name made lowercase.
    date_time = models.DateTimeField(db_column='Date Time', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    portfolio_name = models.TextField(db_column='Portfolio Name', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    company = models.TextField(db_column='Company', blank=True, null=True)  # Field name made lowercase.
    model = models.FloatField(db_column='Model', blank=True, null=True)  # Field name made lowercase.
    actual = models.FloatField(db_column='Actual', blank=True, null=True)  # Field name made lowercase.
    risk = models.FloatField(db_column='Risk', blank=True, null=True)  # Field name made lowercase.
    return_field = models.FloatField(db_column='Return', blank=True, null=True)  # Field name made lowercase. Field renamed because it was a Python reserved word.
    base_portfolio = models.TextField(db_column='Base_Portfolio', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Opt_Saved_Portfolio_Vol'


class Performance(models.Model):
    company = models.TextField(db_column='Company', blank=True, null=True)  # Field name made lowercase.
    number_2019todate = models.FloatField(db_column='2019toDate', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    ytd = models.FloatField(db_column='YTD', blank=True, null=True)  # Field name made lowercase.
    mtd = models.FloatField(db_column='MTD', blank=True, null=True)  # Field name made lowercase.
    number_1y = models.FloatField(db_column='1Y', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    factset_ticker = models.CharField(db_column='Factset_Ticker', max_length=256, blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Performance'


class PortfolioCreationFilters(models.Model):
    company = models.TextField(db_column='Company', blank=True, null=True)  # Field name made lowercase.
    date = models.DateField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    factset_ticker = models.TextField(db_column='Factset_Ticker', blank=True, null=True)  # Field name made lowercase.
    rsi_30 = models.FloatField(db_column='RSI_30', blank=True, null=True)  # Field name made lowercase.
    rsi_90 = models.FloatField(db_column='RSI_90', blank=True, null=True)  # Field name made lowercase.
    rsi_180 = models.FloatField(db_column='RSI_180', blank=True, null=True)  # Field name made lowercase.
    rsi_365 = models.FloatField(db_column='RSI_365', blank=True, null=True)  # Field name made lowercase.
    volume_change = models.FloatField(db_column='Volume_Change', blank=True, null=True)  # Field name made lowercase.
    current_price_vs_52_week_high = models.FloatField(db_column='Current_Price_vs_52_Week_High', blank=True, null=True)  # Field name made lowercase.
    price_vs_ma50 = models.FloatField(db_column='price_vs_MA50', blank=True, null=True)  # Field name made lowercase.
    price_vs_ma200 = models.FloatField(db_column='price_vs_MA200', blank=True, null=True)  # Field name made lowercase.
    adtv_30 = models.FloatField(db_column='ADTV_30', blank=True, null=True)  # Field name made lowercase.
    share_turnover = models.FloatField(db_column='Share Turnover', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Portfolio_Creation_Filters'


class PortfolioV5(models.Model):
    date = models.DateTimeField(db_column='DATE', blank=True, null=True)  # Field name made lowercase.
    factset_ticker = models.CharField(db_column='Factset_Ticker', max_length=255, blank=True, null=True)  # Field name made lowercase.
    company = models.TextField(db_column='COMPANY', blank=True, null=True)  # Field name made lowercase.
    sector = models.CharField(db_column='SECTOR', max_length=255, blank=True, null=True)  # Field name made lowercase.
    curr_price = models.FloatField(db_column='CURR_PRICE', blank=True, null=True)  # Field name made lowercase.
    prev_day_price = models.FloatField(db_column='PREV_DAY_PRICE', blank=True, null=True)  # Field name made lowercase.
    avg_buy_price = models.FloatField(db_column='AVG_BUY_PRICE', blank=True, null=True)  # Field name made lowercase.
    avg_sell_price = models.FloatField(db_column='AVG_SELL_PRICE', blank=True, null=True)  # Field name made lowercase.
    curr_qty = models.FloatField(db_column='CURR_QTY', blank=True, null=True)  # Field name made lowercase.
    total_bought_qty = models.BigIntegerField(db_column='TOTAL_BOUGHT_QTY', blank=True, null=True)  # Field name made lowercase.
    total_sold_qty = models.FloatField(db_column='TOTAL_SOLD_QTY', blank=True, null=True)  # Field name made lowercase.
    long_term_qty = models.IntegerField(db_column='LONG_TERM_QTY', blank=True, null=True)  # Field name made lowercase.
    pnl_realized = models.FloatField(db_column='PNL_REALIZED', blank=True, null=True)  # Field name made lowercase.
    pnl_unrealized = models.FloatField(db_column='PNL_UNREALIZED', blank=True, null=True)  # Field name made lowercase.
    pnl_unrealized_prev_day = models.IntegerField(db_column='PNL_UNREALIZED_PREV_DAY', blank=True, null=True)  # Field name made lowercase.
    value_mkt_price = models.FloatField(db_column='VALUE_MKT_PRICE', blank=True, null=True)  # Field name made lowercase.
    value_cost = models.FloatField(db_column='VALUE_COST', blank=True, null=True)  # Field name made lowercase.
    pct_chg_day = models.FloatField(db_column='PCT_CHG_DAY', blank=True, null=True)  # Field name made lowercase.
    pct_chg = models.FloatField(db_column='PCT_CHG', blank=True, null=True)  # Field name made lowercase.
    profit_loss = models.CharField(db_column='PROFIT_LOSS', max_length=6)  # Field name made lowercase.
    price_chg_day = models.FloatField(db_column='PRICE_CHG_DAY', blank=True, null=True)  # Field name made lowercase.
    customer_name = models.TextField(db_column='CUSTOMER_NAME', blank=True, null=True)  # Field name made lowercase.
    portfolio_name = models.TextField(db_column='PORTFOLIO_NAME', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Portfolio_V5'


class PositionsInput(models.Model):
    security_id = models.CharField(db_column='SECURITY ID', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    tradedate = models.DateTimeField(db_column='TRADEDATE', blank=True, null=True)  # Field name made lowercase.
    action = models.CharField(db_column='ACTION', max_length=255, blank=True, null=True)  # Field name made lowercase.
    status = models.CharField(db_column='STATUS', max_length=255, blank=True, null=True)  # Field name made lowercase.
    allocation_id = models.CharField(db_column='ALLOCATION ID', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    secdescription = models.CharField(db_column='SECDESCRIPTION', max_length=255, blank=True, null=True)  # Field name made lowercase.
    sedol = models.FloatField(db_column='SEDOL', blank=True, null=True)  # Field name made lowercase.
    isin = models.CharField(db_column='ISIN', max_length=255, blank=True, null=True)  # Field name made lowercase.
    cusip = models.CharField(db_column='CUSIP', max_length=255, blank=True, null=True)  # Field name made lowercase.
    trader = models.CharField(db_column='TRADER', max_length=255, blank=True, null=True)  # Field name made lowercase.
    portfolio = models.CharField(db_column='PORTFOLIO', max_length=255, blank=True, null=True)  # Field name made lowercase.
    customer = models.CharField(db_column='Customer', max_length=255, blank=True, null=True)  # Field name made lowercase.
    quantity = models.FloatField(db_column='QUANTITY', blank=True, null=True)  # Field name made lowercase.
    price = models.FloatField(db_column='PRICE', blank=True, null=True)  # Field name made lowercase.
    fx_rate = models.FloatField(db_column='FX RATE', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    grossmoney = models.FloatField(db_column='GROSSMONEY', blank=True, null=True)  # Field name made lowercase.
    totalcomm = models.FloatField(db_column='TOTALCOMM', blank=True, null=True)  # Field name made lowercase.
    totalfees = models.FloatField(db_column='TOTALFEES', blank=True, null=True)  # Field name made lowercase.
    netmoney = models.FloatField(db_column='NETMONEY', blank=True, null=True)  # Field name made lowercase.
    execcurrency = models.CharField(db_column='EXECCURRENCY', max_length=255, blank=True, null=True)  # Field name made lowercase.
    broker = models.CharField(db_column='BROKER', max_length=255, blank=True, null=True)  # Field name made lowercase.
    custodian = models.CharField(db_column='CUSTODIAN', max_length=255, blank=True, null=True)  # Field name made lowercase.
    accrued_int = models.CharField(db_column='ACCRUED INT', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    strategy = models.CharField(db_column='STRATEGY', max_length=255, blank=True, null=True)  # Field name made lowercase.
    bid_qty = models.CharField(db_column='BID QTY', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    bid_price = models.CharField(db_column='BID PRICE', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    pricing_date = models.DateTimeField(db_column='PRICING DATE', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    first_trade_date = models.DateTimeField(db_column='FIRST TRADE DATE', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    deal_captian = models.CharField(db_column='DEAL CAPTIAN', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    hedge = models.CharField(db_column='HEDGE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    intial_target_price = models.CharField(db_column='INTIAL TARGET PRICE', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    target_price = models.CharField(db_column='TARGET PRICE', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ultimate_stop = models.CharField(db_column='ULTIMATE STOP', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    country = models.CharField(db_column='COUNTRY', max_length=255, blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Positions_Input'


class PriceToEarnings(models.Model):
    date = models.DateTimeField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    p_e = models.FloatField(db_column='P/E', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    average = models.FloatField(db_column='Average', blank=True, null=True)  # Field name made lowercase.
    std = models.FloatField(db_column='StD', blank=True, null=True)  # Field name made lowercase.
    number_1sd = models.FloatField(db_column='1sd', blank=True, null=True)  # Field renamed because it wasn't a valid Python identifier.
    number_2sd = models.FloatField(db_column='2sd', blank=True, null=True)  # Field renamed because it wasn't a valid Python identifier.
    number_3sd = models.FloatField(db_column='3sd', blank=True, null=True)  # Field renamed because it wasn't a valid Python identifier.
    field_1sd = models.FloatField(db_column='-1sd', blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it started with '_'.
    field_2sd = models.FloatField(db_column='-2sd', blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it started with '_'.
    field_3sd = models.FloatField(db_column='-3sd', blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it started with '_'.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Price_to_Earnings'


class Qoq(models.Model):
    fs_name = models.CharField(db_column='FS Name', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    fs_ticker = models.CharField(db_column='FS Ticker', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    sector = models.CharField(db_column='Sector', max_length=255, blank=True, null=True)  # Field name made lowercase.
    number_31_mar_14 = models.FloatField(db_column='31-Mar-14', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_30_jun_14 = models.FloatField(db_column='30-Jun-14', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_30_sep_14 = models.FloatField(db_column='30-Sep-14', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_31_dec_14 = models.FloatField(db_column='31-Dec-14', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_31_mar_15 = models.FloatField(db_column='31-Mar-15', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_30_jun_15 = models.FloatField(db_column='30-Jun-15', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_30_sep_15 = models.FloatField(db_column='30-Sep-15', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_31_dec_15 = models.FloatField(db_column='31-Dec-15', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_31_mar_16 = models.FloatField(db_column='31-Mar-16', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_30_jun_16 = models.FloatField(db_column='30-Jun-16', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_30_sep_16 = models.FloatField(db_column='30-Sep-16', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_30_dec_16 = models.FloatField(db_column='30-Dec-16', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_31_mar_17 = models.FloatField(db_column='31-Mar-17', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_30_jun_17 = models.FloatField(db_column='30-Jun-17', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_29_sep_17 = models.FloatField(db_column='29-Sep-17', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_29_dec_17 = models.FloatField(db_column='29-Dec-17', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_28_mar_18 = models.FloatField(db_column='28-Mar-18', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_29_jun_18 = models.FloatField(db_column='29-Jun-18', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_28_sep_18 = models.FloatField(db_column='28-Sep-18', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_31_dec_18 = models.FloatField(db_column='31-Dec-18', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_29_mar_19 = models.FloatField(db_column='29-Mar-19', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_28_jun_19 = models.FloatField(db_column='28-Jun-19', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_30_sep_19 = models.FloatField(db_column='30-Sep-19', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_31_dec_19 = models.FloatField(db_column='31-Dec-19', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_31_mar_20 = models.FloatField(db_column='31-Mar-20', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_30_jun_20 = models.FloatField(db_column='30-Jun-20', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'QoQ'


class RelativeStrengthSectors(models.Model):
    date = models.DateField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    nifty_50 = models.FloatField(db_column='NIFTY 50', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    price = models.FloatField(db_column='Price', blank=True, null=True)  # Field name made lowercase.
    ticker = models.TextField(db_column='Ticker', blank=True, null=True)  # Field name made lowercase.
    ticker_vs_nifty50 = models.FloatField(db_column='Ticker_vs_Nifty50', blank=True, null=True)  # Field name made lowercase.
    average = models.FloatField(db_column='Average', blank=True, null=True)  # Field name made lowercase.
    std = models.FloatField(db_column='StD', blank=True, null=True)  # Field name made lowercase.
    number_1sd = models.FloatField(db_column='1sd', blank=True, null=True)  # Field renamed because it wasn't a valid Python identifier.
    number_2sd = models.FloatField(db_column='2sd', blank=True, null=True)  # Field renamed because it wasn't a valid Python identifier.
    number_3sd = models.FloatField(db_column='3sd', blank=True, null=True)  # Field renamed because it wasn't a valid Python identifier.
    field_1sd = models.FloatField(db_column='-1sd', blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it started with '_'.
    field_2sd = models.FloatField(db_column='-2sd', blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it started with '_'.
    field_3sd = models.FloatField(db_column='-3sd', blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it started with '_'.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Relative_Strength_Sectors'


class RiskFreeRate(models.Model):
    date = models.DateField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    risk_free_rate = models.FloatField(db_column='Risk Free Rate', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Risk_Free_Rate'


class RiskMonitor(models.Model):
    fs_ticker = models.TextField(db_column='FS_Ticker', blank=True, null=True)  # Field name made lowercase.
    company_name = models.TextField(db_column='Company_Name', blank=True, null=True)  # Field name made lowercase.
    number_1y_cumulative_return = models.FloatField(db_column='1Y_Cumulative_Return', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_1m_cumulative_return = models.FloatField(db_column='1M_Cumulative_Return', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_3m_cumulative_return = models.FloatField(db_column='3M_Cumulative_Return', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    price = models.FloatField(db_column='Price', blank=True, null=True)  # Field name made lowercase.
    number_1m_excess_return = models.FloatField(db_column='1M_Excess_Return', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_3m_excess_return = models.FloatField(db_column='3M_Excess_Return', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_1y_excess_return = models.FloatField(db_column='1Y_Excess_Return', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_52_week_high = models.FloatField(db_column='52_Week_High', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_52_week_low = models.FloatField(db_column='52_Week_Low', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_1_month_high = models.FloatField(db_column='1_Month_High', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_1_month_low = models.FloatField(db_column='1_Month_Low', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    rsi = models.FloatField(db_column='RSI', blank=True, null=True)  # Field name made lowercase.
    number_20_ema = models.FloatField(db_column='20_EMA', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_50_dma = models.FloatField(db_column='50_DMA', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    vfx = models.FloatField(db_column='VFX', blank=True, null=True)  # Field name made lowercase.
    views = models.TextField(db_column='Views', blank=True, null=True)  # Field name made lowercase.
    id = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'Risk_Monitor'


class Snp500200Ma(models.Model):
    date = models.DateField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    s_p500_stocks_above_200ma = models.FloatField(db_column='S_P500 stocks above 200MA', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    average = models.FloatField(db_column='Average', blank=True, null=True)  # Field name made lowercase.
    std = models.FloatField(db_column='StD', blank=True, null=True)  # Field name made lowercase.
    number_1sd = models.FloatField(db_column='1sd', blank=True, null=True)  # Field renamed because it wasn't a valid Python identifier.
    number_2sd = models.FloatField(db_column='2sd', blank=True, null=True)  # Field renamed because it wasn't a valid Python identifier.
    number_3sd = models.FloatField(db_column='3sd', blank=True, null=True)  # Field renamed because it wasn't a valid Python identifier.
    field_1sd = models.FloatField(db_column='-1sd', blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it started with '_'.
    field_2sd = models.FloatField(db_column='-2sd', blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it started with '_'.
    field_3sd = models.FloatField(db_column='-3sd', blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it started with '_'.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'SNP500_200MA'


class SP500PePb(models.Model):
    date = models.DateTimeField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    trailing_12m_pe_of_s_p_500 = models.FloatField(db_column='Trailing 12M PE of S&P 500', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'S_P500_PE_PB'


class ScreenerFundamental(models.Model):
    pe = models.TextField(db_column='PE', blank=True, null=True)  # Field name made lowercase.
    pb = models.TextField(db_column='PB', blank=True, null=True)  # Field name made lowercase.
    ev_sales = models.TextField(db_column='EV_Sales', blank=True, null=True)  # Field name made lowercase.
    ev_ebitda = models.TextField(db_column='EV_EBITDA', blank=True, null=True)  # Field name made lowercase.
    div_yield = models.TextField(db_column='Div_Yield', blank=True, null=True)  # Field name made lowercase.
    sales_growth = models.TextField(db_column='Sales_Growth', blank=True, null=True)  # Field name made lowercase.
    sales_growth_3y_cagr = models.TextField(db_column='Sales_growth_3Y_CAGR', blank=True, null=True)  # Field name made lowercase.
    eps_growth = models.TextField(db_column='EPS_Growth', blank=True, null=True)  # Field name made lowercase.
    eps_growth_3y_cagr = models.TextField(db_column='EPS growth_3Y_CAGR', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    roe = models.TextField(db_column='ROE', blank=True, null=True)  # Field name made lowercase.
    roce = models.TextField(db_column='ROCE', blank=True, null=True)  # Field name made lowercase.
    fcf_margin = models.TextField(db_column='FCF_Margin', blank=True, null=True)  # Field name made lowercase.
    sales_total_assets = models.TextField(db_column='Sales_Total_Assets', blank=True, null=True)  # Field name made lowercase.
    net_debt_fcf = models.TextField(db_column='Net_Debt_FCF', blank=True, null=True)  # Field name made lowercase.
    net_debt_ebitda = models.TextField(db_column='Net_Debt_EBITDA', blank=True, null=True)  # Field name made lowercase.
    debt_equity = models.TextField(db_column='Debt_Equity', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Screener_Fundamental'


class ScreenerQuantitative(models.Model):
    quant_market_cap = models.TextField(db_column='Quant_Market_Cap', blank=True, null=True)  # Field name made lowercase.
    quant_sales = models.TextField(db_column='Quant_Sales', blank=True, null=True)  # Field name made lowercase.
    quant_sector = models.TextField(db_column='Quant_Sector', blank=True, null=True)  # Field name made lowercase.
    quant_fo_stocks = models.TextField(db_column='Quant_fo_stocks', blank=True, null=True)  # Field name made lowercase.
    quant_price = models.TextField(db_column='Quant_Price', blank=True, null=True)  # Field name made lowercase.
    quant_target_price = models.TextField(db_column='Quant_Target_Price', blank=True, null=True)  # Field name made lowercase.
    quant_volume = models.TextField(db_column='Quant_Volume', blank=True, null=True)  # Field name made lowercase.
    quant_index = models.TextField(db_column='Quant_Index', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Screener_Quantitative'


class ScreenerRisk(models.Model):
    beta = models.TextField(db_column='Beta', blank=True, null=True)  # Field name made lowercase.
    volatility = models.TextField(db_column='Volatility', blank=True, null=True)  # Field name made lowercase.
    piotroski_score = models.TextField(db_column='Piotroski_Score', blank=True, null=True)  # Field name made lowercase.
    average_daily_traded_volume = models.TextField(db_column='Average_Daily_Traded_Volume', blank=True, null=True)  # Field name made lowercase.
    share_turnover = models.TextField(db_column='Share_Turnover', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Screener_Risk'


class ScreenerTechnical(models.Model):
    relative_strength_30d = models.CharField(db_column='Relative Strength 30D', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    relative_strength_90d = models.CharField(db_column='Relative Strength 90D', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    volume_change = models.CharField(db_column='Volume Change', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    number_52_week_high = models.CharField(db_column='52 Week High', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    ma20 = models.CharField(db_column='MA20', max_length=255, blank=True, null=True)  # Field name made lowercase.
    ma50 = models.CharField(db_column='MA50', max_length=255, blank=True, null=True)  # Field name made lowercase.
    ma200 = models.CharField(db_column='MA200', max_length=255, blank=True, null=True)  # Field name made lowercase.
    rsi_14d_field = models.CharField(db_column='RSI (14D)', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it ended with '_'.
    absolute_return_mtd = models.CharField(db_column='Absolute_Return_MTD', max_length=255, blank=True, null=True)  # Field name made lowercase.
    absolute_return_ytd = models.CharField(db_column='Absolute_Return_YTD', max_length=255, blank=True, null=True)  # Field name made lowercase.
    absolute_return_qtd = models.CharField(db_column='Absolute_Return_QTD', max_length=255, blank=True, null=True)  # Field name made lowercase.
    absolute_return_1y = models.CharField(db_column='Absolute_Return_1Y', max_length=255, blank=True, null=True)  # Field name made lowercase.
    absolute_return_1m = models.CharField(db_column='Absolute_Return_1M', max_length=255, blank=True, null=True)  # Field name made lowercase.
    absolute_return_3m = models.CharField(db_column='Absolute_Return_3M', max_length=255, blank=True, null=True)  # Field name made lowercase.
    absolute_return_6m = models.CharField(db_column='Absolute_Return_6M', max_length=255, blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Screener_Technical'


class ScreenerTechnicalDummy(models.Model):
    relative_strength_30d = models.TextField(db_column='Relative Strength 30D', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    relative_strength_90d = models.TextField(db_column='Relative Strength 90D', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    volume_change = models.TextField(db_column='Volume Change', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    number_52_week_high = models.TextField(db_column='52 Week High', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    ma50 = models.TextField(db_column='MA50', blank=True, null=True)  # Field name made lowercase.
    ma200 = models.TextField(db_column='MA200', blank=True, null=True)  # Field name made lowercase.
    rsi_14d_field = models.TextField(db_column='RSI (14D)', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it ended with '_'.
    relative_strength_180d = models.TextField(db_column='Relative Strength 180D', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    relative_strength_360d = models.TextField(db_column='Relative Strength 360D', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ma20 = models.TextField(db_column='MA20', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Screener_Technical_dummy'


class Screening(models.Model):
    fs_name = models.TextField(db_column='FS Name', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    security_code = models.TextField(db_column='Security Code', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    fs_ticker = models.TextField(db_column='FS Ticker', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    market_cap = models.FloatField(db_column='Market Cap', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    market_cap_category = models.TextField(db_column='Market_Cap_Category', blank=True, null=True)  # Field name made lowercase.
    sales = models.FloatField(db_column='Sales', blank=True, null=True)  # Field name made lowercase.
    gics = models.TextField(db_column='GICS', blank=True, null=True)  # Field name made lowercase.
    beta_1y = models.FloatField(db_column='Beta_1Y', blank=True, null=True)  # Field name made lowercase.
    volatility_1y = models.FloatField(db_column='Volatility_1Y', blank=True, null=True)  # Field name made lowercase.
    piotroski_score = models.FloatField(db_column='Piotroski_Score', blank=True, null=True)  # Field name made lowercase.
    pe = models.FloatField(db_column='PE', blank=True, null=True)  # Field name made lowercase.
    pb = models.FloatField(db_column='PB', blank=True, null=True)  # Field name made lowercase.
    ev_sales = models.FloatField(db_column='EV/Sales', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ev_ebitda = models.FloatField(db_column='EV/EBITDA', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    div_yield = models.FloatField(db_column='Div_Yield', blank=True, null=True)  # Field name made lowercase.
    sales_growth = models.FloatField(db_column='Sales_Growth', blank=True, null=True)  # Field name made lowercase.
    sales_growth_3y_5r_cagr = models.FloatField(db_column='Sales_Growth_3Y/5R_CAGR', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    eps_growth = models.FloatField(db_column='EPS_Growth', blank=True, null=True)  # Field name made lowercase.
    eps_growth_3y_5r_cagr = models.FloatField(db_column='EPS_Growth_3Y/5R_CAGR', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    roe = models.FloatField(db_column='ROE', blank=True, null=True)  # Field name made lowercase.
    roce = models.FloatField(db_column='ROCE', blank=True, null=True)  # Field name made lowercase.
    fcf_margin = models.FloatField(db_column='FCF_Margin', blank=True, null=True)  # Field name made lowercase.
    sales_total_assets = models.FloatField(db_column='Sales/Total Assets', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    net_debt_fcf = models.FloatField(db_column='Net_Debt/FCF', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    net_debt_ebitda = models.FloatField(db_column='Net_Debt/EBITDA', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    debt_equity = models.FloatField(db_column='Debt/Equity', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    price = models.FloatField(db_column='Price', blank=True, null=True)  # Field name made lowercase.
    f_o = models.TextField(db_column='F_O', blank=True, null=True)  # Field name made lowercase.
    volume = models.FloatField(db_column='Volume', blank=True, null=True)  # Field name made lowercase.
    rsi = models.FloatField(db_column='RSI', blank=True, null=True)  # Field name made lowercase.
    ma50 = models.FloatField(db_column='MA50', blank=True, null=True)  # Field name made lowercase.
    ma200 = models.FloatField(db_column='MA200', blank=True, null=True)  # Field name made lowercase.
    ma20 = models.FloatField(db_column='MA20', blank=True, null=True)  # Field name made lowercase.
    number_52_week_high = models.FloatField(db_column='52_Week_High', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    target_price = models.FloatField(db_column='Target_Price', blank=True, null=True)  # Field name made lowercase.
    volume_20 = models.FloatField(db_column='Volume_20', blank=True, null=True)  # Field name made lowercase.
    rsi_30 = models.FloatField(db_column='RSI_30', blank=True, null=True)  # Field name made lowercase.
    rsi_90 = models.FloatField(db_column='RSI_90', blank=True, null=True)  # Field name made lowercase.
    share_turnover = models.FloatField(db_column='Share Turnover', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    volume_change = models.FloatField(db_column='Volume_Change', blank=True, null=True)  # Field name made lowercase.
    mtd = models.FloatField(db_column='MTD', blank=True, null=True)  # Field name made lowercase.
    ytd = models.FloatField(db_column='YTD', blank=True, null=True)  # Field name made lowercase.
    number_1y = models.FloatField(db_column='1Y', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_1m = models.FloatField(db_column='1M', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_3m = models.FloatField(db_column='3M', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_6m = models.FloatField(db_column='6M', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    qtd = models.FloatField(db_column='QTD', blank=True, null=True)  # Field name made lowercase.
    gross_profit_margin = models.FloatField(blank=True, null=True)
    altmanz_score = models.FloatField(db_column='Altmanz_score', blank=True, null=True)  # Field name made lowercase.
    id = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'Screening'


class SectorIndicesPePbPerformance(models.Model):
    gics = models.CharField(db_column='GICS', max_length=255, blank=True, null=True)  # Field name made lowercase.
    factor = models.CharField(db_column='Factor', max_length=255, blank=True, null=True)  # Field name made lowercase.
    date = models.CharField(db_column='Date', max_length=255, blank=True, null=True)  # Field name made lowercase.
    value = models.FloatField(db_column='Value', blank=True, null=True)  # Field name made lowercase.
    date_new = models.DateField(blank=True, null=True)
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Sector_Indices_PE_PB_Performance'


class SecurityMaster(models.Model):
    security_code = models.CharField(db_column='Security Code', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    security_id = models.CharField(db_column='Security Id', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    isin_no = models.CharField(db_column='ISIN No', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    fs_ticker = models.CharField(db_column='FS Ticker', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    bbg_ticker = models.CharField(db_column='BBg Ticker', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    sedol = models.CharField(db_column='SEDOL', max_length=255, blank=True, null=True)  # Field name made lowercase.
    security_name = models.CharField(db_column='Security Name', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    fs_name = models.CharField(db_column='FS Name', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    market_cap = models.FloatField(db_column='Market Cap', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    gics = models.CharField(db_column='GICS', max_length=255, blank=True, null=True)  # Field name made lowercase.
    market_cap_category = models.CharField(db_column='Market_Cap_Category', max_length=50, blank=True, null=True)  # Field name made lowercase.
    flag = models.CharField(db_column='Flag', max_length=10, blank=True, null=True)  # Field name made lowercase.
    beta_1y = models.FloatField(db_column='Beta_1Y', blank=True, null=True)  # Field name made lowercase.
    volatility_1y = models.FloatField(db_column='Volatility_1Y', blank=True, null=True)  # Field name made lowercase.
    idiosyncratic_vol = models.FloatField(db_column='Idiosyncratic_Vol', blank=True, null=True)  # Field name made lowercase.
    mcap_rm_field = models.FloatField(db_column='Mcap(RM)', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it ended with '_'.
    sales_rm_field = models.FloatField(db_column='Sales(RM)', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it ended with '_'.
    pe = models.FloatField(db_column='PE', blank=True, null=True)  # Field name made lowercase.
    pb = models.FloatField(db_column='PB', blank=True, null=True)  # Field name made lowercase.
    div_yield = models.FloatField(db_column='Div_Yield', blank=True, null=True)  # Field name made lowercase.
    div_payout = models.FloatField(db_column='Div_Payout', blank=True, null=True)  # Field name made lowercase.
    sales_growth = models.FloatField(db_column='Sales_Growth', blank=True, null=True)  # Field name made lowercase.
    sales_growth_qoq_growth = models.FloatField(db_column='Sales_Growth_QoQ_Growth', blank=True, null=True)  # Field name made lowercase.
    sales_growth_3y_5r_cagr = models.FloatField(db_column='Sales_Growth_3Y/5R_CAGR', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    eps_growth = models.FloatField(db_column='EPS_Growth', blank=True, null=True)  # Field name made lowercase.
    eps_growth_qoq_growth = models.FloatField(db_column='EPS_Growth_QoQ_Growth', blank=True, null=True)  # Field name made lowercase.
    eps_growth_3y_5r_cagr = models.FloatField(db_column='EPS_Growth_3Y/5R_CAGR', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    roe = models.FloatField(db_column='ROE', blank=True, null=True)  # Field name made lowercase.
    roce = models.FloatField(db_column='ROCE', blank=True, null=True)  # Field name made lowercase.
    ebitda_margin = models.FloatField(db_column='EBITDA_Margin', blank=True, null=True)  # Field name made lowercase.
    net_income_margin = models.FloatField(db_column='Net_Income_Margin', blank=True, null=True)  # Field name made lowercase.
    fcf_margin = models.FloatField(db_column='FCF_Margin', blank=True, null=True)  # Field name made lowercase.
    sales_total_assets = models.FloatField(db_column='Sales/Total Assets', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    net_debt_fcf = models.FloatField(db_column='Net_Debt/FCF', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    net_debt_ebitda = models.FloatField(db_column='Net_Debt/EBITDA', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    debt_equity = models.FloatField(db_column='Debt/Equity', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    piotroski_score = models.FloatField(db_column='Piotroski_Score', blank=True, null=True)  # Field name made lowercase.
    nifty_50 = models.CharField(db_column='Nifty 50', max_length=25, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    nifty_small_cap_100 = models.CharField(db_column='Nifty Small Cap 100', max_length=25, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    nifty_mid_cap_100 = models.CharField(db_column='Nifty Mid Cap 100', max_length=25, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    shares_outstanding = models.BigIntegerField(db_column='Shares Outstanding', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ev_sales = models.FloatField(db_column='EV/Sales', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ev_ebitda = models.FloatField(db_column='EV/EBITDA', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    nseflag = models.CharField(max_length=255, blank=True, null=True)
    bseflag = models.CharField(max_length=255, blank=True, null=True)
    qtr_eps_growth_yoy = models.FloatField(db_column='QTR EPS_Growth_YoY', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    qtr_sales_growth_yoy = models.FloatField(db_column='QTR Sales_Growth_YoY', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ev = models.FloatField(db_column='EV', blank=True, null=True)  # Field name made lowercase.
    etf_flag = models.TextField(db_column='ETF_Flag', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Security_Master'


class Snp500Schiller(models.Model):
    date = models.DateTimeField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    shiller_pe_ratio = models.FloatField(db_column='Shiller PE Ratio', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'SnP500_Schiller'


class Snp500SchillerPriceToEarnings(models.Model):
    date = models.DateTimeField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    p_e = models.FloatField(db_column='P/E', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    average = models.FloatField(db_column='Average', blank=True, null=True)  # Field name made lowercase.
    std = models.FloatField(db_column='StD', blank=True, null=True)  # Field name made lowercase.
    number_1sd = models.FloatField(db_column='1sd', blank=True, null=True)  # Field renamed because it wasn't a valid Python identifier.
    number_2sd = models.FloatField(db_column='2sd', blank=True, null=True)  # Field renamed because it wasn't a valid Python identifier.
    number_3sd = models.FloatField(db_column='3sd', blank=True, null=True)  # Field renamed because it wasn't a valid Python identifier.
    field_1sd = models.FloatField(db_column='-1sd', blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it started with '_'.
    field_2sd = models.FloatField(db_column='-2sd', blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it started with '_'.
    field_3sd = models.FloatField(db_column='-3sd', blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it started with '_'.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'SnP500_Schiller_Price_to_Earnings'


class Snp500TtmPriceToEarnings(models.Model):
    date = models.DateTimeField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    p_e = models.FloatField(db_column='P/E', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    average = models.FloatField(db_column='Average', blank=True, null=True)  # Field name made lowercase.
    std = models.FloatField(db_column='StD', blank=True, null=True)  # Field name made lowercase.
    number_1sd = models.FloatField(db_column='1sd', blank=True, null=True)  # Field renamed because it wasn't a valid Python identifier.
    number_2sd = models.FloatField(db_column='2sd', blank=True, null=True)  # Field renamed because it wasn't a valid Python identifier.
    number_3sd = models.FloatField(db_column='3sd', blank=True, null=True)  # Field renamed because it wasn't a valid Python identifier.
    field_1sd = models.FloatField(db_column='-1sd', blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it started with '_'.
    field_2sd = models.FloatField(db_column='-2sd', blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it started with '_'.
    field_3sd = models.FloatField(db_column='-3sd', blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it started with '_'.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'SnP500_TTM_Price_to_Earnings'


class Snp500StockReturns(models.Model):
    date = models.DateTimeField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    ticker = models.TextField(db_column='Ticker', blank=True, null=True)  # Field name made lowercase.
    name = models.TextField(db_column='Name', blank=True, null=True)  # Field name made lowercase.
    currentprice = models.FloatField(db_column='CurrentPrice', blank=True, null=True)  # Field name made lowercase.
    dailychange = models.FloatField(db_column='DailyChange', blank=True, null=True)  # Field name made lowercase.
    dailypercentchange = models.FloatField(db_column='DailyPercentChange', blank=True, null=True)  # Field name made lowercase.
    weeklypricechange = models.FloatField(db_column='WeeklyPriceChange', blank=True, null=True)  # Field name made lowercase.
    weeklypricechangepercent = models.FloatField(db_column='WeeklyPriceChangePercent', blank=True, null=True)  # Field name made lowercase.
    monthlypricechange = models.FloatField(db_column='MonthlyPriceChange', blank=True, null=True)  # Field name made lowercase.
    monthlypricechangepercent = models.FloatField(db_column='MonthlyPriceChangePercent', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Snp500_Stock_Returns'


class StockRanking(models.Model):
    date = models.DateField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    co_code = models.FloatField(blank=True, null=True)
    nsesymbol = models.TextField(blank=True, null=True)
    fs_name = models.TextField(db_column='FS Name', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    fs_ticker = models.TextField(db_column='FS Ticker', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    sales = models.FloatField(db_column='Sales', blank=True, null=True)  # Field name made lowercase.
    piotroski_score = models.FloatField(db_column='Piotroski_Score', blank=True, null=True)  # Field name made lowercase.
    roe = models.FloatField(db_column='ROE', blank=True, null=True)  # Field name made lowercase.
    debt_equity = models.FloatField(db_column='Debt/Equity', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    fcf_margin = models.FloatField(db_column='FCF_Margin', blank=True, null=True)  # Field name made lowercase.
    beta = models.FloatField(db_column='Beta', blank=True, null=True)  # Field name made lowercase.
    sales_growth = models.FloatField(db_column='Sales_Growth', blank=True, null=True)  # Field name made lowercase.
    eps_growth = models.FloatField(db_column='EPS_Growth', blank=True, null=True)  # Field name made lowercase.
    sales_qoq = models.FloatField(db_column='Sales_QoQ', blank=True, null=True)  # Field name made lowercase.
    price_vs_ema20 = models.FloatField(db_column='Price_vs_EMA20', blank=True, null=True)  # Field name made lowercase.
    price_vs_ema50 = models.FloatField(db_column='Price_vs_EMA50', blank=True, null=True)  # Field name made lowercase.
    price_vs_ema200 = models.FloatField(db_column='Price_vs_EMA200', blank=True, null=True)  # Field name made lowercase.
    ema50_vs_ema200 = models.FloatField(db_column='EMA50_vs_EMA200', blank=True, null=True)  # Field name made lowercase.
    current_price_vs_52_week_high = models.FloatField(db_column='Current_Price_vs_52_Week_High', blank=True, null=True)  # Field name made lowercase.
    rsi = models.FloatField(db_column='RSI', blank=True, null=True)  # Field name made lowercase.
    price = models.FloatField(db_column='Price', blank=True, null=True)  # Field name made lowercase.
    return_field = models.FloatField(db_column='Return', blank=True, null=True)  # Field name made lowercase. Field renamed because it was a Python reserved word.
    net_change = models.FloatField(db_column='Net Change', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    q_sales = models.FloatField(db_column='Q_Sales', blank=True, null=True)  # Field name made lowercase.
    q_piotroski_score = models.FloatField(db_column='Q_Piotroski_Score', blank=True, null=True)  # Field name made lowercase.
    q_roe = models.FloatField(db_column='Q_ROE', blank=True, null=True)  # Field name made lowercase.
    q_debt_equity = models.FloatField(db_column='Q_Debt/Equity', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    q_fcf_margin = models.FloatField(db_column='Q_FCF_Margin', blank=True, null=True)  # Field name made lowercase.
    q_beta = models.IntegerField(db_column='Q_Beta', blank=True, null=True)  # Field name made lowercase.
    g_eps_growth = models.FloatField(db_column='G_EPS_Growth', blank=True, null=True)  # Field name made lowercase.
    g_sales_growth = models.FloatField(db_column='G_Sales_Growth', blank=True, null=True)  # Field name made lowercase.
    g_sales_qoq = models.FloatField(db_column='G_Sales_QoQ', blank=True, null=True)  # Field name made lowercase.
    t_price_vs_ema20 = models.IntegerField(db_column='T_Price_vs_EMA20', blank=True, null=True)  # Field name made lowercase.
    t_price_vs_ema50 = models.IntegerField(db_column='T_Price_vs_EMA50', blank=True, null=True)  # Field name made lowercase.
    t_price_vs_ema200 = models.IntegerField(db_column='T_Price_vs_EMA200', blank=True, null=True)  # Field name made lowercase.
    t_rsi = models.IntegerField(db_column='T_RSI', blank=True, null=True)  # Field name made lowercase.
    t_ema50_vs_ema200 = models.IntegerField(db_column='T_EMA50_vs_EMA200', blank=True, null=True)  # Field name made lowercase.
    t_current_price_vs_52_week_high = models.IntegerField(db_column='T_Current_Price_vs_52_Week_High', blank=True, null=True)  # Field name made lowercase.
    quality_score = models.FloatField(db_column='Quality_Score', blank=True, null=True)  # Field name made lowercase.
    growth_score = models.FloatField(db_column='Growth_Score', blank=True, null=True)  # Field name made lowercase.
    technical_score = models.FloatField(db_column='Technical_Score', blank=True, null=True)  # Field name made lowercase.
    stock_rank = models.FloatField(db_column='Stock_Rank', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Stock_Ranking'


class StocksDailyBeta(models.Model):
    date = models.DateTimeField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    company = models.TextField(db_column='Company', blank=True, null=True)  # Field name made lowercase.
    index = models.TextField(db_column='Index', blank=True, null=True)  # Field name made lowercase.
    return_field = models.FloatField(db_column='Return', blank=True, null=True)  # Field name made lowercase. Field renamed because it was a Python reserved word.
    index_return = models.FloatField(db_column='Index Return', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    beta_1y = models.FloatField(db_column='Beta_1Y', blank=True, null=True)  # Field name made lowercase.
    factset_ticker = models.TextField(db_column='Factset_Ticker', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Stocks_Daily_Beta'


class TargetPrices(models.Model):
    ticker = models.CharField(db_column='Ticker', max_length=255, blank=True, null=True)  # Field name made lowercase.
    current_price = models.FloatField(db_column='Current_Price', blank=True, null=True)  # Field name made lowercase.
    number_52_week_high = models.FloatField(db_column='52_Week_High', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_52_week_low = models.FloatField(db_column='52_Week_Low', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_1_month_high = models.FloatField(db_column='1 Month High', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_1_month_low = models.FloatField(db_column='1 Month Low', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    target_price = models.FloatField(db_column='Target_Price', blank=True, null=True)  # Field name made lowercase.
    avg_upside = models.FloatField(db_column='Avg_Upside', blank=True, null=True)  # Field name made lowercase.
    avg_downside = models.FloatField(db_column='Avg_Downside', blank=True, null=True)  # Field name made lowercase.
    company = models.CharField(db_column='Company', max_length=255, blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Target_prices'


class TechnicalIndicatorsDaily(models.Model):
    date = models.DateTimeField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    fs_ticker = models.TextField(db_column='FS_Ticker', blank=True, null=True)  # Field name made lowercase.
    symbol = models.TextField(db_column='Symbol', blank=True, null=True)  # Field name made lowercase.
    company_name = models.TextField(db_column='Company_Name', blank=True, null=True)  # Field name made lowercase.
    price = models.FloatField(db_column='Price', blank=True, null=True)  # Field name made lowercase.
    return_field = models.FloatField(db_column='Return', blank=True, null=True)  # Field name made lowercase. Field renamed because it was a Python reserved word.
    net_change = models.FloatField(db_column='Net_Change', blank=True, null=True)  # Field name made lowercase.
    ma9 = models.FloatField(db_column='MA9', blank=True, null=True)  # Field name made lowercase.
    ma20 = models.FloatField(db_column='MA20', blank=True, null=True)  # Field name made lowercase.
    ma50 = models.FloatField(db_column='MA50', blank=True, null=True)  # Field name made lowercase.
    ma200 = models.FloatField(db_column='MA200', blank=True, null=True)  # Field name made lowercase.
    ma9_ma20_value = models.TextField(db_column='MA9_MA20_Value', blank=True, null=True)  # Field name made lowercase.
    ma20_ma50_value = models.TextField(db_column='MA20_MA50_Value', blank=True, null=True)  # Field name made lowercase.
    ma50_ma200_value = models.TextField(db_column='MA50_MA200_Value', blank=True, null=True)  # Field name made lowercase.
    ma9_actions = models.TextField(db_column='MA9_Actions', blank=True, null=True)  # Field name made lowercase.
    ma50_actions = models.TextField(db_column='MA50_Actions', blank=True, null=True)  # Field name made lowercase.
    ma20_actions = models.TextField(db_column='MA20_Actions', blank=True, null=True)  # Field name made lowercase.
    ma200_actions = models.TextField(db_column='MA200_Actions', blank=True, null=True)  # Field name made lowercase.
    ma9_ma20_actions = models.TextField(db_column='MA9_MA20_Actions', blank=True, null=True)  # Field name made lowercase.
    ma20_ma50_actions = models.TextField(db_column='MA20_MA50_Actions', blank=True, null=True)  # Field name made lowercase.
    ma50_ma200_actions = models.TextField(db_column='MA50_MA200_Actions', blank=True, null=True)  # Field name made lowercase.
    rsi_value = models.FloatField(db_column='RSI_Value', blank=True, null=True)  # Field name made lowercase.
    cci_value = models.FloatField(db_column='CCI_Value', blank=True, null=True)  # Field name made lowercase.
    williams_value = models.FloatField(db_column='Williams_Value', blank=True, null=True)  # Field name made lowercase.
    stochrsi_value = models.FloatField(db_column='StochRSI_Value', blank=True, null=True)  # Field name made lowercase.
    stochastics_value = models.FloatField(db_column='Stochastics_Value', blank=True, null=True)  # Field name made lowercase.
    rsi_actions = models.TextField(db_column='RSI_Actions', blank=True, null=True)  # Field name made lowercase.
    cci_actions = models.TextField(db_column='CCI_Actions', blank=True, null=True)  # Field name made lowercase.
    williams_actions = models.TextField(db_column='Williams_Actions', blank=True, null=True)  # Field name made lowercase.
    stochrsi_actions = models.TextField(db_column='StochRSI_Actions', blank=True, null=True)  # Field name made lowercase.
    stochastics_actions = models.TextField(db_column='Stochastics_Actions', blank=True, null=True)  # Field name made lowercase.
    macd_value = models.FloatField(db_column='MACD_Value', blank=True, null=True)  # Field name made lowercase.
    atr_value = models.FloatField(db_column='ATR_Value', blank=True, null=True)  # Field name made lowercase.
    adx_value = models.FloatField(db_column='ADX_Value', blank=True, null=True)  # Field name made lowercase.
    super_trend_value = models.FloatField(db_column='Super_Trend_Value', blank=True, null=True)  # Field name made lowercase.
    macd_actions = models.TextField(db_column='MACD_Actions', blank=True, null=True)  # Field name made lowercase.
    atr_actions = models.TextField(db_column='ATR_Actions', blank=True, null=True)  # Field name made lowercase.
    adx_actions = models.TextField(db_column='ADX_Actions', blank=True, null=True)  # Field name made lowercase.
    super_trend_actions = models.TextField(db_column='Super_Trend_Actions', blank=True, null=True)  # Field name made lowercase.
    mfi_value = models.FloatField(db_column='MFI_Value', blank=True, null=True)  # Field name made lowercase.
    pvo_value = models.FloatField(db_column='PVO_Value', blank=True, null=True)  # Field name made lowercase.
    cmf_value = models.FloatField(db_column='CMF_Value', blank=True, null=True)  # Field name made lowercase.
    mfi_actions = models.TextField(db_column='MFI_Actions', blank=True, null=True)  # Field name made lowercase.
    pvo_actions = models.TextField(db_column='PVO_Actions', blank=True, null=True)  # Field name made lowercase.
    cmf_actions = models.TextField(db_column='CMF_Actions', blank=True, null=True)  # Field name made lowercase.
    moving_average_rating = models.TextField(db_column='Moving_Average_Rating', blank=True, null=True)  # Field name made lowercase.
    momentum_rating = models.TextField(db_column='Momentum_Rating', blank=True, null=True)  # Field name made lowercase.
    trend_rating = models.TextField(db_column='Trend_Rating', blank=True, null=True)  # Field name made lowercase.
    volume_rating = models.TextField(db_column='Volume_Rating', blank=True, null=True)  # Field name made lowercase.
    final_rating = models.TextField(db_column='Final_Rating', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Technical_Indicators_Daily'


class Transactions(models.Model):
    security_id = models.CharField(db_column='SECURITY ID', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    tradedate = models.DateTimeField(db_column='TRADEDATE', blank=True, null=True)  # Field name made lowercase.
    action = models.CharField(db_column='ACTION', max_length=255, blank=True, null=True)  # Field name made lowercase.
    status = models.CharField(db_column='STATUS', max_length=255, blank=True, null=True)  # Field name made lowercase.
    allocation_id = models.CharField(db_column='ALLOCATION ID', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    secdescription = models.CharField(db_column='SECDESCRIPTION', max_length=255, blank=True, null=True)  # Field name made lowercase.
    sedol = models.FloatField(db_column='SEDOL', blank=True, null=True)  # Field name made lowercase.
    isin = models.FloatField(db_column='ISIN', blank=True, null=True)  # Field name made lowercase.
    cusip = models.FloatField(db_column='CUSIP', blank=True, null=True)  # Field name made lowercase.
    trader = models.CharField(db_column='TRADER', max_length=255, blank=True, null=True)  # Field name made lowercase.
    portfolio = models.CharField(db_column='PORTFOLIO', max_length=255, blank=True, null=True)  # Field name made lowercase.
    customer = models.CharField(db_column='Customer', max_length=255, blank=True, null=True)  # Field name made lowercase.
    quantity = models.FloatField(db_column='QUANTITY', blank=True, null=True)  # Field name made lowercase.
    price = models.FloatField(db_column='PRICE', blank=True, null=True)  # Field name made lowercase.
    fx_rate = models.FloatField(db_column='FX RATE', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    grossmoney = models.FloatField(db_column='GROSSMONEY', blank=True, null=True)  # Field name made lowercase.
    totalcomm = models.FloatField(db_column='TOTALCOMM', blank=True, null=True)  # Field name made lowercase.
    totalfees = models.FloatField(db_column='TOTALFEES', blank=True, null=True)  # Field name made lowercase.
    netmoney = models.FloatField(db_column='NETMONEY', blank=True, null=True)  # Field name made lowercase.
    execcurrency = models.CharField(db_column='EXECCURRENCY', max_length=255, blank=True, null=True)  # Field name made lowercase.
    broker = models.CharField(db_column='BROKER', max_length=255, blank=True, null=True)  # Field name made lowercase.
    custodian = models.CharField(db_column='CUSTODIAN', max_length=255, blank=True, null=True)  # Field name made lowercase.
    accrued_int = models.CharField(db_column='ACCRUED INT', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    strategy = models.CharField(db_column='STRATEGY', max_length=255, blank=True, null=True)  # Field name made lowercase.
    bid_qty = models.CharField(db_column='BID QTY', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    bid_price = models.CharField(db_column='BID PRICE', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    pricing_date = models.DateTimeField(db_column='PRICING DATE', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    first_trade_date = models.DateTimeField(db_column='FIRST TRADE DATE', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    deal_captian = models.CharField(db_column='DEAL CAPTIAN', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    hedge = models.CharField(db_column='HEDGE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    intial_target_price = models.CharField(db_column='INTIAL TARGET PRICE', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    target_price = models.CharField(db_column='TARGET PRICE', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ultimate_stop = models.CharField(db_column='ULTIMATE STOP', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    country = models.CharField(db_column='COUNTRY', max_length=255, blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Transactions'


class Usmarketpoints(models.Model):
    page_ptr_id = models.IntegerField()
    posted_at = models.DateTimeField(blank=True, null=True)
    body = models.TextField(blank=True, null=True)
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'USMarketPoints'


class Us10YrBy(models.Model):
    date = models.DateTimeField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    price = models.FloatField(db_column='Price', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'US_10Yr_BY'


class UsAdvancesDeclines(models.Model):
    date = models.TextField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    count = models.BigIntegerField(db_column='Count', blank=True, null=True)  # Field name made lowercase.
    type = models.TextField(db_column='Type', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'US_Advances_Declines'


class UsAverageTrueRange(models.Model):
    date = models.DateField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    company_name = models.TextField(db_column='Company_Name', blank=True, null=True)  # Field name made lowercase.
    open = models.FloatField(db_column='Open', blank=True, null=True)  # Field name made lowercase.
    low = models.FloatField(db_column='Low', blank=True, null=True)  # Field name made lowercase.
    high = models.FloatField(db_column='High', blank=True, null=True)  # Field name made lowercase.
    close = models.FloatField(db_column='Close', blank=True, null=True)  # Field name made lowercase.
    volume = models.BigIntegerField(db_column='Volume', blank=True, null=True)  # Field name made lowercase.
    prev_close = models.FloatField(db_column='Prev_Close', blank=True, null=True)  # Field name made lowercase.
    tr = models.FloatField(db_column='TR', blank=True, null=True)  # Field name made lowercase.
    typical_price = models.FloatField(db_column='Typical_Price', blank=True, null=True)  # Field name made lowercase.
    atr = models.FloatField(db_column='ATR', blank=True, null=True)  # Field name made lowercase.
    fs_ticker = models.TextField(db_column='FS_Ticker', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'US_Average_True_Range'


class UsBondEquityEarnings(models.Model):
    date = models.DateTimeField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    p_e = models.FloatField(db_column='P/E', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    number_10yr_g_sec = models.FloatField(db_column='10Yr G-Sec', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    beer = models.FloatField(db_column='Beer', blank=True, null=True)  # Field name made lowercase.
    average = models.FloatField(db_column='Average', blank=True, null=True)  # Field name made lowercase.
    std = models.FloatField(db_column='StD', blank=True, null=True)  # Field name made lowercase.
    number_1sd = models.FloatField(db_column='1sd', blank=True, null=True)  # Field renamed because it wasn't a valid Python identifier.
    number_2sd = models.FloatField(db_column='2sd', blank=True, null=True)  # Field renamed because it wasn't a valid Python identifier.
    number_3sd = models.FloatField(db_column='3sd', blank=True, null=True)  # Field renamed because it wasn't a valid Python identifier.
    field_1sd = models.FloatField(db_column='-1sd', blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it started with '_'.
    field_2sd = models.FloatField(db_column='-2sd', blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it started with '_'.
    field_3sd = models.FloatField(db_column='-3sd', blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it started with '_'.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'US_Bond_Equity_Earnings'


class UsBuyTrades(models.Model):
    stock = models.TextField(db_column='Stock', blank=True, null=True)  # Field name made lowercase.
    trade_time = models.DateTimeField(db_column='Trade_Time', blank=True, null=True)  # Field name made lowercase.
    remaining_position = models.FloatField(db_column='Remaining_Position', blank=True, null=True)  # Field name made lowercase.
    price = models.FloatField(db_column='Price', blank=True, null=True)  # Field name made lowercase.
    customer_name = models.TextField(db_column='Customer_Name', blank=True, null=True)  # Field name made lowercase.
    portfolio_name = models.TextField(db_column='Portfolio_name', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'US_Buy_Trades'


class UsCompanyDescriptions(models.Model):
    co_code = models.TextField(blank=True, null=True)
    company = models.TextField(db_column='Company', blank=True, null=True)  # Field name made lowercase.
    description = models.TextField(db_column='Description', blank=True, null=True)  # Field name made lowercase.
    shortdescription = models.TextField(db_column='ShortDescription', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'US_Company_Descriptions'


class UsCompanyAddressDetails2(models.Model):
    ticker = models.TextField(db_column='Ticker', blank=True, null=True)  # Field name made lowercase.
    lname = models.TextField(db_column='LNAME', blank=True, null=True)  # Field name made lowercase.
    regdist = models.TextField(db_column='REGDIST', blank=True, null=True)  # Field name made lowercase.
    regstate = models.TextField(db_column='REGSTATE', blank=True, null=True)  # Field name made lowercase.
    ind_l_name = models.TextField(blank=True, null=True)
    tel1 = models.TextField(db_column='TEL1', blank=True, null=True)  # Field name made lowercase.
    internet = models.TextField(db_column='INTERNET', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'US_Company_address_details_2'


class UsDailyPositions(models.Model):
    date = models.DateTimeField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    stock = models.TextField(db_column='Stock', blank=True, null=True)  # Field name made lowercase.
    buy_count = models.BigIntegerField(db_column='Buy_Count', blank=True, null=True)  # Field name made lowercase.
    buy_quantity = models.FloatField(db_column='Buy_Quantity', blank=True, null=True)  # Field name made lowercase.
    sell_count = models.FloatField(db_column='Sell_Count', blank=True, null=True)  # Field name made lowercase.
    sell_quantity = models.FloatField(db_column='Sell_Quantity', blank=True, null=True)  # Field name made lowercase.
    close_price = models.FloatField(db_column='Close_Price', blank=True, null=True)  # Field name made lowercase.
    prev_close_price = models.FloatField(db_column='Prev_Close_Price', blank=True, null=True)  # Field name made lowercase.
    prev_position = models.BigIntegerField(db_column='Prev_Position', blank=True, null=True)  # Field name made lowercase.
    prev_p_lunrealized = models.BigIntegerField(db_column='Prev_P&LUnrealized', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    bbg_ticker = models.TextField(db_column='BBG_Ticker', blank=True, null=True)  # Field name made lowercase.
    position = models.FloatField(db_column='Position', blank=True, null=True)  # Field name made lowercase.
    p_lrealized = models.FloatField(db_column='P&LRealized', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    p_lunrealized = models.FloatField(db_column='P&LUnrealized', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    p_l = models.FloatField(db_column='P&L', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    average_buy_price = models.FloatField(db_column='Average_Buy_Price', blank=True, null=True)  # Field name made lowercase.
    average_sell_price = models.FloatField(db_column='Average_Sell_Price', blank=True, null=True)  # Field name made lowercase.
    long_term_qty = models.FloatField(db_column='Long_Term_Qty', blank=True, null=True)  # Field name made lowercase.
    net = models.FloatField(db_column='Net', blank=True, null=True)  # Field name made lowercase.
    aum_eod = models.FloatField(db_column='AUM_EOD', blank=True, null=True)  # Field name made lowercase.
    aum_bod = models.FloatField(db_column='AUM_BOD', blank=True, null=True)  # Field name made lowercase.
    customer_name = models.TextField(blank=True, null=True)
    portfolio_name = models.TextField(blank=True, null=True)
    idhp = models.TextField(db_column='IDHP', blank=True, null=True)  # Field name made lowercase.
    divamount = models.FloatField(db_column='DivAmount', blank=True, null=True)  # Field name made lowercase.
    dividend_pnl = models.FloatField(db_column='Dividend_PNL', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'US_Daily_Positions'


class UsDailyStockReturns(models.Model):
    date = models.DateTimeField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    ticker = models.TextField(db_column='Ticker', blank=True, null=True)  # Field name made lowercase.
    company_name = models.TextField(db_column='Company_Name', blank=True, null=True)  # Field name made lowercase.
    currentprice = models.FloatField(db_column='CurrentPrice', blank=True, null=True)  # Field name made lowercase.
    dailychange = models.FloatField(db_column='DailyChange', blank=True, null=True)  # Field name made lowercase.
    dailypercentchange = models.FloatField(db_column='DailyPercentChange', blank=True, null=True)  # Field name made lowercase.
    weeklypricechange = models.FloatField(db_column='WeeklyPriceChange', blank=True, null=True)  # Field name made lowercase.
    weeklypricechangepercent = models.FloatField(db_column='WeeklyPriceChangePercent', blank=True, null=True)  # Field name made lowercase.
    monthlypricechange = models.FloatField(db_column='MonthlyPriceChange', blank=True, null=True)  # Field name made lowercase.
    monthlypricechangepercent = models.FloatField(db_column='MonthlyPriceChangePercent', blank=True, null=True)  # Field name made lowercase.
    gics_sector = models.TextField(db_column='GICS_Sector', blank=True, null=True)  # Field name made lowercase.
    gics_industry = models.TextField(db_column='GICS_Industry', blank=True, null=True)  # Field name made lowercase.
    gics_industry_group = models.TextField(db_column='GICS_Industry_Group', blank=True, null=True)  # Field name made lowercase.
    gics_subindustry_name = models.TextField(db_column='Gics_SubIndustry_Name', blank=True, null=True)  # Field name made lowercase.
    snp_500 = models.TextField(db_column='SnP_500', blank=True, null=True)  # Field name made lowercase.
    rut_1000 = models.TextField(db_column='RUT_1000', blank=True, null=True)  # Field name made lowercase.
    rut_2000 = models.TextField(db_column='RUT_2000', blank=True, null=True)  # Field name made lowercase.
    dow_30 = models.TextField(db_column='DOW_30', blank=True, null=True)  # Field name made lowercase.
    equalsize = models.FloatField(db_column='EqualSize', blank=True, null=True)  # Field name made lowercase.
    market_cap = models.FloatField(db_column='Market Cap', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    sales_rm_field = models.FloatField(db_column='Sales(RM)', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it ended with '_'.
    volatility_1y = models.FloatField(db_column='Volatility_1Y', blank=True, null=True)  # Field name made lowercase.
    beta_1y = models.FloatField(db_column='Beta_1Y', blank=True, null=True)  # Field name made lowercase.
    pe = models.FloatField(db_column='PE', blank=True, null=True)  # Field name made lowercase.
    pb = models.FloatField(db_column='PB', blank=True, null=True)  # Field name made lowercase.
    roe = models.FloatField(db_column='ROE', blank=True, null=True)  # Field name made lowercase.
    debt_equity = models.FloatField(db_column='Debt/Equity', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    rsi = models.FloatField(db_column='RSI', blank=True, null=True)  # Field name made lowercase.
    fs_ticker = models.TextField(db_column='FS_Ticker', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'US_Daily_Stock_Returns'


class UsEtfMonitor(models.Model):
    date = models.DateField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    security_code = models.TextField(db_column='Security_Code', blank=True, null=True)  # Field name made lowercase.
    fs_ticker = models.TextField(db_column='FS_Ticker', blank=True, null=True)  # Field name made lowercase.
    company_name = models.TextField(db_column='Company_Name', blank=True, null=True)  # Field name made lowercase.
    asset_class = models.TextField(db_column='Asset Class', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    region_country = models.TextField(db_column='Region/Country', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    category = models.TextField(db_column='Category', blank=True, null=True)  # Field name made lowercase.
    issuer = models.TextField(db_column='Issuer', blank=True, null=True)  # Field name made lowercase.
    adj_price_close = models.FloatField(db_column='Adj Price Close', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    number_15d = models.FloatField(db_column='15D', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    mtd = models.FloatField(db_column='MTD', blank=True, null=True)  # Field name made lowercase.
    qtd = models.FloatField(db_column='QTD', blank=True, null=True)  # Field name made lowercase.
    ytd = models.FloatField(db_column='YTD', blank=True, null=True)  # Field name made lowercase.
    number_1m = models.FloatField(db_column='1M', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_3m = models.FloatField(db_column='3M', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_6m = models.FloatField(db_column='6M', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_12m = models.FloatField(db_column='12M', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    ema9 = models.FloatField(db_column='EMA9', blank=True, null=True)  # Field name made lowercase.
    ema50 = models.FloatField(db_column='EMA50', blank=True, null=True)  # Field name made lowercase.
    ema200 = models.FloatField(db_column='EMA200', blank=True, null=True)  # Field name made lowercase.
    number_50ema_200ema = models.TextField(db_column='50EMA>200EMA', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_50ema_200ema_0 = models.TextField(db_column='50EMA<200EMA', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier. Field renamed because of name conflict.
    rsi_14d = models.FloatField(db_column='RSI_14D', blank=True, null=True)  # Field name made lowercase.
    adx = models.FloatField(db_column='ADX', blank=True, null=True)  # Field name made lowercase.
    rsi_14d_weekly = models.FloatField(db_column='RSI_14D_WEEKLY', blank=True, null=True)  # Field name made lowercase.
    adx_weekly = models.FloatField(db_column='ADX_WEEKLY', blank=True, null=True)  # Field name made lowercase.
    short_term = models.TextField(db_column='Short Term', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    intermediate_term = models.TextField(db_column='Intermediate Term', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    short_name = models.TextField(db_column='Short Name', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    id = models.BigIntegerField(blank=True, null=True)
    priority = models.CharField(db_column='Priority', max_length=5, blank=True, null=True)  # Field name made lowercase.
    type = models.TextField(db_column='Type', blank=True, null=True)  # Field name made lowercase.
    market_view_flag = models.CharField(db_column='Market_View_Flag', max_length=5, blank=True, null=True)  # Field name made lowercase.
    order = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'US_ETF_Monitor'


class UsEtfPrices(models.Model):
    date = models.DateTimeField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    open = models.FloatField(db_column='Open', blank=True, null=True)  # Field name made lowercase.
    high = models.FloatField(db_column='High', blank=True, null=True)  # Field name made lowercase.
    low = models.FloatField(db_column='Low', blank=True, null=True)  # Field name made lowercase.
    close = models.FloatField(db_column='Close', blank=True, null=True)  # Field name made lowercase.
    adjusted_close = models.FloatField(db_column='Adjusted_close', blank=True, null=True)  # Field name made lowercase.
    volume = models.BigIntegerField(db_column='Volume', blank=True, null=True)  # Field name made lowercase.
    ticker = models.TextField(db_column='Ticker', blank=True, null=True)  # Field name made lowercase.
    name = models.CharField(db_column='Name', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'US_ETF_Prices'


class UsExponentialMovingAverage(models.Model):
    company = models.TextField(db_column='Company', blank=True, null=True)  # Field name made lowercase.
    factset_ticker = models.TextField(db_column='Factset_Ticker', blank=True, null=True)  # Field name made lowercase.
    date = models.DateField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    price = models.FloatField(db_column='Price', blank=True, null=True)  # Field name made lowercase.
    ema9 = models.FloatField(db_column='EMA9', blank=True, null=True)  # Field name made lowercase.
    ema12 = models.FloatField(db_column='EMA12', blank=True, null=True)  # Field name made lowercase.
    ema = models.FloatField(db_column='EMA', blank=True, null=True)  # Field name made lowercase.
    ema26 = models.FloatField(db_column='EMA26', blank=True, null=True)  # Field name made lowercase.
    ema50 = models.FloatField(db_column='EMA50', blank=True, null=True)  # Field name made lowercase.
    ema200 = models.FloatField(db_column='EMA200', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'US_Exponential_Moving_Average'


class UsFactorLimits(models.Model):
    category = models.TextField(db_column='Category', blank=True, null=True)  # Field name made lowercase.
    factor = models.TextField(db_column='Factor', blank=True, null=True)  # Field name made lowercase.
    range1 = models.FloatField(db_column='Range1', blank=True, null=True)  # Field name made lowercase.
    range2 = models.FloatField(db_column='Range2', blank=True, null=True)  # Field name made lowercase.
    range3 = models.FloatField(db_column='Range3', blank=True, null=True)  # Field name made lowercase.
    range4 = models.FloatField(db_column='Range4', blank=True, null=True)  # Field name made lowercase.
    range5 = models.FloatField(db_column='Range5', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'US_Factor_Limits'


class UsFamaData(models.Model):
    date = models.CharField(db_column='Date', max_length=255, blank=True, null=True)  # Field name made lowercase.
    smb = models.FloatField(db_column='SMB', blank=True, null=True)  # Field name made lowercase.
    hml = models.FloatField(db_column='HML', blank=True, null=True)  # Field name made lowercase.
    wml = models.FloatField(db_column='WML', blank=True, null=True)  # Field name made lowercase.
    rm = models.FloatField(db_column='Rm', blank=True, null=True)  # Field name made lowercase.
    rf = models.FloatField(db_column='Rf', blank=True, null=True)  # Field name made lowercase.
    rm_rf = models.FloatField(db_column='Rm-Rf', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'US_Fama_Data'


class UsFibonacciPivotPoint(models.Model):
    date = models.DateField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    fs_ticker = models.TextField(db_column='FS_Ticker', blank=True, null=True)  # Field name made lowercase.
    company_name = models.TextField(db_column='Company_Name', blank=True, null=True)  # Field name made lowercase.
    open = models.FloatField(db_column='Open', blank=True, null=True)  # Field name made lowercase.
    high = models.FloatField(db_column='High', blank=True, null=True)  # Field name made lowercase.
    low = models.FloatField(db_column='Low', blank=True, null=True)  # Field name made lowercase.
    close = models.FloatField(db_column='Close', blank=True, null=True)  # Field name made lowercase.
    pp = models.FloatField(db_column='PP', blank=True, null=True)  # Field name made lowercase.
    s1 = models.FloatField(db_column='S1', blank=True, null=True)  # Field name made lowercase.
    s2 = models.FloatField(db_column='S2', blank=True, null=True)  # Field name made lowercase.
    s3 = models.FloatField(db_column='S3', blank=True, null=True)  # Field name made lowercase.
    r1 = models.FloatField(db_column='R1', blank=True, null=True)  # Field name made lowercase.
    r2 = models.FloatField(db_column='R2', blank=True, null=True)  # Field name made lowercase.
    r3 = models.FloatField(db_column='R3', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'US_Fibonacci_Pivot_Point'


class UsGicsClassification(models.Model):
    company = models.CharField(db_column='Company', max_length=255, blank=True, null=True)  # Field name made lowercase.
    tier = models.CharField(db_column='Tier', max_length=11)  # Field name made lowercase.
    fs_ticker = models.TextField(db_column='FS_Ticker', blank=True, null=True)  # Field name made lowercase.
    gics = models.CharField(db_column='GICS', max_length=255, blank=True, null=True)  # Field name made lowercase.
    description = models.TextField(db_column='Description', blank=True, null=True)  # Field name made lowercase. This field type is a guess.
    rating = models.CharField(db_column='Rating', max_length=255, blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'US_GICS_Classification'


class UsGainersLosers(models.Model):
    date = models.TextField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    price = models.FloatField(db_column='Price', blank=True, null=True)  # Field name made lowercase.
    return_field = models.FloatField(db_column='Return', blank=True, null=True)  # Field name made lowercase. Field renamed because it was a Python reserved word.
    factset_ticker = models.TextField(db_column='Factset_Ticker', blank=True, null=True)  # Field name made lowercase.
    type = models.TextField(db_column='Type', blank=True, null=True)  # Field name made lowercase.
    companytype = models.TextField(db_column='CompanyType', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'US_Gainers_Losers'


class UsHistoricalMultiples(models.Model):
    date = models.DateField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    factor = models.TextField(db_column='Factor', blank=True, null=True)  # Field name made lowercase.
    ticker = models.TextField(db_column='Ticker', blank=True, null=True)  # Field name made lowercase.
    value = models.FloatField(blank=True, null=True)
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'US_Historical_Multiples'


class UsHistoricalMultiplesOld(models.Model):
    date = models.DateField(db_column='Date')  # Field name made lowercase.
    factor = models.CharField(db_column='Factor', max_length=50)  # Field name made lowercase.
    t_us = models.FloatField(db_column='T-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ctl_us = models.FloatField(db_column='CTL-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    vz_us = models.FloatField(db_column='VZ-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ttwo_us = models.FloatField(db_column='TTWO-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    lyv_us = models.FloatField(db_column='LYV-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ea_us = models.FloatField(db_column='EA-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    atvi_us = models.FloatField(db_column='ATVI-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    dis_us = models.FloatField(db_column='DIS-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    nflx_us = models.FloatField(db_column='NFLX-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    googl_us = models.FloatField(db_column='GOOGL-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    goog_us = models.FloatField(db_column='GOOG-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    fb_us = models.FloatField(db_column='FB-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    twtr_us = models.FloatField(db_column='TWTR-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    omc_us = models.FloatField(db_column='OMC-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    cmcsa_us = models.FloatField(db_column='CMCSA-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    disca_us = models.FloatField(db_column='DISCA-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    chtr_us = models.FloatField(db_column='CHTR-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    disck_us = models.FloatField(db_column='DISCK-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ipg_us = models.FloatField(db_column='IPG-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    dish_us = models.FloatField(db_column='DISH-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    nws_us = models.FloatField(db_column='NWS-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    viac_us = models.FloatField(db_column='VIAC-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    fox_us = models.FloatField(db_column='FOX-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    nwsa_us = models.FloatField(db_column='NWSA-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    foxa_us = models.FloatField(db_column='FOXA-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    tmus_us = models.FloatField(db_column='TMUS-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    aptv_us = models.FloatField(db_column='APTV-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    bwa_us = models.FloatField(db_column='BWA-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    gm_us = models.FloatField(db_column='GM-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    f_us = models.FloatField(db_column='F-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    hog_us = models.FloatField(db_column='HOG-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    lkq_us = models.FloatField(db_column='LKQ-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    gpc_us = models.FloatField(db_column='GPC-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    hrb_us = models.FloatField(db_column='HRB-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    cmg_us = models.FloatField(db_column='CMG-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    mgm_us = models.FloatField(db_column='MGM-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    yum_us = models.FloatField(db_column='YUM-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ccl_us = models.FloatField(db_column='CCL-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    hlt_us = models.FloatField(db_column='HLT-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    sbux_us = models.FloatField(db_column='SBUX-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    rcl_us = models.FloatField(db_column='RCL-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    mcd_us = models.FloatField(db_column='MCD-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    lvs_us = models.FloatField(db_column='LVS-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    dri_us = models.FloatField(db_column='DRI-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    mar_us = models.FloatField(db_column='MAR-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    wynn_us = models.FloatField(db_column='WYNN-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    nclh_us = models.FloatField(db_column='NCLH-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    phm_us = models.FloatField(db_column='PHM-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    whr_us = models.FloatField(db_column='WHR-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    nvr_us = models.FloatField(db_column='NVR-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    mhk_us = models.FloatField(db_column='MHK-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    dhi_us = models.FloatField(db_column='DHI-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    len_us = models.FloatField(db_column='LEN-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    nwl_us = models.FloatField(db_column='NWL-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    grmn_us = models.FloatField(db_column='GRMN-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    leg_us = models.FloatField(db_column='LEG-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    amzn_us = models.FloatField(db_column='AMZN-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ebay_us = models.FloatField(db_column='EBAY-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    expe_us = models.FloatField(db_column='EXPE-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    bkng_us = models.FloatField(db_column='BKNG-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    has_us = models.FloatField(db_column='HAS-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    dltr_us = models.FloatField(db_column='DLTR-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    kss_us = models.FloatField(db_column='KSS-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    jwn_us = models.FloatField(db_column='JWN-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    dg_us = models.FloatField(db_column='DG-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    m_us = models.FloatField(db_column='M-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    tgt_us = models.FloatField(db_column='TGT-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    low_us = models.FloatField(db_column='LOW-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    hd_us = models.FloatField(db_column='HD-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    lb_us = models.FloatField(db_column='LB-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    aap_us = models.FloatField(db_column='AAP-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    tjx_us = models.FloatField(db_column='TJX-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    orly_us = models.FloatField(db_column='ORLY-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    gps_us = models.FloatField(db_column='GPS-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    kmx_us = models.FloatField(db_column='KMX-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    tif_us = models.FloatField(db_column='TIF-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ulta_us = models.FloatField(db_column='ULTA-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    bby_us = models.FloatField(db_column='BBY-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    tsco_us = models.FloatField(db_column='TSCO-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    azo_us = models.FloatField(db_column='AZO-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    rost_us = models.FloatField(db_column='ROST-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ua_us = models.FloatField(db_column='UA-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    nke_us = models.FloatField(db_column='NKE-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    vfc_us = models.FloatField(db_column='VFC-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    uaa_us = models.FloatField(db_column='UAA-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    tpr_us = models.FloatField(db_column='TPR-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    hbi_us = models.FloatField(db_column='HBI-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    pvh_us = models.FloatField(db_column='PVH-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    rl_us = models.FloatField(db_column='RL-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    cpri_us = models.FloatField(db_column='CPRI-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    bf_b_us = models.FloatField(db_column='BF.B-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    stz_us = models.FloatField(db_column='STZ-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ko_us = models.FloatField(db_column='KO-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    pep_us = models.FloatField(db_column='PEP-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    tap_us = models.FloatField(db_column='TAP-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    mnst_us = models.FloatField(db_column='MNST-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    syy_us = models.FloatField(db_column='SYY-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    kr_us = models.FloatField(db_column='KR-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    wmt_us = models.FloatField(db_column='WMT-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    cost_us = models.FloatField(db_column='COST-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    wba_us = models.FloatField(db_column='WBA-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    mdlz_us = models.FloatField(db_column='MDLZ-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    khc_us = models.FloatField(db_column='KHC-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    hrl_us = models.FloatField(db_column='HRL-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    adm_us = models.FloatField(db_column='ADM-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    sjm_us = models.FloatField(db_column='SJM-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    cag_us = models.FloatField(db_column='CAG-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    gis_us = models.FloatField(db_column='GIS-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    tsn_us = models.FloatField(db_column='TSN-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    cpb_us = models.FloatField(db_column='CPB-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    hsy_us = models.FloatField(db_column='HSY-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    k_us = models.FloatField(db_column='K-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    lw_us = models.FloatField(db_column='LW-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    mkc_us = models.FloatField(db_column='MKC-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    chd_us = models.FloatField(db_column='CHD-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    clx_us = models.FloatField(db_column='CLX-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    cl_us = models.FloatField(db_column='CL-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    pg_us = models.FloatField(db_column='PG-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    kmb_us = models.FloatField(db_column='KMB-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    coty_us = models.FloatField(db_column='COTY-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    el_us = models.FloatField(db_column='EL-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    mo_us = models.FloatField(db_column='MO-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    pm_us = models.FloatField(db_column='PM-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    hal_us = models.FloatField(db_column='HAL-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    hp_us = models.FloatField(db_column='HP-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    fti_us = models.FloatField(db_column='FTI-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    slb_us = models.FloatField(db_column='SLB-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    nov_us = models.FloatField(db_column='NOV-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    bkr_us = models.FloatField(db_column='BKR-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    hes_us = models.FloatField(db_column='HES-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    hfc_us = models.FloatField(db_column='HFC-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    apa_us = models.FloatField(db_column='APA-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    fang_us = models.FloatField(db_column='FANG-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    dvn_us = models.FloatField(db_column='DVN-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    mpc_us = models.FloatField(db_column='MPC-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    xom_us = models.FloatField(db_column='XOM-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    cog_us = models.FloatField(db_column='COG-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    nbl_us = models.FloatField(db_column='NBL-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    vlo_us = models.FloatField(db_column='VLO-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    oxy_us = models.FloatField(db_column='OXY-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    cxo_us = models.FloatField(db_column='CXO-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    cop_us = models.FloatField(db_column='COP-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    pxd_us = models.FloatField(db_column='PXD-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    wmb_us = models.FloatField(db_column='WMB-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    psx_us = models.FloatField(db_column='PSX-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    oke_us = models.FloatField(db_column='OKE-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    eog_us = models.FloatField(db_column='EOG-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    cvx_us = models.FloatField(db_column='CVX-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    kmi_us = models.FloatField(db_column='KMI-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    mro_us = models.FloatField(db_column='MRO-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    c_us = models.FloatField(db_column='C-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    wfc_us = models.FloatField(db_column='WFC-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    rf_us = models.FloatField(db_column='RF-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    sivb_us = models.FloatField(db_column='SIVB-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    frc_us = models.FloatField(db_column='FRC-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    fitb_us = models.FloatField(db_column='FITB-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    mtb_us = models.FloatField(db_column='MTB-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    jpm_us = models.FloatField(db_column='JPM-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    pnc_us = models.FloatField(db_column='PNC-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    hban_us = models.FloatField(db_column='HBAN-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    bac_us = models.FloatField(db_column='BAC-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    key_us = models.FloatField(db_column='KEY-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    pbct_us = models.FloatField(db_column='PBCT-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    tfc_us = models.FloatField(db_column='TFC-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    usb_us = models.FloatField(db_column='USB-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    cfg_us = models.FloatField(db_column='CFG-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    zion_us = models.FloatField(db_column='ZION-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    cma_us = models.FloatField(db_column='CMA-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    amp_us = models.FloatField(db_column='AMP-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    schw_us = models.FloatField(db_column='SCHW-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ms_us = models.FloatField(db_column='MS-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ndaq_us = models.FloatField(db_column='NDAQ-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    etfc_us = models.FloatField(db_column='ETFC-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    stt_us = models.FloatField(db_column='STT-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    cboe_us = models.FloatField(db_column='CBOE-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    cme_us = models.FloatField(db_column='CME-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ivz_us = models.FloatField(db_column='IVZ-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    spgi_us = models.FloatField(db_column='SPGI-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    rjf_us = models.FloatField(db_column='RJF-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    mktx_us = models.FloatField(db_column='MKTX-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    msci_us = models.FloatField(db_column='MSCI-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    trow_us = models.FloatField(db_column='TROW-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    bk_us = models.FloatField(db_column='BK-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ice_us = models.FloatField(db_column='ICE-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ben_us = models.FloatField(db_column='BEN-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    mco_us = models.FloatField(db_column='MCO-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    gs_us = models.FloatField(db_column='GS-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    blk_us = models.FloatField(db_column='BLK-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ntrs_us = models.FloatField(db_column='NTRS-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    syf_us = models.FloatField(db_column='SYF-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    cof_us = models.FloatField(db_column='COF-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    dfs_us = models.FloatField(db_column='DFS-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    axp_us = models.FloatField(db_column='AXP-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    brk_b_us = models.FloatField(db_column='BRK.B-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    gl_us = models.FloatField(db_column='GL-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    aiz_us = models.FloatField(db_column='AIZ-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    cb_us = models.FloatField(db_column='CB-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    l_us = models.FloatField(db_column='L-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    aon_us = models.FloatField(db_column='AON-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    mmc_us = models.FloatField(db_column='MMC-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    afl_us = models.FloatField(db_column='AFL-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    met_us = models.FloatField(db_column='MET-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    cinf_us = models.FloatField(db_column='CINF-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ajg_us = models.FloatField(db_column='AJG-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    pru_us = models.FloatField(db_column='PRU-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    wltw_us = models.FloatField(db_column='WLTW-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    all_us = models.FloatField(db_column='ALL-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    trv_us = models.FloatField(db_column='TRV-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    aig_us = models.FloatField(db_column='AIG-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    pgr_us = models.FloatField(db_column='PGR-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    unm_us = models.FloatField(db_column='UNM-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    pfg_us = models.FloatField(db_column='PFG-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    wrb_us = models.FloatField(db_column='WRB-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    lnc_us = models.FloatField(db_column='LNC-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    hig_us = models.FloatField(db_column='HIG-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    re_us = models.FloatField(db_column='RE-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    regn_us = models.FloatField(db_column='REGN-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    incy_us = models.FloatField(db_column='INCY-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    vrtx_us = models.FloatField(db_column='VRTX-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    biib_us = models.FloatField(db_column='BIIB-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    gild_us = models.FloatField(db_column='GILD-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    abbv_us = models.FloatField(db_column='ABBV-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    amgn_us = models.FloatField(db_column='AMGN-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    alxn_us = models.FloatField(db_column='ALXN-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    xray_us = models.FloatField(db_column='XRAY-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    holx_us = models.FloatField(db_column='HOLX-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    isrg_us = models.FloatField(db_column='ISRG-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    abt_us = models.FloatField(db_column='ABT-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    var_us = models.FloatField(db_column='VAR-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    idxx_us = models.FloatField(db_column='IDXX-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ste_us = models.FloatField(db_column='STE-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    bdx_us = models.FloatField(db_column='BDX-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    abmd_us = models.FloatField(db_column='ABMD-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    syk_us = models.FloatField(db_column='SYK-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    zbh_us = models.FloatField(db_column='ZBH-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    mdt_us = models.FloatField(db_column='MDT-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    tfx_us = models.FloatField(db_column='TFX-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    coo_us = models.FloatField(db_column='COO-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    rmd_us = models.FloatField(db_column='RMD-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ew_us = models.FloatField(db_column='EW-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    algn_us = models.FloatField(db_column='ALGN-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    bax_us = models.FloatField(db_column='BAX-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    dhr_us = models.FloatField(db_column='DHR-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    bsx_us = models.FloatField(db_column='BSX-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    dgx_us = models.FloatField(db_column='DGX-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    unh_us = models.FloatField(db_column='UNH-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    lh_us = models.FloatField(db_column='LH-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    cah_us = models.FloatField(db_column='CAH-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    cnc_us = models.FloatField(db_column='CNC-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    hca_us = models.FloatField(db_column='HCA-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    abc_us = models.FloatField(db_column='ABC-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    antm_us = models.FloatField(db_column='ANTM-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    mck_us = models.FloatField(db_column='MCK-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    cvs_us = models.FloatField(db_column='CVS-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    dva_us = models.FloatField(db_column='DVA-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    hum_us = models.FloatField(db_column='HUM-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ci_us = models.FloatField(db_column='CI-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    uhs_us = models.FloatField(db_column='UHS-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    hsic_us = models.FloatField(db_column='HSIC-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    cern_us = models.FloatField(db_column='CERN-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    a_us = models.FloatField(db_column='A-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    mtd_us = models.FloatField(db_column='MTD-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    iqv_us = models.FloatField(db_column='IQV-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    pki_us = models.FloatField(db_column='PKI-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ilmn_us = models.FloatField(db_column='ILMN-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    wat_us = models.FloatField(db_column='WAT-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    tmo_us = models.FloatField(db_column='TMO-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    myl_us = models.FloatField(db_column='MYL-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    agn_us = models.FloatField(db_column='AGN-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    zts_us = models.FloatField(db_column='ZTS-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    bmy_us = models.FloatField(db_column='BMY-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    jnj_us = models.FloatField(db_column='JNJ-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    pfe_us = models.FloatField(db_column='PFE-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    prgo_us = models.FloatField(db_column='PRGO-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    lly_us = models.FloatField(db_column='LLY-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    mrk_us = models.FloatField(db_column='MRK-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    tdg_us = models.FloatField(db_column='TDG-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ba_us = models.FloatField(db_column='BA-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    gd_us = models.FloatField(db_column='GD-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    rtn_us = models.FloatField(db_column='RTN-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    utx_us = models.FloatField(db_column='UTX-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    arnc_us = models.FloatField(db_column='ARNC-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    lmt_us = models.FloatField(db_column='LMT-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    lhx_us = models.FloatField(db_column='LHX-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    hii_us = models.FloatField(db_column='HII-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    txt_us = models.FloatField(db_column='TXT-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    noc_us = models.FloatField(db_column='NOC-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    fdx_us = models.FloatField(db_column='FDX-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ups_us = models.FloatField(db_column='UPS-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    expd_us = models.FloatField(db_column='EXPD-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    chrw_us = models.FloatField(db_column='CHRW-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    aal_us = models.FloatField(db_column='AAL-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    dal_us = models.FloatField(db_column='DAL-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ual_us = models.FloatField(db_column='UAL-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    luv_us = models.FloatField(db_column='LUV-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    alk_us = models.FloatField(db_column='ALK-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    mas_us = models.FloatField(db_column='MAS-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    alle_us = models.FloatField(db_column='ALLE-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    tt_us = models.FloatField(db_column='TT-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    aos_us = models.FloatField(db_column='AOS-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    jci_us = models.FloatField(db_column='JCI-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    fbhs_us = models.FloatField(db_column='FBHS-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    rsg_us = models.FloatField(db_column='RSG-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    rol_us = models.FloatField(db_column='ROL-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    cprt_us = models.FloatField(db_column='CPRT-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ctas_us = models.FloatField(db_column='CTAS-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    wm_us = models.FloatField(db_column='WM-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    j_us = models.FloatField(db_column='J-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    pwr_us = models.FloatField(db_column='PWR-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    emr_us = models.FloatField(db_column='EMR-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ame_us = models.FloatField(db_column='AME-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    rok_us = models.FloatField(db_column='ROK-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    etn_us = models.FloatField(db_column='ETN-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    rop_us = models.FloatField(db_column='ROP-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ge_us = models.FloatField(db_column='GE-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    mmm_us = models.FloatField(db_column='MMM-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    hon_us = models.FloatField(db_column='HON-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    cmi_us = models.FloatField(db_column='CMI-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ftv_us = models.FloatField(db_column='FTV-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    xyl_us = models.FloatField(db_column='XYL-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    pnr_us = models.FloatField(db_column='PNR-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    itw_us = models.FloatField(db_column='ITW-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    iex_us = models.FloatField(db_column='IEX-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ir_us = models.FloatField(db_column='IR-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    swk_us = models.FloatField(db_column='SWK-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    fls_us = models.FloatField(db_column='FLS-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    pcar_us = models.FloatField(db_column='PCAR-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    de_us = models.FloatField(db_column='DE-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    wab_us = models.FloatField(db_column='WAB-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ph_us = models.FloatField(db_column='PH-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    sna_us = models.FloatField(db_column='SNA-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    dov_us = models.FloatField(db_column='DOV-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    cat_us = models.FloatField(db_column='CAT-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    info_us = models.FloatField(db_column='INFO-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    rhi_us = models.FloatField(db_column='RHI-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    vrsk_us = models.FloatField(db_column='VRSK-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    nlsn_us = models.FloatField(db_column='NLSN-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    efx_us = models.FloatField(db_column='EFX-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ksu_us = models.FloatField(db_column='KSU-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    unp_us = models.FloatField(db_column='UNP-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    csx_us = models.FloatField(db_column='CSX-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    nsc_us = models.FloatField(db_column='NSC-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    jbht_us = models.FloatField(db_column='JBHT-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    odfl_us = models.FloatField(db_column='ODFL-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    uri_us = models.FloatField(db_column='URI-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    fast_us = models.FloatField(db_column='FAST-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    gww_us = models.FloatField(db_column='GWW-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    jnpr_us = models.FloatField(db_column='JNPR-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    anet_us = models.FloatField(db_column='ANET-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ffiv_us = models.FloatField(db_column='FFIV-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    csco_us = models.FloatField(db_column='CSCO-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    msi_us = models.FloatField(db_column='MSI-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ipgp_us = models.FloatField(db_column='IPGP-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    cdw_us = models.FloatField(db_column='CDW-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    flir_us = models.FloatField(db_column='FLIR-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    glw_us = models.FloatField(db_column='GLW-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    aph_us = models.FloatField(db_column='APH-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    tel_us = models.FloatField(db_column='TEL-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    keys_us = models.FloatField(db_column='KEYS-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    zbra_us = models.FloatField(db_column='ZBRA-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    pypl_us = models.FloatField(db_column='PYPL-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    vrsn_us = models.FloatField(db_column='VRSN-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    wu_us = models.FloatField(db_column='WU-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ldos_us = models.FloatField(db_column='LDOS-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    akam_us = models.FloatField(db_column='AKAM-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    br_us = models.FloatField(db_column='BR-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    v_us = models.FloatField(db_column='V-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    it_us = models.FloatField(db_column='IT-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ads_us = models.FloatField(db_column='ADS-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ibm_us = models.FloatField(db_column='IBM-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    jkhy_us = models.FloatField(db_column='JKHY-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    flt_us = models.FloatField(db_column='FLT-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ma_us = models.FloatField(db_column='MA-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    dxc_us = models.FloatField(db_column='DXC-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    adp_us = models.FloatField(db_column='ADP-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    fisv_us = models.FloatField(db_column='FISV-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    acn_us = models.FloatField(db_column='ACN-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    payx_us = models.FloatField(db_column='PAYX-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    fis_us = models.FloatField(db_column='FIS-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    gpn_us = models.FloatField(db_column='GPN-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ctsh_us = models.FloatField(db_column='CTSH-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    amd_us = models.FloatField(db_column='AMD-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    klac_us = models.FloatField(db_column='KLAC-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    xlnx_us = models.FloatField(db_column='XLNX-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    nvda_us = models.FloatField(db_column='NVDA-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    txn_us = models.FloatField(db_column='TXN-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    lrcx_us = models.FloatField(db_column='LRCX-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    avgo_us = models.FloatField(db_column='AVGO-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    qcom_us = models.FloatField(db_column='QCOM-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    mchp_us = models.FloatField(db_column='MCHP-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    mu_us = models.FloatField(db_column='MU-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    adi_us = models.FloatField(db_column='ADI-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    intc_us = models.FloatField(db_column='INTC-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    qrvo_us = models.FloatField(db_column='QRVO-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    amat_us = models.FloatField(db_column='AMAT-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    swks_us = models.FloatField(db_column='SWKS-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    mxim_us = models.FloatField(db_column='MXIM-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    payc_us = models.FloatField(db_column='PAYC-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ctxs_us = models.FloatField(db_column='CTXS-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    intu_us = models.FloatField(db_column='INTU-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    nlok_us = models.FloatField(db_column='NLOK-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    anss_us = models.FloatField(db_column='ANSS-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    msft_us = models.FloatField(db_column='MSFT-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    now_us = models.FloatField(db_column='NOW-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    cdns_us = models.FloatField(db_column='CDNS-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    crm_us = models.FloatField(db_column='CRM-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    adsk_us = models.FloatField(db_column='ADSK-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    snps_us = models.FloatField(db_column='SNPS-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    adbe_us = models.FloatField(db_column='ADBE-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ftnt_us = models.FloatField(db_column='FTNT-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    orcl_us = models.FloatField(db_column='ORCL-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    stx_us = models.FloatField(db_column='STX-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    aapl_us = models.FloatField(db_column='AAPL-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    xrx_us = models.FloatField(db_column='XRX-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ntap_us = models.FloatField(db_column='NTAP-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    hpq_us = models.FloatField(db_column='HPQ-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    hpe_us = models.FloatField(db_column='HPE-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    wdc_us = models.FloatField(db_column='WDC-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    lin_us = models.FloatField(db_column='LIN-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    shw_us = models.FloatField(db_column='SHW-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    dow_us = models.FloatField(db_column='DOW-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ctva_us = models.FloatField(db_column='CTVA-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    mos_us = models.FloatField(db_column='MOS-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    lyb_us = models.FloatField(db_column='LYB-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    cf_us = models.FloatField(db_column='CF-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    dd_us = models.FloatField(db_column='DD-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    fmc_us = models.FloatField(db_column='FMC-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    iff_us = models.FloatField(db_column='IFF-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    apd_us = models.FloatField(db_column='APD-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ecl_us = models.FloatField(db_column='ECL-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    emn_us = models.FloatField(db_column='EMN-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ppg_us = models.FloatField(db_column='PPG-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    alb_us = models.FloatField(db_column='ALB-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ce_us = models.FloatField(db_column='CE-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    vmc_us = models.FloatField(db_column='VMC-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    mlm_us = models.FloatField(db_column='MLM-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    see_us = models.FloatField(db_column='SEE-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    wrk_us = models.FloatField(db_column='WRK-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    bll_us = models.FloatField(db_column='BLL-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ip_us = models.FloatField(db_column='IP-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    amcr_us = models.FloatField(db_column='AMCR-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    avy_us = models.FloatField(db_column='AVY-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    pkg_us = models.FloatField(db_column='PKG-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    nem_us = models.FloatField(db_column='NEM-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    fcx_us = models.FloatField(db_column='FCX-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    nue_us = models.FloatField(db_column='NUE-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    o_us = models.FloatField(db_column='O-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    pld_us = models.FloatField(db_column='PLD-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    reg_us = models.FloatField(db_column='REG-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    spg_us = models.FloatField(db_column='SPG-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    sbac_us = models.FloatField(db_column='SBAC-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    eqr_us = models.FloatField(db_column='EQR-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    vno_us = models.FloatField(db_column='VNO-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    dre_us = models.FloatField(db_column='DRE-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    maa_us = models.FloatField(db_column='MAA-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    avb_us = models.FloatField(db_column='AVB-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    frt_us = models.FloatField(db_column='FRT-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    eqix_us = models.FloatField(db_column='EQIX-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    irm_us = models.FloatField(db_column='IRM-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    bxp_us = models.FloatField(db_column='BXP-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    aiv_us = models.FloatField(db_column='AIV-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    slg_us = models.FloatField(db_column='SLG-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    are_us = models.FloatField(db_column='ARE-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    hst_us = models.FloatField(db_column='HST-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    peak_us = models.FloatField(db_column='PEAK-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    wy_us = models.FloatField(db_column='WY-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    well_us = models.FloatField(db_column='WELL-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    psa_us = models.FloatField(db_column='PSA-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    exr_us = models.FloatField(db_column='EXR-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    cci_us = models.FloatField(db_column='CCI-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ess_us = models.FloatField(db_column='ESS-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    kim_us = models.FloatField(db_column='KIM-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    vtr_us = models.FloatField(db_column='VTR-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    dlr_us = models.FloatField(db_column='DLR-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    amt_us = models.FloatField(db_column='AMT-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    udr_us = models.FloatField(db_column='UDR-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    cbre_us = models.FloatField(db_column='CBRE-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    evrg_us = models.FloatField(db_column='EVRG-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    xel_us = models.FloatField(db_column='XEL-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    so_us = models.FloatField(db_column='SO-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    eix_us = models.FloatField(db_column='EIX-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ppl_us = models.FloatField(db_column='PPL-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    pnw_us = models.FloatField(db_column='PNW-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    lnt_us = models.FloatField(db_column='LNT-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    aep_us = models.FloatField(db_column='AEP-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    exc_us = models.FloatField(db_column='EXC-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    etr_us = models.FloatField(db_column='ETR-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    duk_us = models.FloatField(db_column='DUK-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    es_us = models.FloatField(db_column='ES-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    nrg_us = models.FloatField(db_column='NRG-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    nee_us = models.FloatField(db_column='NEE-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    fe_us = models.FloatField(db_column='FE-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ato_us = models.FloatField(db_column='ATO-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    aes_us = models.FloatField(db_column='AES-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    peg_us = models.FloatField(db_column='PEG-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    dte_us = models.FloatField(db_column='DTE-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ni_us = models.FloatField(db_column='NI-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    aee_us = models.FloatField(db_column='AEE-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ed_us = models.FloatField(db_column='ED-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    wec_us = models.FloatField(db_column='WEC-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    sre_us = models.FloatField(db_column='SRE-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    cms_us = models.FloatField(db_column='CMS-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    d_us = models.FloatField(db_column='D-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    cnp_us = models.FloatField(db_column='CNP-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    awk_us = models.FloatField(db_column='AWK-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'US_Historical_Multiples_Old'


class UsIndexconstituents(models.Model):
    index_code = models.FloatField(blank=True, null=True)
    index_name = models.TextField(blank=True, null=True)
    index_ticker = models.CharField(max_length=28)
    co_code = models.FloatField(blank=True, null=True)
    fs_ticker = models.TextField(db_column='FS_Ticker', blank=True, null=True)  # Field name made lowercase.
    lname = models.TextField(blank=True, null=True)
    flag = models.TextField(db_column='Flag', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'US_IndexConstituents'


class UsIndexconstituentsDonotuse(models.Model):
    code = models.TextField(db_column='Code', blank=True, null=True)  # Field name made lowercase.
    exchange = models.TextField(db_column='Exchange', blank=True, null=True)  # Field name made lowercase.
    name = models.TextField(db_column='Name', blank=True, null=True)  # Field name made lowercase.
    sector = models.TextField(db_column='Sector', blank=True, null=True)  # Field name made lowercase.
    industry = models.TextField(db_column='Industry', blank=True, null=True)  # Field name made lowercase.
    index_name = models.TextField(db_column='Index_Name', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'US_IndexConstituents_donotuse'


class UsIndexPerformance(models.Model):
    ticker = models.TextField(db_column='Ticker', blank=True, null=True)  # Field name made lowercase.
    period = models.TextField(db_column='Period', blank=True, null=True)  # Field name made lowercase.
    return_field = models.FloatField(db_column='Return', blank=True, null=True)  # Field name made lowercase. Field renamed because it was a Python reserved word.
    downloaddate = models.DateField(db_column='downloadDate', blank=True, null=True)  # Field name made lowercase.
    id = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'US_Index_Performance'


class UsIndexSectorBreakdown(models.Model):
    sector = models.CharField(db_column='Sector', max_length=255, blank=True, null=True)  # Field name made lowercase.
    breakdown = models.FloatField(db_column='Breakdown', blank=True, null=True)  # Field name made lowercase.
    date = models.DateField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'US_Index_Sector_Breakdown'


class UsIndicesCorrelation(models.Model):
    company = models.TextField(db_column='Company', blank=True, null=True)  # Field name made lowercase.
    s_p_500 = models.FloatField(db_column='S&P 500', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    russell_2000 = models.FloatField(db_column='Russell 2000', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    dow_jones_industrial_average = models.FloatField(db_column='Dow Jones Industrial Average', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    xlb_us = models.FloatField(db_column='XLB-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    xlc_us = models.FloatField(db_column='XLC-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    xle_us = models.FloatField(db_column='XLE-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    xlf_us = models.FloatField(db_column='XLF-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    xli_us = models.FloatField(db_column='XLI-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    xlk_us = models.FloatField(db_column='XLK-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    xlp_us = models.FloatField(db_column='XLP-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    xlre_us = models.FloatField(db_column='XLRE-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    xlu_us = models.FloatField(db_column='XLU-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    xlv_us = models.FloatField(db_column='XLV-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    xly_us = models.FloatField(db_column='XLY-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    date = models.DateField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'US_Indices_Correlation'


class UsInputAiml(models.Model):
    date = models.DateTimeField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    open = models.FloatField(db_column='Open', blank=True, null=True)  # Field name made lowercase.
    high = models.FloatField(db_column='High', blank=True, null=True)  # Field name made lowercase.
    low = models.FloatField(db_column='Low', blank=True, null=True)  # Field name made lowercase.
    close = models.FloatField(db_column='Close', blank=True, null=True)  # Field name made lowercase.
    adjusted_close = models.FloatField(db_column='Adjusted_close', blank=True, null=True)  # Field name made lowercase.
    volume = models.BigIntegerField(db_column='Volume', blank=True, null=True)  # Field name made lowercase.
    ticker = models.TextField(db_column='Ticker', blank=True, null=True)  # Field name made lowercase.
    name = models.TextField(db_column='Name', blank=True, null=True)  # Field name made lowercase.
    return_field = models.FloatField(db_column='Return', blank=True, null=True)  # Field name made lowercase. Field renamed because it was a Python reserved word.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'US_Input_AIML'


class UsInsightsDailyTemp(models.Model):
    date = models.DateTimeField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    ticker = models.TextField(db_column='Ticker', blank=True, null=True)  # Field name made lowercase.
    price = models.FloatField(db_column='Price', blank=True, null=True)  # Field name made lowercase.
    return_field = models.FloatField(db_column='Return', blank=True, null=True)  # Field name made lowercase. Field renamed because it was a Python reserved word.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'US_Insights_Daily_temp'


class UsMacroData(models.Model):
    date = models.DateField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    oil_field = models.FloatField(db_column='Oil ', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it ended with '_'.
    fx_rate = models.FloatField(db_column='FX_Rate', blank=True, null=True)  # Field name made lowercase.
    interest_rate = models.FloatField(db_column='Interest_Rate', blank=True, null=True)  # Field name made lowercase.
    vix = models.FloatField(db_column='VIX', blank=True, null=True)  # Field name made lowercase.
    inflation_rate = models.FloatField(db_column='Inflation_Rate', blank=True, null=True)  # Field name made lowercase.
    gold = models.FloatField(db_column='Gold', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'US_Macro_Data'


class UsMarketByte(models.Model):
    ticker = models.TextField(db_column='Ticker', blank=True, null=True)  # Field name made lowercase.
    currentprice = models.FloatField(db_column='CurrentPrice', blank=True, null=True)  # Field name made lowercase.
    dailychange = models.FloatField(db_column='dailyChange', blank=True, null=True)  # Field name made lowercase.
    dailypercentchange = models.FloatField(db_column='dailyPercentChange', blank=True, null=True)  # Field name made lowercase.
    weekly_price_change = models.FloatField(blank=True, null=True)
    weekly_price_change_percent = models.FloatField(blank=True, null=True)
    monthly_price_change = models.FloatField(blank=True, null=True)
    monthly_price_change_percent = models.FloatField(blank=True, null=True)
    yearly_price_change = models.FloatField(blank=True, null=True)
    yearly_price_change_percent = models.FloatField(blank=True, null=True)
    date = models.DateTimeField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    country = models.TextField(db_column='Country', blank=True, null=True)  # Field name made lowercase.
    order = models.IntegerField(db_column='Order', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'US_Market_Byte'


class UsMarketIndex(models.Model):
    company = models.TextField(db_column='Company', blank=True, null=True)  # Field name made lowercase.
    factset_ticker = models.TextField(db_column='Factset_Ticker', blank=True, null=True)  # Field name made lowercase.
    date = models.DateField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    price = models.FloatField(db_column='Price', blank=True, null=True)  # Field name made lowercase.
    return_field = models.FloatField(db_column='Return', blank=True, null=True)  # Field name made lowercase. Field renamed because it was a Python reserved word.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'US_Market_Index'


class UsMarketInsights(models.Model):
    date = models.DateTimeField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    company = models.TextField(db_column='Company', blank=True, null=True)  # Field name made lowercase.
    price = models.FloatField(db_column='Price', blank=True, null=True)  # Field name made lowercase.
    return_field = models.FloatField(db_column='Return', blank=True, null=True)  # Field name made lowercase. Field renamed because it was a Python reserved word.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'US_Market_Insights'


class UsMarketVixInput(models.Model):
    date = models.DateTimeField(blank=True, null=True)
    vix = models.FloatField(blank=True, null=True)
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'US_Market_VIX_Input'


class UsMovingAverage(models.Model):
    date = models.DateField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    company = models.TextField(db_column='Company')  # Field name made lowercase.
    price = models.FloatField(db_column='Price', blank=True, null=True)  # Field name made lowercase.
    factset_ticker = models.TextField(db_column='Factset_Ticker', blank=True, null=True)  # Field name made lowercase.
    ma9 = models.FloatField(db_column='MA9', blank=True, null=True)  # Field name made lowercase.
    ma20 = models.FloatField(db_column='MA20', blank=True, null=True)  # Field name made lowercase.
    ma26 = models.FloatField(db_column='MA26', blank=True, null=True)  # Field name made lowercase.
    ma50 = models.FloatField(db_column='MA50', blank=True, null=True)  # Field name made lowercase.
    ma100 = models.FloatField(db_column='MA100', blank=True, null=True)  # Field name made lowercase.
    ma200 = models.FloatField(db_column='MA200', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'US_Moving_Average'


class UsMovingAverageConvergenceDivergence(models.Model):
    date = models.DateField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    company_name = models.TextField(db_column='Company_Name', blank=True, null=True)  # Field name made lowercase.
    ema12 = models.FloatField(db_column='EMA12', blank=True, null=True)  # Field name made lowercase.
    ema26 = models.FloatField(db_column='EMA26', blank=True, null=True)  # Field name made lowercase.
    macd_line = models.FloatField(db_column='MACD Line', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    signal_line = models.FloatField(db_column='Signal Line', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    macd_histogram = models.FloatField(db_column='MACD Histogram', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    fs_ticker = models.TextField(db_column='FS_Ticker', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'US_Moving_Average_Convergence_Divergence'


class UsPerformance(models.Model):
    company = models.TextField(db_column='Company', blank=True, null=True)  # Field name made lowercase.
    number_2017todate = models.FloatField(db_column='2017toDate', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    ytd = models.FloatField(db_column='YTD', blank=True, null=True)  # Field name made lowercase.
    mtd = models.FloatField(db_column='MTD', blank=True, null=True)  # Field name made lowercase.
    number_1y = models.FloatField(db_column='1Y', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    factset_ticker = models.TextField(db_column='Factset_Ticker', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'US_Performance'


class UsPortfolioCreationFilters(models.Model):
    company = models.TextField(db_column='Company', blank=True, null=True)  # Field name made lowercase.
    date = models.DateField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    factset_ticker = models.TextField(db_column='Factset_Ticker', blank=True, null=True)  # Field name made lowercase.
    rsi_30 = models.FloatField(db_column='RSI_30', blank=True, null=True)  # Field name made lowercase.
    rsi_90 = models.FloatField(db_column='RSI_90', blank=True, null=True)  # Field name made lowercase.
    rsi_180 = models.FloatField(db_column='RSI_180', blank=True, null=True)  # Field name made lowercase.
    rsi_365 = models.FloatField(db_column='RSI_365', blank=True, null=True)  # Field name made lowercase.
    volume_change = models.FloatField(db_column='Volume_Change', blank=True, null=True)  # Field name made lowercase.
    current_price_vs_52_week_high = models.FloatField(db_column='Current_Price_vs_52_Week_High', blank=True, null=True)  # Field name made lowercase.
    price_vs_ma50 = models.FloatField(db_column='price_vs_MA50', blank=True, null=True)  # Field name made lowercase.
    price_vs_ma200 = models.FloatField(db_column='price_vs_MA200', blank=True, null=True)  # Field name made lowercase.
    adtv_30 = models.FloatField(db_column='ADTV_30', blank=True, null=True)  # Field name made lowercase.
    share_turnover = models.FloatField(db_column='Share Turnover', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'US_Portfolio_Creation_Filters'


class UsPortfolioV5(models.Model):
    date = models.DateTimeField(db_column='DATE', blank=True, null=True)  # Field name made lowercase.
    factset_ticker = models.CharField(db_column='Factset_Ticker', max_length=255, blank=True, null=True)  # Field name made lowercase.
    company = models.TextField(db_column='COMPANY', blank=True, null=True)  # Field name made lowercase.
    sector = models.CharField(db_column='SECTOR', max_length=255, blank=True, null=True)  # Field name made lowercase.
    curr_price = models.FloatField(db_column='CURR_PRICE', blank=True, null=True)  # Field name made lowercase.
    prev_day_price = models.FloatField(db_column='PREV_DAY_PRICE', blank=True, null=True)  # Field name made lowercase.
    avg_buy_price = models.FloatField(db_column='AVG_BUY_PRICE', blank=True, null=True)  # Field name made lowercase.
    avg_sell_price = models.FloatField(db_column='AVG_SELL_PRICE', blank=True, null=True)  # Field name made lowercase.
    curr_qty = models.FloatField(db_column='CURR_QTY', blank=True, null=True)  # Field name made lowercase.
    total_bought_qty = models.BigIntegerField(db_column='TOTAL_BOUGHT_QTY', blank=True, null=True)  # Field name made lowercase.
    total_sold_qty = models.FloatField(db_column='TOTAL_SOLD_QTY', blank=True, null=True)  # Field name made lowercase.
    long_term_qty = models.IntegerField(db_column='LONG_TERM_QTY', blank=True, null=True)  # Field name made lowercase.
    pnl_realized = models.FloatField(db_column='PNL_REALIZED', blank=True, null=True)  # Field name made lowercase.
    pnl_unrealized = models.FloatField(db_column='PNL_UNREALIZED', blank=True, null=True)  # Field name made lowercase.
    pnl_unrealized_prev_day = models.IntegerField(db_column='PNL_UNREALIZED_PREV_DAY', blank=True, null=True)  # Field name made lowercase.
    value_mkt_price = models.FloatField(db_column='VALUE_MKT_PRICE', blank=True, null=True)  # Field name made lowercase.
    value_cost = models.FloatField(db_column='VALUE_COST', blank=True, null=True)  # Field name made lowercase.
    pct_chg_day = models.FloatField(db_column='PCT_CHG_DAY', blank=True, null=True)  # Field name made lowercase.
    pct_chg = models.FloatField(db_column='PCT_CHG', blank=True, null=True)  # Field name made lowercase.
    profit_loss = models.CharField(db_column='PROFIT_LOSS', max_length=6)  # Field name made lowercase.
    price_chg_day = models.FloatField(db_column='PRICE_CHG_DAY', blank=True, null=True)  # Field name made lowercase.
    customer_name = models.TextField(db_column='CUSTOMER_NAME', blank=True, null=True)  # Field name made lowercase.
    portfolio_name = models.TextField(db_column='PORTFOLIO_NAME', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'US_Portfolio_V5'


class UsPositionsInput(models.Model):
    security_id = models.CharField(db_column='SECURITY ID', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    tradedate = models.DateTimeField(db_column='TRADEDATE', blank=True, null=True)  # Field name made lowercase.
    action = models.CharField(db_column='ACTION', max_length=255, blank=True, null=True)  # Field name made lowercase.
    status = models.CharField(db_column='STATUS', max_length=255, blank=True, null=True)  # Field name made lowercase.
    allocation_id = models.CharField(db_column='ALLOCATION ID', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    secdescription = models.CharField(db_column='SECDESCRIPTION', max_length=255, blank=True, null=True)  # Field name made lowercase.
    sedol = models.CharField(db_column='SEDOL', max_length=255, blank=True, null=True)  # Field name made lowercase.
    isin = models.CharField(db_column='ISIN', max_length=255, blank=True, null=True)  # Field name made lowercase.
    cusip = models.CharField(db_column='CUSIP', max_length=255, blank=True, null=True)  # Field name made lowercase.
    trader = models.CharField(db_column='TRADER', max_length=255, blank=True, null=True)  # Field name made lowercase.
    portfolio = models.CharField(db_column='PORTFOLIO', max_length=255, blank=True, null=True)  # Field name made lowercase.
    customer = models.CharField(db_column='Customer', max_length=255, blank=True, null=True)  # Field name made lowercase.
    quantity = models.FloatField(db_column='QUANTITY', blank=True, null=True)  # Field name made lowercase.
    price = models.FloatField(db_column='PRICE', blank=True, null=True)  # Field name made lowercase.
    fx_rate = models.FloatField(db_column='FX RATE', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    grossmoney = models.FloatField(db_column='GROSSMONEY', blank=True, null=True)  # Field name made lowercase.
    totalcomm = models.CharField(db_column='TOTALCOMM', max_length=255, blank=True, null=True)  # Field name made lowercase.
    totalfees = models.CharField(db_column='TOTALFEES', max_length=255, blank=True, null=True)  # Field name made lowercase.
    netmoney = models.FloatField(db_column='NETMONEY', blank=True, null=True)  # Field name made lowercase.
    execcurrency = models.CharField(db_column='EXECCURRENCY', max_length=255, blank=True, null=True)  # Field name made lowercase.
    broker = models.CharField(db_column='BROKER', max_length=255, blank=True, null=True)  # Field name made lowercase.
    custodian = models.CharField(db_column='CUSTODIAN', max_length=255, blank=True, null=True)  # Field name made lowercase.
    accrued_int = models.CharField(db_column='ACCRUED INT', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    strategy = models.CharField(db_column='STRATEGY', max_length=255, blank=True, null=True)  # Field name made lowercase.
    bid_qty = models.CharField(db_column='BID QTY', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    bid_price = models.CharField(db_column='BID PRICE', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    pricing_date = models.DateTimeField(db_column='PRICING DATE', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    first_trade_date = models.DateTimeField(db_column='FIRST TRADE DATE', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    deal_captian = models.CharField(db_column='DEAL CAPTIAN', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    hedge = models.CharField(db_column='HEDGE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    intial_target_price = models.CharField(db_column='INTIAL TARGET PRICE', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    target_price = models.CharField(db_column='TARGET PRICE', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ultimate_stop = models.CharField(db_column='ULTIMATE STOP', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    country = models.CharField(db_column='COUNTRY', max_length=255, blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'US_Positions_Input'


class UsPredictions(models.Model):
    ticker = models.CharField(db_column='Ticker', max_length=255, blank=True, null=True)  # Field name made lowercase.
    company_name = models.CharField(db_column='Company_Name', max_length=255, blank=True, null=True)  # Field name made lowercase.
    return_prediction = models.FloatField(db_column='Return_Prediction', blank=True, null=True)  # Field name made lowercase.
    confidence_level = models.CharField(db_column='Confidence LEVEL', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    period = models.CharField(db_column='Period', max_length=255, blank=True, null=True)  # Field name made lowercase.
    flag = models.CharField(db_column='Flag', max_length=255, blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'US_Predictions'


class UsQoq(models.Model):
    company = models.CharField(db_column='Company', max_length=255, blank=True, null=True)  # Field name made lowercase.
    fs_ticker = models.CharField(db_column='FS Ticker', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    sector = models.CharField(db_column='Sector', max_length=255, blank=True, null=True)  # Field name made lowercase.
    number_3_29_2019 = models.FloatField(db_column='3/29/2019', blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_6_28_2019 = models.FloatField(db_column='6/28/2019', blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_9_30_2019 = models.FloatField(db_column='9/30/2019', blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_12_31_2019 = models.FloatField(db_column='12/31/2019', blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_3_31_2020 = models.FloatField(db_column='3/31/2020', blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_6_30_2020 = models.FloatField(db_column='6/30/2020', blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'US_QoQ'


class UsRelativeStrengthSectors(models.Model):
    date = models.DateField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    s_p_500 = models.FloatField(db_column='S&P 500', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    price = models.FloatField(db_column='Price', blank=True, null=True)  # Field name made lowercase.
    ticker = models.TextField(db_column='Ticker', blank=True, null=True)  # Field name made lowercase.
    company = models.TextField(db_column='Company', blank=True, null=True)  # Field name made lowercase.
    ticker_vs_s_p500 = models.FloatField(db_column='Ticker_vs_S&P500', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    average = models.FloatField(db_column='Average', blank=True, null=True)  # Field name made lowercase.
    std = models.FloatField(db_column='StD', blank=True, null=True)  # Field name made lowercase.
    number_1sd = models.FloatField(db_column='1sd', blank=True, null=True)  # Field renamed because it wasn't a valid Python identifier.
    number_2sd = models.FloatField(db_column='2sd', blank=True, null=True)  # Field renamed because it wasn't a valid Python identifier.
    number_3sd = models.FloatField(db_column='3sd', blank=True, null=True)  # Field renamed because it wasn't a valid Python identifier.
    field_1sd = models.FloatField(db_column='-1sd', blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it started with '_'.
    field_2sd = models.FloatField(db_column='-2sd', blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it started with '_'.
    field_3sd = models.FloatField(db_column='-3sd', blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it started with '_'.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'US_Relative_Strength_Sectors'


class UsRiskFreeRate(models.Model):
    date = models.DateField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    risk_free_rate = models.FloatField(db_column='Risk Free Rate', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'US_Risk_Free_Rate'


class UsRiskMonitor(models.Model):
    fs_ticker = models.TextField(db_column='FS_Ticker', blank=True, null=True)  # Field name made lowercase.
    company_name = models.TextField(db_column='Company_Name', blank=True, null=True)  # Field name made lowercase.
    number_1y_cumulative_return = models.FloatField(db_column='1Y_Cumulative_Return', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_1m_cumulative_return = models.FloatField(db_column='1M_Cumulative_Return', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_3m_cumulative_return = models.FloatField(db_column='3M_Cumulative_Return', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    price = models.FloatField(db_column='Price', blank=True, null=True)  # Field name made lowercase.
    number_1m_excess_return = models.FloatField(db_column='1M_Excess_Return', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_3m_excess_return = models.FloatField(db_column='3M_Excess_Return', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_1y_excess_return = models.FloatField(db_column='1Y_Excess_Return', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_52_week_high = models.FloatField(db_column='52_Week_High', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_52_week_low = models.FloatField(db_column='52_Week_Low', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_1_month_high = models.FloatField(db_column='1_Month_High', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_1_month_low = models.FloatField(db_column='1_Month_Low', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    rsi = models.FloatField(db_column='RSI', blank=True, null=True)  # Field name made lowercase.
    number_20_ema = models.FloatField(db_column='20_EMA', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_50_dma = models.FloatField(db_column='50_DMA', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    vfx = models.FloatField(db_column='VFX', blank=True, null=True)  # Field name made lowercase.
    views = models.TextField(db_column='Views', blank=True, null=True)  # Field name made lowercase.
    id = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'US_Risk_Monitor'


class UsSnp500Above200MaInput(models.Model):
    date = models.DateField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    pct_of_s_p500_above_200ma = models.FloatField(db_column='pct_of_S_P500_Above_200MA', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'US_SNP_500_Above_200MA_Input'


class UsScreenerFundamental(models.Model):
    pe = models.TextField(db_column='PE', blank=True, null=True)  # Field name made lowercase.
    pb = models.TextField(db_column='PB', blank=True, null=True)  # Field name made lowercase.
    ev_sales = models.TextField(db_column='EV_Sales', blank=True, null=True)  # Field name made lowercase.
    ev_ebitda = models.TextField(db_column='EV_EBITDA', blank=True, null=True)  # Field name made lowercase.
    div_yield = models.TextField(db_column='Div_Yield', blank=True, null=True)  # Field name made lowercase.
    sales_growth = models.TextField(db_column='Sales_Growth', blank=True, null=True)  # Field name made lowercase.
    sales_growth_3y_cagr = models.TextField(db_column='Sales_growth_3Y_CAGR', blank=True, null=True)  # Field name made lowercase.
    eps_growth = models.TextField(db_column='EPS_Growth', blank=True, null=True)  # Field name made lowercase.
    eps_growth_3y_cagr = models.TextField(db_column='EPS growth_3Y_CAGR', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    roe = models.TextField(db_column='ROE', blank=True, null=True)  # Field name made lowercase.
    roce = models.TextField(db_column='ROCE', blank=True, null=True)  # Field name made lowercase.
    fcf_margin = models.TextField(db_column='FCF_Margin', blank=True, null=True)  # Field name made lowercase.
    sales_total_assets = models.TextField(db_column='Sales_Total_Assets', blank=True, null=True)  # Field name made lowercase.
    net_debt_fcf = models.TextField(db_column='Net_Debt_FCF', blank=True, null=True)  # Field name made lowercase.
    net_debt_ebitda = models.TextField(db_column='Net_Debt_EBITDA', blank=True, null=True)  # Field name made lowercase.
    debt_equity = models.TextField(db_column='Debt_Equity', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'US_Screener_Fundamental'


class UsScreenerQuantitative(models.Model):
    quant_market_cap = models.TextField(db_column='Quant_Market_Cap', blank=True, null=True)  # Field name made lowercase.
    quant_sales = models.TextField(db_column='Quant_Sales', blank=True, null=True)  # Field name made lowercase.
    quant_sector = models.TextField(db_column='Quant_Sector', blank=True, null=True)  # Field name made lowercase.
    quant_fo_stocks = models.TextField(db_column='Quant_fo_stocks', blank=True, null=True)  # Field name made lowercase.
    quant_price = models.TextField(db_column='Quant_Price', blank=True, null=True)  # Field name made lowercase.
    quant_target_price = models.TextField(db_column='Quant_Target_Price', blank=True, null=True)  # Field name made lowercase.
    quant_volume = models.TextField(db_column='Quant_Volume', blank=True, null=True)  # Field name made lowercase.
    quant_index = models.TextField(db_column='Quant_Index', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'US_Screener_Quantitative'


class UsScreenerRisk(models.Model):
    beta = models.TextField(db_column='Beta', blank=True, null=True)  # Field name made lowercase.
    volatility = models.TextField(db_column='Volatility', blank=True, null=True)  # Field name made lowercase.
    piotroski_score = models.TextField(db_column='Piotroski_Score', blank=True, null=True)  # Field name made lowercase.
    average_daily_traded_volume = models.TextField(db_column='Average_Daily_Traded_Volume', blank=True, null=True)  # Field name made lowercase.
    share_turnover = models.TextField(db_column='Share_Turnover', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'US_Screener_Risk'


class UsSectorsForWizard(models.Model):
    sector = models.CharField(db_column='Sector', max_length=255, blank=True, null=True)  # Field name made lowercase.
    name = models.CharField(db_column='Name', max_length=255, blank=True, null=True)  # Field name made lowercase.
    fs_ticker = models.CharField(db_column='FS_Ticker', max_length=255, blank=True, null=True)  # Field name made lowercase.
    index_ticker = models.TextField(blank=True, null=True)
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'US_Sectors_for_Wizard'


class UsSecurityMaster(models.Model):
    isin_no = models.CharField(db_column='ISIN No', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    security_code = models.TextField(db_column='Security Code', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    bbg_ticker = models.TextField(db_column='BBG_Ticker', blank=True, null=True)  # Field name made lowercase.
    fs_ticker = models.CharField(db_column='FS Ticker', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    fs_name = models.CharField(db_column='FS Name', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    market_cap = models.FloatField(db_column='Market Cap', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    gics = models.CharField(db_column='GICS', max_length=255, blank=True, null=True)  # Field name made lowercase.
    market_cap_category = models.CharField(db_column='Market_Cap_Category', max_length=255, blank=True, null=True)  # Field name made lowercase.
    beta_1y = models.FloatField(db_column='Beta_1Y', blank=True, null=True)  # Field name made lowercase.
    volatility_1y = models.FloatField(db_column='Volatility_1Y', blank=True, null=True)  # Field name made lowercase.
    idiosyncratic_vol = models.FloatField(db_column='Idiosyncratic_Vol', blank=True, null=True)  # Field name made lowercase.
    mcap_rm_field = models.FloatField(db_column='Mcap(RM)', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it ended with '_'.
    sales_rm_field = models.FloatField(db_column='Sales(RM)', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it ended with '_'.
    pe = models.FloatField(db_column='PE', blank=True, null=True)  # Field name made lowercase.
    pb = models.FloatField(db_column='PB', blank=True, null=True)  # Field name made lowercase.
    div_yield = models.FloatField(db_column='Div_Yield', blank=True, null=True)  # Field name made lowercase.
    div_payout = models.FloatField(db_column='Div_Payout', blank=True, null=True)  # Field name made lowercase.
    sales_growth = models.FloatField(db_column='Sales_Growth', blank=True, null=True)  # Field name made lowercase.
    sales_growth_qoq_growth = models.FloatField(db_column='Sales_Growth_QoQ_Growth', blank=True, null=True)  # Field name made lowercase.
    sales_growth_3y_5r_cagr = models.FloatField(db_column='Sales_Growth_3Y/5R_CAGR', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    eps_growth = models.FloatField(db_column='EPS_Growth', blank=True, null=True)  # Field name made lowercase.
    eps_growth_qoq_growth = models.FloatField(db_column='EPS_Growth_QoQ_Growth', blank=True, null=True)  # Field name made lowercase.
    eps_growth_3y_5r_cagr = models.FloatField(db_column='EPS_Growth_3Y/5R_CAGR', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    roe = models.FloatField(db_column='ROE', blank=True, null=True)  # Field name made lowercase.
    roce = models.FloatField(db_column='ROCE', blank=True, null=True)  # Field name made lowercase.
    ebitda_margin = models.FloatField(db_column='EBITDA_Margin', blank=True, null=True)  # Field name made lowercase.
    net_income_margin = models.FloatField(db_column='Net_Income_Margin', blank=True, null=True)  # Field name made lowercase.
    fcf_margin = models.FloatField(db_column='FCF_Margin', blank=True, null=True)  # Field name made lowercase.
    sales_total_assets = models.FloatField(db_column='Sales/Total Assets', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    net_debt_fcf = models.FloatField(db_column='Net_Debt/FCF', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    net_debt_ebitda = models.FloatField(db_column='Net_Debt/EBITDA', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    debt_equity = models.FloatField(db_column='Debt/Equity', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    piotroski_score = models.FloatField(db_column='Piotroski_Score', blank=True, null=True)  # Field name made lowercase.
    s_p_500 = models.CharField(db_column='S&P 500', max_length=25, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    dow_jones_30 = models.CharField(db_column='Dow Jones 30', max_length=25, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    shares_outstanding = models.FloatField(db_column='Shares Outstanding', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ev_sales = models.FloatField(db_column='EV/Sales', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ev_ebitda = models.FloatField(db_column='EV/EBITDA', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    qtr_eps_growth_yoy = models.FloatField(db_column='QTR EPS_Growth_YoY', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    qtr_sales_growth_yoy = models.FloatField(db_column='QTR Sales_Growth_YoY', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ev = models.FloatField(db_column='EV', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()
    flag = models.CharField(db_column='Flag', max_length=255, blank=True, null=True)  # Field name made lowercase.
    etf_flag = models.CharField(db_column='ETF_Flag', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'US_Security_Master'


class UsStockRanking(models.Model):
    date = models.DateTimeField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    co_code = models.TextField(blank=True, null=True)
    nsesymbol = models.TextField(blank=True, null=True)
    fs_name = models.TextField(db_column='FS Name', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    fs_ticker = models.TextField(db_column='FS Ticker', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    sales = models.FloatField(db_column='Sales', blank=True, null=True)  # Field name made lowercase.
    piotroski_score = models.FloatField(db_column='Piotroski_Score', blank=True, null=True)  # Field name made lowercase.
    roe = models.FloatField(db_column='ROE', blank=True, null=True)  # Field name made lowercase.
    debt_equity = models.FloatField(db_column='Debt/Equity', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    fcf_margin = models.FloatField(db_column='FCF_Margin', blank=True, null=True)  # Field name made lowercase.
    beta = models.FloatField(db_column='Beta', blank=True, null=True)  # Field name made lowercase.
    sales_growth = models.FloatField(db_column='Sales_Growth', blank=True, null=True)  # Field name made lowercase.
    eps_growth = models.FloatField(db_column='EPS_Growth', blank=True, null=True)  # Field name made lowercase.
    sales_qoq = models.FloatField(db_column='Sales_QoQ', blank=True, null=True)  # Field name made lowercase.
    price_vs_ema20 = models.FloatField(db_column='Price_vs_EMA20', blank=True, null=True)  # Field name made lowercase.
    price_vs_ema50 = models.FloatField(db_column='Price_vs_EMA50', blank=True, null=True)  # Field name made lowercase.
    price_vs_ema200 = models.FloatField(db_column='Price_vs_EMA200', blank=True, null=True)  # Field name made lowercase.
    ema50_vs_ema200 = models.FloatField(db_column='EMA50_vs_EMA200', blank=True, null=True)  # Field name made lowercase.
    current_price_vs_52_week_high = models.FloatField(db_column='Current_Price_vs_52_Week_High', blank=True, null=True)  # Field name made lowercase.
    rsi = models.FloatField(db_column='RSI', blank=True, null=True)  # Field name made lowercase.
    price = models.FloatField(db_column='Price', blank=True, null=True)  # Field name made lowercase.
    return_field = models.FloatField(db_column='Return', blank=True, null=True)  # Field name made lowercase. Field renamed because it was a Python reserved word.
    net_change = models.FloatField(db_column='Net Change', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    q_sales = models.FloatField(db_column='Q_Sales', blank=True, null=True)  # Field name made lowercase.
    q_piotroski_score = models.FloatField(db_column='Q_Piotroski_Score', blank=True, null=True)  # Field name made lowercase.
    q_roe = models.FloatField(db_column='Q_ROE', blank=True, null=True)  # Field name made lowercase.
    q_debt_equity = models.FloatField(db_column='Q_Debt/Equity', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    q_fcf_margin = models.FloatField(db_column='Q_FCF_Margin', blank=True, null=True)  # Field name made lowercase.
    q_beta = models.FloatField(db_column='Q_Beta', blank=True, null=True)  # Field name made lowercase.
    g_sales_growth = models.FloatField(db_column='G_Sales_Growth', blank=True, null=True)  # Field name made lowercase.
    g_eps_growth = models.FloatField(db_column='G_EPS_Growth', blank=True, null=True)  # Field name made lowercase.
    g_sales_qoq = models.FloatField(db_column='G_Sales_QoQ', blank=True, null=True)  # Field name made lowercase.
    t_price_vs_ema20 = models.FloatField(db_column='T_Price_vs_EMA20', blank=True, null=True)  # Field name made lowercase.
    t_price_vs_ema50 = models.FloatField(db_column='T_Price_vs_EMA50', blank=True, null=True)  # Field name made lowercase.
    t_price_vs_ema200 = models.FloatField(db_column='T_Price_vs_EMA200', blank=True, null=True)  # Field name made lowercase.
    t_ema50_vs_ema200 = models.FloatField(db_column='T_EMA50_vs_EMA200', blank=True, null=True)  # Field name made lowercase.
    t_current_price_vs_52_week_high = models.FloatField(db_column='T_Current_Price_vs_52_Week_High', blank=True, null=True)  # Field name made lowercase.
    t_rsi = models.FloatField(db_column='T_RSI', blank=True, null=True)  # Field name made lowercase.
    quality_score = models.FloatField(db_column='Quality_Score', blank=True, null=True)  # Field name made lowercase.
    growth_score = models.FloatField(db_column='Growth_Score', blank=True, null=True)  # Field name made lowercase.
    technical_score = models.FloatField(db_column='Technical_Score', blank=True, null=True)  # Field name made lowercase.
    stock_rank = models.FloatField(db_column='Stock_Rank', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'US_Stock_Ranking'


class UsStocksDailyBeta(models.Model):
    date = models.DateTimeField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    factset_ticker = models.TextField(db_column='Factset_Ticker', blank=True, null=True)  # Field name made lowercase.
    company = models.TextField(db_column='Company', blank=True, null=True)  # Field name made lowercase.
    index = models.TextField(db_column='Index', blank=True, null=True)  # Field name made lowercase.
    return_field = models.FloatField(db_column='Return', blank=True, null=True)  # Field name made lowercase. Field renamed because it was a Python reserved word.
    index_return = models.FloatField(db_column='Index Return', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    beta_1y = models.FloatField(db_column='Beta_1Y', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'US_Stocks_Daily_Beta'


class UsTargetPrices(models.Model):
    ticker = models.TextField(db_column='Ticker', blank=True, null=True)  # Field name made lowercase.
    company = models.TextField(db_column='Company', blank=True, null=True)  # Field name made lowercase.
    current_price = models.FloatField(db_column='Current_Price', blank=True, null=True)  # Field name made lowercase.
    target_price = models.FloatField(db_column='Target_Price', blank=True, null=True)  # Field name made lowercase.
    avg_upside = models.FloatField(db_column='Avg_Upside', blank=True, null=True)  # Field name made lowercase.
    avg_downside = models.FloatField(db_column='Avg_Downside', blank=True, null=True)  # Field name made lowercase.
    number_52_week_high = models.FloatField(db_column='52_Week_High', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_52_week_low = models.FloatField(db_column='52_Week_Low', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_1_month_high = models.FloatField(db_column='1 Month High', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_1_month_low = models.FloatField(db_column='1 Month Low', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'US_Target_prices'


class UsTechnicalIndicatorsDaily(models.Model):
    date = models.DateTimeField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    fs_ticker = models.TextField(db_column='FS_Ticker', blank=True, null=True)  # Field name made lowercase.
    symbol = models.TextField(db_column='Symbol', blank=True, null=True)  # Field name made lowercase.
    company_name = models.TextField(db_column='Company_Name', blank=True, null=True)  # Field name made lowercase.
    price = models.FloatField(db_column='Price', blank=True, null=True)  # Field name made lowercase.
    return_field = models.FloatField(db_column='Return', blank=True, null=True)  # Field name made lowercase. Field renamed because it was a Python reserved word.
    net_change = models.FloatField(db_column='Net_Change', blank=True, null=True)  # Field name made lowercase.
    ma9 = models.FloatField(db_column='MA9', blank=True, null=True)  # Field name made lowercase.
    ma20 = models.FloatField(db_column='MA20', blank=True, null=True)  # Field name made lowercase.
    ma50 = models.FloatField(db_column='MA50', blank=True, null=True)  # Field name made lowercase.
    ma200 = models.FloatField(db_column='MA200', blank=True, null=True)  # Field name made lowercase.
    ma9_ma20_value = models.TextField(db_column='MA9_MA20_Value', blank=True, null=True)  # Field name made lowercase.
    ma20_ma50_value = models.TextField(db_column='MA20_MA50_Value', blank=True, null=True)  # Field name made lowercase.
    ma50_ma200_value = models.TextField(db_column='MA50_MA200_Value', blank=True, null=True)  # Field name made lowercase.
    ma9_actions = models.TextField(db_column='MA9_Actions', blank=True, null=True)  # Field name made lowercase.
    ma50_actions = models.TextField(db_column='MA50_Actions', blank=True, null=True)  # Field name made lowercase.
    ma20_actions = models.TextField(db_column='MA20_Actions', blank=True, null=True)  # Field name made lowercase.
    ma200_actions = models.TextField(db_column='MA200_Actions', blank=True, null=True)  # Field name made lowercase.
    ma9_ma20_actions = models.TextField(db_column='MA9_MA20_Actions', blank=True, null=True)  # Field name made lowercase.
    ma20_ma50_actions = models.TextField(db_column='MA20_MA50_Actions', blank=True, null=True)  # Field name made lowercase.
    ma50_ma200_actions = models.TextField(db_column='MA50_MA200_Actions', blank=True, null=True)  # Field name made lowercase.
    rsi_value = models.FloatField(db_column='RSI_Value', blank=True, null=True)  # Field name made lowercase.
    cci_value = models.FloatField(db_column='CCI_Value', blank=True, null=True)  # Field name made lowercase.
    williams_value = models.FloatField(db_column='Williams_Value', blank=True, null=True)  # Field name made lowercase.
    stochrsi_value = models.FloatField(db_column='StochRSI_Value', blank=True, null=True)  # Field name made lowercase.
    stochastics_value = models.FloatField(db_column='Stochastics_Value', blank=True, null=True)  # Field name made lowercase.
    rsi_actions = models.TextField(db_column='RSI_Actions', blank=True, null=True)  # Field name made lowercase.
    cci_actions = models.TextField(db_column='CCI_Actions', blank=True, null=True)  # Field name made lowercase.
    williams_actions = models.TextField(db_column='Williams_Actions', blank=True, null=True)  # Field name made lowercase.
    stochrsi_actions = models.TextField(db_column='StochRSI_Actions', blank=True, null=True)  # Field name made lowercase.
    stochastics_actions = models.TextField(db_column='Stochastics_Actions', blank=True, null=True)  # Field name made lowercase.
    macd_value = models.FloatField(db_column='MACD_Value', blank=True, null=True)  # Field name made lowercase.
    atr_value = models.FloatField(db_column='ATR_Value', blank=True, null=True)  # Field name made lowercase.
    adx_value = models.FloatField(db_column='ADX_Value', blank=True, null=True)  # Field name made lowercase.
    super_trend_value = models.FloatField(db_column='Super_Trend_Value', blank=True, null=True)  # Field name made lowercase.
    macd_actions = models.TextField(db_column='MACD_Actions', blank=True, null=True)  # Field name made lowercase.
    atr_actions = models.TextField(db_column='ATR_Actions', blank=True, null=True)  # Field name made lowercase.
    adx_actions = models.TextField(db_column='ADX_Actions', blank=True, null=True)  # Field name made lowercase.
    super_trend_actions = models.TextField(db_column='Super_Trend_Actions', blank=True, null=True)  # Field name made lowercase.
    mfi_value = models.FloatField(db_column='MFI_Value', blank=True, null=True)  # Field name made lowercase.
    pvo_value = models.FloatField(db_column='PVO_Value', blank=True, null=True)  # Field name made lowercase.
    cmf_value = models.FloatField(db_column='CMF_Value', blank=True, null=True)  # Field name made lowercase.
    mfi_actions = models.TextField(db_column='MFI_Actions', blank=True, null=True)  # Field name made lowercase.
    pvo_actions = models.TextField(db_column='PVO_Actions', blank=True, null=True)  # Field name made lowercase.
    cmf_actions = models.TextField(db_column='CMF_Actions', blank=True, null=True)  # Field name made lowercase.
    moving_average_rating = models.TextField(db_column='Moving_Average_Rating', blank=True, null=True)  # Field name made lowercase.
    momentum_rating = models.TextField(db_column='Momentum_Rating', blank=True, null=True)  # Field name made lowercase.
    trend_rating = models.TextField(db_column='Trend_Rating', blank=True, null=True)  # Field name made lowercase.
    volume_rating = models.TextField(db_column='Volume_Rating', blank=True, null=True)  # Field name made lowercase.
    final_rating = models.TextField(db_column='Final_Rating', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'US_Technical_Indicators_Daily'


class UsTransactions(models.Model):
    security_id = models.CharField(db_column='SECURITY ID', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    tradedate = models.DateTimeField(db_column='TRADEDATE', blank=True, null=True)  # Field name made lowercase.
    action = models.CharField(db_column='ACTION', max_length=255, blank=True, null=True)  # Field name made lowercase.
    status = models.CharField(db_column='STATUS', max_length=255, blank=True, null=True)  # Field name made lowercase.
    allocation_id = models.CharField(db_column='ALLOCATION ID', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    secdescription = models.CharField(db_column='SECDESCRIPTION', max_length=255, blank=True, null=True)  # Field name made lowercase.
    sedol = models.CharField(db_column='SEDOL', max_length=255, blank=True, null=True)  # Field name made lowercase.
    isin = models.CharField(db_column='ISIN', max_length=255, blank=True, null=True)  # Field name made lowercase.
    cusip = models.CharField(db_column='CUSIP', max_length=255, blank=True, null=True)  # Field name made lowercase.
    trader = models.CharField(db_column='TRADER', max_length=255, blank=True, null=True)  # Field name made lowercase.
    portfolio = models.CharField(db_column='PORTFOLIO', max_length=255, blank=True, null=True)  # Field name made lowercase.
    customer = models.CharField(db_column='Customer', max_length=255, blank=True, null=True)  # Field name made lowercase.
    quantity = models.FloatField(db_column='QUANTITY', blank=True, null=True)  # Field name made lowercase.
    price = models.FloatField(db_column='PRICE', blank=True, null=True)  # Field name made lowercase.
    fx_rate = models.FloatField(db_column='FX RATE', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    grossmoney = models.FloatField(db_column='GROSSMONEY', blank=True, null=True)  # Field name made lowercase.
    totalcomm = models.CharField(db_column='TOTALCOMM', max_length=255, blank=True, null=True)  # Field name made lowercase.
    totalfees = models.CharField(db_column='TOTALFEES', max_length=255, blank=True, null=True)  # Field name made lowercase.
    netmoney = models.FloatField(db_column='NETMONEY', blank=True, null=True)  # Field name made lowercase.
    execcurrency = models.CharField(db_column='EXECCURRENCY', max_length=255, blank=True, null=True)  # Field name made lowercase.
    broker = models.CharField(db_column='BROKER', max_length=255, blank=True, null=True)  # Field name made lowercase.
    custodian = models.CharField(db_column='CUSTODIAN', max_length=255, blank=True, null=True)  # Field name made lowercase.
    accrued_int = models.CharField(db_column='ACCRUED INT', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    strategy = models.CharField(db_column='STRATEGY', max_length=255, blank=True, null=True)  # Field name made lowercase.
    bid_qty = models.CharField(db_column='BID QTY', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    bid_price = models.CharField(db_column='BID PRICE', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    pricing_date = models.DateTimeField(db_column='PRICING DATE', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    first_trade_date = models.DateTimeField(db_column='FIRST TRADE DATE', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    deal_captian = models.CharField(db_column='DEAL CAPTIAN', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    hedge = models.CharField(db_column='HEDGE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    intial_target_price = models.CharField(db_column='INTIAL TARGET PRICE', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    target_price = models.CharField(db_column='TARGET PRICE', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ultimate_stop = models.CharField(db_column='ULTIMATE STOP', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    country = models.CharField(db_column='COUNTRY', max_length=255, blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'US_Transactions'


class UsUniversePricesMain(models.Model):
    date = models.DateField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    company = models.TextField(db_column='Company')  # Field name made lowercase.
    price = models.FloatField(db_column='Price', blank=True, null=True)  # Field name made lowercase.
    factset_ticker = models.TextField(db_column='Factset_Ticker', blank=True, null=True)  # Field name made lowercase.
    return_field = models.FloatField(db_column='Return', blank=True, null=True)  # Field name made lowercase. Field renamed because it was a Python reserved word.
    company_type = models.TextField(db_column='Company_Type', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'US_Universe_Prices_Main'


class UsVolume20Data(models.Model):
    date = models.DateField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    ticker = models.TextField(db_column='Ticker', blank=True, null=True)  # Field name made lowercase.
    volume_20 = models.FloatField(db_column='Volume_20', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'US_Volume_20_Data'


class UsVolumeData(models.Model):
    date = models.DateField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    factor = models.TextField(db_column='Factor', blank=True, null=True)  # Field name made lowercase.
    ticker = models.TextField(db_column='Ticker', blank=True, null=True)  # Field name made lowercase.
    value = models.FloatField(blank=True, null=True)
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'US_Volume_Data'


class UsVolumeDataOld(models.Model):
    date = models.DateField(db_column='Date')  # Field name made lowercase.
    factor = models.CharField(db_column='Factor', max_length=50)  # Field name made lowercase.
    t_us = models.FloatField(db_column='T-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ctl_us = models.FloatField(db_column='CTL-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    vz_us = models.FloatField(db_column='VZ-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ttwo_us = models.FloatField(db_column='TTWO-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    lyv_us = models.FloatField(db_column='LYV-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ea_us = models.FloatField(db_column='EA-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    atvi_us = models.FloatField(db_column='ATVI-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    dis_us = models.FloatField(db_column='DIS-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    nflx_us = models.FloatField(db_column='NFLX-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    googl_us = models.FloatField(db_column='GOOGL-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    goog_us = models.FloatField(db_column='GOOG-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    fb_us = models.FloatField(db_column='FB-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    twtr_us = models.FloatField(db_column='TWTR-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    omc_us = models.FloatField(db_column='OMC-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    cmcsa_us = models.FloatField(db_column='CMCSA-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    disca_us = models.FloatField(db_column='DISCA-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    chtr_us = models.FloatField(db_column='CHTR-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    disck_us = models.FloatField(db_column='DISCK-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ipg_us = models.FloatField(db_column='IPG-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    dish_us = models.FloatField(db_column='DISH-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    nws_us = models.FloatField(db_column='NWS-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    viac_us = models.FloatField(db_column='VIAC-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    fox_us = models.FloatField(db_column='FOX-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    nwsa_us = models.FloatField(db_column='NWSA-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    foxa_us = models.FloatField(db_column='FOXA-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    tmus_us = models.FloatField(db_column='TMUS-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    aptv_us = models.FloatField(db_column='APTV-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    bwa_us = models.FloatField(db_column='BWA-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    gm_us = models.FloatField(db_column='GM-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    f_us = models.FloatField(db_column='F-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    hog_us = models.FloatField(db_column='HOG-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    lkq_us = models.FloatField(db_column='LKQ-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    gpc_us = models.FloatField(db_column='GPC-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    hrb_us = models.FloatField(db_column='HRB-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    cmg_us = models.FloatField(db_column='CMG-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    mgm_us = models.FloatField(db_column='MGM-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    yum_us = models.FloatField(db_column='YUM-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ccl_us = models.FloatField(db_column='CCL-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    hlt_us = models.FloatField(db_column='HLT-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    sbux_us = models.FloatField(db_column='SBUX-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    rcl_us = models.FloatField(db_column='RCL-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    mcd_us = models.FloatField(db_column='MCD-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    lvs_us = models.FloatField(db_column='LVS-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    dri_us = models.FloatField(db_column='DRI-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    mar_us = models.FloatField(db_column='MAR-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    wynn_us = models.FloatField(db_column='WYNN-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    nclh_us = models.FloatField(db_column='NCLH-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    phm_us = models.FloatField(db_column='PHM-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    whr_us = models.FloatField(db_column='WHR-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    nvr_us = models.FloatField(db_column='NVR-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    mhk_us = models.FloatField(db_column='MHK-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    dhi_us = models.FloatField(db_column='DHI-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    len_us = models.FloatField(db_column='LEN-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    nwl_us = models.FloatField(db_column='NWL-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    grmn_us = models.FloatField(db_column='GRMN-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    leg_us = models.FloatField(db_column='LEG-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    amzn_us = models.FloatField(db_column='AMZN-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ebay_us = models.FloatField(db_column='EBAY-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    expe_us = models.FloatField(db_column='EXPE-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    bkng_us = models.FloatField(db_column='BKNG-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    has_us = models.FloatField(db_column='HAS-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    dltr_us = models.FloatField(db_column='DLTR-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    kss_us = models.FloatField(db_column='KSS-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    jwn_us = models.FloatField(db_column='JWN-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    dg_us = models.FloatField(db_column='DG-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    m_us = models.FloatField(db_column='M-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    tgt_us = models.FloatField(db_column='TGT-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    low_us = models.FloatField(db_column='LOW-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    hd_us = models.FloatField(db_column='HD-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    lb_us = models.FloatField(db_column='LB-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    aap_us = models.FloatField(db_column='AAP-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    tjx_us = models.FloatField(db_column='TJX-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    orly_us = models.FloatField(db_column='ORLY-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    gps_us = models.FloatField(db_column='GPS-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    kmx_us = models.FloatField(db_column='KMX-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    tif_us = models.FloatField(db_column='TIF-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ulta_us = models.FloatField(db_column='ULTA-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    bby_us = models.FloatField(db_column='BBY-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    tsco_us = models.FloatField(db_column='TSCO-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    azo_us = models.FloatField(db_column='AZO-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    rost_us = models.FloatField(db_column='ROST-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ua_us = models.FloatField(db_column='UA-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    nke_us = models.FloatField(db_column='NKE-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    vfc_us = models.FloatField(db_column='VFC-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    uaa_us = models.FloatField(db_column='UAA-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    tpr_us = models.FloatField(db_column='TPR-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    hbi_us = models.FloatField(db_column='HBI-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    pvh_us = models.FloatField(db_column='PVH-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    rl_us = models.FloatField(db_column='RL-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    cpri_us = models.FloatField(db_column='CPRI-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    bf_b_us = models.FloatField(db_column='BF.B-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    stz_us = models.FloatField(db_column='STZ-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ko_us = models.FloatField(db_column='KO-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    pep_us = models.FloatField(db_column='PEP-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    tap_us = models.FloatField(db_column='TAP-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    mnst_us = models.FloatField(db_column='MNST-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    syy_us = models.FloatField(db_column='SYY-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    kr_us = models.FloatField(db_column='KR-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    wmt_us = models.FloatField(db_column='WMT-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    cost_us = models.FloatField(db_column='COST-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    wba_us = models.FloatField(db_column='WBA-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    mdlz_us = models.FloatField(db_column='MDLZ-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    khc_us = models.FloatField(db_column='KHC-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    hrl_us = models.FloatField(db_column='HRL-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    adm_us = models.FloatField(db_column='ADM-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    sjm_us = models.FloatField(db_column='SJM-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    cag_us = models.FloatField(db_column='CAG-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    gis_us = models.FloatField(db_column='GIS-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    tsn_us = models.FloatField(db_column='TSN-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    cpb_us = models.FloatField(db_column='CPB-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    hsy_us = models.FloatField(db_column='HSY-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    k_us = models.FloatField(db_column='K-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    lw_us = models.FloatField(db_column='LW-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    mkc_us = models.FloatField(db_column='MKC-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    chd_us = models.FloatField(db_column='CHD-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    clx_us = models.FloatField(db_column='CLX-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    cl_us = models.FloatField(db_column='CL-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    pg_us = models.FloatField(db_column='PG-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    kmb_us = models.FloatField(db_column='KMB-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    coty_us = models.FloatField(db_column='COTY-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    el_us = models.FloatField(db_column='EL-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    mo_us = models.FloatField(db_column='MO-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    pm_us = models.FloatField(db_column='PM-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    hal_us = models.FloatField(db_column='HAL-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    hp_us = models.FloatField(db_column='HP-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    fti_us = models.FloatField(db_column='FTI-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    slb_us = models.FloatField(db_column='SLB-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    nov_us = models.FloatField(db_column='NOV-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    bkr_us = models.FloatField(db_column='BKR-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    hes_us = models.FloatField(db_column='HES-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    hfc_us = models.FloatField(db_column='HFC-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    apa_us = models.FloatField(db_column='APA-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    fang_us = models.FloatField(db_column='FANG-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    dvn_us = models.FloatField(db_column='DVN-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    mpc_us = models.FloatField(db_column='MPC-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    xom_us = models.FloatField(db_column='XOM-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    cog_us = models.FloatField(db_column='COG-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    nbl_us = models.FloatField(db_column='NBL-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    vlo_us = models.FloatField(db_column='VLO-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    oxy_us = models.FloatField(db_column='OXY-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    cxo_us = models.FloatField(db_column='CXO-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    cop_us = models.FloatField(db_column='COP-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    pxd_us = models.FloatField(db_column='PXD-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    wmb_us = models.FloatField(db_column='WMB-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    psx_us = models.FloatField(db_column='PSX-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    oke_us = models.FloatField(db_column='OKE-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    eog_us = models.FloatField(db_column='EOG-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    cvx_us = models.FloatField(db_column='CVX-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    kmi_us = models.FloatField(db_column='KMI-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    mro_us = models.FloatField(db_column='MRO-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    c_us = models.FloatField(db_column='C-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    wfc_us = models.FloatField(db_column='WFC-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    rf_us = models.FloatField(db_column='RF-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    sivb_us = models.FloatField(db_column='SIVB-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    frc_us = models.FloatField(db_column='FRC-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    fitb_us = models.FloatField(db_column='FITB-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    mtb_us = models.FloatField(db_column='MTB-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    jpm_us = models.FloatField(db_column='JPM-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    pnc_us = models.FloatField(db_column='PNC-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    hban_us = models.FloatField(db_column='HBAN-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    bac_us = models.FloatField(db_column='BAC-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    key_us = models.FloatField(db_column='KEY-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    pbct_us = models.FloatField(db_column='PBCT-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    tfc_us = models.FloatField(db_column='TFC-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    usb_us = models.FloatField(db_column='USB-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    cfg_us = models.FloatField(db_column='CFG-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    zion_us = models.FloatField(db_column='ZION-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    cma_us = models.FloatField(db_column='CMA-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    amp_us = models.FloatField(db_column='AMP-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    schw_us = models.FloatField(db_column='SCHW-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ms_us = models.FloatField(db_column='MS-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ndaq_us = models.FloatField(db_column='NDAQ-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    etfc_us = models.FloatField(db_column='ETFC-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    stt_us = models.FloatField(db_column='STT-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    cboe_us = models.FloatField(db_column='CBOE-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    cme_us = models.FloatField(db_column='CME-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ivz_us = models.FloatField(db_column='IVZ-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    spgi_us = models.FloatField(db_column='SPGI-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    rjf_us = models.FloatField(db_column='RJF-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    mktx_us = models.FloatField(db_column='MKTX-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    msci_us = models.FloatField(db_column='MSCI-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    trow_us = models.FloatField(db_column='TROW-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    bk_us = models.FloatField(db_column='BK-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ice_us = models.FloatField(db_column='ICE-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ben_us = models.FloatField(db_column='BEN-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    mco_us = models.FloatField(db_column='MCO-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    gs_us = models.FloatField(db_column='GS-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    blk_us = models.FloatField(db_column='BLK-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ntrs_us = models.FloatField(db_column='NTRS-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    syf_us = models.FloatField(db_column='SYF-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    cof_us = models.FloatField(db_column='COF-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    dfs_us = models.FloatField(db_column='DFS-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    axp_us = models.FloatField(db_column='AXP-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    brk_b_us = models.FloatField(db_column='BRK.B-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    gl_us = models.FloatField(db_column='GL-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    aiz_us = models.FloatField(db_column='AIZ-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    cb_us = models.FloatField(db_column='CB-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    l_us = models.FloatField(db_column='L-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    aon_us = models.FloatField(db_column='AON-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    mmc_us = models.FloatField(db_column='MMC-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    afl_us = models.FloatField(db_column='AFL-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    met_us = models.FloatField(db_column='MET-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    cinf_us = models.FloatField(db_column='CINF-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ajg_us = models.FloatField(db_column='AJG-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    pru_us = models.FloatField(db_column='PRU-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    wltw_us = models.FloatField(db_column='WLTW-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    all_us = models.FloatField(db_column='ALL-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    trv_us = models.FloatField(db_column='TRV-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    aig_us = models.FloatField(db_column='AIG-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    pgr_us = models.FloatField(db_column='PGR-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    unm_us = models.FloatField(db_column='UNM-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    pfg_us = models.FloatField(db_column='PFG-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    wrb_us = models.FloatField(db_column='WRB-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    lnc_us = models.FloatField(db_column='LNC-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    hig_us = models.FloatField(db_column='HIG-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    re_us = models.FloatField(db_column='RE-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    regn_us = models.FloatField(db_column='REGN-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    incy_us = models.FloatField(db_column='INCY-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    vrtx_us = models.FloatField(db_column='VRTX-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    biib_us = models.FloatField(db_column='BIIB-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    gild_us = models.FloatField(db_column='GILD-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    abbv_us = models.FloatField(db_column='ABBV-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    amgn_us = models.FloatField(db_column='AMGN-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    alxn_us = models.FloatField(db_column='ALXN-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    xray_us = models.FloatField(db_column='XRAY-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    holx_us = models.FloatField(db_column='HOLX-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    isrg_us = models.FloatField(db_column='ISRG-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    abt_us = models.FloatField(db_column='ABT-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    var_us = models.FloatField(db_column='VAR-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    idxx_us = models.FloatField(db_column='IDXX-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ste_us = models.FloatField(db_column='STE-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    bdx_us = models.FloatField(db_column='BDX-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    abmd_us = models.FloatField(db_column='ABMD-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    syk_us = models.FloatField(db_column='SYK-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    zbh_us = models.FloatField(db_column='ZBH-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    mdt_us = models.FloatField(db_column='MDT-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    tfx_us = models.FloatField(db_column='TFX-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    coo_us = models.FloatField(db_column='COO-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    rmd_us = models.FloatField(db_column='RMD-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ew_us = models.FloatField(db_column='EW-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    algn_us = models.FloatField(db_column='ALGN-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    bax_us = models.FloatField(db_column='BAX-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    dhr_us = models.FloatField(db_column='DHR-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    bsx_us = models.FloatField(db_column='BSX-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    dgx_us = models.FloatField(db_column='DGX-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    unh_us = models.FloatField(db_column='UNH-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    lh_us = models.FloatField(db_column='LH-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    cah_us = models.FloatField(db_column='CAH-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    cnc_us = models.FloatField(db_column='CNC-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    hca_us = models.FloatField(db_column='HCA-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    abc_us = models.FloatField(db_column='ABC-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    antm_us = models.FloatField(db_column='ANTM-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    mck_us = models.FloatField(db_column='MCK-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    cvs_us = models.FloatField(db_column='CVS-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    dva_us = models.FloatField(db_column='DVA-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    hum_us = models.FloatField(db_column='HUM-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ci_us = models.FloatField(db_column='CI-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    uhs_us = models.FloatField(db_column='UHS-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    hsic_us = models.FloatField(db_column='HSIC-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    cern_us = models.FloatField(db_column='CERN-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    a_us = models.FloatField(db_column='A-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    mtd_us = models.FloatField(db_column='MTD-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    iqv_us = models.FloatField(db_column='IQV-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    pki_us = models.FloatField(db_column='PKI-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ilmn_us = models.FloatField(db_column='ILMN-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    wat_us = models.FloatField(db_column='WAT-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    tmo_us = models.FloatField(db_column='TMO-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    myl_us = models.FloatField(db_column='MYL-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    agn_us = models.FloatField(db_column='AGN-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    zts_us = models.FloatField(db_column='ZTS-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    bmy_us = models.FloatField(db_column='BMY-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    jnj_us = models.FloatField(db_column='JNJ-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    pfe_us = models.FloatField(db_column='PFE-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    prgo_us = models.FloatField(db_column='PRGO-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    lly_us = models.FloatField(db_column='LLY-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    mrk_us = models.FloatField(db_column='MRK-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    tdg_us = models.FloatField(db_column='TDG-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ba_us = models.FloatField(db_column='BA-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    gd_us = models.FloatField(db_column='GD-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    rtn_us = models.FloatField(db_column='RTN-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    utx_us = models.FloatField(db_column='UTX-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    arnc_us = models.FloatField(db_column='ARNC-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    lmt_us = models.FloatField(db_column='LMT-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    lhx_us = models.FloatField(db_column='LHX-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    hii_us = models.FloatField(db_column='HII-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    txt_us = models.FloatField(db_column='TXT-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    noc_us = models.FloatField(db_column='NOC-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    fdx_us = models.FloatField(db_column='FDX-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ups_us = models.FloatField(db_column='UPS-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    expd_us = models.FloatField(db_column='EXPD-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    chrw_us = models.FloatField(db_column='CHRW-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    aal_us = models.FloatField(db_column='AAL-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    dal_us = models.FloatField(db_column='DAL-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ual_us = models.FloatField(db_column='UAL-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    luv_us = models.FloatField(db_column='LUV-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    alk_us = models.FloatField(db_column='ALK-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    mas_us = models.FloatField(db_column='MAS-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    alle_us = models.FloatField(db_column='ALLE-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    tt_us = models.FloatField(db_column='TT-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    aos_us = models.FloatField(db_column='AOS-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    jci_us = models.FloatField(db_column='JCI-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    fbhs_us = models.FloatField(db_column='FBHS-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    rsg_us = models.FloatField(db_column='RSG-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    rol_us = models.FloatField(db_column='ROL-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    cprt_us = models.FloatField(db_column='CPRT-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ctas_us = models.FloatField(db_column='CTAS-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    wm_us = models.FloatField(db_column='WM-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    j_us = models.FloatField(db_column='J-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    pwr_us = models.FloatField(db_column='PWR-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    emr_us = models.FloatField(db_column='EMR-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ame_us = models.FloatField(db_column='AME-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    rok_us = models.FloatField(db_column='ROK-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    etn_us = models.FloatField(db_column='ETN-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    rop_us = models.FloatField(db_column='ROP-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ge_us = models.FloatField(db_column='GE-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    mmm_us = models.FloatField(db_column='MMM-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    hon_us = models.FloatField(db_column='HON-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    cmi_us = models.FloatField(db_column='CMI-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ftv_us = models.FloatField(db_column='FTV-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    xyl_us = models.FloatField(db_column='XYL-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    pnr_us = models.FloatField(db_column='PNR-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    itw_us = models.FloatField(db_column='ITW-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    iex_us = models.FloatField(db_column='IEX-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ir_us = models.FloatField(db_column='IR-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    swk_us = models.FloatField(db_column='SWK-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    fls_us = models.FloatField(db_column='FLS-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    pcar_us = models.FloatField(db_column='PCAR-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    de_us = models.FloatField(db_column='DE-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    wab_us = models.FloatField(db_column='WAB-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ph_us = models.FloatField(db_column='PH-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    sna_us = models.FloatField(db_column='SNA-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    dov_us = models.FloatField(db_column='DOV-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    cat_us = models.FloatField(db_column='CAT-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    info_us = models.FloatField(db_column='INFO-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    rhi_us = models.FloatField(db_column='RHI-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    vrsk_us = models.FloatField(db_column='VRSK-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    nlsn_us = models.FloatField(db_column='NLSN-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    efx_us = models.FloatField(db_column='EFX-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ksu_us = models.FloatField(db_column='KSU-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    unp_us = models.FloatField(db_column='UNP-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    csx_us = models.FloatField(db_column='CSX-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    nsc_us = models.FloatField(db_column='NSC-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    jbht_us = models.FloatField(db_column='JBHT-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    odfl_us = models.FloatField(db_column='ODFL-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    uri_us = models.FloatField(db_column='URI-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    fast_us = models.FloatField(db_column='FAST-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    gww_us = models.FloatField(db_column='GWW-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    jnpr_us = models.FloatField(db_column='JNPR-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    anet_us = models.FloatField(db_column='ANET-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ffiv_us = models.FloatField(db_column='FFIV-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    csco_us = models.FloatField(db_column='CSCO-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    msi_us = models.FloatField(db_column='MSI-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ipgp_us = models.FloatField(db_column='IPGP-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    cdw_us = models.FloatField(db_column='CDW-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    flir_us = models.FloatField(db_column='FLIR-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    glw_us = models.FloatField(db_column='GLW-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    aph_us = models.FloatField(db_column='APH-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    tel_us = models.FloatField(db_column='TEL-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    keys_us = models.FloatField(db_column='KEYS-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    zbra_us = models.FloatField(db_column='ZBRA-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    pypl_us = models.FloatField(db_column='PYPL-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    vrsn_us = models.FloatField(db_column='VRSN-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    wu_us = models.FloatField(db_column='WU-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ldos_us = models.FloatField(db_column='LDOS-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    akam_us = models.FloatField(db_column='AKAM-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    br_us = models.FloatField(db_column='BR-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    v_us = models.FloatField(db_column='V-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    it_us = models.FloatField(db_column='IT-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ads_us = models.FloatField(db_column='ADS-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ibm_us = models.FloatField(db_column='IBM-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    jkhy_us = models.FloatField(db_column='JKHY-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    flt_us = models.FloatField(db_column='FLT-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ma_us = models.FloatField(db_column='MA-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    dxc_us = models.FloatField(db_column='DXC-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    adp_us = models.FloatField(db_column='ADP-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    fisv_us = models.FloatField(db_column='FISV-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    acn_us = models.FloatField(db_column='ACN-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    payx_us = models.FloatField(db_column='PAYX-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    fis_us = models.FloatField(db_column='FIS-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    gpn_us = models.FloatField(db_column='GPN-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ctsh_us = models.FloatField(db_column='CTSH-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    amd_us = models.FloatField(db_column='AMD-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    klac_us = models.FloatField(db_column='KLAC-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    xlnx_us = models.FloatField(db_column='XLNX-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    nvda_us = models.FloatField(db_column='NVDA-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    txn_us = models.FloatField(db_column='TXN-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    lrcx_us = models.FloatField(db_column='LRCX-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    avgo_us = models.FloatField(db_column='AVGO-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    qcom_us = models.FloatField(db_column='QCOM-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    mchp_us = models.FloatField(db_column='MCHP-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    mu_us = models.FloatField(db_column='MU-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    adi_us = models.FloatField(db_column='ADI-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    intc_us = models.FloatField(db_column='INTC-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    qrvo_us = models.FloatField(db_column='QRVO-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    amat_us = models.FloatField(db_column='AMAT-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    swks_us = models.FloatField(db_column='SWKS-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    mxim_us = models.FloatField(db_column='MXIM-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    payc_us = models.FloatField(db_column='PAYC-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ctxs_us = models.FloatField(db_column='CTXS-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    intu_us = models.FloatField(db_column='INTU-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    nlok_us = models.FloatField(db_column='NLOK-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    anss_us = models.FloatField(db_column='ANSS-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    msft_us = models.FloatField(db_column='MSFT-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    now_us = models.FloatField(db_column='NOW-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    cdns_us = models.FloatField(db_column='CDNS-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    crm_us = models.FloatField(db_column='CRM-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    adsk_us = models.FloatField(db_column='ADSK-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    snps_us = models.FloatField(db_column='SNPS-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    adbe_us = models.FloatField(db_column='ADBE-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ftnt_us = models.FloatField(db_column='FTNT-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    orcl_us = models.FloatField(db_column='ORCL-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    stx_us = models.FloatField(db_column='STX-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    aapl_us = models.FloatField(db_column='AAPL-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    xrx_us = models.FloatField(db_column='XRX-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ntap_us = models.FloatField(db_column='NTAP-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    hpq_us = models.FloatField(db_column='HPQ-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    hpe_us = models.FloatField(db_column='HPE-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    wdc_us = models.FloatField(db_column='WDC-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    lin_us = models.FloatField(db_column='LIN-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    shw_us = models.FloatField(db_column='SHW-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    dow_us = models.FloatField(db_column='DOW-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ctva_us = models.FloatField(db_column='CTVA-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    mos_us = models.FloatField(db_column='MOS-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    lyb_us = models.FloatField(db_column='LYB-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    cf_us = models.FloatField(db_column='CF-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    dd_us = models.FloatField(db_column='DD-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    fmc_us = models.FloatField(db_column='FMC-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    iff_us = models.FloatField(db_column='IFF-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    apd_us = models.FloatField(db_column='APD-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ecl_us = models.FloatField(db_column='ECL-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    emn_us = models.FloatField(db_column='EMN-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ppg_us = models.FloatField(db_column='PPG-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    alb_us = models.FloatField(db_column='ALB-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ce_us = models.FloatField(db_column='CE-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    vmc_us = models.FloatField(db_column='VMC-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    mlm_us = models.FloatField(db_column='MLM-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    see_us = models.FloatField(db_column='SEE-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    wrk_us = models.FloatField(db_column='WRK-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    bll_us = models.FloatField(db_column='BLL-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ip_us = models.FloatField(db_column='IP-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    amcr_us = models.FloatField(db_column='AMCR-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    avy_us = models.FloatField(db_column='AVY-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    pkg_us = models.FloatField(db_column='PKG-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    nem_us = models.FloatField(db_column='NEM-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    fcx_us = models.FloatField(db_column='FCX-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    nue_us = models.FloatField(db_column='NUE-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    o_us = models.FloatField(db_column='O-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    pld_us = models.FloatField(db_column='PLD-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    reg_us = models.FloatField(db_column='REG-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    spg_us = models.FloatField(db_column='SPG-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    sbac_us = models.FloatField(db_column='SBAC-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    eqr_us = models.FloatField(db_column='EQR-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    vno_us = models.FloatField(db_column='VNO-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    dre_us = models.FloatField(db_column='DRE-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    maa_us = models.FloatField(db_column='MAA-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    avb_us = models.FloatField(db_column='AVB-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    frt_us = models.FloatField(db_column='FRT-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    eqix_us = models.FloatField(db_column='EQIX-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    irm_us = models.FloatField(db_column='IRM-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    bxp_us = models.FloatField(db_column='BXP-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    aiv_us = models.FloatField(db_column='AIV-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    slg_us = models.FloatField(db_column='SLG-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    are_us = models.FloatField(db_column='ARE-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    hst_us = models.FloatField(db_column='HST-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    peak_us = models.FloatField(db_column='PEAK-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    wy_us = models.FloatField(db_column='WY-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    well_us = models.FloatField(db_column='WELL-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    psa_us = models.FloatField(db_column='PSA-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    exr_us = models.FloatField(db_column='EXR-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    cci_us = models.FloatField(db_column='CCI-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ess_us = models.FloatField(db_column='ESS-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    kim_us = models.FloatField(db_column='KIM-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    vtr_us = models.FloatField(db_column='VTR-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    dlr_us = models.FloatField(db_column='DLR-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    amt_us = models.FloatField(db_column='AMT-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    udr_us = models.FloatField(db_column='UDR-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    cbre_us = models.FloatField(db_column='CBRE-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    evrg_us = models.FloatField(db_column='EVRG-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    xel_us = models.FloatField(db_column='XEL-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    so_us = models.FloatField(db_column='SO-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    eix_us = models.FloatField(db_column='EIX-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ppl_us = models.FloatField(db_column='PPL-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    pnw_us = models.FloatField(db_column='PNW-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    lnt_us = models.FloatField(db_column='LNT-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    aep_us = models.FloatField(db_column='AEP-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    exc_us = models.FloatField(db_column='EXC-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    etr_us = models.FloatField(db_column='ETR-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    duk_us = models.FloatField(db_column='DUK-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    es_us = models.FloatField(db_column='ES-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    nrg_us = models.FloatField(db_column='NRG-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    nee_us = models.FloatField(db_column='NEE-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    fe_us = models.FloatField(db_column='FE-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ato_us = models.FloatField(db_column='ATO-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    aes_us = models.FloatField(db_column='AES-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    peg_us = models.FloatField(db_column='PEG-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    dte_us = models.FloatField(db_column='DTE-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ni_us = models.FloatField(db_column='NI-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    aee_us = models.FloatField(db_column='AEE-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ed_us = models.FloatField(db_column='ED-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    wec_us = models.FloatField(db_column='WEC-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    sre_us = models.FloatField(db_column='SRE-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    cms_us = models.FloatField(db_column='CMS-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    d_us = models.FloatField(db_column='D-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    cnp_us = models.FloatField(db_column='CNP-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    awk_us = models.FloatField(db_column='AWK-US', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'US_Volume_Data_Old'


class UsGicswisecompaniesclassification(models.Model):
    gics = models.CharField(db_column='GICS', max_length=255, blank=True, null=True)  # Field name made lowercase.
    description = models.TextField(db_column='Description', blank=True, null=True)  # Field name made lowercase. This field type is a guess.
    bottom_tier = models.CharField(db_column='Bottom Tier', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    mid_tier = models.CharField(db_column='Mid Tier', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    top_tier = models.CharField(db_column='Top Tier', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    rating = models.CharField(db_column='Rating', max_length=255, blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'US_gicswisecompaniesclassification'


class UsWizardtable(models.Model):
    date = models.TextField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    index_name = models.TextField(blank=True, null=True)
    company = models.TextField(db_column='Company', blank=True, null=True)  # Field name made lowercase.
    factset_ticker = models.TextField(db_column='Factset_Ticker', blank=True, null=True)  # Field name made lowercase.
    security_code = models.TextField(db_column='Security Code', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    price = models.FloatField(db_column='Price', blank=True, null=True)  # Field name made lowercase.
    return_field = models.FloatField(db_column='return', blank=True, null=True)  # Field renamed because it was a Python reserved word.
    number_52_week_high = models.FloatField(db_column='52_Week_High', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    current_price = models.FloatField(db_column='Current_Price', blank=True, null=True)  # Field name made lowercase.
    piotroski_score = models.FloatField(db_column='Piotroski_Score', blank=True, null=True)  # Field name made lowercase.
    div_yield = models.FloatField(db_column='Div_Yield', blank=True, null=True)  # Field name made lowercase.
    div_payout = models.FloatField(db_column='Div_Payout', blank=True, null=True)  # Field name made lowercase.
    roe = models.FloatField(db_column='ROE', blank=True, null=True)  # Field name made lowercase.
    market_cap_category = models.TextField(db_column='Market_Cap_Category', blank=True, null=True)  # Field name made lowercase.
    gics = models.TextField(db_column='GICS', blank=True, null=True)  # Field name made lowercase.
    market_cap = models.FloatField(db_column='Market Cap', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    rsi = models.FloatField(db_column='RSI', blank=True, null=True)  # Field name made lowercase.
    ma9 = models.FloatField(db_column='MA9', blank=True, null=True)  # Field name made lowercase.
    ma26 = models.FloatField(db_column='MA26', blank=True, null=True)  # Field name made lowercase.
    tier = models.TextField(db_column='Tier', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'US_wizardTable'


class UniversePrices(models.Model):
    date = models.DateField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    company = models.TextField(db_column='Company', blank=True, null=True)  # Field name made lowercase.
    ticker = models.TextField(db_column='Ticker', blank=True, null=True)  # Field name made lowercase.
    price = models.FloatField(db_column='Price', blank=True, null=True)  # Field name made lowercase.
    volume = models.BigIntegerField(db_column='Volume', blank=True, null=True)  # Field name made lowercase.
    dividends = models.FloatField(db_column='Dividends', blank=True, null=True)  # Field name made lowercase.
    stock_splits = models.BigIntegerField(db_column='Stock Splits', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Universe_Prices'


class UniversePricesMain(models.Model):
    date = models.DateField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    company = models.TextField(db_column='Company')  # Field name made lowercase.
    price = models.FloatField(db_column='Price', blank=True, null=True)  # Field name made lowercase.
    factset_ticker = models.TextField(db_column='Factset_Ticker', blank=True, null=True)  # Field name made lowercase.
    return_field = models.FloatField(db_column='Return', blank=True, null=True)  # Field name made lowercase. Field renamed because it was a Python reserved word.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Universe_Prices_Main'


class UsScreening(models.Model):
    fs_name = models.TextField(db_column='FS Name', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    security_code = models.TextField(db_column='Security Code', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    fs_ticker = models.TextField(db_column='FS Ticker', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    market_cap = models.FloatField(db_column='Market Cap', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    market_cap_category = models.TextField(db_column='Market_Cap_Category', blank=True, null=True)  # Field name made lowercase.
    sales = models.FloatField(db_column='Sales', blank=True, null=True)  # Field name made lowercase.
    gics = models.TextField(db_column='GICS', blank=True, null=True)  # Field name made lowercase.
    beta_1y = models.FloatField(db_column='Beta_1Y', blank=True, null=True)  # Field name made lowercase.
    volatility_1y = models.FloatField(db_column='Volatility_1Y', blank=True, null=True)  # Field name made lowercase.
    piotroski_score = models.FloatField(db_column='Piotroski_Score', blank=True, null=True)  # Field name made lowercase.
    pe = models.FloatField(db_column='PE', blank=True, null=True)  # Field name made lowercase.
    pb = models.FloatField(db_column='PB', blank=True, null=True)  # Field name made lowercase.
    ev_sales = models.FloatField(db_column='EV/Sales', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ev_ebitda = models.FloatField(db_column='EV/EBITDA', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    div_yield = models.FloatField(db_column='Div_Yield', blank=True, null=True)  # Field name made lowercase.
    sales_growth = models.FloatField(db_column='Sales_Growth', blank=True, null=True)  # Field name made lowercase.
    sales_growth_3y_5r_cagr = models.FloatField(db_column='Sales_Growth_3Y/5R_CAGR', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    eps_growth = models.FloatField(db_column='EPS_Growth', blank=True, null=True)  # Field name made lowercase.
    eps_growth_3y_5r_cagr = models.FloatField(db_column='EPS_Growth_3Y/5R_CAGR', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    roe = models.FloatField(db_column='ROE', blank=True, null=True)  # Field name made lowercase.
    roce = models.FloatField(db_column='ROCE', blank=True, null=True)  # Field name made lowercase.
    fcf_margin = models.FloatField(db_column='FCF_Margin', blank=True, null=True)  # Field name made lowercase.
    sales_total_assets = models.FloatField(db_column='Sales/Total Assets', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    net_debt_fcf = models.FloatField(db_column='Net_Debt/FCF', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    net_debt_ebitda = models.FloatField(db_column='Net_Debt/EBITDA', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    debt_equity = models.FloatField(db_column='Debt/Equity', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    price = models.FloatField(db_column='Price', blank=True, null=True)  # Field name made lowercase.
    f_o = models.TextField(db_column='F_O', blank=True, null=True)  # Field name made lowercase.
    volume = models.FloatField(db_column='Volume', blank=True, null=True)  # Field name made lowercase.
    rsi = models.FloatField(db_column='RSI', blank=True, null=True)  # Field name made lowercase.
    ma20 = models.FloatField(db_column='MA20', blank=True, null=True)  # Field name made lowercase.
    ma50 = models.FloatField(db_column='MA50', blank=True, null=True)  # Field name made lowercase.
    ma200 = models.FloatField(db_column='MA200', blank=True, null=True)  # Field name made lowercase.
    number_52_week_high = models.FloatField(db_column='52_Week_High', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    target_price = models.FloatField(db_column='Target_Price', blank=True, null=True)  # Field name made lowercase.
    volume_20 = models.FloatField(db_column='Volume_20', blank=True, null=True)  # Field name made lowercase.
    rsi_30 = models.FloatField(db_column='RSI_30', blank=True, null=True)  # Field name made lowercase.
    rsi_90 = models.FloatField(db_column='RSI_90', blank=True, null=True)  # Field name made lowercase.
    share_turnover = models.FloatField(db_column='Share Turnover', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    volume_change = models.FloatField(db_column='Volume_Change', blank=True, null=True)  # Field name made lowercase.
    mtd = models.FloatField(db_column='MTD', blank=True, null=True)  # Field name made lowercase.
    ytd = models.FloatField(db_column='YTD', blank=True, null=True)  # Field name made lowercase.
    number_1y = models.FloatField(db_column='1Y', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_1m = models.FloatField(db_column='1M', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_3m = models.FloatField(db_column='3M', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    number_6m = models.FloatField(db_column='6M', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    qtd = models.FloatField(db_column='QTD', blank=True, null=True)  # Field name made lowercase.
    gross_profit_margin = models.FloatField(blank=True, null=True)
    id = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'Us_Screening'


class Volume20Data(models.Model):
    date = models.DateField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    ticker = models.TextField(db_column='Ticker', blank=True, null=True)  # Field name made lowercase.
    volume_20 = models.FloatField(db_column='Volume_20', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Volume_20_Data'


class VolumeData(models.Model):
    date = models.DateField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    factor = models.TextField(db_column='Factor', blank=True, null=True)  # Field name made lowercase.
    ticker = models.TextField(db_column='Ticker', blank=True, null=True)  # Field name made lowercase.
    value = models.FloatField(blank=True, null=True)
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Volume_Data'


class VolumeDataOld(models.Model):
    date = models.DateField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    factor = models.TextField(db_column='Factor', blank=True, null=True)  # Field name made lowercase.
    number_506590_in = models.FloatField(db_column='506590-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_506655_in = models.FloatField(db_column='506655-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_506943_in = models.FloatField(db_column='506943-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_507685_in = models.FloatField(db_column='507685-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_507880_in = models.FloatField(db_column='507880-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_509243_in = models.FloatField(db_column='509243-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_509930_in = models.FloatField(db_column='509930-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_509966_in = models.FloatField(db_column='509966-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_511218_in = models.FloatField(db_column='511218-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_512070_in = models.FloatField(db_column='512070-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_512179_in = models.FloatField(db_column='512179-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_512237_in = models.FloatField(db_column='512237-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_513023_in = models.FloatField(db_column='513023-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_513377_in = models.FloatField(db_column='513377-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_513434_in = models.FloatField(db_column='513434-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_513683_in = models.FloatField(db_column='513683-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_514162_in = models.FloatField(db_column='514162-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_517334_in = models.FloatField(db_column='517334-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_517385_in = models.FloatField(db_column='517385-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_517506_in = models.FloatField(db_column='517506-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_517569_in = models.FloatField(db_column='517569-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_520051_in = models.FloatField(db_column='520051-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_520111_in = models.FloatField(db_column='520111-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_521064_in = models.FloatField(db_column='521064-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_522113_in = models.FloatField(db_column='522113-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_522287_in = models.FloatField(db_column='522287-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_523261_in = models.FloatField(db_column='523261-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_523385_in = models.FloatField(db_column='523385-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_523398_in = models.FloatField(db_column='523398-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_523405_in = models.FloatField(db_column='523405-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_523457_in = models.FloatField(db_column='523457-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_523598_in = models.FloatField(db_column='523598-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_523642_in = models.FloatField(db_column='523642-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_523756_in = models.FloatField(db_column='523756-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_524000_in = models.FloatField(db_column='524000-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_524200_in = models.FloatField(db_column='524200-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_524230_in = models.FloatField(db_column='524230-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_524715_in = models.FloatField(db_column='524715-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_524816_in = models.FloatField(db_column='524816-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_526299_in = models.FloatField(db_column='526299-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_526371_in = models.FloatField(db_column='526371-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_526947_in = models.FloatField(db_column='526947-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_530007_in = models.FloatField(db_column='530007-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_530019_in = models.FloatField(db_column='530019-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_530239_in = models.FloatField(db_column='530239-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_530517_in = models.FloatField(db_column='530517-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_530549_in = models.FloatField(db_column='530549-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_530813_in = models.FloatField(db_column='530813-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_531213_in = models.FloatField(db_column='531213-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_531335_in = models.FloatField(db_column='531335-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_531426_in = models.FloatField(db_column='531426-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_531500_in = models.FloatField(db_column='531500-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_531548_in = models.FloatField(db_column='531548-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_531642_in = models.FloatField(db_column='531642-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_531910_in = models.FloatField(db_column='531910-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532144_in = models.FloatField(db_column='532144-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532189_in = models.FloatField(db_column='532189-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532209_in = models.FloatField(db_column='532209-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532218_in = models.FloatField(db_column='532218-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532221_in = models.FloatField(db_column='532221-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532234_in = models.FloatField(db_column='532234-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532276_in = models.FloatField(db_column='532276-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532286_in = models.FloatField(db_column='532286-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532300_in = models.FloatField(db_column='532300-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532301_in = models.FloatField(db_column='532301-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532313_in = models.FloatField(db_column='532313-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532343_in = models.FloatField(db_column='532343-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532374_in = models.FloatField(db_column='532374-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532388_in = models.FloatField(db_column='532388-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532461_in = models.FloatField(db_column='532461-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532466_in = models.FloatField(db_column='532466-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532477_in = models.FloatField(db_column='532477-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532478_in = models.FloatField(db_column='532478-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532497_in = models.FloatField(db_column='532497-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532498_in = models.FloatField(db_column='532498-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532500_in = models.FloatField(db_column='532500-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532504_in = models.FloatField(db_column='532504-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532505_in = models.FloatField(db_column='532505-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532508_in = models.FloatField(db_column='532508-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532509_in = models.FloatField(db_column='532509-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532515_in = models.FloatField(db_column='532515-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532522_in = models.FloatField(db_column='532522-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532524_in = models.FloatField(db_column='532524-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532525_in = models.FloatField(db_column='532525-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532531_in = models.FloatField(db_column='532531-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532532_in = models.FloatField(db_column='532532-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532538_in = models.FloatField(db_column='532538-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532539_in = models.FloatField(db_column='532539-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532540_in = models.FloatField(db_column='532540-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532541_in = models.FloatField(db_column='532541-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532555_in = models.FloatField(db_column='532555-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532636_in = models.FloatField(db_column='532636-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532638_in = models.FloatField(db_column='532638-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532642_in = models.FloatField(db_column='532642-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532644_in = models.FloatField(db_column='532644-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532648_in = models.FloatField(db_column='532648-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532652_in = models.FloatField(db_column='532652-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532659_in = models.FloatField(db_column='532659-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532667_in = models.FloatField(db_column='532667-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532689_in = models.FloatField(db_column='532689-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532705_in = models.FloatField(db_column='532705-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532706_in = models.FloatField(db_column='532706-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532710_in = models.FloatField(db_column='532710-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532712_in = models.FloatField(db_column='532712-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532714_in = models.FloatField(db_column='532714-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532720_in = models.FloatField(db_column='532720-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532725_in = models.FloatField(db_column='532725-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532733_in = models.FloatField(db_column='532733-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532755_in = models.FloatField(db_column='532755-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532756_in = models.FloatField(db_column='532756-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532777_in = models.FloatField(db_column='532777-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532779_in = models.FloatField(db_column='532779-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532784_in = models.FloatField(db_column='532784-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532800_in = models.FloatField(db_column='532800-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532805_in = models.FloatField(db_column='532805-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532810_in = models.FloatField(db_column='532810-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532814_in = models.FloatField(db_column='532814-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532819_in = models.FloatField(db_column='532819-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532822_in = models.FloatField(db_column='532822-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532827_in = models.FloatField(db_column='532827-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532832_in = models.FloatField(db_column='532832-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532856_in = models.FloatField(db_column='532856-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532865_in = models.FloatField(db_column='532865-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532872_in = models.FloatField(db_column='532872-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532880_in = models.FloatField(db_column='532880-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532889_in = models.FloatField(db_column='532889-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532890_in = models.FloatField(db_column='532890-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532892_in = models.FloatField(db_column='532892-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532898_in = models.FloatField(db_column='532898-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532926_in = models.FloatField(db_column='532926-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532939_in = models.FloatField(db_column='532939-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532942_in = models.FloatField(db_column='532942-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532947_in = models.FloatField(db_column='532947-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532953_in = models.FloatField(db_column='532953-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532955_in = models.FloatField(db_column='532955-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532960_in = models.FloatField(db_column='532960-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_533023_in = models.FloatField(db_column='533023-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_533088_in = models.FloatField(db_column='533088-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_533098_in = models.FloatField(db_column='533098-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_533106_in = models.FloatField(db_column='533106-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_533148_in = models.FloatField(db_column='533148-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_533155_in = models.FloatField(db_column='533155-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_533171_in = models.FloatField(db_column='533171-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_533179_in = models.FloatField(db_column='533179-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_533206_in = models.FloatField(db_column='533206-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_533269_in = models.FloatField(db_column='533269-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_533273_in = models.FloatField(db_column='533273-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_533274_in = models.FloatField(db_column='533274-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_533286_in = models.FloatField(db_column='533286-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_533398_in = models.FloatField(db_column='533398-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_533519_in = models.FloatField(db_column='533519-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_533520_in = models.FloatField(db_column='533520-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_533655_in = models.FloatField(db_column='533655-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_534091_in = models.FloatField(db_column='534091-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_534139_in = models.FloatField(db_column='534139-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_534309_in = models.FloatField(db_column='534309-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_534690_in = models.FloatField(db_column='534690-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_534809_in = models.FloatField(db_column='534809-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_534816_in = models.FloatField(db_column='534816-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_534976_in = models.FloatField(db_column='534976-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_535322_in = models.FloatField(db_column='535322-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_535648_in = models.FloatField(db_column='535648-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_535754_in = models.FloatField(db_column='535754-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_535789_in = models.FloatField(db_column='535789-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_538835_in = models.FloatField(db_column='538835-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_538962_in = models.FloatField(db_column='538962-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_539083_in = models.FloatField(db_column='539083-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_539150_in = models.FloatField(db_column='539150-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_539268_in = models.FloatField(db_column='539268-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_539437_in = models.FloatField(db_column='539437-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_539448_in = models.FloatField(db_column='539448-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_539450_in = models.FloatField(db_column='539450-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_539524_in = models.FloatField(db_column='539524-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_539542_in = models.FloatField(db_column='539542-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_539551_in = models.FloatField(db_column='539551-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_539597_in = models.FloatField(db_column='539597-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_539658_in = models.FloatField(db_column='539658-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_539807_in = models.FloatField(db_column='539807-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_539871_in = models.FloatField(db_column='539871-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_539874_in = models.FloatField(db_column='539874-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_539889_in = models.FloatField(db_column='539889-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_539957_in = models.FloatField(db_column='539957-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_539978_in = models.FloatField(db_column='539978-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_539981_in = models.FloatField(db_column='539981-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_540005_in = models.FloatField(db_column='540005-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_540064_in = models.FloatField(db_column='540064-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_540065_in = models.FloatField(db_column='540065-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_540115_in = models.FloatField(db_column='540115-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_540133_in = models.FloatField(db_column='540133-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_540173_in = models.FloatField(db_column='540173-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_540180_in = models.FloatField(db_column='540180-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_540203_in = models.FloatField(db_column='540203-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_540222_in = models.FloatField(db_column='540222-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_540425_in = models.FloatField(db_column='540425-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_540530_in = models.FloatField(db_column='540530-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_540575_in = models.FloatField(db_column='540575-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_540595_in = models.FloatField(db_column='540595-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_540673_in = models.FloatField(db_column='540673-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_540716_in = models.FloatField(db_column='540716-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_540719_in = models.FloatField(db_column='540719-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_540743_in = models.FloatField(db_column='540743-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_540749_in = models.FloatField(db_column='540749-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_540750_in = models.FloatField(db_column='540750-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_540755_in = models.FloatField(db_column='540755-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_540762_in = models.FloatField(db_column='540762-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_540767_in = models.FloatField(db_column='540767-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_540768_in = models.FloatField(db_column='540768-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_540769_in = models.FloatField(db_column='540769-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_540777_in = models.FloatField(db_column='540777-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_540935_in = models.FloatField(db_column='540935-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_541154_in = models.FloatField(db_column='541154-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_541179_in = models.FloatField(db_column='541179-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_541233_in = models.FloatField(db_column='541233-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_541301_in = models.FloatField(db_column='541301-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_541336_in = models.FloatField(db_column='541336-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_541556_in = models.FloatField(db_column='541556-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_541578_in = models.FloatField(db_column='541578-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_541700_in = models.FloatField(db_column='541700-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_541729_in = models.FloatField(db_column='541729-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_541956_in = models.FloatField(db_column='541956-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_542649_in = models.FloatField(db_column='542649-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_542650_in = models.FloatField(db_column='542650-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_542652_in = models.FloatField(db_column='542652-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_523395_in = models.FloatField(db_column='523395-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_541988_in = models.FloatField(db_column='541988-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500002_in = models.FloatField(db_column='500002-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500488_in = models.FloatField(db_column='500488-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_540691_in = models.FloatField(db_column='540691-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_535755_in = models.FloatField(db_column='535755-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500410_in = models.FloatField(db_column='500410-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_512599_in = models.FloatField(db_column='512599-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_542066_in = models.FloatField(db_column='542066-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_541450_in = models.FloatField(db_column='541450-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532921_in = models.FloatField(db_column='532921-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_533096_in = models.FloatField(db_column='533096-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_539254_in = models.FloatField(db_column='539254-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_540025_in = models.FloatField(db_column='540025-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500003_in = models.FloatField(db_column='500003-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532683_in = models.FloatField(db_column='532683-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532331_in = models.FloatField(db_column='532331-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500710_in = models.FloatField(db_column='500710-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532480_in = models.FloatField(db_column='532480-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_539523_in = models.FloatField(db_column='539523-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532749_in = models.FloatField(db_column='532749-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500008_in = models.FloatField(db_column='500008-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500425_in = models.FloatField(db_column='500425-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532418_in = models.FloatField(db_column='532418-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532259_in = models.FloatField(db_column='532259-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_533758_in = models.FloatField(db_column='533758-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_533573_in = models.FloatField(db_column='533573-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_508869_in = models.FloatField(db_column='508869-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500877_in = models.FloatField(db_column='500877-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_533271_in = models.FloatField(db_column='533271-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500477_in = models.FloatField(db_column='500477-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500820_in = models.FloatField(db_column='500820-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_540975_in = models.FloatField(db_column='540975-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532830_in = models.FloatField(db_column='532830-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_506820_in = models.FloatField(db_column='506820-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500027_in = models.FloatField(db_column='500027-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_540611_in = models.FloatField(db_column='540611-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_524804_in = models.FloatField(db_column='524804-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_512573_in = models.FloatField(db_column='512573-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532215_in = models.FloatField(db_column='532215-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532977_in = models.FloatField(db_column='532977-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_533229_in = models.FloatField(db_column='533229-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500031_in = models.FloatField(db_column='500031-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532978_in = models.FloatField(db_column='532978-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500490_in = models.FloatField(db_column='500490-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500034_in = models.FloatField(db_column='500034-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_502355_in = models.FloatField(db_column='502355-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_523319_in = models.FloatField(db_column='523319-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500038_in = models.FloatField(db_column='500038-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_541153_in = models.FloatField(db_column='541153-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532134_in = models.FloatField(db_column='532134-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532149_in = models.FloatField(db_column='532149-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500042_in = models.FloatField(db_column='500042-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500043_in = models.FloatField(db_column='500043-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_506285_in = models.FloatField(db_column='506285-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_501425_in = models.FloatField(db_column='501425-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_541143_in = models.FloatField(db_column='541143-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500049_in = models.FloatField(db_column='500049-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500048_in = models.FloatField(db_column='500048-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_509480_in = models.FloatField(db_column='509480-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500493_in = models.FloatField(db_column='500493-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532454_in = models.FloatField(db_column='532454-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500103_in = models.FloatField(db_column='500103-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532523_in = models.FloatField(db_column='532523-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500335_in = models.FloatField(db_column='500335-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_506197_in = models.FloatField(db_column='506197-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_526612_in = models.FloatField(db_column='526612-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500067_in = models.FloatField(db_column='500067-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500020_in = models.FloatField(db_column='500020-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500530_in = models.FloatField(db_column='500530-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500547_in = models.FloatField(db_column='500547-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500825_in = models.FloatField(db_column='500825-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532321_in = models.FloatField(db_column='532321-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532483_in = models.FloatField(db_column='532483-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_511196_in = models.FloatField(db_column='511196-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_524742_in = models.FloatField(db_column='524742-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_513375_in = models.FloatField(db_column='513375-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_534804_in = models.FloatField(db_column='534804-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500870_in = models.FloatField(db_column='500870-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_519600_in = models.FloatField(db_column='519600-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500878_in = models.FloatField(db_column='500878-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532885_in = models.FloatField(db_column='532885-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_501150_in = models.FloatField(db_column='501150-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532548_in = models.FloatField(db_column='532548-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532443_in = models.FloatField(db_column='532443-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500093_in = models.FloatField(db_column='500093-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_542399_in = models.FloatField(db_column='542399-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500085_in = models.FloatField(db_column='500085-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500110_in = models.FloatField(db_column='500110-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_511243_in = models.FloatField(db_column='511243-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_504973_in = models.FloatField(db_column='504973-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500087_in = models.FloatField(db_column='500087-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_533278_in = models.FloatField(db_column='533278-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_540678_in = models.FloatField(db_column='540678-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_539436_in = models.FloatField(db_column='539436-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500830_in = models.FloatField(db_column='500830-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_531344_in = models.FloatField(db_column='531344-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_506395_in = models.FloatField(db_column='506395-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532179_in = models.FloatField(db_column='532179-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_541770_in = models.FloatField(db_column='541770-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500092_in = models.FloatField(db_column='500092-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_539876_in = models.FloatField(db_column='539876-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532210_in = models.FloatField(db_column='532210-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500480_in = models.FloatField(db_column='500480-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532175_in = models.FloatField(db_column='532175-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500096_in = models.FloatField(db_column='500096-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_542216_in = models.FloatField(db_column='542216-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_533151_in = models.FloatField(db_column='533151-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_540047_in = models.FloatField(db_column='540047-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_540701_in = models.FloatField(db_column='540701-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532772_in = models.FloatField(db_column='532772-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_523367_in = models.FloatField(db_column='523367-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500645_in = models.FloatField(db_column='500645-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_506401_in = models.FloatField(db_column='506401-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532848_in = models.FloatField(db_column='532848-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_507717_in = models.FloatField(db_column='507717-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_511072_in = models.FloatField(db_column='511072-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532839_in = models.FloatField(db_column='532839-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532488_in = models.FloatField(db_column='532488-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532868_in = models.FloatField(db_column='532868-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_540376_in = models.FloatField(db_column='540376-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500124_in = models.FloatField(db_column='500124-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532927_in = models.FloatField(db_column='532927-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532922_in = models.FloatField(db_column='532922-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_505200_in = models.FloatField(db_column='505200-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500125_in = models.FloatField(db_column='500125-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500840_in = models.FloatField(db_column='500840-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_531162_in = models.FloatField(db_column='531162-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_540153_in = models.FloatField(db_column='540153-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532178_in = models.FloatField(db_column='532178-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_539844_in = models.FloatField(db_column='539844-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_540596_in = models.FloatField(db_column='540596-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500495_in = models.FloatField(db_column='500495-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500135_in = models.FloatField(db_column='500135-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_531508_in = models.FloatField(db_column='531508-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500086_in = models.FloatField(db_column='500086-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_533400_in = models.FloatField(db_column='533400-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_531599_in = models.FloatField(db_column='531599-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500469_in = models.FloatField(db_column='500469-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500144_in = models.FloatField(db_column='500144-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_541557_in = models.FloatField(db_column='541557-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500940_in = models.FloatField(db_column='500940-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_536507_in = models.FloatField(db_column='536507-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500033_in = models.FloatField(db_column='500033-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532843_in = models.FloatField(db_column='532843-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532809_in = models.FloatField(db_column='532809-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532155_in = models.FloatField(db_column='532155-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532767_in = models.FloatField(db_column='532767-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532622_in = models.FloatField(db_column='532622-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532309_in = models.FloatField(db_column='532309-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500620_in = models.FloatField(db_column='500620-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_522275_in = models.FloatField(db_column='522275-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500171_in = models.FloatField(db_column='500171-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_511676_in = models.FloatField(db_column='511676-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_507815_in = models.FloatField(db_column='507815-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500660_in = models.FloatField(db_column='500660-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532296_in = models.FloatField(db_column='532296-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532181_in = models.FloatField(db_column='532181-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532754_in = models.FloatField(db_column='532754-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500670_in = models.FloatField(db_column='500670-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500163_in = models.FloatField(db_column='500163-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532424_in = models.FloatField(db_column='532424-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500164_in = models.FloatField(db_column='500164-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_533150_in = models.FloatField(db_column='533150-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_533248_in = models.FloatField(db_column='533248-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532482_in = models.FloatField(db_column='532482-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_509488_in = models.FloatField(db_column='509488-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500300_in = models.FloatField(db_column='500300-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_501455_in = models.FloatField(db_column='501455-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_506076_in = models.FloatField(db_column='506076-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500690_in = models.FloatField(db_column='500690-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500676_in = models.FloatField(db_column='500676-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532702_in = models.FloatField(db_column='532702-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_530001_in = models.FloatField(db_column='530001-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_539336_in = models.FloatField(db_column='539336-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_538567_in = models.FloatField(db_column='538567-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_533162_in = models.FloatField(db_column='533162-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_517354_in = models.FloatField(db_column='517354-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532281_in = models.FloatField(db_column='532281-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500010_in = models.FloatField(db_column='500010-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500180_in = models.FloatField(db_column='500180-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_509631_in = models.FloatField(db_column='509631-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500292_in = models.FloatField(db_column='500292-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_519552_in = models.FloatField(db_column='519552-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500182_in = models.FloatField(db_column='500182-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532129_in = models.FloatField(db_column='532129-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500183_in = models.FloatField(db_column='500183-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_514043_in = models.FloatField(db_column='514043-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500440_in = models.FloatField(db_column='500440-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_513599_in = models.FloatField(db_column='513599-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500104_in = models.FloatField(db_column='500104-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500696_in = models.FloatField(db_column='500696-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500188_in = models.FloatField(db_column='500188-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_517174_in = models.FloatField(db_column='517174-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500184_in = models.FloatField(db_column='500184-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532174_in = models.FloatField(db_column='532174-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500116_in = models.FloatField(db_column='500116-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_505726_in = models.FloatField(db_column='505726-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500106_in = models.FloatField(db_column='500106-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532514_in = models.FloatField(db_column='532514-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500850_in = models.FloatField(db_column='500850-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_530005_in = models.FloatField(db_column='530005-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_532187_in = models.FloatField(db_column='532187-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500209_in = models.FloatField(db_column='500209-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_530965_in = models.FloatField(db_column='530965-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_524494_in = models.FloatField(db_column='524494-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500875_in = models.FloatField(db_column='500875-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_509496_in = models.FloatField(db_column='509496-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_523610_in = models.FloatField(db_column='523610-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500378_in = models.FloatField(db_column='500378-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500219_in = models.FloatField(db_column='500219-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500380_in = models.FloatField(db_column='500380-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500228_in = models.FloatField(db_column='500228-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500233_in = models.FloatField(db_column='500233-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500165_in = models.FloatField(db_column='500165-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500247_in = models.FloatField(db_column='500247-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500252_in = models.FloatField(db_column='500252-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500253_in = models.FloatField(db_column='500253-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500510_in = models.FloatField(db_column='500510-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500257_in = models.FloatField(db_column='500257-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500520_in = models.FloatField(db_column='500520-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500266_in = models.FloatField(db_column='500266-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500265_in = models.FloatField(db_column='500265-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500271_in = models.FloatField(db_column='500271-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500290_in = models.FloatField(db_column='500290-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500109_in = models.FloatField(db_column='500109-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500294_in = models.FloatField(db_column='500294-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_505355_in = models.FloatField(db_column='505355-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500790_in = models.FloatField(db_column='500790-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500730_in = models.FloatField(db_column='500730-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500312_in = models.FloatField(db_column='500312-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500315_in = models.FloatField(db_column='500315-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500302_in = models.FloatField(db_column='500302-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500680_in = models.FloatField(db_column='500680-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500459_in = models.FloatField(db_column='500459-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500126_in = models.FloatField(db_column='500126-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_503100_in = models.FloatField(db_column='503100-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500331_in = models.FloatField(db_column='500331-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500338_in = models.FloatField(db_column='500338-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500339_in = models.FloatField(db_column='500339-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500355_in = models.FloatField(db_column='500355-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500260_in = models.FloatField(db_column='500260-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500330_in = models.FloatField(db_column='500330-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500111_in = models.FloatField(db_column='500111-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500325_in = models.FloatField(db_column='500325-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500390_in = models.FloatField(db_column='500390-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500113_in = models.FloatField(db_column='500113-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500674_in = models.FloatField(db_column='500674-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500112_in = models.FloatField(db_column='500112-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_505790_in = models.FloatField(db_column='505790-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500387_in = models.FloatField(db_column='500387-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500550_in = models.FloatField(db_column='500550-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500472_in = models.FloatField(db_column='500472-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500285_in = models.FloatField(db_column='500285-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_503806_in = models.FloatField(db_column='503806-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500403_in = models.FloatField(db_column='500403-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_503310_in = models.FloatField(db_column='503310-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500770_in = models.FloatField(db_column='500770-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500408_in = models.FloatField(db_column='500408-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500800_in = models.FloatField(db_column='500800-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_501301_in = models.FloatField(db_column='501301-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500570_in = models.FloatField(db_column='500570-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500400_in = models.FloatField(db_column='500400-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500470_in = models.FloatField(db_column='500470-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500411_in = models.FloatField(db_column='500411-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500413_in = models.FloatField(db_column='500413-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500114_in = models.FloatField(db_column='500114-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500420_in = models.FloatField(db_column='500420-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500251_in = models.FloatField(db_column='500251-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500148_in = models.FloatField(db_column='500148-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500295_in = models.FloatField(db_column='500295-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500575_in = models.FloatField(db_column='500575-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_502986_in = models.FloatField(db_column='502986-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_505533_in = models.FloatField(db_column='505533-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_500238_in = models.FloatField(db_column='500238-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_505537_in = models.FloatField(db_column='505537-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_504067_in = models.FloatField(db_column='504067-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    number_590095_in = models.FloatField(db_column='590095-IN', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'Volume_Data_Old'


class AccountEmailaddress(models.Model):
    email = models.CharField(unique=True, max_length=254)
    verified = models.BooleanField()
    primary = models.BooleanField()
    user = models.ForeignKey('AuthUser', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'account_emailaddress'


class AccountEmailconfirmation(models.Model):
    created = models.DateTimeField()
    sent = models.DateTimeField(blank=True, null=True)
    key = models.CharField(unique=True, max_length=64)
    email_address = models.ForeignKey(AccountEmailaddress, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'account_emailconfirmation'


class AccountsCustomerfeedback(models.Model):
    rating = models.FloatField(db_column='Rating', blank=True, null=True)  # Field name made lowercase.
    feedback = models.TextField(db_column='Feedback', blank=True, null=True)  # Field name made lowercase.
    country = models.CharField(max_length=10)
    customer = models.ForeignKey('AuthUser', models.DO_NOTHING, db_column='Customer_Id')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'accounts_customerfeedback'


class AccountsCustomerpricemovementsalerts(models.Model):
    customer_email = models.CharField(max_length=255)
    portfolio_name = models.CharField(max_length=255)
    company_name = models.CharField(max_length=255)
    alert_type = models.CharField(max_length=25)
    last_price = models.FloatField(blank=True, null=True)
    current_price = models.FloatField(blank=True, null=True)
    price_52wh = models.FloatField(blank=True, null=True)
    price_value_condition = models.CharField(max_length=25, blank=True, null=True)
    price_value = models.FloatField(blank=True, null=True)
    price_percent_condition = models.CharField(max_length=25, blank=True, null=True)
    price_percent = models.FloatField(blank=True, null=True)
    price_52wh_condition = models.CharField(max_length=25, blank=True, null=True)
    price_52wh_percent = models.FloatField(blank=True, null=True)
    price_createdat = models.DateTimeField(blank=True, null=True)
    pricepercent_createdat = models.DateTimeField(blank=True, null=True)
    price52wh_createdat = models.DateTimeField(blank=True, null=True)
    price_triggeredat = models.DateTimeField(blank=True, null=True)
    percent_triggeredat = models.DateTimeField(blank=True, null=True)
    price52wh_triggeredat = models.DateTimeField(blank=True, null=True)
    price_status = models.CharField(max_length=10)
    percent_status = models.CharField(max_length=10)
    price52wh_status = models.CharField(max_length=10)
    price_read = models.CharField(max_length=5)
    percent_read = models.CharField(max_length=5)
    price52wh_read = models.CharField(max_length=5)
    price_checked = models.CharField(max_length=5)
    percent_checked = models.CharField(max_length=5)
    price52wh_checked = models.CharField(max_length=5)
    country = models.CharField(max_length=10)
    customer_name = models.ForeignKey('AuthUser', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'accounts_customerpricemovementsalerts'


class AccountsCustomersubscription(models.Model):
    first_name = models.CharField(max_length=255, blank=True, null=True)
    last_name = models.CharField(max_length=255, blank=True, null=True)
    email = models.CharField(max_length=255, blank=True, null=True)
    contact_no = models.CharField(max_length=15, blank=True, null=True)
    address = models.CharField(max_length=255, blank=True, null=True)
    pan_no = models.CharField(max_length=255, blank=True, null=True)
    gst_no = models.CharField(max_length=255, blank=True, null=True)
    subscription_type = models.CharField(max_length=255, blank=True, null=True)
    subscription_starts = models.DateField(blank=True, null=True)
    subscription_ends = models.DateField(blank=True, null=True)
    subscription_status = models.CharField(max_length=255)
    subscription_plan = models.CharField(max_length=25, blank=True, null=True)
    paid = models.CharField(max_length=4)
    currency = models.CharField(max_length=4)
    customer_name = models.ForeignKey('AuthUser', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'accounts_customersubscription'


class AccountsCustomertechnicalindicationalerts(models.Model):
    customer_email = models.CharField(max_length=255)
    portfolio_name = models.CharField(max_length=255)
    company_name = models.CharField(max_length=255)
    alert_type = models.CharField(max_length=25)
    factset_ticker = models.CharField(max_length=25, blank=True, null=True)
    rsi_metric = models.CharField(max_length=25, blank=True, null=True)
    rsi_condition = models.CharField(max_length=25, blank=True, null=True)
    rsi_operand1 = models.FloatField(blank=True, null=True)
    rsi_operand2 = models.FloatField(blank=True, null=True)
    dma_operand1 = models.CharField(max_length=25, blank=True, null=True)
    dma_condition = models.CharField(max_length=25, blank=True, null=True)
    dma_operand2 = models.CharField(max_length=25, blank=True, null=True)
    ma9 = models.FloatField(db_column='MA9', blank=True, null=True)  # Field name made lowercase.
    ma20 = models.FloatField(db_column='MA20', blank=True, null=True)  # Field name made lowercase.
    ma26 = models.FloatField(db_column='MA26', blank=True, null=True)  # Field name made lowercase.
    ma50 = models.FloatField(db_column='MA50', blank=True, null=True)  # Field name made lowercase.
    ma100 = models.FloatField(db_column='MA100', blank=True, null=True)  # Field name made lowercase.
    ma200 = models.FloatField(db_column='MA200', blank=True, null=True)  # Field name made lowercase.
    current_price = models.FloatField(blank=True, null=True)
    rsi_createdat = models.DateTimeField(blank=True, null=True)
    dma_createdat = models.DateTimeField(blank=True, null=True)
    rsi_triggeredat = models.DateTimeField(blank=True, null=True)
    dma_triggeredat = models.DateTimeField(blank=True, null=True)
    rsi_status = models.CharField(max_length=10)
    dma_status = models.CharField(max_length=10)
    rsi_read = models.CharField(max_length=10)
    dma_read = models.CharField(max_length=10)
    rsi_checked = models.CharField(max_length=10)
    dma_checked = models.CharField(max_length=10)
    country = models.CharField(max_length=10)
    customer_name = models.ForeignKey('AuthUser', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'accounts_customertechnicalindicationalerts'


class AccountsNewslettersubscription(models.Model):
    email = models.CharField(db_column='Email', max_length=254, blank=True, null=True)  # Field name made lowercase.
    subscribed_at = models.DateTimeField(db_column='Subscribed_at')  # Field name made lowercase.
    country = models.CharField(max_length=10)

    class Meta:
        managed = False
        db_table = 'accounts_newslettersubscription'


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=150)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.BooleanField()
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=150)
    email = models.CharField(max_length=254)
    is_staff = models.BooleanField()
    is_active = models.BooleanField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)


class AuthtokenToken(models.Model):
    key = models.CharField(primary_key=True, max_length=40)
    created = models.DateTimeField()
    user = models.OneToOneField(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'authtoken_token'


class BlogsBlogs(models.Model):
    page_ptr = models.OneToOneField('WagtailcorePage', models.DO_NOTHING, primary_key=True)
    short_description = models.TextField(blank=True, null=True)
    posted_at = models.DateTimeField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    image = models.ForeignKey('WagtailimagesImage', models.DO_NOTHING, blank=True, null=True)
    book_file = models.ForeignKey('WagtaildocsDocument', models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'blogs_blogs'


class BuyTradesBkp(models.Model):
    stock = models.TextField(db_column='Stock', blank=True, null=True)  # Field name made lowercase.
    trade_time = models.DateTimeField(db_column='Trade_Time', blank=True, null=True)  # Field name made lowercase.
    remaining_position = models.BigIntegerField(db_column='Remaining_Position', blank=True, null=True)  # Field name made lowercase.
    price = models.FloatField(db_column='Price', blank=True, null=True)  # Field name made lowercase.
    customer_name = models.TextField(db_column='Customer_Name', blank=True, null=True)  # Field name made lowercase.
    portfolio_name = models.TextField(db_column='Portfolio_name', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'buy_trades_bkp'


class CorporateActionsOccuredCodes(models.Model):
    co_code = models.BigIntegerField(blank=True, null=True)
    company_code = models.FloatField(db_column='Company_Code', blank=True, null=True)  # Field name made lowercase.
    company_name = models.CharField(db_column='Company_Name', max_length=255, blank=True, null=True)  # Field name made lowercase.
    category = models.CharField(db_column='Category', max_length=255, blank=True, null=True)  # Field name made lowercase.
    isin = models.CharField(db_column='ISIN', max_length=255, blank=True, null=True)  # Field name made lowercase.
    fs_ticker = models.CharField(db_column='FS_Ticker', max_length=255, blank=True, null=True)  # Field name made lowercase.
    etf_flag = models.CharField(db_column='ETF_Flag', max_length=5, blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'corporate_Actions_Occured_Codes'


class DailyPositionsBkp(models.Model):
    date = models.DateField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    stock = models.TextField(db_column='Stock', blank=True, null=True)  # Field name made lowercase.
    buy_count = models.BigIntegerField(db_column='Buy_Count', blank=True, null=True)  # Field name made lowercase.
    buy_quantity = models.BigIntegerField(db_column='Buy_Quantity', blank=True, null=True)  # Field name made lowercase.
    sell_count = models.FloatField(db_column='Sell_Count', blank=True, null=True)  # Field name made lowercase.
    sell_quantity = models.FloatField(db_column='Sell_Quantity', blank=True, null=True)  # Field name made lowercase.
    close_price = models.FloatField(db_column='Close_Price', blank=True, null=True)  # Field name made lowercase.
    prev_close_price = models.FloatField(db_column='Prev_Close_Price', blank=True, null=True)  # Field name made lowercase.
    prev_position = models.BigIntegerField(db_column='Prev_Position', blank=True, null=True)  # Field name made lowercase.
    prev_p_lunrealized = models.BigIntegerField(db_column='Prev_P&LUnrealized', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    bbg_ticker = models.TextField(db_column='BBG_Ticker', blank=True, null=True)  # Field name made lowercase.
    position = models.FloatField(db_column='Position', blank=True, null=True)  # Field name made lowercase.
    p_lrealized = models.FloatField(db_column='P&LRealized', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    p_lunrealized = models.FloatField(db_column='P&LUnrealized', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    p_l = models.FloatField(db_column='P&L', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    average_buy_price = models.FloatField(db_column='Average_Buy_Price', blank=True, null=True)  # Field name made lowercase.
    average_sell_price = models.FloatField(db_column='Average_Sell_Price', blank=True, null=True)  # Field name made lowercase.
    long_term_qty = models.BigIntegerField(db_column='Long_Term_Qty', blank=True, null=True)  # Field name made lowercase.
    net = models.FloatField(db_column='Net', blank=True, null=True)  # Field name made lowercase.
    aum_eod = models.FloatField(db_column='AUM_EOD', blank=True, null=True)  # Field name made lowercase.
    aum_bod = models.FloatField(db_column='AUM_BOD', blank=True, null=True)  # Field name made lowercase.
    customer_name = models.TextField(blank=True, null=True)
    portfolio_name = models.TextField(blank=True, null=True)
    idhp = models.TextField(db_column='IDHP', blank=True, null=True)  # Field name made lowercase.
    divamount = models.FloatField(db_column='DivAmount', blank=True, null=True)  # Field name made lowercase.
    dividend_pnl = models.FloatField(db_column='Dividend_PNL', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'daily_positions_bkp'


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.SmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'


class DjangoSite(models.Model):
    domain = models.CharField(unique=True, max_length=100)
    name = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'django_site'


class EarningsTranscriptsDump(models.Model):
    file_name = models.TextField(db_column='File_Name', blank=True, null=True)  # Field name made lowercase.
    ticker = models.TextField(db_column='Ticker', blank=True, null=True)  # Field name made lowercase.
    quarter_year = models.TextField(db_column='Quarter_Year', blank=True, null=True)  # Field name made lowercase.
    content = models.TextField(db_column='Content', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'earnings_transcripts_dump'


class EarningsTranscriptsDumpSentiment(models.Model):
    ticker = models.CharField(db_column='Ticker', max_length=200)  # Field name made lowercase.
    content = models.TextField(db_column='Content')  # Field name made lowercase. This field type is a guess.
    quarter = models.CharField(db_column='Quarter', max_length=10)  # Field name made lowercase.
    year = models.CharField(db_column='Year', max_length=10)  # Field name made lowercase.
    polarity = models.FloatField()
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'earnings_transcripts_dump_sentiment'


class EarningsTranscriptsDumpSentimentNew(models.Model):
    ticker = models.CharField(db_column='Ticker', max_length=500)  # Field name made lowercase.
    content = models.TextField(db_column='Content')  # Field name made lowercase. This field type is a guess.
    quarter = models.CharField(db_column='Quarter', max_length=10)  # Field name made lowercase.
    year = models.CharField(db_column='Year', max_length=10)  # Field name made lowercase.
    avg_positive_polarity = models.FloatField(db_column='Avg_positive_polarity')  # Field name made lowercase.
    avg_negative_polarity = models.FloatField(db_column='Avg_negative_polarity')  # Field name made lowercase.
    positive_count = models.IntegerField(db_column='Positive_count', blank=True, null=True)  # Field name made lowercase.
    negative_count = models.IntegerField(db_column='Negative_count', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'earnings_transcripts_dump_sentiment_new'


class Gicswisecompaniesclassification(models.Model):
    gics = models.CharField(db_column='GICS', max_length=255, blank=True, null=True)  # Field name made lowercase.
    description = models.TextField(db_column='Description', blank=True, null=True)  # Field name made lowercase. This field type is a guess.
    bottom_tier = models.CharField(db_column='Bottom Tier', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    mid_tier = models.CharField(db_column='Mid Tier', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    top_tier = models.CharField(db_column='Top Tier', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    rating = models.CharField(db_column='Rating', max_length=255, blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'gicswisecompaniesclassification'


class InvitationsInvitation(models.Model):
    id = models.AutoField()
    email = models.CharField(max_length=254)
    accepted = models.BooleanField()
    created = models.DateTimeField()
    key = models.CharField(max_length=64)
    sent = models.DateTimeField(blank=True, null=True)
    inviter_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'invitations_invitation'


class MapNse(models.Model):
    symbol = models.CharField(db_column='Symbol', max_length=255, blank=True, null=True)  # Field name made lowercase.
    company_name = models.CharField(db_column='Company Name', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'map_nse'


class PagesFuturesandoptions(models.Model):
    company_name = models.CharField(max_length=255)
    fs_ticker = models.CharField(max_length=255, blank=True, null=True)
    sector = models.CharField(max_length=255, blank=True, null=True)
    month = models.CharField(max_length=255)
    begin_price = models.FloatField(blank=True, null=True)
    high_probability_range = models.CharField(db_column='High_Probability_Range', max_length=255, blank=True, null=True)  # Field name made lowercase.
    remark_1 = models.CharField(db_column='Remark_1', max_length=255, blank=True, null=True)  # Field name made lowercase.
    signal = models.CharField(max_length=255, blank=True, null=True)
    end_price = models.FloatField(blank=True, null=True)
    signal_efficiency = models.CharField(db_column='Signal_Efficiency', max_length=2, blank=True, null=True)  # Field name made lowercase.
    start_date = models.DateField(blank=True, null=True)
    end_date = models.DateField(blank=True, null=True)
    country = models.CharField(max_length=5, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pages_futuresandoptions'


class PagesIndexfuturesandoptions(models.Model):
    country = models.CharField(max_length=30, blank=True, null=True)
    index = models.CharField(db_column='Index', max_length=255, blank=True, null=True)  # Field name made lowercase.
    month = models.CharField(max_length=255, blank=True, null=True)
    begin_price = models.FloatField(blank=True, null=True)
    high_probability_range = models.TextField(db_column='High_Probability_Range', blank=True, null=True)  # Field name made lowercase.
    remark_1 = models.TextField(db_column='Remark_1', blank=True, null=True)  # Field name made lowercase.
    end_price = models.FloatField(blank=True, null=True)
    signal_efficiency = models.CharField(db_column='Signal_Efficiency', max_length=2, blank=True, null=True)  # Field name made lowercase.
    start_date = models.DateField(blank=True, null=True)
    end_date = models.DateField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pages_indexfuturesandoptions'


class PagesOurindiamarketview(models.Model):
    page_ptr = models.OneToOneField('WagtailcorePage', models.DO_NOTHING, primary_key=True)
    posted_at = models.DateTimeField(blank=True, null=True)
    body = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pages_ourindiamarketview'


class PagesOurusmarketview(models.Model):
    page_ptr = models.OneToOneField('WagtailcorePage', models.DO_NOTHING, primary_key=True)
    posted_at = models.DateTimeField(blank=True, null=True)
    body = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pages_ourusmarketview'


class PagesResearchreports(models.Model):
    page_ptr = models.OneToOneField('WagtailcorePage', models.DO_NOTHING, primary_key=True)
    country = models.CharField(max_length=30, blank=True, null=True)
    sector = models.CharField(max_length=255)
    posted_at = models.DateTimeField(blank=True, null=True)
    book_file = models.ForeignKey('WagtaildocsDocument', models.DO_NOTHING, blank=True, null=True)
    image = models.ForeignKey('WagtailimagesImage', models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pages_researchreports'


class PagesTradeideasperformance(models.Model):
    company_name = models.CharField(max_length=255)
    fs_ticker = models.CharField(max_length=255, blank=True, null=True)
    buy_sell = models.CharField(max_length=4)
    issue_date = models.DateTimeField()
    purchased_price = models.CharField(max_length=255, blank=True, null=True)
    target_price = models.CharField(max_length=255, blank=True, null=True)
    time_period = models.CharField(max_length=255, blank=True, null=True)
    performance = models.CharField(max_length=255, blank=True, null=True)
    action = models.CharField(max_length=255, blank=True, null=True)
    signal_message = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pages_tradeideasperformance'


class PagesTradesignals(models.Model):
    diamond = models.CharField(max_length=255)
    company_name = models.CharField(max_length=255)
    fs_ticker = models.CharField(max_length=255, blank=True, null=True)
    fundamental = models.TextField()
    fund_rating = models.IntegerField()
    quantitative = models.TextField()
    quant_rating = models.IntegerField()
    technical = models.TextField()
    technical_rating = models.IntegerField()
    situations = models.TextField()
    situations_rating = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'pages_tradesignals'


class PagesUstradeideasperformance(models.Model):
    company_name = models.CharField(max_length=255)
    fs_ticker = models.CharField(max_length=255, blank=True, null=True)
    buy_sell = models.CharField(max_length=4)
    issue_date = models.DateTimeField()
    purchased_price = models.CharField(max_length=255, blank=True, null=True)
    target_price = models.CharField(max_length=255, blank=True, null=True)
    time_period = models.CharField(max_length=255, blank=True, null=True)
    performance = models.CharField(max_length=255, blank=True, null=True)
    action = models.CharField(max_length=255, blank=True, null=True)
    signal_message = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pages_ustradeideasperformance'


class PagesUstradesignals(models.Model):
    diamond = models.CharField(max_length=255)
    company_name = models.CharField(max_length=255)
    fs_ticker = models.CharField(max_length=255, blank=True, null=True)
    fundamental = models.TextField()
    fund_rating = models.IntegerField()
    quantitative = models.TextField()
    quant_rating = models.IntegerField()
    technical = models.TextField()
    technical_rating = models.IntegerField()
    situations = models.TextField()
    situations_rating = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'pages_ustradesignals'


class RobotsRule(models.Model):
    id = models.AutoField()
    robot = models.CharField(max_length=255)
    crawl_delay = models.DecimalField(max_digits=3, decimal_places=1, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'robots_rule'


class RobotsRuleAllowed(models.Model):
    id = models.AutoField()
    rule_id = models.IntegerField()
    url_id = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'robots_rule_allowed'


class RobotsRuleDisallowed(models.Model):
    id = models.AutoField()
    rule_id = models.IntegerField()
    url_id = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'robots_rule_disallowed'


class RobotsRuleSites(models.Model):
    id = models.AutoField()
    rule_id = models.IntegerField()
    site_id = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'robots_rule_sites'


class RobotsUrl(models.Model):
    id = models.AutoField()
    pattern = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'robots_url'


class ScreeningDummy(models.Model):
    fs_name = models.TextField(db_column='FS Name', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    security_code = models.TextField(db_column='Security Code', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    fs_ticker = models.TextField(db_column='FS Ticker', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    market_cap = models.FloatField(db_column='Market Cap', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    market_cap_category = models.TextField(db_column='Market_Cap_Category', blank=True, null=True)  # Field name made lowercase.
    sales = models.FloatField(db_column='Sales', blank=True, null=True)  # Field name made lowercase.
    gics = models.TextField(db_column='GICS', blank=True, null=True)  # Field name made lowercase.
    beta_1y = models.FloatField(db_column='Beta_1Y', blank=True, null=True)  # Field name made lowercase.
    volatility_1y = models.FloatField(db_column='Volatility_1Y', blank=True, null=True)  # Field name made lowercase.
    piotroski_score = models.FloatField(db_column='Piotroski_Score', blank=True, null=True)  # Field name made lowercase.
    pe = models.FloatField(db_column='PE', blank=True, null=True)  # Field name made lowercase.
    pb = models.FloatField(db_column='PB', blank=True, null=True)  # Field name made lowercase.
    ev_sales = models.FloatField(db_column='EV/Sales', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ev_ebitda = models.FloatField(db_column='EV/EBITDA', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    div_yield = models.FloatField(db_column='Div_Yield', blank=True, null=True)  # Field name made lowercase.
    sales_growth = models.FloatField(db_column='Sales_Growth', blank=True, null=True)  # Field name made lowercase.
    sales_growth_3y_5r_cagr = models.FloatField(db_column='Sales_Growth_3Y/5R_CAGR', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    eps_growth = models.FloatField(db_column='EPS_Growth', blank=True, null=True)  # Field name made lowercase.
    eps_growth_3y_5r_cagr = models.FloatField(db_column='EPS_Growth_3Y/5R_CAGR', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    roe = models.FloatField(db_column='ROE', blank=True, null=True)  # Field name made lowercase.
    roce = models.FloatField(db_column='ROCE', blank=True, null=True)  # Field name made lowercase.
    fcf_margin = models.FloatField(db_column='FCF_Margin', blank=True, null=True)  # Field name made lowercase.
    sales_total_assets = models.FloatField(db_column='Sales/Total Assets', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    net_debt_fcf = models.FloatField(db_column='Net_Debt/FCF', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    net_debt_ebitda = models.FloatField(db_column='Net_Debt/EBITDA', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    debt_equity = models.FloatField(db_column='Debt/Equity', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    price = models.FloatField(db_column='Price', blank=True, null=True)  # Field name made lowercase.
    f_o = models.TextField(db_column='F_O', blank=True, null=True)  # Field name made lowercase.
    volume = models.FloatField(db_column='Volume', blank=True, null=True)  # Field name made lowercase.
    rsi = models.FloatField(db_column='RSI', blank=True, null=True)  # Field name made lowercase.
    ma50 = models.FloatField(db_column='MA50', blank=True, null=True)  # Field name made lowercase.
    ma200 = models.FloatField(db_column='MA200', blank=True, null=True)  # Field name made lowercase.
    ma20 = models.FloatField(db_column='MA20', blank=True, null=True)  # Field name made lowercase.
    number_52_week_high = models.FloatField(db_column='52_Week_High', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    target_price = models.FloatField(db_column='Target_Price', blank=True, null=True)  # Field name made lowercase.
    volume_20 = models.FloatField(db_column='Volume_20', blank=True, null=True)  # Field name made lowercase.
    rsi_30 = models.FloatField(db_column='RSI_30', blank=True, null=True)  # Field name made lowercase.
    rsi_90 = models.FloatField(db_column='RSI_90', blank=True, null=True)  # Field name made lowercase.
    rsi_180 = models.FloatField(db_column='RSI_180', blank=True, null=True)  # Field name made lowercase.
    rsi_365 = models.FloatField(db_column='RSI_365', blank=True, null=True)  # Field name made lowercase.
    share_turnover = models.FloatField(db_column='Share Turnover', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    volume_change = models.FloatField(db_column='Volume_Change', blank=True, null=True)  # Field name made lowercase.
    mtd = models.FloatField(db_column='MTD', blank=True, null=True)  # Field name made lowercase.
    ytd = models.FloatField(db_column='YTD', blank=True, null=True)  # Field name made lowercase.
    number_1y = models.FloatField(db_column='1Y', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    onem = models.FloatField(db_column='OneM', blank=True, null=True)  # Field name made lowercase.
    threem = models.FloatField(db_column='threeM', blank=True, null=True)  # Field name made lowercase.
    sixm = models.FloatField(db_column='sixM', blank=True, null=True)  # Field name made lowercase.
    qtd = models.FloatField(db_column='QTD', blank=True, null=True)  # Field name made lowercase.
    gross_profit_margin = models.FloatField(blank=True, null=True)
    altmanz_score = models.FloatField(db_column='Altmanz_score', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'screening_dummy'


class SocialaccountSocialaccount(models.Model):
    provider = models.CharField(max_length=30)
    uid = models.CharField(max_length=191)
    last_login = models.DateTimeField()
    date_joined = models.DateTimeField()
    extra_data = models.TextField()
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'socialaccount_socialaccount'
        unique_together = (('provider', 'uid'),)


class SocialaccountSocialapp(models.Model):
    provider = models.CharField(max_length=30)
    name = models.CharField(max_length=40)
    client_id = models.CharField(max_length=191)
    secret = models.CharField(max_length=191)
    key = models.CharField(max_length=191)

    class Meta:
        managed = False
        db_table = 'socialaccount_socialapp'


class SocialaccountSocialappSites(models.Model):
    socialapp = models.ForeignKey(SocialaccountSocialapp, models.DO_NOTHING)
    site = models.ForeignKey(DjangoSite, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'socialaccount_socialapp_sites'
        unique_together = (('socialapp', 'site'),)


class SocialaccountSocialtoken(models.Model):
    token = models.TextField()
    token_secret = models.TextField()
    expires_at = models.DateTimeField(blank=True, null=True)
    account = models.ForeignKey(SocialaccountSocialaccount, models.DO_NOTHING)
    app = models.ForeignKey(SocialaccountSocialapp, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'socialaccount_socialtoken'
        unique_together = (('app', 'account'),)


class Sysdiagrams(models.Model):
    name = models.CharField(max_length=128)
    principal_id = models.IntegerField()
    diagram_id = models.AutoField()
    version = models.IntegerField(blank=True, null=True)
    definition = models.BinaryField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'sysdiagrams'


class TaggitTag(models.Model):
    name = models.CharField(unique=True, max_length=100)
    slug = models.CharField(unique=True, max_length=100)

    class Meta:
        managed = False
        db_table = 'taggit_tag'


class TaggitTaggeditem(models.Model):
    object_id = models.IntegerField()
    content_type = models.ForeignKey(DjangoContentType, models.DO_NOTHING)
    tag = models.ForeignKey(TaggitTag, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'taggit_taggeditem'
        unique_together = (('content_type', 'object_id', 'tag'),)


class TransactionsBkp(models.Model):
    security_id = models.CharField(db_column='SECURITY ID', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    tradedate = models.DateTimeField(db_column='TRADEDATE', blank=True, null=True)  # Field name made lowercase.
    action = models.CharField(db_column='ACTION', max_length=255, blank=True, null=True)  # Field name made lowercase.
    status = models.CharField(db_column='STATUS', max_length=255, blank=True, null=True)  # Field name made lowercase.
    allocation_id = models.CharField(db_column='ALLOCATION ID', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    secdescription = models.CharField(db_column='SECDESCRIPTION', max_length=255, blank=True, null=True)  # Field name made lowercase.
    sedol = models.FloatField(db_column='SEDOL', blank=True, null=True)  # Field name made lowercase.
    isin = models.FloatField(db_column='ISIN', blank=True, null=True)  # Field name made lowercase.
    cusip = models.FloatField(db_column='CUSIP', blank=True, null=True)  # Field name made lowercase.
    trader = models.CharField(db_column='TRADER', max_length=255, blank=True, null=True)  # Field name made lowercase.
    portfolio = models.CharField(db_column='PORTFOLIO', max_length=255, blank=True, null=True)  # Field name made lowercase.
    customer = models.CharField(db_column='Customer', max_length=255, blank=True, null=True)  # Field name made lowercase.
    quantity = models.FloatField(db_column='QUANTITY', blank=True, null=True)  # Field name made lowercase.
    price = models.FloatField(db_column='PRICE', blank=True, null=True)  # Field name made lowercase.
    fx_rate = models.FloatField(db_column='FX RATE', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    grossmoney = models.FloatField(db_column='GROSSMONEY', blank=True, null=True)  # Field name made lowercase.
    totalcomm = models.FloatField(db_column='TOTALCOMM', blank=True, null=True)  # Field name made lowercase.
    totalfees = models.FloatField(db_column='TOTALFEES', blank=True, null=True)  # Field name made lowercase.
    netmoney = models.FloatField(db_column='NETMONEY', blank=True, null=True)  # Field name made lowercase.
    execcurrency = models.CharField(db_column='EXECCURRENCY', max_length=255, blank=True, null=True)  # Field name made lowercase.
    broker = models.CharField(db_column='BROKER', max_length=255, blank=True, null=True)  # Field name made lowercase.
    custodian = models.CharField(db_column='CUSTODIAN', max_length=255, blank=True, null=True)  # Field name made lowercase.
    accrued_int = models.CharField(db_column='ACCRUED INT', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    strategy = models.CharField(db_column='STRATEGY', max_length=255, blank=True, null=True)  # Field name made lowercase.
    bid_qty = models.CharField(db_column='BID QTY', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    bid_price = models.CharField(db_column='BID PRICE', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    pricing_date = models.DateTimeField(db_column='PRICING DATE', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    first_trade_date = models.DateTimeField(db_column='FIRST TRADE DATE', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    deal_captian = models.CharField(db_column='DEAL CAPTIAN', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    hedge = models.CharField(db_column='HEDGE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    intial_target_price = models.CharField(db_column='INTIAL TARGET PRICE', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    target_price = models.CharField(db_column='TARGET PRICE', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    ultimate_stop = models.CharField(db_column='ULTIMATE STOP', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    country = models.CharField(db_column='COUNTRY', max_length=255, blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'transactions_bkp'


class UsScreenerTechnical(models.Model):
    relative_strength_30d = models.CharField(db_column='Relative Strength 30D', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    relative_strength_90d = models.CharField(db_column='Relative Strength 90D', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    volume_change = models.CharField(db_column='Volume Change', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    number_52_week_high = models.CharField(db_column='52 Week High', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it wasn't a valid Python identifier.
    ma20 = models.CharField(db_column='MA20', max_length=255, blank=True, null=True)  # Field name made lowercase.
    ma50 = models.CharField(db_column='MA50', max_length=255, blank=True, null=True)  # Field name made lowercase.
    ma200 = models.CharField(db_column='MA200', max_length=255, blank=True, null=True)  # Field name made lowercase.
    rsi_14d_field = models.CharField(db_column='RSI (14D)', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it ended with '_'.
    absolute_return_mtd = models.CharField(db_column='Absolute_Return_MTD', max_length=255, blank=True, null=True)  # Field name made lowercase.
    absolute_return_ytd = models.CharField(db_column='Absolute_Return_YTD', max_length=255, blank=True, null=True)  # Field name made lowercase.
    absolute_return_qtd = models.CharField(db_column='Absolute_Return_QTD', max_length=255, blank=True, null=True)  # Field name made lowercase.
    absolute_return_1y = models.CharField(db_column='Absolute_Return_1Y', max_length=255, blank=True, null=True)  # Field name made lowercase.
    absolute_return_1m = models.CharField(db_column='Absolute_Return_1M', max_length=255, blank=True, null=True)  # Field name made lowercase.
    absolute_return_3m = models.CharField(db_column='Absolute_Return_3M', max_length=255, blank=True, null=True)  # Field name made lowercase.
    absolute_return_6m = models.CharField(db_column='Absolute_Return_6M', max_length=255, blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'us_Screener_Technical'


class WagtailcoreCollection(models.Model):
    path = models.CharField(unique=True, max_length=255)
    depth = models.IntegerField()
    numchild = models.IntegerField()
    name = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'wagtailcore_collection'


class WagtailcoreCollectionviewrestriction(models.Model):
    restriction_type = models.CharField(max_length=20)
    password = models.CharField(max_length=255)
    collection = models.ForeignKey(WagtailcoreCollection, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'wagtailcore_collectionviewrestriction'


class WagtailcoreCollectionviewrestrictionGroups(models.Model):
    collectionviewrestriction = models.ForeignKey(WagtailcoreCollectionviewrestriction, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'wagtailcore_collectionviewrestriction_groups'
        unique_together = (('collectionviewrestriction', 'group'),)


class WagtailcoreGroupcollectionpermission(models.Model):
    collection = models.ForeignKey(WagtailcoreCollection, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'wagtailcore_groupcollectionpermission'
        unique_together = (('group', 'collection', 'permission'),)


class WagtailcoreGrouppagepermission(models.Model):
    permission_type = models.CharField(max_length=20)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    page = models.ForeignKey('WagtailcorePage', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'wagtailcore_grouppagepermission'
        unique_together = (('group', 'page', 'permission_type'),)


class WagtailcorePage(models.Model):
    path = models.CharField(unique=True, max_length=255)
    depth = models.IntegerField()
    numchild = models.IntegerField()
    title = models.CharField(max_length=255)
    slug = models.CharField(max_length=255)
    live = models.BooleanField()
    has_unpublished_changes = models.BooleanField()
    url_path = models.TextField()
    seo_title = models.CharField(max_length=255)
    show_in_menus = models.BooleanField()
    search_description = models.TextField()
    go_live_at = models.DateTimeField(blank=True, null=True)
    expire_at = models.DateTimeField(blank=True, null=True)
    expired = models.BooleanField()
    content_type = models.ForeignKey(DjangoContentType, models.DO_NOTHING)
    owner = models.ForeignKey(AuthUser, models.DO_NOTHING, blank=True, null=True)
    locked = models.BooleanField()
    latest_revision_created_at = models.DateTimeField(blank=True, null=True)
    first_published_at = models.DateTimeField(blank=True, null=True)
    live_revision = models.ForeignKey('WagtailcorePagerevision', models.DO_NOTHING, blank=True, null=True)
    last_published_at = models.DateTimeField(blank=True, null=True)
    draft_title = models.CharField(max_length=255)
    locked_at = models.DateTimeField(blank=True, null=True)
    locked_by = models.ForeignKey(AuthUser, models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'wagtailcore_page'


class WagtailcorePagerevision(models.Model):
    submitted_for_moderation = models.BooleanField()
    created_at = models.DateTimeField()
    content_json = models.TextField()
    approved_go_live_at = models.DateTimeField(blank=True, null=True)
    page = models.ForeignKey(WagtailcorePage, models.DO_NOTHING)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'wagtailcore_pagerevision'


class WagtailcorePageviewrestriction(models.Model):
    password = models.CharField(max_length=255)
    page = models.ForeignKey(WagtailcorePage, models.DO_NOTHING)
    restriction_type = models.CharField(max_length=20)

    class Meta:
        managed = False
        db_table = 'wagtailcore_pageviewrestriction'


class WagtailcorePageviewrestrictionGroups(models.Model):
    pageviewrestriction = models.ForeignKey(WagtailcorePageviewrestriction, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'wagtailcore_pageviewrestriction_groups'


class WagtailcoreSite(models.Model):
    hostname = models.CharField(max_length=255)
    port = models.IntegerField()
    is_default_site = models.BooleanField()
    root_page = models.ForeignKey(WagtailcorePage, models.DO_NOTHING)
    site_name = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'wagtailcore_site'
        unique_together = (('hostname', 'port'),)


class WagtaildocsDocument(models.Model):
    title = models.CharField(max_length=255)
    file = models.CharField(max_length=100)
    created_at = models.DateTimeField()
    uploaded_by_user = models.ForeignKey(AuthUser, models.DO_NOTHING, blank=True, null=True)
    collection = models.ForeignKey(WagtailcoreCollection, models.DO_NOTHING)
    file_size = models.IntegerField(blank=True, null=True)
    file_hash = models.CharField(max_length=40)

    class Meta:
        managed = False
        db_table = 'wagtaildocs_document'


class WagtailembedsEmbed(models.Model):
    url = models.CharField(max_length=200)
    max_width = models.SmallIntegerField(blank=True, null=True)
    type = models.CharField(max_length=10)
    html = models.TextField()
    title = models.TextField()
    author_name = models.TextField()
    provider_name = models.TextField()
    thumbnail_url = models.CharField(max_length=255, blank=True, null=True)
    width = models.IntegerField(blank=True, null=True)
    height = models.IntegerField(blank=True, null=True)
    last_updated = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'wagtailembeds_embed'
        unique_together = (('url', 'max_width'),)


class WagtailformsFormsubmission(models.Model):
    form_data = models.TextField()
    submit_time = models.DateTimeField()
    page = models.ForeignKey(WagtailcorePage, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'wagtailforms_formsubmission'


class WagtailimagesImage(models.Model):
    title = models.CharField(max_length=255)
    file = models.CharField(max_length=100)
    width = models.IntegerField()
    height = models.IntegerField()
    created_at = models.DateTimeField()
    focal_point_x = models.IntegerField(blank=True, null=True)
    focal_point_y = models.IntegerField(blank=True, null=True)
    focal_point_width = models.IntegerField(blank=True, null=True)
    focal_point_height = models.IntegerField(blank=True, null=True)
    uploaded_by_user = models.ForeignKey(AuthUser, models.DO_NOTHING, blank=True, null=True)
    file_size = models.IntegerField(blank=True, null=True)
    collection = models.ForeignKey(WagtailcoreCollection, models.DO_NOTHING)
    file_hash = models.CharField(max_length=40)

    class Meta:
        managed = False
        db_table = 'wagtailimages_image'


class WagtailimagesRendition(models.Model):
    file = models.CharField(max_length=100)
    width = models.IntegerField()
    height = models.IntegerField()
    focal_point_key = models.CharField(max_length=16)
    filter_spec = models.CharField(max_length=255)
    image = models.ForeignKey(WagtailimagesImage, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'wagtailimages_rendition'
        unique_together = (('image', 'filter_spec', 'focal_point_key'),)


class WagtailimagesUploadedimage(models.Model):
    file = models.CharField(max_length=200)
    uploaded_by_user = models.ForeignKey(AuthUser, models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'wagtailimages_uploadedimage'


class WagtailredirectsRedirect(models.Model):
    old_path = models.CharField(max_length=255)
    is_permanent = models.BooleanField()
    redirect_link = models.CharField(max_length=255)
    redirect_page = models.ForeignKey(WagtailcorePage, models.DO_NOTHING, blank=True, null=True)
    site = models.ForeignKey(WagtailcoreSite, models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'wagtailredirects_redirect'
        unique_together = (('old_path', 'site'),)


class WagtailsearchEditorspick(models.Model):
    sort_order = models.IntegerField(blank=True, null=True)
    description = models.TextField()
    page = models.ForeignKey(WagtailcorePage, models.DO_NOTHING)
    query = models.ForeignKey('WagtailsearchQuery', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'wagtailsearch_editorspick'


class WagtailsearchQuery(models.Model):
    query_string = models.CharField(unique=True, max_length=255)

    class Meta:
        managed = False
        db_table = 'wagtailsearch_query'


class WagtailsearchQuerydailyhits(models.Model):
    date = models.DateField()
    hits = models.IntegerField()
    query = models.ForeignKey(WagtailsearchQuery, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'wagtailsearch_querydailyhits'
        unique_together = (('query', 'date'),)


class WagtailusersUserprofile(models.Model):
    submitted_notifications = models.BooleanField()
    approved_notifications = models.BooleanField()
    rejected_notifications = models.BooleanField()
    user = models.OneToOneField(AuthUser, models.DO_NOTHING)
    preferred_language = models.CharField(max_length=10)
    current_time_zone = models.CharField(max_length=40)
    avatar = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'wagtailusers_userprofile'


class Wizardtable(models.Model):
    date = models.DateField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    index_name = models.TextField(blank=True, null=True)
    company = models.TextField(db_column='Company', blank=True, null=True)  # Field name made lowercase.
    factset_ticker = models.TextField(db_column='Factset_Ticker', blank=True, null=True)  # Field name made lowercase.
    price = models.FloatField(db_column='Price', blank=True, null=True)  # Field name made lowercase.
    return_field = models.FloatField(db_column='return', blank=True, null=True)  # Field renamed because it was a Python reserved word.
    security_code = models.TextField(db_column='Security Code', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    number_52_week_high = models.FloatField(db_column='52_Week_High', blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    current_price = models.FloatField(db_column='Current_Price', blank=True, null=True)  # Field name made lowercase.
    piotroski_score = models.FloatField(db_column='Piotroski_Score', blank=True, null=True)  # Field name made lowercase.
    div_yield = models.FloatField(db_column='Div_Yield', blank=True, null=True)  # Field name made lowercase.
    div_payout = models.FloatField(db_column='Div_Payout', blank=True, null=True)  # Field name made lowercase.
    roe = models.FloatField(db_column='ROE', blank=True, null=True)  # Field name made lowercase.
    market_cap_category = models.TextField(db_column='Market_Cap_Category', blank=True, null=True)  # Field name made lowercase.
    gics = models.TextField(blank=True, null=True)
    market_cap = models.FloatField(db_column='Market Cap', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    rsi = models.FloatField(db_column='RSI', blank=True, null=True)  # Field name made lowercase.
    ma9 = models.FloatField(db_column='MA9', blank=True, null=True)  # Field name made lowercase.
    ma26 = models.FloatField(db_column='MA26', blank=True, null=True)  # Field name made lowercase.
    tier = models.TextField(db_column='Tier', blank=True, null=True)  # Field name made lowercase.
    niftyuniverse = models.TextField(blank=True, null=True)
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'wizardTable'
