from django.db import models
from modelcluster.fields import ParentalKey
from wagtail.core.models import Page, Orderable
from wagtail.core.fields import StreamField
from wagtail.core import blocks
from wagtail.admin.edit_handlers import FieldPanel, MultiFieldPanel, InlinePanel, StreamFieldPanel
from wagtail.images.edit_handlers import ImageChooserPanel
from wagtail.documents.models import Document
from wagtail.documents.edit_handlers import DocumentChooserPanel
from wagtail.contrib.settings.models import BaseSetting, register_setting
from wagtail.contrib.routable_page.models import RoutablePageMixin, route
from wagtail.core import hooks
from wagtail.api import APIField
from django.core.validators import MaxValueValidator, MinValueValidator
from wagtail.images.api.fields import ImageRenditionField

# Create your models here.

class Blogs(Page):
    short_description = models.TextField('Short Description',blank=True, null=True)
    posted_at = models.DateTimeField('Posted At',auto_now_add=True,null=True)
    image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )
    description = StreamField([
        ('Description', blocks.RichTextBlock(null=True)),
    ],null=True)
    content_panels = Page.content_panels + [
        FieldPanel('short_description'),
        ImageChooserPanel('image'),
        StreamFieldPanel('description'),
    ]

    # Export fields over the API
    api_fields = [
        APIField('short_description'),
        APIField('posted_at'),
        APIField('image'),
        APIField('description'),
    ]