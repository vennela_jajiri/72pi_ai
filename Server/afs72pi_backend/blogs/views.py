from django.shortcuts import render
from .models import Blogs
from rest_framework.response import Response
from rest_framework.views import APIView
from django.contrib.auth.models import User
from wagtail.images.models import Image
from rest_framework.permissions import IsAuthenticated
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
import json


# Create your views here.

#Blog view
class BlogView(APIView):
    authentication_classes = []
    permission_classes = []

    #on GET type call return all the blogs
    def get(self, request, format=None):
        protocol = 'https' if self.request.is_secure() else 'http'
        domain_name = self.request.get_host()
        site_url= '%s://%s' % (protocol, domain_name)
        blogs = Blogs.objects.values('title','short_description','posted_at','image_id').order_by('-posted_at')
        blogposts = []
        for blog in blogs:
            blogposts.append(blog['title'])
            images = Image.objects.filter(id=blog['image_id']).values('file').first()
            blog['image_title'] = images['file']
        context = {'blogs':blogs,'site':site_url,'blogposts':blogposts}
        return Response(context)

    #on POST type call return the selected blog for readmore
    def post(self, request, format=None):
        protocol = 'https' if self.request.is_secure() else 'http'
        domain_name = self.request.get_host()
        site_url= '%s://%s' % (protocol, domain_name)
        requestBody = json.loads(request.body.decode('utf-8'))
        selected_blog_title = requestBody['title']
        blogs = Blogs.objects.filter(title=selected_blog_title).values('title','description','posted_at','image_id')
        for blog in blogs:
            images = Image.objects.filter(id=blog['image_id']).values('file').first()
            blog['image_title'] = images['file']
            blog['description'] = str(blog['description']).replace('src="','src="'+site_url)
        context = {'blog':blogs,'site':site_url}
        return Response(context)