from django.urls import path
from blogs import views
from .api import api_router

urlpatterns = [
    path('', views.BlogView.as_view(), name='blog'),
]

